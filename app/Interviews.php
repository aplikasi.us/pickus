<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interviews extends Model {

    protected $table = "interviews";

    public function client() {
        return $this->belongsTo('App\Clients', 'client_id', 'id');
    }

    public function job() {
        return $this->belongsTo('App\Jobopenings', 'job_id', 'id');
    }

    public function candidate() {
        return $this->belongsTo('App\Candidates', 'candidate_id', 'id');
    }

    public function notes() {
        return $this->hasMany('App\Interviewnote', 'INTERVIEWID', 'INTERVIEWID');
    }

}
