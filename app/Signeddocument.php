<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signeddocument extends Model
{
    protected $table = "signed_documents";
    
    public function attachments(){
        return $this->hasMany('App\Signeddocumentattachment','signed_document_id','id');
    }
}
