<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidateprofilerequests extends Model
{
    protected $table = "candidate_profile_requests";

    public function candidate(){
        return $this->belongsTo('App\Candidates','CANDIDATEID','CANDIDATEID');
    }
    public function client(){
        return $this->belongsTo('App\Clientcontact','CONTACTID','CONTACTID');
    }
}