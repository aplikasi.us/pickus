<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultant extends Model {

    protected $table = "candidates";

    public function experience() {
        return $this->hasMany('App\Experience', 'CANDIDATEID', 'CANDIDATEID');
    }
    public function education() {
        return $this->hasMany('App\Education', 'CANDIDATEID', 'CANDIDATEID');
    }
    public function reference() {
        return $this->hasMany('App\Reference', 'CANDIDATEID', 'CANDIDATEID');
    }
    public function associatejobopenings() {
        return $this->hasMany('App\Jobassociatecandidates', 'CANDIDATEID', 'CANDIDATEID');
    }
}