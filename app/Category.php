<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "faqcategory";

    public function faq(){
        return $this->hasMany('App\Faq','category','id');
    }
}
