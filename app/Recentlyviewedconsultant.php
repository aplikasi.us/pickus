<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recentlyviewedconsultant extends Model
{
    protected $table = "recently_vieweds_consultant";
    
    public function candidate(){
        return $this->belongsTo('App\Candidates','candidate_id','id');
    }
}
