<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Humantech\Zoho\Recruit\Api\Client\Client;
use App\Token;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Notification;
use App\States;
use App\Industry;
use App\Candidates;
use App\Tagline;
use App\User;
use App\Addcompare;
use App\Recentlyviewedconsultant;
use PulkitJalan\GeoIP\GeoIP;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request) {
        // $geoip = new GeoIP();
        // $country = $geoip->getCountry();

        /* For Client Notifications */
        view()->composer(['Layouts.General.doyouknow', 'Layouts.General.tips', 'Layouts.General.rating', 'Layouts.General.header',  'Layouts.General.header_new','Layouts.Client.header', 'Layouts.Client.header_new', 'Layouts.Consultant.header', 'Layouts.Consultant.header_new', 'users::client_home', 'users::client_jobs','users::client_search_job', 'consultant::consultant_home', 'users::client_notifications','users::client_interviews' ,'users::client_notifications','users::client_settings','users::add_to_compare', 'consultant::consultant_notifications', 'backend.settings.notification', 'Layouts.backend.header', 'Layouts.General.footer', 'Layouts.Client.footer', 'Layouts.Client.footer_new', 'Layouts.Consultant.footer', 'Layouts.Consultant.footer_new', 'Layouts.Client.sidebar', 'Layouts.Client.sidebar_new', 'Layouts.Consultant.sidebar', 'Layouts.Consultant.sidebar_new', 'home'], function($view) {
            $doyouknow = Tagline::where('category', 'Facts')->orderBy(\DB::raw('RAND()'))->first();

            $tips = Tagline::where('category', 'Tips')->orderBy(\DB::raw('RAND()'))->first();
            // $rating = Tagline::where('category', 'Rating')->orderBy(\DB::raw('RAND()'))->first();
            $rating = Tagline::where('category', 'Rating')->orderBy('subcategory' ,'ASC')->get();
            if (Auth::check() && Auth::user()->role_id != 3) {
                if (Auth::user()->role_id == 1) {
                    $read = Notification::where('receiver_id', Auth::user()->CONTACTID)->whereNotNull('receiver_id')->where('to', 'Client')->where('is_read', 1)->orderBy('created_at', 'desc')->limit(3)->get();
                    $unread = Notification::where('receiver_id', Auth::user()->CONTACTID)->whereNotNull('receiver_id')->where('to', 'Client')->where('is_read', 0)->orderBy('created_at', 'desc')->limit(3)->get();
                    $notifications = Notification::where('receiver_id', Auth::user()->CONTACTID)->whereNotNull('receiver_id')->where('to', 'Client')->orderBy('created_at', 'desc')->paginate(20);
                    $recentlyViewed = Recentlyviewedconsultant::with('candidate')
                            ->whereHas('candidate', function ($query) {
                                $query->where('is_delete', 0);
                            })
                            ->where('CLIENTID', Auth::user()->zoho_id)
                            ->where('contact_id', Auth::user()->contact_tbl_id)
                            ->orderBy('updated_at', 'desc')
                            ->limit(10)
                            ->get();
                    $addTocompare_id = Addcompare::where([
                                'CLIENTID' => Auth::user()->zoho_id,
                                'contact_id' => Auth::user()->contact_tbl_id
                            ])->pluck('CANDIDATEID')->toArray();
                    $view->with('recentlyViewed', $recentlyViewed);
                    $view->with('addTocompare_id', $addTocompare_id);
                } else {
                    $read = Notification::where('receiver_id', Auth::user()->zoho_id)->where('is_read', 1)->orderBy('created_at', 'desc')->limit(3)->get();
                    $unread = Notification::where('receiver_id', Auth::user()->zoho_id)->where('is_read', 0)->orderBy('created_at', 'desc')->limit(3)->get();
                    $notifications = Notification::where('receiver_id', Auth::user()->zoho_id)->orderBy('created_at', 'desc')->paginate(6);
                }
                $view->with('notifications', $notifications);
                $view->with('unread', $unread);
                $view->with('read', $read);
                $candidate_us = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                $view->with('candidate_us', $candidate_us);
                if (Auth::user()->role_id == 1) {
                    $userType = "Client";
                } else {
                    $userType = "Consultant";
                }
                $doyouknow = Tagline::where('user_type', $userType)->where('category', 'Facts')->orderBy(\DB::raw('RAND()'))->first();
                $tips = Tagline::where('user_type', $userType)->where('category', 'Tips')->orderBy(\DB::raw('RAND()'))->first();
                // $rating = Tagline::where('user_type', $userType)->where('category', 'Rating')->orderBy(\DB::raw('RAND()'))->first();
                // $rating = Tagline::where('user_type', $userType)->where('category', 'Rating')->orderBy(\DB::raw('RAND()'))->get();
            } else if (Auth::check()) {
                if (Auth::user()->user_type == "Territory Admin") {
                    $notifications = Notification::where('to', 'Admin')->whereNotNull('territory')->where('territory', Auth::user()->territory)->orderBy('created_at', 'desc')->paginate(50);
                    $unread = Notification::where('to', 'Admin')->whereNotNull('territory')->where('territory', Auth::user()->territory)->orderBy('created_at', 'desc')->where('is_read', 0)->limit(3)->get();
                } else {
                    $notifications = Notification::where('to', 'Admin')->orderBy('created_at', 'desc')->paginate(50);
                    $unread = Notification::where('to', 'Admin')->orderBy('created_at', 'desc')->where('is_read', 0)->limit(3)->get();
                }
                $view->with('notifications', $notifications);
                $view->with('unread', $unread);
            }

            /* For Filters */
            $categories = ['Basis/S & A' => 'Basis/S & A', 'Functional' => 'Functional', 'Technical' => 'Technical', 'Project Manager/Solution Architect' => 'Project Manager/Solution Architect', 'Others' => 'Others'];
            $states = States::where('country_name',Session::get('territory'))->pluck('state_name', 'state_name')->toArray();
            $baserateArray = ['3000' => '< 3000', '4000' => '< 4000', '5000' => '< 5000', '6000' => '< 6000', '7000' => '< 7000', '7000up' => '> 7000'];
            $avalabilityDateArray = ['week' => 'Within Week', 'month' => 'Within Month', 'three' => 'Within 3 Months', 'six' => 'Within 6 Months', 'year' => 'Within Year'];
            $experienceList = ['0' => 'Fresh', '3' => '1-3 Years', '5' => '4-5 Years', '10' => '5-10 Years', '10plus' => '10+ Years'];
            $workPrefrencesArray = ['fulltime' => 'Full Time', 'parttime' => 'Part Time', 'willing_to_travel' => 'Willing to travel'];
            // $jobTypeList = ['Contract' => 'Contract', 'Permanent' => 'Permanent', 'Internship' => 'Internship', 'Full time' => 'Full time', 'Part time' => 'Part time', 'Any' => 'Any', 'Training' => 'Training', 'Volunteer' => 'Volunteer', 'Freelance' => 'Freelance', 'Seasonal' => 'Seasonal'];
            $jobTypeList = ['Contract' => 'Contract', 'Permanent' => 'Permanent', 'Internship' => 'Internship'];
            $startDateList = ['week' => 'Within Week', 'month' => 'Within Month', 'three' => 'Within 3 Months', 'six' => 'Within 6 Months', 'year' => 'Within Year'];
            $durationList = ["3" => '1-3 months', '6' => "4-6 months", '9' => '6-12 months', '12' => '> 12 months'];
            $industryList = Industry::where('name', '!=', 'None')->pluck('name', 'name')->toArray();
            $jobModeList = ['Full time' => 'Full Time', 'Part time' => 'Part Time'];

            $view->with('categories', $categories);
            $view->with('states', $states);
            $view->with('baserateArray', $baserateArray);
            $view->with('avalabilityDateArray', $avalabilityDateArray);
            $view->with('experienceList', $experienceList);
            $view->with('workPrefrencesArray', $workPrefrencesArray);
            $view->with('jobTypeList', $jobTypeList);
            $view->with('startDateList', $startDateList);
            $view->with('durationList', $durationList);
            $view->with('industryList', $industryList);
            $view->with('jobModeList', $jobModeList);
            $wordpresBlog = [];
            /* ---for wordpress blog-- */
//            $content = file_get_contents("http://206.189.86.153/blog/wp-json/wp/v2/posts?order_by=date");
//            $wordpresBlog = json_decode($content, true);
            $view->with('wordpresBlog', $wordpresBlog);
            $view->with('doyouknow', $doyouknow);
            $view->with('tips', $tips);
            $view->with('rating', $rating);
//            $territory = Session::get('territory');
//            $view->with('territory', $territory);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}