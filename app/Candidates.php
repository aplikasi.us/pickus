<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidates extends Model
{
    protected $table = "candidates";
    
    public function interviews(){
        return $this->hasMany('App\Interviews','candidate_id','id');
    }
    public function attachments(){
        return $this->hasMany('App\Attachment','candidate_id','id');
    }
    public function candidatecategory(){
        return $this->hasMany('App\Candiatecategory','candidate_id','id');
    }
    public function comparison() {
        return $this->hasMany('App\Addcompare', 'candidate_id', 'id');
    }
    public function wishlist() {
        return $this->hasMany('App\Clientwishlist', 'candidate_id', 'id');
    }
    public function profileRequests() {
        return $this->hasMany('App\Candidateprofilerequests', 'CANDIDATEID', 'CANDIDATEID');
    }
}