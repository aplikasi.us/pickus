<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addcompare extends Model
{
    //
    protected $table = "consultant_compare";
    
    public function candidate(){
        return $this->belongsTo('App\Candidates','candidate_id','id');
    }
    public function client(){
        return $this->belongsTo('App\Clients','client_id','id');
    }
}
