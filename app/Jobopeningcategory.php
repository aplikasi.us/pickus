<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobopeningcategory extends Model
{
    protected $table = "jobopening_category";
}