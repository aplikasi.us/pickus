<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobassociatecandidates extends Model
{
    protected $table = "jobassociate_candidates";
    
    public function candidate(){
        return $this->belongsTo('App\Candidates','candidate_id','id');
    }
    public function job(){
        return $this->belongsTo('App\Jobopenings','job_id','id');
    }
    public function consultant(){
		return $this->belongsTo('App\Consultant','CANDIDATEID','CANDIDATEID');
}
}
