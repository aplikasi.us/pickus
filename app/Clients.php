<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = "clients";
    
    public function jobs(){
        return $this->hasMany('App\Jobopenings','client_id','id');
    }
    
    public function interviews(){
        return $this->hasMany('App\Interviews','client_id','id');
    }
    public function contacts(){
        return $this->hasMany('App\Clientcontact','CLIENTID','CLIENTID');
    }
}
