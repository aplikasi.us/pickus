<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobnotes extends Model
{
    protected $table = "jobopening_notes";
}