<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientcontact extends Model
{
    protected $table = "client_contacts";
    
    public function client(){
        return $this->belongsTo('App\Clients','CLIENTID','CLIENTID');
    }
    public function user(){
        return $this->belongsTo('App\User','id','contact_tbl_id');
    }
    public function jobopenings(){
        return $this->hasMany('App\Jobopenings','db_contact_id','id');
    }
}
