<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Token;
use App\User;
use App\Clients;
use App\Jobopenings;
use App\Interviews;
use App\Interviewnote;
use App\Candidates;
use App\Jobassociatecandidates;
use App\Attachment;
use App\Education;
use App\Experience;
use App\Contracthistory;
use App\Reference;
use App\Signeddocument;
use App\Signeddocumentattachment;
use App\Cronlog;
use App\Notification;
use App\Clientcontact;
use App\Candiatecategory;
use App\Settings;
use App\Clientwishlist;
use App\Candidateprofilerequests;
use App\Jobnotes;
use App\Jobopeningcategory;
use App\Addcompare;
use App\Recentlyviewedconsultant;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ZohoqueryController;

class Syncdata extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:zoho';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data to zoho and vice versa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $zohoquery;

    public function __construct()
    {
        parent::__construct();
        $this->zohoquery = new ZohoqueryController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "------ Cron is Running... -----\n";
        echo "------ Sending Candidates -----\n";
        $this->sendCandidates("MY");
        $this->sendCandidates("PH");
        echo "------ Receiving Candidates -----\n";
        $this->receiveCandidates("MY");
        $this->receiveCandidates("PH");
        echo "------ Receiving Candidate Signed Attachments -----\n";
        $this->receiveCandidateSignedAttachment("MY");
        $this->receiveCandidateSignedAttachment("PH");
        echo "------ Receiving Candidate Profile Requests -----\n";
        $this->receiveCandidateProfileRequests("MY");
        $this->receiveCandidateProfileRequests("PH");
        echo "------ Sending Clients -----\n";
        $this->sendClients("MY");
        $this->sendClients("PH");
        echo "------ Receiving Clients -----\n";
        $this->receiveClients("MY");
        $this->receiveClients("PH");
        echo "------ Sending Contacts -----\n";
        $this->sendContacts("MY");
        $this->sendContacts("PH");
        echo "------ Receiving Contacts -----\n";
        $this->receiveContacts("MY");
        $this->receiveContacts("PH");
        echo "------ Sending Job Openings -----\n";
        $this->sendJobOpenings("MY");
        $this->sendJobOpenings("PH");
        echo "------ Receiving Job Openings -----\n";
        $this->receiveJobOpenings("MY");
        $this->receiveJobOpenings("PH");
        echo "------ Receiving Interviews -----\n";
        $this->receiveInterviews("MY");
        $this->receiveInterviews("PH");
        echo "------Cron Runs Successfully!!-----\n";
    }

    /**
     * Syncing clients data
     * 
     */
    public function sendClients($territory = "MY")
    {
        $clients = Clients::where([
            //    'CLIENTID' => '401682000003197023', // Remove this line
            'territories' => $territory,
            'is_synced' => 0
        ])->get();
        if (count($clients)) {
            $result = Token::where('territory', $territory)->first();
            $this->zohoquery->token = $result->token;
            foreach ($clients as $key => $value) {
                $data = [];
                $data['Client Name'] = (isset($value->client_name)) ? $value->client_name : "";
                $response = $this->zohoquery->updateClient($value->CLIENTID, $data);
                if ($response['status'] == "500") { // Record deleted from zoho
                    /** Deleting all it's dependency */
                    User::where('zoho_id', $value->CLIENTID)->delete();
                    Clients::where('CLIENTID', $value->CLIENTID)->delete();
                    Clientcontact::where('CLIENTID', $value->CLIENTID)->delete();
                    Clientwishlist::where('CLIENTID', $value->CLIENTID)->delete();
                    Addcompare::where('CLIENTID', $value->CLIENTID)->delete();
                    $interviews = Interviews::where('CLIENTID', $value->CLIENTID)->get();
                    if ($interviews != null) {
                        foreach ($interviews as $intKey => $intValue) {
                            Interviewnote::where('INTERVIEWID', $intValue->INTERVIEWID)->delete();
                            $intValue->delete();
                        }
                    }
                    Recentlyviewedconsultant::where('CLIENTID', $value->CLIENTID)->delete();
                    $jobOpenings = Jobopenings::where('CLIENTID', $value->CLIENTID)->get();
                    if ($jobOpenings != null) {
                        foreach ($jobOpenings as $jobKey => $jobValue) {
                            Jobopeningcategory::where('job_id', $jobValue->id)->delete();
                            Jobassociatecandidates::where('job_id', $jobValue->id)->delete();
                            Jobnotes::where('job_id', $jobValue->id)->delete();
                            $jobValue->delete();
                        }
                    }
                } else if ($response['status'] == "401") {
                    echo '\n' . $response['message'];
                    exit;
                } else {
                    $value->is_synced = 1;
                    $value->save();
                }
            }
        }
        return;
    }

    public function receiveClients($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;

        $data = $this->zohoquery->getClientRecords();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    if (isset($value['Territories'])) {
                        $isClientExists = Clients::where('CLIENTID', $value['CLIENTID'])->first();
                        if ($isClientExists != null) { // Update
                            $client = $isClientExists;
                        } else { // Add
                            $client = new Clients();
                        }
                        $client->CLIENTID = (isset($value['CLIENTID'])) ? $value['CLIENTID'] : "";
                        $client->client_name = (isset($value['Client Name'])) ? $value['Client Name'] : "";
                        $client->parent_client = (isset($value['Parent Client'])) ? $value['Parent Client'] : "";
                        $client->PARENTCLIENTID = (isset($value['PARENTCLIENTID'])) ? $value['PARENTCLIENTID'] : "";
                        $client->contact_number = (isset($value['Contact Number'])) ? $value['Contact Number'] : "";
                        $client->fax = (isset($value['Fax'])) ? $value['Fax'] : "";
                        $client->account_manager = (isset($value['Account Manager'])) ? $value['Account Manager'] : "";
                        $client->website = (isset($value['Website'])) ? $value['Website'] : "";
                        $client->industry = (isset($value['Industry'])) ? $value['Industry'] : "";
                        $client->about = (isset($value['About'])) ? $value['About'] : "";
                        $client->revenue_type = (isset($value['Revenue Type'])) ? $value['Revenue Type'] : "";
                        $client->currency = (isset($value['Currency'])) ? $value['Currency'] : "";
                        $client->territories = (isset($value['Territories'])) ? $value['Territories'] : "";
                        $client->last_activity_time = (isset($value['Last Activity Time'])) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : "";
                        $client->billing_street = (isset($value['Billing Street'])) ? $value['Billing Street'] : "";
                        $client->billing_city = (isset($value['Billing City'])) ? $value['Billing City'] : "";
                        $client->billing_state = (isset($value['Billing State'])) ? $value['Billing State'] : "";
                        $client->billing_code = (isset($value['Billing Code'])) ? $value['Billing Code'] : "";
                        $client->billing_country = (isset($value['Billing Country'])) ? $value['Billing Country'] : "";
                        $client->shipping_street = (isset($value['Shipping Street'])) ? $value['Shipping Street'] : "";
                        $client->shipping_city = (isset($value['Shipping City'])) ? $value['Shipping City'] : "";
                        $client->shipping_state = (isset($value['Shipping State'])) ? $value['Shipping State'] : "";
                        $client->shipping_code = (isset($value['Shipping Code'])) ? $value['Shipping Code'] : "";
                        $client->shipping_country = (isset($value['Shipping Country'])) ? $value['Shipping Country'] : "";
                        $client->created_by_id = (isset($value['SMCREATORID'])) ? $value['SMCREATORID'] : "";
                        $client->created_by = (isset($value['Created By'])) ? $value['Created By'] : "";
                        $client->modified_by_id = (isset($value['MODIFIEDBY'])) ? $value['MODIFIEDBY'] : "";
                        $client->modified_by = (isset($value['Modified By'])) ? $value['Modified By'] : "";
                        $client->source = (isset($value['Source'])) ? $value['Source'] : "";
                        $client->others = (isset($value['Others'])) ? $value['Others'] : "";
                        $client->created_at = date("Y-m-d H:i:s", time());
                        $client->updated_at = date("Y-m-d H:i:s", time());
                        $client->save();
                        /* Add/Update in users table */
                        $contactsEntry = User::where('zoho_id', $value['CLIENTID'])->get();
                        if (count($contactsEntry)) {
                            foreach ($contactsEntry as $contactKey => $contactValue) {
                                $contactValue->company_name = isset($value['Client Name']) ? $value['Client Name'] : "";
                                $contactValue->save();
                            }
                        }
                    }
                }
            }
            $cronLog = Cronlog::where('module_name', 'clients')->first();
            $cronLog->last_run = date('Y-m-d H:i:s', time());
            $cronLog->save();
        }
        return;
    }

    /**
     * Syncing client contacts data
     * 
     */
    public function sendContacts($territory = "MY")
    {
        $contacts = Clientcontact::where([
            // 'CLIENTID' => '401682000003197023', // Remove this line
            'territory' => $territory,
            'is_synced' => 0
        ])->get();
        if (count($contacts)) {
            $result = Token::where('territory', $territory)->first();
            $this->zohoquery->token = $result->token;
            foreach ($contacts as $key => $value) {
                $data = [];
                $data['CONTACTID'] = (isset($value->CONTACTID)) ? $value->CONTACTID : "";
                $data['First Name'] = (isset($value->first_name)) ? $value->first_name : "";
                $data['Last Name'] = (isset($value->last_name)) ? $value->last_name : "";
                $data['Job Title'] = (isset($value->job_title)) ? $value->job_title : "";
                $data['Department'] = (isset($value->department)) ? $value->department : "";
                $data['Mobile'] = (isset($value->mobile)) ? $value->mobile : "";
                $data['Mailing City'] = (isset($value->mailing_city)) ? $value->mailing_city : "";

                $response = $this->zohoquery->updateClientContact($value->CONTACTID, $data);
                if ($represponseonse['status'] == "500") {
                    /** Deleting all it's dependency */
                    User::where('CONTACTID', $value->CONTACTID)->delete();
                    Clientcontact::where('CONTACTID', $value->CONTACTID)->delete();
                    Clientwishlist::where('CONTACTID', $value->CONTACTID)->delete();
                    Addcompare::where('CONTACTID', $value->CONTACTID)->delete();
                    $interviews = Interviews::where('CLIENTID', $value->CONTACTID)->get();
                    if ($interviews != null) {
                        foreach ($interviews as $intKey => $intValue) {
                            Interviewnote::where('INTERVIEWID', $intValue->INTERVIEWID)->delete();
                            $intValue->delete();
                        }
                    }
                    Recentlyviewedconsultant::where('CONTACTID', $value->CONTACTID)->delete();
                    $jobOpenings = Jobopenings::where('CONTACTID', $value->CONTACTID)->get();
                    if ($jobOpenings != null) {
                        foreach ($jobOpenings as $jobKey => $jobValue) {
                            Jobopeningcategory::where('job_id', $jobValue->id)->delete();
                            Jobassociatecandidates::where('job_id', $jobValue->id)->delete();
                            Jobnotes::where('job_id', $jobValue->id)->delete();
                            $jobValue->delete();
                        }
                    }
                } else if ($response['status'] == "401") {
                    echo '\n' . $response['message'];
                    exit;
                } else {
                    $value->is_synced = 1;
                    $value->save();
                }
            }
        }
        return;
    }

    public function receiveContacts($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;
        $data = $this->zohoquery->getClientContacts(1, 20, 0, array());
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    if (isset($value['Territory'])) {
                        $isContactExists = Clientcontact::where('CONTACTID', $value['CONTACTID'])->first();
                        if ($isContactExists != null) { // Update
                            $contact = $isContactExists;
                        } else { // Add
                            $contact = new Clientcontact();
                        }
                        $contact->CONTACTID = $value['CONTACTID'];
                        $contact->first_name = isset($value['First Name']) ? $value['First Name'] : "";
                        $contact->salutation = isset($value['Salutation']) ? $value['Salutation'] : "";
                        $contact->last_name = isset($value['Last Name']) ? $value['Last Name'] : "";
                        $contact->department = isset($value['Department']) ? $value['Department'] : "";
                        $contact->CLIENTID = isset($value['CLIENTID']) ? $value['CLIENTID'] : "";
                        $contact->client_name = isset($value['Client Name']) ? $value['Client Name'] : "";
                        $contact->work_phone = isset($value['Work Phone']) ? $value['Work Phone'] : "";
                        $contact->job_title = isset($value['Job Title']) ? $value['Job Title'] : "";
                        $contact->fax = isset($value['Fax']) ? $value['Fax'] : "";
                        $contact->email = isset($value['Email']) ? $value['Email'] : "";
                        $contact->skype_id = isset($value['Skype ID']) ? $value['Skype ID'] : "";
                        $contact->mobile = isset($value['Mobile']) ? $value['Mobile'] : "";
                        $contact->is_primary_contact = isset($value['Is primary contact']) ? 1 : 0;
                        $contact->twitter = isset($value['Twitter']) ? $value['Twitter'] : "";
                        $contact->currency = isset($value['Currency']) ? $value['Currency'] : "";
                        $contact->email_opt_out = isset($value['Email Opt Out']) ? 1 : 0;
                        $contact->territory = isset($value['Territory']) ? $value['Territory'] : "";
                        $contact->secondary_email = isset($value['Secondary Email']) ? $value['Secondary Email'] : "";
                        $contact->how_you_know_about_us = isset($value['How You Know About Us']) ? $value['How You Know About Us'] : "";
                        $contact->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format("Y-m-d H:i:s") : "";
                        $contact->territories = isset($value['Territories']) ? $value['Territories'] : "";
                        $contact->mailing_street = isset($value['Mailing Street']) ? $value['Mailing Street'] : "";
                        $contact->other_street = isset($value['Other Street']) ? $value['Other Street'] : "";
                        $contact->mailing_city = isset($value['Mailing City']) ? $value['Mailing City'] : "";
                        $contact->other_city = isset($value['Other City']) ? $value['Other City'] : "";
                        $contact->mailing_state = isset($value['Mailing State']) ? $value['Mailing State'] : "";
                        $contact->other_state = isset($value['Other State']) ? $value['Other State'] : "";
                        $contact->mailing_zip = isset($value['Mailing Zip']) ? $value['Mailing Zip'] : "";
                        $contact->other_zip = isset($value['Other Zip']) ? $value['Other Zip'] : "";
                        $contact->mailing_country = isset($value['Mailing Country']) ? $value['Mailing Country'] : "";
                        $contact->other_country = isset($value['Other Country']) ? $value['Other Country'] : "";
                        $contact->SMOWNERID = isset($value['SMOWNERID']) ? $value['SMOWNERID'] : "";
                        $contact->contact_owner = isset($value['Contact Owner']) ? $value['Contact Owner'] : "";
                        $contact->source = isset($value['Source']) ? $value['Source'] : "";
                        $contact->SMCREATORID = isset($value['SMCREATORID']) ? $value['SMCREATORID'] : "";
                        $contact->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                        $contact->MODIFIEDBY = isset($value['MODIFIEDBY']) ? $value['MODIFIEDBY'] : "";
                        $contact->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                        $contact->category = isset($value['Category']) ? $value['Category'] : "";
                        $contact->subject = isset($value['Subject']) ? $value['Subject'] : "";
                        $contact->message = isset($value['Message']) ? $value['Message'] : "";
                        $contact->others = isset($value['Others']) ? $value['Others'] : "";
                        $contact->created_at = date("Y-m-d H:i:s", time());
                        $contact->updated_at = date("Y-m-d H:i:s", time());
                        $contact->save();
                        if (isset($value['CLIENTID'])) {
                            $isUserExists = User::where('zoho_id', $value['CLIENTID'])->where('CONTACTID', $value['CONTACTID'])->first();
                            if ($isUserExists != null) { // Update
                                $user = $isUserExists;
                            } else { // Add
                                $user = new User();
                            }
                            $user->role_id = 1;
                            $user->zoho_id = $value['CLIENTID'];
                            $user->CONTACTID = $value['CONTACTID'];
                            $user->contact_tbl_id = $contact->id;
                            $user->first_name = isset($value['First Name']) ? $value['First Name'] : "";
                            $user->last_name = isset($value['Last Name']) ? $value['Last Name'] : "";
                            $user->sap_job_title = isset($value['Job Title']) ? $value['Job Title'] : "";
                            $user->hear = isset($value['How You Know About Us']) ? $value['How You Know About Us'] : "";
                            $user->email = isset($value['Email']) ? $value['Email'] : "";
                            $user->phone = isset($value['Mobile']) ? $value['Mobile'] : "";
                            //                    $user->territory = isset($value['Territory']) ? $value['Territory'] : "";
                            $user->created_at = date("Y-m-d H:i:s", time());
                            $user->updated_at = date("Y-m-d H:i:s", time());
                            $user->save();
                        }
                    }
                }
            }
            $cronLog = Cronlog::where('module_name', 'contacts')->first();
            $cronLog->last_run = date('Y-m-d H:i:s', time());
            $cronLog->save();
        }
        return;
    }

    /**
     * Syncing job openings data
     * 
     */
    public function sendJobOpenings($territory = "MY")
    {
        $jobs = Jobopenings::with('client')
            ->where([
                // 'CLIENTID' => '401682000002786001', // Remove this line
                'territory' => $territory,
                'is_synced' => 0,
                'is_approve_by_admin' => 1,
                'is_delete' => 0
            ])->get();
        if (count($jobs)) {
            $result = Token::where('territory', $territory)->first();
            $this->zohoquery->token = $result->token;
            foreach ($jobs as $key => $value) {
                $data = [];
                if ($value->contact_name != null) {
                    $data['Contact Name'] = $value->contact_name;
                }
                if (isset($value->client)) {
                    $data['Client Name'] = $value->client->client_name;
                }
                $data['CLIENTID'] = $value->CLIENTID;
                $data['Posting Title'] = isset($value->posting_title) ? $value->posting_title : "";
                $data['Job Type'] = isset($value->job_type) ? $value->job_type : "";
                $data['Job Mode'] = isset($value->job_mode) ? $value->job_mode : "";
                $data['Date Opened'] = isset($value->date_opened) ? $value->date_opened : null;
                $data['City'] = isset($value->city) ? $value->city : "";
                $data['Job Duration (months)'] = isset($value->job_duration) ? $value->job_duration : "";
                $data['State'] = isset($value->state) ? $value->state : "";
                $data['Industry'] = isset($value->industry) ? $value->industry : "";
                $data['Job Base Rate'] = isset($value->job_base_rate) ? $value->job_base_rate : "";
                $data['Work Experience'] = isset($value->work_experience) ? $value->work_experience : "";
                $data['Key Skills'] = isset($value->key_skills) ? $value->key_skills : "";
                $data['Job Description'] = htmlspecialchars($value->all_description, ENT_COMPAT);
                $data['Publish in Us'] = ($value->publish_in_us == 1) ? "true" : "false";
                $data['Category'] = isset($value->category) ? htmlentities(str_replace(",", ";", $value->category)) : "";
                $data['Actual Revenue'] = isset($value->actual_revenue) ? $value->actual_revenue : "";
                $data['Account Manager'] = isset($value->account_manager) ? $value->account_manager : "";
                $data['Target Date'] = isset($value->target_date) ? $value->target_date : "";
                $data['Territory'] = isset($value->territory) ? $value->territory : "MY";
                if ($value->JOBOPENINGID != null) { // Update Job
                    $response = $this->zohoquery->updateJob($value->JOBOPENINGID, $data);

                    if ($response['status'] == "500") {
                        /** Deleting all it's dependency */
                        $jobOpenings = Jobopenings::where('JOBOPENINGID', $value->JOBOPENINGID)->first();
                        if ($jobOpenings != null) {
                            Jobopeningcategory::where('job_id', $jobOpenings->id)->delete();
                            Jobassociatecandidates::where('job_id', $jobOpenings->id)->delete();
                            Jobnotes::where('job_id', $jobOpenings->id)->delete();
                            $jobOpenings->delete();
                        }
                        $interviews = Interviews::where('JOBOPENINGID', $value->JOBOPENINGID)->get();
                        if ($interviews != null) {
                            foreach ($interviews as $intKey => $intValue) {
                                Interviewnote::where('INTERVIEWID', $intValue->INTERVIEWID)->delete();
                                $intValue->delete();
                            }
                        }
                    } else if ($response['status'] == "401") {
                        echo '\n' . $response['message'];
                        exit;
                    } else {
                        $value->is_synced = 1;
                        $value->save();
                    }
                } else { // Add Job
                    $response = $this->zohoquery->addJob($data);
                    if (count($response)) {
                        $value->JOBOPENINGID = $response[0]['JOBOPENINGID'];
                        $value->Job_ID = $response[0]['Job Opening ID'];
                        /* Updating JOBOPENINGID in job associated candidates */
                        DB::table('jobassociate_candidates')->where('job_id', $value->id)->update([
                            'JOBOPENINGID' => $response[0]['JOBOPENINGID']
                        ]);
                    }
                    $value->is_synced = 1;
                    $value->save();
                }
            }
        }
        return;
    }

    public function receiveJobOpenings($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;
        $data = $this->zohoquery->getJobOpenings(1, 20, 0, array());
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    if (isset($value['Territory'])) {
                        $isJobExists = Jobopenings::where('JOBOPENINGID', $value['JOBOPENINGID'])->first();
                        if ($isJobExists != null) {
                            $jobOpening = $isJobExists;
                        } else {
                            $jobOpening = new Jobopenings();
                        }
                        if (isset($value['Contact Name'])) {
                            $contact = Clientcontact::where(DB::raw("CONCAT(first_name, ' ', last_name)"), "LIKE", '%' . $value['Contact Name'] . '%')->first();
                            if ($contact != null) {
                                $jobOpening->db_contact_id = $contact->id;
                                $jobOpening->CONTACTID = $contact->CONTACTID;
                            }
                        }
                        $jobOpening->JOBOPENINGID = $value['JOBOPENINGID'];
                        $client = Clients::where('CLIENTID', $value['CLIENTID'])->first();
                        if ($client != null) {
                            $jobOpening->client_id = $client->id;
                        }
                        $jobOpening->CLIENTID = (isset($value['CLIENTID'])) ? $value['CLIENTID'] : "";
                        $jobOpening->Job_ID = (isset($value['Job Opening ID'])) ? $value['Job Opening ID'] : "";
                        $jobOpening->posting_title = isset($value['Posting Title']) ? $value['Posting Title'] : "";
                        $jobOpening->job_type = isset($value['Job Type']) ? $value['Job Type'] : "";
                        $jobOpening->job_mode = isset($value['Job Mode']) ? $value['Job Mode'] : "";
                        $jobOpening->date_opened = isset($value['Date Opened']) ? $value['Date Opened']->format('Y-m-d H:i:s') : null;
                        $jobOpening->contact_name = isset($value['Contact Name']) ? $value['Contact Name'] : "";
                        $jobOpening->city = isset($value['City']) ? $value['City'] : "";
                        $jobOpening->target_date = isset($value['Target Date']) ? $value['Target Date']->format('Y-m-d') : null;
                        $jobOpening->job_duration = isset($value['Job Duration (months)']) ? $value['Job Duration (months)'] : "";
                        $jobOpening->zipcode = isset($value['Zip Code']) ? $value['Zip Code'] : "";
                        $jobOpening->state = isset($value['State']) ? $value['State'] : "";
                        $jobOpening->country = isset($value['Country']) ? $value['Country'] : "";
                        $jobOpening->salary = isset($value['Salary']) ? $value['Salary'] : "";
                        $jobOpening->industry = isset($value['Industry']) ? $value['Industry'] : "";
                        $jobOpening->job_base_rate = isset($value['Job Base Rate']) ? $value['Job Base Rate'] : "";
                        $jobOpening->work_experience = isset($value['Work Experience']) ? $value['Work Experience'] : "";
                        $jobOpening->key_skills = isset($value['Key Skills']) ? $value['Key Skills'] : "";
                        $jobOpening->recruiter_id = isset($value['RECRUITERID']) ? $value['RECRUITERID'] : "";
                        $jobOpening->assigned_recruiter = isset($value['Assigned Recruiter']) ? $value['Assigned Recruiter'] : "";
                        $jobOpening->job_description = isset($value['Job Description']) ? html_entity_decode($value['Job Description']) : "";
                        $jobOpening->internal_hire = isset($value['Internal Hire']) ? $value['Internal Hire'] : "";
                        $jobOpening->job_requirements = isset($value['Job Requirements']) ? html_entity_decode($value['Job Requirements']) : "";
                        $jobOpening->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : "";
                        $jobOpening->job_benefits = isset($value['Job Benefits']) ? html_entity_decode($value['Job Benefits']) : "";
                        $jobOpening->category = isset($value['Category']) ? str_replace(";", ",", $value['Category']) : "";
                        $jobOpening->currency = isset($value['Currency']) ? $value['Currency'] : "";
                        $jobOpening->territory = isset($value['Territory']) ? $value['Territory'] : "";
                        $jobOpening->date_closed = isset($value['Date Closed']) ? $value['Date Closed'] : null;
                        $jobOpening->publish_in_us = (isset($value['Publish in Us']) && $value['Publish in Us'] != null) ? 1 : 0;
                        $jobOpening->job_opening_status = isset($value['Job Opening Status']) ? $value['Job Opening Status'] : "";
                        $jobOpening->account_manager_id = isset($value['SMOWNERID']) ? $value['SMOWNERID'] : "";
                        $jobOpening->account_manager = isset($value['Account Manager']) ? $value['Account Manager'] : "";
                        $jobOpening->created_by_id = isset($value['SMCREATORID']) ? $value['SMCREATORID'] : "";
                        $jobOpening->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                        $jobOpening->modified_by_id = isset($value['MODIFIEDBY']) ? $value['MODIFIEDBY'] : "";
                        $jobOpening->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                        $jobOpening->revenue_stream = isset($value['Revenue stream']) ? $value['Revenue stream'] : "";
                        $jobOpening->no_of_position = isset($value['Number of Positions']) ? $value['Number of Positions'] : "";
                        $jobOpening->revenue_per_position = isset($value['Revenue per Position']) ? $value['Revenue per Position'] : "";
                        $jobOpening->expected_revenue = isset($value['Expected Revenue']) ? $value['Expected Revenue'] : "";
                        $jobOpening->actual_revenue = isset($value['Actual Revenue']) ? $value['Actual Revenue'] : "";
                        $jobOpening->missed_revenue = isset($value['Missed Revenue']) ? $value['Missed Revenue'] : "";
                        $jobOpening->is_approve_by_admin = 1;
                        $jobOpening->created_at = date('Y-m-d H:i:s', time());
                        $jobOpening->updated_at = date('Y-m-d H:i:s', time());
                        $jobOpening->save();

                        /* Removing old categories and adding new */
                        Jobopeningcategory::where([
                            'job_id' => $jobOpening->id
                        ])->delete();
                        if ($jobOpening->category != "" || $jobOpening->category != null) {
                            $catsArray = explode(',', $jobOpening->category);
                            if (!empty($catsArray)) {
                                foreach ($catsArray as $k => $v) {
                                    if ($v != "") {
                                        $category = new Jobopeningcategory();
                                        $category->job_id = $jobOpening->id;
                                        $category->category = $v;
                                        $category->created_at = date('Y-m-d H:i:s', time());
                                        $category->updated_at = date('Y-m-d H:i:s', time());
                                        $category->save();
                                    }
                                }
                            }
                        }

                        /* Save Job Associate Candidates */
                        $this->receiveJobAssociateCandidates($jobOpening->id, $value['JOBOPENINGID']);
                        /* Save job notes */
                        $this->receiveJobNotes($jobOpening->id, $jobOpening->JOBOPENINGID);
                    }
                }
            }
            $cronLog = Cronlog::where('module_name', 'job_openings')->first();
            $cronLog->last_run = date('Y-m-d H:i:s', time());
            $cronLog->save();
        }
        return;
    }

    public function receiveJobAssociateCandidates($jobId, $JOBOPENINGID)
    {
        $data = $this->zohoquery->getJobAssociateCandidates($JOBOPENINGID);
        if (count($data)) {
            foreach ($data as $key => $value) {
                $isAssociatedCandidateExists = Jobassociatecandidates::where([
                    'job_id' => $jobId,
                    'CANDIDATEID' => $value['CANDIDATEID']
                ])->first();
                if ($isAssociatedCandidateExists != null) {
                    $candidate = $isAssociatedCandidateExists;
                } else {
                    $candidate = new Jobassociatecandidates();
                }
                $candidate->job_id = $jobId;
                $candidate->JOBOPENINGID = $JOBOPENINGID;
                $candidate->CANDIDATEID = $value['CANDIDATEID'];
                $candidate->status = $value['STATUS'];
                $candidate->created_at = date('Y-m-d H:i:s', time());
                $candidate->updated_at = date('Y-m-d H:i:s', time());
                $candidate->save();
            }
        }
        return;
    }

    public function receiveJobNotes($jobId, $JOBOPENINGID)
    {
        $data = $this->zohoquery->getJobNotes($JOBOPENINGID);
        if (count($data)) {
            foreach ($data as $key => $value) {
                $isNoteExists = Jobnotes::where('NOTEID', $value['id'])->first();
                if ($isNoteExists != null) {
                    $note = $isNoteExists;
                } else {
                    $note = new Jobnotes();
                }
                $note->NOTEID = $value['id'];
                $note->JOBOPENINGID = $JOBOPENINGID;
                $note->job_id = $jobId;
                $note->title = $value['Title'];
                $note->note_content = isset($value['Note Content']) ? $value['Note Content'] : "";
                $note->sm_owner_id = isset($value['SMOWNERID']) ? $value['SMOWNERID'] : "";
                $note->owner_name = isset($value['Owner Name']) ? $value['Owner Name'] : "";
                $note->sm_creator_id = isset($value['SMCREATORID']) ? $value['SMCREATORID'] : "";
                $note->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                $note->created_time = isset($value['Created Time']) ? $value['Created Time']->format('Y-m-d H:i:s') : "";
                $note->modify_by_id = isset($value['MODIFIEDBY']) ? $value['MODIFIEDBY'] : "";
                $note->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                $note->modified_time = isset($value['Modified Time']) ? $value['Modified Time']->format('Y-m-d H:i:s') : "";
                $note->created_at = date("Y-m-d H:i:s", time());
                $note->updated_at = date("Y-m-d H:i:s", time());
                $note->save();
            }
        }
        return;
    }

    /**
     * Syncing candidates data
     * 
     */
    public function sendCandidates($territory = "MY")
    {
        $candidates = Candidates::where([
            // 'CANDIDATEID' => '401682000002785194', // Remove this line
            'territory' => $territory,
            'is_synced' => 0,
            'is_delete' => 0
        ])->get();
        if (count($candidates)) {
            $result = Token::where('territory', $territory)->first();
            $this->zohoquery->token = $result->token;
            foreach ($candidates as $key => $value) {
                $data = [];
                $data['First Name'] = isset($value->first_name) ? $value->first_name : "";
                $data['Last Name'] = isset($value->last_name) ? $value->last_name : "";
                $data['Mobile'] = isset($value->mobile_number) ? $value->mobile_number : "";
                $data['SAP Job Title'] = isset($value->sap_job_title) ? $value->sap_job_title : "";
                $data['Experience in Years'] = isset($value->experience_in_years) ? $value->experience_in_years : 0;
                $data['Notice Period (Days)'] = isset($value->notice_period_days) ? $value->notice_period_days : 0;
                $data['Base Rate (BR)'] = isset($value->base_rate) ? $value->base_rate : "";
                $data['Reserved Base Rate'] = isset($value->reserved_base_rate) ? $value->reserved_base_rate : "";
                $data['Skill Set'] = isset($value->skill_set) ? $value->skill_set : "";
                $data['Certifications & Trainings'] = isset($value->certification_and_training) ? strip_tags($value->certification_and_training) : "";
                $data['Additional Info'] = isset($value->additional_info) ? htmlentities($value->additional_info) : "";
                $data['Category'] = isset($value->category) ? htmlentities(str_replace(",", ";", $value->category)) : "";
                $data['Availability Date'] = isset($value->availability_date) ? $value->availability_date : null;
                $data['Willing to travel'] = ($value->willing_to_travel == 1) ? "true" : "false";
                $data['Full-time'] = ($value->full_time == 1) ? "true" : "false";
                $data['Part-time'] = ($value->part_time == 1) ? "true" : "false";
                $data['Project'] = ($value->project == 1) ? "true" : "false";
                $data['Support'] = ($value->support == 1) ? "true" : "false";
                $data['State'] = isset($value->state) ? $value->state : "";
                $data['Country'] = isset($value->country) ? $value->country : "";
                $data['City'] = isset($value->city) ? $value->city : "";
                $data['Territory'] = isset($value->territory) ? $value->territory : "MY";

                $response = $this->zohoquery->updateCandidate($value->CANDIDATEID, $data);
                if ($response['status'] == 500) {  // Record deleted from zoho
                    /** Deleting all it's dependency */
                    User::where('zoho_id', $value->CANDIDATEID)->delete();
                    Candidates::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Attachment::where('zoho_id', $value->CANDIDATEID)->delete();
                    Candiatecategory::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Contracthistory::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Education::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Experience::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Candidateprofilerequests::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Reference::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Clientwishlist::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Addcompare::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    $interviews = Interviews::where('CANDIDATEID', $value->CANDIDATEID)->get();
                    if ($interviews != null) {
                        foreach ($interviews as $intKey => $intValue) {
                            Interviewnote::where('INTERVIEWID', $intValue->INTERVIEWID)->delete();
                            $intValue->delete();
                        }
                    }
                    Jobassociatecandidates::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    Recentlyviewedconsultant::where('CANDIDATEID', $value->CANDIDATEID)->delete();
                    $signedDocument = Signeddocument::where('CANDIDATEID', $value->CANDIDATEID)->get();
                    if ($signedDocument != null) {
                        foreach ($signedDocument as $docKey => $docValue) {
                            Signeddocumentattachment::where('signed_document_id', $docValue->id)->delete();
                            $docValue->delete();
                        }
                    }
                } else if ($response['status'] == "401") {
                    echo '\n' . $response['message'];
                    exit;
                } else {
                    $value->is_synced = 1;
                    $value->save();
                }
            }
        }
        return;
    }

    public function receiveCandidates($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;
        $data = $this->zohoquery->getCandidateData();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    if (isset($value['Territory'])) {
                        $isCandidateExists = Candidates::where('CANDIDATEID', $value['CANDIDATEID'])->first();
                        if ($isCandidateExists != null) {
                            $candidate = $isCandidateExists;
                        } else {
                            $candidate = new Candidates();
                        }
                        $candidate->candidate_id = isset($value['Candidate ID']) ? $value['Candidate ID'] : "";
                        $candidate->first_name = isset($value['First Name']) ? $value['First Name'] : "";
                        $candidate->last_name = isset($value['Last Name']) ? $value['Last Name'] : "";
                        $candidate->salutation = isset($value['Salutation']) ? $value['Salutation'] : "";
                        $candidate->email = isset($value['Email']) ? $value['Email'] : "";
                        $candidate->skype_id = isset($value['Skype ID']) ? $value['Skype ID'] : "";
                        $candidate->mobile_number = isset($value['Mobile']) ? $value['Mobile'] : "";
                        $candidate->twitter = isset($value['Twitter']) ? $value['Twitter'] : "";
                        $candidate->website = isset($value['Website']) ? $value['Website'] : "";
                        $candidate->internal_hire = isset($value['Internal Hire']) ? $value['Internal Hire'] : 0;
                        $candidate->CANDIDATEID = isset($value['CANDIDATEID']) ? $value['CANDIDATEID'] : "";
                        $candidate->publish_in_us = (isset($value['Publish in Us']) && $value['Publish in Us'] != null) ? 1 : 0;
                        $candidate->secondary_email = isset($value['Secondary Email']) ? $value['Secondary Email'] : "";
                        $candidate->block = isset($value['Block']) ? $value['Block'] : 0;
                        $candidate->nationality = isset($value['Nationality']) ? $value['Nationality'] : "";
                        $candidate->currency = isset($value['Currency']) ? $value['Currency'] : "";
                        $candidate->territory = isset($value['Territory']) ? $value['Territory'] : "";
                        $candidate->sap_job_title = isset($value['SAP Job Title']) ? $value['SAP Job Title'] : "";
                        $candidate->highest_qualification_held = isset($value['Highest Qualification Held']) ? $value['Highest Qualification Held'] : "";
                        $candidate->experience_in_years = isset($value['Experience in Years']) ? $value['Experience in Years'] : 0;
                        $candidate->current_employer = isset($value['Current Employer']) ? $value['Current Employer'] : "";
                        $candidate->notice_period_days = isset($value['Notice Period (Days)']) ? $value['Notice Period (Days)'] : 0;
                        $candidate->expected_salary = isset($value['Expected Salary']) ? $value['Expected Salary'] : "";
                        $candidate->employment_type = isset($value['Employment Type']) ? $value['Employment Type'] : "";
                        $candidate->base_rate = isset($value['Base Rate (BR)']) ? $value['Base Rate (BR)'] : "";
                        $candidate->current_salary = isset($value['Current Salary']) ? $value['Current Salary'] : "";
                        $candidate->reserved_base_rate = isset($value['Reserved Base Rate']) ? $value['Reserved Base Rate'] : "";
                        $candidate->skill_set = isset($value['Skill Set']) ? $value['Skill Set'] : "";
                        $candidate->language = isset($value['Language']) ? $value['Language'] : "";
                        $candidate->certification_and_training = isset($value['Certifications & Trainings']) ? $value['Certifications & Trainings'] : "";
                        $candidate->additional_info = isset($value['Additional Info']) ? $value['Additional Info'] : "";
                        $candidate->category = isset($value['Category']) ? str_replace(";", ",", $value['Category']) : "";
                        $candidate->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : null;
                        $candidate->available_for_contract = isset($value['Available for Contract']) ? $value['Available for Contract'] : 0;
                        $candidate->availability_date = isset($value['Availability Date']) ? $value['Availability Date']->format('Y-m-d') : null;
                        $candidate->willing_to_travel = isset($value['Willing to travel']) ? $value['Willing to travel'] : 0;
                        $candidate->full_time = isset($value['Full-time']) ? $value['Full-time'] : 0;
                        $candidate->part_time = isset($value['Part-time']) ? $value['Part-time'] : 0;
                        $candidate->project = isset($value['Project']) ? $value['Project'] : 0;
                        $candidate->support = isset($value['Support']) ? $value['Support'] : 0;
                        $candidate->client = isset($value['Client']) ? $value['Client'] : "";
                        $candidate->client_contracts = isset($value['Client Contact']) ? $value['Client Contact'] : "";
                        $candidate->client_billing_rate = isset($value['Client Billing Rate']) ? $value['Client Billing Rate'] : "";
                        $candidate->client_billing_mode = isset($value['Client Billing Mode']) ? $value['Client Billing Mode'] : "";
                        $candidate->consultant_pay_rate = isset($value['Consultant Pay Rate']) ? $value['Consultant Pay Rate'] : "";
                        $candidate->consultant_pay_mode = isset($value['Consultant Pay Mode']) ? $value['Consultant Pay Mode'] : "";
                        $candidate->current_project = isset($value['Current Project']) ? $value['Current Project'] : "";
                        $candidate->start_date = isset($value['Start Date']) ? $value['Start Date']->format('Y-m-d') : null;
                        $candidate->notes = isset($value['Notes']) ? $value['Notes'] : "";
                        $candidate->end_date = isset($value['End Date']) ? $value['End Date']->format('Y-m-d') : null;
                        $candidate->revenue_stream = isset($value['Revenue Stream']) ? $value['Revenue Stream'] : "";
                        $candidate->parked_candidate = isset($value['Parked Candidate']) ? $value['Parked Candidate'] : 0;
                        $candidate->candidate_status = isset($value['Candidate Status']) ? $value['Candidate Status'] : "";
                        $candidate->candidate_owner = isset($value['Candidate Owner']) ? $value['Candidate Owner'] : "";
                        $candidate->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                        $candidate->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                        $candidate->source = isset($value['Source']) ? $value['Source'] : "";
                        $candidate->privacy_policy_and_tnc = isset($value['Privacy Policy and T&C']) ? $value['Privacy Policy and T&C'] : 0;
                        $candidate->street = isset($value['Street']) ? $value['Street'] : "";
                        $candidate->zipcode = isset($value['Zip Code']) ? $value['Zip Code'] : "";
                        $candidate->city = isset($value['City']) ? $value['City'] : "";
                        $candidate->state = isset($value['State']) ? $value['State'] : "";
                        $candidate->country = isset($value['Country']) ? $value['Country'] : "";
                        $candidate->resume = isset($value['Resume']) ? $value['Resume'] : "";
                        $candidate->formated_resume = isset($value['Formatted Resume']) ? $value['Formatted Resume'] : "";
                        $candidate->cover_letter = isset($value['Cover Letter']) ? $value['Cover Letter'] : "";
                        $candidate->others = isset($value['Others']) ? $value['Others'] : "";
                        $candidate->hcl_formated_resume = isset($value['HCL Formatted Resume']) ? $value['HCL Formatted Resume'] : "";
                        $candidate->rating = isset($value['Rating']) ? $value['Rating'] : null;
                        $candidate->created_at = date('Y-m-d H:i:s', time());
                        $candidate->updated_at = date('Y-m-d H:i:s', time());
                        $candidate->save();
                        /* Removing old categories and adding new */
                        Candiatecategory::where([
                            'CANDIDATEID' => $candidate->CANDIDATEID
                        ])->delete();
                        if ($candidate->category != "" || $candidate->category != null) {
                            $catsArray = explode(',', $candidate->category);
                            if (!empty($catsArray)) {
                                foreach ($catsArray as $k => $v) {
                                    if ($v != "") {
                                        $category = new Candiatecategory();
                                        $category->candidate_id = $candidate->id;
                                        $category->CANDIDATEID = $candidate->CANDIDATEID;
                                        $category->category = $v;
                                        $category->created_at = date('Y-m-d H:i:s', time());
                                        $category->updated_at = date('Y-m-d H:i:s', time());
                                        $category->save();
                                    }
                                }
                            }
                        }
                        /* End Removing old categories and adding new */
                        /* Receiving candidate attachments */
                        $attachmentList = $this->zohoquery->getAttachments("Candidates", $candidate->CANDIDATEID);
                        if (count($attachmentList)) {
                            $userData['id'] = $candidate->id;
                            $userData['zoho_id'] = $candidate->CANDIDATEID;
                            $userData['type'] = "candidate";
                            $this->receiveCandidateAttachements($userData, $attachmentList);
                        }
                        /* End Receiving candidate attachments */
                        /* Update Jobassociatecandidates */
                        $associatedJob = Jobassociatecandidates::where([
                            'CANDIDATEID' => $candidate->CANDIDATEID
                        ])->get();

                        if (count($associatedJob)) {
                            foreach ($associatedJob as $key => $value) {
                                $job = Jobopenings::where('JOBOPENINGID', $value->JOBOPENINGID)->first();
                                if ($job != null) {
                                    $value->job_id = $job->id;
                                    $value->candidate_id = $candidate->id;
                                    $value->save();
                                }
                            }
                        }
                        /* End Update Jobassociatecandidates */
                        /* Add Update User Table */
                        $isExists = User::where('zoho_id', $candidate->CANDIDATEID)->first();
                        if ($isExists != null) { // Update
                            $isExists->first_name = $candidate->first_name;
                            $isExists->last_name = $candidate->last_name;
                            $isExists->sap_job_title = $candidate->sap_job_title;
                            $isExists->phone = $candidate->mobile_number;
                            $isExists->save();
                        } else { // Add
                            $userRecords = new User();
                            $userRecords->role_id = 2;
                            $userRecords->zoho_id = $candidate->CANDIDATEID;
                            $userRecords->first_name = $candidate->first_name;
                            $userRecords->last_name = $candidate->last_name;
                            $userRecords->email = $candidate->email;
                            $userRecords->sap_job_title = $candidate->sap_job_title;
                            $userRecords->phone = $candidate->mobile_number;
                            $userRecords->save();
                        }
                        /* End Add Update User Table */
                        /* Tabular Records [ Experience, Education, Contract History ] */
                        $tabularData = $this->zohoquery->getCandidateTabularRecords($candidate->CANDIDATEID);
                        if ($tabularData != null) {
                            $this->receiveTabularData($candidate, $tabularData);
                        }
                        /* End Tabular Records [ Experience, Education, Contract History ] */
                        /* Download Profile Picture */
                        $profilePicResponse = $this->zohoquery->donwloadProfilePhoto("Candidates", $candidate->CANDIDATEID);
                        if ($profilePicResponse['code'] == 200) {
                            $user = User::where('role_id', 2)->where('zoho_id', $candidate->CANDIDATEID)->first();
                            if ($user != null) {
                                $user->profile_pic = $profilePicResponse['filename'];
                                $user->save();
                            }
                        }
                        /* End Download Profile Picture */
                    }
                }
            }
            $cronLog = Cronlog::where('module_name', 'candidates')->first();
            $cronLog->last_run = date('Y-m-d H:i:s', time());
            $cronLog->save();
        }
        return;
    }

    public function receiveCandidateAttachements($userData, $attachmentList)
    {
        foreach ($attachmentList as $key => $value) {
            if ($value['Category'] == "Resume") {
                Attachment::where('zoho_id', $userData['zoho_id'])->where('category', 'Resume')->delete();
            }
            $isAttachmentExists = Attachment::where('attachment_id', $value['id'])->first();
            if ($isAttachmentExists != null) { // Update
                $attachment = $isAttachmentExists;
            } else { // Add
                $attachment = new Attachment();
            }
            if ($userData['type'] == "client") {
                $attachment->client_id = $userData['id'];
            } else {
                $attachment->candidate_id = $userData['id'];
            }
            $attachment->zoho_id = $userData['zoho_id'];
            $attachment->attachment_id = $value['id'];
            $attachment->filename = $value['File Name'];
            $attachment->size = $value['Size'];
            $attachment->category = $value['Category'];
            $attachment->attach_by = $value['Attached By'];
            $attachment->created_at = $value['Modified Time']->format('Y-m-d H:i:s');
            $attachment->updated_at = $value['Modified Time']->format('Y-m-d H:i:s');
            $attachment->save();
        }
        return;
    }

    public function receiveTabularData($candidate, $tabularData)
    {
        if (count($tabularData)) {
            if (isset($tabularData['response']['result'])) {
                unset($tabularData['response']['result']['uri']);
                foreach ($tabularData['response']['result']['Candidates']['FL'] as $key1 => $value1) {
                    /* Saving Education Data */
                    if ($value1['val'] == "Educational Details") {
                        if (isset($value1['TR'])) {
                            if (isset($value1['TR']['TL'])) {
                                $value1['TR'][0] = $value1['TR'];
                            }
                            foreach ($value1['TR'] as $trkey1 => $trvalue1) {
                                if (isset($trvalue1['TL'])) {
                                    if (key_exists(0, $trvalue1['TL'])) { // Getting improper data so
                                        foreach ($trvalue1['TL'] as $tdkey1 => $tdvalue1) {
                                            if ($tdvalue1['val'] == "TABULARROWID" && isset($tdvalue1['content'])) {
                                                $isEducationExists = Education::where('TABULARROWID', $tdvalue1['content'])->first();
                                                if ($isEducationExists != null) {
                                                    $education = $isEducationExists;
                                                } else {
                                                    $education = new Education();
                                                }
                                                $education->candidate_id = $candidate->id;
                                                $education->CANDIDATEID = $candidate->CANDIDATEID;
                                                $education->TABULARROWID = $tdvalue1['content'];
                                                $education->currently_pursuing = "false";
                                            }
                                            if ($tdvalue1['val'] == "Institute / School" && isset($tdvalue1['content'])) {
                                                $education->institute = $tdvalue1['content'];
                                            }
                                            if ($tdvalue1['val'] == "Major / Department" && isset($tdvalue1['content'])) {
                                                $education->department = $tdvalue1['content'];
                                            }
                                            if ($tdvalue1['val'] == "Degree" && isset($tdvalue1['content'])) {
                                                $education->degree = $tdvalue1['content'];
                                            }
                                            if ($tdvalue1['val'] == "Duration_From" && isset($tdvalue1['content'])) {
                                                $education->duration_from = $tdvalue1['content'];
                                            }
                                            if ($tdvalue1['val'] == "Duration_To" && isset($tdvalue1['content'])) {
                                                $education->duration_to = $tdvalue1['content'];
                                            }
                                            if ($tdvalue1['val'] == "Currently pursuing" && isset($tdvalue1['content'])) {
                                                $education->currently_pursuing = "true";
                                            }
                                        }
                                        $education->created_at = date('Y-m-d h:i:s', time());
                                        $education->updated_at = date('Y-m-d h:i:s', time());
                                        $education->save();
                                    }
                                }
                            }
                        }
                    }
                }

                /* Saving Experience Data */
                foreach ($tabularData['response']['result']['Candidates']['FL'] as $key2 => $value2) {
                    if ($value2['val'] == "Experience Details") {
                        if (isset($value2['TR'])) {
                            if (isset($value2['TR']['TL'])) {
                                $value2['TR'][0] = $value2['TR'];
                            }
                            foreach ($value2['TR'] as $trkey2 => $trvalue2) {
                                if (isset($trvalue2['TL'])) {
                                    if (key_exists(0, $trvalue2['TL'])) { // Getting improper data so
                                        foreach ($trvalue2['TL'] as $tdkey2 => $tdvalue2) {
                                            if ($tdvalue2['val'] == "TABULARROWID" && isset($tdvalue2['content'])) {
                                                $isExperienceExists = Experience::where('TABULARROWID', $tdvalue2['content'])->first();
                                                if ($isExperienceExists != null) {
                                                    $experience = $isExperienceExists;
                                                } else {
                                                    $experience = new Experience();
                                                }
                                                $experience->candidate_id = $candidate->id;
                                                $experience->CANDIDATEID = $candidate->CANDIDATEID;
                                                $experience->TABULARROWID = $tdvalue2['content'];
                                                $experience->is_currently_working_here = "false";
                                            }
                                            if ($tdvalue2['val'] == "Occupation / Title" && isset($tdvalue2['content'])) {
                                                $experience->occupation = $tdvalue2['content'];
                                            }
                                            if ($tdvalue2['val'] == "Company" && isset($tdvalue2['content'])) {
                                                $experience->company = $tdvalue2['content'];
                                            }
                                            if ($tdvalue2['val'] == "Industry" && isset($tdvalue2['content'])) {
                                                $experience->industry = $tdvalue2['content'];
                                            }
                                            if ($tdvalue2['val'] == "Work Duration_From" && isset($tdvalue2['content'])) {
                                                $experience->work_duration = $tdvalue2['content'];
                                            }
                                            if ($tdvalue2['val'] == "Work Duration_To" && isset($tdvalue2['content'])) {
                                                $experience->work_duration_to = $tdvalue2['content'];
                                            }
                                            if ($tdvalue2['val'] == "I currently work here" && isset($tdvalue2['content'])) {
                                                $experience->is_currently_working_here = "true";
                                            }
                                            if ($tdvalue2['val'] == "Summary" && isset($tdvalue2['content'])) {
                                                $experience->summary = $tdvalue2['content'];
                                            }
                                        }
                                        $experience->created_at = date('Y-m-d h:i:s', time());
                                        $experience->updated_at = date('Y-m-d h:i:s', time());
                                        $experience->save();
                                    }
                                }
                            }
                        }
                    }
                }
                /* Saving Contract History Data */
                foreach ($tabularData['response']['result']['Candidates']['FL'] as $key3 => $value3) {
                    if ($value3['val'] == "Contract History") {
                        if (isset($value3['TR'])) {
                            if (isset($value3['TR']['TL'])) {
                                $value3['TR'][0] = $value3['TR'];
                            }
                            foreach ($value3['TR'] as $trkey3 => $trvalue3) {
                                if (isset($trvalue3['TL'])) {
                                    if (key_exists(0, $trvalue3['TL'])) { // Getting improper data so
                                        foreach ($trvalue3['TL'] as $tdkey3 => $tdvalue3) {
                                            if ($tdvalue3['val'] == "TABULARROWID" && isset($tdvalue3['content'])) {
                                                $isContarctHistoryExists = Contracthistory::where('TABULARROWID', $tdvalue3['content'])->first();
                                                if ($isContarctHistoryExists != null) {
                                                    $contractHistory = $isContarctHistoryExists;
                                                } else {
                                                    $contractHistory = new Contracthistory();
                                                }
                                                $contractHistory->candidate_id = $candidate->id;
                                                $contractHistory->CANDIDATEID = $candidate->CANDIDATEID;
                                                $contractHistory->TABULARROWID = $tdvalue3['content'];
                                            }
                                            if ($tdvalue3['val'] == "Past Client" && isset($tdvalue3['content'])) {
                                                $contractHistory->past_client = $tdvalue3['content'];
                                            }
                                            if ($tdvalue3['val'] == "Past Project" && isset($tdvalue3['content'])) {
                                                $contractHistory->past_project = $tdvalue3['content'];
                                            }
                                            if ($tdvalue3['val'] == "Pay Rate" && isset($tdvalue3['content'])) {
                                                $contractHistory->pay_rate = $tdvalue3['content'];
                                            }
                                            if ($tdvalue3['val'] == "Contract Period_From" && isset($tdvalue3['content'])) {
                                                $contractHistory->contract_period_from = date('Y-m-d', strtotime($tdvalue3['content']));
                                            }
                                            if ($tdvalue3['val'] == "Contract Period_To" && isset($tdvalue3['content'])) {
                                                $contractHistory->contract_period_to = date('Y-m-d', strtotime($tdvalue3['content']));
                                            }
                                            if ($tdvalue3['val'] == "Additional Notes" && isset($tdvalue3['content'])) {
                                                $contractHistory->additional_note = $tdvalue3['content'];
                                            }
                                        }
                                        $contractHistory->created_at = date('Y-m-d h:i:s', time());
                                        $contractHistory->updated_at = date('Y-m-d h:i:s', time());
                                        $contractHistory->save();
                                    }
                                }
                            }
                        }
                    }
                }
                /* Saving candidate References */
                foreach ($tabularData['response']['result']['Candidates']['FL'] as $key4 => $value4) {
                    if ($value4['val'] == "References") {
                        if (isset($value4['TR'])) {

                            if (isset($value4['TR']['TL'])) {
                                $value4['TR'][0] = $value4['TR'];
                            }

                            foreach ($value4['TR'] as $trkey4 => $trvalue4) {
                                if (isset($trvalue4['TL'])) {
                                    if (key_exists(0, $trvalue4['TL'])) { // Getting improper data so
                                        foreach ($trvalue4['TL'] as $tdkey4 => $tdvalue4) {
                                            if ($tdvalue4['val'] == "TABULARROWID" && isset($tdvalue4['content'])) {
                                                $isReferenceExists = Reference::where('TABULARROWID', $tdvalue4['content'])->first();
                                                if ($isReferenceExists != null) {
                                                    $reference = $isReferenceExists;
                                                } else {
                                                    $reference = new Reference();
                                                }
                                                $reference->candidate_id = $candidate->id;
                                                $reference->CANDIDATEID = $candidate->CANDIDATEID;
                                                $reference->TABULARROWID = $tdvalue4['content'];
                                            }
                                            if ($tdvalue4['val'] == "Reference Name" && isset($tdvalue4['content'])) {
                                                $reference->name = $tdvalue4['content'];
                                            }
                                            if ($tdvalue4['val'] == "Reference Position" && isset($tdvalue4['content'])) {
                                                $reference->position = $tdvalue4['content'];
                                            }
                                            if ($tdvalue4['val'] == "Reference Company" && isset($tdvalue4['content'])) {
                                                $reference->company = $tdvalue4['content'];
                                            }
                                            if ($tdvalue4['val'] == "Reference Phone no." && isset($tdvalue4['content'])) {
                                                $reference->phone = $tdvalue4['content'];
                                            }
                                            if ($tdvalue4['val'] == "Reference Email" && isset($tdvalue4['content'])) {
                                                $reference->email = $tdvalue4['content'];
                                            }
                                        }
                                        $reference->created_at = date('Y-m-d h:i:s', time());
                                        $reference->updated_at = date('Y-m-d h:i:s', time());
                                        $reference->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $cronLog = Cronlog::where('module_name', 'candidate_tabular_data')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        return;
    }

    public function receiveCandidateSignedAttachment($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;
        $data = $this->zohoquery->getSignedAttachment();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    $isDocumentExists = Signeddocument::where('CUSTOMMODULE5_ID', $value['CUSTOMMODULE5_ID'])->first();
                    if ($isDocumentExists) {
                        $document = $isDocumentExists;
                    } else {
                        $document = new Signeddocument();
                    }
                    $document->CUSTOMMODULE5_ID = isset($value['CUSTOMMODULE5_ID']) ? $value['CUSTOMMODULE5_ID'] : "";
                    $document->zoho_sign_document_name = isset($value['ZohoSign Documents Name']) ? $value['ZohoSign Documents Name'] : "";
                    $document->zoho_sign_document_owner = isset($value['ZohoSign Documents Owner']) ? $value['ZohoSign Documents Owner'] : "";
                    $document->email = isset($value['Email']) ? $value['Email'] : "";
                    $document->secondary_email = isset($value['Secondary Email']) ? $value['Secondary Email'] : "";
                    $document->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                    $document->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                    $document->contact = isset($value['Contact']) ? $value['Contact'] : "";
                    $document->JOBOPENINGID = isset($value['Job Opening_ID']) ? $value['Job Opening_ID'] : "";
                    $document->CANDIDATEID = isset($value['Candidate_ID']) ? $value['Candidate_ID'] : "";
                    $document->date_completed = isset($value['Date Completed']) ? $value['Date Completed']->format('Y-m-d') : "";
                    $document->date_declined = isset($value['Date Declined']) ? $value['Date Declined']->format('Y-m-d') : "";
                    $document->date_sent = isset($value['Date Sent']) ? $value['Date Sent']->format('Y-m-d') : "";
                    $document->decline_reason = isset($value['Declined Reason']) ? $value['Declined Reason'] : "";
                    $document->document_decline = isset($value['Document Deadline']) ? $value['Document Deadline'] : "";
                    $document->document_description = isset($value['Document Description']) ? $value['Document Description'] : "";
                    $document->document_status = isset($value['Document Status']) ? $value['Document Status'] : "";
                    $document->time_to_complete = isset($value['Time to complete']) ? $value['Time to complete'] : "";
                    $document->preview_or_position_signature = isset($value['Preview or Position Signature Fields']) ? $value['Preview or Position Signature Fields'] : "";
                    $document->email_opt_out = isset($value['Email Opt Out']) ? $value['Email Opt Out'] : "";
                    $document->currency = isset($value['Currency']) ? $value['Currency'] : "";
                    $document->recalled_reason = isset($value['Recalled Reason']) ? $value['Recalled Reason'] : "";
                    $document->declined_reason_zero = isset($value['Declined Reason0']) ? $value['Declined Reason0'] : "";
                    $document->zoho_signed_document_id_zero = isset($value['ZohoSign Document ID0']) ? $value['ZohoSign Document ID0'] : "";
                    $document->document_note_zero = isset($value['Document Note0']) ? $value['Document Note0'] : "";
                    $document->is_from_offer = isset($value['Is From Offer']) ? $value['Is From Offer'] : "";
                    $document->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d h:i:s') : "";
                    $document->others = isset($value['Others']) ? $value['Others'] : "";
                    $document->created_at = date('Y-m-d h:i:s', time());
                    $document->updated_at = date('Y-m-d h:i:s', time());
                    $document->save();

                    $attahements = $this->zohoquery->downloadSignedAttachment($value['CUSTOMMODULE5_ID']);
                    if (count($attahements)) {
                        foreach ($attahements as $key1 => $value1) {
                            $isAttachementExists = Signeddocumentattachment::where('attachment_id', $value1['id'])->first();
                            if ($isAttachementExists != null) {
                                $signedAttachment = $isAttachementExists;
                            } else {
                                $signedAttachment = new Signeddocumentattachment();
                                /* Send notifiction to Admin */
                                $candidate = Candidates::where('CANDIDATEID', $document->CANDIDATEID)->first();
                                if ($candidate != null) {
                                    $notifications = new Notification;
                                    $notifications->s_id = $candidate->id;
                                    $notifications->sender_id = $candidate->CANDIDATEID;
                                    $notifications->message = "New signed document has been attached by " . $candidate->first_name . ' ' . $candidate->last_name;
                                    $notifications->type = "new_signed_document_attached";
                                    $notifications->to = "Admin";
                                    $notifications->save();
                                }
                            }
                            $signedAttachment->signed_document_id = $document->id;
                            $signedAttachment->attachment_id = $value1['id'];
                            $signedAttachment->filename = $value1['File Name'];
                            $signedAttachment->size = $value1['Size'];
                            $signedAttachment->modified_time = $value1['Modified Time']->format('Y-m-d h:i:s');
                            $signedAttachment->attach_by = $value1['Attached By'];
                            $signedAttachment->category = $value1['Category'];
                            $signedAttachment->created_at = date('Y-m-d h:i:s', time());
                            $signedAttachment->updated_at = date('Y-m-d h:i:s', time());
                            $signedAttachment->save();
                        }
                    }
                }
            }
        }
        $cronLog = Cronlog::where('module_name', 'service_agreement')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        return;
    }

    /**
     * Syncing interviews data
     * 
     */
    public function receiveInterviews($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;
        $data = $this->zohoquery->getInterviews();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    if (isset($value['Territory'])) {
                        if (isset($value['CLIENTID']) && isset($value['JOBOPENINGID']) && isset($value['CANDIDATEID'])) {
                            $db_client = Clients::where('CLIENTID', $value['CLIENTID'])->first();
                            $db_job = Jobopenings::where('JOBOPENINGID', $value['JOBOPENINGID'])->first();
                            $db_candidate = Candidates::where('CANDIDATEID', $value['CANDIDATEID'])->first();

                            //                        $isExistsInterview = Interviews::where([
                            //                                    'INTERVIEWID' => $value['INTERVIEWID'],
                            //                                    'JOBOPENINGID' => $value['JOBOPENINGID'],
                            //                                    'CANDIDATEID' => $value['CANDIDATEID']
                            //                                ])->first();
                            $isExistsInterview = Interviews::where([
                                'INTERVIEWID' => $value['INTERVIEWID']
                            ])->first();
                            if ($isExistsInterview != null) {
                                $interview = $isExistsInterview;
                            } else {
                                $interview = new Interviews();
                            }
                            $interview->INTERVIEWID = isset($value['INTERVIEWID']) ? $value['INTERVIEWID'] : "";
                            $interview->interview_name = isset($value['Interview Name']) ? $value['Interview Name'] : "";
                            $interview->CLIENTID = (isset($value['CLIENTID'])) ? $value['CLIENTID'] : "";
                            $interview->JOBOPENINGID = (isset($value['JOBOPENINGID'])) ? $value['JOBOPENINGID'] : "";
                            $interview->CANDIDATEID = (isset($value['CANDIDATEID'])) ? $value['CANDIDATEID'] : "";
                            $interview->client_id = isset($db_client) ? $db_client->id : "";
                            $interview->type = (isset($value['Type'])) ? $value['Type'] : "";
                            $interview->job_id = (isset($db_job)) ? $db_job->id : 0;
                            $interview->candidate_id = (isset($db_candidate)) ? $db_candidate->id : 0;
                            $interview->Interviewers = isset($value['Interviewer(s)']) ? $value['Interviewer(s)'] : "";
                            $interview->start_datetime = isset($value['Start DateTime']) ? $value['Start DateTime']->format('Y-m-d H:i:s') : null;
                            $interview->end_datetime = isset($value['End DateTime']) ? $value['End DateTime']->format('Y-m-d H:i:s') : null;
                            $interview->venue = isset($value['Venue']) ? $value['Venue'] : "";
                            $interview->reminder = isset($value['Reminder']) ? $value['Reminder'] : "";
                            $interview->schedule_comment = isset($value['Schedule Comments']) ? $value['Schedule Comments'] : "";
                            $interview->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : null;
                            $interview->interview_status = isset($value['Interview Status']) ? $value['Interview Status'] : "";
                            $interview->interview_owner = isset($value['Interview Owner']) ? $value['Interview Owner'] : "";
                            $interview->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                            $interview->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                            $interview->territory = isset($value['Territory']) ? $value['Territory'] : "";
                            $interview->others = isset($value['Interview Name']) ? $value['Interview Name'] : "";
                            $interview->ics = isset($value['ICS']) ? $value['ICS'] : "";
                            $interview->created_at = date('Y-m-d H:i:s', time());
                            $interview->updated_at = date('Y-m-d H:i:s', time());
                            $interview->save();

                            $interviewNotes = $this->zohoquery->getInterviewNotes($interview->INTERVIEWID);
                            if (count($interviewNotes)) {
                                $this->receiveInterviewNotes($interviewNotes, $interview->INTERVIEWID);
                            }
                        }
                    }
                }
            }
        }
        $cronLog = Cronlog::where('module_name', 'interviews')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        return;
    }

    public function receiveInterviewNotes($interviewNotes, $interviewId)
    {
        foreach ($interviewNotes as $key1 => $value1) {
            $isExistsNote = Interviewnote::where([
                'note_id' => $value1['id']
            ])->first();
            if ($isExistsNote != null) {
                $note = $isExistsNote;
            } else {
                $note = new Interviewnote();
            }
            $note->INTERVIEWID = $interviewId;
            $note->note_id = isset($value1['id']) ? $value1['id'] : "";
            $note->title = isset($value1['Title']) ? $value1['Title'] : "";
            $note->note_content = isset($value1['Note Content']) ? $value1['Note Content'] : "";
            $note->owner_id = isset($value1['SMOWNERID']) ? $value1['SMOWNERID'] : "";
            $note->owner_name = isset($value1['Owner Name']) ? $value1['Owner Name'] : "";
            $note->created_id = isset($value1['SMCREATORID']) ? $value1['SMCREATORID'] : "";
            $note->created_by = isset($value1['Created By']) ? $value1['Created By'] : "";
            $note->created_time = isset($value1['Created Time']) ? $value1['Created Time']->format('Y-m-d H:i:s') : "";
            $note->modify_id = isset($value1['MODIFIEDBY']) ? $value1['MODIFIEDBY'] : "";
            $note->modify_by = isset($value1['Modified By']) ? $value1['Modified By'] : "";
            $note->modify_time = isset($value1['Modified Time']) ? $value1['Modified Time']->format('Y-m-d H:i:s') : "";
            $note->candidate_name = isset($value1['Candidate Name']) ? $value1['Candidate Name'] : "";
            $note->job_opening_name = isset($value1['Job Opening Name']) ? $value1['Job Opening Name'] : "";
            $note->client_name = isset($value1['Client Name']) ? $value1['Client Name'] : "";
            $note->contact_name = isset($value1['Contact Name']) ? $value1['Contact Name'] : "";
            $note->is_voice_note = isset($value1['ISVOICENOTES']) ? $value1['ISVOICENOTES'] : "";
            $note->created_at = date("Y-m-d H:i:s", time());
            $note->updated_at = date("Y-m-d H:i:s", time());
            $note->save();
        }
        return;
    }

    /**
     * Syncing candidate profile requests
     * 
     */
    public function receiveCandidateProfileRequests($territory = "MY")
    {
        $result = Token::where('territory', $territory)->first();
        $this->zohoquery->token = $result->token;
        $data = $this->zohoquery->getProfileRequests();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    $isProfileRequestExists = Candidateprofilerequests::where('CUSTOMMODULE2_ID', $value['CUSTOMMODULE2_ID'])->first();
                    if ($isProfileRequestExists) {
                        $profilerequest = $isProfileRequestExists;
                    } else {
                        $profilerequest = new Candidateprofilerequests();
                    }
                    $profilerequest->CUSTOMMODULE2_ID = isset($value['CUSTOMMODULE2_ID']) ? $value['CUSTOMMODULE2_ID'] : "";
                    $profilerequest->first_name = isset($value['First Name']) ? $value['First Name'] : "";
                    $profilerequest->last_name = isset($value['Last Name']) ? $value['Last Name'] : "";
                    $profilerequest->SMOWNERID = isset($value['SMOWNERID']) ? $value['SMOWNERID'] : "";
                    $profilerequest->custommodule2_owner = isset($value['CustomModule2 Owner']) ? $value['CustomModule2 Owner'] : "";
                    $profilerequest->email = isset($value['Email']) ? $value['Email'] : "";
                    $profilerequest->SMCREATORID = isset($value['SMCREATORID']) ? $value['SMCREATORID'] : "";
                    $profilerequest->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                    $profilerequest->MODIFIEDBY = isset($value['MODIFIEDBY']) ? $value['MODIFIEDBY'] : "";
                    $profilerequest->modify_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                    $profilerequest->created_time = isset($value['Created Time']) ? $value['Created Time']->format('Y-m-d h:i:s') : "";
                    $profilerequest->modified_time = isset($value['Modified Time']) ? $value['Modified Time']->format('Y-m-d h:i:s') : "";
                    $profilerequest->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d h:i:s') : "";
                    $profilerequest->currency = isset($value['Currency']) ? $value['Currency'] : "";
                    $profilerequest->exchange_rate = isset($value['Exchange Rate']) ? $value['Exchange Rate'] : "";
                    $profilerequest->CANDIDATEID = isset($value['Link to Candidate_ID']) ? $value['Link to Candidate_ID'] : "";
                    $profilerequest->candidate_name = isset($value['Link to Candidate']) ? $value['Link to Candidate'] : "";
                    $profilerequest->CONTACTID = isset($value['Link to Contact_ID']) ? $value['Link to Contact_ID'] : "";
                    $profilerequest->contact_name = isset($value['Link to Contact']) ? $value['Link to Contact'] : "";
                    $profilerequest->request_status = isset($value['Request Status']) ? $value['Request Status'] : "";
                    $profilerequest->request_id = isset($value['Request ID']) ? $value['Request ID'] : "";
                    $profilerequest->message = isset($value['Message']) ? $value['Message'] : "";
                    $profilerequest->candidate_uid = isset($value['Candidate ID']) ? $value['Candidate ID'] : "";
                    $profilerequest->territory = isset($value['Territory']) ? $value['Territory'] : "";
                    $profilerequest->is_synced = 1;
                    $profilerequest->created_at = date('Y-m-d h:i:s', time());
                    $profilerequest->updated_at = date('Y-m-d h:i:s', time());
                    $profilerequest->save();
                }
            }
        }
        $cronLog = Cronlog::where('module_name', 'candidate_profile_requests')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        return;
    }
}