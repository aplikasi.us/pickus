<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Humantech\Zoho\Recruit\Api\Client\Client;
use App\Token;
use App\User;
use App\Clients;
use App\Jobopenings;
use App\Interviews;
use App\Interviewnote;
use App\Candidates;
use App\Jobassociatecandidates;
use App\Attachment;
use App\Education;
use App\Experience;
use App\Contracthistory;
use App\Reference;
use App\Signeddocument;
use App\Signeddocumentattachment;
use App\Cronlog;
use App\Notification;
use App\Clientcontact;
use App\Candiatecategory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ZohoqueryController;

class SyncFromZohotoAplikasi extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:toaplikasi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data from zoho to aplikasi [Clients, Candidates, Jobs, Interviews]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $token;

    public function __construct() {
        parent::__construct();
        $result = Token::first();
        $this->token = $result->token;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        echo "Function is Running...\n";
        echo "Saving Client and Job Opening...\n";
        $this->saveClientData();
        // echo "Saving Candidate...\n";
        // $this->saveCandidates();
        // echo "Saving Candidate Signed Attachments...\n";
        // $this->saveCandidateSignedAttachment();
        //echo "Saving Interview...\n";
        // $this->saveInterviews();
        echo "Migration Runs Successfully!!";
    }

    /* Saving Client Data */

    public function saveClientData() {
        $zohoQuery = new ZohoqueryController();
        $data = $zohoQuery->getClientRecords();
        $user = new User();
        
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    $isClientExists = Clients::where('id_zoho', $value['CLIENTID'])->first();

                    if ($isClientExists != null) { // Update
                        $client = $isClientExists;

                    } else { // Add
                        echo "add new client into table client \n";
                        $client = new Clients();

                    }
                    
                    $client->id_zoho = (isset($value['CLIENTID'])) ? $value['CLIENTID'] : "";
                    $client->client_name = (isset($value['Client Name'])) ? $value['Client Name'] : "";
                    $client->parent_client = (isset($value['Parent Client'])) ? $value['Parent Client'] : "";
                    $client->PARENTCLIENTID = (isset($value['PARENTCLIENTID'])) ? $value['PARENTCLIENTID'] : "";
                    $client->contact_number = (isset($value['Contact Number'])) ? $value['Contact Number'] : "";
                    $client->fax = (isset($value['Fax'])) ? $value['Fax'] : "";
                    $client->account_manager = (isset($value['Account Manager'])) ? $value['Account Manager'] : "";
                    $client->website = (isset($value['Website'])) ? $value['Website'] : "";
                    $client->industry = (isset($value['Industry'])) ? $value['Industry'] : "";
                    $client->about = (isset($value['About'])) ? $value['About'] : "";
                    $client->revenue_type = (isset($value['Revenue Type'])) ? $value['Revenue Type'] : "";
                    $client->currency = (isset($value['Currency'])) ? $value['Currency'] : "";
                    $client->territories = (isset($value['Territories'])) ? $value['Territories'] : "";
                    $client->last_activity_time = (isset($value['Last Activity Time'])) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : "";
                    $client->billing_street = (isset($value['Billing Street'])) ? $value['Billing Street'] : "";
                    $client->billing_city = (isset($value['Billing City'])) ? $value['Billing City'] : "";
                    $client->billing_state = (isset($value['Billing State'])) ? $value['Billing State'] : "";
                    $client->billing_code = (isset($value['Billing Code'])) ? $value['Billing Code'] : "";
                    $client->billing_country = (isset($value['Billing Country'])) ? $value['Billing Country'] : "";
                    $client->shipping_street = (isset($value['Shipping Street'])) ? $value['Shipping Street'] : "";
                    $client->shipping_city = (isset($value['Shipping City'])) ? $value['Shipping City'] : "";
                    $client->shipping_state = (isset($value['Shipping State'])) ? $value['Shipping State'] : "";
                    $client->shipping_code = (isset($value['Shipping Code'])) ? $value['Shipping Code'] : "";
                    $client->shipping_country = (isset($value['Shipping Country'])) ? $value['Shipping Country'] : "";
                    $client->created_by_id = (isset($value['SMCREATORID'])) ? $value['SMCREATORID'] : "";
                    $client->created_by = (isset($value['Created By'])) ? $value['Created By'] : "";
                    $client->modified_by_id = (isset($value['MODIFIEDBY'])) ? $value['MODIFIEDBY'] : "";
                    $client->modified_by = (isset($value['Modified By'])) ? $value['Modified By'] : "";
                    $client->source = (isset($value['Source'])) ? $value['Source'] : "";
                    $client->others = (isset($value['Others'])) ? $value['Others'] : "";
                    $client->created_at = date("Y-m-d H:i:s", time());
                    $client->updated_at = date("Y-m-d H:i:s", time());
                    $client->save();

                    $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'client_profile')->first();
                    $cronLog->last_run = date('Y-m-d H:i:s', time());
                    $cronLog->save();
                    
                    /* Add Update User Table */
                    
                    $this->addUpdateUserTable($client, 1);
                    
                    // $attachmentList = $zohoQuery->getAttachments("Clients", $value['CLIENTID']);
                    // if (count($attachmentList)) {
                    //     $userData['id'] = $client->id;
                    //     $userData['zoho_id'] = $client->id_zoho;
                    //     $userData['type'] = "client";
                    //     $this->saveAttachements($userData, $attachmentList);
                    // }

                            }

                        }
                    }
                    /* Save client contacts */
                     
                    $contacts = $zohoQuery->getClientContacts();
                    if (!empty($contacts)) {
                    $this->saveClientConatcts($contacts);
                    }
                    // save job opening 
                    $jobs = $zohoQuery->getJobOpenings();
                    if (count($jobs)) {
                        $this->saveClientJobs($jobs);
                    } 

        return;
    }

    public function saveClientConatcts($data) {
        foreach ($data as $page => $pageData) {
        foreach ($pageData as $key => $value) {
            $isContactExists = Clientcontact::where('id_zoho', $value['CONTACTID'])->first();
            if ($isContactExists != null) { // Update
                $contact = $isContactExists;
            } else { // Add
                $contact = new Clientcontact();
            }
            
            if(isset($value['CLIENTID'])){
                $client = Clients::where('id_zoho', $value['CLIENTID'])->first();
                $contact->CONTACTID = 'CONTACT_0'.substr($client->CLIENTID,8);
                $contact->CLIENTID = $client->CLIENTID;
                $user = User::where('id_zoho',$value['CLIENTID'])->first();
            }

            
            
            $contact->id_zoho =  isset($value['CONTACTID']) ? $value['CONTACTID'] : "";
            $contact->first_name = isset($value['First Name']) ? $value['First Name'] : "";
            $contact->salutation = isset($value['Salutation']) ? $value['Salutation'] : "";
            $contact->last_name = isset($value['Last Name']) ? $value['Last Name'] : "";
            $contact->department = isset($value['Department']) ? $value['Department'] : "";
            $contact->client_name = isset($value['Client Name']) ? $value['Client Name'] : "";
            $contact->work_phone = isset($value['Work Phone']) ? $value['Work Phone'] : "";
            $contact->job_title = isset($value['Job Title']) ? $value['Job Title'] : "";
            $contact->fax = isset($value['Fax']) ? $value['Fax'] : "";
            $contact->email = isset($value['Email']) ? $value['Email'] : "";
            $contact->skype_id = isset($value['Skype ID']) ? $value['Skype ID'] : "";
            $contact->mobile = isset($value['Mobile']) ? $value['Mobile'] : "";
            $contact->is_primary_contact = isset($value['Is primary contact']) ? 1 : 0;
            $contact->twitter = isset($value['Twitter']) ? $value['Twitter'] : "";
            $contact->currency = isset($value['Currency']) ? $value['Currency'] : "";
            $contact->email_opt_out = isset($value['Email Opt Out']) ? 1 : 0;
            $contact->territory = isset($value['Territory']) ? $value['Territory'] : "";
            $contact->secondary_email = isset($value['Secondary Email']) ? $value['Secondary Email'] : "";
            $contact->how_you_know_about_us = isset($value['How You Know About Us']) ? $value['How You Know About Us'] : "";
            $contact->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format("Y-m-d H:i:s") : "";
            $contact->territories = isset($value['Territories']) ? $value['Territories'] : "";
            $contact->mailing_street = isset($value['Mailing Street']) ? $value['Mailing Street'] : "";
            $contact->other_street = isset($value['Other Street']) ? $value['Other Street'] : "";
            $contact->mailing_city = isset($value['Mailing City']) ? $value['Mailing City'] : "";
            $contact->other_city = isset($value['Other City']) ? $value['Other City'] : "";
            $contact->mailing_state = isset($value['Mailing State']) ? $value['Mailing State'] : "";
            $contact->other_state = isset($value['Other State']) ? $value['Other State'] : "";
            $contact->mailing_zip = isset($value['Mailing Zip']) ? $value['Mailing Zip'] : "";
            $contact->other_zip = isset($value['Other Zip']) ? $value['Other Zip'] : "";
            $contact->mailing_country = isset($value['Mailing Country']) ? $value['Mailing Country'] : "";
            $contact->other_country = isset($value['Other Country']) ? $value['Other Country'] : "";
            $contact->SMOWNERID = isset($value['SMOWNERID']) ? $value['SMOWNERID'] : "";
            $contact->contact_owner = isset($value['Contact Owner']) ? $value['Contact Owner'] : "";
            $contact->source = isset($value['Source']) ? $value['Source'] : "";
            $contact->SMCREATORID = isset($value['SMCREATORID']) ? $value['SMCREATORID'] : "";
            $contact->created_by = isset($value['Created By']) ? $value['Created By'] : "";
            $contact->MODIFIEDBY = isset($value['MODIFIEDBY']) ? $value['MODIFIEDBY'] : "";
            $contact->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
            $contact->category = isset($value['Category']) ? $value['Category'] : "";
            $contact->subject = isset($value['Subject']) ? $value['Subject'] : "";
            $contact->message = isset($value['Message']) ? $value['Message'] : "";
            $contact->others = isset($value['Others']) ? $value['Others'] : "";
            $contact->created_at = date("Y-m-d H:i:s", time());
            $contact->updated_at = date("Y-m-d H:i:s", time());
            $contact->save();

            if(isset($user)){
                $user->email = $value['Email'];
                $user->contact_tbl_id = $contact->id;
                $user->save();
                }
        }
        return true;
    }
}

  
    // Add/Update in users table
    public function addUpdateUserTable($user, $role_id) {
        if ($role_id == 1) { // For Client
            $isExists = User::where('id_zoho', $user->id_zoho)->where('role_id', $role_id)->first();
            if ($isExists != null) { // Update
                $isExists->first_name = $user->client_name;
                $isExists->last_name = "";
                $isExists->sap_job_title = $user->about;
                $isExists->phone = $user->contact_number;
                $isExists->save();
            } else { // Add
                $client = new User();
                
                // // client table
                $clientRecord = Clients::where('id_zoho', $user['id_zoho'])->first();
                if($clientRecord != null){
                    $clientRecord->CLIENTID = 'CLIENT_0'.$user->id;
                    $clientRecord->save();
                   
                    }
                
                // user table
                $client->contact_tbl_id = 
                $client->is_confirm = 0;
                $client->id_zoho = $user->id_zoho;
                $client->role_id = $role_id;
                $client->zoho_id = 'CLIENT_0'.$user->id;
                $client->CONTACTID = 'CONTACT_0'.$user->id;
                $client->first_name = $user->client_name;
                $client->last_name = "";
                $client->sap_job_title = $user->about;
                $client->phone = $user->contact_number;
                $client->save();
         
            }
        } else { // For Candidate
            $isExists = User::where('zoho_id', $user->CANDIDATEID)->where('role_id', $role_id)->first();
            if ($isExists != null) { // Update
                $isExists->first_name = $user->first_name;
                $isExists->last_name = $user->last_name;
                $isExists->sap_job_title = $user->sap_job_title;
                $isExists->phone = $user->mobile_number;
                $isExists->save();
            } else { // Add
                $candidate = new User();
                $candidateRecord = Candidates::where('id_zoho', $user['id_zoho'])->first();
                $candidateRecord->CANDIDATEID = 'CONS_0'.$user->id;
                $candidateRecord->candidate_id = 'US_0'.$user->id;
                $candidate->role_id = $role_id;
                $candidate->zoho_id = 'CONS_0'.$user->id;
                // $user->zoho_id = 'CONS_0'.$user->id;
                $candidate->first_name = $user->first_name;
                $candidate->last_name = $user->last_name;
                $candidate->sap_job_title = $user->sap_job_title;
                $candidate->phone = $user->mobile_number;
                $candidateRecord->save();
                $candidate->save();
            }
        }
        return;
    }


    
    public function saveAttachements($userData, $attachmentList) {
        // here attachment 
        foreach ($attachmentList as $key => $value) {
            $isAttachmentExists = Attachment::where('attachment_id', $value['id'])->first();
//            $pendingAttachmentSync = 0;
            if ($isAttachmentExists != null) { // Update
                $attachment = $isAttachmentExists;
//                if ($isAttachmentExists->is_synced == 0) {
//                    $pendingAttachmentSync = 1;
//                }
            } else { // Add
                $attachment = new Attachment();
            }
//            if ($pendingAttachmentSync == 0) {
            if ($userData['type'] == "client") {
                $attachment->client_id = $userData['id'];
                $attachment->zoho_id = "CLIENT_0".$userData['id'];
            } else {
                $attachment->candidate_id = $userData['id'];
                $attachment->zoho_id = "CONS_0".$userData['id'];
            }
            
            $attachment->attachment_id = $value['id'];
            $attachment->filename = $value['File Name'];
            $attachment->size = $value['Size'];
            $attachment->category = $value['Category'];
            $attachment->attach_by = $value['Attached By'];
            $attachment->created_at = $value['Modified Time']->format('Y-m-d H:i:s');
            $attachment->updated_at = $value['Modified Time']->format('Y-m-d H:i:s');
            $attachment->save();

            $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'resume')->first();
            $cronLog->last_run = date('Y-m-d H:i:s', time());
            $cronLog->save();
//            }
        }
        
        return;
    }


    public function saveClientJobs($jobs) {
        
        foreach ($jobs as $key) {
            foreach($key as $value){
            $isJobExists = Jobopenings::where('id_zoho', $value['JOBOPENINGID'])->first();
            
            if ($isJobExists != null) {
                $jobOpening = $isJobExists;

            } else {
                $jobOpening = new Jobopenings();
            }

            if(isset($value['CONTACTID'])){
                $contact = Clientcontact::where("id_zoho",$value['CONTACTID'])->first();
                $client = Clients::where("id_zoho", $value['CLIENTID']);
                
                if ($contact != null) {
                    $jobOpening->db_contact_id = $contact->id;
                    $jobOpening->CONTACTID = $contact->CONTACTID;
                    $jobOpening->CLIENTID = $contact->CLIENTID;                    
                }

                if($client != null){
                    $jobOpening->client_id = $client->id; 
                }
            }

            $jobOpening->id_zoho =  $value['JOBOPENINGID'];
            $jobOpening->posting_title = isset($value['Posting Title']) ? $value['Posting Title'] : "";
            $jobOpening->job_type = isset($value['Job Type']) ? $value['Job Type'] : "";
            $jobOpening->job_mode = isset($value['Job Mode']) ? $value['Job Mode'] : "";
            $jobOpening->date_opened = isset($value['Date Opened']) ? $value['Date Opened']->format('Y-m-d H:i:s') : null;
            $jobOpening->contact_name = isset($value['Contact Name']) ? $value['Contact Name'] : "";
            $jobOpening->city = isset($value['City']) ? $value['City'] : "";
            $jobOpening->target_date = isset($value['Target Date']) ? $value['Target Date']->format('Y-m-d') : null;
            $jobOpening->job_duration = isset($value['Job Duration (months)']) ? $value['Job Duration (months)'] : "";
            $jobOpening->zipcode = isset($value['Zip Code']) ? $value['Zip Code'] : "";
            $jobOpening->state = isset($value['State']) ? $value['State'] : "";
            $jobOpening->country = isset($value['Country']) ? $value['Country'] : "";
            $jobOpening->salary = isset($value['Salary']) ? $value['Salary'] : "";
            $jobOpening->industry = isset($value['Industry']) ? $value['Industry'] : "";
            $jobOpening->job_base_rate = isset($value['Job Base Rate']) ? $value['Job Base Rate'] : "";
            $jobOpening->work_experience = isset($value['Work Experience']) ? $value['Work Experience'] : "";
            $jobOpening->key_skills = isset($value['Key Skills']) ? $value['Key Skills'] : "";
            $jobOpening->recruiter_id = isset($value['RECRUITERID']) ? $value['RECRUITERID'] : "";
            $jobOpening->assigned_recruiter = isset($value['Assigned Recruiter']) ? $value['Assigned Recruiter'] : "";
            $jobOpening->job_description = isset($value['Job Description']) ? html_entity_decode($value['Job Description']) : "";
            $jobOpening->internal_hire = isset($value['Internal Hire']) ? $value['Internal Hire'] : "";
            $jobOpening->job_requirements = isset($value['Job Requirements']) ? html_entity_decode($value['Job Requirements']) : "";
            $jobOpening->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : "";
            $jobOpening->job_benefits = isset($value['Job Benefits']) ? html_entity_decode($value['Job Benefits']) : "";
            $jobOpening->category = isset($value['Category']) ? $value['Category'] : "";
            $jobOpening->currency = isset($value['Currency']) ? $value['Currency'] : "";
            $jobOpening->territory = isset($value['Territory']) ? $value['Territory'] : "";
            $jobOpening->date_closed = isset($value['Date Closed']) ? $value['Date Closed'] : null;
            $jobOpening->publish_in_us = isset($value['Publish in Us']) ? $value['Publish in Us'] : 0;
            $jobOpening->job_opening_status = isset($value['Job Opening Status']) ? $value['Job Opening Status'] : "";
            $jobOpening->account_manager_id = isset($value['SMOWNERID']) ? $value['SMOWNERID'] : "";
            $jobOpening->account_manager = isset($value['Account Manager']) ? $value['Account Manager'] : "";
            $jobOpening->created_by_id = isset($value['SMCREATORID']) ? $value['SMCREATORID'] : "";
            $jobOpening->created_by = isset($value['Created By']) ? $value['Created By'] : "";
            $jobOpening->modified_by_id = isset($value['MODIFIEDBY']) ? $value['MODIFIEDBY'] : "";
            $jobOpening->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
            $jobOpening->revenue_stream = isset($value['Revenue stream']) ? $value['Revenue stream'] : "";
            $jobOpening->no_of_position = isset($value['Number of Positions']) ? $value['Number of Positions'] : "";
            $jobOpening->revenue_per_position = isset($value['Revenue per Position']) ? $value['Revenue per Position'] : "";
            $jobOpening->expected_revenue = isset($value['Expected Revenue']) ? $value['Expected Revenue'] : "";
            $jobOpening->actual_revenue = isset($value['Actual Revenue']) ? $value['Actual Revenue'] : "";
            $jobOpening->missed_revenue = isset($value['Missed Revenue']) ? $value['Missed Revenue'] : "";
            $jobOpening->is_approve_by_admin = 1;
            $jobOpening->created_at = date('Y-m-d H:i:s', time());
            $jobOpening->updated_at = date('Y-m-d H:i:s', time());
            $jobOpening->save(); //to get the id of the current record
            $jobOpening->JOBOPENINGID = "JOB_0".$jobOpening->id;
            $jobOpening->Job_ID = "JOB_0".$jobOpening->id;
            
            $jobOpening->save();
            $temp = "JOB_0".$jobOpening->id;
            
            /* Save Job Associate Candidates */
            $this->saveJobAssociateCandidates($jobOpening->id, $value['JOBOPENINGID'] , $temp);
            
            $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'client_job_openings')->first();
            $cronLog->last_run = date('Y-m-d H:i:s', time());
            $cronLog->save();

        }
    }
        return;
    }

    /* Saving job associate candidate */

    public function saveJobAssociateCandidates($jobId, $JOBOPENINGID, $temp) {
        $zohoQuery = new ZohoqueryController();
        $data = $zohoQuery->getJobAssociateCandidates($JOBOPENINGID);
        if (count($data)) {
            foreach ($data as $key => $value) {
              
                $isAssociatedCandidateExists = Jobassociatecandidates::where([
                            'job_id' => $jobId,
                            'id_zoho' => $value['CANDIDATEID']
                        ])->first();
                $candidateRecord = Candidates::where('id_zoho',$value['CANDIDATEID'])->first();                      
                // $pendingAssociatedCandidateSync = 0;
                if ($isAssociatedCandidateExists != null) {
                    $candidate = $isAssociatedCandidateExists;
                    // if ($isAssociatedCandidateExists->is_synced == 0) {
                    //     $pendingAssociatedCandidateSync = 1;
                    // }
                } else {
                    $candidate = new Jobassociatecandidates();
                }
                // if ($pendingAssociatedCandidateSync == 0) {
                $candidate->job_id = $jobId;
                $candidate->id_zoho = $JOBOPENINGID;
                $candidate->JOBOPENINGID = $temp;

                if(isset($candidateRecord)){
                    $candidate->CANDIDATEID = $candidateRecord->CANDIDATEID;
                }
                
                $candidate->status = $value['STATUS'];
                $candidate->created_at = date('Y-m-d H:i:s', time());
                $candidate->updated_at = date('Y-m-d H:i:s', time());
                $candidate->save();
                // }
            }
        }
    
        return;
    }

    /* Saving candidate data */

    public function saveCandidates() {
        $zohoQuery = new ZohoqueryController();
        $data = $zohoQuery->getCandidateData();

        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    $isCandidateExists = Candidates::where('id_zoho', $value['CANDIDATEID'])->first();
                    $pendingCandidateSync = 0;
                    if ($isCandidateExists != null) {
                        $candidate = $isCandidateExists;
                        if ($isCandidateExists->is_synced == 0) {
                            $pendingCandidateSync = 1;
                        }
                    } else {
                        $candidate = new Candidates();
                    }
                    if ($pendingCandidateSync == 0) {
                        $candidate->id_zoho = isset($value['CANDIDATEID']) ? $value['CANDIDATEID'] : "";
                        $candidate->first_name = isset($value['First Name']) ? $value['First Name'] : "";
                        $candidate->last_name = isset($value['Last Name']) ? $value['Last Name'] : "";
                        $candidate->salutation = isset($value['Salutation']) ? $value['Salutation'] : "";
                        $candidate->email = isset($value['Email']) ? $value['Email'] : "";
                        $candidate->skype_id = isset($value['Skype ID']) ? $value['Skype ID'] : "";
                        $candidate->mobile_number = isset($value['Mobile']) ? $value['Mobile'] : "";
                        $candidate->twitter = isset($value['Twitter']) ? $value['Twitter'] : "";
                        $candidate->website = isset($value['Website']) ? $value['Website'] : "";
                        $candidate->internal_hire = isset($value['Internal Hire']) ? $value['Internal Hire'] : 0;
                        $candidate->CANDIDATEID = isset($value['CANDIDATEID']) ? $value['CANDIDATEID'] : "";
                        $candidate->publish_in_us = isset($value['Publish in Us']) ? $value['Publish in Us'] : 0;
                        $candidate->secondary_email = isset($value['Secondary Email']) ? $value['Secondary Email'] : "";
                        $candidate->block = isset($value['Block']) ? $value['Block'] : 0;
                        $candidate->nationality = isset($value['Nationality']) ? $value['Nationality'] : "";
                        $candidate->currency = isset($value['Currency']) ? $value['Currency'] : "";
                        $candidate->territory = isset($value['Territory']) ? $value['Territory'] : "";
                        $candidate->sap_job_title = isset($value['SAP Job Title']) ? $value['SAP Job Title'] : "";
                        $candidate->highest_qualification_held = isset($value['Highest Qualification Held']) ? $value['Highest Qualification Held'] : "";
                        $candidate->experience_in_years = isset($value['Experience in Years']) ? $value['Experience in Years'] : 0;
                        $candidate->current_employer = isset($value['Current Employer']) ? $value['Current Employer'] : "";
                        $candidate->notice_period_days = isset($value['Notice Period (Days)']) ? $value['Notice Period (Days)'] : 0;
                        $candidate->expected_salary = isset($value['Expected Salary']) ? $value['Expected Salary'] : "";
                        $candidate->employment_type = isset($value['Employment Type']) ? $value['Employment Type'] : "";
                        $candidate->base_rate = isset($value['Base Rate (BR)']) ? $value['Base Rate (BR)'] : "";
                        $candidate->current_salary = isset($value['Current Salary']) ? $value['Current Salary'] : "";
                        $candidate->reserved_base_rate = isset($value['Reserved Base Rate']) ? $value['Reserved Base Rate'] : "";
                        $candidate->skill_set = isset($value['Skill Set']) ? $value['Skill Set'] : "";
                        $candidate->language = isset($value['Language']) ? $value['Language'] : "";
                        $candidate->certification_and_training = isset($value['Certifications & Trainings']) ? $value['Certifications & Trainings'] : "";
                        $candidate->additional_info = isset($value['Additional Info']) ? $value['Additional Info'] : "";
                        $candidate->category = isset($value['Category']) ? str_replace(";", ",", $value['Category']) : "";
                        $candidate->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : null;
                        $candidate->available_for_contract = isset($value['Available for Contract']) ? $value['Available for Contract'] : 0;
                        $candidate->availability_date = isset($value['Availability Date']) ? $value['Availability Date']->format('Y-m-d') : null;
                        $candidate->willing_to_travel = isset($value['Willing to travel']) ? $value['Willing to travel'] : 0;
                        $candidate->full_time = isset($value['Full-time']) ? $value['Full-time'] : 0;
                        $candidate->part_time = isset($value['Part-time']) ? $value['Part-time'] : 0;
                        $candidate->project = isset($value['Project']) ? $value['Project'] : 0;
                        $candidate->support = isset($value['Support']) ? $value['Support'] : 0;
                        $candidate->client = isset($value['Client']) ? $value['Client'] : "";
                        $candidate->client_contracts = isset($value['Client Contact']) ? $value['Client Contact'] : "";
                        $candidate->client_billing_rate = isset($value['Client Billing Rate']) ? $value['Client Billing Rate'] : "";
                        $candidate->client_billing_mode = isset($value['Client Billing Mode']) ? $value['Client Billing Mode'] : "";
                        $candidate->consultant_pay_rate = isset($value['Consultant Pay Rate']) ? $value['Consultant Pay Rate'] : "";
                        $candidate->consultant_pay_mode = isset($value['Consultant Pay Mode']) ? $value['Consultant Pay Mode'] : "";
                        $candidate->current_project = isset($value['Current Project']) ? $value['Current Project'] : "";
                        $candidate->start_date = isset($value['Start Date']) ? $value['Start Date']->format('Y-m-d') : null;
                        $candidate->notes = isset($value['Notes']) ? $value['Notes'] : "";
                        $candidate->end_date = isset($value['End Date']) ? $value['End Date']->format('Y-m-d') : null;
                        $candidate->revenue_stream = isset($value['Revenue Stream']) ? $value['Revenue Stream'] : "";
                        $candidate->parked_candidate = isset($value['Parked Candidate']) ? $value['Parked Candidate'] : 0;
                        $candidate->candidate_status = isset($value['Candidate Status']) ? $value['Candidate Status'] : "";
                        $candidate->candidate_owner = isset($value['Candidate Owner']) ? $value['Candidate Owner'] : "";
                        $candidate->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                        $candidate->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                        $candidate->source = isset($value['Source']) ? $value['Source'] : "";
                        $candidate->privacy_policy_and_tnc = isset($value['Privacy Policy and T&C']) ? $value['Privacy Policy and T&C'] : 0;
                        $candidate->street = isset($value['Street']) ? $value['Street'] : "";
                        $candidate->zipcode = isset($value['Zip Code']) ? $value['Zip Code'] : "";
                        $candidate->city = isset($value['City']) ? $value['City'] : "";
                        $candidate->state = isset($value['State']) ? $value['State'] : "";
                        $candidate->country = isset($value['Country']) ? $value['Country'] : "";
                        $candidate->resume = isset($value['Resume']) ? $value['Resume'] : "";
                        $candidate->formated_resume = isset($value['Formatted Resume']) ? $value['Formatted Resume'] : "";
                        $candidate->cover_letter = isset($value['Cover Letter']) ? $value['Cover Letter'] : "";
                        $candidate->others = isset($value['Others']) ? $value['Others'] : "";
                        $candidate->hcl_formated_resume = isset($value['HCL Formatted Resume']) ? $value['HCL Formatted Resume'] : "";
                        $candidate->rating = isset($value['Rating']) ? $value['Rating'] : null;
                        $candidate->created_at = date('Y-m-d H:i:s', time());
                        $candidate->updated_at = date('Y-m-d H:i:s', time());
                        $candidate->save();
                        echo "candidate saved \n";
                        $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'candidate_profile')->first();
                        $cronLog->last_run = date('Y-m-d H:i:s', time());
                        $cronLog->save();
                        if(isset($candidate->CANDIDATEID)){
                            Candiatecategory::where([
                                'id_zoho' => $candidate->CANDIDATEID
                            ])->delete();
                        }

                        if ($candidate->category != "" || $candidate->category != null) {
                            $catsArray = explode(',', $candidate->category);
                            if (!empty($catsArray)) {
                                foreach ($catsArray as $k => $v) {
                                    if ($v != "") {
                                        $category = new Candiatecategory();
                                        $category->candidate_id = $candidate->id;
                                        $category->CANDIDATEID = "US_0".$candidate->id;
                                        $category->id_zoho = $candidate->id_zoho;
                                        $category->category = $v;
                                        $category->created_at = date('Y-m-d H:i:s', time());
                                        $category->updated_at = date('Y-m-d H:i:s', time());
                                        $category->save();
                                    }
                                }
                            }
                        }
                    }
                    echo "candidate category save \n";
                    echo $candidate->id_zoho. "\n";
                    $attachmentList = $zohoQuery->getAttachments("Candidates", $candidate->id_zoho);
                    if (count($attachmentList)) {
                        $userData['id'] = $candidate->id;
                        $userData['id_zoho'] = $candidate->id_zoho;
                        $userData['type'] = "candidate";

                        $this->saveAttachements($userData, $attachmentList);
                    }

                    echo "attachment saved \n";
                    /* Update Jobassociatecandidates */
                    $associatedJob = Jobassociatecandidates::where([
                                'id_zoho' => $candidate->id_zoho
                            ])->get();
                    if (count($associatedJob)) {
                        foreach ($associatedJob as $key => $value) {
                            $job = Jobopenings::where('id_job_zoho', $value->JOBOPENINGID)->first();
                            if ($job != null) {
                                $value->job_id = $job->id;
                                $value->candidate_id = $candidate->id;
                                $value->save();
                            }
                        }
                    }
                    echo "job associated save \n";
                    /* Add Update User Table */
                    $this->addUpdateUserTable($candidate, 2);
                    echo "candidate user save \n";
                    /* Update Tabular Data Before Sync From Zoho */
                    // $this->syncTabulardataToZoho($zohoQuery,$candidate);
                    /* Tabular Records [ Experience, Education, Contract History ] */
                    $tabularData = $zohoQuery->getCandidateTabularRecords($candidate->CANDIDATEID);
                    if (count($tabularData)) {
                        $this->saveTabularData($candidate, $tabularData);
                    }
                    echo "tabular record save \n";

                    /* Download Profile Picture */
                    $profilePicResponse = $zohoQuery->donwloadProfilePhoto("Candidates", $candidate->CANDIDATEID);
                    if ($profilePicResponse['code'] == 200) {
                        $user = User::where('role_id', 2)->where('id_zoho', $candidate->CANDIDATEID)->first();
                        if ($user != null) {
                            $user->profile_pic = $profilePicResponse['filename'];
                            $user->save();
                        }
                    }
                    echo "profile pic save \n";
                }
            }
        }
        return;
    }

    public function syncTabulardataToZoho($zohoQuery, $candidate) {
        /* Updating experience */
        $experience = Experience::where([
                    'CANDIDATEID' => $candidate->CANDIDATEID
                ])->get();
        if (count($experience)) {
            $experienceXmlData = $this->createExperienceXMLData($experience);
            $zohoQuery->updateCandidateTabulardata($candidate->CANDIDATEID, $experienceXmlData);
        }
        /* Updating education */
        $education = Education::where([
                    'CANDIDATEID' => $candidate->CANDIDATEID
                ])->get();
        if (count($education)) {
            $educationXmlData = $this->createEducationXMLData($education);
            $zohoQuery->updateCandidateTabulardata($candidate->CANDIDATEID, $educationXmlData);
        }
        /* Updating References */
        $references = Reference::where([
                    'CANDIDATEID' => $candidate->CANDIDATEID
                ])->get();
        if (count($references)) {
            $referenceXmlData = $this->createReferenceXMLData($references);
            $zohoQuery->updateCandidateTabulardata($candidate->CANDIDATEID, $referenceXmlData);
        }
        return true;
    }

    public function createExperienceXMLData($experience) {
        $xml = '<Candidates><FL val="Experience Details">';
        foreach ($experience as $key => $value) {
            $xml .= '';
            $xml .= '<TR no="' . $key . '">' .
                    '<TL val="Occupation / Title">' . $value->occupation . '</TL>' .
                    '<TL val="Company">' . $value->company . '</TL>' .
                    '<TL val="Industry">' . $value->industry . '</TL>' .
                    '<TL val="Summary">' . strip_tags($value->summary) . '</TL>' .
                    '<TL val="Work Duration_From">' . $value->work_duration . '</TL>' .
                    '<TL val="Work Duration_To">' . $value->work_duration_to . '</TL>' .
                    '<TL val="I currently work here">' . $value->is_currently_working_here . '</TL>' .
                    '</TR>';
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }

    public function createEducationXMLData($education) {
        $xml = '<Candidates><FL val="Educational Details">';
        foreach ($education as $key => $value) {
            $xml .= '';
            $xml .= '<TR no="' . $key . '">' .
                    '<TL val="Institute / School">' . $value->institute . '</TL>' .
                    '<TL val="Major / Department">' . $value->department . '</TL>' .
                    '<TL val="Degree">' . $value->degree . '</TL>' .
                    '<TL val="Duration_From">' . $value->duration_from . '</TL>' .
                    '<TL val="Duration_To">' . $value->duration_to . '</TL>' .
                    '<TL val="Currently pursuing">' . $value->currently_pursuing . '</TL>' .
                    '</TR>';
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }

    public function createReferenceXMLData($references) {
        $xml = '<Candidates><FL val="References">';
        foreach ($references as $key => $value) {
            $xml .= '';
            $xml .= '<TR no="' . $key . '">' .
                    '<TL val="Reference Name">' . $value->name . '</TL>' .
                    '<TL val="Reference Position">' . $value->position . '</TL>' .
                    '<TL val="Reference Company">' . $value->company . '</TL>' .
                    '<TL val="Reference Phone no.">' . $value->phone . '</TL>' .
                    '<TL val="Reference Email">' . $value->email . '</TL>' .
                    '</TR>';
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }

    public function saveTabularData($candidate, $tabularData) {
        if (count($tabularData)) {
            unset($tabularData['response']['result']['uri']);
            foreach ($tabularData['response']['result']['Candidates']['FL'] as $key1 => $value1) {
                /* Saving Education Data */
                if ($value1['val'] == "Educational Details") {
                    if (isset($value1['TR'])) {

                        if (isset($value1['TR']['TL'])) {
                            $value1['TR'][0] = $value1['TR'];
                        }

                        foreach ($value1['TR'] as $trkey1 => $trvalue1) {
                            if (isset($trvalue1['TL'])) {
                                if (key_exists(0, $trvalue1['TL'])) { // Getting improper data so
                                    foreach ($trvalue1['TL'] as $tdkey1 => $tdvalue1) {
                                        if ($tdvalue1['val'] == "TABULARROWID" && isset($tdvalue1['content'])) {
                                            $isEducationExists = Education::where('TABULARROWID', $tdvalue1['content'])->first();
                                            if ($isEducationExists != null) {
                                                $education = $isEducationExists;
                                            } else {
                                                $education = new Education();
                                            }
                                            $education->candidate_id = $candidate->id;
                                            $education->CANDIDATEID = "CONS_0".$candidate->id;
                                            $education->TABULARROWID = $tdvalue1['content'];
                                        }
                                        if ($tdvalue1['val'] == "Institute / School" && isset($tdvalue1['content'])) {
                                            $education->institute = $tdvalue1['content'];
                                        }
                                        if ($tdvalue1['val'] == "Major / Department" && isset($tdvalue1['content'])) {
                                            $education->department = $tdvalue1['content'];
                                        }
                                        if ($tdvalue1['val'] == "Degree" && isset($tdvalue1['content'])) {
                                            $education->degree = $tdvalue1['content'];
                                        }
                                        if ($tdvalue1['val'] == "Duration_From" && isset($tdvalue1['content'])) {
                                            $education->duration_from = $tdvalue1['content'];
                                        }
                                        if ($tdvalue1['val'] == "Duration_To" && isset($tdvalue1['content'])) {
                                            $education->duration_to = $tdvalue1['content'];
                                        }
                                        if ($tdvalue1['val'] == "Currently pursuing" && isset($tdvalue1['content'])) {
                                            $education->currently_pursuing = $tdvalue1['content'];
                                        }
                                    }
                                    $education->created_at = date('Y-m-d h:i:s', time());
                                    $education->updated_at = date('Y-m-d h:i:s', time());
                                    $education->save();
                                }
                            }
                        }
                    }
                }
            }
            /* Saving Experience Data */
            foreach ($tabularData['response']['result']['Candidates']['FL'] as $key2 => $value2) {
                if ($value2['val'] == "Experience Details") {
                    if (isset($value2['TR'])) {

                        if (isset($value2['TR']['TL'])) {
                            $value2['TR'][0] = $value2['TR'];
                        }

                        foreach ($value2['TR'] as $trkey2 => $trvalue2) {
                            if (isset($trvalue2['TL'])) {
                                if (key_exists(0, $trvalue2['TL'])) { // Getting improper data so
                                    foreach ($trvalue2['TL'] as $tdkey2 => $tdvalue2) {
                                        if ($tdvalue2['val'] == "TABULARROWID" && isset($tdvalue2['content'])) {
                                            $isExperienceExists = Experience::where('TABULARROWID', $tdvalue2['content'])->first();
                                            if ($isExperienceExists != null) {
                                                $experience = $isExperienceExists;
                                            } else {
                                                $experience = new Experience();
                                            }
                                            $experience->candidate_id = $candidate->id;
                                            $experience->CANDIDATEID = "CONS_0".$candidate->id;
                                            $experience->TABULARROWID = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "Occupation / Title" && isset($tdvalue2['content'])) {
                                            $experience->occupation = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "Company" && isset($tdvalue2['content'])) {
                                            $experience->company = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "Industry" && isset($tdvalue2['content'])) {
                                            $experience->industry = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "Work Duration_From" && isset($tdvalue2['content'])) {
                                            $experience->work_duration = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "Work Duration_To" && isset($tdvalue2['content'])) {
                                            $experience->work_duration_to = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "I currently work here" && isset($tdvalue2['content'])) {
                                            $experience->is_currently_working_here = $tdvalue2['content'];
                                        }
                                        if ($tdvalue2['val'] == "Summary" && isset($tdvalue2['content'])) {
                                            $experience->summary = $tdvalue2['content'];
                                        }
                                    }
                                    $experience->created_at = date('Y-m-d h:i:s', time());
                                    $experience->updated_at = date('Y-m-d h:i:s', time());
                                    $experience->save();
                                }
                            }
                        }
                    }
                }
            }
            /* Saving Contract History Data */
            foreach ($tabularData['response']['result']['Candidates']['FL'] as $key3 => $value3) {
                if ($value3['val'] == "Contract History") {
                    if (isset($value3['TR'])) {

                        if (isset($value3['TR']['TL'])) {
                            $value3['TR'][0] = $value3['TR'];
                        }

                        foreach ($value3['TR'] as $trkey3 => $trvalue3) {
                            if (isset($trvalue3['TL'])) {
                                if (key_exists(0, $trvalue3['TL'])) { // Getting improper data so
                                    foreach ($trvalue3['TL'] as $tdkey3 => $tdvalue3) {
                                        if ($tdvalue3['val'] == "TABULARROWID" && isset($tdvalue3['content'])) {
                                            $isContarctHistoryExists = Contracthistory::where('TABULARROWID', $tdvalue3['content'])->first();
                                            if ($isContarctHistoryExists != null) {
                                                $contractHistory = $isContarctHistoryExists;
                                            } else {
                                                $contractHistory = new Contracthistory();
                                            }
                                            $contractHistory->candidate_id = $candidate->id;
                                            $contractHistory->CANDIDATEID = "CONS_0".$candidate->id;
                                            $contractHistory->TABULARROWID = $tdvalue3['content'];
                                        }
                                        if ($tdvalue3['val'] == "Past Client" && isset($tdvalue3['content'])) {
                                            $contractHistory->past_client = $tdvalue3['content'];
                                        }
                                        if ($tdvalue3['val'] == "Past Project" && isset($tdvalue3['content'])) {
                                            $contractHistory->past_project = $tdvalue3['content'];
                                        }
                                        if ($tdvalue3['val'] == "Pay Rate" && isset($tdvalue3['content'])) {
                                            $contractHistory->pay_rate = $tdvalue3['content'];
                                        }
                                        if ($tdvalue3['val'] == "Contract Period_From" && isset($tdvalue3['content'])) {
                                            $contractHistory->contract_period_from = date('Y-m-d', strtotime($tdvalue3['content']));
                                        }
                                        if ($tdvalue3['val'] == "Contract Period_To" && isset($tdvalue3['content'])) {
                                            $contractHistory->contract_period_to = date('Y-m-d', strtotime($tdvalue3['content']));
                                        }
                                        if ($tdvalue3['val'] == "Additional Notes" && isset($tdvalue3['content'])) {
                                            $contractHistory->additional_note = $tdvalue3['content'];
                                        }
                                    }
                                    $contractHistory->created_at = date('Y-m-d h:i:s', time());
                                    $contractHistory->updated_at = date('Y-m-d h:i:s', time());
                                    $contractHistory->save();
                                }
                            }
                        }
                    }
                }
            }
            /* Saving candidate References */
            foreach ($tabularData['response']['result']['Candidates']['FL'] as $key4 => $value4) {
                if ($value4['val'] == "References") {
                    if (isset($value4['TR'])) {

                        if (isset($value4['TR']['TL'])) {
                            $value4['TR'][0] = $value4['TR'];
                        }

                        foreach ($value4['TR'] as $trkey4 => $trvalue4) {
                            if (isset($trvalue4['TL'])) {
                                if (key_exists(0, $trvalue4['TL'])) { // Getting improper data so
                                    foreach ($trvalue4['TL'] as $tdkey4 => $tdvalue4) {
                                        if ($tdvalue4['val'] == "TABULARROWID" && isset($tdvalue4['content'])) {
                                            $isReferenceExists = Reference::where('TABULARROWID', $tdvalue4['content'])->first();
                                            if ($isReferenceExists != null) {
                                                $reference = $isReferenceExists;
                                            } else {
                                                $reference = new Reference();
                                            }
                                            $reference->candidate_id = $candidate->id;
                                            $reference->CANDIDATEID = "CONS_0".$candidate->id;
                                            $reference->TABULARROWID = $tdvalue4['content'];
                                        }
                                        if ($tdvalue4['val'] == "Reference Name" && isset($tdvalue4['content'])) {
                                            $reference->name = $tdvalue4['content'];
                                        }
                                        if ($tdvalue4['val'] == "Reference Position" && isset($tdvalue4['content'])) {
                                            $reference->position = $tdvalue4['content'];
                                        }
                                        if ($tdvalue4['val'] == "Reference Company" && isset($tdvalue4['content'])) {
                                            $reference->company = $tdvalue4['content'];
                                        }
                                        if ($tdvalue4['val'] == "Reference Phone no." && isset($tdvalue4['content'])) {
                                            $reference->phone = $tdvalue4['content'];
                                        }
                                        if ($tdvalue4['val'] == "Reference Email" && isset($tdvalue4['content'])) {
                                            $reference->email = $tdvalue4['content'];
                                        }
                                    }
                                    $reference->created_at = date('Y-m-d h:i:s', time());
                                    $reference->updated_at = date('Y-m-d h:i:s', time());
                                    $reference->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'candidate_tabular_data')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        return;
    }

    public function saveCandidateSignedAttachment() {
        $zohoQuery = new ZohoqueryController();
        $data = $zohoQuery->getSignedAttachment();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    $isDocumentExists = Signeddocument::where('CUSTOMMODULE5_ID', $value['CUSTOMMODULE5_ID'])->first();
                    if ($isDocumentExists) {
                        $document = $isDocumentExists;
                    } else {
                        $document = new Signeddocument();
                    }
                    $document->CUSTOMMODULE5_ID = isset($value['CUSTOMMODULE5_ID']) ? $value['CUSTOMMODULE5_ID'] : "";
                    $document->zoho_sign_document_name = isset($value['ZohoSign Documents Name']) ? $value['ZohoSign Documents Name'] : "";
                    $document->zoho_sign_document_owner = isset($value['ZohoSign Documents Owner']) ? $value['ZohoSign Documents Owner'] : "";
                    $document->email = isset($value['Email']) ? $value['Email'] : "";
                    $document->secondary_email = isset($value['Secondary Email']) ? $value['Secondary Email'] : "";
                    $document->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                    $document->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                    $document->contact = isset($value['Contact']) ? $value['Contact'] : "";
                    $document->JOBOPENINGID = isset($value['Job Opening_ID']) ? $value['Job Opening_ID'] : "";
                    $document->CANDIDATEID = isset($value['Candidate_ID']) ? $value['Candidate_ID'] : "";
                    $document->date_completed = isset($value['Date Completed']) ? $value['Date Completed']->format('Y-m-d') : "";
                    $document->date_declined = isset($value['Date Declined']) ? $value['Date Declined']->format('Y-m-d') : "";
                    $document->date_sent = isset($value['Date Sent']) ? $value['Date Sent']->format('Y-m-d') : "";
                    $document->decline_reason = isset($value['Declined Reason']) ? $value['Declined Reason'] : "";
                    $document->document_decline = isset($value['Document Deadline']) ? $value['Document Deadline'] : "";
                    $document->document_description = isset($value['Document Description']) ? $value['Document Description'] : "";
                    $document->document_status = isset($value['Document Status']) ? $value['Document Status'] : "";
                    $document->time_to_complete = isset($value['Time to complete']) ? $value['Time to complete'] : "";
                    $document->preview_or_position_signature = isset($value['Preview or Position Signature Fields']) ? $value['Preview or Position Signature Fields'] : "";
                    $document->email_opt_out = isset($value['Email Opt Out']) ? $value['Email Opt Out'] : "";
                    $document->currency = isset($value['Currency']) ? $value['Currency'] : "";
                    $document->recalled_reason = isset($value['Recalled Reason']) ? $value['Recalled Reason'] : "";
                    $document->declined_reason_zero = isset($value['Declined Reason0']) ? $value['Declined Reason0'] : "";
                    $document->zoho_signed_document_id_zero = isset($value['ZohoSign Document ID0']) ? $value['ZohoSign Document ID0'] : "";
                    $document->document_note_zero = isset($value['Document Note0']) ? $value['Document Note0'] : "";
                    $document->is_from_offer = isset($value['Is From Offer']) ? $value['Is From Offer'] : "";
                    $document->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d h:i:s') : "";
                    $document->others = isset($value['Others']) ? $value['Others'] : "";
                    $document->created_at = date('Y-m-d h:i:s', time());
                    $document->updated_at = date('Y-m-d h:i:s', time());
                    $document->save();

                    $attahements = $zohoQuery->downloadSignedAttachment($value['CUSTOMMODULE5_ID']);
                    if (count($attahements)) {
                        foreach ($attahements as $key1 => $value1) {
                            $isAttachementExists = Signeddocumentattachment::where('attachment_id', $value1['id'])->first();
                            if ($isAttachementExists != null) {
                                $signedAttachment = $isAttachementExists;
                            } else {
                                $signedAttachment = new Signeddocumentattachment();
                                /* Send notifiction to Admin */
                                $candidate = Candidates::where('CANDIDATEID', $document->CANDIDATEID)->first();
                                if ($candidate != null) {
                                    $notifications = new Notification;
                                    $notifications->s_id = $candidate->id;
                                    $notifications->sender_id = $candidate->CANDIDATEID;
                                    $notifications->message = "New signed document has been attached by " . $candidate->first_name . ' ' . $candidate->last_name;
                                    $notifications->type = "new_signed_document_attached";
                                    $notifications->to = "Admin";
                                    $notifications->save();
                                }
                            }
                            $signedAttachment->signed_document_id = $document->id;
                            $signedAttachment->attachment_id = $value1['id'];
                            $signedAttachment->filename = $value1['File Name'];
                            $signedAttachment->size = $value1['Size'];
                            $signedAttachment->modified_time = $value1['Modified Time']->format('Y-m-d h:i:s');
                            $signedAttachment->attach_by = $value1['Attached By'];
                            $signedAttachment->category = $value1['Category'];
                            $signedAttachment->created_at = date('Y-m-d h:i:s', time());
                            $signedAttachment->updated_at = date('Y-m-d h:i:s', time());
                            $signedAttachment->save();
                        }
                    }
                }
            }
        }
        $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'service_agreement')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        
        return;
    }
// test
    public function saveInterviews() {
        $zohoQuery = new ZohoqueryController();
        $data = $zohoQuery->getInterviews();
        if (count($data)) {
            foreach ($data as $page => $pageData) {
                foreach ($pageData as $key => $value) {
                    if (isset($value['CLIENTID']) && isset($value['JOBOPENINGID']) && isset($value['CANDIDATEID'])) {
                        $db_client = Clients::where('CLIENTID', $value['CLIENTID'])->first();
                        $db_job = Jobopenings::where('JOBOPENINGID', $value['JOBOPENINGID'])->first();
                        $db_candidate = Candidates::where('CANDIDATEID', $value['CANDIDATEID'])->first();

                        $isExistsInterview = Interviews::where([
                                    'CLIENTID' => $value['CLIENTID'],
                                    'JOBOPENINGID' => $value['JOBOPENINGID'],
                                    'CANDIDATEID' => $value['CANDIDATEID']
                                ])->first();
                        if ($isExistsInterview != null) {
                            $interview = $isExistsInterview;
                        } else {
                            $interview = new Interviews();
                        }
                        $interview->INTERVIEWID = isset($value['INTERVIEWID']) ? $value['INTERVIEWID'] : "";
                        $interview->interview_name = isset($value['Interview Name']) ? $value['Interview Name'] : "";
                        $interview->CLIENTID = (isset($value['CLIENTID'])) ? $value['CLIENTID'] : "";
                        $interview->JOBOPENINGID = (isset($value['JOBOPENINGID'])) ? $value['JOBOPENINGID'] : "";
                        $interview->CANDIDATEID = (isset($value['CANDIDATEID'])) ? $value['CANDIDATEID'] : "";
                        $interview->client_id = isset($db_client) ? $db_client->id : "";
                        $interview->type = (isset($value['Type'])) ? $value['Type'] : "";
                        $interview->job_id = (isset($db_job)) ? $db_job->id : 0;
                        $interview->candidate_id = (isset($db_candidate)) ? $db_candidate->id : 0;
                        $interview->Interviewers = isset($value['Interviewer(s)']) ? $value['Interviewer(s)'] : "";
                        $interview->start_datetime = isset($value['Start DateTime']) ? $value['Start DateTime']->format('Y-m-d H:i:s') : null;
                        $interview->end_datetime = isset($value['End DateTime']) ? $value['End DateTime']->format('Y-m-d H:i:s') : null;
                        $interview->venue = isset($value['Venue']) ? $value['Venue'] : "";
                        $interview->reminder = isset($value['Reminder']) ? $value['Reminder'] : "";
                        $interview->schedule_comment = isset($value['Schedule Comments']) ? $value['Schedule Comments'] : "";
                        $interview->last_activity_time = isset($value['Last Activity Time']) ? $value['Last Activity Time']->format('Y-m-d H:i:s') : null;
                        $interview->interview_status = isset($value['Interview Status']) ? $value['Interview Status'] : "";
                        $interview->interview_owner = isset($value['Interview Owner']) ? $value['Interview Owner'] : "";
                        $interview->created_by = isset($value['Created By']) ? $value['Created By'] : "";
                        $interview->modified_by = isset($value['Modified By']) ? $value['Modified By'] : "";
                        $interview->territory = isset($value['Territory']) ? $value['Territory'] : "";
                        $interview->others = isset($value['Interview Name']) ? $value['Interview Name'] : "";
                        $interview->ics = isset($value['ICS']) ? $value['ICS'] : "";
                        $interview->created_at = date('Y-m-d H:i:s', time());
                        $interview->updated_at = date('Y-m-d H:i:s', time());
                        $interview->save();

                        $interviewNotes = $zohoQuery->getInterviewNotes();
                        if (count($interviewNotes)) {
                            $this->saveInterviewNotes($interviewNotes, $interview->INTERVIEWID);
                        }
                    }
                }
            }
        }
        $cronLog = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'client_interview')->first();
        $cronLog->last_run = date('Y-m-d H:i:s', time());
        $cronLog->save();
        $cronLog1 = Cronlog::where('cron_name', 'zoho-to-aplikasi')->where('module_name', 'candidate_interview')->first();
        $cronLog1->last_run = date('Y-m-d H:i:s', time());
        $cronLog1->save();
        return;
    }

    public function saveInterviewNotes($interviewNotes, $interviewId) {
        foreach ($interviewNotes as $key1 => $value1) {
            $isExistsNote = Interviewnote::where([
                        'note_id' => $value1['id']
                    ])->first();
            if ($isExistsNote != null) {
                $note = $isExistsNote;
            } else {
                $note = new Interviewnote();
            }
            $note->INTERVIEWID = $interviewId;
            $note->note_id = isset($value1['id']) ? $value1['id'] : "";
            $note->title = isset($value1['Title']) ? $value1['Title'] : "";
            $note->note_content = isset($value1['Note Content']) ? $value1['Note Content'] : "";
            $note->owner_id = isset($value1['SMOWNERID']) ? $value1['SMOWNERID'] : "";
            $note->owner_name = isset($value1['Owner Name']) ? $value1['Owner Name'] : "";
            $note->created_id = isset($value1['SMCREATORID']) ? $value1['SMCREATORID'] : "";
            $note->created_by = isset($value1['Created By']) ? $value1['Created By'] : "";
            $note->created_time = isset($value1['Created Time']) ? $value1['Created Time']->format('Y-m-d H:i:s') : "";
            $note->modify_id = isset($value1['MODIFIEDBY']) ? $value1['MODIFIEDBY'] : "";
            $note->modify_by = isset($value1['Modified By']) ? $value1['Modified By'] : "";
            $note->modify_time = isset($value1['Modified Time']) ? $value1['Modified Time']->format('Y-m-d H:i:s') : "";
            $note->candidate_name = isset($value1['Candidate Name']) ? $value1['Candidate Name'] : "";
            $note->job_opening_name = isset($value1['Job Opening Name']) ? $value1['Job Opening Name'] : "";
            $note->client_name = isset($value1['Client Name']) ? $value1['Client Name'] : "";
            $note->contact_name = isset($value1['Contact Name']) ? $value1['Contact Name'] : "";
            $note->is_voice_note = isset($value1['ISVOICENOTES']) ? $value1['ISVOICENOTES'] : "";
            $note->created_at = date("Y-m-d H:i:s", time());
            $note->updated_at = date("Y-m-d H:i:s", time());
            $note->save();
        }
        return;
    }

}