<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Token;
use App\User;
use App\Clients;
use App\Jobopenings;
use App\Interviews;
use App\Candidates;
use App\Clientwishlist;
use App\Jobassociatecandidates;
use App\Attachment;
use App\Education;
use App\Experience;
use App\Contracthistory;
use App\Reference;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ZohoqueryController;

class Syncdatatozoho extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aplikasi:zoho';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Data syncing to zoho';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        echo "Cron is Running...\n";
        echo "Syncing clients...\n";
        $this->syncClientData();
        echo "Syncing job openings...\n";
        $this->syncJobsData();
//        echo "Syncing job associate candidates...\n";
//        $this->syncJobassociateCandidates();
//        echo "Syncing candidate profile requests...\n";
//        $this->syncCandidateProfileRequests();
        echo "Syncing candidates...\n";
        $this->syncCandidateData();
//        echo "Syncing candidate Attachments...\n";
//        $this->syncCandidateAttachments();
        echo "Cron Runs Successfully...";
    }

    /*
     * Syncing client data to zoho
     * 
     */

    public function syncClientData() {
        $clients = Clients::with('contacts')->where([
                    'is_synced' => 0
                ])->get();
        if (count($clients)) {
            $this->updateClientData($clients);
        }
        return;
    }

    public function updateClientData($clients) {
        $zohoQuery = new ZohoqueryController();
        foreach ($clients as $key => $value) {
            $data = [];
            $data['Client Name'] = (isset($value->client_name)) ? $value->client_name : "";
            $data['Contact Number'] = (isset($value->contact_number)) ? $value->contact_number : "";
            $data['Billing Street'] = (isset($value->billing_street)) ? $value->billing_street : "";
            $data['Billing City'] = (isset($value->billing_city)) ? $value->billing_city : "";
            $data['Billing State'] = (isset($value->billing_state)) ? $value->billing_state : "";
            $data['Billing Code'] = (isset($value->billing_code)) ? $value->billing_code : "";
            $data['Billing Country'] = (isset($value->billing_country)) ? $value->billing_country : "";
            $zohoQuery->updateClient($value->CLIENTID, $data);
            $value->is_synced = 1;
            $value->save();

            $this->updateContactData($value->contacts);
        }
        return;
    }
    public function updateContactData($contacts) {
        $zohoQuery = new ZohoqueryController();
        foreach ($contacts as $key => $value) {
            $data = [];
            $data['CONTACTID'] = (isset($value->CONTACTID)) ? $value->CONTACTID : "";
            $data['First Name'] = (isset($value->first_name)) ? $value->first_name : "";
            $data['Last Name'] = (isset($value->last_name)) ? $value->last_name : "";
            $data['Job Title'] = (isset($value->job_title)) ? $value->job_title : "";
            $data['Department'] = (isset($value->department)) ? $value->department : "";
            $data['Mobile'] = (isset($value->mobile)) ? $value->mobile : "";
            $data['Mailing City'] = (isset($value->mailing_city)) ? $value->mailing_city : "";
            $zohoQuery->updateClientContact($value->CONTACTID, $data);
            $value->is_synced = 1;
            $value->save();
        }
        return;
    }

    /*
     * Syncing job data to zoho 
     *  
     */

    public function syncJobsData() {
        $jobs = Jobopenings::with('client')->where([
//                    'CLIENTID' => '401682000002786001', // Remove this line
                    'is_synced' => 0,
                    'is_approve_by_admin' => 1,
                    'is_delete' => 0
                ])->get();
        if (count($jobs)) {
            $zohoQuery = new ZohoqueryController();
            foreach ($jobs as $key => $value) {
                $data = [];
                if ($value->contact_name != null) {
                    $data['Contact Name'] = $value->contact_name;
                }
                if(isset($value->client)){
                    $data['Client Name'] = $value->client->client_name;
                }
                $data['CLIENTID'] = $value->CLIENTID;
                $data['Posting Title'] = isset($value->posting_title) ? $value->posting_title : "";
                $data['Job Type'] = isset($value->job_type) ? $value->job_type : "";
                $data['Job Mode'] = isset($value->job_mode) ? $value->job_mode : "";
                $data['Date Opened'] = isset($value->date_opened) ? $value->date_opened : null;
                $data['City'] = isset($value->city) ? $value->city : "";
                $data['Job Duration (months)'] = isset($value->job_duration) ? $value->job_duration : "";
                $data['State'] = isset($value->state) ? $value->state : "";
                $data['Industry'] = isset($value->industry) ? $value->industry : "";
                $data['Job Base Rate'] = isset($value->job_base_rate) ? $value->job_base_rate : "";
                $data['Work Experience'] = isset($value->work_experience) ? $value->work_experience : "";
                $data['Key Skills'] = isset($value->key_skills) ? $value->key_skills : "";
                $data['Job Description'] = htmlspecialchars($value->all_description, ENT_COMPAT);
                // $data['Job Requirements'] = isset($value->job_requirements) ? htmlentities($value->job_requirements) : "";
                // $data['Job Benefits'] = isset($value->job_benefits) ? htmlentities($value->job_benefits) : "";
                $data['Category'] = isset($value->category) ? htmlentities($value->category) : "";
                $data['Actual Revenue'] = isset($value->actual_revenue) ? $value->actual_revenue : "";
                $data['Account Manager'] = isset($value->account_manager) ? $value->account_manager : "";
                $data['Target Date'] = isset($value->target_date) ? $value->target_date : "";
                if ($value->JOBOPENINGID != null) { // Update Job
                    $zohoQuery->updateJob($value->JOBOPENINGID, $data);
                } else { // Add Job
                    $response = $zohoQuery->addJob($data);
                    if (count($response)) {
                        $value->JOBOPENINGID = $response[0]['JOBOPENINGID'];
                        $value->Job_ID = $response[0]['Job Opening ID'];
                        /* Updating JOBOPENINGID in job associated candidates */
                        DB::table('jobassociate_candidates')->where('job_id', $value->id)->update([
                            'JOBOPENINGID' => $response[0]['JOBOPENINGID']
                        ]);
                    }
                }
                $value->is_synced = 1;
                $value->save();
            }
        }
        return;
    }

    /*
     * Syncing job associated candidates
     * 
     */

    public function syncJobassociateCandidates() {
        $candidates = Jobassociatecandidates::where([
                    'id' => 314, // Remove this line
                    'is_synced' => 0
                ])->whereNotNull('JOBOPENINGID')->whereNotNull('CANDIDATEID')->get();
        if (count($candidates)) {
            $zohoQuery = new ZohoqueryController();
            foreach ($candidates as $key => $value) {
                $data = [];
                $data['jobIds'] = $value->JOBOPENINGID;
                $data['candidateIds'] = $value->CANDIDATEID;
                if ($value->status == "Withdrawn") {
                    $status = "Application Withdrawn";
                } else {
                    $status = $value->status;
                }
                $data['status'] = $status;
                $zohoQuery->addAssociatedCandidateToJob($data);
                $value->is_synced = 1;
                $value->save();
            }
        }
        return;
    }

    /*
     * Syncing candidate profile requests
     * 
     */

    public function syncCandidateProfileRequests() {
        $requestedProfiles = Clientwishlist::with('candidate')->where([
                    'CLIENTID' => '401682000002786001', // Remove this line
                    'is_profile_requested' => 1,
                    'is_synced' => 0
                ])->get();
        if (count($requestedProfiles)) {
            $zohoQuery = new ZohoqueryController();
            foreach ($requestedProfiles as $key => $value) {
                $data = [];
                $data['Candidate ID'] = $value->candidate->candidate_id;
                $data['Link to Candidate'] = $value->candidate->first_name . ' ' . $value->candidate->last_name;
                $data['Territory'] = ($value->candidate->territory != null) ? $value->candidate->territory : "MY";
                $data['First Name'] = $value->candidate->first_name;
                $data['Last Name'] = $value->candidate->last_name;
                $data['Email'] = $value->candidate->email;
                $data['Telephone'] = $value->candidate->mobile_number;
                $data['Message'] = "For Profile Request";
                $data['Request Status'] = "Approved";
                $zohoQuery->createProfileRequest($data);
                $value->is_synced = 1;
                $value->save();
            }
        }
        return;
    }

    /*
     * Syncing candidate data
     * 
     */

    public function syncCandidateData() {
        $candidates = Candidates::where([
//                    'CANDIDATEID' => '401682000002785194', // Remove this line
                    'is_synced' => 0,
                    'is_delete' => 0
                ])->get();
        if (count($candidates)) {
            $zohoQuery = new ZohoqueryController();
            foreach ($candidates as $key => $value) {
                $data = [];
                $data['First Name'] = isset($value->first_name) ? $value->first_name : "";
                $data['Last Name'] = isset($value->last_name) ? $value->last_name : "";
                $data['Mobile'] = isset($value->mobile_number) ? $value->mobile_number : "";
                $data['SAP Job Title'] = isset($value->sap_job_title) ? $value->sap_job_title : "";
                $data['Experience in Years'] = isset($value->experience_in_years) ? $value->experience_in_years : 0;
                $data['Notice Period (Days)'] = isset($value->notice_period_days) ? $value->notice_period_days : 0;
                $data['Base Rate (BR)'] = isset($value->base_rate) ? $value->base_rate : "";
                $data['Reserved Base Rate'] = isset($value->reserved_base_rate) ? $value->reserved_base_rate : "";
                $data['Skill Set'] = isset($value->skill_set) ? $value->skill_set : "";
                $data['Certifications & Trainings'] = isset($value->certification_and_training) ? strip_tags($value->certification_and_training) : "";
                $data['Additional Info'] = isset($value->additional_info) ? htmlentities($value->additional_info) : "";
                $data['Category'] = isset($value->category) ? htmlentities(str_replace(",",";",$value->category)) : "";
                $data['Availability Date'] = isset($value->availability_date) ? $value->availability_date : null;
                $data['Willing to travel'] = ($value->willing_to_travel == 1) ? "true" : "false";
                $data['Full-time'] = ($value->full_time == 1) ? "true" : "false";
                $data['Part-time'] = ($value->part_time == 1) ? "true" : "false";
                $data['Project'] = ($value->project == 1) ? "true" : "false";
                $data['Support'] = ($value->support == 1) ? "true" : "false";
                $data['State'] = isset($value->state) ? $value->state : "";
                $data['Country'] = isset($value->country) ? $value->country : "";

                $zohoQuery->updateCandidate($value->CANDIDATEID, $data);
                /* Updating resume */
                $zohoQuery->updateAttachment($value->CANDIDATEID, $value->resume, "Resume");
                $value->is_synced = 1;
                $value->save();
            }
        }
        return;
    }

    public function createExperienceXMLData($experience) {
        $xml = '<Candidates><FL val="Experience Details">';
        foreach ($experience as $key => $value) {
            $xml .= '';
            if ($value->is_currently_working_here == "true") {
                $xml .= '<TR no="' . $key . '">' .
                        '<TL val="Occupation / Title">' . $value->occupation . '</TL>' .
                        '<TL val="Company">' . $value->company . '</TL>' .
                        '<TL val="Industry">' . $value->industry . '</TL>' .
                        '<TL val="Summary">' . $value->summary . '</TL>' .
                        '<TL val="Work Duration_From">' . $value->work_duration . '</TL>' .
                        '<TL val="I currently work here">true</TL>' .
                        '</TR>';
            } else {
                $xml .= '<TR no="' . $key . '">' .
                        '<TL val="Occupation / Title">' . $value->occupation . '</TL>' .
                        '<TL val="Company">' . $value->company . '</TL>' .
                        '<TL val="Industry">' . $value->industry . '</TL>' .
                        '<TL val="Summary">' . $value->summary . '</TL>' .
                        '<TL val="Work Duration_From">' . $value->work_duration . '</TL>' .
                        '<TL val="Work Duration_To">' . $value->work_duration_to . '</TL>' .
                        '</TR>';
            }
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }

    public function createEducationXMLData($education) {
        $xml = '<Candidates><FL val="Educational Details">';
        foreach ($education as $key => $value) {
            $xml .= '';
            if ($value->currently_pursuing == "true") {
                $xml .= '<TR no="' . $key . '">' .
                        '<TL val="Institute / School">' . $value->institute . '</TL>' .
                        '<TL val="Major / Department">' . $value->department . '</TL>' .
                        '<TL val="Degree">' . $value->degree . '</TL>' .
                        '<TL val="Duration_From">' . $value->duration_from . '</TL>' .
                        '<TL val="Currently pursuing">true</TL>' .
                        '</TR>';
            } else {
                $xml .= '<TR no="' . $key . '">' .
                        '<TL val="Institute / School">' . $value->institute . '</TL>' .
                        '<TL val="Major / Department">' . $value->department . '</TL>' .
                        '<TL val="Degree">' . $value->degree . '</TL>' .
                        '<TL val="Duration_From">' . $value->duration_from . '</TL>' .
                        '<TL val="Duration_To">' . $value->duration_to . '</TL>' .
                        '</TR>';
            }
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }

    public function createReferenceXMLData($references) {
        $xml = '<Candidates><FL val="References">';
        foreach ($references as $key => $value) {
            $xml .= '';
            $xml .= '<TR no="' . $key . '">' .
                    '<TL val="Reference Name">' . $value->name . '</TL>' .
                    '<TL val="Reference Position">' . $value->position . '</TL>' .
                    '<TL val="Reference Company">' . $value->company . '</TL>' .
                    '<TL val="Reference Phone no.">' . $value->phone . '</TL>' .
                    '<TL val="Reference Email">' . $value->email . '</TL>' .
                    '</TR>';
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }

    /*
     * Syncing candidate attachments
     * 
     */

    public function syncCandidateAttachments() {
        $attachments = Attachment::where([
                    'zoho_id' => '401682000002785194', // Remove this line
                    'is_synced' => 0
                ])->get();
        if (count($attachments)) {
            $zohoQuery = new ZohoqueryController();
            foreach ($attachments as $key => $value) {
                $response = $zohoQuery->updateAttachment($value->zoho_id, $value->filename, "Others");
                if (count($response)) {
                    $value->attachment_id = $response['response']['result']['recorddetail']['FL'][0]['content'];
                    $value->is_synced = 1;
                    $value->save();
                }
            }
        }
        return;
    }

}