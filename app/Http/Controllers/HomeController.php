<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PulkitJalan\GeoIP\GeoIP;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller {
    /*
     * Dashboard
     * 
     */

    public function index(Request $request) {
        if (isset($request->locale) && $request->locale != null) {
            $request->session()->put('territory', $request->locale);
        } else {
            $request->session()->put('territory', "MY");
        }
        if ($request->locale == "MY" || $request->locale == "PH") {
            return view('home');
        } else {
            URL::defaults(['locale' => 'MY']);
            return view('loader');
        }
    }

    public function setTerritories(Request $request) {
        $request->session()->put('territory', $request->territory);
        return redirect('/' . $request->territory);
    }

    public function checkCountry(Request $request) {
        // $geoip = new GeoIP();
        // $country = $geoip->getCountry();
        // if ($country == "Philippines") {
        //     $request->session()->put('territory', "PH");
        // } else {
//            $request->session()->put('territory', "MY");
// 
//        return redirect('/' . $request->session()->get('territory'));
        $territory = $request->session()->get('territory');
        if ($request->session()->get('remoteip') == null) {
            $request->session()->put('remoteip', $_SERVER['REMOTE_ADDR']);
        }
        if ($territory != null && ($request->session()->get('remoteip') == $_SERVER['REMOTE_ADDR'])) {
            if ($territory == "MY" || $territory == "PH") {
                return redirect('/' . $territory);
            } else {
                URL::defaults(['locale' => 'MY']);
                return view('loader');
            }
        } else {
            URL::defaults(['locale' => 'MY']);
            return view('loader');
        }
    }

    public function loginRedirect(Request $request) {
        $redirectUrl = $request->session()->get('redirecturl');
        if ($redirectUrl != "" || $redirectUrl != null) {
            return redirect($redirectUrl);
        }
        if (Auth::user()->role_id == 1) {
            return redirect()->route('client-home');
        } else {
            return redirect()->route('consultant-home');
        }
    }

}
