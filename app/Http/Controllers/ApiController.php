<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZCRMRestClient;
use ZohoOAuth;
use ZCRMModule;
use ZCRMOrganization;
use Zohotestclass;
use Humantech\Zoho\Recruit\Api\Client\AuthenticationClient;
use Humantech\Zoho\Recruit\Api\Client\Client;

class ApiController extends Controller {
    /*
     *
     * Test API Call
     *
     */

//    public function test() {
//        $config = [
//            'client_id' => '1000.2850LS3JLQER72090PPDWEP6T3HUHU',
//            'client_secret' => 'e126fe46bf35bff6d328a6d47d7b09af7020bd0c80',
//            'redirect_uri' => 'https://www.aipxperts.com',
//            'accounts_url' => 'https://accounts.zoho.com',
//            'token_persistence_path' => '/var/www/html/aplikasi/public/',
//            'currentUserEmail' => 'talent@aplikasi.us'
//        ];
//        $zohoCrmClient = new ZCRMRestClient();
//        $zohoCrmClient->initialize($config);
//
//        $zcrmModuleIns = ZCRMModule::getInstance("Leads");
//        $bulkAPIResponse = $zcrmModuleIns->getRecords();
//        $recordsArray = $bulkAPIResponse->getData(); // $recordsArray - array of ZCRMRecord instances
//        echo "<pre>";
//        print_r($recordsArray);
////        exit;
//        if($recordsArray != null){
//            foreach ($recordsArray as $key => $value) {
////                echo $value->
//                echo "<pre>";
//                print_r($value->getFieldValue('Company'));
//            }
//        }
//    }
    public function test() {
//        $authClient = new AuthenticationClient();
//        $token = $authClient->generateAuthToken('talent@aplikasi.us', 'aplikasi123');
        // Note : This token is user specific and it is permanent
        $token = "e21643e62b9d0f2d98e8e50db83e0f12";
        $client = new Client($token);
//        $data = [
//            'First Name' => "Test Ramesh",
//            'Last Name' => "Test Chudasama",
//            'Email' => 'ramesh.chudasama@aplikasi.us',
//            'Phone' => '9898989898',
//            'Current Job Title' => "Test Ramesh SAP"
//        ];
//        $jobOpenings = $client->addRecords('Candidates', $data);

        /* Search Record By Email */
        $getRecordByEmail = $client->getSearchRecords(
                'Candidates', 'Candidates(Candidate ID,First Name,Last Name,Email,Mobile)', '(Email|contains|*ramesh.chudasama@aplikasi.us*)'
        );
        $file = public_path('/images/advertise.png');
         /* Document upload is only avaialble in Paid version of account */
        if (count($getRecordByEmail)) {
            if (isset($getRecordByEmail[0]['CANDIDATEID'])) {
                print_r($client->uploadFile(
                                $getRecordByEmail[0]['CANDIDATEID'], 'Others',"$file"
                ));
            }
        }
        exit;
    }

}
