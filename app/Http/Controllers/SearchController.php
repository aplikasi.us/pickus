<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Jobopenings;
use Illuminate\Support\Facades\DB;
use App\Tagline;
use App\Candidates;
use App\User;
use App\States;
use App\Cities;
use App\Addcompare;
use Illuminate\Support\Facades\Crypt;
use App\Emailtemplate;
use Illuminate\Support\Facades\Mail;
use CURLFile;
use apora\Zoho\Recruit\Client\Php;
use App\Clients;

class SearchController extends Controller
{
    // job search section

    public function index(Request $request) {
        $search_type = $request->search_type;
        
        $jobsearchterm = $job_type = $job_type2 = $job_baserate = $job_baserate_from = $job_baserate_to = $job_baserate2_from = $job_baserate2_to = $work_experience = $work_experience2 = $job_start_date = $job_start_date2 = $job_duration = $job_duration2 = $job_industry = $job_industry2 = "";
        $jobcategory = [];
        $jobcategory2 = [];
        $job_mode1 = [];
        $job_mode2 = [];
        $job_state = [];
        $job_state2 = [];
        if(!isset($request->jobsearchterm)){
            return ;
        }
        $query = Jobopenings::with(['client', 'contact', 'jobopeningcategory', 'associatecandidates'])
            ->whereHas('contact', function ($query) {
                $query->where('is_delete', 0);
            });
            
            // ->whereHas('associatecandidates', function($q) {
            //     $q->where('CANDIDATEID', 401682000002785194);
            // });

        // $query = Jobopenings::with(['client', 'contact', 'jobopeningcategory', 'associatecandidates']);

        /* Category */
        if ((isset($request->jobcategory) && !empty($request->jobcategory)) || (isset($request->job_category2) && !empty($request->job_category2))) {
            $jobcategory = $request->jobcategory;
            $jobcategory2 = $request->job_category2;
            if (!empty($jobcategory2)) {
                // $query = $query->whereIn('category', $request->job_category2);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory2) {
                    $query = $query->whereIn('category', $jobcategory2);
                });
            } else {
                $jobcategory2 = $jobcategory;
                // $query = $query->whereIn('category', $request->jobcategory);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory) {
                    $query = $query->whereIn('category', $jobcategory);
                });
            }
        }
        if (isset($request->jobsearchterm) && $request->jobsearchterm != "") {
            $jobsearchterm = $request->jobsearchterm;
            $query = $query->where(function ($query2) use ($jobsearchterm) {
                $query2->where('Job_ID', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('posting_title', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('key_skills', 'LIKE', "%" . $jobsearchterm . "%");
            });
        }
        /* Job Mode */
        if ((isset($request->job_mode1) && !empty($request->job_mode1)) || (isset($request->job_mode2) && !empty($request->job_mode2))) {
            $job_mode1 = $request->job_mode1;
            $job_mode2 = $request->job_mode2;
            if (!empty($job_mode2)) {
                $query = $query->whereIn('job_mode', $job_mode2);
            } else {
                $job_mode2 = $job_mode1;
                $query = $query->whereIn('job_mode', $job_mode1);
            }
        }
        /* Job Type */
        if ((isset($request->job_type) && $request->job_type != "") || isset($request->job_type2) && $request->job_type2 != "") {
            $job_type = $request->job_type;
            $job_type2 = $request->job_type2;
            if ($job_type2 != null) {
                $query = $query->whereIn('job_type', $request->job_type2);
            } else {
                $job_type2 = $job_type;
                $query = $query->whereIn('job_type', $request->job_type);
            }
        }
        /* Job Base Rate */
        if ((isset($request->job_baserate_from) || isset($request->job_baserate_to)) || (isset($request->job_baserate2_from) || isset($request->job_baserate2_to))) {
            $job_baserate_from = $request->job_baserate_from;
            $job_baserate_to = $request->job_baserate_to;
            $job_baserate2_from = $request->job_baserate2_from;
            $job_baserate2_to = $request->job_baserate2_to;

            if ($job_baserate2_from != null && $job_baserate2_to != null) {
                $query = $query->whereBetween('job_base_rate', [$job_baserate2_from, $job_baserate2_to]);
            } else if ($job_baserate_from != null && $job_baserate_to != null) {
                $job_baserate2_from = $job_baserate_from;
                $job_baserate2_to = $job_baserate_to;
                $query = $query->whereBetween('job_base_rate', [$job_baserate_from, $job_baserate_to]);
            } else if ($job_baserate2_from != null) {
                $query = $query->where('job_base_rate', '>=', $job_baserate2_from);
            } else if ($job_baserate2_to != null) {
                $query = $query->where('job_base_rate', '<=', $job_baserate2_to);
            } else if ($job_baserate_from != null) {
                $job_baserate2_from = $job_baserate_from;
                $query = $query->where('job_base_rate', '>=', $job_baserate_from);
            } else if ($job_baserate_to != null) {
                $job_baserate2_to = $job_baserate_to;
                $query = $query->where('job_base_rate', '<=', $job_baserate_to);
            }
        }

        /* Work Experience */
        if ((isset($request->work_experience) && $request->work_experience != "") || isset($request->work_experience2)) {
            $work_experience = $request->work_experience;
            $work_experience2 = $request->work_experience2;
            if ($work_experience2 != null) {
                if ($work_experience2 == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience2 == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience2 == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience2 == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            } else {
                $work_experience2 = $work_experience;
                if ($work_experience == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            }
        }
        /* Start Date */
        if ((isset($request->job_start_date) && $request->job_start_date != "") || isset($request->job_start_date2)) {
            $job_start_date = $request->job_start_date;
            $job_start_date2 = $request->job_start_date2;
            if ($job_start_date2 != null) {
                if ($job_start_date2 == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date2 == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            } else {
                $job_start_date2 = $job_start_date;
                if ($job_start_date == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            }
        }
        /* Duration */
        if ((isset($request->job_duration) && $request->job_duration != "") || isset($request->job_duration2)) {
            $job_duration = $request->job_duration;
            $job_duration2 = $request->job_duration2;
            if ($job_duration2 != null) {
                if ($job_duration2 == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration2 == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration2 == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            } else {
                $job_duration2 = $job_duration;
                if ($job_duration == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            }
        }
        /* Industry */
        if ((isset($request->job_industry) && $request->job_industry != "") || isset($request->job_industry2)) {
            $job_industry = $request->job_industry;
            $job_industry2 = $request->job_industry2;
            if ($job_industry2 != null) {
                $query = $query->where('industry', $job_industry2);
            } else {
                $job_industry2 = $job_industry;
                $query = $query->where('industry', $job_industry);
            }
        }
        /* State */
        if ((isset($request->job_state) && !empty($request->job_state)) || (isset($request->job_state2) && !empty($request->job_state2))) {
            $job_state = $request->job_state;
            $job_state2 = $request->job_state2;
            if (!empty($job_state2)) {
                $query = $query->whereIn('state', $job_state2);
            } else {
                $job_state2 = $job_state;
                $query = $query->whereIn('state', $job_state);
            }
        }
        /* Teritorries */
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }
        $jobs = $query->where([
            'job_opening_status' => "In-progress",
            'publish_in_us' => 1,
            'block' => 0,
            'is_delete' => 0
        ])->get();
        $total = count($jobs);

        $displayJobMightLike = 1;
        if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
                $displayJobMightLike = 0;
            }
            $jobsMightLike = [];
            if (Auth::user()->role_id == 2) {
                /* Jobs you might Like */
                $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                $categoryArray[] = "";
                $categoryArray = explode(';', $candidateDetailarray->category);
                $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
                $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
            }
        } else {
            $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
        }
        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];

        // return view('general-search-job', compact('displayJobMightLike', 'jobsMightLike', 'jobs', 'search_type', 'jobcategory', 'jobcategory2', 'jobsearchterm', 'job_mode1', 'job_mode2', 'job_type', 'job_type2', 'job_baserate_from', 'job_baserate_to', 'job_baserate2_from', 'job_baserate2_to', 'work_experience', 'work_experience2', 'job_start_date', 'job_start_date2', 'job_duration', 'job_duration2', 'job_industry', 'job_industry2', 'job_state', 'job_state2', 'candidateScreeningStatus', 'total'));

        
    

        return $jobs;
    }

    public function fetchState(){

        /* Teritorries */
        if (Session::get('territory') != null) {
            $territory = $query->where('territory', Session::get('territory'));
        }
        else
            $territory = "MY";

        $stateList = DB::table('states')->where('country_name', $territory)->get();

        return $stateList;

    }

    public function fetchCity(){

         /* Teritorries */
         if (Session::get('territory') != null) {
            $territory = $query->where('territory', Session::get('territory'));
        }
        else
            $territory = "MY";


        if($territory == "PH"){
                return DB::table('cities')->where('id', '>=', '19')->get();

        } 
        else    
        return DB::table('cities')->where('id', '<', '19')->get();
        

         

    }
    public function fetchIndustry(){

        $industryList = DB::table('industries')->get();

        return $industryList;

    }

    public function tips(){

        $tips = Tagline::where('category', 'Tips')->orderBy(\DB::raw('RAND()'))->first();

        return $tips;

    }

    public function doYouKnow(){
        $doyouknow = Tagline::where('category', 'Facts')->orderBy(\DB::raw('RAND()'))->first();

        return $doyouknow;
    }

    public function indexConsultant(Request $request)
    {
        $search_type = $request->search_type;
        $query = Candidates::with(['attachments', 'comparison', 'wishlist', 'profileRequests']);
        $searchterm = $available = $willing_to_travel = $full_time = $part_time = $project = $support = $job_title_matching = $baserate_from = $baserate_to = $baserate2_from = $baserate2_to = $avalabilityDate = $availability_date2 = $experience = $experience2 = "";
        $category = [];
        $category2 = [];
        $workpreference = [];
        $state = [];

        if(!isset($request->searchterm)){
            return ;
        }

        /* Category */
        if ((isset($request->category) && !empty($request->category)) || isset($request->category2)) {
            $category = $request->category;
            $category2 = $request->category2;
            if (!empty($category2)) {
                $query = $query->whereNotNull('category');
                // $query = $query->whereIn('category',$category2);
                $query = $query->whereHas('candidatecategory', function ($query) use ($category2) {
                    $query = $query->whereIn('category', $category2);
                });
                // $category = $category2;
            } else {
                $category2 = $category;
                $query = $query->whereNotNull('category');
                // $query = $query->whereIn('category',$category);
                $query = $query->whereHas('candidatecategory', function ($query) use ($category) {
                    $query = $query->whereIn('category', $category);
                });
            }
        }
        /* General */

        /* Available */
        if (isset($request->available)) {
            $available = "Yes";
            $query = $query->where('available_for_contract', 1);
        }

        /* Project */
        if (isset($request->project)) {
            $project = "Yes";
            $query = $query->where('project', 1);
        }
        /* Support */
        if (isset($request->support)) {
            $support = "Yes";
            $query = $query->where('support', 1);
        }
        /* Keyword Matching Job Title Only */
        if (isset($request->job_title_matching)) {
            $searchterm = $request->searchterm;
            $job_title_matching = "Yes";
            $query = $query->where('sap_job_title', 'LIKE', "%" . $searchterm . "%");
        } else if (isset($request->searchterm) && $request->searchterm != "") {
            //General search
            $searchterm = $request->searchterm;
            $query = $query->where(function ($query) use ($searchterm) {
                $query = $query->where('skill_set', 'LIKE', "%" . $searchterm . "%");
                $query = $query->orWhere('sap_job_title', 'LIKE', "%" . $searchterm . "%");
                $query = $query->orWhere('candidate_id', 'LIKE', "%" . $searchterm . "%");
            });
        }

        /* Base Rate */
        if ((isset($request->baserate_from) || isset($request->baserate_to)) || (isset($request->baserate2_from) || isset($request->baserate2_to))) {

            $baserate_from = $request->baserate_from;
            $baserate_to = $request->baserate_to;
            $baserate2_from = $request->baserate2_from;
            $baserate2_to = $request->baserate2_to;

            if ($baserate2_from != null && $baserate2_to != null) {
                $query = $query->whereBetween('base_rate', [$baserate2_from, $baserate2_to]);
                // $baserate_from = '';
                // $baserate_to = '';
            } else if ($baserate_from != null && $baserate_to != null) {
                $baserate2_from = $baserate_from;
                $baserate2_to = $baserate_to;
                $query = $query->whereBetween('base_rate', [$baserate_from, $baserate_to]);
            } else if ($baserate2_from != null) {
                // $baserate_from = '';
                // $baserate_to = '';
                $query = $query->where('base_rate', '>=', $baserate2_from);
            } else if ($baserate2_to != null) {
                // $baserate_from = '';
                // $baserate_to = '';
                $query = $query->where('base_rate', '<=', $baserate2_to);
            } else if ($baserate_from != null) {
                $baserate2_from = $baserate_from;
                $query = $query->where('base_rate', '>=', $baserate_from);
            } else if ($baserate_to != null) {
                $baserate2_to = $baserate_to;
                $query = $query->where('base_rate', '<=', $baserate_to);
            }
        }


        /* Availability Date */
        if (isset($request->availability_date) || isset($request->availability_date2)) {
            $avalabilityDate = $request->availability_date;
            $availability_date2 = $request->availability_date2;
            if ($availability_date2 != null) {
                if ($availability_date2 == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($availability_date2 == "month") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($availability_date2 == "three") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+90 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($availability_date2 == "six") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+180 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+365 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                }
            } else {
                $availability_date2 = $avalabilityDate;
                if ($avalabilityDate == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($avalabilityDate == "month") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($avalabilityDate == "three") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+90 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($avalabilityDate == "six") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+180 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+365 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                }
            }
        }

        /* Experience */
        if (isset($request->experience) || isset($request->experience2)) {
            $experience = $request->experience;
            $experience2 = $request->experience2;
            if ($experience2 != null) {
                if ($experience2 == "0") {
                    $query = $query->where('experience_in_years', 0);
                } else if ($experience2 == "3") {
                    $query = $query->whereBetween('experience_in_years', [1, 3]);
                } else if ($experience2 == "5") {
                    $query = $query->whereBetween('experience_in_years', [4, 5]);
                } else if ($experience2 == "10") {
                    $query = $query->whereBetween('experience_in_years', [6, 10]);
                } else {
                    $query = $query->where('experience_in_years', '>', 10);
                }
            } else {
                $experience2 = $experience;
                if ($experience == "0") {
                    $query = $query->where('experience_in_years', 0);
                } else if ($experience == "3") {
                    $query = $query->whereBetween('experience_in_years', [1, 3]);
                } else if ($experience == "5") {
                    $query = $query->whereBetween('experience_in_years', [4, 5]);
                } else if ($experience == "10") {
                    $query = $query->whereBetween('experience_in_years', [6, 10]);
                } else {
                    $query = $query->where('experience_in_years', '>', 10);
                }
            }
        }
        /* Work Preferences */
        if (isset($request->willing_to_travel) || isset($request->full_time) || (isset($request->part_time))) {

            /* Willing to Travel */
            if (isset($request->willing_to_travel)) {
                $willing_to_travel = "Yes";
                $query = $query->where('willing_to_travel', 1);
            }
            /* full time */
            if (isset($request->full_time)) {
                $full_time = "Yes";
                $query = $query->where('full_time', 1);
            }
            /* Part Time */
            if (isset($request->part_time)) {
                $part_time = "Yes";
                $query = $query->where('part_time', 1);
            }
        } else if (isset($request->work_preference) && !empty($request->work_preference)) {
            $workpreference = $request->work_preference;
            if (in_array('Full Time', $workpreference)) {
                $full_time = "Yes";
                $query = $query->where('full_time', 1);
            }
            if (in_array("Part Time", $workpreference)) {
                $part_time = "Yes";
                $query = $query->where('part_time', 1);
            }
            if (in_array("Willing to travel", $workpreference)) {
                $willing_to_travel = "Yes";
                $query = $query->where('willing_to_travel', 1);
            }
        }
        /* State */
        if (isset($request->state) && !empty($request->state)) {
            $state = $request->state;
            $query = $query->whereIn('state', $state);
        }
        /* Teritorries */
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }

        $query = $query->where('block', 0)->where('internal_hire', 0)->where('publish_in_us', 1)->where('is_delete', 0)->whereNotIn('candidate_status', ['Unqualified', 'Qualified Expat', 'Unqualified Expat']);

        return $query->get();
        $candidates = $query->paginate(15);

        $total = $candidates->total();
        $candidates->appends([
            'search_type' => $search_type,
            'category' => $category,
            'category2' => $category2,
            'searchterm' => $searchterm,
            'available' => $available,
            'willing_to_travel' => $willing_to_travel,
            'full_time' => $full_time,
            'part_time' => $part_time,
            'project' => $project,
            'support' => $support,
            'job_title_matching' => $job_title_matching,
            'baserate_from' => $baserate_from,
            'baserate_to' => $baserate_to,
            'baserate2_from' => $baserate2_from,
            'baserate2_to' => $baserate2_to,
            'avalabilityDate' => $avalabilityDate,
            'availability_date2' => $availability_date2,
            'experience' => $experience,
            'experience2' => $experience2,
            'workpreference' => $workpreference,
            'state' => $state,
            'total' => $total

        ])->render(); 
        $wishlist = [];
        if (Auth::check()) {
            $wishlist = Clientwishlist::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->pluck('CANDIDATEID')->toArray();
        }
        $addTocompare = [];
        if (Auth::check()) {
            $addTocompare = Addcompare::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->pluck('CANDIDATEID')->toArray();
        }
        $user = Auth::user();

        if(!isset($user)) {
            $user = json_encode( (object) ['role_id' => 0] );
        }


        return view('search-result-consultant', compact('user', 'candidates', 'category', 'category2', 'searchterm', 'search_type', 'available', 'willing_to_travel', 'full_time', 'part_time', 'project', 'support', 'job_title_matching', 'wishlist', 'baserate_from', 'baserate_to', 'baserate2_from', 'baserate2_to', 'avalabilityDate', 'availability_date2', 'experience', 'experience2', 'workpreference', 'state', 'addTocompare', 'total'));
    }

    public function getTotalCompare(Request $request) {
        $user = User::findOrFail($request->userId);

        $addTocompare = Addcompare::where([
            'CLIENTID' => $user->zoho_id,
            'contact_id' => $user->contact_tbl_id,
        ])->get();

        return $addTocompare;
    }

    // zoho account auto register laravel system 
    public function createUserClient(Request $request){

            $user = new User();
            // check
            $checkExist = $user::where('email',$request->email)->get();

            if($checkExist->isEmpty()){
                $user->role_id = $request->role_id;
                $user->id_zoho = $request->id_zoho;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->company_name = $request->company_name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->sap_job_title = $request->job_title;
                $user->hear = $request->hear;
                $user->territory = $request->territory;
                $user->created_at = date("Y-m-d H:i:s", time()); 
                $user->save();
                $this->sendRegistrationEmail($user);
                return "200";
            }
            else{
                DB::table('error_log')->insert([
                    ['operation_type' => 'create client',
                     'affected_email'=> $request->email ,
                     'issue' => 'email exist',
                     'date-time'=> date("Y-m-d H:i:s", time()),
                    ]
                ]);
                return "error";
                
            }
            
    }

    public function sendRegistrationEmail($userObject)
            {
                $id = Crypt::encryptString($userObject->id);
                $territory = $userObject->territory;
                $template = EmailTemplate::where('name', 'generate_password')->first();
                if ($template != null) {
                    $emaildata['username'] = $userObject->first_name . ' ' . $userObject->last_name;
                    $emaildata['email'] = $userObject->email;
                    $emaildata['link'] =  "localhost/aplikasi-laravel/".$territory."/user/generate-password/".$id; //please tkar ni to dynamic 
                    
                    Mail::send([], [], function ($messages) use ($template, $emaildata) {
                        $messages->to($emaildata['email'])
                            ->subject($template->subject)
                            ->setBody($template->parse($emaildata), 'text/html');
                    });
                    DB::table('users')
                        ->where('id', "=", $userObject->id)
                        ->update([
                            'reset_token' => date('Y-m-d h:i:s', time()),
                            'updated_at' => date('Y-m-d h:i:s', time())
                        ]);
                }
                return;
            }
            
            public function AssignIdToZoho($laravel_id, $zohoId, $territory){
               
                // territory checking 
                
                    $zohoAuthToken = DB::table('token')
                    ->where('territory','=',$territory)
                    ->value('token');
                     
                    if(!isset($zohoAuthToken)){
                        DB::table('error_log')->insert([
                            ['operation_type' => 'fetch zoho key',
                             'affected_email'=> "null" ,
                             'issue' =>  "no zoho key in DB",
                             'date-time'=> date("Y-m-d H:i:s", time()),
                            ]
                        ]); 
                        return;
                    }
                
                
                // start zoho call operation
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://recruit.zoho.com/recruit/private/json/Contacts/updateRecords?authtoken=". $zohoAuthToken ."&scope=recruitapi&version=2&id=". $zohoId . "&xmlData=%3CContacts%3E%0A%3Crow%20no=%221%22%3E%0A%3CFL%20val=%22Laravel%20ID%22%3E".$laravel_id ."%3C/FL%3E%0A%3C/row%3E%0A%3C/Contacts%3E",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,    
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Accept-Encoding: gzip, deflate",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Content-Length: 0",
                    "Content-Type: application/xml",
                    "Cookie: b122402a95=355551aa8f4be42a19a0c7a7c6d99e81; crmcsr=252b0cf4-a0b7-4955-bb77-1db980bf01f9",
                    "Host: recruit.zoho.com",
                    "Postman-Token: bd601b8e-41ad-4916-9412-82996151a187,e98ce024-156e-4112-8fd1-4585d0922214",
                    "User-Agent: PostmanRuntime/7.17.1",
                    "cache-control: no-cache"
                ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                
                curl_close($curl);

                // error log for failed operation
                if(!$response){
                    DB::table('error_log')->insert([
                            ['operation_type' => 'test zoho call using background process',
                             'affected_email'=> "null" ,
                             'issue' =>  $response,
                             'date-time'=> date("Y-m-d H:i:s", time()),
                            ]
                        ]); 
                        echo "cURL Error #:" . $err;
                }
                else {
                    return $response;
                }
            }


            // public function testfunction(Request $request){

            //     $Candidates = Candidates::where('CANDIDATEID', $request->laravel_id)->first();
            //     $user = User::where('zoho_id', $request->laravel_id)->first();
                
            //     if(isset($user) || isset($Candidates)){

            //         // first name and last name split and merge
            //         $temp = explode(" ",$request->candidate_name);
            //         $lastIndex = count($temp);
            //         $last_name = $temp[$lastIndex-1];
            //         $popLastIndex = array_pop($temp);
            //         $first_name = join($temp);

            //         // edit candidate n users table 
            //         if (isset($request->candidate_name) && !empty($request->candidate_name)) {
            //             $Candidates->first_name = $first_name;
            //             $Candidates->last_name = $last_name;
            //             $user->first_name = $first_name;
            //             $user->last_name = $last_name;
            //         }
                   
            //         if (isset($request->sap_job_title) && !empty($request->sap_job_title)) {
            //             $Candidates->sap_job_title = $request->sap_job_title;
            //             $user->sap_job_title = $request->sap_job_title;
            //         }
            //         if (isset($request->experience_in_years) && !empty($request->experience_in_years)) {
            //             $Candidates->experience_in_years = $request->experience_in_years;
            //         }
            //         if (isset($request->category) && !empty($request->category)) {
            //             $Candidates->category = $request->category;
            //         }
            //         if (isset($request->mobile_number) && !empty($request->mobile_number)) {
            //             $Candidates->mobile_number = $request->mobile_number;
            //             $user->phone = $request->mobile_number;
            //         }
            //         if (isset($request->state) && !empty($request->state)) {
            //             $Candidates->state = $request->state;
            //         }
            //         if (isset($request->country) && !empty($request->country)) {
            //             $Candidates->country = $request->country;
            //         }
            //         if (isset($request->city) && !empty($request->city)) {
            //             $Candidates->city = $request->city;
            //         }
            //         if (isset($request->additional_info) && !empty($request->additional_info)) {
            //             $Candidates->additional_info_2 = $request->additional_info;
            //         }
            //         if (isset($request->pay_rate) && !empty($request->pay_rate)) {
            //             $Candidates->base_rate = $request->pay_rate;
            //         }
            //         if (isset($request->notice_period) && !empty($request->notice_period)) {
            //             $Candidates->notice_period_days = $request->notice_period;
            //         }
            //         if (isset($request->skill_set) && !empty($request->skill_set)) {
            //             $Candidates->skill_set = $request->skill_set;
            //         }
            //         if (isset($request->certification_and_training) && !empty($request->certification_and_training)) {
            //             $Candidates->certification_and_training = $request->certification_and_training;
            //         }
            //         if (isset($request->fulltime) && !empty($request->fulltime)) {
            //             $Candidates->full_time = $request->fulltime;
            //         }
            //         if (isset($request->parttime) && !empty($request->parttime)) {
            //             $Candidates->part_time = $request->parttime;
            //         }
            //         if (isset($request->willingtravel) && !empty($request->willingtravel)) {
            //             $Candidates->willing_to_travel = $request->willingtravel;
            //         }
            //         if (isset($request->project) && !empty($request->project)) {
            //             $Candidates->project = $request->project;
            //         }
            //         if (isset($request->support) && !empty($request->support)) {
            //             $Candidates->support = $request->support;
            //         }
            //         if (isset($request->datevalue) && !empty($request->datevalue)) {
            //             $Candidates->availability_date = $request->datevalue;
            //         }
            //         //time operation happen 
            //         $user->updated_at = date('Y-m-d H:i:s', time());
            //         $Candidates->is_synced = 1;
            //         $Candidates->updated_at =  date('Y-m-d H:i:s', time());
            //         $Candidates->save();
            //         $user->save();

            //         return response()->json([
            //             'status' => 200
            // ]);
            //     }
            //     else
            //     {
            //         DB::table('error_log')->insert([
            //             ['operation_type' => 'edit user contact',
            //              'affected_email'=>  $request->email,
            //              'issue' =>  "user not found",
            //              'date-time'=> date("Y-m-d H:i:s", time()),
            //             ]
            //         ]); 
            //         return response()->json([
            //             'status' => 500
            // ]);
            //     }

            // }
      
            public function updateClient(Request $request){
                
                $users = new User();
                $client = Clients::where('CLIENTID', $request->laravel_id)->first();
                $user = $users::where('zoho_id', $request->laravel_id)->first();

                if(isset($client)){

                    // first name and last name split and merge
                    $temp = explode(" ",$request->candidate_name);
                    $lastIndex = count($temp);
                    $last_name = $temp[$lastIndex-1];
                    $popLastIndex = array_pop($temp);
                    $first_name = join($temp);

                    // edit client n users table 
                    if (isset($request->client_name) && !empty($request->client_name)) {
                        $client->client_name = $request->client_name;
                        $user->first_name = $first_name;
                    }
                    
                    if (isset($request->job_title) && !empty($request->job_title)) {
                        $client->others = $request->job_title;
                        $user->last_name = $last_name;
                    }
                    if (isset($request->phone) && !empty($request->phone)) {
                        $client->contact_number = $request->phone;
                        $user->phone = $request->phone;
                    }
                    if (isset($request->company_name) && !empty($request->company_name)) {
                        $user->company_name = $request->company_name;
                    }

                    //time operation happen 
                    $user->updated_at = date('Y-m-d H:i:s', time());
                    $client->is_synced = 1;
                    $client->updated_at =  date('Y-m-d H:i:s', time());
                    $client->save();
                    $user->save();

                        return response()->json([
                            'status' => 200
                ]);

                }
                else
                {
                    DB::table('error_log')->insert([
                        ['operation_type' => 'edit user contact',
                         'affected_email'=>  $request->email,
                         'issue' =>  "user not found",
                         'date-time'=> date("Y-m-d H:i:s", time()),
                        ]
                    ]); 
                    return response()->json([
                    'status' => 500
        ]);
                }

            }

            public function updateConsultant(Request $request){
                $Candidates = Candidates::where('CANDIDATEID', $request->laravel_id)->first();
                $user = User::where('zoho_id', $request->laravel_id)->first();
                
                if(isset($user) || isset($Candidates)){

                    // first name and last name split and merge
                    $temp = explode(" ",$request->candidate_name);
                    $lastIndex = count($temp);
                    $last_name = $temp[$lastIndex-1];
                    $popLastIndex = array_pop($temp);
                    $first_name = join($temp);

                    // edit candidate n users table 
                    if (isset($request->candidate_name) && !empty($request->candidate_name)) {
                        $Candidates->first_name = $first_name;
                        $Candidates->last_name = $last_name;
                        $user->first_name = $first_name;
                        $user->last_name = $last_name;
                    }
                   
                    if (isset($request->sap_job_title) && !empty($request->sap_job_title)) {
                        $Candidates->sap_job_title = $request->sap_job_title;
                        $user->sap_job_title = $request->sap_job_title;
                    }
                    if (isset($request->experience_in_years) && !empty($request->experience_in_years)) {
                        $Candidates->experience_in_years = $request->experience_in_years;
                    }
                    if (isset($request->category) && !empty($request->category)) {
                        $Candidates->category = $request->category;
                    }
                    if (isset($request->mobile_number) && !empty($request->mobile_number)) {
                        $Candidates->mobile_number = $request->mobile_number;
                        $user->phone = $request->mobile_number;
                    }
                    if (isset($request->state) && !empty($request->state)) {
                        $Candidates->state = $request->state;
                    }
                    if (isset($request->country) && !empty($request->country)) {
                        $Candidates->country = $request->country;
                    }
                    if (isset($request->city) && !empty($request->city)) {
                        $Candidates->city = $request->city;
                    }
                    if (isset($request->additional_info) && !empty($request->additional_info)) {
                        $Candidates->additional_info_2 = $request->additional_info;
                    }
                    if (isset($request->pay_rate) && !empty($request->pay_rate)) {
                        $Candidates->base_rate = $request->pay_rate;
                    }
                    if (isset($request->notice_period) && !empty($request->notice_period)) {
                        $Candidates->notice_period_days = $request->notice_period;
                    }
                    if (isset($request->skill_set) && !empty($request->skill_set)) {
                        $Candidates->skill_set = $request->skill_set;
                    }
                    if (isset($request->certification_and_training) && !empty($request->certification_and_training)) {
                        $Candidates->certification_and_training = $request->certification_and_training;
                    }
                    if (isset($request->fulltime) && !empty($request->fulltime)) {
                        $Candidates->full_time = $request->fulltime;
                    }
                    if (isset($request->parttime) && !empty($request->parttime)) {
                        $Candidates->part_time = $request->parttime;
                    }
                    if (isset($request->willingtravel) && !empty($request->willingtravel)) {
                        $Candidates->willing_to_travel = $request->willingtravel;
                    }
                    if (isset($request->project) && !empty($request->project)) {
                        $Candidates->project = $request->project;
                    }
                    if (isset($request->support) && !empty($request->support)) {
                        $Candidates->support = $request->support;
                    }
                    if (isset($request->datevalue) && !empty($request->datevalue)) {
                        $Candidates->availability_date = $request->datevalue;
                    }
                    //time operation happen 
                    $user->updated_at = date('Y-m-d H:i:s', time());
                    $Candidates->is_synced = 1;
                    $Candidates->updated_at =  date('Y-m-d H:i:s', time());
                    $Candidates->save();
                    $user->save();

                    response()->json([
                        'status' => 200
            ]);
                }
                else
                {
                    DB::table('error_log')->insert([
                        ['operation_type' => 'edit user contact',
                         'affected_email'=>  $request->email,
                         'issue' =>  "user not found",
                         'date-time'=> date("Y-m-d H:i:s", time()),
                        ]
                    ]); 
                    response()->json([
                        'status' => 500
            ]);
                }
            }

}
