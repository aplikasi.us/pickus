<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobopenings;
use App\Clients;
use App\User;
use App\Jobassociatecandidates;
use App\Modules\Users\Http\Controllers\UsersController;
use App\States;
use App\Cities;
use Session;
use App\Emailtemplate;
use App\Jobopeningcategory;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\ZohoqueryController;
use App\Token;
use App\Industry;

class JobOpeningController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function human_filesize($bytes, $decimals = 2) {
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor > 0)
            $sz = 'KMGT';
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
    }

    public function index(Request $request) {
        if (Auth::user()->user_type == "Territory Admin") {
            $model = Jobopenings::with(['associatecandidates', 'contact','client'])
                    ->select('*')
                    ->whereHas('contact', function ($query) {
                        $query->where('is_delete', 0);
                    })
                    ->where('territory', Auth::user()->territory);
        } else {
            $sessionterri = $request->session()->get('territoryadmin');
            if ($sessionterri != 'Global') {
                $model = Jobopenings::with(['associatecandidates', 'contact','client'])
                        ->select('*')
                        ->whereHas('contact', function ($query) {
                            $query->where('is_delete', 0);
                        })
                        ->where('territory', $sessionterri);
            } else {
                $model = Jobopenings::with(['associatecandidates', 'contact','client'])
                        ->select('*')
                        ->whereHas('contact', function ($query) {
                    $query->where('is_delete', 0);
                });
            }
        }
        $q = $request->get('s');
        if ($q && $q != '') {
            $model = $model->where(function($query) use($q){
                $query->where('Job_ID', $q);
                $query->orWhere('posting_title', 'LIKE', '%' . $q . '%');
                $query->orWhereHas('associatecandidates.consultant', function ($query1) use ($q) {
                    $query1->where(\DB::raw('concat(first_name," ",last_name)'), 'LIKE', '%' . $q . '%');
                });
                $query->orWhereHas('user', function ($query2) use ($q) {
                    $query2->where(\DB::raw('concat(first_name," ",last_name)'), 'LIKE', '%' . $q . '%');
                });
            });
        }
        $jobid = $request->get('jobid');
        if (isset($jobid) && $jobid != '') {
            $model = $model->where('id', $jobid);
        }
        $client_id = $request->get('client_id');
        if ($request->has('client_id') && $client_id != '') {
            $model = $model->where('CONTACTID', $client_id);
        }


        $consultant_id = $request->get('consultant_id');
        if ($request->has('consultant_id') && $consultant_id != '') {
            $model = $model->whereHas('associatecandidates', function($query1)use($consultant_id) {
                $query1->where('CANDIDATEID', $consultant_id);
            });
        }
        $status = $request->get('status');
        if ($request->has('status') && $status != '') {
            $model = $model->where('job_opening_status', $status);
        }
        $jobs = $model->where('is_delete', '0')->orderByRaw("FIELD(job_opening_status, 'Pending','Inactive','In-progress','On-Hold','Filled','Waiting for CV feedback','Waiting for client approval','Cancelled','Declined')")->paginate(10)->appends($request->all());
        /* $clientarray = Client::pluck('client_name','CLIENTID')->toArray(); */
        $statusArray = array(
            'In-progress' => 'Active',
            'Pending' => 'Pending',
            'Cancelled' => 'Cancelled',
            'Inactive' => 'Inactive',
            'On-Hold' => 'On-Hold',
            'Filled' => 'Filled',
            'Waiting for CV feedback' => 'Waiting for CV feedback',
            'Declined' => 'Declined',
        );
        $clientarray = User::where('role_id', '1')->whereNotNull('company_name')->orderBy('company_name','asc')->pluck('company_name', 'CONTACTID')->toArray();

        $consultantarray = User::select('zoho_id', \DB::raw("concat(first_name,' ',last_name) as full_name"))->where('role_id', '2')->orderBy('first_name')->pluck('full_name', 'zoho_id')->toArray();


        /* $clientarray = User::where('role_id','1')->pluck('first_name','zoho_id')->toArray(); */
        /* dd($clientarray); */

        // dd($clientarray->toArray());
        return view('backend.jobs.index', compact('jobs', 'clientarray', 'consultantarray', 'statusArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        //
        $experienceList1 = [
            'None' => 'None',
            'Fresh' => 'Fresh',
            '1-3 years' => '1-3 years',
            '4-5 years' => '4-5 years',
            '5-10 years' => '5-10 years',
            '10+ years' => '10+ years'
        ];

        $jobTypeList = [
            'Contract' => 'Contract',
            'Permanent' => 'Permanent',
            'Internship' => 'Internship'
        ];

        $jobModeList = [
            'None' => 'None',
            'Full time' => 'Full time',
            'Part time' => 'Part time'
        ];
        $userTerritory = User::where('CONTACTID', $request->id)->pluck('territory')->first();
        $stateList = States::where('country_name', $userTerritory)->pluck('state_name', 'id')->toArray();

        $industryList = Industry::pluck('name', 'name')->toArray();

        $jobCategoryList = [
            'Basis/S & A' => 'Basis/S & A',
            'Functional' => 'Functional',
            'Technical' => 'Technical',
            'Project Manager/Solution Architect' => 'Project Manager/Solution Architect',
            'Others' => 'Others'
        ];

        $contactId = $request->id;

        //return view('backend.jobs.create');
        return view('backend.jobs.create', compact('experienceList1', 'jobTypeList', 'jobModeList', 'stateList', 'industryList', 'jobCategoryList', 'contactId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $userController = new UsersController;
        $request->isAdmin = true;
        $userController->clientPostJob($request);
        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        $request->session()->forget('current_url');
        $job = Jobopenings::with('contact')->where('id', $id)->first();
        if ($job->is_delete == 0) {
            if ($job->contact != null) {
                if ($job->contact->is_delete == 0) {
                    $userController = new UsersController();
                    $experienceList = $userController->experienceList();
                    $jobTypeList = $userController->jobTypeList();
                    $jobModeList = $userController->jobModeList();
                    $stateList = States::where('country_name', $job->territory)->pluck('state_name', 'id')->toArray();
                    $industryList = $userController->industryList();
                    $jobCategoryList = $userController->jobCategoryList();
                    $state = States::where('state_name', $job->state)->first();
                    $city = "";
                    if (count($state)) {
                        $city = Cities::where('state_id', $state->id)->pluck('city_name', 'city_name')->toArray();
                    }
                    $categories = ['Basis/S & A' => 'Basis/S & A', 'Functional' => 'Functional', 'Technical' => 'Technical', 'Project Manager/Solution Architect' => 'Project Manager/Solution Architect', 'Others' => 'Others'];
                    return view('backend.jobs.edit', compact('job', 'experienceList', 'jobTypeList', 'jobModeList', 'stateList', 'industryList', 'jobCategoryList', 'state', 'city', 'categories'));
                } else {
                    Session::flash('error', 'Client account has been deactivated');
                    return redirect()->route('jobs.index');
                }
            } else {
                Session::flash('error', 'Client account has been deactivated');
                return redirect()->route('jobs.index');
            }
        } else {
            Session::flash('error', 'This job has been deleted');
            return redirect()->route('jobs.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $job = Jobopenings::where('id', $request->id)->first();
        $oldJobStatus = $job->job_opening_status;
        $state = States::where('id', $request->state)->first()->state_name;
        $job->posting_title = $request->job_title;
        $job->job_base_rate = $request->budget;
        $job->work_experience = $request->experiece;
        $job->job_type = $request->job_type;
        $job->job_mode = $request->job_mode;
        $job->date_opened = $request->start_date;
        $job->job_duration = $request->duration;
        $job->state = $state;
        $job->city = $request->city;
        $job->industry = $request->industry;
        $job->category = implode(',', $request->category);
        $job->key_skills = $request->key_skills;
        $job->job_description = $request->job_description;
        $job->job_requirements = $request->requirements;
        $job->job_benefits = $request->benefits;
        $job->all_description = '<span id="spandesc"><div>' . $request->job_description . '</div></span><br /><span id="spanreq"><h3>Requirements</h3><div>' . $request->requirements . '</div></span><br /><span id="spanben"><h3>Benefits</h3><div>' . $request->benefits . '</div></span>';
        $job->job_opening_status = $request->job_opening_status;
        if ($request->job_opening_status == "In-progress") {
            $job->is_approve_by_admin = 1;
            $job->publish_in_us = 1;
            $job->block = 0;
            if ($job->territory == "MY") {
                $job->account_manager = "Talent Admin";
            } else if ($job->territory == "PH") {
                $job->account_manager = "Talent PH Admin";
            } else {
                $job->account_manager = "Talent Admin";
            }
        }
        $job->is_synced = 0;
        $job->save();
        /* Updating status in zoho*/
        $zohoQuery = new ZohoqueryController();
        $territory = "MY";
        if ($job->territory != null) {
            $territory = $job->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $zohoQuery->token = $result->token;
        $res = $zohoQuery->updateStatus('JobOpenings',$job->JOBOPENINGID,$job->job_opening_status);
        
        if (isset($request->category)) {
            Jobopeningcategory::where([
                'job_id' => $job->id
            ])->delete();
            foreach ($request->category as $catKey => $catValue) {
                $category = new Jobopeningcategory();
                $category->job_id = $job->id;
                // $category->JOBOPENINGID = $job->JOBOPENINGID;
                $category->category = $catValue;
                $category->created_at = date('Y-m-d H:i:s', time());
                $category->updated_at = date('Y-m-d H:i:s', time());
                $category->save();
            }
        }

        if ($oldJobStatus == "Pending" && $request->job_opening_status == "In-progress") {
            $client = Clients::where('CLIENTID', $job->CLIENTID)->first();
            /* Send Notification to client it self */
            if ($client != null) {
                /* Send Notification to Client */
                $userController = new UsersController();
                $data['db_sender_id'] = "";
                $data['db_receiver_id'] = $job->db_contact_id;
                $data['sender_id'] = "";
                $data['receiver_id'] = $job->CONTACTID;
                $data['message'] = "Your job " . $job->posting_title . " is approved";
                $data['type'] = "job_approved";
                $data['to'] = "Client";
                if ($client->territories != null) {
                    $data['redirect_url'] = url($client->territories . '/job-details/' . Crypt::encryptString($job->id));
                }
                $userController->sendNotification($data);

                $user = User::where('zoho_id', $client->CLIENTID)->first();
                if ($user != null) {
                    if ($user->email != null) {
                        $template = EmailTemplate::where('name', 'approve_job_post')->first();
                        if ($template != null) {
                            $emailData['clientname'] = $client->client_name;
                            $emailData['job_title'] = $job->posting_title;
                            $emailData['email'] = $user->email;
                            $emailData['subject'] = "Admin has approved the job " . $job->posting_title;
                            Mail::send([], [], function($messages) use ($template, $emailData) {
                                $messages->to($emailData['email'], $emailData['clientname'])
                                        ->subject($emailData['subject'])
                                        ->setBody($template->parse($emailData), 'text/html');
                            });
                        }
                    }
                }
            }
        }
        Session::flash('success', 'Job has been updated successfully');
        return redirect()->route('jobs.edit', [$request->id]);
    }

    /* Approve Job */

    public function approveJob(Request $request) {
        $job = Jobopenings::where('id', $request->id)->first();
        
        $job->Job_ID = 'JOB_0'.$job->id;
        $job->JOBOPENINGID = $job->Job_ID;
        $job->is_approve_by_admin = 1;
        $job->job_opening_status = "In-progress";
        $job->publish_in_us = 1; // True
        $job->block = 0; // False
        if ($job->territory == "MY") {
            $job->account_manager = "Talent Admin";
        } else if ($job->territory == "PH") {
            $job->account_manager = "Talent PH Admin";
        } else {
            $job->account_manager = "Talent Admin";
        }
        $job->save();

        $client = Clients::where('CLIENTID', $job->CLIENTID)->first();
        /* Send Notification to client it self */
        if ($client != null) {
            /* Send Notification to Client */
            $userController = new UsersController();
            $data['db_sender_id'] = "";
            $data['db_receiver_id'] = $job->db_contact_id;
            $data['sender_id'] = "";
            $data['receiver_id'] = $job->CONTACTID;
            $data['message'] = "Your job " . $job->posting_title . " is approved";
            $data['type'] = "job_approved";
            $data['to'] = "Client";
            if ($client->territories != null) {
                $data['redirect_url'] = url($client->territories . '/job-details/' . Crypt::encryptString($job->id));
            }
            $userController->sendNotification($data);

            $user = User::where('zoho_id', $client->CLIENTID)->first();
            if ($user != null) {
                if ($user->email != null) {
                    $template = EmailTemplate::where('name', 'approve_job_post')->first();
                    if ($template != null) {
                        $emailData['clientname'] = $client->client_name;
                        $emailData['job_title'] = $job->posting_title;
                        $emailData['email'] = $user->email;
                        $emailData['subject'] = "Admin has approved the job " . $job->posting_title;
                        Mail::send([], [], function($messages) use ($template, $emailData) {
                            $messages->to($emailData['email'], $emailData['clientname'])
                                    ->subject($emailData['subject'])
                                    ->setBody($template->parse($emailData), 'text/html');
                        });
                    }
                }
            }
        }
        Session::flash('success', 'Job has been approved successfully');
        return redirect()->route('jobs.edit', [$request->id]);
    }

    public function getCityList(Request $request) {
        $city = Cities::where('state_id', $request->id)->pluck('city_name', 'city_name')->toArray();
        return response()->json([
                    'city' => $city
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->id;
        $JobOpening = Jobopenings::where('id', $id)->first();

        if (!empty($JobOpening)) {

            Jobopenings::where('id', $id)->update(['is_delete' => 1, 'is_synced' => 0]);
            if ($JobOpening->JOBOPENINGID != null) {
                $this->deleteJobopeningFromZoho($JobOpening->JOBOPENINGID);
            }
            /* $User->is_delete = 1; */

            return Response()->json(["success" => true, "message" => "Job opening has been deleted successfully."]);
        }
    }

    public function deleteJobopeningFromZoho($recordId) {
        $url = 'https://recruit.zoho.com/recruit/private/json/JobOpenings/deleteRecords?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=' . $recordId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);  // Execute a cURL request
        $response = json_decode($response, true);
        return $response;
    }

    public function status(Request $request) {
        $id = $request->id;
        $JobOpening = Jobopenings::where("id", $id)->first();
        if ($JobOpening['job_opening_status'] == 'Inactive' || $JobOpening['job_opening_status'] == 'Pending') {
            $data = array(
                'job_opening_status' => 'In-progress',
                'is_synced' => 0
            );
        }
        $Status = Jobopenings::where('id', $id)->update($data);

        return Response()->json(["success" => true, "message" => "Status changed successfully."]);
    }

    public function jobRequestedProfile(Request $request) {
        if (Auth::user()->user_type == "Territory Admin") {
            $model = Jobopenings::with(['associatecandidates', 'contact','client'])
                    ->select('*')
                    ->whereHas('contact', function ($query) {
                        $query->where('is_delete', 0);
                    })
                    ->where('territory', Auth::user()->territory);
        } else {
            $sessionterri = $request->session()->get('territoryadmin');
            if ($sessionterri != 'Global') {
                $model = Jobopenings::with(['associatecandidates', 'contact','client'])
                        ->select('*')
                        ->whereHas('contact', function ($query) {
                            $query->where('is_delete', 0);
                        })
                        ->where('territory', $sessionterri);
            } else {
                $model = Jobopenings::with(['associatecandidates', 'contact','client'])
                        ->select('*')
                        ->whereHas('contact', function ($query) {
                    $query->where('is_delete', 0);
                });
            }
        }


        $q = $request->get('s');
        if ($q && $q != '') {
            $model = $model->whereHas('associatecandidates.consultant', function ($query) use ($q) {
                $query->where(\DB::raw('concat(first_name," ",last_name)'), 'LIKE', '%' . $q . '%');
                $query->orWhere('Job_ID', $q);
                $query->orWhere('posting_title', 'LIKE', '%' . $q . '%');
                $query->orWhere('job_type', 'LIKE', '%' . $q . '%');
                $query->orWhere('job_mode', 'LIKE', '%' . $q . '%');
                $query->orWhere('city', 'LIKE', '%' . $q . '%');
            });
            $model = $model->orWhereHas('user', function ($query) use ($q) {
                $query->where(\DB::raw('concat(first_name," ",last_name)'), 'LIKE', '%' . $q . '%');
                $query->orWhere('Job_ID', $q);
                $query->orWhere('posting_title', 'LIKE', '%' . $q . '%');
                $query->orWhere('job_type', 'LIKE', '%' . $q . '%');
                $query->orWhere('job_mode', 'LIKE', '%' . $q . '%');
                $query->orWhere('city', 'LIKE', '%' . $q . '%');
            });
        }

        $client_id = $request->get('client_id');
        if ($request->has('client_id') && $client_id != '') {
            $model = $model->where('CONTACTID', $client_id);
        }


        $consultant_id = $request->get('consultant_id');
        if ($request->has('consultant_id') && $consultant_id != '') {
            $model = $model->whereHas('associatecandidates', function($query1)use($consultant_id) {
                $query1->where('CANDIDATEID', $consultant_id);
            });
        }
        $jobs = $model->where('is_delete', '0')->where('job_opening_status', 'Pending')->orderByRaw("FIELD(job_opening_status, 'Pending')")->paginate(10)->appends($request->all());
        /* $clientarray = Client::pluck('client_name','CLIENTID')->toArray(); */
        $clientarray = User::whereNotNull('company_name')->orderBy('company_name','asc')->pluck('company_name', 'CONTACTID')->toArray();

        $consultantarray = User::select('zoho_id', \DB::raw("concat(first_name,' ',last_name) as full_name"))->where('role_id', '2')->orderBy('first_name')->pluck('full_name', 'zoho_id')->toArray();

        $statusArray = array(
            'job_opening_status' => 'In-progress',
            'is_synced' => 0
        );

        /* $clientarray = User::where('role_id','1')->pluck('first_name','zoho_id')->toArray(); */
        /* dd($clientarray); */

        // dd($clientarray->toArray());
        return view('backend.jobs.jobrequestedprofile', compact('jobs', 'clientarray', 'consultantarray', 'statusArray'));
    }

}