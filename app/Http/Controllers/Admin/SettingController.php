<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Emailtemplate;
use App\Notification;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Settings;
use Illuminate\Support\Facades\Auth;
use App\Console\Commands\SyncFromZohotoAplikasi;
use App\Console\Commands\Syncdata;
use App\Tagline;
use App\Faq;
use App\Category;


class SettingController extends Controller
{
    /*
     * Email Templates
     * 
     */

    public function emailTemplates(Request $request)
    {
        $templates = EmailTemplate::paginate(10);
        return View('backend.settings.emailtemplates', compact('templates'));
    }

    /*
     * Edit email template
     * 
     */

    public function editEmailTemplate(Request $request)
    {
        $template = EmailTemplate::where('id', $request->id)->first();
        return View('backend.settings.editemailtemplate', compact('template'));
    }

    /*
     * Update email template
     * 
     */

    public function updateEmailTemplate(Request $request)
    {
        DB::table('email_templates')
            ->where('id', $request->id)
            ->update(['subject' => $request->subject, 'content' => $request->content, 'updated_at' => date('Y-m-d H:i:s', time())]);
        //        $template = EmailTemplate::where('name', 'client_request_profile')->first();
        //        Mail::send([], [], function($messages) use ($template) {            
        //            $messages->to("ramesh.chudasama@aipxperts.com", "Ramesh Chudasama")
        //                    ->subject($template->subject)
        //                    ->setBody($template->content, 'text/html');
        //        });        
        Session::flash('success', 'Email template updated successfully.');
        return redirect()->route('emailtemplate');
    }

    // Site settings
    public function site(Request $request)
    {
        $date_format = array(
            '' => '--Select Date Format--',
            '1' => 'dd/MM/yyyy',
            '2' => 'dd.MM.yyyy',
            '3' => 'MM/dd/yyyy',
            '4' => 'MM.dd.yyyy',
            '5' => 'yyyy/MM/dd',
            '6' => 'yyyy.MM.dd',
            '7' => 'yyyy-MM-dd',
        );
        $site_settings = Settings::get();
        return View('backend.settings.sitesettings', compact('site_settings', 'date_format'));
    }

    // Update Site Settings
    public function updateSiteSettings(Request $request)
    {
        foreach ($_POST as $key => $value) {
            DB::table('settings')
                ->where('settingkey', $key)
                ->update(['settingvalue' => $value, 'updated_at' => date('Y-m-d h:i:s', time())]);
        }
        Session::flash('success', 'Site settings updated successfully.');
        return redirect()->route('site');
    }

    // Notification list 
    public function notification(Request $request)
    {
        if (Auth::user()->user_type == "Territory Admin") {
            $notificationsRead = Notification::where('to', 'Admin')->where('is_read', '0')->whereNotNull('territory')->where('territory', Auth::user()->territory)->orderBy('created_at', 'desc')->get();
        } else {
            $notificationsRead = Notification::where('to', 'Admin')->where('is_read', '0')->orderBy('created_at', 'desc')->get();
        }
        if (count($notificationsRead)) {
            foreach ($notificationsRead as $key => $value) {
                $value->is_read = 1;
                $value->save();
            }
        }
        return View('backend.settings.notification');
    }

    public function verifyAdminPassword(Request $request)
    {
        if (\Hash::check($request->password, Auth::user()->password)) {
            return response()->json([
                'status' => 200
            ]);
        } else {
            return response()->json([
                'status' => 401
            ]);
        }
    }

    /*
     * Sync zoho data
     * 
     */

    public function syncZohoData(Request $request)
    {
            
        if ($request->isMethod('POST')) { //problem here 
            $syncdata = new Syncdata();

            $syncdata->sendCandidates("MY");
            $syncdata->sendCandidates("PH");

            $syncdata->receiveCandidates("MY");
            $syncdata->receiveCandidates("PH");

            $syncdata->receiveCandidateSignedAttachment("MY");
            $syncdata->receiveCandidateSignedAttachment("PH");

            $syncdata->receiveCandidateProfileRequests("MY");
            $syncdata->receiveCandidateProfileRequests("PH");

            $syncdata->sendClients("MY");
            $syncdata->sendClients("PH");

            $syncdata->receiveClients("MY");
            $syncdata->receiveClients("PH");

            $syncdata->sendContacts("MY");
            $syncdata->sendContacts("PH");

            $syncdata->receiveContacts("MY");
            $syncdata->receiveContacts("PH");

            $syncdata->sendJobOpenings("MY");
            $syncdata->sendJobOpenings("PH");

            $syncdata->receiveJobOpenings("MY");
            $syncdata->receiveJobOpenings("PH");

            $syncdata->receiveInterviews("MY");
            $syncdata->receiveInterviews("PH");
            return response()->json([
                'status' => 200
            ]);
        } else {
            return view('backend.settings.synczohodata');
            // return response()->json([
            //     'status' => 404
            // ]);
        }
    }

    /*
     * List taglines
     * 
     */

    public function tagLines(Request $request)
    {
        if (Auth::user()->role_id == 3 && Auth::user()->user_type == "Super Admin") {
            $taglines = Tagline::select('*');

            $q = $request->get('s');
            $userType = $request->get('user_type');
            $category = $request->get('category');
            if ($userType && $userType != "") {
                $taglines = $taglines->where('user_type', $userType);
            }
            if ($category && $category != "") {
                $taglines = $taglines->where('category', $category);
            }
            if ($q && $q != '') {
                $taglines = $taglines->where('content', 'LIKE', "%" . $q . "%");
            }
            $taglines = $taglines->orderBy('updated_at', 'desc')->paginate(10)->appends($request->all());
            return View('backend.settings.taglines', compact('taglines'));
        } else {
            Session::flash('error', 'You are not authorized to access this feature');
            return redirect()->route('dashboard');
        }
    }

    /*
     * Add tagline
     * 
     */

    public function addTagline(Request $request)
    {
        if (Auth::user()->role_id == 3 && Auth::user()->user_type == "Super Admin") {
            if ($request->isMethod('POST')) {
                $tagline = new Tagline();
                $tagline->user_type = $request->user_type;
                $tagline->category = $request->category;
                $tagline->content = $request->content;
                $tagline->created_at = date('Y-m-d h:i:s', time());
                $tagline->updated_at = date('Y-m-d h:i:s', time());
                $tagline->save();
                Session::flash('success', 'Tagline inserted successfully.');
                return redirect()->route('taglines');
            } else {
                return view('backend.settings.addtagline');
            }
        } else {
            Session::flash('error', 'You are not authorized to access this feature');
            return redirect()->route('dashboard');
        }
    }

    /*
     * Update Tagline
     * 
     */

    public function updateTagline(Request $request)
    {
        if (Auth::user()->role_id == 3 && Auth::user()->user_type == "Super Admin") {
            $tagline = Tagline::where('id', $request->id)->first();
            if ($request->isMethod('POST')) {
                $tagline->content = $request->content;
                $tagline->status = $request->status;
                $tagline->updated_at = date('Y-m-d h:i:s', time());
                $tagline->save();
                Session::flash('success', 'Tagline updated successfully.');
                return redirect()->route('taglines');
            } else {
                if ($tagline != null) {
                    return view('backend.settings.edittagline', compact('tagline'));
                } else {
                    Session::flash('error', 'No tagline found.');
                    return redirect()->route('taglines');
                }
            }
        } else {
            Session::flash('error', 'You are not authorized to access this feature');
            return redirect()->route('dashboard');
        }
    }

    /*
     * Delete Tagline
     * 
     */

    public function deleteTagline(Request $request)
    {
        if (Auth::user()->role_id == 3 && Auth::user()->user_type == "Super Admin") {
            Tagline::where('id', $request->id)->delete();
            Session::flash('success', 'Tagline deleted successfully.');
            return redirect()->route('taglines');
        } else {
            Session::flash('error', 'You are not authorized to access this feature');
            return redirect()->route('dashboard');
        }
    }

    /*
     * Read Notification and Remove Counter From Bell
     * 
     */

    public function readNotification(Request $request)
    {
        $unreadNotification = Notification::where('to', 'Admin')->where('is_read', 0)->get();
        /* Mark as read */
        if (count($unreadNotification)) {
            foreach ($unreadNotification as $key => $value) {
                $value->is_read = 1;
                $value->save();
            }
        }
        return;
    }

    /*
     * FAQ'S List
     * 
     */

    public function faq(Request $request)
    {
        if (Auth::user()->role_id == 3 && Auth::user()->user_type == "Super Admin") {
            $query = Faq::with('faqcategory')->select('*');
            $q = $request->get('s');
            if ($q && $q != '') {
                $query = $query->where('question', 'LIKE', "%" . $q . "%")
                    ->orWhere('answer', 'LIKE', "%" . $q . "%")
                    ->orWhere('territory', 'LIKE', "%" . $q . "%")
                    ->orWhereHas('faqcategory', function ($query2) use ($q) {
                        $query2->where('name', 'LIKE', "%" . $q . "%");
                    });
            }
            $faq = $query->orderBy('updated_at', 'desc')->paginate(10)->appends($request->all());
            return View('backend.settings.faqs', compact('faq'));
        } else {
            Session::flash('error', 'You are not authorized to access this feature');
            return redirect()->route('dashboard');
        }
    }

    /*
     * Add FAQ
     */

    public function addFAQ(Request $request)
    {
        if ($request->isMethod('POST')) {
            $faq = new Faq();
            $faq->category = $request->category;
            $faq->question = $request->question;
            $faq->answer = $request->answer;
            $faq->territory = $request->territory;
            $faq->created_at = date('Y-m-d H:i:s', time());
            $faq->updated_at = date('Y-m-d H:i:s', time());
            $faq->save();
            Session::flash('success', 'Faq has been saved successfully');
            return redirect()->route('faq');
        } else {
            $category = Category::pluck('name', 'id')->toArray();
            return view('backend.settings.addfaq', compact('category'));
        }
    }

    /*
     * Update FAQ
     * 
     */

    public function updateFAQ(Request $request)
    {
        $faq = Faq::where('id', $request->id)->first();
        if ($request->isMethod('POST')) {
            $faq->category = $request->category;
            $faq->question = $request->question;
            $faq->answer = $request->answer;
            $faq->territory = $request->territory;
            $faq->updated_at = date('Y-m-d H:i:s', time());
            $faq->save();
            Session::flash('success', 'Faq has been updated successfully');
            return redirect()->route('faq');
        } else {
            $category = Category::pluck('name', 'id')->toArray();
            return view('backend.settings.updatefaq', compact('category', 'faq'));
        }
    }

    /*
     * Delete FAQ
     * 
     */

    public function deleteFAQ(Request $request)
    {
        Faq::where('id', $request->id)->delete();
        Session::flash('success', 'Faq has been deleted successfully');
        return redirect()->route('faq');
    }
}