<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clients;
use App\Consultant;
use App\Experience;
use App\Education;
use App\Reference;
use App\Contracthistory;
use App\Jobassociatecandidates;
use App\Recentlyviewedconsultant;
use App\User;
use App\Emailtemplate;
use App\Notification;
use App\Interviews;
use Illuminate\Support\Facades\Mail;
use Humantech\Zoho\Recruit\Api\Client\AuthenticationClient;
use Illuminate\Support\Facades\DB;
use Humantech\Zoho\Recruit\Api\Client\Client;
use Illuminate\Support\Facades\Crypt;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Token;
use App\Attachment;
use App\Clientwishlist;
use App\Signeddocument;
use App\Signeddocumentattachment;
use App\Console\Commands\Syncdatatozoho;
use App\Http\Controllers\ZohoqueryController;
use App\Candiatecategory;
use App\Modules\Users\Http\Controllers\UsersController;

class ConsultantController extends Controller {

    public $token;

    public function __construct() {
        $result = Token::first();
        $this->token = $result->token;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        if (Auth::user()->user_type == "Territory Admin") {
            $model = Consultant::select('*')->where('territory', Auth::user()->territory);
        } else {
            $sessionterri = $request->session()->get('territoryadmin');
            if ($sessionterri != 'Global') {
                $model = Consultant::select('*')->where('territory', $sessionterri);
            } else {
                $model = Consultant::select('*');
            }
        }
        $q = $request->get('s');
        $requestedSummry = $request->get('requested_summary');
        if ($requestedSummry && $requestedSummry != "") {
            $model->where('is_summary_requested', $requestedSummry);
        }
        $account_status = $request->get('account_status');
        if ($account_status && $account_status != "") {
            if ($account_status == 1) {
                $model->where('is_delete', 0);
            } else {
                $model->where('is_delete', 1);
            }
        }
        if ($q && $q != '') {
            $model = $model->where(function($query) use ($q) {
                $query->where(\DB::raw('concat(first_name," ",last_name)'), 'LIKE', '%' . $q . '%');
                $query->orWhere('candidate_id', 'LIKE', '%' . $q . '%');
                $query->orWhere('email', 'LIKE', '%' . $q . '%');
                $query->orWhere('salutation', 'LIKE', '%' . $q . '%');
                $query->orWhere('currency', 'LIKE', '%' . $q . '%');
                $query->orWhere('mobile_number', 'LIKE', '%' . $q . '%');
            });
        }

        $consultant = $model->orderBy("is_summary_requested", 'desc')->paginate(10)->appends($request->all());
        return view('backend.consultant.index', compact('consultant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.consultant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->role_id = 2;
        $request->territory = 'MY';
        $request->isAdmin = true;
        $userController = new UsersController();
        return $userController->consultantRegistration($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function deleteConsultant(Request $request) {
        if (Auth::user()->role_id == 3) {
            $response = $this->deleteConsultantFromZoho($request->id);
            $consultant = Consultant::where('CANDIDATEID', $request->id)->first();
            if (!key_exists('error', $response['response'])) {
                Consultant::where('CANDIDATEID', $request->id)->delete();
                Attachment::where('zoho_id', $request->id)->delete();
                Contracthistory::where('CANDIDATEID', $request->id)->delete();
                Education::where('CANDIDATEID', $request->id)->delete();
                Experience::where('CANDIDATEID', $request->id)->delete();
                Reference::where('CANDIDATEID', $request->id)->delete();
                Clientwishlist::where('CANDIDATEID', $request->id)->delete();
                Jobassociatecandidates::where('CANDIDATEID', $request->id)->delete();
                Notification::where('sender_id', $request->id)->orWhere('receiver_id', $request->id)->delete();
                Recentlyviewedconsultant::where('CANDIDATEID', $request->id)->delete();
                if ($consultant != null) {
                    $signedDocumentId = Signeddocument::where('email', $consultant->email)->pluck('id')->toArray();
                    Signeddocument::where('email', $consultant->email)->delete();
                    if (count($signedDocumentId)) {
                        Signeddocumentattachment::whereIn('signed_document_id', $signedDocumentId)->delete();
                    }
                }
                User::where('zoho_id', $request->id)->delete();
                Session::flash('success', 'Consultant has been deleted successfully');
                return redirect()->back();
            } else {
                Session::flash('error', 'Consultant not able to delete from zoho');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('dashboard');
        }
    }

    public function deleteConsultantFromZoho($recordId) {
        $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/deleteRecords?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=' . $recordId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);  // Execute a cURL request
        $response = json_decode($response, true);
        return $response;
    }

    public function status(Request $request) {

        $id = $request->id;
        $status = $request->status;
        $user = User::where("zoho_id", $id)->first();

        if (!empty($user)) {
            if ($status == "inactive") {
                $user->is_delete = 1;
            } else {
                $user->is_delete = 0;
            }
            $user->save();
        }

        $candidate = Consultant::where("CANDIDATEID", $id)->first();

        if (!empty($candidate)) {
            if ($status == "inactive") {
                $candidate->is_delete = 1;
            } else {
                $candidate->is_delete = 0;
            }
            $candidate->is_synced = 0;
            $candidate->save();
        }
        return Response()->json(["success" => true, "message" => "Status changed successfully."]);
    }

    /**
     * 
     * Consultant edit get
     */
    public function editConsultant(Request $request) {
        $zoho_id = $request->id;
        $editconsultant = Consultant::with(['associatejobopenings','experience', 'education', 'reference'])->where('CANDIDATEID', $zoho_id)->orderby('id', 'desc')->first();
        $status = $this->candidateStatusList();
        $categories = ['Basis/S & A' => 'Basis/S & A', 'Functional' => 'Functional', 'Technical' => 'Technical', 'Project Manager/Solution Architect' => 'Project Manager/Solution Architect', 'Others' => 'Others'];
        return view('backend.consultant.edit', compact('editconsultant', 'status', 'categories'));
    }

    /**
     * 
     * Summary approve by admin
     */
    public function summaryApprove(Request $request) {
        $zoho_id = $request->id;
        $editconsultant = Consultant::where('CANDIDATEID', $zoho_id)->orderby('id', 'desc')->first();
        $consultantCheck = Consultant::where('CANDIDATEID', $zoho_id)->orderby('id', 'desc')->first();
        $consultantCurrentStatus = $editconsultant->candidate_status;
        if (count($editconsultant)) {
            if ($request->updatesummary == 1) {
                $editconsultant->additional_info_2 = $request->additional_info_2;
                $editconsultant->additional_info = $request->additional_info;
            }
            if ($request->updatesummary == 2) {
                $editconsultant->additional_info_2 = $request->additional_info_2;
                $editconsultant->additional_info = $request->additional_info_2;
            }
            $editconsultant->sap_job_title = $request->sap_job_title;
            $editconsultant->first_name = $request->first_name;
            $editconsultant->last_name = $request->last_name;
            $editconsultant->skype_id = $request->skype_id;
            $editconsultant->mobile_number = $request->mobile_number;
            $editconsultant->twitter = $request->twitter;
            $editconsultant->experience_in_years = $request->experience_in_years;
            $editconsultant->state = $request->state;
            $editconsultant->city = $request->city;
            $editconsultant->skill_set = $request->skill_set;
            $editconsultant->base_rate = $request->base_rate;
            $editconsultant->reserved_base_rate = $request->reserved_base_rate;
            $editconsultant->notice_period_days = $request->notice_period_days;
            $editconsultant->availability_date = $request->availability_date;
            $editconsultant->certification_and_training = $request->certification_and_training;
            $editconsultant->category = implode(',', $request->category);
            if (isset($request->full_time)) {
                $editconsultant->full_time = 1;
            } else {
                $editconsultant->full_time = 0;
            }
            if (isset($request->part_time)) {
                $editconsultant->part_time = 1;
            } else {
                $editconsultant->part_time = 0;
            }
            if (isset($request->willing_to_travel)) {
                $editconsultant->willing_to_travel = 1;
            } else {
                $editconsultant->willing_to_travel = 0;
            }
            if (isset($request->project)) {
                $editconsultant->project = 1;
            } else {
                $editconsultant->project = 0;
            }
            if (isset($request->support)) {
                $editconsultant->support = 1;
            } else {
                $editconsultant->support = 0;
            }
            if ($request->updatesummary == 2) {
                $editconsultant->is_summary_requested = 2;
            }
            if ($request->updatesummary == 3) {
                $editconsultant->is_summary_requested = 0;
            }
            $editconsultant->candidate_status = $request->status;
            $editconsultant->is_synced = 0;
            $editconsultant->save();
            /* Updating status in zoho */
            // if(isset($request->status) && $consultantCurrentStatus != $request->status){
            //     $zohoQuery = new ZohoqueryController();
            //     $territory = "MY";
            //     if ($editconsultant->territory != null) {
            //         $territory = $editconsultant->territory;
            //     }
            //     $result = Token::where('territory', $territory)->first();
            //     $zohoQuery->token = $result->token;
            //     $zohoQuery->updateCandidateStatus($editconsultant->CANDIDATEID,null, $editconsultant->candidate_status);
            // }
            if (isset($request->category)) {
                Candiatecategory::where([
                    'candidate_id' => $editconsultant->id
                ])->delete();
                foreach ($request->category as $catKey => $catValue) {
                    $category = new Candiatecategory();
                    $category->candidate_id = $editconsultant->id;
                    $category->CANDIDATEID = $editconsultant->CANDIDATEID;
                    $category->category = $catValue;
                    $category->created_at = date('Y-m-d H:i:s', time());
                    $category->updated_at = date('Y-m-d H:i:s', time());
                    $category->save();
                }
            }
            if ($request->updatesummary == 2) {
                $template = EmailTemplate::where('name', 'approve_candidate_summary')->first();
                if ($template != null) {
                    $emaildata['email'] = $editconsultant->email;
                    $emaildata['candidate_name'] = $editconsultant->first_name . ' ' . $editconsultant->last_name;
                    // $emaildata['profile_link'] = route('editConsultant', [$Candidates->CANDIDATEID]);
                    // $emaildata['candidate_id'] = $editconsultant->CANDIDATEID;
                    $emaildata['subject'] = $editconsultant->first_name . ' ' . $editconsultant->last_name . " has summary approved";
                    Mail::send([], [], function($messages) use ($template, $emaildata) {
                        $messages->to($emaildata['email'], "Admin")
                                ->subject($emaildata['subject'])
                                ->setBody($template->parse($emaildata), 'text/html');
                    });
                }
                $notificationData['db_sender_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
                $notificationData['db_receiver_id'] = $editconsultant->id;
                $notificationData['sender_id'] = "";
                $notificationData['receiver_id'] = $editconsultant->CANDIDATEID;
                $notificationData['message'] = $editconsultant->first_name . ' ' . $editconsultant->last_name . ' profile summary has been approved';
                $notificationData['type'] = "candidate_submit_summary";
                $notificationData['to'] = "Consultant";
                if ($editconsultant->territory != null) {
                    $notificationData['redirect_url'] = url($editconsultant->territory . '/consultant/profile');
                }
                $this->sendNotification($notificationData);
                // Updating candidate summary in Zoho
                $token = Token::first();
                // if ($token != null) {
                //     $data['Additional Info'] = strip_tags($editconsultant->additional_info);
                //     $client = new Client($token->token);
                //     $client->updateRecords('Candidates', $editconsultant->CANDIDATEID, $data);
                // }
            }
            if ($request->updatesummary == 3) {
                $template = EmailTemplate::where('name', 'rejected_candidate_summary')->first();
                if ($template != null) {
                    $emaildata['email'] = $editconsultant->email;
                    $emaildata['candidate_name'] = $editconsultant->first_name . ' ' . $editconsultant->last_name;
                    // $emaildata['profile_link'] = route('editConsultant', [$Candidates->CANDIDATEID]);
                    // $emaildata['candidate_id'] = $editconsultant->CANDIDATEID;
                    $emaildata['subject'] = "Profile Summary Rejected";
                    Mail::send([], [], function($messages) use ($template, $emaildata) {
                        $messages->to($emaildata['email'], "Admin")
                                ->subject($emaildata['subject'])
                                ->setBody($template->parse($emaildata), 'text/html');
                    });
                }
                $notificationData['db_sender_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
                $notificationData['db_receiver_id'] = $editconsultant->id;
                $notificationData['sender_id'] = "";
                $notificationData['receiver_id'] = $editconsultant->CANDIDATEID;
                $notificationData['message'] = "O'oh... Your summary has something that we can't approve, sorry";
                $notificationData['type'] = "candidate_submit_summary";
                $notificationData['to'] = "Consultant";
                if ($editconsultant->territory != null) {
                    $notificationData['redirect_url'] = url($editconsultant->territory . '/consultant/profile');
                }
                $this->sendNotification($notificationData);
            }
            if ($request->updatesummary == 1) {
                Session::flash('success', 'Profile has been updated.');
            } elseif ($request->updatesummary == 2) {
                Session::flash('success', 'You have successfully approved the requested summary and summary has been sent to zoho!');
            } else {
                Session::flash('error', 'Summary has been rejected.');
            }
            return redirect()->back();
        } else {
            Session::flash('error', 'Somthing went wrong please try sometimes later!!!');
            return redirect()->back();
        }
    }

    /**
     * 
     * Send Notfcation function 
     * 
     */
    public function sendNotification($data = []) {
        $notification = new Notification();

        $notification->s_id = $data['db_sender_id'];
        $notification->sender_id = $data['sender_id'];
        if (isset($data['db_sender_id'])) {
            $notification->r_id = $data['db_receiver_id'];
            $notification->receiver_id = $data['receiver_id'];
        }
        if (isset($data['redirect_url'])) {
            $notification->redirect_url = $data['redirect_url'];
        }
        $notification->message = $data['message'];
        $notification->type = $data['type'];
        $notification->to = $data['to'];
        $notification->created_at = date('Y-m-d H:i:s', time());
        $notification->updated_at = date('Y-m-d H:i:s', time());
        $notification->save();
        return;
    }

    /**
     * Add experience 
     * 
     */
    public function addExperience(Request $request) {
        $id = $request->id;
        $consultant = Consultant::where("CANDIDATEID", $id)->where('is_delete', 0)->first();
        if (count($consultant)) {
            if ($request->isMethod('POST')) {
                $consultant->is_synced = 0;
                $consultant->save();

                $zohoQuery = new ZohoqueryController();
                $experienceList = Experience::where(['CANDIDATEID' => $request->id])->get();
                $xmlData = $zohoQuery->createUpdateExperienceXmlData($experienceList, $request, "add");
                $response = $zohoQuery->addCandidateTabularRecord($id, $xmlData);

                $tabularData = $zohoQuery->getCandidateTabularRecords($id, "Experience Details");
                /* Removing old experience */
                Experience::where(['CANDIDATEID' => $request->id])->delete();
                if (key_exists(0, $tabularData['response']['result']['Candidates']['FL']['TR'])) {
                    $data = $tabularData['response']['result']['Candidates']['FL']['TR'];
                } else {
                    $data[0] = $tabularData['response']['result']['Candidates']['FL']['TR'];
                }
                foreach ($data as $trkey2 => $trvalue2) {
                    if (key_exists('TL', $trvalue2)) {
                        if (key_exists(0, $trvalue2['TL'])) { // Getting improper data so
                            foreach ($trvalue2['TL'] as $tdkey2 => $tdvalue2) {
                                if ($tdvalue2['val'] == "TABULARROWID" && isset($tdvalue2['content'])) {
                                    $experience = new Experience();
                                    $experience->candidate_id = $consultant->id;
                                    $experience->CANDIDATEID = $id;
                                    $experience->TABULARROWID = $tdvalue2['content'];
                                    $experience->is_currently_working_here = "false";
                                }
                                if ($tdvalue2['val'] == "Occupation / Title" && isset($tdvalue2['content'])) {
                                    $experience->occupation = $tdvalue2['content'];
                                }
                                if ($tdvalue2['val'] == "Company" && isset($tdvalue2['content'])) {
                                    $experience->company = $tdvalue2['content'];
                                }
                                if ($tdvalue2['val'] == "Industry" && isset($tdvalue2['content'])) {
                                    $experience->industry = $tdvalue2['content'];
                                }
                                if ($tdvalue2['val'] == "Work Duration_From" && isset($tdvalue2['content'])) {
                                    $experience->work_duration = $tdvalue2['content'];
                                }
                                if ($tdvalue2['val'] == "Work Duration_To" && isset($tdvalue2['content'])) {
                                    $experience->work_duration_to = $tdvalue2['content'];
                                }
                                if ($tdvalue2['val'] == "I currently work here" && $tdvalue2['content'] != null) {
                                    /* [ Note : Zoho returning false if candidate is currently working here ] */
                                    $experience->is_currently_working_here = "true";
                                }
                                if ($tdvalue2['val'] == "Summary" && isset($tdvalue2['content'])) {
                                    $experience->summary = $tdvalue2['content'];
                                }
                            }
                            $experience->created_at = date('Y-m-d h:i:s', time());
                            $experience->updated_at = date('Y-m-d h:i:s', time());
                            $experience->save();
                        }
                    }
                }
                Session::flash('success', 'Experience details has been added');
                return redirect()->route('editConsultant', [$request->id]);
            } else {
                $monthList = $this->getMonths();
                $yearList = $this->getYears();
                $id = $request->id;
                $alreadyWorkingInCompany = Experience::where([
                            'CANDIDATEID' => $request->id,
                            'is_currently_working_here' => "true"
                        ])->whereNotNull('is_currently_working_here')->where('is_currently_working_here', '!=', "")->first();
                return view('backend.consultant.addexperience', compact('id', 'monthList', 'yearList', 'alreadyWorkingInCompany'));
            }
        } else {
            Session::flash('error', 'Consultant not found');
            return redirect()->route('consultant.index');
        }
    }

    /*
     * Update Experience
     * 
     */

    public function updateExperience(Request $request) {
        if ($request->isMethod('POST')) {
            $experience = Experience::where('TABULARROWID', $request->id)->first();
            if ($experience == null) {
                Session::flash('error', 'Experience data has been synced from zoho. Please try again to update');
                return redirect()->route('editConsultant', $request->CANDIDATEID);
            } else {
                $experience->company = $request->company;
                $experience->industry = $request->industry;
                $experience->occupation = $request->occupation;
                $experience->work_duration = $request->work_duration_from_month . '-' . $request->work_duration_from_year;
                if (isset($request->is_currently_working_here)) {
                    $experience->is_currently_working_here = "true";
                } else {
                    $experience->is_currently_working_here = "false";
                    $experience->work_duration_to = $request->work_duration_to_month . '-' . $request->work_duration_to_year;
                }
                $experience->summary = $request->summary;
                $experience->save();

                $zohoQuery = new ZohoqueryController();
                $xmlData = $zohoQuery->createUpdateExperienceXmlData($experience, $request, "update");
                $response = $zohoQuery->updateCandidateTabularRecord($experience->CANDIDATEID, $xmlData);

                Session::flash('success', 'Experience details has been updated');
                return redirect()->route('editConsultant', [$experience->CANDIDATEID]);
            }
        } else {
            $experience = Experience::where('TABULARROWID', $request->id)->first();
            $monthList = $this->getMonths();
            $yearList = $this->getYears();
            if ($experience != null) {
                $alreadyWorkingInCompany = Experience::where([
                            'CANDIDATEID' => $experience->CANDIDATEID,
                            'is_currently_working_here' => "true"
                        ])->whereNotNull('is_currently_working_here')->where('is_currently_working_here', '!=', "")->first();
                return view('backend.consultant.editexperience', compact('experience', 'monthList', 'yearList', 'alreadyWorkingInCompany'));
            } else {
                Session::flash('error', 'Experience data has been synced from zoho. Please try again to update');
                return redirect()->back();
            }
        }
    }

    /*
     * Delete experience
     * 
     */

    public function deleteExperience(Request $request) {
        $experience = Experience::where('TABULARROWID', $request->id)->first();
        if (count($experience)) {
            $consultant = Consultant::where("CANDIDATEID", $experience->CANDIDATEID)->where('is_delete', 0)->first();
            $consultant->is_synced = 0;
            $consultant->save();
            // Deleting experience From ZOHO
            $zohoQuery->deleteCandidateTabularRecord($experience->CANDIDATEID, $experience->TABULARROWID, 'Experience Details');
            // Deleting experience From DB
            $experience->delete();
            Session::flash('success', 'Experience details has been sucessfully deleted');
            return redirect()->route('editConsultant', [$consultant->CANDIDATEID]);
        } else {
            Session::flash('error', 'Experience data has been synced from zoho. Please try again to delete');
            return redirect()->back();
        }
    }

    /*
     * Add Education
     * 
     */

    public function addEducation(Request $request) {
        $id = $request->id;
        $consultant = Consultant::where("CANDIDATEID", $id)->where('is_delete', 0)->first();
        if (count($consultant)) {
            if ($request->isMethod('POST')) {
                $consultant->is_synced = 0;
                $consultant->save();

                $zohoQuery = new ZohoqueryController();
                $educationList = Education::where(['CANDIDATEID' => $request->id])->get();
                $xmlData = $zohoQuery->createUpdateEducationXmlData($educationList, $request, "add");
                $response = $zohoQuery->addCandidateTabularRecord($id, $xmlData);

                $tabularData = $zohoQuery->getCandidateTabularRecords($id, "Educational Details");

                /* Removing old educations */
                Education::where(['CANDIDATEID' => $request->id])->delete();
                if (key_exists(0, $tabularData['response']['result']['Candidates']['FL']['TR'])) {
                    $data = $tabularData['response']['result']['Candidates']['FL']['TR'];
                } else {
                    $data[0] = $tabularData['response']['result']['Candidates']['FL']['TR'];
                }
                foreach ($data as $trkey2 => $trvalue1) {
                    if (key_exists('TL', $trvalue1)) {
                        if (key_exists(0, $trvalue1['TL'])) { // Getting improper data so
                            foreach ($trvalue1['TL'] as $tdkey1 => $tdvalue1) {
                                if ($tdvalue1['val'] == "TABULARROWID" && isset($tdvalue1['content'])) {
                                    $education = new Education();
                                    $education->candidate_id = $consultant->id;
                                    $education->CANDIDATEID = $id;
                                    $education->TABULARROWID = $tdvalue1['content'];
                                    $education->currently_pursuing = "false";
                                }
                                if ($tdvalue1['val'] == "Institute / School" && isset($tdvalue1['content'])) {
                                    $education->institute = $tdvalue1['content'];
                                }
                                if ($tdvalue1['val'] == "Major / Department" && isset($tdvalue1['content'])) {
                                    $education->department = $tdvalue1['content'];
                                }
                                if ($tdvalue1['val'] == "Degree" && isset($tdvalue1['content'])) {
                                    $education->degree = $tdvalue1['content'];
                                }
                                if ($tdvalue1['val'] == "Duration_From" && isset($tdvalue1['content'])) {
                                    $education->duration_from = $tdvalue1['content'];
                                }
                                if ($tdvalue1['val'] == "Duration_To" && isset($tdvalue1['content'])) {
                                    $education->duration_to = $tdvalue1['content'];
                                }
                                if ($tdvalue1['val'] == "Currently pursuing" && isset($tdvalue1['content'])) {
                                    $education->currently_pursuing = "true";
                                }
                            }
                            $education->created_at = date('Y-m-d h:i:s', time());
                            $education->updated_at = date('Y-m-d h:i:s', time());
                            $education->save();
                        }
                    }
                }

                Session::flash('success', 'Education details has been added');
                return redirect()->route('editConsultant', [$education->CANDIDATEID]);
            } else {
                $monthList = $this->getMonths();
                $yearList = $this->getYears();
                $id = $request->id;
                $alreadyPersuing = Education::where([
                            'CANDIDATEID' => $request->id,
                            'currently_pursuing' => "true"
                        ])->whereNotNull('currently_pursuing')->where('currently_pursuing', '!=', "")->first();
                return view('backend.consultant.addeducation', compact('id', 'monthList', 'yearList', 'alreadyPersuing'));
            }
        } else {
            Session::flash('error', 'Consultant not found');
            return redirect()->route('consultant.index');
        }
    }

    /*
     * Update Education
     * 
     */

    public function updateEducation(Request $request) {
        if ($request->isMethod('POST')) {
            $education = Education::where('TABULARROWID', $request->id)->first();
            if ($education == null) {
                Session::flash('error', 'Education data has been synced from zoho. Please try again to update');
                return redirect()->route('editConsultant', $request->CANDIDATEID);
            } else {
                $education->institute = $request->institute;
                $education->department = $request->department;
                $education->degree = $request->degree;
                $education->duration_from = $request->duration_from_month . '-' . $request->duration_from_year;
                if (isset($request->is_currently_pursuing)) {
                    $education->currently_pursuing = "true";
                } else {
                    $education->currently_pursuing = "false";
                    $education->duration_to = $request->duration_to_month . '-' . $request->duration_to_year;
                }
                $education->save();
                $zohoQuery = new ZohoqueryController();
                $xmlData = $zohoQuery->createUpdateEducationXmlData($education, $request, "update");
                $response = $zohoQuery->updateCandidateTabularRecord($education->CANDIDATEID, $xmlData);

                Session::flash('success', 'Education details has been updated');
                return redirect()->route('editConsultant', [$education->CANDIDATEID]);
            }
        } else {
            $education = Education::where('TABULARROWID', $request->id)->first();
            $monthList = $this->getMonths();
            $yearList = $this->getYears();
            if ($education != null) {
                $alreadyPersuing = Education::where([
                            'CANDIDATEID' => $education->CANDIDATEID,
                            'currently_pursuing' => "true"
                        ])->whereNotNull('currently_pursuing')->where('currently_pursuing', '!=', "")->first();
                return view('backend.consultant.editeducation', compact('education', 'monthList', 'yearList', 'alreadyPersuing'));
            } else {
                Session::flash('error', 'Education data has been synced from zoho. Please try again to update');
                return redirect()->back();
            }
        }
    }

    /*
     * Delete Education
     * 
     */

    public function deleteEducation(Request $request) {
        $education = Education::where('TABULARROWID', $request->id)->first();
        if (count($education)) {
            $consultant = Consultant::where("CANDIDATEID", $education->CANDIDATEID)->where('is_delete', 0)->first();
            $consultant->is_synced = 0;
            $consultant->save();
            $zohoQuery = new ZohoqueryController();
            // Deleting education From ZOHO
            $zohoQuery->deleteCandidateTabularRecord($education->CANDIDATEID, $education->TABULARROWID, 'Educational Details');
            // Deleting education From DB
            $education->delete();
            Session::flash('error', 'Education details has been sucessfully deleted');
            return redirect()->route('editConsultant', [$consultant->CANDIDATEID]);
        } else {
            Session::flash('error', 'Education data has been synced from zoho. Please try again to delete');
            return redirect()->back();
        }
    }

    /*
     * Add Reference
     * 
     */

    public function addReference(Request $request) {
        $id = $request->id;
        $consultant = Consultant::where("CANDIDATEID", $id)->where('is_delete', 0)->first();
        if (count($consultant)) {
            if ($request->isMethod('POST')) {
                $consultant->is_synced = 0;
                $consultant->save();
                $zohoQuery = new ZohoqueryController();
                $references = Reference::where(['CANDIDATEID' => $request->id])->get();
                $xmlData = $zohoQuery->createUpdateReferenceXmlData($references, $request, "add");
                $response = $zohoQuery->addCandidateTabularRecord($id, $xmlData);

                $tabularData = $zohoQuery->getCandidateTabularRecords($id, "References");

                /* Removing old reference */
                Reference::where(['CANDIDATEID' => $request->id])->delete();
                if (key_exists(0, $tabularData['response']['result']['Candidates']['FL']['TR'])) {
                    $data = $tabularData['response']['result']['Candidates']['FL']['TR'];
                } else {
                    $data[0] = $tabularData['response']['result']['Candidates']['FL']['TR'];
                }
                foreach ($data as $trkey2 => $trvalue4) {
                    if (key_exists('TL', $trvalue4)) {
                        if (key_exists(0, $trvalue4['TL'])) { // Getting improper data so
                            foreach ($trvalue4['TL'] as $tdkey4 => $tdvalue4) {
                                if ($tdvalue4['val'] == "TABULARROWID" && isset($tdvalue4['content'])) {
                                    $reference = new Reference();
                                    $reference->candidate_id = $consultant->id;
                                    $reference->CANDIDATEID = $id;
                                    $reference->TABULARROWID = $tdvalue4['content'];
                                }
                                if ($tdvalue4['val'] == "Reference Name" && isset($tdvalue4['content'])) {
                                    $reference->name = $tdvalue4['content'];
                                }
                                if ($tdvalue4['val'] == "Reference Position" && isset($tdvalue4['content'])) {
                                    $reference->position = $tdvalue4['content'];
                                }
                                if ($tdvalue4['val'] == "Reference Company" && isset($tdvalue4['content'])) {
                                    $reference->company = $tdvalue4['content'];
                                }
                                if ($tdvalue4['val'] == "Reference Phone no." && isset($tdvalue4['content'])) {
                                    $reference->phone = $tdvalue4['content'];
                                }
                                if ($tdvalue4['val'] == "Reference Email" && isset($tdvalue4['content'])) {
                                    $reference->email = $tdvalue4['content'];
                                }
                            }
                            $reference->created_at = date('Y-m-d h:i:s', time());
                            $reference->updated_at = date('Y-m-d h:i:s', time());
                            $reference->save();
                        }
                    }
                }
                Session::flash('success', 'Reference details has been added');
                return redirect()->route('editConsultant', [$reference->CANDIDATEID]);
            } else {
                return view('backend.consultant.addreference', compact('reference', 'id'));
            }
        } else {
            Session::flash('error', 'Consultant not found');
            return redirect()->route('consultant.index');
        }
    }

    /*
     * Update Reference
     * 
     */

    public function updateReference(Request $request) {
        if ($request->isMethod('POST')) {
            $reference = Reference::where('TABULARROWID', $request->id)->first();
            if ($reference == null) {
                Session::flash('error', 'Reference data has been synced from zoho. Please try again to update');
                return redirect()->route('editConsultant', [$request->CANDIDATEID]);
            } else {
                $reference->name = $request->name;
                $reference->position = $request->position;
                $reference->company = $request->company;
                $reference->phone = $request->phone;
                $reference->email = $request->email;
                $reference->save();

                $zohoQuery = new ZohoqueryController();
                $xmlData = $zohoQuery->createUpdateReferenceXmlData($reference, $request, "update");
                $response = $zohoQuery->updateCandidateTabularRecord($reference->CANDIDATEID, $xmlData);
                Session::flash('success', 'Reference details has been updated');
                return redirect()->route('editConsultant', [$reference->CANDIDATEID]);
            }
        } else {
            $reference = Reference::where('TABULARROWID', $request->id)->first();
            if ($reference != null) {
                return view('backend.consultant.editreference', compact('reference'));
            } else {
                Session::flash('error', 'Reference data has been synced from zoho. Please try again to update');
                return redirect()->back();
            }
        }
    }

    /*
     * Delete Reference
     * 
     */

    public function deleteReference(Request $request) {
        $reference = Reference::where('TABULARROWID', $request->id)->first();
        if (count($reference)) {
            $consultant = Consultant::where("CANDIDATEID", $reference->CANDIDATEID)->where('is_delete', 0)->first();
            $consultant->is_synced = 0;
            $consultant->save();
            $zohoQuery = new ZohoqueryController();
            // Deleting education From ZOHO
            $zohoQuery->deleteCandidateTabularRecord($education->CANDIDATEID, $education->TABULARROWID, 'References');
            // Deleting education From DB
            $reference->delete();

            Session::flash('error', 'Reference details has been sucessfully deleted');
            return redirect()->route('editConsultant', [$consultant->CANDIDATEID]);
        } else {
            Session::flash('error', 'Reference data has been synced from zoho. Please try again to delete');
            return redirect()->back();
        }
    }

    public function getMonths() {
        return [
            "01" => "Jan",
            "02" => "Feb",
            "03" => "Mar",
            "04" => "Apr",
            "05" => "May",
            "06" => "Jun",
            "07" => "Jul",
            "08" => "Aug",
            "09" => "Sep",
            "10" => "Oct",
            "11" => "Nov",
            "12" => "Dec"
        ];
    }

    public function getYears() {
        $years = [];
        for ($i = 1950; $i <= 2050; $i++) {
            $years[$i] = $i;
        }
        return $years;
    }

    /*
     * Restore Consultant Account
     * 
     */

    public function restoreConsultantAccount(Request $request) {
        User::where('zoho_id', $request->id)->update([
            'is_delete' => 0
        ]);
        Consultant::where('CANDIDATEID', $request->id)->update([
            'is_delete' => 0
        ]);
        Session::flash('success', 'Account has been restored successfully');
        return redirect()->back();
    }

    /**
     * Upcoming interviews
     * 
     */
    public function upcomingInterviews(Request $request) {
        if ($request->session()->get('territoryadmin') == "Global") {
            $territory = ["MY", "PH"];
        } else {
            $territory = [$request->session()->get('territoryadmin')];
        }
        $model = Interviews::with([
                    'job',
                    'candidate',
                    'client',
                    'notes' => function ($query) {
                        $query->latest('created_time')->first();
                    }
                ])
                ->whereIn('territory', $territory)
                ->where('start_datetime', '>=', date('Y-m-d H:i:s'));
        $q = $request->get('s');
        $client = $request->get('client');
        $candidate = $request->get('candidate');
        if ($client != null) {
            $model = $model->where('client_id', $client);
        }
        if ($candidate != null) {
            $model = $model->where('candidate_id', $candidate);
        }
        if ($q && $q != '') {
            $model = $model->where(function($query) use ($q) {
                $query->where('interview_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('Interviewers', 'LIKE', '%' . $q . '%');
                $query->orWhere('venue', 'LIKE', '%' . $q . '%');
            });
        }
        $upcomingInterviews = $model->orderBy('last_activity_time','desc')->paginate(10)->appends($request->all());

        $clients = Clients::where([
                    'status' => 'Active'
                ])->orderBy('client_name', 'asc')->pluck('client_name', 'id')->toArray();
        $consultants = Consultant::select(
                                DB::raw("CONCAT(first_name,' ',last_name) AS name"), 'id')
                        ->where([
                            'status' => 'Active',
                            'is_delete' => 0
                        ])->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        return view('backend.consultant.upcoming_interviews', compact('upcomingInterviews', 'clients', 'consultants'));
    }
    /**
     * Associate Job Openings
     * 
     */
    public function associateJobOpenings(Request $request){
        $consultant = Consultant::where('CANDIDATEID', $request->id)->first();
        $query = Jobassociatecandidates::with(['job.client'])->where('CANDIDATEID',$request->id);
        $associateJobOpenings = $query->paginate(10)->appends($request->all());
        $statusList = $this->candidateStatusList();
        // echo "<pre>";
        // print_r($associateJobOpenings->toArray());
        // exit;
        return view('backend.consultant.associate_jobopenings',compact('associateJobOpenings','consultant','statusList'));
    }
    /**
     * Change consultant status
     * 
     */
    public function changeConsultantStatus(Request $request){
            if($request->job_ids != null || $request->job_ids != ""){
                $consultant = Consultant::where('CANDIDATEID', $request->consultant_id)->first();
                $zohoQuery = new ZohoqueryController();
                $territory = "MY";
                if ($consultant->territory != null) {
                    $territory = $consultant->territory;
                }
                $result = Token::where('territory', $territory)->first();
                $zohoQuery->token = $result->token;
                $jobIds = explode(",",$request->job_ids);
                foreach($jobIds as $key => $value){
                    $zohoQuery->updateCandidateStatus($request->consultant_id,$value, $request->status);    
                    Jobassociatecandidates::where([
                        'JOBOPENINGID' => $value,
                        'CANDIDATEID' => $request->consultant_id
                    ])->update([
                        'status' => $request->status
                    ]);
                }
                $consultant->candidate_status = $request->status;
                $consultant->save();
                Session::flash('success', 'Status changed successfully');
                return redirect()->route('associate-job-openings',[$request->consultant_id]);
            }else{
                Session::flash('error', 'Please select atleast one job');
                return redirect()->route('associate-job-openings',[$request->consultant_id]);
            }
        //    
    }
    public function candidateStatusList(){
        return [
            "New" => "New",
            "Waiting-for-Evaluation" => "Waiting-for-Evaluation",
            "Contacted" => "Contacted",
            "Contact in Future" => "Contact in Future",
            "Not Contacted" => "Not Contacted",
            "Attempted to Contact" => "Attempted to Contact",
            "Qualified" => "Qualified",
            "Unqualified" => "Unqualified",
            "Qualified Expat" => "Qualified Expat",
            "Unqualified Expat" => "Unqualified Expat",
            "Junk candidate" => "Junk candidate",
            "Associated" => "Associated",
            "Application Withdrawn" => "Application Withdrawn",
            "Rejected" => "Rejected",
            "Submitted-to-client" => "Submitted-to-client",
            "Approved by client" => "Approved by client",
            "Rejected by client" => "Rejected by client",
            "Interview-to-be-Scheduled" => "Interview-to-be-Scheduled",
            "Interview-Scheduled" => "Interview-Scheduled",
            "Interview-in-Progress" => "Interview-in-Progress",
            "Interviewed-Accepted" => "Interviewed-Accepted",
            "Interviewed-Rejected" => "Interviewed-Rejected",
            "To-be-Offered" => "To-be-Offered",
            "Offer-Accepted" => "Offer-Accepted",
            "Offer-Made" => "Offer-Made",
            "Offer-Declined" => "Offer-Declined",
            "Offer-Withdrawn" => "Offer-Withdrawn",
            "No-Show" => "No-Show",
            "Converted - Employee" => "Converted - Employee",
            "Converted - Contractor" => "Converted - Contractor"
        ];
    }
}