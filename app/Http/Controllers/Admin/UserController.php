<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
use Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if(Auth::user()->user_type == "Territory Admin"){
            return redirect()->route('dashboard');
        }
        // echo $request->session()->get('territoryadmin');
        $model = User::select('*');

        $q = $request->get('s');

        if ($q && $q != '') {
            $model = $model->where(function($query) use ($q) {
                $query->where('first_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('email', 'LIKE', '%' . $q . '%');
            });
        }

        $systemuser = $model->where('role_id', 3)->where('is_delete', 0)->orderby('id', 'desc')->paginate(10)->appends($request->all());

        return view('backend.systemuser.index', compact('systemuser'));
    }
   
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.systemuser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->status = $request->status;
        $user->user_type = $request->systemadmin_type;
        if ($user->user_type == "Territory Admin") {
            $user->territory = $request->territory;
        }
        $user->role_id = '3';
        $user->save();

        if ($user) {
            Session::flash('errorSuccess', 'System User created successfully');
        } else {
            Session::flash('errorFails', 'Error! something went to wrong!');
        }
        return redirect('admin/systemuser');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $editAdmin = User::find($id);
        return view('backend.systemuser.edit', compact('editAdmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $system_user = User::find($request->id);
        $system_user->first_name = $request->first_name;
        $system_user->last_name = $request->last_name;
        /* $system_user->email = $request->email; */
        if ($request->has('status')) {
            $system_user->status = $request->status;
        }
        $system_user->save();

        if ($system_user) {
            Session::flash('errorSuccess', 'System User updated successfully');
        } else {
            Session::flash('errorFails', 'Error! something went to wrong!');
        }
        return redirect('admin/systemuser');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function isUserEmailExists(Request $request) {
        $isValid = true;
        $message = '';

        $isExist = User::where('email', '=', $request->email)->first();

        if ($isExist) {
            $isValid = false;
            $message = 'Email already exists';
        }

        return response()->json([
                    'valid' => $isValid,
                    'message' => $message
        ]);
    }

    public function status(Request $request) {

        $id = $request->id;
        $user = User::where("id", $id)->first();

        if (!empty($user)) {
            if ($user->status == "Active") {
                $user->status = 'Inactive';
            } else {
                $user->status = 'Active';
            }
            $user->save();
        }
        return Response()->json(["success" => true, "message" => "Status changed successfully."]);
    }

    public function delete(Request $request) {
        $ids = explode(",", $request->ids);

        $User = User::whereIn('id', $ids)->get();

        if (!empty($User)) {

            User::whereIn('id', $ids)->delete();

            return Response()->json(["success" => true, "message" => "System User delete successfully."]);
        }
    }

    public function deleteSingleRecord(Request $request){
        User::where('id', $request->id)->delete();
        Session::flash('errorSuccess', 'System user has been deleted successfully');
        return redirect()->route('systemuser.index');
    }
    
    public function changepwd(Request $request) {
        $User = User::find($request->id);
        $User->password = Hash::make($request->password);
        $User->save();

        if ($User) {
            Session::flash('errorSuccess', 'Password updated successfully');
        } else {
            Session::flash('errorFails', 'Error! something went to wrong!');
        }
        return redirect('admin/systemuser');
    }

}