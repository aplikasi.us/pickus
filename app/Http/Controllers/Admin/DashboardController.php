<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clients;
use App\Consultant;
use App\Jobopenings;
use App\Clientwishlist;
use App\Interviews;
use App\Clientcontact;
use App\Candidateprofilerequests;

class DashboardController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        if (Auth::user()->user_type == "Territory Admin") {
            // $model = Clientcontact::with('user')->select('*')->where('territories', Auth::user()->territory);
            $clients = Clientcontact::where('territories', Auth::user()->territory)->where('is_delete', 0)->count();
            $consultant = Consultant::where('territory', Auth::user()->territory)->where('status', 'Active')->where('is_delete', 0)->count();
            $jobs = Jobopenings::with('contact')
                    ->whereHas('contact', function ($query) {
                        $query->where('is_delete', 0);
                    })->where('territory', Auth::user()->territory)
                    ->where('is_delete', 0)
                    ->count();
            $requestedProfiles = Candidateprofilerequests::with(['candidate', 'client']);
            if (Auth::user()->user_type == "Territory Admin") {
                $territory = Auth::user()->territory;
                $requestedProfiles = $requestedProfiles->whereHas('candidate', function ($query)use($territory) {
                    $query->where('territory', $territory);
                });
            }
            $requestedProfiles = $requestedProfiles->count();
            $requestedSummary = Consultant::where('territory', Auth::user()->territory)->where('is_summary_requested', '1')->where('is_delete', 0)->count();
            $pendingJobRequests = Jobopenings::with('contact')
                    ->whereHas('contact', function ($query) {
                        $query->where('is_delete', 0);
                    })
                    ->where('job_opening_status', 'Pending')->where('territory', Auth::user()->territory)
                    ->count();
            $upcomingInterview = Interviews::where('territory',Auth::user()->territory)
                ->where('start_datetime', '>=', date('Y-m-d H:i:s'))
                ->count();
        } else {
            
            $sessionterri = $request->session()->get('territoryadmin');
            if( $sessionterri != 'Global' ){
           
                // $model = Clientcontact::with('user')->select('*')->where('territories', $sessionterri);
                $clients = Clientcontact::where('territories', $sessionterri)->where('is_delete', 0)->count();
               
                $consultant = Consultant::where('territory', $sessionterri)->where('status', 'Active')->where('is_delete', 0)->count();
                $jobs = Jobopenings::with('contact')
                        ->whereHas('contact', function ($query) {
                            $query->where('is_delete', 0);
                        })->where('territory', $sessionterri)
                        ->where('is_delete', 0)
                        ->count();
                $requestedProfiles = Candidateprofilerequests::with(['candidate', 'client']);
                $requestedProfiles = $requestedProfiles->whereHas('candidate', function ($query)use($sessionterri) {
                    $query->where('territory', $sessionterri);
                });
                $requestedProfiles = $requestedProfiles->count();
                $requestedSummary = Consultant::where('territory', $sessionterri)->where('is_summary_requested', '1')->where('is_delete', 0)->count();
                $pendingJobRequests = Jobopenings::with('contact')
                        ->whereHas('contact', function ($query) {
                            $query->where('is_delete', 0);
                        })
                        ->where('job_opening_status', 'Pending')->where('territory', $sessionterri)
                        ->count();
                $upcomingInterview = Interviews::where('territory',$sessionterri)
                ->where('start_datetime', '>=', date('Y-m-d H:i:s'))
                ->count();
            }else{
                $clients = Clientcontact::where('is_delete', 0)->whereIn('territories', array('MY','PH'))->count();
                $consultant = Consultant::where('status', 'Active')->whereIn('territory', array('MY','PH'))->where('is_delete', 0)->count();
                $jobs = Jobopenings::with('contact')
                        ->whereHas('contact', function ($query) {
                            $query->where('is_delete', 0);
                        })
                        ->where('is_delete', 0)
                        ->count();
                $requestedProfiles = Candidateprofilerequests::count();
                $requestedSummary = Consultant::where('is_summary_requested', '1')->whereIn('territory', array('MY','PH'))->where('is_delete', 0)->count();
                $pendingJobRequests = Jobopenings::with('contact')
                        ->whereHas('contact', function ($query) {
                            $query->where('is_delete', 0);
                        })
                        ->where('job_opening_status', 'Pending')->whereIn('territory', array('MY','PH'))
                        ->count();
                $upcomingInterview = Interviews::whereIn('territory',array('MY','PH'))
                ->where('start_datetime', '>=', date('Y-m-d H:i:s'))
                ->count();
            }
            
        }
        return view('backend.home', compact('clients', 'consultant', 'jobs', 'requestedProfiles', 'requestedSummary', 'pendingJobRequests', 'upcomingInterview'));
    }
    /**
     * store session 
     */
    public function territorySession(Request $request) {
        $request->session()->put('territoryadmin',$request->territory);
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}