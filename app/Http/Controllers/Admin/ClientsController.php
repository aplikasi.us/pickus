<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clients;
use App\Clientwishlist;
use App\Clientcontact;
use App\Jobopenings;
use App\Jobassociatecandidates;
use App\Recentlyviewedconsultant;
use Illuminate\Support\Facades\DB;
use App\Notification;
use App\User;
use App\Attachment;
use App\Token;
use Session;
use App\Candidates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Emailtemplate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Humantech\Zoho\Recruit\Api\Client\Client;
use App\Candidateprofilerequests;
use App\Http\Controllers\ZohoqueryController;
use App\Modules\Users\Http\Controllers\UsersController;

class ClientsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $token;

    public function __construct()
    {
        $result = Token::first();
        $this->token = $result->token;
    }

    public function index(Request $request)
    {
        if (Auth::user()->user_type == "Territory Admin") {
            $model = Clientcontact::with('user')->select('*')->where('territories', Auth::user()->territory);
        } else {
            $sessionterri = $request->session()->get('territoryadmin');
            if ($sessionterri != 'Global') {
                $model = Clientcontact::with('user')->select('*')->where('territories', $sessionterri);
            } else {
                $model = Clientcontact::with('user')->select('*');
            }
        }

        $q = $request->get('s');
        $account_status = $request->get('account_status');
        if ($account_status && $account_status != "") {
            if ($account_status == 1) {
                $model->where('is_delete', 0);
            } else {
                $model->where('is_delete', 1);
            }
        }
        $clientid = $request->get('clientid');
        if (isset($clientid) && $clientid != null) {
            $model->where('CLIENTID', $clientid);
        }
        if ($q && $q != '') {
            $model = $model->where(function ($query) use ($q) {
                $query->where('client_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('email', 'LIKE', '%' . $q . '%');
                $query->orWhere('mobile', 'LIKE', '%' . $q . '%');
                $query->orWhere(DB::raw("CONCAT(first_name,' ',last_name)"), 'LIKE', '%' . $q . '%');
            });
        }
        $ClientRequested = Clientwishlist::pluck('CLIENTID')->toArray();
        $client = $model->orderby('id', 'desc')->paginate(10)->appends($request->all());
        if (count($client)) {
            foreach ($client as $key => $value) {
                $user = User::where('zoho_id', $value->CLIENTID)->where('role_id', 1)->first();
                if ($user != null) {
                    $value->email = $user->email;
                } else {
                    $value->email = "";
                }
            }
        }
        return view('backend.clients.index', compact('client', 'ClientRequested'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->role_id = 1;
        $request->territory = 'MY';
        $request->isAdmin = true;
        $userController = new UsersController();
        return $userController->clientRegistration($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteClient(Request $request)
    {
        if (Auth::user()->role_id == 3) {
            $response = $this->deleteContactFromZoho($request->id);
            if (!key_exists('error', $response['response'])) {
                Clientcontact::where('CONTACTID', $request->id)->delete();
                Clientwishlist::where('CONTACTID', $request->id)->delete();
                $jobIds = Jobopenings::where('CONTACTID', $request->id)->pluck('id')->toArray();
                Jobopenings::where('CONTACTID', $request->id)->delete();
                if (count($jobIds)) {
                    Jobassociatecandidates::whereIn('job_id', $jobIds)->delete();
                }
                Notification::where('sender_id', $request->id)->orWhere('receiver_id', $request->id)->delete();
                Recentlyviewedconsultant::where('CONTACTID', $request->id)->delete();
                User::where('CONTACTID', $request->id)->delete();
                Session::flash('success', 'Client contact has been deleted successfully');
                return redirect()->back();
            } else {
                Session::flash('error', 'Client contact not able to delete from zoho');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('dashboard');
        }
    }

    public function deleteContactFromZoho($recordId)
    {
        $contact = Clientcontact::where('CONTACTID', $recordId)->first();
        $territory = "MY";
        if ($contact->territory != null) {
            $territory = $contact->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        $url = 'https://recruit.zoho.com/recruit/private/json/Contacts/deleteRecords?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=' . $recordId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);  // Execute a cURL request
        $response = json_decode($response, true);
        return $response;
    }

    public function status(Request $request)
    {

        $id = $request->id;
        $status = $request->status;
        $user = User::where("CONTACTID", $id)->first();

        if (!empty($user)) {
            if ($status == "inactive") {
                $user->is_delete = 1;
            } else {
                $user->is_delete = 0;
            }
            $user->save();
        }

        $client = Clientcontact::where("CONTACTID", $id)->first();

        if (!empty($client)) {
            if ($status == "inactive") {
                $client->is_delete = 1;
            } else {
                $client->is_delete = 0;
            }
            $client->is_synced = 0;
            $client->save();
        }
        return Response()->json(["success" => true, "message" => "Status changed successfully."]);
    }

    /**
     * View Requested Profile list
     * 
     */
    public function viewRequestProfile(Request $request)
    {
        $CLIENTID = $request->id;
        $clientRequested = Clientwishlist::where('CLIENTID', $CLIENTID)->paginate(10);
        return view('backend.clients.viewrequestedprofile', compact('clientRequested'));
    }

    /**
     * View Requested Profile Index
     * 
     */
    public function indexRequestProfile(Request $request)
    {
        // $clientRequested = Clientwishlist::orderByRaw("FIELD(is_request_approve_by_admin, '1')")->orderBy('updated_at', 'desc')->paginate(10);
        $model = Candidateprofilerequests::with(['candidate', 'client'])
            ->select('*')
            ->whereHas('candidate', function ($query) {
                $query->where('is_delete', 0);
            });

        $consultant_id = $request->get('consultant_id');
        if ($request->has('consultant_id') && $consultant_id != '') {
            $model = $model->where('CANDIDATEID', $consultant_id);
        }
        $client_id = $request->get('client_id');
        if ($request->has('client_id') && $client_id != '') {
            $model = $model->where('CONTACTID', $client_id);
        }
        $status = $request->get('status');
        if ($request->has('status') && $status != '') {
            $model = $model->where('request_status', $status);
        } else {
            $model = $model->whereIn('request_status', ['New', 'Approved']);
        }

        if (Auth::user()->user_type == "Territory Admin") {
            $territory = Auth::user()->territory;
            $model = $model->whereHas('candidate', function ($query) use ($territory) {
                $query->where('territory', $territory);
            });
        }
        $clientRequested = $model->groupBy('CANDIDATEID')->orderBy('id', 'desc')->paginate(10)->appends($request->all());

        $clientarray = User::where('role_id', '1')->orderBy('company_name')->pluck('company_name', 'CONTACTID')->toArray();
        $requeststatus = array('New' => 'New', 'Approved' => 'Approved');
        $consultantarray = User::select('zoho_id', \DB::raw("concat(first_name,' ',last_name) as full_name"))->where('role_id', '2')->orderBy('first_name')->pluck('full_name', 'zoho_id')->toArray();

        // echo '<pre>';
        // print_r($clientRequested->toArray());
        return view('backend.clients.requestedprofileindex', compact('clientRequested', 'clientarray', 'consultantarray', 'requeststatus'));
    }

    /*     * *
     * 
     * Formatted Resume download
     * 
     * 
     */

    public function formattedResumeDownload(Request $request)
    {

        $attachment = Attachment::where('zoho_id', $request->id)->where('category', "Formatted Resume")->first();
        if ($attachment != null) {
            $isAlreadyDownload = 0;
            /* Downloading file from local storage */
            $destination_folder = public_path('cv/' . $attachment->downloaded_cvname);
            if ($attachment->downloaded_cvname != null) {
                if (file_exists($destination_folder)) {
                    $isAlreadyDownload = 1;
                }
            }
            /* Downloading file from zoho */
            if ($isAlreadyDownload == 0) {
                $candidate = Candidates::where('id', $attachment->candidate_id)->first();
                $territory = "MY";
                if ($candidate != null) {
                    if ($candidate->territory != null) {
                        $territory = $candidate->territory;
                    }
                }
                $result = Token::where('territory', $territory)->first();
                $this->token = $result->token;

                /* Dowloading File */
                $url = "https://recruit.zoho.com/recruit/private/xml/Candidates/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $attachment->attachment_id;
                $ext = pathinfo($attachment->filename, PATHINFO_EXTENSION);
                /* Storing in public cv folder */
                $fileName = time() . '.' . $ext;
                $destination_folder = public_path('cv/' . $fileName);
                file_put_contents("$destination_folder", fopen($url, 'r'));
                /* Save downwloaded file name in DB */
                $attachment->downloaded_cvname = $fileName;
                $attachment->save();
            }
            // print_r($destination_folder);
            // exit;
            return response()->download($destination_folder);
            Session::flash('success', 'Profile has been downloaded successfully.');
            // return redirect()->route('indexRequestProfile');
            // Session::flash('success', 'Formatted CV downloaded');
        } else {
            Session::flash('error', 'Invalid profile format. It cannot be approved.');
            return redirect()->route('indexRequestProfile');
        }
    }

    /**
     * multiple Formatted Resume download
     * 
     */
    public function multipleFormattedResumeDownload(Request $request)
    {
        $ids = $request->get('ids');
        if ($ids) {
            $requestprofile = explode(',', $ids);
            $failureCount = 0;
            for ($i = 0; $i < count($requestprofile); $i++) {
                $clientRequested = Candidateprofilerequests::where('id', $requestprofile[$i])->first();
                $attachment = Attachment::where('zoho_id', $clientRequested->CANDIDATEID)->where('category', "Formatted Resume")->first();
                if ($attachment != null) {
                    $isAlreadyDownload = 0;
                    /* Downloading file from local storage */
                    $destination_folder = public_path('cv/' . $attachment->downloaded_cvname);
                    if ($attachment->downloaded_cvname != null) {
                        if (file_exists($destination_folder)) {
                            $isAlreadyDownload = 1;
                        }
                    }
                    /* Downloading file from zoho */
                    if ($isAlreadyDownload == 0) {
                        /* Dowloading File */
                        $url = "https://recruit.zoho.com/recruit/private/xml/Candidates/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $attachment->attachment_id;
                        $ext = pathinfo($attachment->filename, PATHINFO_EXTENSION);
                        /* Storing in public cv folder */
                        $fileName = time() . '.' . $ext;
                        $destination_folder = public_path('cv/' . $fileName);
                        file_put_contents("$destination_folder", fopen($url, 'r'));
                        /* Save downwloaded file name in DB */
                        $attachment->downloaded_cvname = $fileName;
                        $attachment->save();
                    }
                    // response()->download($destination_folder);
                } else {
                    $failureCount++;
                }
            }
            if ($failureCount == 0) {
                Session::flash('success', 'Profile has been downloaded successfully.');
                $data['success'] = true;
                $data['message'] = 'Profile has been downloaded successfully.';
            } else {
                Session::flash('error', 'Formatted resume does not exists in some profile. Kindly download the formatted resume & then Proceed to Approve or cancel it');
                $data['success'] = true;
                $data['message'] = 'Formatted resume does not exists in some profile. Kindly download the formatted resume & then Proceed to Approve or cancel it';
            }
        } else {
            Session::flash('error', 'some error during downloaded process.');
            $data['success'] = false;
            $data['message'] = 'some error during deleting process.';
        }
        return response()->json([$data]);
    }

    /**
     * Cancelled Requsted profile
     * 
     */
    public function cancelRequestProfile(Request $request)
    {
        $id = $request->id;
        $clientRequested = Candidateprofilerequests::where('id', $id)->first();
        if (count($clientRequested)) {

            $data['Request Status'] = "Fake request";
            $zohoQuery = new ZohoqueryController();

            $territory = "MY";
            if ($clientRequested->territory != null) {
                $territory = $clientRequested->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $zohoQuery->token = $result->token;

            $response = $zohoQuery->updateProfileRequest($clientRequested->CUSTOMMODULE2_ID, $data);
            if ($response['status'] == "500") {
                Candidateprofilerequests::where('CUSTOMMODULE2_ID', $clientRequested->CUSTOMMODULE2_ID)->delete();
                Session::flash('error', 'This profile request has been deleted from zoho.');
                return redirect()->back();
            } else if ($response['status'] == "401") {
                Session::flash('error', $response['message']);
                return redirect()->back();
            } else {
                $clientRequested->request_status = "Fake request";
                $clientRequested->save();
                $contact = Clientcontact::where('CONTACTID', $clientRequested->CONTACTID)->first();
                if ($contact != null) {
                    $notificationData['db_sender_id'] = "";
                    $notificationData['db_receiver_id'] = $contact->id;
                    $notificationData['sender_id'] = "";
                    $notificationData['receiver_id'] = $clientRequested->CONTACTID;
                    $notificationData['message'] = 'You request for ' . $clientRequested->candidate_uid . ' has been cancelled';
                    $notificationData['type'] = "request_profile";
                    $notificationData['to'] = "Client";
                    if ($clientRequested->client->territories != null) {
                        $notificationData['redirect_url'] = url($clientRequested->client->territories . '/consultants-details/' . Crypt::encryptString($clientRequested->candidate->CANDIDATEID));
                    }
                    $this->sendNotification($notificationData);
                }
                Session::flash('success', 'Your request has been cancelled');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Your request has been wrong');
            return redirect()->back();
        }
    }

    /**
     * Approve Requsted profile
     * 
     */
    public function approveRequestProfile(Request $request)
    {
        $id = $request->id;
        $clientRequested = Candidateprofilerequests::where('id', $id)->first();
        if (count($clientRequested)) {
            $attachment = Attachment::where('zoho_id', $clientRequested->CANDIDATEID)->where('category', "Formatted Resume")->first();
            if ($attachment != null) {
                $zohoQuery = new ZohoqueryController();
                $data['Request Status'] = "Approved";
                $response = $zohoQuery->updateProfileRequest($clientRequested->CUSTOMMODULE2_ID, $data);

                if ($response['status'] == "500") {
                    Candidateprofilerequests::where('CUSTOMMODULE2_ID', $clientRequested->CUSTOMMODULE2_ID)->delete();
                    Session::flash('success', 'This profile request has been deleted from zoho.');
                    return redirect()->back();
                } else if ($response['status'] == "401") {
                    Session::flash('error', $response['message']);
                    return redirect()->back();
                } else {
                    $isAlreadyDownload = 0;
                    /* Downloading file from local storage */
                    $destination_folder = public_path('cv/' . $attachment->downloaded_cvname);
                    if ($attachment->downloaded_cvname != null) {
                        if (file_exists($destination_folder)) {
                            $isAlreadyDownload = 1;
                        }
                    }

                    $territory = "MY";
                    if ($clientRequested->territory != null) {
                        $territory = $clientRequested->territory;
                    }
                    $result = Token::where('territory', $territory)->first();
                    $zohoQuery->token = $result->token;
                    $this->token = $result->token;
                    /* Downloading file from zoho */
                    if ($isAlreadyDownload == 0) {
                        /* Dowloading File */
                        $url = "https://recruit.zoho.com/recruit/private/xml/Candidates/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $attachment->attachment_id;
                        $ext = pathinfo($attachment->filename, PATHINFO_EXTENSION);
                        /* Storing in public cv folder */
                        $fileName = time() . '.' . $ext;
                        $destination_folder = public_path('cv/' . $fileName);
                        file_put_contents("$destination_folder", fopen($url, 'r'));
                        /* Save downwloaded file name in DB */
                        $attachment->downloaded_cvname = $fileName;
                        $attachment->save();
                    }

                    $clientRequested->request_status = "Approved";
                    $clientRequested->save();
                    $contact = Clientcontact::where('CONTACTID', $clientRequested->CONTACTID)->first();
                    if ($contact != null) {
                        $notificationData['db_sender_id'] = "";
                        $notificationData['db_receiver_id'] = $contact->id;
                        $notificationData['sender_id'] = "";
                        $notificationData['receiver_id'] = $clientRequested->CONTACTID;
                        $notificationData['message'] = 'Your request for ' . $clientRequested->candidate_uid . ' profile has been approved. You can download resume now.';
                        $notificationData['type'] = "request_profile";
                        $notificationData['to'] = "Client";
                        if ($contact->territories != null) {
                            $notificationData['redirect_url'] = url($contact->territories . '/consultants-details/' . Crypt::encryptString($clientRequested->CANDIDATEID));
                        }
                        $this->sendNotification($notificationData);
                    }
                    Session::flash('success', 'Request has been approved. Client have access to profile now.');
                    return redirect()->back();
                }
            } else {
                Session::flash('error', 'Formatted resume does not exists. Kindly download the formatted resume & then Proceed to Approve or cancel it');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Your request has been wrong');
            return redirect()->back();
        }
    }

    /**
     * Multiple delete rquest profile
     */
    public function multipleDeleteRequest(Request $request)
    {
        $ids = $request->get('ids');

        if ($ids) {
            $requestprofile = explode(',', $ids);
            $customeModule2Ids = [];
            $zohoQuery = new ZohoqueryController();
            for ($i = 0; $i < count($requestprofile); $i++) {
                $profile = Candidateprofilerequests::where('id', $requestprofile[$i])->first();
                if ($profile != null) {

                    $territory = "MY";
                    if ($profile->territory != null) {
                        $territory = $profile->territory;
                    }
                    $result = Token::where('territory', $territory)->first();
                    $zohoQuery->token = $result->token;

                    $zohoQuery->deleteProfileRequest($profile->CUSTOMMODULE2_ID);
                    //                    $customeModule2Ids[] = $profile->CUSTOMMODULE2_ID;
                    Candidateprofilerequests::where('id', $requestprofile[$i])->delete();
                }
            }
            //            $recordIds = implode(';', $customeModule2Ids);
            //            $zohoQuery = new ZohoqueryController();
            //            
            //            $zohoQuery->deleteProfileRequest($recordIds);

            Session::flash('success', 'Selected profile access requests has been deleted successfully.');
            $data['success'] = true;
            $data['message'] = 'Selected profile access requests has been deleted successfully.';
        } else {
            Session::flash('error', 'some error during deleting process.');
            $data['success'] = false;
            $data['message'] = 'some error during deleting process.';
        }
        return response()->json([$data]);
    }

    /**
     * Notification Send 
     */
    public function sendNotification($data = [])
    {
        $notification = new Notification();

        $notification->s_id = $data['db_sender_id'];
        $notification->sender_id = $data['sender_id'];
        if (isset($data['db_sender_id'])) {
            $notification->r_id = $data['db_receiver_id'];
            $notification->receiver_id = $data['receiver_id'];
        }
        if (isset($data['redirect_url'])) {
            $notification->redirect_url = $data['redirect_url'];
        }
        $notification->message = $data['message'];
        $notification->type = $data['type'];
        $notification->to = $data['to'];
        $notification->created_at = date('Y-m-d H:i:s', time());
        $notification->updated_at = date('Y-m-d H:i:s', time());
        $notification->save();
        return;
    }

    /*
     * Contact change password
     * 
     */

    public function contactChangePassword(Request $request)
    {
        $template = EmailTemplate::where('name', 'generate_contact_password')->first();
        if ($template != null) {
            $user = User::where('CONTACTID', $request->id)->first();
            if ($user != null) {
                $emaildata['email'] = $user->email;
                $emaildata['contact_name'] = $user->first_name . ' ' . $user->last_name;
                $emaildata['subject'] = $template->subject;
                $emaildata['password'] = $this->randomPassword();
                Mail::send([], [], function ($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], $emaildata['contact_name'])
                        ->subject($emaildata['subject'])
                        ->setBody($template->parse($emaildata), 'text/html');
                });
                $user->password = \Hash::make($emaildata['password']);
                $user->save();
                Session::flash('success', 'Account password has been generated successfully');
                return redirect()->back();
            } else {
                Session::flash('error', 'No such regitered user found');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Please set email template');
            return redirect()->back();
        }
    }

    public function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /*
     * Restore client contact account
     * 
     */

    public function restoreClientContactAccount(Request $request)
    {
        User::where('CONTACTID', $request->id)->update([
            'is_delete' => 0
        ]);
        Clientcontact::where('CONTACTID', $request->id)->update([
            'is_delete' => 0
        ]);
        Session::flash('success', 'Account has been restored successfully');
        return redirect()->back();
    }
}