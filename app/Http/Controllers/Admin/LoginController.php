<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;  
use Illuminate\Support\Facades\Route;
use Hash;
use Session;

class LoginController extends Controller
{
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
    public function index(Request $request){
        // if( Route::currentRouteName() == "jobs.edit"){
        //     $request->session()->put('current_url', url()->previous());
        // }
        $request->session()->put('current_url', url()->previous());
        if(Auth::check()){
            if (Auth::user()->role_id == 3) {
                return redirect('admin/dashboard');
            }
        }
       return view('backend.login');
    }

    public function check(Request $request){

        if($request->input('email')){
            $username = $request->input('email');
            $password = $request->input('password');
            // $redirect_url = $request->input('redirect_url');
            $count = User::where(['email'=>$username])->first();
            
            if(password_verify($password, $count['password'])){
                if($count['role_id'] == 3){
                    if($count['is_delete'] == 1){
                        $request->session()->flash('errorLogin', 'Your account is deleted.');
                        return view('backend.login');
                    }
                    if($count['status'] == 'Inactive'){
                        $request->session()->flash('errorLogin', 'Your account is deactive.');
                        return view('backend.login');
                    }
                    $countUser = User::where(['email'=>$username])->first();
                    if($countUser){
                        Auth::loginUsingId($count->id);
                        $getvalue = $request->session()->get('current_url');
                        $request->session()->put('territoryadmin','Global');
                        $request->session()->regenerate();
//                        if(isset($getvalue) && !empty($getvalue)){
//                            $request->session()->forget('current_url');
//                            return redirect($getvalue);
//                        }else{
                            return redirect()->intended('admin/dashboard');
//                        }
                        
                    }else{
                        $request->session()->flash('errorLogin', 'Invalid Email or Password.');
                        return view('backend.login');
                    }
                }else{
                    $request->session()->flash('errorLogin', 'Invalid Email or Password.');
                    return view('backend.login');    
                }
            }else{
                $request->session()->flash('errorLogin', 'Invalid Email or Password.');
                return view('backend.login');
            }  
        }else{
            $request->session()->flash('errorLogin', 'Invalid Email or Password.');
        }
        return view('backend.login');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->forget('territoryadmin');
        Session::flash('success', 'You have been logged out successfully.');
        return redirect('admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }
}