<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Humantech\Zoho\Recruit\Api\Client\Client;
use App\Token;
use CURLFile;
use App\Cronlog;

class ZohoqueryController extends Controller {

    public $token;
    
    public function __construct() {
        $result = Token::first();
        $this->token = $result->token;        
    }

    /* Getting All Clients 
     * 
     *  ZOHO returns default 20 pagination so calling recursive function to get all clients
     */

    public function getClientRecords($from = 1, $to = 20, $mainCount = 0, $data = array()) {
        $cronLog = Cronlog::where('cron_name','zoho-to-aplikasi')->where('module_name','client_profile')->first();
        $client = new Client($this->token);
        $queryResult = $client->getRecords('Clients', array(
            'selectColumns' => 'Clients(CLIENTID,Client Name,Parent Client,Contact Number,Fax,Account Manager,Website,Industry,About,Revenue Type,Currency,Territories,Last Activity Time,Billing Street,Billing City,Billing State,Billing Code,Billing Country,Shipping Street,Shipping City,Shipping State,Shipping Code,Shipping Country,Created By,Modified By,Source,Client Contract,Client Logo,Others)', 'fromIndex' => $from, 'toIndex' => $to,'lastModifiedTime' => $cronLog->last_run
        ));
        $data[] = $queryResult;
        $mainCount += count($queryResult);
        if (count($queryResult) >= 20) {
            $from = $mainCount + 1;
            $to = $from + 19;
            return $this->getClientRecords($from, $to, $mainCount, $data);
        }
        return $data;
    }

    /*
     * Getting Client Attachments
     */

    public function getAttachments($entity, $zohoId) {
        $client = new Client($this->token);
        $attachments = $client->getRelatedRecords(
                'Attachments', "$entity", $zohoId
        );
        return $attachments;
    }

    /*
     * Download client attachements
     * 
     */

    public function downloadClientAttachments($attachmentId) {
        $client = new Client($this->token);
        $response = $client->downloadFile(
                $attachmentId, public_path('cv')
        );
        return $response;
    }

    /*
     * Getting Job Openings For a Client
     *  
     */

    public function getJobOpenings($clientName) {
        $client = new Client($this->token);
        $queryResult = $client->getSearchRecords(
                'JobOpenings', 'JobOpenings(Job ID,Job Opening ID,Posting Title,Job Type,Client Name,Job Mode,Date Opened,Contact Name,City,Target Date,Job Duration (months),Zip Code,State,Country,Salary,Industry,Job Base Rate,Work Experience,Key Skills,Assigned Recruiter,Job Description,Internal Hire,Skillset,Job Requirements,Last Activity Time,Job Benefits,Job Opening ID,Category,Currency,Territory,Date Closed,Job Opening Status,Account Manager,Created By,Modified By,Revenue stream,Number of Positions,Revenue per Position,Expected Revenue,Actual Revenue,Missed Revenue,Job Summary,Others,Publish in Us)', '(Client Name|is|' . $clientName . ')'
        );
        return $queryResult;
    }

    /*
     * Get job associate Candidates
     * 
     */

    public function getJobAssociateCandidates($job_id) {
        $client = new Client($this->token);
        return $client->getAssociatedCandidates($job_id);
    }

    /*
     * Getting interviews
     * 
     */

    public function getInterviews($from = 1, $to = 20, $mainCount = 0, $data = array()) {
        $cronLog = Cronlog::where('cron_name','zoho-to-aplikasi')->where('module_name','client_interview')->first();
        
        $client = new Client($this->token);
        $queryResult = $client->getRecords('Interviews', array(
            'selectColumns' => 'Interviews(Interview Name,Candidate Name,Client Name,Posting Title,Interviewer(s),Type,Start DateTime,End DateTime,Venue,Reminder,Schedule Comments,Last Activity Time,Interview Status,Interview Owner,Created By,Modified By,Territory,Others,ICS)', 'fromIndex' => $from, 'toIndex' => $to
        ));
        $data[] = $queryResult;
        $mainCount += count($queryResult);
        if (count($queryResult) >= 20) {
            $from = $mainCount + 1;
            $to = $from + 19;
            return $this->getInterviews($from, $to, $mainCount, $data);
        }
        return $data;
    }

    /*
     * Getting Candidate Data
     * 
     */

    public function getCandidateData($from = 1, $to = 20, $mainCount = 0, $data = array()) {
        $cronLog = Cronlog::where('cron_name','zoho-to-aplikasi')->where('module_name','candidate_profile')->first();
        $client = new Client($this->token);
        $queryResult = $client->getRecords('Candidates', array(
            'selectColumns' => 'Candidates(Candidate ID,First Name,Salutation,Email,Last Name,Skype ID,Mobile,Twitter,Website,Internal Hire,Candidate ID,Publish in Us,Secondary Email,Block,Nationality,Currency,Territory,SAP Job Title,Highest Qualification Held,Experience in Years,Current Employer,Notice Period (Days),Expected Salary,Employment Type,Base Rate (BR),Current Salary,Reserved Base Rate,Skill Set,Language,Certifications & Trainings,Additional Info,Category,Last Activity Time,Available for Contract,Availability Date,Willing to travel,Full-time,Part-time,Project,Support,Client,Client Contact,Client Billing Rate,Client Billing Mode,Consultant Pay Rate,Consultant Pay Mode,Current Project,Start Date,Notes,End Date,Revenue Stream,Parked Candidate,Candidate Status,Candidate Owner,Created By,Modified By,Source,Privacy Policy and T&C,Street,Zip Code,City,State,Country,Resume,Formatted Resume,Cover Letter,Others,HCL Formatted Resume)', 'fromIndex' => $from, 'toIndex' => $to,'lastModifiedTime' => $cronLog->last_run
        ));
        $data[] = $queryResult;
        $mainCount += count($queryResult);
        if (count($queryResult) >= 20) {
            $from = $mainCount + 1;
            $to = $from + 19;
            return $this->getCandidateData($from, $to, $mainCount, $data);
        }
        return $data;
    }

    /*
     * Getting candidate tabular data 
     * 
     */

    public function getCandidateTabularRecords($candidateId,$tabularNames = 'Experience Details, Educational Details, Contract History, References') {
        $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/getTabularRecords?authtoken=' . $this->token . '&scope=recruitapi&id=' . $candidateId . '&tabularNames=('.$tabularNames.')';
        // Initialise a cURL handle
        $url = str_replace(" ", '%20', $url);
        $ch = curl_init();
        // Set any other cURL options that are required
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        return json_decode($response, true);
    }

    /*
     * Download Profile Photo
     * 
     */

    public function donwloadProfilePhoto($module, $zohoId) {
        $code = 200;
        $url = 'https://recruit.zoho.com/recruit/private/json/' . $module . '/downloadPhoto?authtoken=' . $this->token . '&scope=recruitapi&id=' . $zohoId . '&version=2';
        // Initialise a cURL handle
        $ch = curl_init();
        // Set any other cURL options that are required
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        if ($this->isJSON($response)) {
            $code = 401;
        } else {
            // write file
            if(file_exists(public_path("/profile_image") . "/" . "$zohoId.png")){
                unlink(public_path("/profile_image") . "/" . "$zohoId.png");
            }
            file_put_contents(public_path("/profile_image") . "/" . "$zohoId.png", $response, FILE_APPEND);
        }
        return ['code' => $code, 'filename' => "$zohoId.png"];
    }

    public function isJSON($string) {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    public function updateClient($clientId, $data) {
        $client = new Client($this->token);
        $queryResult = $client->updateRecords('Clients', $clientId, $data);
        return $queryResult;
    }
    public function updateClientContact($contactId, $data) {
        $client = new Client($this->token);
        $queryResult = $client->updateRecords('Contacts', $contactId, $data);
        return $queryResult;
    }

    public function updateJob($jobOpeningId, $data) {
        $client = new Client($this->token);
        $queryResult = $client->updateRecords('JobOpenings', $jobOpeningId, $data);
        return $queryResult;
    }

    public function addJob($data) {
        $client = new Client($this->token);
        $queryResult = $client->addRecords('JobOpenings', $data);

        $job = $client->getSearchRecords(
                'JobOpenings', 'JobOpenings(Job ID,Job Opening ID,Posting Title)', '(Posting Title|contains|*' . $data['Posting Title'] . '*)'
        );
        return $job;
    }
    public function addAssociatedCandidateToJob($data) {
        $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/associateJobOpening?authtoken=' . $this->token . '&scope=recruitapi&version=2';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "jobIds=" . $data['jobIds'] . "&candidateIds=" . $data['candidateIds']);

        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        return $response;
    }
    public function withdrawJobApplication($data) {
        $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/changeStatus?authtoken=' . $this->token . '&scope=recruitapi&version=2';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "jobId=" . $data['jobIds'] . "&candidateIds=" . $data['candidateIds'] . "&candidateStatus=" . urlencode($data['status']));

        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        return $response;
    }

    /*
     * Creating Profile Request
     * CustomModule2 = Requests Module in Zoho
     */

    public function createProfileRequest($data) {
        $client = new Client($this->token);
        $queryResult = $client->addRecords('CustomModule2', $data);
        return $queryResult;
    }

    /*
     * Update candidate data
     * 
     */

    public function updateCandidate($candidateId, $data) {
        $client = new Client($this->token);
        $queryResult = $client->updateRecords('Candidates', $candidateId, $data);
        return $queryResult;
    }

    /*
     * Upload candidate resume [ Overwrite if exist ]
     * 
     */

    public function updateAttachment($candidateId, $fileName, $type) {
        $file = public_path('cv/' . $fileName);
        $ch = curl_init();
        $cFile = new CURLFile("$file", 'application/pdf,application/msword');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://recruit.zoho.com/recruit/private/json/Candidates/uploadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&type=$type");
        curl_setopt($ch, CURLOPT_POST, true);
        $post = array("id" => $candidateId, "content" => $cFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        return json_decode($response, true);
    }

    /*
     * Update candidate experience details
     * 
     */

    public function updateCandidateTabulardata($candidateId, $xmlData) {
        $url = 'https://recruit.zoho.com/recruit/private/xml/Candidates/addTabularRecords?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=' . $candidateId;
        // Initialise a cURL handle
        $ch = curl_init();
//        $data = http_build_query($data);
        // Set any other cURL options that are required
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "xmlData=$xmlData");
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        return json_decode($response, true);
    }

    /*
     * Get signed attachments for all candidates
     * 
     */

    public function getSignedAttachment($from = 1, $to = 20, $mainCount = 0, $data = array()) {
        $cronLog = Cronlog::where('cron_name','zoho-to-aplikasi')->where('module_name','service_agreement')->first();
        $client = new Client($this->token);
        $queryResult = $client->getRecords('CustomModule5', array(
            'selectColumns' => 'CustomModule5(ZohoSign Documents Name,ZohoSign Documents Owner,Email,Secondary Email,Created By,Modified By,Email Opt Out,Contact,Job Opening,Candidate,Date Completed,Date Declined,Date Sent,Declined Reason,Document Deadline,Document Description,Document Status,Preview or Position Signature Fields,Time to complete,Currency,Recalled Reason,Declined Reason0,ZohoSign Document ID0,Document Note0,Is From Offer,Last Activity Time,Others)', 'fromIndex' => $from, 'toIndex' => $to,'lastModifiedTime' => $cronLog->last_run
        ));
        $data[] = $queryResult;
        $mainCount += count($queryResult);
        if (count($queryResult) >= 20) {
            $from = $mainCount + 1;
            $to = $from + 19;
            return $this->getSignedAttachment($from, $to, $mainCount, $data);
        }
        return $data;
    }

    /*
     * Download candidate signed attachements
     * 
     */

    public function downloadSignedAttachment($signedDocumentId) {
        $client = new Client($this->token);
        $attachments = $client->getRelatedRecords(
                'Attachments', "CustomModule5", "$signedDocumentId"
        );
//        if (count($attachments)) {
//            foreach ($attachments as $key => $value) {
//                $destination_folder = public_path('signed_attachments/' . $value['File Name']);
//                if (!file_exists($destination_folder)) {
//                    $url = "https://recruit.zoho.com/recruit/private/json/CustomModule5/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $value['id'];
//                    file_put_contents("$destination_folder", fopen($url, 'r'));
//                }
//            }
//        }
        return $attachments;
    }
    public function deleteCandidateAttachment($attachmentId){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://recruit.zoho.com/recruit/private/json/Candidates/deleteFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=".$attachmentId);
        curl_setopt($ch, CURLOPT_POST, true);
        $response = curl_exec($ch);
        return json_decode($response, true);
    }
    public function getInterviewNotes(){
        $client = new Client($this->token);
        $notes = $client->getRelatedRecords(
                'Notes', "Interview", "401682000002794054"
        );
        return $notes;
    }
    public function getClientContacts($clientId){
        $client = new Client($this->token);
        $queryResult = $client->getSearchRecords(
                'Contacts', 'Contacts(First Name,Salutation,Last Name,Department,Client Name,Work Phone,Job Title,Fax,Email,Skype ID,Mobile,Is primary contact,Twitter,Currency,Email Opt Out,Territory,Secondary Email,How You Know About Us,Last Activity Time,Territories,Mailing Street,Other Street,Mailing City,Other City,Mailing State,Other State,Mailing Zip,Other Zip,Mailing Country,Other Country,Contact Owner,Source,Created By,Modified By,Type,Subject,Message,Others)', '(CLIENTID|is|'.$clientId.')'
        );
        return $queryResult;
    }
    public function addCandidateTabularRecord($candidateId,$xmlData){
        $url = 'https://recruit.zoho.com/recruit/private/xml/Candidates/addTabularRecords?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=' . $candidateId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "xmlData=$xmlData");
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        return $response;
    }
    public function updateCandidateTabularRecord($candidateId,$xmlData){
        $url = 'https://recruit.zoho.com/recruit/private/xml/Candidates/updateTabularRecords?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=' . $candidateId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "xmlData=$xmlData");
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        return $response;
    }
    public function deleteCandidateTabularRecord($candidateId,$tabularRowId,$tabularName){
        $url = 'https://recruit.zoho.com/recruit/private/xml/Candidates/deleteTabularRecords?id='.$candidateId.'&tabularName='.urlencode($tabularName).'&deleteType=partial&tabularRowIds='.$tabularRowId.'&authtoken='.$this->token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request        
        curl_close($ch);      // Closing the cURL handle
        return $response;
    }
    /*
    * Experience XML Data
    *
    */
    public function createUpdateExperienceXmlData($experienceList,$requestData,$action){
        $xml = '<Candidates><FL val="Experience Details">';        
        if($action == "add"){ // For Add
            $lastKey = 0;
            if($experienceList != null){
                foreach($experienceList as $key => $value){                    
                    $xml .= '';
                    if (isset($value->is_currently_working_here)) {
                        $xml .= '<TR no="' . $lastKey . '">' .
                                '<TL val="Occupation / Title">' . $value->occupation . '</TL>' .
                                '<TL val="Company">' . $value->company . '</TL>' .
                                '<TL val="Industry">' . $value->industry . '</TL>' .
                                '<TL val="Summary">' . $value->summary . '</TL>' .
                                '<TL val="Work Duration_From">' . $value->work_duration . '</TL>' .
                                '<TL val="I currently work here">true</TL>' .
                                '</TR>';
                    } else {
                        $xml .= '<TR no="' . $lastKey . '">' .
                                '<TL val="Occupation / Title">' . $value->occupation . '</TL>' .
                                '<TL val="Company">' . $value->company . '</TL>' .
                                '<TL val="Industry">' . $value->industry . '</TL>' .
                                '<TL val="Summary">' . $value->summary . '</TL>' .
                                '<TL val="Work Duration_From">' . $value->work_duration . '</TL>' .
                                '<TL val="Work Duration_To">' . $value->work_duration_to . '</TL>' .
                                '</TR>';
                    }
                    $lastKey++;
                }
            }
            $xml .= '';
            if (isset($requestData->is_currently_working_here) && $requestData->is_currently_working_here == "true") {
                $xml .= '<TR no="' . $lastKey++ . '">' .
                        '<TL val="Occupation / Title">' . $requestData->occupation . '</TL>' .
                        '<TL val="Company">' . $requestData->company . '</TL>' .
                        '<TL val="Industry">' . $requestData->industry . '</TL>' .
                        '<TL val="Summary">' . $requestData->summary . '</TL>' .
                        '<TL val="Work Duration_From">' . $requestData->work_duration_from_month .'-'. $requestData->work_duration_from_year . '</TL>' .
                        '<TL val="I currently work here">true</TL>' .
                        '</TR>';
            } else {
                $xml .= '<TR no="' . $lastKey++ . '">' .
                        '<TL val="Occupation / Title">' . $requestData->occupation . '</TL>' .
                        '<TL val="Company">' . $requestData->company . '</TL>' .
                        '<TL val="Industry">' . $requestData->industry . '</TL>' .
                        '<TL val="Summary">' . $requestData->summary . '</TL>' .
                        '<TL val="Work Duration_From">' . $requestData->work_duration_from_month .'-'. $requestData->work_duration_from_year . '</TL>' .
                        '<TL val="Work Duration_To">' . $requestData->work_duration_to_month .'-'. $requestData->work_duration_to_year . '</TL>' .
                        '</TR>';
            }
        }else{ // For Update 
            if ($experienceList->is_currently_working_here == "true") {
                $xml .= '<TR no="' . 0 . '">' .
                        '<TL val="TABULARROWID">' . $experienceList->TABULARROWID . '</TL>' .
                        '<TL val="Occupation / Title">' . $experienceList->occupation . '</TL>' .
                        '<TL val="Company">' . $experienceList->company . '</TL>' .
                        '<TL val="Industry">' . $experienceList->industry . '</TL>' .
                        '<TL val="Summary">' . $experienceList->summary . '</TL>' .
                        '<TL val="Work Duration_From">' . $experienceList->work_duration . '</TL>' .
                        '<TL val="I currently work here">true</TL>' .
                        '</TR>';
            } else {
                $xml .= '<TR no="' . 0 . '">' .
                        '<TL val="TABULARROWID">' . $experienceList->TABULARROWID . '</TL>' .
                        '<TL val="Occupation / Title">' . $experienceList->occupation . '</TL>' .
                        '<TL val="Company">' . $experienceList->company . '</TL>' .
                        '<TL val="Industry">' . $experienceList->industry . '</TL>' .
                        '<TL val="Summary">' . $experienceList->summary . '</TL>' .
                        '<TL val="Work Duration_From">' . $experienceList->work_duration . '</TL>' .
                        '<TL val="Work Duration_To">' . $experienceList->work_duration_to . '</TL>' .
                        '</TR>';
            }
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }
    /* 
    * Education XML Data
    *
    */
    public function createUpdateEducationXmlData($educations,$requestData,$action){
        $xml = '<Candidates><FL val="Educational Details">';
        if($action == "add"){ // For Add
            $lastKey = 0;
            if($educations != null){
                foreach($educations as $key => $value){                    
                    $xml .= '';
                    if ($value->currently_pursuing == "true") {
                        $xml .= '<TR no="' . $key . '">' .
                                '<TL val="Institute / School">' . $value->institute . '</TL>' .
                                '<TL val="Major / Department">' . $value->department . '</TL>' .
                                '<TL val="Degree">' . $value->degree . '</TL>' .
                                '<TL val="Duration_From">' . $value->duration_from . '</TL>' .
                                '<TL val="Currently pursuing">true</TL>' .
                                '</TR>';
                    } else {
                        $xml .= '<TR no="' . $key . '">' .
                                '<TL val="Institute / School">' . $value->institute . '</TL>' .
                                '<TL val="Major / Department">' . $value->department . '</TL>' .
                                '<TL val="Degree">' . $value->degree . '</TL>' .
                                '<TL val="Duration_From">' . $value->duration_from . '</TL>' .
                                '<TL val="Duration_To">' . $value->duration_to . '</TL>' .
                                '</TR>';
                    }
                    $lastKey++;
                }
            }
            $xml .= '';
            if (isset($requestData->is_currently_pursuing) && $requestData->is_currently_pursuing == "true") {
                $xml .= '<TR no="' . $lastKey . '">' .
                        '<TL val="Institute / School">' . $requestData->institute . '</TL>' .
                        '<TL val="Major / Department">' . $requestData->department . '</TL>' .
                        '<TL val="Degree">' . $requestData->degree . '</TL>' .
                        '<TL val="Duration_From">' . $requestData->duration_from_month.'-'.$requestData->duration_from_year . '</TL>' .
                        '<TL val="Currently pursuing">true</TL>' .
                        '</TR>';
            } else {
                $xml .= '<TR no="' . $lastKey . '">' .
                        '<TL val="Institute / School">' . $requestData->institute . '</TL>' .
                        '<TL val="Major / Department">' . $requestData->department . '</TL>' .
                        '<TL val="Degree">' . $requestData->degree . '</TL>' .
                        '<TL val="Duration_From">' . $requestData->duration_from_month.'-'.$requestData->duration_from_year . '</TL>' .
                        '<TL val="Duration_To">' . $requestData->duration_to_month.'-'.$requestData->duration_to_year . '</TL>' .
                        '</TR>';
            }
        }else{ // For Update 
            if ($educations->currently_pursuing == "true") {
                $xml .= '<TR no="' . 0 . '">' .
                        '<TL val="TABULARROWID">' . $educations->TABULARROWID . '</TL>' .
                        '<TL val="Institute / School">' . $educations->institute . '</TL>' .
                        '<TL val="Major / Department">' . $educations->department . '</TL>' .
                        '<TL val="Degree">' . $educations->degree . '</TL>' .
                        '<TL val="Duration_From">' . $educations->duration_from . '</TL>' .
                        '<TL val="Currently pursuing">true</TL>' .
                        '</TR>';
            } else {
                $xml .= '<TR no="' . 0 . '">' .
                        '<TL val="TABULARROWID">' . $educations->TABULARROWID . '</TL>' .
                        '<TL val="Institute / School">' . $educations->institute . '</TL>' .
                        '<TL val="Major / Department">' . $educations->department . '</TL>' .
                        '<TL val="Degree">' . $educations->degree . '</TL>' .
                        '<TL val="Duration_From">' . $educations->duration_from . '</TL>' .
                        '<TL val="Duration_To">' . $educations->duration_to . '</TL>' .
                        '</TR>';
            }
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }
    /* 
    * Reference XML Data
    *
    */
    public function createUpdateReferenceXmlData($references,$requestData,$action){
        $xml = '<Candidates><FL val="References">';
        if($action == "add"){ // For Add
            $lastKey = 0;
            if($references != null){
                foreach($references as $key => $value){                    
                    $xml .= '';
                    $xml .= '<TR no="' . $key . '">' .
                                '<TL val="Reference Name">' . $value->name . '</TL>' .
                                '<TL val="Reference Position">' . $value->position . '</TL>' .
                                '<TL val="Reference Company">' . $value->company . '</TL>' .
                                '<TL val="Reference Phone no.">' . $value->phone . '</TL>' .
                                '<TL val="Reference Email">' . $value->email . '</TL>' .
                            '</TR>';
                    $lastKey++;
                }
            }
            $xml .= '';
            $xml .= '<TR no="' . $lastKey . '">' .
                        '<TL val="Reference Name">' . $requestData->name . '</TL>' .
                        '<TL val="Reference Position">' . $requestData->position . '</TL>' .
                        '<TL val="Reference Company">' . $requestData->company . '</TL>' .
                        '<TL val="Reference Phone no.">' . $requestData->phone . '</TL>' .
                        '<TL val="Reference Email">' . $requestData->email . '</TL>' .
                    '</TR>';
        }else{ // For Update 
            $xml .= '<TR no="' . 0 . '">' .
                        '<TL val="TABULARROWID">' . $references->TABULARROWID . '</TL>' .
                        '<TL val="Reference Name">' . $references->name . '</TL>' .
                        '<TL val="Reference Position">' . $references->position . '</TL>' .
                        '<TL val="Reference Company">' . $references->company . '</TL>' .
                        '<TL val="Reference Phone no.">' . $references->phone . '</TL>' .
                        '<TL val="Reference Email">' . $references->email . '</TL>' .
                    '</TR>';
        }
        $xml .= '</FL></Candidates>';
        return $xml;
    }
}