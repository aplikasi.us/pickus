<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidates;
use Session;
use Illuminate\Support\Facades\Auth;

class searchControllerConsultant extends Controller
{
    
    
public function index(Request $request)
{
    $search_type = $request->search_type;
    $query = Candidates::with(['attachments']);
    $searchterm = $available = $willing_to_travel = $full_time = $part_time = $project = $support = $job_title_matching = $baserate_from = $baserate_to = $baserate2_from = $baserate2_to = $avalabilityDate = $availability_date2 = $experience = $experience2 = "";
    $category = [];
    $category2 = [];
    $workpreference = [];
    $state = [];
    
    if(!isset($request->searchterm) || $request->searchterm == ""){
        return ;
    }

    /* Category */
    if ((isset($request->category) && !empty($request->category)) || isset($request->category2)) {
        $category = $request->category;
        $category2 = $request->category2;
        if (!empty($category2)) {
            $query = $query->whereNotNull('category');
            // $query = $query->whereIn('category',$category2);
            $query = $query->whereHas('candidatecategory', function ($query) use ($category2) {
                $query = $query->whereIn('category', $category2);
            });
            // $category = $category2;
        } else {
            $category2 = $category;
            $query = $query->whereNotNull('category');
            // $query = $query->whereIn('category',$category);
            $query = $query->whereHas('candidatecategory', function ($query) use ($category) {
                $query = $query->whereIn('category', $category);
            });
        }
    }
    /* General */

    /* Available */
    if (isset($request->available)) {
        $available = "Yes";
        $query = $query->where('available_for_contract', 1);
    }

    /* Project */
    if (isset($request->project)) {
        $project = "Yes";
        $query = $query->where('project', 1);
    }
    /* Support */
    if (isset($request->support)) {
        $support = "Yes";
        $query = $query->where('support', 1);
    }
    /* Keyword Matching Job Title Only */
    if (isset($request->job_title_matching)) {
        $searchterm = $request->searchterm;
        $job_title_matching = "Yes";
        $query = $query->where('sap_job_title', 'LIKE', "%" . $searchterm . "%");
    } else if (isset($request->searchterm) && $request->searchterm != "") {
        //General search
        $searchterm = $request->searchterm;
        $query = $query->where(function ($query) use ($searchterm) {
            $query = $query->where('skill_set', 'LIKE', "%" . $searchterm . "%");
            $query = $query->orWhere('sap_job_title', 'LIKE', "%" . $searchterm . "%");
            $query = $query->orWhere('candidate_id', 'LIKE', "%" . $searchterm . "%");
        });
    }

    /* Base Rate */
    if ((isset($request->baserate_from) || isset($request->baserate_to)) || (isset($request->baserate2_from) || isset($request->baserate2_to))) {

        $baserate_from = $request->baserate_from;
        $baserate_to = $request->baserate_to;
        $baserate2_from = $request->baserate2_from;
        $baserate2_to = $request->baserate2_to;

        if ($baserate2_from != null && $baserate2_to != null) {
            $query = $query->whereBetween('base_rate', [$baserate2_from, $baserate2_to]);
            // $baserate_from = '';
            // $baserate_to = '';
        } else if ($baserate_from != null && $baserate_to != null) {
            $baserate2_from = $baserate_from;
            $baserate2_to = $baserate_to;
            $query = $query->whereBetween('base_rate', [$baserate_from, $baserate_to]);
        } else if ($baserate2_from != null) {
            // $baserate_from = '';
            // $baserate_to = '';
            $query = $query->where('base_rate', '>=', $baserate2_from);
        } else if ($baserate2_to != null) {
            // $baserate_from = '';
            // $baserate_to = '';
            $query = $query->where('base_rate', '<=', $baserate2_to);
        } else if ($baserate_from != null) {
            $baserate2_from = $baserate_from;
            $query = $query->where('base_rate', '>=', $baserate_from);
        } else if ($baserate_to != null) {
            $baserate2_to = $baserate_to;
            $query = $query->where('base_rate', '<=', $baserate_to);
        }
    }


    /* Availability Date */
    if (isset($request->availability_date) || isset($request->availability_date2)) {
        $avalabilityDate = $request->availability_date;
        $availability_date2 = $request->availability_date2;
        if ($availability_date2 != null) {
            if ($availability_date2 == "week") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+7 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else if ($availability_date2 == "month") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+30 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else if ($availability_date2 == "three") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+90 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else if ($availability_date2 == "six") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+180 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+365 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            }
        } else {
            $availability_date2 = $avalabilityDate;
            if ($avalabilityDate == "week") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+7 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else if ($avalabilityDate == "month") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+30 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else if ($avalabilityDate == "three") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+90 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else if ($avalabilityDate == "six") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+180 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            } else {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('+365 day'));
                $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
            }
        }
    }

    /* Experience */
    if (isset($request->experience) || isset($request->experience2)) {
        $experience = $request->experience;
        $experience2 = $request->experience2;
        if ($experience2 != null) {
            if ($experience2 == "0") {
                $query = $query->where('experience_in_years', 0);
            } else if ($experience2 == "3") {
                $query = $query->whereBetween('experience_in_years', [1, 3]);
            } else if ($experience2 == "5") {
                $query = $query->whereBetween('experience_in_years', [4, 5]);
            } else if ($experience2 == "10") {
                $query = $query->whereBetween('experience_in_years', [6, 10]);
            } else {
                $query = $query->where('experience_in_years', '>', 10);
            }
        } else {
            $experience2 = $experience;
            if ($experience == "0") {
                $query = $query->where('experience_in_years', 0);
            } else if ($experience == "3") {
                $query = $query->whereBetween('experience_in_years', [1, 3]);
            } else if ($experience == "5") {
                $query = $query->whereBetween('experience_in_years', [4, 5]);
            } else if ($experience == "10") {
                $query = $query->whereBetween('experience_in_years', [6, 10]);
            } else {
                $query = $query->where('experience_in_years', '>', 10);
            }
        }
    }
    /* Work Preferences */
    if (isset($request->willing_to_travel) || isset($request->full_time) || (isset($request->part_time))) {

        /* Willing to Travel */
        if (isset($request->willing_to_travel)) {
            $willing_to_travel = "Yes";
            $query = $query->where('willing_to_travel', 1);
        }
        /* full time */
        if (isset($request->full_time)) {
            $full_time = "Yes";
            $query = $query->where('full_time', 1);
        }
        /* Part Time */
        if (isset($request->part_time)) {
            $part_time = "Yes";
            $query = $query->where('part_time', 1);
        }
    } else if (isset($request->work_preference) && !empty($request->work_preference)) {
        $workpreference = $request->work_preference;
        if (in_array('Full Time', $workpreference)) {
            $full_time = "Yes";
            $query = $query->where('full_time', 1);
        }
        if (in_array("Part Time", $workpreference)) {
            $part_time = "Yes";
            $query = $query->where('part_time', 1);
        }
        if (in_array("Willing to travel", $workpreference)) {
            $willing_to_travel = "Yes";
            $query = $query->where('willing_to_travel', 1);
        }
    }
    /* State */
    if (isset($request->state) && !empty($request->state)) {
        $state = $request->state;
        $query = $query->whereIn('state', $state);
    }
    /* Teritorries */
    if (Session::get('territory') != null) {
        $query = $query->where('territory', Session::get('territory'));
    }

    $query = $query->where('block', 0)->where('internal_hire', 0)->where('publish_in_us', 1)->where('is_delete', 0)->whereNotIn('candidate_status', ['Unqualified', 'Qualified Expat', 'Unqualified Expat']);
    $candidates = $query->paginate(15);
    $total = $candidates->total();
    
    $wishlist = [];
    if (Auth::check()) {
        $wishlist = Clientwishlist::where([
            'CLIENTID' => Auth::user()->zoho_id,
            'contact_id' => Auth::user()->contact_tbl_id
        ])->pluck('CANDIDATEID')->toArray();
    }
    $addTocompare = [];
    if (Auth::check()) {
        $addTocompare = Addcompare::where([
            'CLIENTID' => Auth::user()->zoho_id,
            'contact_id' => Auth::user()->contact_tbl_id
        ])->pluck('CANDIDATEID')->toArray();
    }
    return $candidates;
}

}
