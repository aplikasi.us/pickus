<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use App\User;

class CheckAccountDeleted {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::check()) {
            $isAccountDeleted = User::where('id', Auth::user()->id)->where('is_delete',0)->exists();
            if ($isAccountDeleted != 1) { // Do logout forcefully
                Session::forget('profile');
                Session::forget('territory');
                Session::forget('redirecturl');
                Auth::logout();
                return redirect('/');
            }
            return $next($request);
        }
        return $next($request);
    }

}
