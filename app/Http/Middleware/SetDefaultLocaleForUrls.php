<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use PulkitJalan\GeoIP\GeoIP;

class SetDefaultLocaleForUrls {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $territory = $request->session()->get('territory');
        if ($request->session()->get('territory') == "") {
            $geoip = new GeoIP();
            $country = $geoip->getCountry();
            if ($country == "Philippines") {
                $request->session()->put('territory', "PH");
            } else {
                $request->session()->put('territory', "MY");
        }
            }
        $territory = $request->session()->get('territory');
        URL::defaults(['locale' => $territory]);

        return $next($request);
    }

}
