<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "MY/client/savetowishlist","PH/client/savetowishlist","MY/readnotification","PH/readnotification","admin/readadminnotification","admin/requestprofilelist/delete","admin/requestprofilelist/downloadcv","admin/synczohodata","MY/client/remove-to-compare","PH/client/remove-to-compare","MY/client/add-tocompare","PH/client/add-tocompare","MY/client/wishlist/removetowishlist","PH/client/wishlist/removetowishlist","MY/consultant/appliedjob","PH/consultant/appliedjob","MY/client/upload-profilepic","PH/client/upload-profilepic","MY/user/login","PH/user/login","PH/user/forgotpassword","MY/user/forgotpassword","MY/getsearchconsultant","PH/getsearchconsultant","MY/getsearchjob","PH/getsearchjob"
    ];
}
