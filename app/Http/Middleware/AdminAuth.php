<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role_id == 3){
            $Admin  = User::where(['email'=>Auth::user()->email,'status'=>'Active' ,'is_delete' => '0'])->first();
            if(!$Admin){
                $request->session()->flush();
                return redirect('admin/dashboard');
            }
           return $next($request);
        }else{
//           return redirect('admin');
            return redirect('/');
        }
    }
}
