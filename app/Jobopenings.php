<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobopenings extends Model {

    protected $table = "jobopening";

    public function client() {
        return $this->belongsTo('App\Clients', 'client_id', 'id');
    }

    public function contact() {
        return $this->belongsTo('App\Clientcontact', 'db_contact_id', 'id');
    }

    public function associatecandidates() {
        return $this->hasMany('App\Jobassociatecandidates', 'job_id', 'id');
    }

    /* public function client(){
      return $this->belongsTo('App\Clients','CLIENTID','CLIENTID');
      } */

    public function Jobassociatecandidates() {
        return $this->hasMany('App\Jobassociatecandidates', 'JOBOPENINGID', 'JOBOPENINGID');
    }

    public function user() {
        return $this->belongsTo('App\User', 'CLIENTID', 'zoho_id');
    }
    public function notes() {
        return $this->hasMany('App\Jobnotes', 'job_id', 'id');
    }
    public function jobopeningcategory(){
        return $this->hasMany('App\Jobopeningcategory','job_id','id');
    }
}