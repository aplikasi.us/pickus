<?php

namespace App\Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Humantech\Zoho\Recruit\Api\Client\AuthenticationClient;
use Humantech\Zoho\Recruit\Api\Client\Client;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Token;
use CURLFile;
use App\Industry;
use App\States;
use App\Cities;
use App\Clients;
use App\Consultant;
use App\Jobopenings;
use App\Interviews;
use App\Candidates;
use App\Jobassociatecandidates;
use App\Clientwishlist;
use App\Notification;
use App\Recentlyviewedconsultant;
use App\Settings;
use App\Emailtemplate;
use App\Attachment;
use App\Clientcontact;
use App\Experience;
use App\Addcompare;
use App\Jobopeningcategory;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\ZohoqueryController;
use App\Faq;
use App\Category;
use App\Candidateprofilerequests;
use App\Jobnotes;
use App\Http\Controllers\SearchController;

class UsersController extends Controller
{

    public $token;

    public function __construct()
    {
        $result = Token::first();
        $this->token = $result->token;
    }

    /*
     * Consultant Registration
     * 
     */

    public function consultantRegistration(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = new User();
            $user->role_id = $request->role_id;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->sap_job_title = $request->sap_job_title;
            $user->hear = $request->hear;
            $user->status = "Active";
            if(!$request->territory) {
                $user->territory = Session::get('territory');
            } else {
                $user->territory = $request->territory;
            }
            if ($request->hasFile('cv')) {
                $file = $request->file()['cv'];
                $filename = "";
                $filename = time() . '-' . str_replace(" ", "-", $file->getClientOriginalName());
                $path = public_path('cv/' . $filename);
                $request->file('cv')->move(public_path("/cv"), $filename);
                $user->cv_filename = $filename;
            }
            $user->save();
            $this->sendRegistrationEmail($user);

            Session::flash('success', 'You are successfully registered');
            if($request->isAdmin) {
                return redirect()->route('consultant.index');
            }
            return redirect()->route('consultantThankyou');
        } else {
            return view('users::consultant_registration');
        }
    }

    public function consultantNewRegistration(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = new User();
            $user->role_id = $request->role_id;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->sap_job_title = $request->sap_job_title;
            $user->hear = $request->hear;
            $user->status = "Active";
            if(!$request->territory) {
                $user->territory = Session::get('territory');
            } else {
                $user->territory = $request->territory;
            }
            if ($request->hasFile('cv')) {
                $file = $request->file()['cv'];
                $filename = "";
                $filename = time() . '-' . str_replace(" ", "-", $file->getClientOriginalName());
                $path = public_path('cv/' . $filename);
                $request->file('cv')->move(public_path("/cv"), $filename);
                $user->cv_filename = $filename;
            }
            $user->save();
            $this->sendRegistrationEmail($user);

            Session::flash('success', 'You are successfully registered');
            if($request->isAdmin) {
                return redirect()->route('consultant.index');
            }
            return redirect()->route('consultantThankyou');
        } else {
            return view('users::consultant_registration_new');
        }
    }

    /*
     * Consultant Registration in Zoho 
     * 
     */

    public function zohoConsultantRegistration($user)
    {
        if ($user != null) {
            $territory = "MY";
            if ($user->territory != null) {
                $territory = $user->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $this->token = $result->token;

            $client = new Client($this->token);
            $data = [
                'First Name' => htmlspecialchars($user->first_name),
                'Last Name' => htmlspecialchars($user->last_name),
                'Email' => $user->email,
                'Mobile' => $user->phone,
                'Current Job Title' => htmlspecialchars($user->sap_job_title),
                'SAP Job Title' => htmlspecialchars($user->sap_job_title),
                'Territory' => $user->territory,
                'Publish in Us' => 'true',
                'Block' => 'false',
                'How You Know About Us' => $user->hear
            ];
            $candidateResponse = $client->addRecords('Candidates', $data, array(), 'json', true);
            if (!empty($candidateResponse)) {
                if (isset($candidateResponse['response']['result']['recorddetail']['FL'])) {
                    $CANDIDATEID = $candidateResponse['response']['result']['recorddetail']['FL'][0]['content'];
                    $user->zoho_id = $CANDIDATEID;
                    $user->save();
                }
            }

            $territory = "MY";
            if ($user->territory != null) {
                $territory = $user->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $this->token = $result->token;

            /* Resume Upload Using CURL */
            $zohoQuery = new ZohoqueryController();

            $zohoQuery->token = $this->token;
            $zohoQuery->updateAttachment($user->zoho_id, $user->cv_filename, "Resume");

            $this->updateConsultantZohoId($user);
        }
        return;
    }

    public function human_filesize($bytes, $decimals = 2)
    {
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor > 0)
            $sz = 'KMGT';
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
    }

    /*
     * Client Registration in Zoho 
     * 
     */

    public function zohoClientRegistration($user)
    {
        if ($user != null) {
            $territory = "MY";
            if ($user->territory != null) {
                $territory = $user->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $this->token = $result->token;

            $client = new Client($this->token);
            $data = [
                'Client Name' => htmlspecialchars($user->company_name),
                'Contact Number' => $user->phone,
                'Territories' => $user->territory,
                'About' => htmlspecialchars($user->sap_job_title)
            ];
            $clientResponse = $client->addRecords('Clients', $data, array(), 'json', true);
            if (!empty($clientResponse)) {
                if (isset($clientResponse['response']['result']['recorddetail']['FL'])) {
                    $CLIENTID = $clientResponse['response']['result']['recorddetail']['FL'][0]['content'];
                    $user->zoho_id = $CLIENTID;
                    $user->save();
                }
            }

            $data1['First Name'] = $user->first_name;
            $data1['Last Name'] = $user->last_name;
            $data1['Job Title'] = $user->sap_job_title;
            $data1['Email'] = $user->email;
            $data1['Mobile'] = $user->phone;
            $data1['Client Name'] = $user->company_name;
            $data1['Territory'] = $user->territory;
            $data1['Territories'] = $user->territory;
            $data1['How You Know About Us'] = $user->hear;
            $contactResponse = $client->addRecords('Contacts', $data1, array(), 'json', true);
            if (!empty($contactResponse)) {
                if (isset($contactResponse['response']['result']['recorddetail']['FL'])) {
                    $CONTACTID = $contactResponse['response']['result']['recorddetail']['FL'][0]['content'];
                    $user->CONTACTID = $CONTACTID;
                    $user->save();
                }
            }
            $this->updateClientZohoId($user);
        }
        return;
    }

    /*
     * Update Consultant Zoho ID
     *
     */

    public function updateConsultantZohoId($user)
    {
        $territory = "MY";
        if ($user->territory != null) {
            $territory = $user->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        $client = new Client($this->token);
        //$data = $client->getRecordById('Candidates', $user->zoho_id);
        $data = $user;
        $userDetails = $user;
        if (count($data)) {
            $candidateObject = new Candidates();
            //$candidateObject->CANDIDATEID = $data[0]['CANDIDATEID'];
            $candidateObject->CANDIDATEID = $userDetails->zoho_id;
            // $candidateObject->candidate_id = $data[0]['Candidate ID'];
            $candidateObject->candidate_id = 'US_0'.$userDetails->id;
            $candidateObject->first_name = $userDetails->first_name;
            $candidateObject->last_name = $userDetails->last_name;
            $candidateObject->email = $userDetails->email;
            $candidateObject->territory = $userDetails->territory;
            $candidateObject->mobile_number = $userDetails->phone;
            $candidateObject->sap_job_title = $userDetails->sap_job_title;
            $candidateObject->publish_in_us = 1;
            $candidateObject->internal_hire = 0;
            $candidateObject->block = 0;
            $candidateObject->created_at = date('Y-m-d H:i:s', time());
            $candidateObject->updated_at = date('Y-m-d H:i:s', time());
            $candidateObject->save();

            $size = filesize(public_path('cv/' . $user->cv_filename));
            $attachment = new Attachment();
            $attachment->candidate_id = $candidateObject->id;
            $attachment->zoho_id = $candidateObject->CANDIDATEID;
            $attachment->filename = $userDetails->cv_filename;
            $attachment->size = $this->human_filesize($size);
            $attachment->category = "Resume";
            if ($territory == "MY") {
                $attachment->attach_by = "Talent Admin";
            } else {
                $attachment->attach_by = "Talent PH Admin";
            }
            $attachment->created_at = date("Y-m-d h:i:s", time());
            $attachment->updated_at = date("Y-m-d h:i:s", time());
            $attachment->save();
        }
        //        $file = public_path('images/about-img1.png');
        //        $candidateList = $client->getRecords('Candidates', array(
        //            'selectColumns' => 'Candidates('.$user->zoho_id.')'
        //        ));
        //        $file = file_get_contents($file);
        //        if (isset($candidateList[0]['CANDIDATEID'])) {
        //           $respopnse = $client->uploadFile(
        //                            $candidateList[0]['CANDIDATEID'], 'Resume', "$file"
        //            );
        //        }
        //        $respopnse = $client->uploadFile(
        //                $user->zoho_id, 'Resume', "$file"
        //        );
        /* Resume Upload Using CURL */
        //        $token = $this->token;
        //        $file = public_path('cv/' . $user->cv_filename);
        //        $ch = curl_init();
        //        $cFile = new CURLFile("$file", 'application/pdf');
        //        curl_setopt($ch, CURLOPT_HEADER, 0);
        //        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //        curl_setopt($ch, CURLOPT_URL, "https://recruit.zoho.com/recruit/private/xml/Candidates/uploadFile?authtoken=$token&scope=recruitapi&version=2&type=Resume");
        //        curl_setopt($ch, CURLOPT_POST, true);
        //        $post = array("id" => $user->zoho_id, "content" => $cFile);
        //        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        //        $response = curl_exec($ch);
        return;
    }

    /*
     * Update Client Zoho ID
     *
     */

    public function updateClientZohoId($user)
    {
        $territory = "MY";
        if ($user->territory != null) {
            $territory = $user->territory;
        }
        $result = Token::where('territory', $territory)->first();
        //$this->token = $result->token;

        $contact = new Clientcontact();
        $contact->CONTACTID = $user->CONTACTID;
        $contact->first_name = $user->first_name;
        $contact->last_name = $user->last_name;
        $contact->CLIENTID = $user->zoho_id;
        $contact->client_name = $user->company_name;
        $contact->email = $user->email;
        $contact->territory = $user->territory;
        $contact->territories = $user->territory;
        $contact->mobile = $user->phone;
        $contact->how_you_know_about_us = $user->hear;
        $contact->save();

        $user->contact_tbl_id = $contact->id;
        $user->save();


        $clientObject = new Clients();
        $clientObject->CLIENTID = $user->zoho_id;
        $clientObject->client_name = $user->company_name;
        $clientObject->contact_number = $user->phone;
        $clientObject->territories = $user->territory;
        $clientObject->created_at = date('Y-m-d H:i:s', time());
        $clientObject->updated_at = date('Y-m-d H:i:s', time());
        $clientObject->save();
        
        $client = new Client($this->token);
        // $data = $client->getRecordById('Clients', $user->zoho_id);
        // if (count($data)) {
        //     $getContact = $client->getRecordById('Contacts', $user->CONTACTID);
        //     if (count($getContact)) {
        //         $contact = new Clientcontact();
        //         $contact->CONTACTID = $user->CONTACTID;
        //         $contact->first_name = $user->first_name;
        //         $contact->last_name = $user->last_name;
        //         $contact->CLIENTID = $user->zoho_id;
        //         $contact->client_name = $user->company_name;
        //         $contact->email = $user->email;
        //         $contact->territory = $user->territory;
        //         $contact->territories = $user->territory;
        //         $contact->mobile = $user->phone;
        //         $contact->how_you_know_about_us = $user->hear;
        //         $contact->save();

        //         $user->contact_tbl_id = $contact->id;
        //     }
        //     $user->save();

        //     $clientObject = new Clients();
        //     $clientObject->CLIENTID = $user->zoho_id;
        //     $clientObject->client_name = $user->company_name;
        //     $clientObject->contact_number = $user->phone;
        //     $clientObject->territories = $user->territory;
        //     $clientObject->created_at = date('Y-m-d H:i:s', time());
        //     $clientObject->updated_at = date('Y-m-d H:i:s', time());
        //     $clientObject->save();
        // }
        return;
    }

    /*
     * Send Registration Email
     * 
     */

    public function sendRegistrationEmail($userObject)
    {
        $id = Crypt::encryptString($userObject->id);
        $template = EmailTemplate::where('name', 'generate_password')->first();
        if ($template != null) {
            $emaildata['username'] = $userObject->first_name . ' ' . $userObject->last_name;
            $emaildata['email'] = $userObject->email;
            $emaildata['link'] = route('generate-password', ['locale' => $userObject->territory,  'id' => $id]);
            Mail::send([], [], function ($messages) use ($template, $emaildata) {
                $messages->to($emaildata['email'])
                    ->subject($template->subject)
                    ->setBody($template->parse($emaildata), 'text/html');
            });
            DB::table('users')
                ->where('id', "=", $userObject->id)
                ->update([
                    'reset_token' => date('Y-m-d h:i:s', time()),
                    'updated_at' => date('Y-m-d h:i:s', time())
                ]);
        }
        return;
    }

    /*
     * Generate Password
     * 
     */

    public function generatePassword(Request $request)
    {
        $decrypted = Crypt::decryptString($request->id);
        $user = User::where('id', $decrypted)->first();
        if ($request->isMethod('post')) {
            $territory = "MY";
            if ($user->territory != null) {
                $territory = $user->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $this->token = $result->token;

            $client = new Client($this->token);

            $user->password = \Hash::make($request->password);
            $user->is_confirm = 1;

            if ($user->role_id == 1) {
                $user->zoho_id = 'CLIENT_0'.$user->id;
                $user->CONTACTID = "CONTACT_0".$user->id;
            } else {
                $user->zoho_id = 'CONS_0'.$user->id;
                $this->updateConsultantZohoId($user);
            }
            
            $user->save();
            if ($user->role_id == 1) {
                //$this->zohoClientRegistration($user);
                $this->updateClientZohoId($user);
            } 
            // else {
            //     $this->zohoConsultantRegistration($user);
            // }
            Session::put('loginemail', $user->email);
            
            $zoho = new SearchController();
            
            if(isset($user->id_zoho)){
                $zoho->AssignIdToZoho($user->zoho_id, $IDzoho ,$territory);
            }
            
            // Session::flash('success', 'Your password has been set successfully <a href="javascript:;" title="Login" class="login-form-poup" >Click here to login</a>');
            return response()->json([
                'status' => 200,
                'message' => 'Your password has been set successfully'
            ]);
            // return redirect('/' . Session::get('territory'));
        } else {
            if ($user != null) {
                if ($user->is_confirm == 0) {
                    $id = $request->id;
                    //                    return view('users::consultant_generate_password', compact('id', $id));
                    return view('home', compact('id', $id));
                } else {
                    Session::flash('success', 'You have already generate your password <a href="javascript:;" title="Login" class="login-form-poup">Click here to login</a>');
                    return redirect('/' . Session::get('territory'));
                }
            } else {
                Session::flash('success', 'No such registered consultant found');
                return redirect('/' . Session::get('territory'));
            }
        }
    }

    



    /*
     * Check email is already exists
     * 
     */

    public function emailExists(Request $request)
    {
        $user = DB::table('users')->where('email', '=', $request->email)->first();
        if ($request->action == 'edit') { // Check For Edit User
            if ($user === null) {
                $isValid = true;
            } else {
                if (count($user) == 1) {
                    if ($user->id == $request->userid) {
                        $isValid = true;
                    } else {
                        $isValid = false;
                    }
                } else {
                    $isValid = false;
                }
            }
        } else { // Check For New User
            if ($user === null) {
                $isValid = true;
            } else {
                $isValid = false;
            }
        }
        echo json_encode(array(
            'valid' => $isValid,
        ));
    }

    /*
     * Consultant Login
     * 
     */

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->where('is_delete', 0)->whereIn('role_id', array('1', '2'))->first();
        if ($user != null) {
            if ($user->is_confirm == 1) {
                if ($user->status == 'Active') {
                    if (\Hash::check($request->password, $user->password)) {
                        Session::forget('territory');
                        $territory = "MY";
                        if ($user->role_id == 1) {
                            $clientCandidate = Clients::where('CLIENTID', $user->zoho_id)->first();
                            if (count($clientCandidate)) {
                                $territory = $clientCandidate->territories;
                            }
                        } else {
                            $clientCandidate = Candidates::where('CANDIDATEID', $user->zoho_id)->first();
                            if (count($clientCandidate)) {
                                $territory = $clientCandidate->territory;
                            }
                        }
                        Auth::loginUsingId($user->id);
                        $request->session()->regenerate();
                        if (isset($request->redirect_url) && $request->redirect_url != "") {
                            Session::put('redirecturl', $request->redirect_url);
                        }
                        Session::put('territory', $territory);
                        Session::flash('success', 'You have logged in successfully. Your are switched to ' . $territory . ' Territory.');
                        $status = 200;
                        $message = "Login successful";
                        $url = route('login-successful');
                        // return redirect()->route('login-successful');
                        /* consultant added to Wishlist by client */
                        //                        if (isset($request->redirect_url) && $request->redirect_url != "") {
                        //                            return redirect($request->redirect_url);
                        //                        }
                        //                        if ($user->role_id == 1) {
                        //                            return redirect()->route('client-home');
                        //                        } else {
                        //                            return redirect()->route('consultant-home');
                        //                        }
                    } else {
                        $status = 401;
                        $message = "You have enter invalid password";
                        $url = redirect('/' . Session::get('territory'));
                        // Session::flash('error', 'You have enter invalid password');
                        // return redirect('/' . Session::get('territory'));
                    }
                } else {
                    $status = 401;
                    $message = "your account has been inactived";
                    $url = redirect('/' . Session::get('territory'));
                    // Session::flash('error', 'your account has been inactived');
                    // return redirect('/' . Session::get('territory'));
                }
            } else {
                $status = 401;
                $message = "Please verify your email first";
                $url = redirect('/' . Session::get('territory'));
                // Session::flash('error', 'Please verify your email first');
                // return redirect('/' . Session::get('territory'));
            }
        } else {
            $status = 401;
            $message = "No such registered user found";
            $url = redirect('/' . Session::get('territory'));
            // Session::flash('error', 'No such registered user found');
            // return redirect('/' . Session::get('territory'));
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'redirecturl' => $url
        ]);
    }

    /*
     * Logout
     *
     */

    public function logout()
    {
        Session::forget('profile');
        // Session::forget('territory');
        Session::forget('redirecturl');
        Session::forget('loginemail');
        Auth::logout();
        Session::flash('success', 'You have been logged out successfully.');
        return redirect('/' . Session::get('territory'));
    }

    /**
     * Forgot Passowrd 
     * 
     */
    public function forgotPassword(Request $request)
    {
        if (isset($request->email)) {
            $user = User::Where('email', '=', $request->email)->whereIn('role_id', array('1', '2'))->first();
            if ($user) {
                $eid = Crypt::encryptString($user->id);
                //                $mailData = ["id" => $user->id, "FirstName" => $user->first_name, 'LastName' => $user->last_name, "email" => $user->email, 'reseturl' => route('getresetpassword', [$eid])];
                //                Mail::send(['html' => 'emails.reset_password'], $mailData, function($message) use($mailData) {
                //                    $message->to($mailData['email'])->subject
                //                            ('Reset Password');
                //                    // $message->to("dominic@dzoo.com.my")->subject
                //                    //         ('Generate Password');
                //                });
                $template = EmailTemplate::where('name', 'reset_password')->first();
                if ($template != null) {
                    $emaildata['username'] = $user->first_name . ' ' . $user->last_name;
                    $emaildata['email'] = $user->email;
                    $emaildata['link'] = route('getresetpassword', [$eid]);
                    Mail::send([], [], function ($messages) use ($template, $emaildata) {
                        $messages->to($emaildata['email'])
                            ->subject($template->subject)
                            ->setBody($template->parse($emaildata), 'text/html');
                    });
                }
                DB::table('users')
                    ->where('id', "=", $user->id)
                    ->update([
                        'reset_token' => date('Y-m-d h:i:s', time()),
                        'updated_at' => date('Y-m-d h:i:s', time())
                    ]);
                $status = 200;
                $message = "Reset password link sent to your registered email address.";
                // Session::flash('success', 'Reset password link sent to your registered email address.');
                // return redirect('/' . Session::get('territory'));
            } else {
                $status = 401;
                $message = "No such registered user found.";
                // Session::flash('error', 'No such registered user found.');
                // return redirect('/' . Session::get('territory'));
            }
        } else { // Enter valid username and password
            $status = 401;
            $message = "Please enter valid email address.";
            // Session::flash('error', 'Please enter valid email address.');
            // return redirect('/' . Session::get('territory'));
        }
        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

    /**
     * Show reset password form
     * 
     */
    public function getResetPassword(Request $request)
    {
        $id = $request->id;
        $decryptedId = Crypt::decryptString($request->id);
        if (is_numeric($decryptedId)) {
            if ($decryptedId != null) {
                $token = User::where('id', "=", $decryptedId)
                    ->first();
                if ($token != null) {
                    if ($token->reset_token != null) {
                        $diff = strtotime(date("Y-m-d h:i:s")) - strtotime($token->reset_token);
                        $minuteDifference = round(abs($diff) / 60, 2);
                        if ($minuteDifference > 30) { // Check if token is expires or not. Token will be expire after 30 minutes
                            Session::flash('error', 'Token has been expired.');
                            return redirect('/');
                        } else { // Show reset password form
                            return view('users::resetpassword', compact('id'));
                        }
                    } else {
                        return view('errors.404');
                    }
                } else {
                    return view('errors.404');
                }
            } else {
                Session::flash('success', 'Something went wrong. Please try again.');
                return redirect('/' . Session::get('territory'));
            }
        } else {
            return view('errors.404');
        }
    }

    /**
     * 
     * Post forgot password form
     * 
     */
    public function postResetPassword(Request $request)
    {
        $decryptedId = Crypt::decryptString($request->id);
        if ($decryptedId != null) {
            User::where('id', "=", $decryptedId)
                ->update([
                    'password' => \Hash::make($request->password),
                    'reset_token' => date('Y-m-d h:i:s', time()),
                    'updated_at' => date('Y-m-d h:i:s', time()),
                    'is_confirm' => 1
                ]);
            Session::flash('success', 'Your password has been reset successfully.');
            return redirect('/' . Session::get('territory'));
        } else {
            Session::flash('error', 'Your account does not exists to reset password.');
            return redirect('/' . Session::get('territory'));
        }
    }

    /**
     * 
     * Post Change Password
     */
    public function postChangePassword(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->where('is_delete', 0)->first();
        if (!\Hash::check($request->old_password, $user->password)) {
            Session::flash('error', 'Old password is wrong.');
            if ($user->role_id == 1) {
                return redirect()->route('clientSettings');
            } else {
                return redirect()->route('consultantsettings');
            }
        } else {
            $newPassword = \Hash::make($request->new_password);
            $user->password = $newPassword;
            $user->updated_at = date('Y-m-d h:i:s', time());
            $user->save();
            Session::flash('success', 'Your password has been changed successfully.');
            if ($user->role_id == 1) {
                return redirect()->route('clientSettings');
            } else {
                return redirect()->route('consultantsettings');
            }
        }
    }

    /**
     * 
     * Post Delete Account Password
     */
    public function postDeleteAccount(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if (!\Hash::check($request->password, $user->password)) {
            Session::flash('error', 'Your password is wrong.');
            if ($user->role_id == 1) {
                return redirect()->route('clientSettings');
            } else {
                return redirect()->route('consultantsettings');
            }
        } else {
            $user->is_delete = 1;
            $user->save();
            if (Auth::user()->role_id == 1) { // Client contact delete
                Clientcontact::where('id', Auth::user()->contact_tbl_id)->update(['is_delete' => 1]);
            } else { // Consultant delete
                Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->update([
                    'is_delete' => 1
                ]);
            }
            Auth::logout();
            Session::flash('success', 'Your account has been deactivated successfully');
            return redirect('/' . Session::get('territory'));
        }
    }

    /**
     * Notification settings
     * 
     */
    public function postNotifications(Request $request)
    {
        // $decryptedId = Crypt::decryptString($request->id);
        // print_r($request->toArray());
        // exit;
        $user = User::where('id', Auth::user()->id)->where('is_delete', 0)->first();
        // print_r($user->toArray());

        if ($user) {
            if ($user->role_id == "2") {
                if (isset($request->job_alerts) && !empty($request->job_alerts)) {
                    $user->job_alerts = $request->job_alerts;
                    if ($request->job_alerts == 1) {
                        $user->job_alerts_tags = json_encode($request->job_alerts_tags, JSON_FORCE_OBJECT);
                    }
                } else {
                    $user->job_alerts = 0;
                }
                if (isset($request->job_wishlist) && !empty($request->job_wishlist)) {
                    $user->job_wishlist = $request->job_wishlist;
                } else {
                    $user->job_wishlist = 0;
                }
                if (isset($request->applied_job) && !empty($request->applied_job)) {
                    $user->applied_job = $request->applied_job;
                } else {
                    $user->applied_job = 0;
                }
                if (isset($request->profile_show) && !empty($request->profile_show)) {
                    $user->profile_show = $request->profile_show;
                } else {
                    $user->profile_show = 0;
                }
                $user->save();
                Session::flash('success', 'Notification settings saved successfully');
                return redirect()->route('consultantsettings');
            } else {
                if (isset($request->client_job_wishlist) && !empty($request->client_job_wishlist)) {
                    $user->client_job_wishlist = $request->client_job_wishlist;
                } else {
                    $user->client_job_wishlist = 0;
                }
                if (isset($request->client_applied_job) && !empty($request->client_applied_job)) {
                    $user->client_applied_job = $request->client_applied_job;
                } else {
                    $user->client_applied_job = 0;
                }
                $user->save();
                Session::flash('success', 'Notification settings saved successfully');
                return redirect()->route('clientSettings');
            }
        } else {
            if ($user->role_id == "2") {
                Session::flash('success', 'User not founded');
                return redirect()->route('consultantsettings');
            } else {
                Session::flash('success', 'User not founded');
                return redirect()->route('clientSettings');
            }
        }
    }

    /*
     * Consultant Homepage
     *
     */

    public function consultantHome(Request $request)
    {
        if (Auth::user()->role_id == 2) {
            return view('users::consultant_home');
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Consultant Profile
     *
     */

    public function consultantProfile(Request $request)
    {
        if (Auth::user()->role_id == 2) {
            return view('users::consultant_profile');
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Consultant Interviews
     *
     */

    public function consultantInterviews(Request $request)
    {
        if (Auth::user()->role_id == 2) {
            return view('users::consultant_interviews');
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Consultant Contract History
     *
     */

    public function consultantContractHistory(Request $request)
    {
        if (Auth::user()->role_id == 2) {
            return view('users::consultant_contract_history');
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Consultant Attachments
     *
     */

    public function consultantAttachments(Request $request)
    {
        if (Auth::user()->role_id == 2) {
            return view('users::consultant_attachments');
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Client Registration
     * 
     */

    public function clientRegistration(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = new User();
            $user->role_id = $request->role_id;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->company_name = $request->company_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->sap_job_title = $request->job_title;
            $user->hear = $request->hear;
            if(!$request->territory) {
                $user->territory = Session::get('territory');
            } else {
                $user->territory = $request->territory;
            }

            $user->save();
            $this->sendRegistrationEmail($user);

            Session::flash('success', 'You are successfully registered');
            if($request->isAdmin) {
                return redirect()->route('clients.index');
            }
            return redirect()->route('clientThankyou');
            // return redirect('/' . Session::get('territory'));
        } else {
            return view('users::client_registration');
        }
    }

    
    /*
     * Client new Registration
     * 
     */

     public function clientNewRegistration(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = new User();
            $user->role_id = $request->role_id;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->company_name = $request->company_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->sap_job_title = $request->job_title;
            $user->hear = $request->hear;
            if(!$request->territory) {
                $user->territory = Session::get('territory');
            } else {
                $user->territory = $request->territory;
            }

            $user->save();
            $this->sendRegistrationEmail($user);

            Session::flash('success', 'You are successfully registered');
            if($request->isAdmin) {
                return redirect()->route('clients.index');
            }
            return redirect()->route('clientThankyou');
            // return redirect('/' . Session::get('territory'));
        } else {
            return view('users::client_registration_new');
        }
    }

    public function getSuccessfulHire()
    { }

    /*
     * Client Home
     *
     */

    public function clientHome(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            /* Client Profile */
            $profileArray = $this->getClientProfile();
            /* Upcoming Interviews Count */
            $interviews = $this->interviewCounts();

            //            $successfulHires = $this->getSuccessfulHire();
            /* Active Jobs Count */
            $jobs = $this->jobCounts();
            /* Wishlist */
            $wishlist = Clientwishlist::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->pluck('candidate_id')->toArray();

            $addTocompare = Addcompare::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->pluck('CANDIDATEID')->toArray();
            /* Candidate Actions */
            $data = $this->candidateData();
            return view('users::client_home', compact('profileArray', 'interviews', 'jobs', 'data', 'wishlist', 'addTocompare'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Successful Hire List
     * 
     */

    public function successfulHires(Request $request)
    {
        $candidates = Interviews::where('CLIENTID', Auth::user()->zoho_id)->whereNotNull('interview_status')->where('interview_status', 'Selected')->get();
        $data['past'] = [];
        $data['current'] = [];
        //        if (count($candidates)) {
        //            foreach ($candidates as $key => $value) {
        //                $past = Jobassociatecandidates::with(['candidate', 'job'])
        //                        ->where([
        //                            'job_id' => $value->job_id,
        //                            'candidate_id' => $value->candidate_id
        //                        ])
        //                        ->whereHas('job', function ($query) {
        //                            $query->where('job_opening_status', 'Filled');
        //                        })
        //                        ->first();
        //                $current = Jobassociatecandidates::with(['candidate', 'job'])
        //                        ->where([
        //                            'job_id' => $value->job_id,
        //                            'candidate_id' => $value->candidate_id
        //                        ])
        //                        ->whereHas('job', function ($query) {
        //                            $query->where('job_opening_status', 'In-progress');
        //                        })
        //                        ->first();
        //                if (count($past)) {
        //                    $data['past'][] = $past;
        //                }
        //                if (count($current)) {
        //                    $data['current'][] = $current;
        //                }
        //            }
        //        }
        $statusList = ["No-Show", "Converted - Employee", "Converted - Contractor"];
        $past = Jobassociatecandidates::with(['candidate', 'job'])
            ->whereHas('job', function ($query) {
                $query->where('job_opening_status', 'Filled');
            })
            ->whereHas('job.contact', function ($query) {
                $query->where('id', Auth::user()->contact_tbl_id);
            })
            ->whereIn('status', $statusList)
            ->get();
        $current = Jobassociatecandidates::with(['candidate', 'job'])
            ->whereHas('job', function ($query) {
                $query->where('job_opening_status', 'In-progress');
            })
            ->whereHas('job.contact', function ($query) {
                $query->where('id', Auth::user()->contact_tbl_id);
            })
            ->whereIn('status', $statusList)
            ->get();

        return view('users::successfulhires', compact('past', 'current'));
    }

    public function candidateData()
    {
        $data['requested_profiles'] = $this->requestedProfiles();
        $data['requested_profiles_candidate_ids'] = $this->requestedProfilesCandidateIds();
        $data['wishlist'] = $this->clientWishlist();
        $data['recently_viewed'] = $this->recentlyViewedList();
        return $data;
    }

    public function requestedProfiles()
    {
        return Candidateprofilerequests::with('candidate')
            ->whereHas('candidate', function ($query) {
                $query->where('is_delete', 0);
            })
            ->where('CONTACTID', Auth::user()->CONTACTID)
            ->whereIn('request_status', ["New", 'Approved'])
            ->groupBy('CANDIDATEID')
            ->orderBy('id', 'desc')
            ->get();
    }

    public function requestedProfilesCandidateIds()
    {
        return Candidateprofilerequests::where('CONTACTID', Auth::user()->CONTACTID)
            ->whereIn('request_status', ["New", 'Approved'])
            ->pluck('CANDIDATEID', 'CANDIDATEID')
            ->toArray();
    }

    public function clientWishlist()
    {
        return Clientwishlist::with('candidate')
            ->whereHas('candidate', function ($query) {
                $query->where('is_delete', 0);
            })
            ->where('CLIENTID', Auth::user()->zoho_id)
            ->where('contact_id', Auth::user()->contact_tbl_id)
            ->orderBy('updated_at', 'desc')
            ->get();
    }

    public function recentlyViewedList()
    {
        return Recentlyviewedconsultant::with('candidate')
            ->whereHas('candidate', function ($query) {
                $query->where('is_delete', 0);
            })
            ->where('CLIENTID', Auth::user()->zoho_id)
            ->where('contact_id', Auth::user()->contact_tbl_id)
            ->orderBy('updated_at', 'desc')
            ->limit(10)
            ->get();
    }

    public function jobCounts()
    {
        $concludedJobs = Jobopenings::where('CLIENTID', Auth::user()->zoho_id)->whereNotNull('db_contact_id')->where('db_contact_id', Auth::user()->contact_tbl_id)->where('job_opening_status', '!=', "In-progress")->where('job_opening_status', '!=', "Pending")->count();
        $activeJobs = Jobopenings::where('CLIENTID', Auth::user()->zoho_id)->whereNotNull('db_contact_id')->where('db_contact_id', Auth::user()->contact_tbl_id)->where('job_opening_status', "In-progress")->count();
        return ['concludedJobs' => $concludedJobs, 'activeJobs' => $activeJobs];
    }

    public function interviewCounts()
    {
        $upcomingInterview = Interviews::where('CLIENTID', Auth::user()->zoho_id)->where('start_datetime', '>=', date('Y-m-d H:i:s'))->count();
        //        $successfulHires = Interviews::where('CLIENTID', Auth::user()->zoho_id)->whereNotNull('interview_status')->where('interview_status', 'Selected')->count();
        $statusList = ["No-Show", "Converted - Employee", "Converted - Contractor"];
        $past = Jobassociatecandidates::with(['candidate', 'job'])
            ->whereHas('job', function ($query) {
                $query->where('job_opening_status', 'Filled');
            })
            ->whereHas('job.contact', function ($query) {
                $query->where('id', Auth::user()->contact_tbl_id);
            })
            ->whereIn('status', $statusList)
            ->count();
        $current = Jobassociatecandidates::with(['candidate', 'job'])
            ->whereHas('job', function ($query) {
                $query->where('job_opening_status', 'In-progress');
            })
            ->whereHas('job.contact', function ($query) {
                $query->where('id', Auth::user()->contact_tbl_id);
            })
            ->whereIn('status', $statusList)
            ->count();
        $successfulHires = $past + $current;
        return ['upcomingInterview' => $upcomingInterview, 'successfulHires' => $successfulHires];
    }

    public function getClientProfile()
    {
        return Clientcontact::with('client')->where('id', Auth::user()->contact_tbl_id)->first();
    }

    /* Download candidate CV */

    public function downloadCandidateCv(Request $request)
    {
        $attachment = Attachment::where('zoho_id', $request->id)->where('category', "Formatted Resume")->first();
        if ($attachment == null) {
            $attachment = Attachment::where('zoho_id', $request->id)->where('category', "Resume")->first();
        }
        if ($attachment != null) {
            $isAlreadyDownload = 0;
            /* Downloading file from local storage */
            $destination_folder = public_path('cv/' . $attachment->downloaded_cvname);
            if ($attachment->downloaded_cvname != null) {
                if (file_exists($destination_folder)) {
                    $isAlreadyDownload = 1;
                }
            }
            /* Downloading file from zoho */
            if ($isAlreadyDownload == 0) {
                /* Dowloading File */
                $url = "https://recruit.zoho.com/recruit/private/xml/Candidates/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $attachment->attachment_id;
                $ext = pathinfo($attachment->filename, PATHINFO_EXTENSION);
                /* Storing in public cv folder */
                $fileName = time() . '.' . $ext;
                $destination_folder = public_path('cv/' . $fileName);
                file_put_contents("$destination_folder", fopen($url, 'r'));
                /* Save downwloaded file name in DB */
                $attachment->downloaded_cvname = $fileName;
                $attachment->save();
            }
            return response()->download($destination_folder);
        } else {
            Session::flash('error', 'Formated resume or Resume does not exists. It cannot be approved.');
            return redirect()->route('client-home');
        }
    }

    public function interviews()
    {
        $clientRecord = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $territory = "MY";
        if ($clientRecord != null) {
            if ($clientRecord->territory != null) {
                $territory = $clientRecord->territory;
            }
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        // $client = new Client($this->token);
        // $interViews = $client->getSearchRecords(
        //     'Interviews',
        //     'Interviews(Posting Title,Job Opening ID,Interview Name,Candidate Name,Start DateTime,Type,Interview Owner,From,To,Interview Status,Venue)',
        //     '(CLIENTID|is|' . Auth::user()->zoho_id . ')',
        //     ['sortColumnString' => 'Last Activity Time', 'sortOrderString' => 'desc']
        // );
        $upComingInterviewCount = 0;
        $successfulHiresCount = 0;
        $interviewList['upcoming_interviews'] = [];
        $interviewList['concluded_interviews'] = [];

        // if (count($interViews)) {
        //     foreach ($interViews as $key1 => $value1) {
        //         $job = $candidate = [];
        //         $job = $client->getRecordById('JobOpenings', $value1['JOBOPENINGID']);
        //         $candidate = $client->getRecordById('Candidates', $value1['CANDIDATEID']);
        //         if (count($job)) {
        //             $value1['job'] = $job[0];
        //         }
        //         if (count($candidate)) {
        //             $value1['candidate'] = $candidate[0];
        //         }
        //         if (date("Y-m-d H:i:s") <= $value1['Start DateTime']->format('Y-m-d H:i:s')) {
        //             $upComingInterviewCount++;

        //             $interviewList['upcoming_interviews'][] = $value1;
        //         }
        //         if (date("Y-m-d H:i:s") > $value1['Start DateTime']->format('Y-m-d H:i:s')) {
        //             $successfulHiresCount++;
        //             $interviewList['concluded_interviews'][] = $value1;
        //         }
        //     }
        // }
        return [
            'upComingInterviewCount' => $upComingInterviewCount,
            'successfulHiresCount' => $successfulHiresCount,
            'interviewList' => $interviewList
        ];
    }

    public function jobs()
    {
        $clientRecord = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $territory = "MY";
        if ($clientRecord != null) {
            if ($clientRecord->territory != null) {
                $territory = $clientRecord->territory;
            }
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        $client = new Client($this->token);
        $jobs = $client->getSearchRecords(
            'JobOpenings',
            'JobOpenings(Job ID,Job Opening Status)',
            '(Client Name|contains|*' . Auth::user()->first_name . '*)'
        );
        $activeJobsCount = 0;
        $concludedJobsCount = 0;
        if (count($jobs)) {
            foreach ($jobs as $key1 => $value1) {
                if ($value1['Job Opening Status'] == "In-progress") {
                    $activeJobsCount++;
                } else {
                    $concludedJobsCount++;
                }
            }
        }
        return [
            'activeJobsCount' => $activeJobsCount,
            'concludedJobsCount' => $concludedJobsCount
        ];
    }

    /*
     * Client Profile
     *
     */

    public function clientProfile(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            /* Client Profile */
            $profileArray = $this->getClientProfile();
            $id = Crypt::encryptString(Auth::user()->zoho_id);
            return view('users::client_profile', compact('profileArray', 'id'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Edit contact profile
     * 
     */

    public function editContactProfile(Request $request)
    {
        $contact = Clientcontact::where('id', Auth::user()->contact_tbl_id)->first();
        $user = User::where('id', Auth::user()->id)->first();
        if ($contact != null) {
            $html = view('users::getprofile', compact('contact', 'user'))->render();
            return response()->json([
                'status' => 200,
                'message' => 'Profile found successfully',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Oops! Profile not found',
                'html' => ''
            ]);
        }
    }

    /*
     * view contact profile
     * 
     */

    public function viewContactProfile(Request $request)
    {
        // $contact = Clientcontact::where('id', Auth::user()->contact_tbl_id)->first();
        $user = User::where('id', Auth::user()->id)->first();
        // print_r($request->all());
        // exit;
        if ($request->requestCancel != "true") {
            if ($user != null) {
                $user->first_name = $request->firstname;
                $user->last_name = $request->lastname;
                $user->phone = $request->mobile;
                if ($request->file('profile_pic') != null) {
                    $filename = time() . '-' . $request->file('profile_pic')->getClientOriginalName();
                    $path = public_path('profile_image/' . $filename);
                    Image::make($request->file('profile_pic')->getRealPath())->save($path);
                    $user->profile_pic = $filename;
                    $this->uploadProfilePicInZoho($user->CONTACTID, $path);
                }
                

                $user->save();
            }
            $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
            if ($client != null) {
                $client->client_name = $request->title;
                $client->is_synced = 0;
                $client->save();
            }
            $contact = Clientcontact::where('id', Auth::user()->contact_tbl_id)->first();
            if ($contact != null) {
                $contact->first_name = $request->firstname;
                $contact->last_name = $request->lastname;
                $contact->job_title = $request->title;
                $contact->client_name = $request->title;
                $contact->department = $request->department;
                $contact->mobile = $request->mobile;
                $contact->mailing_street = $request->mailing_street;
                $contact->mailing_city = $request->mailing_city;
                $contact->mailing_zip = $request->mailing_zip;
                $contact->mailing_state = $request->mailing_state;
                $contact->mailing_country = $request->mailing_country;
                $contact->updated_at = date('Y-m-d H:i:s', time());
                $contact->is_synced = 0;
                $contact->save();
            }
        }

        $profileArray = $this->getClientProfile();

        $user = User::where('id', Auth::user()->id)->first();
        if ($profileArray != null) {
            $html = view('users::viewprofile', compact('profileArray', 'user'))->render();
            return response()->json([
                'status' => 200,
                'message' => 'Profile found successfully',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Oops! Profile not found',
                'html' => ''
            ]);
        }
    }

    public function updateClientProfile(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user != null) {
            $user->first_name = $request->firstname;
            $user->last_name = $request->lastname;
            $user->phone = $request->mobile;
            if ($request->file('profile_pic') != null) {
                $filename = time() . '-' . $request->file('profile_pic')->getClientOriginalName();
                $path = public_path('profile_image/' . $filename);
                Image::make($request->file('profile_pic')->getRealPath())->save($path);
                $user->profile_pic = $filename;
                $this->uploadProfilePicInZoho($user->CONTACTID, $path);
            }
            $user->save();
        }
        $contact = Clientcontact::where('id', Auth::user()->contact_tbl_id)->first();
        if ($contact != null) {
            $contact->first_name = $request->firstname;
            $contact->last_name = $request->lastname;
            $contact->job_title = $request->title;
            $contact->client_name = $request->title;
            $contact->department = $request->department;
            $contact->mobile = $request->mobile;
            $contact->mailing_street = $request->mailing_street;
            $contact->mailing_city = $request->mailing_city;
            $contact->mailing_zip = $request->mailing_zip;
            $contact->mailing_state = $request->mailing_state;
            $contact->mailing_country = $request->mailing_country;
            $contact->updated_at = date('Y-m-d H:i:s', time());
            $contact->is_synced = 0;
            $contact->save();
        }
        Session::flash('success', 'Profile has been updated successfully.');
        return redirect()->route('client-profile');
    }

    public function uploadProfilePicInZoho($recordId, $filePath)
    {
        $contactRecord = Clientcontact::where('CONTACTID', Auth::user()->CONTACTID)->first();
        $territory = "MY";
        if ($contactRecord != null) {
            if ($contactRecord->territory != null) {
                $territory = $contactRecord->territory;
            }
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        $ch = curl_init();
        $cFile = new CURLFile($filePath, 'image/png');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://recruit.zoho.com/recruit/private/xml/Contacts/uploadPhoto?authtoken=" . $this->token . "&scope=recruitapi&version=2");
        curl_setopt($ch, CURLOPT_POST, true);
        $post = array("id" => $recordId, "content" => $cFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        return $response;
    }

    /*
     * upload contact profile picture
     * 
     */

    public function uploadContactProfilePicture()
    {
        // $user = User::where('contact_tbl_id', Auth::user()->contact_tbl_id)->first();
        $user = User::where('id', Auth::user()->id)->first();

        if ($user != null) {
            if ($request->file('profile_pic') != null) {
                $filename = time() . '-' . $request->file('profile_pic')->getClientOriginalName();
                $path = public_path('profile_image/' . $filename);
                Image::make($request->file('profile_pic')->getRealPath())->save($path);
                $user->profile_pic = $filename;
                $this->uploadProfilePicInZoho($user->CONTACTID, $path);
            }
            $user->save();
            return response()->json([
                'status' => 200,
                'message' => 'Profile image has been uploaded successfully'
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Profile image can not be upload. Please try again'
            ]);
        }
    }

    /*
     * Remove contact profile picture
     * 
     */

    public function removeContactProfilePicture()
    {
        $user = User::where('contact_tbl_id', Auth::user()->contact_tbl_id)->first();
        if ($user != null) {
            if (file_exists(public_path('profile_image/' . $user->profile_pic))) {
                unlink(public_path('profile_image/' . $user->profile_pic));
            }
            $user->profile_pic = "";
            $path = public_path('profile_image/zoho_default.png');
            $this->uploadProfilePicInZoho($user->CONTACTID, $path);
            $user->save();
            return response()->json([
                'status' => 200,
                'message' => 'Profile image has been deleted successfully'
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Profile image can not delete. Please try again'
            ]);
        }
    }

    /* Update Client Profile */

    public function editClientProfile(Request $request)
    {
        if ($request->isMethod('POST')) {
            DB::table('users')->where('id', Auth::user()->id)->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'sap_job_title' => $request->job_title,
                'hear' => $request->hear,
                'phone' => $request->phone,
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
            DB::table('clients')->where('CLIENTID', Auth::user()->zoho_id)->update([
                'client_name' => $request->first_name . ' ' . $request->last_name,
                'others' => $request->job_title,
                'contact_number' => $request->phone,
                'is_synced' => 0,
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
            Session::flash('success', 'Profile has been updated successfully.');
            return redirect()->route('client-profile');
        } else {
            return view('users::client-profile');
        }
    }

    /*
     * Client Interviews
     *
     */

    public function clientInterviews(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            //            $upcomingInterviews = Interviews::with([
            //                                'job',
            //                                'candidate',
            //                                'notes' => function ($query) {
            //                                    $query->latest('created_time')->first();
            //                                }
            //                            ])
            //                            ->where('territory', Session::get('territory'))->where('start_datetime', '>=', date('Y-m-d') . ' 00:00:00')->where('CLIENTID', Auth::user()->zoho_id)->get();
            //            $concludedInterviews = Interviews::with([
            //                                'job',
            //                                'candidate',
            //                                'notes' => function ($query) {
            //                                    $query->latest('created_time')->first();
            //                                }
            //                            ])
            //                            ->where('territory', Session::get('territory'))->where(function ($query) {
            //                        $query->where('interview_status', '<>', "");
            //                        $query->whereNotNull('interview_status');
            //                    })->where('CLIENTID', Auth::user()->zoho_id)->get();
            //            $recentlyViewed = $this->recentlyViewedList();
            $wishlist = [];
            if (Auth::check()) {
                $wishlist = Clientwishlist::where([
                    'CLIENTID' => Auth::user()->zoho_id,
                    'contact_id' => Auth::user()->contact_tbl_id
                ])->pluck('CANDIDATEID')->toArray();
            }
            $interviews = $this->interviews();
            //            $addTocompare = Addcompare::where([
            //                        'CLIENTID' => Auth::user()->zoho_id,
            //                        'contact_id' => Auth::user()->contact_tbl_id
            //                    ])->pluck('CANDIDATEID')->toArray();
            return view('users::client_interviews', compact('interviews', 'wishlist'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * Client Jobs
     *
     */

    public function clientJobs(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            if ($request->isMethod('POST')) {
                $this->clientPostJob($request);
                Session::flash('success', 'Your job has been sent to the launch pad. We will update you on the approval, Captain!');
                return redirect()->route('client-jobs', ['tab' => 'pendingJobs']);
            } else {
                $experienceList1 = $this->experienceList();
                $jobTypeList = $this->jobTypeList();
                $jobModeList = $this->jobModeList();
                $stateList = $this->stateList();
                $industryList = $this->industryList();
                $jobCategoryList = $this->jobCategoryList();
                $jobList = $this->clientJobList();
                $wishlist = [];
                if (Auth::check()) {
                    $wishlist = Clientwishlist::where([
                        'CLIENTID' => Auth::user()->zoho_id,
                        'contact_id' => Auth::user()->contact_tbl_id
                    ])->pluck('CANDIDATEID')->toArray();
                }
                return view('users::client_jobs', compact('experienceList1', 'jobTypeList', 'jobModeList', 'stateList', 'industryList', 'jobCategoryList', 'jobList', 'wishlist'));
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    public function clientJobList()
    {
        $jobs = Jobopenings::with(['associatecandidates.candidate', 'notes'])->where('territory', Session::get('territory'))->where('CLIENTID', Auth::user()->zoho_id)->whereNotNull('db_contact_id')->where('db_contact_id', Auth::user()->contact_tbl_id)->orderBy('updated_at', 'desc')->get()->toArray();
        $data['pendingJobs'] = [];
        $data['activeJobs']['list'] = [];
        $data['activeJobs']['candidates'] = [];
        $data['history']['list'] = [];
        $data['history']['candidates'] = [];

        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];
        $candidateSubmissionStatus = ["Submitted-to-client", "Approved by client", "Rejected by client"];
        $candidateInterviewStatus = ["Interview-to-be-Scheduled", "Interview-Scheduled", "Interview-in-Progress", "Interviewed-Accepted", "Interviewed-Rejected"];
        $candidateOfferStatus = ["To-be-Offered", "Offer-Accepted", "Offer-Made", "Offer-Declined", "Offer-Withdrawn"];
        $candidateHireStatus = ["No-Show", "Converted - Employee", "Converted - Contractor"];

        if (count($jobs)) {
            foreach ($jobs as $key => $value) {
                if ($value['job_opening_status'] == "Pending") {
                    $data['pendingJobs'][] = $value;
                }
                if ($value['job_opening_status'] == "In-progress") {
                    $data['activeJobs']['list'][$key]['data'] = $value;
                    if (count($value['associatecandidates'])) {
                        foreach ($value['associatecandidates'] as $candidateKey => $candidateValue) {
                            /* For Screening Tab */
                            if (in_array($candidateValue['status'], $candidateScreeningStatus)) {
                                $data['activeJobs']['list'][$key]['screening'][] = $candidateValue['candidate'];
                            }
                            /* For Submission Tab */
                            if (in_array($candidateValue['status'], $candidateSubmissionStatus)) {
                                $data['activeJobs']['list'][$key]['submission'][] = $candidateValue['candidate'];
                            }
                            /* For Interview Tab */
                            if (in_array($candidateValue['status'], $candidateInterviewStatus)) {
                                $data['activeJobs']['list'][$key]['interview'][] = $candidateValue['candidate'];
                            }
                            /* For Offer Tab */
                            if (in_array($candidateValue['status'], $candidateOfferStatus)) {
                                $data['activeJobs']['list'][$key]['offer'][] = $candidateValue['candidate'];
                            }
                            /* For Hire Tab */
                            if (in_array($candidateValue['status'], $candidateHireStatus)) {
                                $data['activeJobs']['list'][$key]['hire'][] = $candidateValue['candidate'];
                            }
                        }
                    }
                }
                if ($value['job_opening_status'] != "Pending" && $value['job_opening_status'] != "In-progress") { // Completed
                    $data['history']['list'][$key]['data'] = $value;

                    if (count($value['associatecandidates'])) {
                        foreach ($value['associatecandidates'] as $candidateKey => $candidateValue) {
                            /* For Screening Tab */
                            if (in_array($candidateValue['status'], $candidateScreeningStatus)) {
                                $data['history']['list'][$key]['screening'][] = $candidateValue['candidate'];
                            }
                            /* For Submission Tab */
                            if (in_array($candidateValue['status'], $candidateSubmissionStatus)) {
                                $data['history']['list'][$key]['submission'][] = $candidateValue['candidate'];
                            }
                            /* For Interview Tab */
                            if (in_array($candidateValue['status'], $candidateInterviewStatus)) {
                                $data['history']['list'][$key]['interview'][] = $candidateValue['candidate'];
                            }
                            /* For Offer Tab */
                            if (in_array($candidateValue['status'], $candidateOfferStatus)) {
                                $data['history']['list'][$key]['offer'][] = $candidateValue['candidate'];
                            }
                            /* For Hire Tab */
                            if (in_array($candidateValue['status'], $candidateHireStatus)) {
                                $data['history']['list'][$key]['hire'][] = $candidateValue['candidate'];
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

    public function clientPostJob($requestData)
    {
        $state = States::where('id', $requestData->state)->first()->state_name;

        if(isset($requestData->isAdmin)) {
            $user = User::where('CONTACTID', $requestData->contactId)->first();
            $client = Clients::where('CLIENTID', $user->zoho_id)->first();
        } else {
            $user = User::where('id', Auth::user()->id)->first();
            $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        }

        // $Job_ID = Settings::where('settingkey','job_number')->first()->settingvalue;
        // $JobIncrement = $Job_ID + 1;
        //     for ($i= $JobIncrement ; $i <= $JobIncrement + 1000  ; $i++) { 
        //         # code...
        //             $Job_IDexist = Jobopenings::where('Job_ID','Job_'.$i)->exists();
        //         if ($Job_IDexist == 0){
        //             $JobIncrementI = $i;
        //             break;
        //         }
        //     }
        // $Settings = Settings::where('settingkey','job_number')->first();
        // $Settings->settingvalue = $JobIncrementI;
        // $Settings->save();

        $job = new Jobopenings();
        $job->client_id = $client->id;

        if(isset($requestData->isAdmin)) {
            $job->CLIENTID = $user->zoho_id;
            $job->CONTACTID = $user->CONTACTID;
            $job->db_contact_id = $user->contact_tbl_id;
            $job->contact_name = $user->first_name . ' ' . $user->last_name;
        }else {
            $job->CLIENTID = Auth::user()->zoho_id;
            $job->CONTACTID = Auth::user()->CONTACTID;
            $job->db_contact_id = Auth::user()->contact_tbl_id;
            $job->contact_name = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        }
        $job->posting_title = $requestData->job_title;
        // $job->Job_ID = 'Job_'.$JobIncrementI;
        $job->job_base_rate = $requestData->baserate;
        $job->actual_revenue = $requestData->budget;
        $job->work_experience = $requestData->experiece;
        $job->job_type = $requestData->job_type;
        $job->job_mode = $requestData->job_mode;
        $job->date_opened = date('Y-m-d', time());
        $job->target_date = date('Y-m-d', strtotime($requestData->start_date));
        $job->job_duration = $requestData->duration;
        $job->state = $state;
        $job->city = $requestData->city;
        $job->industry = $requestData->industry;
        if ($client->territories == "MY") {
            $job->account_manager = "Talent Admin";
        } else {
            $job->account_manager = "Talent PH Admin";
        }
        $job->territory = $client->territories;
        $job->category = implode(',', $requestData->category);
        $job->key_skills = $requestData->key_skills;
        $job->job_description = $requestData->job_description;
        $job->job_requirements = $requestData->requirements;
        $job->job_benefits = $requestData->benefits;
        $job->all_description = '<span id="spandesc"><div>' . $requestData->job_description . '</div></span><br /><span id="spanreq"><h3>Requirements</h3><div>' . $requestData->requirements . '</div></span><br /><span id="spanben"><h3>Benefits</h3><div>' . $requestData->benefits . '</div></span>';
        $job->job_opening_status = "Pending";
        $job->is_synced = 0;
        $job->publish_in_us = 1;
        $job->block = 0;
        $job->save();

        $consultantIds = [];
        if (isset($requestData->category)) {
            Jobopeningcategory::where([
                'job_id' => $job->id
            ])->delete();
            foreach ($requestData->category as $catKey => $catValue) {
                $category = new Jobopeningcategory();
                $category->job_id = $job->id;
                // $category->JOBOPENINGID = $job->JOBOPENINGID;
                $category->category = $catValue;
                $category->created_at = date('Y-m-d H:i:s', time());
                $category->updated_at = date('Y-m-d H:i:s', time());
                $category->save();

                /* Finding consultant to send only one notification to a consultant even if he belongs to more than one category */
                $consultant = User::whereRaw('JSON_SEARCH(job_alerts_tags, "all", "' . $catValue . '%") IS NOT NULL')->get();
                if ($consultant != null) {
                    foreach ($consultant as $consultantKey => $consultantValue) {
                        $consultantIds[] = $consultantValue->id;
                    }
                }
            }
        }
        if (!empty($consultantIds)) {
            $cIds = array_unique($consultantIds);
            /* Send Notification to consultant */
            $consultant = User::whereIn('id', $cIds)->get();
            if (count($consultant)) {
                if ($client != null) {
                    foreach ($consultant as $key => $value) {
                        $check = User::where('id', $value->id)->first();
                        if ($check != null) {
                            if ($check->job_alerts == 1) {
                                $data['db_sender_id'] = Auth::user()->contact_tbl_id;
                                $data['db_receiver_id'] = $value->id;
                                $data['sender_id'] = Auth::user()->CONTACTID;
                                $data['receiver_id'] = $value->zoho_id;
                                $data['message'] = 'A client has posted a job for category [' . implode(',', $requestData->category) . '] which matches to your profile';
                                $data['type'] = "job_post";
                                $data['to'] = "Consultant";
                                $data['redirect_url'] = route('consultantjobdetails', [\Crypt::encryptString($job->id)]);
                                $this->sendNotification($data);
                            }
                        }
                    }
                }
            }
        }
        /* Send Notification to client it self */
        if ($client != null) {
            $data['db_sender_id'] = Auth::user()->contact_tbl_id;
            $data['db_receiver_id'] = Auth::user()->contact_tbl_id;
            $data['sender_id'] = Auth::user()->CONTACTID;
            $data['receiver_id'] = Auth::user()->CONTACTID;
            $data['message'] = "A client has posted a job for category [" . implode(',', $requestData->category) . "]";
            $data['type'] = "job_post";
            $data['job_id'] = $job->id;
            $data['to'] = "Client";
            $data['redirect_url'] = route('job-details', ['locale' => $user->territory, 'id' => \Crypt::encryptString($job->id)]);
            $this->sendNotification($data);
            /* Send Notification to Admin */
            if ($client->territories == "PH") {
                $setting = Settings::where('settingkey', "ph_email")->first();
            } else {
                $setting = Settings::where('settingkey', "my_email")->first();
            }
            $notificationData['db_sender_id'] = Auth::user()->contact_tbl_id;
            $notificationData['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
            $notificationData['sender_id'] = Auth::user()->CONTACTID;
            $notificationData['receiver_id'] = "";
            $notificationData['message'] = Auth::user()->first_name . ' ' . Auth::user()->last_name . ' has new job posting for <a href="' . route('jobs.edit', [$job->id]) . '">' . $job->posting_title . '</a>';
            $notificationData['type'] = "job_post";
            $notificationData['to'] = "Admin";
            $notificationData['job_id'] = $job->id;
            $notificationData['territory'] = $client->territories;
            $this->sendNotification($notificationData);
            $template = EmailTemplate::where('name', 'client_job_post')->first();
            if ($template != null) {
                $emaildata['job_title'] = $job->posting_title;
                $emaildata['client_name'] = Auth::user()->first_name . ' ' . Auth::user()->last_name;
                $emaildata['email'] = $setting->settingvalue;
                $emaildata['subject'] = $client->client_name . ' has new job posting for ' . $job->posting_title;
                $emaildata['job_link'] = route('jobs.edit', [$job->id]);
                Mail::send([], [], function ($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                        ->subject($emaildata['subject'])
                        ->setBody($template->parse($emaildata), 'text/html');
                });
            }
        }
        return;
    }

    /* Client Edit Pending Job */

    public function clientEditJob(Request $request)
    {
        $decrypted = Crypt::decryptString($request->id);
        $job = Jobopenings::where('id', $decrypted)->first();
        if ($request->isMethod('POST')) {
            $state = States::where('id', $request->state)->first()->state_name;
            $job->posting_title = $request->job_title;
            $job->job_base_rate = $request->baserate;
            $job->actual_revenue = $request->budget;
            $job->work_experience = $request->experiece;
            $job->job_type = $request->job_type;
            $job->job_mode = $request->job_mode;
            $job->target_date = date('Y-m-d', strtotime($request->start_date));
            $job->job_duration = $request->duration;
            $job->state = $state;
            $job->city = $request->city;
            $job->industry = $request->industry;
            $job->category = implode(',', $request->category);
            $job->key_skills = $request->key_skills;
            $job->job_description = $request->job_description;
            $job->job_requirements = $request->requirements;
            $job->job_benefits = $request->benefits;
            $job->all_description = '<span id="spandesc"><div>' . $request->job_description . '</div></span><br /><span id="spanreq"><h3>Requirements</h3><div>' . $request->requirements . '</div></span><br /><span id="spanben"><h3>Benefits</h3><div>' . $request->benefits . '</div></span>';
            $job->job_opening_status = "Pending";
            $job->is_synced = 0;
            $job->save();
            if (isset($request->category)) {
                Jobopeningcategory::where([
                    'job_id' => $job->id
                ])->delete();
                foreach ($request->category as $catKey => $catValue) {
                    $category = new Jobopeningcategory();
                    $category->job_id = $job->id;
                    // $category->JOBOPENINGID = $job->JOBOPENINGID;
                    $category->category = $catValue;
                    $category->created_at = date('Y-m-d H:i:s', time());
                    $category->updated_at = date('Y-m-d H:i:s', time());
                    $category->save();
                }
            }
            Session::flash('success', 'Your job has been updated successfully.');
            return redirect()->route('client-jobs', ['tab' => 'pendingJobs']);
        } else {
            $experienceList = $this->experienceList();
            $jobTypeList = $this->jobTypeList();
            $jobModeList = $this->jobModeList();
            $stateList = $this->stateList();
            $industryList = $this->industryList();
            $jobCategoryList = $this->jobCategoryList();
            $stateRecord = States::where('state_name', $job->state)->first();
            $city = Cities::where('state_id', $stateRecord->id)->pluck('city_name', 'city_name')->toArray();
            $categories = ['Basis/S & A' => 'Basis/S & A', 'Functional' => 'Functional', 'Technical' => 'Technical', 'Project Manager/Solution Architect' => 'Project Manager/Solution Architect', 'Others' => 'Others'];
            return view('users::client_editjob', compact('job', 'experienceList', 'jobTypeList', 'jobModeList', 'stateList', 'industryList', 'jobCategoryList', 'stateRecord', 'city', 'categories'));
        }
    }

    /* Add Job Note */

    public function clientAddJobNote(Request $request)
    {
        $job = Jobopenings::where('id', $request->job_id)->first();
        if ($job != null) {
            $zohoQuery = new ZohoqueryController();

            $territory = "MY";
            if ($job->territory != null) {
                $territory = $job->territory;
            }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;
            
            $lastId = Jobnotes::all()->last()->id;

            $note = new Jobnotes();
            $note->NOTEID = 'NOTE_0' . ( intval($lastId) + 1);
            $note->JOBOPENINGID = $job->JOBOPENINGID;
            $note->job_id = $job->id;
            $note->title = 'Notes';
            $note->note_content = $request->note;
            $note->sm_owner_id = Auth::user()->id;
            $note->owner_name = Auth::user()->first_name;
            $note->sm_creator_id = Auth::user()->id;
            $note->created_by = Auth::user()->first_name;
            $note->created_time = date("Y-m-d H:i:s", time());
            $note->modify_by_id = Auth::user()->id;
            $note->modified_by =  Auth::user()->first_name;
            $note->modified_time = date("Y-m-d H:i:s", time());
            $note->created_at = date("Y-m-d H:i:s", time());
            $note->updated_at = date("Y-m-d H:i:s", time());
            $note->save();

            // $data['entityId'] = $job->JOBOPENINGID; // JobopeningId
            // $data['Note Type'] = "Notes";
            // $data['Type Id'] = "401682000000072353"; // Type Id For Note
            // $data['Note Content'] = $request->note;
            // $data['Parent Module'] = "JobOpenings";
            // $response = $zohoQuery->addJobNote($data);
            // if (!empty($response)) {
            //     $note = new Jobnotes();
            //     $note->NOTEID = $response[0]['id'];
            //     $note->JOBOPENINGID = $job->JOBOPENINGID;
            //     $note->job_id = $job->id;
            //     $note->title = $response[0]['Title'];
            //     $note->note_content = isset($response[0]['Note Content']) ? $response[0]['Note Content'] : "";
            //     $note->sm_owner_id = isset($response[0]['SMOWNERID']) ? $response[0]['SMOWNERID'] : "";
            //     $note->owner_name = isset($response[0]['Owner Name']) ? $response[0]['Owner Name'] : "";
            //     $note->sm_creator_id = isset($response[0]['SMCREATORID']) ? $response[0]['SMCREATORID'] : "";
            //     $note->created_by = isset($response[0]['Created By']) ? $response[0]['Created By'] : "";
            //     $note->created_time = isset($response[0]['Created Time']) ? $response[0]['Created Time']->format('Y-m-d H:i:s') : "";
            //     $note->modify_by_id = isset($response[0]['MODIFIEDBY']) ? $response[0]['MODIFIEDBY'] : "";
            //     $note->modified_by = isset($response[0]['Modified By']) ? $response[0]['Modified By'] : "";
            //     $note->modified_time = isset($response[0]['Modified Time']) ? $response[0]['Modified Time']->format('Y-m-d H:i:s') : "";
            //     $note->created_at = date("Y-m-d H:i:s", time());
            //     $note->updated_at = date("Y-m-d H:i:s", time());
            //     $note->save();

            //     $job->is_synced = 0;
            //     $job->save();
            // }
            Session::flash('success', 'Notes has been updated successfully.');
        } else {
            Session::flash('error', 'No such jobs found.');
        }
        return redirect()->route('client-jobs', ['tab' => 'activeJobs']);
    }

    /* Client Delete Pending Job */

    public function clientDeleteJob(Request $request)
    {
        Jobopenings::where('id', $request->id)->delete();
        Notification::where('job_id', $request->id)->delete();
        Session::flash('success', 'Job has been deleted successfully');
        return redirect()->route('client-jobs');
    }

    public function experienceList()
    {
        return [
            'None' => 'None',
            'Fresh' => 'Fresh',
            '1-3 years' => '1-3 years',
            '4-5 years' => '4-5 years',
            '5-10 years' => '5-10 years',
            '10+ years' => '10+ years'
        ];
    }

    public function jobTypeList()
    {
        return [
            'Contract' => 'Contract',
            'Permanent' => 'Permanent',
            'Internship' => 'Internship'
        ];
    }

    public function jobModeList()
    {
        return [
            'None' => 'None',
            'Full time' => 'Full time',
            'Part time' => 'Part time'
        ];
    }

    public function jobCategoryList()
    {
        return [
            'Basis/S & A' => 'Basis/S & A',
            'Functional' => 'Functional',
            'Technical' => 'Technical',
            'Project Manager/Solution Architect' => 'Project Manager/Solution Architect',
            'Others' => 'Others'
        ];
    }

    public function stateList()
    {
        $userTerritory = User::where('id', Auth::user()->id)->pluck('territory')->first();
        return States::where('country_name', $userTerritory)->pluck('state_name', 'id')->toArray();
    }

    public function getCityList(Request $request)
    {
        $city = Cities::where('state_id', $request->id)->pluck('city_name', 'city_name')->toArray();
        return response()->json([
            'city' => $city
        ]);
    }

    public function industryList()
    {
        return Industry::pluck('name', 'name')->toArray();
    }

    /* GENERAL SEARCH */

    public function generalSearch(Request $request)
    {

        if ($request->search_type == "consultant") {
            return $this->searchGeneralConsultant($request);
        } else if ($request->search_type == "job") {
            return $this->searchGeneralJob($request);
        }
        else { // Default
            return $this->searchGeneralConsultant($request);
        }
    }

    public function searchGeneralConsultant($request)
    {
        $search_type = $request->search_type;
        $query = Candidates::with(['attachments']);
        $searchterm = $available = $willing_to_travel = $full_time = $part_time = $project = $support = $job_title_matching = $baserate_from = $baserate_to = $baserate2_from = $baserate2_to = $avalabilityDate = $availability_date2 = $experience = $experience2 = "";
        $category = [];
        $category2 = [];
        $workpreference = [];
        $state = [];
        /* Category */
        if ((isset($request->category) && !empty($request->category)) || isset($request->category2)) {
            $category = $request->category;
            $category2 = $request->category2;
            if (!empty($category2)) {
                $query = $query->whereNotNull('category');
                // $query = $query->whereIn('category',$category2);
                $query = $query->whereHas('candidatecategory', function ($query) use ($category2) {
                    $query = $query->whereIn('category', $category2);
                });
                // $category = $category2;
            } else {
                $category2 = $category;
                $query = $query->whereNotNull('category');
                // $query = $query->whereIn('category',$category);
                $query = $query->whereHas('candidatecategory', function ($query) use ($category) {
                    $query = $query->whereIn('category', $category);
                });
            }
        }
        /* General */

        /* Available */
        if (isset($request->available)) {
            $available = "Yes";
            $query = $query->where('available_for_contract', 1);
        }

        /* Project */
        if (isset($request->project)) {
            $project = "Yes";
            $query = $query->where('project', 1);
        }
        /* Support */
        if (isset($request->support)) {
            $support = "Yes";
            $query = $query->where('support', 1);
        }
        /* Keyword Matching Job Title Only */
        if (isset($request->job_title_matching)) {
            $searchterm = $request->searchterm;
            $job_title_matching = "Yes";
            $query = $query->where('sap_job_title', 'LIKE', "%" . $searchterm . "%");
        } else if (isset($request->searchterm) && $request->searchterm != "") {
            //General search
            $searchterm = $request->searchterm;
            $query = $query->where(function ($query) use ($searchterm) {
                $query = $query->where('skill_set', 'LIKE', "%" . $searchterm . "%");
                $query = $query->orWhere('sap_job_title', 'LIKE', "%" . $searchterm . "%");
                $query = $query->orWhere('candidate_id', 'LIKE', "%" . $searchterm . "%");
            });
        }

        /* Base Rate */
        if ((isset($request->baserate_from) || isset($request->baserate_to)) || (isset($request->baserate2_from) || isset($request->baserate2_to))) {

            $baserate_from = $request->baserate_from;
            $baserate_to = $request->baserate_to;
            $baserate2_from = $request->baserate2_from;
            $baserate2_to = $request->baserate2_to;

            if ($baserate2_from != null && $baserate2_to != null) {
                $query = $query->whereBetween('base_rate', [$baserate2_from, $baserate2_to]);
                // $baserate_from = '';
                // $baserate_to = '';
            } else if ($baserate_from != null && $baserate_to != null) {
                $baserate2_from = $baserate_from;
                $baserate2_to = $baserate_to;
                $query = $query->whereBetween('base_rate', [$baserate_from, $baserate_to]);
            } else if ($baserate2_from != null) {
                // $baserate_from = '';
                // $baserate_to = '';
                $query = $query->where('base_rate', '>=', $baserate2_from);
            } else if ($baserate2_to != null) {
                // $baserate_from = '';
                // $baserate_to = '';
                $query = $query->where('base_rate', '<=', $baserate2_to);
            } else if ($baserate_from != null) {
                $baserate2_from = $baserate_from;
                $query = $query->where('base_rate', '>=', $baserate_from);
            } else if ($baserate_to != null) {
                $baserate2_to = $baserate_to;
                $query = $query->where('base_rate', '<=', $baserate_to);
            }
        }


        /* Availability Date */
        if (isset($request->availability_date) || isset($request->availability_date2)) {
            $avalabilityDate = $request->availability_date;
            $availability_date2 = $request->availability_date2;
            if ($availability_date2 != null) {
                if ($availability_date2 == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($availability_date2 == "month") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($availability_date2 == "three") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+90 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($availability_date2 == "six") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+180 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+365 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                }
            } else {
                $availability_date2 = $avalabilityDate;
                if ($avalabilityDate == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($avalabilityDate == "month") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($avalabilityDate == "three") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+90 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else if ($avalabilityDate == "six") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+180 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+365 day'));
                    $query = $query->whereNotNull('availability_date')->whereBetween('availability_date', [$startDate, $endDate]);
                }
            }
        }

        /* Experience */
        if (isset($request->experience) || isset($request->experience2)) {
            $experience = $request->experience;
            $experience2 = $request->experience2;
            if ($experience2 != null) {
                if ($experience2 == "0") {
                    $query = $query->where('experience_in_years', 0);
                } else if ($experience2 == "3") {
                    $query = $query->whereBetween('experience_in_years', [1, 3]);
                } else if ($experience2 == "5") {
                    $query = $query->whereBetween('experience_in_years', [4, 5]);
                } else if ($experience2 == "10") {
                    $query = $query->whereBetween('experience_in_years', [6, 10]);
                } else {
                    $query = $query->where('experience_in_years', '>', 10);
                }
            } else {
                $experience2 = $experience;
                if ($experience == "0") {
                    $query = $query->where('experience_in_years', 0);
                } else if ($experience == "3") {
                    $query = $query->whereBetween('experience_in_years', [1, 3]);
                } else if ($experience == "5") {
                    $query = $query->whereBetween('experience_in_years', [4, 5]);
                } else if ($experience == "10") {
                    $query = $query->whereBetween('experience_in_years', [6, 10]);
                } else {
                    $query = $query->where('experience_in_years', '>', 10);
                }
            }
        }
        /* Work Preferences */
        if (isset($request->willing_to_travel) || isset($request->full_time) || (isset($request->part_time))) {

            /* Willing to Travel */
            if (isset($request->willing_to_travel)) {
                $willing_to_travel = "Yes";
                $query = $query->where('willing_to_travel', 1);
            }
            /* full time */
            if (isset($request->full_time)) {
                $full_time = "Yes";
                $query = $query->where('full_time', 1);
            }
            /* Part Time */
            if (isset($request->part_time)) {
                $part_time = "Yes";
                $query = $query->where('part_time', 1);
            }
        } else if (isset($request->work_preference) && !empty($request->work_preference)) {
            $workpreference = $request->work_preference;
            if (in_array('Full Time', $workpreference)) {
                $full_time = "Yes";
                $query = $query->where('full_time', 1);
            }
            if (in_array("Part Time", $workpreference)) {
                $part_time = "Yes";
                $query = $query->where('part_time', 1);
            }
            if (in_array("Willing to travel", $workpreference)) {
                $willing_to_travel = "Yes";
                $query = $query->where('willing_to_travel', 1);
            }
        }
        /* State */
        if (isset($request->state) && !empty($request->state)) {
            $state = $request->state;
            $query = $query->whereIn('state', $state);
        }
        /* Teritorries */
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }

        $query = $query->where('block', 0)->where('internal_hire', 0)->where('publish_in_us', 1)->where('is_delete', 0)->whereNotIn('candidate_status', ['Unqualified', 'Qualified Expat', 'Unqualified Expat']);
        $candidates = $query->paginate(15);
        $total = $candidates->total();
        $candidates->appends([
            'search_type' => $search_type,
            'category' => $category,
            'category2' => $category2,
            'searchterm' => $searchterm,
            'available' => $available,
            'willing_to_travel' => $willing_to_travel,
            'full_time' => $full_time,
            'part_time' => $part_time,
            'project' => $project,
            'support' => $support,
            'job_title_matching' => $job_title_matching,
            'baserate_from' => $baserate_from,
            'baserate_to' => $baserate_to,
            'baserate2_from' => $baserate2_from,
            'baserate2_to' => $baserate2_to,
            'avalabilityDate' => $avalabilityDate,
            'availability_date2' => $availability_date2,
            'experience' => $experience,
            'experience2' => $experience2,
            'workpreference' => $workpreference,
            'state' => $state,
            'total' => $total

        ])->render(); // Append query string to URL
        $wishlist = [];
        if (Auth::check()) {
            $wishlist = Clientwishlist::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->pluck('CANDIDATEID')->toArray();
        }
        $addTocompare = [];
        if (Auth::check()) {
            $addTocompare = Addcompare::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->pluck('CANDIDATEID')->toArray();
        }
        $user = Auth::user();

        if(!isset($user)) {
            $user = json_encode( (object) ['role_id' => 0] );
        }

        return view('search-result-consultant', compact('user', 'candidates', 'category', 'category2', 'searchterm', 'search_type', 'available', 'willing_to_travel', 'full_time', 'part_time', 'project', 'support', 'job_title_matching', 'wishlist', 'baserate_from', 'baserate_to', 'baserate2_from', 'baserate2_to', 'avalabilityDate', 'availability_date2', 'experience', 'experience2', 'workpreference', 'state', 'addTocompare', 'total'));
    }

    public function searchGeneralJob($request)
    {
        // $jobopening = Jobopenings::get();
        // foreach ($jobopening as $jkey => $jvalue) {
        //     # code...
        //     if(isset($jvalue->category) && !empty($jvalue->category)){
        //         Jobopeningcategory::where([
        //             'job_id' => $jvalue->id
        //         ])->delete();
        //         $jcategory = explode(",", $jvalue->category);
        //         foreach($jcategory as $catKey => $catValue){
        //             $category = new Jobopeningcategory();
        //             $category->job_id = $jvalue->id;
        //             // $category->JOBOPENINGID = $job->JOBOPENINGID;
        //             $category->category = $catValue;
        //             $category->created_at = date('Y-m-d H:i:s',time());
        //             $category->updated_at = date('Y-m-d H:i:s',time());
        //             $category->save();
        //         }
        //     }
        // }
        $search_type = $request->search_type;
        $jobsearchterm = $job_type = $job_type2 = $job_baserate = $job_baserate_from = $job_baserate_to = $job_baserate2_from = $job_baserate2_to = $work_experience = $work_experience2 = $job_start_date = $job_start_date2 = $job_duration = $job_duration2 = $job_industry = $job_industry2 = "";
        $jobcategory = [];
        $jobcategory2 = [];
        $job_mode1 = [];
        $job_mode2 = [];
        $job_state = [];
        $job_state2 = [];
        
        $query = Jobopenings::with(['client', 'contact', 'jobopeningcategory'])
            ->whereHas('contact', function ($query) {
                $query->where('is_delete', 0);
            });
        /* Category */
        if ((isset($request->jobcategory) && !empty($request->jobcategory)) || (isset($request->job_category2) && !empty($request->job_category2))) {
            $jobcategory = $request->jobcategory;
            $jobcategory2 = $request->job_category2;
            if (!empty($jobcategory2)) {
                // $query = $query->whereIn('category', $request->job_category2);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory2) {
                    $query = $query->whereIn('category', $jobcategory2);
                });
            } else {
                $jobcategory2 = $jobcategory;
                // $query = $query->whereIn('category', $request->jobcategory);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory) {
                    $query = $query->whereIn('category', $jobcategory);
                });
            }
        }
        if (isset($request->jobsearchterm) && $request->jobsearchterm != "") {
            $jobsearchterm = $request->jobsearchterm;
            $query = $query->where(function ($query2) use ($jobsearchterm) {
                $query2->where('Job_ID', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('posting_title', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('key_skills', 'LIKE', "%" . $jobsearchterm . "%");
            });
        }
        /* Job Mode */
        if ((isset($request->job_mode1) && !empty($request->job_mode1)) || (isset($request->job_mode2) && !empty($request->job_mode2))) {
            $job_mode1 = $request->job_mode1;
            $job_mode2 = $request->job_mode2;
            if (!empty($job_mode2)) {
                $query = $query->whereIn('job_mode', $job_mode2);
            } else {
                $job_mode2 = $job_mode1;
                $query = $query->whereIn('job_mode', $job_mode1);
            }
        }
        /* Job Type */
        if ((isset($request->job_type) && $request->job_type != "") || isset($request->job_type2) && $request->job_type2 != "") {
            $job_type = $request->job_type;
            $job_type2 = $request->job_type2;
            if ($job_type2 != null) {
                $query = $query->whereIn('job_type', $request->job_type2);
            } else {
                $job_type2 = $job_type;
                $query = $query->whereIn('job_type', $request->job_type);
            }
        }
        /* Job Base Rate */
        if ((isset($request->job_baserate_from) || isset($request->job_baserate_to)) || (isset($request->job_baserate2_from) || isset($request->job_baserate2_to))) {
            $job_baserate_from = $request->job_baserate_from;
            $job_baserate_to = $request->job_baserate_to;
            $job_baserate2_from = $request->job_baserate2_from;
            $job_baserate2_to = $request->job_baserate2_to;

            if ($job_baserate2_from != null && $job_baserate2_to != null) {
                $query = $query->whereBetween('job_base_rate', [$job_baserate2_from, $job_baserate2_to]);
            } else if ($job_baserate_from != null && $job_baserate_to != null) {
                $job_baserate2_from = $job_baserate_from;
                $job_baserate2_to = $job_baserate_to;
                $query = $query->whereBetween('job_base_rate', [$job_baserate_from, $job_baserate_to]);
            } else if ($job_baserate2_from != null) {
                $query = $query->where('job_base_rate', '>=', $job_baserate2_from);
            } else if ($job_baserate2_to != null) {
                $query = $query->where('job_base_rate', '<=', $job_baserate2_to);
            } else if ($job_baserate_from != null) {
                $job_baserate2_from = $job_baserate_from;
                $query = $query->where('job_base_rate', '>=', $job_baserate_from);
            } else if ($job_baserate_to != null) {
                $job_baserate2_to = $job_baserate_to;
                $query = $query->where('job_base_rate', '<=', $job_baserate_to);
            }
        }

        /* Work Experience */
        if ((isset($request->work_experience) && $request->work_experience != "") || isset($request->work_experience2)) {
            $work_experience = $request->work_experience;
            $work_experience2 = $request->work_experience2;
            if ($work_experience2 != null) {
                if ($work_experience2 == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience2 == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience2 == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience2 == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            } else {
                $work_experience2 = $work_experience;
                if ($work_experience == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            }
        }
        /* Start Date */
        if ((isset($request->job_start_date) && $request->job_start_date != "") || isset($request->job_start_date2)) {
            $job_start_date = $request->job_start_date;
            $job_start_date2 = $request->job_start_date2;
            if ($job_start_date2 != null) {
                if ($job_start_date2 == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date2 == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            } else {
                $job_start_date2 = $job_start_date;
                if ($job_start_date == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            }
        }
        /* Duration */
        if ((isset($request->job_duration) && $request->job_duration != "") || isset($request->job_duration2)) {
            $job_duration = $request->job_duration;
            $job_duration2 = $request->job_duration2;
            if ($job_duration2 != null) {
                if ($job_duration2 == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration2 == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration2 == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            } else {
                $job_duration2 = $job_duration;
                if ($job_duration == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            }
        }
        /* Industry */
        if ((isset($request->job_industry) && $request->job_industry != "") || isset($request->job_industry2)) {
            $job_industry = $request->job_industry;
            $job_industry2 = $request->job_industry2;
            if ($job_industry2 != null) {
                $query = $query->where('industry', $job_industry2);
            } else {
                $job_industry2 = $job_industry;
                $query = $query->where('industry', $job_industry);
            }
        }
        /* State */
        if ((isset($request->job_state) && !empty($request->job_state)) || (isset($request->job_state2) && !empty($request->job_state2))) {
            $job_state = $request->job_state;
            $job_state2 = $request->job_state2;
            if (!empty($job_state2)) {
                $query = $query->whereIn('state', $job_state2);
            } else {
                $job_state2 = $job_state;
                $query = $query->whereIn('state', $job_state);
            }
        }
        /* Teritorries */
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }
        $jobs = $query->where([
            'job_opening_status' => "In-progress",
            'publish_in_us' => 1,
            'block' => 0,
            'is_delete' => 0
        ])->get();
        $total = 0;

        // $jobs->appends([
        //     'search_type' => $search_type,
        //     'jobcategory' => $jobcategory,
        //     'jobcategory2' => $jobcategory2,
        //     'jobsearchterm' => $jobsearchterm,
        //     'job_type' => $job_type,
        //     'job_type2' => $job_type2,
        //     'job_baserate_from' => $job_baserate_from,
        //     'job_baserate_to' => $job_baserate_to,
        //     'job_baserate2_from' => $job_baserate2_from,
        //     'job_baserate2_to' => $job_baserate2_to,
        //     'work_experience' => $work_experience,
        //     'work_experience2' => $work_experience2,
        //     'job_start_date' => $job_start_date,
        //     'job_start_date2' => $job_start_date2,
        //     'job_mode1' => $job_mode1,
        //     'job_mode2' => $job_mode2,
        //     'job_duration' => $job_duration,
        //     'job_duration2' => $job_duration2,
        //     'job_industry' => $job_industry,
        //     'job_industry2' => $job_industry2,
        //     'job_state' => $job_state,
        //     'job_state2' => $job_state2,
        //     'total'=> $total
        // ])->render();

        $displayJobMightLike = 1;
        if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
                $displayJobMightLike = 0;
            }
            $jobsMightLike = [];
            if (Auth::user()->role_id == 2) {
                /* Jobs you might Like */
                $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                $categoryArray[] = "";
                $categoryArray = explode(';', $candidateDetailarray->category);
                $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
                $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
            }
        } else {
            $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
        }
        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];

        // return view('general-search-job', compact('displayJobMightLike', 'jobsMightLike', 'jobs', 'search_type', 'jobcategory', 'jobcategory2', 'jobsearchterm', 'job_mode1', 'job_mode2', 'job_type', 'job_type2', 'job_baserate_from', 'job_baserate_to', 'job_baserate2_from', 'job_baserate2_to', 'work_experience', 'work_experience2', 'job_start_date', 'job_start_date2', 'job_duration', 'job_duration2', 'job_industry', 'job_industry2', 'job_state', 'job_state2', 'candidateScreeningStatus', 'total'));

        $user = Auth::user();

        if(!isset($user)) {
            $user = json_encode( (object) ['role_id' => 0] );
        }

        return view('search-result-job', compact('displayJobMightLike', 'jobsMightLike', 'jobs', 'search_type', 'jobcategory', 'jobcategory2', 'jobsearchterm', 'job_mode1', 'job_mode2', 'job_type', 'job_type2', 'job_baserate_from', 'job_baserate_to', 'job_baserate2_from', 'job_baserate2_to', 'work_experience', 'work_experience2', 'job_start_date', 'job_start_date2', 'job_duration', 'job_duration2', 'job_industry', 'job_industry2', 'job_state', 'job_state2', 'candidateScreeningStatus', 'total', 'user'));
    }


    
    /* View Job Details */

    public function viewJobDetail(Request $request)
    {
       
        // exception throw for job details from vue js

            try {
                $decrypted = Crypt::decryptString($request->id);  //global request
            } catch (DecryptException $e) {
                $decrypted = $request->id;  //request came from vue 
            }
        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];
        $candidateSubmissionStatus = ["Submitted-to-client", "Approved by client", "Rejected by client"];
        $candidateInterviewStatus = ["Interview-to-be-Scheduled", "Interview-Scheduled", "Interview-in-Progress", "Interviewed-Accepted", "Interviewed-Rejected"];
        $candidateOfferStatus = ["To-be-Offered", "Offer-Accepted", "Offer-Made", "Offer-Declined", "Offer-Withdrawn"];
        $candidateHireStatus = ["No-Show", "Converted - Employee", "Converted - Contractor"];
        $job = Jobopenings::with(['associatecandidates', 'contact'])
            ->where('id', $decrypted)
            ->first();
        if ($job->is_delete == 0) {
            $displayJobMightLike = 1;
            if (Auth::check()) {
                if (Auth::user()->role_id == 1) {
                    $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
                    $displayJobMightLike = 0;
                } else {
                    /* Jobs you might Like */
                    $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                    $categoryArray[] = "";
                    if (count($candidateDetailarray)) {
                        $categoryArray = explode(';', $candidateDetailarray->category);
                    }
                    $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
                    $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
                }
            } else {
                $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
            }
            return view("job_details", compact('displayJobMightLike', 'jobsMightLike', 'job', 'candidateScreeningStatus', 'candidateSubmissionStatus', 'candidateInterviewStatus', 'candidateOfferStatus', 'candidateHireStatus'));
        } else {
            Session::flash('error', 'This job has been deleted');
            if (Auth::check()) {
                if (Auth::user()->role_id != 3) {
                    return redirect('/' . Session::get('territory'));
                } else {
                    return redirect()->route('dashboard');
                }
            } else {
                return redirect('/' . Session::get('territory'));
            }
        }
    }

    /* View consultant Details */

    public function viewConsultantDetail(Request $request)
    {
        // $decrypted = Crypt::decryptString($request->id);
        try {
            $decrypted = Crypt::decryptString($request->id);  //global request
        } catch (DecryptException $e) {
            $decrypted = $request->id;  //request came from vue 
        }
        // return $decrypted;
        $consultant = Candidates::where('CANDIDATEID', $decrypted)->first();
        $wishlist = [];
        $experience = [];
        if ($consultant->is_delete == 0) {
            if (Auth::check()) {
                $wishlist = Clientwishlist::where([
                    'CLIENTID' => Auth::user()->zoho_id,
                    'contact_id' => Auth::user()->contact_tbl_id
                ])->pluck('CANDIDATEID')->toArray();
                $experience = Experience::where([
                    'CANDIDATEID' => $consultant->CANDIDATEID,
                ])->get();
                // print_r($experience->toArray());
                /* Update in recently viewed */
                $addTocompare = [];
                if (Auth::user()->role_id == 1) {
                    $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
                    if ($client != null) {
                        $recentEntry = Recentlyviewedconsultant::where([
                            'client_id' => $client->id,
                            'candidate_id' => $consultant->id
                        ])->first();
                        if ($recentEntry != null) {
                            $recentEntry->updated_at = date('Y-m-d H:i:s', time());
                            $recentEntry->save();
                        } else {
                            $recentlyViewed = new Recentlyviewedconsultant();
                            $recentlyViewed->client_id = $client->id;
                            $recentlyViewed->candidate_id = $consultant->id;
                            $recentlyViewed->CLIENTID = Auth::user()->zoho_id;
                            $recentlyViewed->CANDIDATEID = $decrypted;
                            $recentlyViewed->contact_id = Auth::user()->contact_tbl_id;
                            $recentlyViewed->CONTACTID = Auth::user()->CONTACTID;
                            $recentlyViewed->created_at = date('Y-m-d H:i:s', time());
                            $recentlyViewed->updated_at = date('Y-m-d H:i:s', time());
                            $recentlyViewed->save();
                        }
                        $check = User::where('zoho_id', $decrypted)->where('profile_show', 1)->first();
                        if ($check != null) {
                            $data['db_sender_id'] = Auth::user()->contact_tbl_id;
                            $data['db_receiver_id'] = $consultant->id;
                            $data['sender_id'] = Auth::user()->CONTACTID;
                            $data['receiver_id'] = $consultant->CANDIDATEID;
                            $data['message'] = 'A client has recently viewed your profile';
                            $data['type'] = "profile_view";
                            $data['to'] = "Consultant";
                            $this->sendNotification($data);
                        }
                    }
                    $addTocompare = Addcompare::where([
                        'CLIENTID' => Auth::user()->zoho_id,
                        'contact_id' => Auth::user()->contact_tbl_id
                    ])->pluck('CANDIDATEID')->toArray();
                }
            }
            return view("consultant_detail", compact('consultant', 'wishlist', 'experience', 'addTocompare'));
        } else {
            Session::flash('error', 'This consultant account has been deactivated');
            if (Auth::check()) {
                if (Auth::user()->role_id != 3) {
                    return redirect('/' . Session::get('territory'));
                } else {
                    return redirect()->route('dashboard');
                }
            } else {
                return redirect('/' . Session::get('territory'));
            }
        }
    }

    /**
     *  consultant settings 
     * 
     */
    public function clientSettings(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            // $candidateDetailarray = $this->getConsultantProfile();
            $user = User::where('id', Auth::user()->id)->first();
            $id = Crypt::encryptString($user->id);
            $experienceList = $this->experienceList();
            $jobCategoryList = $this->jobCategoryList();
            $jobAlertsTags = $this->jobAlertsTags();
            $wishlist = [];
            if (Auth::check()) {
                $wishlist = Clientwishlist::where([
                    'CLIENTID' => Auth::user()->zoho_id,
                    'contact_id' => Auth::user()->contact_tbl_id
                ])->pluck('CANDIDATEID')->toArray();
            }

            return view('users::client_settings', compact('user', 'experienceList', 'jobCategoryList', 'id', 'jobAlertsTags', 'wishlist'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /*
     * 
     * Client Notificatios
     */

    public function clientNotifications(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            $wishlist = [];
            if (Auth::check()) {
                $wishlist = Clientwishlist::where([
                    'CLIENTID' => Auth::user()->zoho_id,
                    'contact_id' => Auth::user()->contact_tbl_id
                ])->pluck('CANDIDATEID')->toArray();
            }
            $unreadNotification = Notification::where('receiver_id', Auth::user()->CONTACTID)->whereNotNull('receiver_id')->where('to', 'Client')->where('is_read', 0)->get();
            /* Mark as read */
            if (count($unreadNotification)) {
                foreach ($unreadNotification as $key => $value) {
                    $value->is_read = 1;
                    $value->save();
                }
            }
            return view('users::client_notifications', compact('wishlist'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /**
     * Job Alerts tags
     */
    public function jobAlertsTags()
    {
        return [
            'All' => 'All',
            'Basis/S&A' => 'Basis/S&A',
            'Technical' => 'Technical',
            'Functional' => 'Functional',
            'Project Manager/Solution Architect' => 'Project Manager/Solution Architect'
        ];
    }

    /* Save to wishlist */

    public function addToWishList(Request $request)
    {

        $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $data['sender_id'] = Auth::user()->CONTACTID;
        $data['receiver_id'] = $request->consultant_id;
        $data['db_sender_id'] = Auth::user()->contact_tbl_id;
        $data['db_receiver_id'] = $request->db_id;
        $data['message'] = 'A client has added you in his wishlist';
        $data['type'] = "wishlist";
        $data['to'] = "Consultant";
        $response = $this->saveToWishlist($data);
        return response()->json([
            'status' => $response,
            'redirect_url' => route('client-home', ['tab' => 'wishlist'])
        ]);
    }

    public function saveToWishlist($requestData = [])
    {
        $isExists = Clientwishlist::where([
            'CLIENTID' => Auth::user()->zoho_id,
            'contact_id' => Auth::user()->contact_tbl_id,
            'candidate_id' => $requestData['db_receiver_id']
        ])->exists();
        if ($isExists) {
            return "fail";
        } else {
            $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
            if ($client != null) {
                $consultant = new Clientwishlist();
                $consultant->client_id = $client->id;
                $consultant->candidate_id = $requestData['db_receiver_id'];
                $consultant->CLIENTID = Auth::user()->zoho_id;
                $consultant->CANDIDATEID = $requestData['receiver_id'];
                $consultant->contact_id = Auth::user()->contact_tbl_id;
                $consultant->CONTACTID = Auth::user()->CONTACTID;
                $consultant->created_at = date('Y-m-d H:i:s', time());
                $consultant->updated_at = date('Y-m-d H:i:s', time());
                $consultant->save();
                /* Send Notification */
                $check = User::where('zoho_id', $requestData['receiver_id'])->where('job_wishlist', 1)->first();
                if ($check != null) {
                    $this->sendNotification($requestData);
                }
                return "success";
            } else {
                return "fail";
            }
        }
    }

    /*     * *Remove whish list  ajax */

    public function removeToWhishlist(Request $request)
    {
        $cosutantwhishlist = Clientwishlist::where('CANDIDATEID', $request->consultant_id)->where('CLIENTID', Auth::user()->zoho_id)->first();
        $response = 200;
        if (count($cosutantwhishlist)) {
            $cosutantwhishlist->delete();
        } else {
            $response = 400;
        }
        return response()->json([
            'status' => $response
        ]);
    }

    public function sendNotification($data = [])
    {
        $notification = new Notification();

        $notification->s_id = $data['db_sender_id'];
        $notification->sender_id = $data['sender_id'];
        if (isset($data['db_receiver_id'])) {
            $notification->r_id = $data['db_receiver_id'];
            $notification->receiver_id = $data['receiver_id'];
        }
        if (isset($data['redirect_url'])) {
            $notification->redirect_url = $data['redirect_url'];
        }
        $notification->message = $data['message'];
        $notification->type = $data['type'];
        $notification->to = $data['to'];
        $notification->job_id = isset($data['job_id']) ? $data['job_id'] : "";
        $notification->territory = (isset($data['territory']) ? $data['territory'] : "");
        $notification->created_at = date('Y-m-d H:i:s', time());
        $notification->updated_at = date('Y-m-d H:i:s', time());
        $notification->save();
        return;
    }

    /* Remove consultant from wishlist */

    public function deleteConsultantFromWishlist(Request $request)
    {
        Clientwishlist::where('id', $request->id)->delete();
        Session::flash('success', 'Consultant has been removed from your wishlist');
        return redirect()->route('client-home', ['tab' => 'wishlist']);
    }

    /* Remove consultant from Request profiles */

    public function deleteConsultantFromRequest(Request $request)
    {
        $profile = Candidateprofilerequests::where('id', $request->id)->first();
        if (count($profile)) {
            $recordIds = $profile->CUSTOMMODULE2_ID;
            Candidateprofilerequests::where('id', $request->id)->delete();

            $zohoQuery = new ZohoqueryController();

            $territory = "MY";
            if ($profile->territory != null) {
                $territory = $profile->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $zohoQuery->token = $result->token;

            $zohoQuery->deleteProfileRequest($recordIds);
            Session::flash('success', 'Consultant has been removed from your requested profile list');
            return redirect()->route('client-home', ['tab' => 'wishlist']);
        } else {
            Session::flash('error', 'Consultant has not been removed from your requested profile list');
            return redirect()->route('client-home', ['tab' => 'wishlist']);
        }
    }

    /*
     * Schedule interviews
     * 
     */

    public function scheduleInterview(Request $request)
    {
        $jobId = Crypt::decryptString($request->id);
        if (Auth::user()->role_id == 1) {
            if ($request->isMethod('POST')) { } else {
                $job = Jobopenings::where('id', $jobId)->first();
                $candidateIds = Jobassociatecandidates::where([
                    'status' => 'Associated',
                    'job_id' => $jobId
                ])->pluck('candidate_id')->toArray();
                $candidates = [];
                if (count($candidateIds)) {
                    $candidates = Candidates::select(DB::raw("CONCAT(first_name,' ',last_name) AS name"), 'id')->whereIn('id', $candidateIds)->where('status', 'Active')->where('is_delete', 0)->pluck('name', 'id')->toArray();
                }
                $interviewType = $this->interviewTypes();
                $interviewReminders = $this->interviewReminders();
                return view('users::client_schedule_interview', compact('candidates', 'job', 'interviewType', 'interviewReminders'));
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /* Interview types */

    public function interviewTypes()
    {
        return [
            "None" => "None",
            "Internal Interview" => "Internal Interview",
            "General Interview" => "General Interview",
            "Online Interview" => "Online Interview",
            "Level 2 Interview" => "Level 2 Interview",
            "Level 3 Interview" => "Level 3 Interview",
            "Level 4 Interview" => "Level 4 Interview"
        ];
    }

    /* Interview reminders */

    public function interviewReminders()
    {
        return [
            "None" => "None",
            "At time of event" => "At time of event",
            "5 minutes before" => "5 minutes before",
            "10 minutes before" => "10 minutes before",
            "15 minutes before" => "15 minutes before",
            "30 minutes before" => "30 minutes before",
            "1 hour before" => "1 hour before",
            "2 hours before" => "2 hours before",
            "1 day before" => "1 day before",
            "2 days before" => "2 days before"
        ];
    }

    /**
     * cancelled request profile
     */
    public function cancelCandidateProfile(Request $request)
    {
        $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $candidate = Candidates::where('id', $request->id)->first();
        if ($client != null && $candidate != null) {
            $profile = Candidateprofilerequests::where([
                'CONTACTID' => Auth::user()->CONTACTID,
                'CANDIDATEID' => $candidate->CANDIDATEID
            ])->orderBy('id', 'desc')->first();
            if ($profile != null) {
                $data['Request Status'] = "Fake request";
                $zohoQuery = new ZohoqueryController();

                $territory = "MY";
                if ($profile->territory != null) {
                    $territory = $profile->territory;
                }
                $result = Token::where('territory', $territory)->first();
                $zohoQuery->token = $result->token;

                //$zohoQuery->updateProfileRequest($profile->CUSTOMMODULE2_ID, $data);

                $profile->request_status = "Fake request";
                $profile->save();
                /* Send Notification to Admin */
                if ($client->territories == "PH") {
                    $setting = Settings::where('settingkey', "ph_email")->first();
                } else {
                    $setting = Settings::where('settingkey', "my_email")->first();
                }
                $data['db_sender_id'] = Auth::user()->contact_tbl_id;
                $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
                $data['sender_id'] = Auth::user()->CONTACTID;
                $data['receiver_id'] = "";
                $data['message'] = $client->client_name . ' has cancelled request the profile of candidate ' . $candidate->first_name . ' ' . $candidate->last_name . '(<a target="_blank" href="' . route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]) . '">' . $candidate->CANDIDATEID . '</a>)';
                $data['type'] = "request_profile";
                $data['to'] = "Admin";
                $data['territory'] = $client->territories;

                $this->sendNotification($data);
                Session::flash('success', 'Your request has been cancelled successfully.');
            } else {
                Session::flash('error', 'Your request could not find');
            }
        } else {
            Session::flash('error', 'Your request could not submit');
        }
        return redirect()->route('client-home', ['tab' => 'wishlist']);
    }

    public function cancelCandidateProfileAPI(Request $request)
    {
        $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $candidate = Candidates::where('id', $request->id)->first();
        if ($client != null && $candidate != null) {
            $profile = Candidateprofilerequests::where([
                'CONTACTID' => Auth::user()->CONTACTID,
                'CANDIDATEID' => $candidate->CANDIDATEID
            ])->orderBy('id', 'desc')->first();
            if ($profile != null) {
                $data['Request Status'] = "Fake request";
                $zohoQuery = new ZohoqueryController();

                $territory = "MY";
                if ($profile->territory != null) {
                    $territory = $profile->territory;
                }
                $result = Token::where('territory', $territory)->first();
                $zohoQuery->token = $result->token;

                //$zohoQuery->updateProfileRequest($profile->CUSTOMMODULE2_ID, $data);

                $profile->request_status = "Fake request";
                //$profile->save();
                $profile->delete();
                /* Send Notification to Admin */
                if ($client->territories == "PH") {
                    $setting = Settings::where('settingkey', "ph_email")->first();
                } else {
                    $setting = Settings::where('settingkey', "my_email")->first();
                }
                $data['db_sender_id'] = Auth::user()->contact_tbl_id;
                $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
                $data['sender_id'] = Auth::user()->CONTACTID;
                $data['receiver_id'] = "";
                $data['message'] = $client->client_name . ' has cancelled request the profile of candidate ' . $candidate->first_name . ' ' . $candidate->last_name . '(<a target="_blank" href="' . route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]) . '">' . $candidate->CANDIDATEID . '</a>)';
                $data['type'] = "request_profile";
                $data['to'] = "Admin";
                $data['territory'] = $client->territories;

                $this->sendNotification($data);
                return response(array(
                    'message' => 'Success',
                 ), 202);
            } else {
                return response(array(
                    'message' => 'Not Found',
                 ), 400);
            }
        } else {
            return response(array(
                'message' => 'Not Found',
             ), 400);
        }
    }

    /* Request for candidate profile */

    public function requestCandidateProfile(Request $request)
    {
        $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $candidate = Candidates::where('id', $request->id)->first();

        if ($client != null && $candidate != null) {
            $data = [];
            $data['Candidate ID'] = $candidate->candidate_id;
            $data['Link to Contact_ID'] = Auth::user()->CONTACTID;
            $data['Link to Candidate'] = $candidate->first_name . ' ' . $candidate->last_name;
            $data['Territory'] = $candidate->territory;
            $data['First Name'] = $candidate->first_name;
            $data['Last Name'] = $candidate->last_name;
            $data['Email'] = $candidate->email;
            $data['Telephone'] = $candidate->mobile_number;
            $data['Message'] = "For Profile Request";
            $data['Request Status'] = "New";

            $zohoQuery = new ZohoqueryController();

            $territory = "MY";
            if ($candidate->territory != null) {
                $territory = $candidate->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $zohoQuery->token = $result->token;

            $response = $zohoQuery->createProfileRequest($data);
            //if (!empty($response)) {
                //if (isset($response['response']['result']['recorddetail']['FL'])) {
                    //$CUSTOMMODULE2_ID = $response['response']['result']['recorddetail']['FL'][0]['content'];
                    //$result = $zohoQuery->getProfileRequestById($CUSTOMMODULE2_ID);
                    //if (!empty($result)) {
                        $requestProfile = new Candidateprofilerequests();
                        //$requestProfile->CUSTOMMODULE2_ID = $result[0]['CUSTOMMODULE2_ID'];
                        //$requestProfile->request_id = $result[0]['Request ID'];
                        $requestProfile->CONTACTID = Auth::user()->CONTACTID;
                        $requestProfile->CANDIDATEID = $candidate->CANDIDATEID;
                        $requestProfile->first_name = $candidate->first_name;
                        $requestProfile->last_name = $candidate->last_name;
                        $requestProfile->email = $candidate->email;
                        $requestProfile->created_time = date("Y-m-d h:i:s");
                        $requestProfile->modified_time = date("Y-m-d h:i:s");
                        $requestProfile->last_activity_time = date("Y-m-d h:i:s");
                        if ($candidate->territory == "MY") {
                            $requestProfile->currency = "MYR";
                        } else {
                            $requestProfile->currency = "PHP";
                        }
                        $requestProfile->exchange_rate = $candidate->base_rate;
                        $requestProfile->candidate_name = $candidate->first_name . ' ' . $candidate->last_name;
                        $requestProfile->contact_name = $client->client_name;
                        $requestProfile->request_status = "New";
                        $requestProfile->candidate_uid = $candidate->candidate_id;
                        $requestProfile->territory = $candidate->territory;
                        $requestProfile->is_synced = 0;
                        $requestProfile->created_at = date("Y-m-d h:i:s");
                        $requestProfile->updated_at = date("Y-m-d h:i:s");
                        $requestProfile->save();
                    //}
                //}
            // }
            /* Send Notification to Admin */
            if ($client->territories == "PH") {
                $setting = Settings::where('settingkey', "ph_email")->first();
            } else {
                $setting = Settings::where('settingkey', "my_email")->first();
            }
            $data['db_sender_id'] = Auth::user()->contact_tbl_id;
            $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
            $data['sender_id'] = Auth::user()->CONTACTID;
            $data['receiver_id'] = "";
            $data['message'] = $client->client_name . ' has requested access of ' . $candidate->first_name . ' ' . $candidate->last_name . 'View Profile here.(<a target="_blank" href="' . route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]) . '">' . $candidate->CANDIDATEID . '</a>)';
            $data['type'] = "request_profile";
            $data['to'] = "Admin";
            $data['territory'] = $client->territories;

            $this->sendNotification($data);
            $template = EmailTemplate::where('name', 'client_request_profile')->first();
            if ($template != null) {
                $emaildata['client_name'] = $client->client_name;
                $emaildata['email'] = $setting->settingvalue;
                $emaildata['candidate_name'] = $candidate->first_name . ' ' . $candidate->last_name;
                $emaildata['profile_link'] = route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]);
                $emaildata['candidate_id'] = $candidate->CANDIDATEID;
                $emaildata['subject'] = $client->client_name . " Requests Candidate Profile";
                Mail::send([], [], function ($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                        ->subject($emaildata['subject'])
                        ->setBody($template->parse($emaildata), 'text/html');
                });
            }
            Session::flash('success', 'Your request for profile access has been submitted to admin.  You will be notified once approved.');
        } else {
            Session::flash('error', 'Your request could not submit');
        }
        return redirect()->route('client-home', ['tab' => 'wishlist']);
    }

    public function requestCandidateProfileAPI(Request $request)
    {
        $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
        $candidate = Candidates::where('id', $request->id)->first();

        if ($client != null && $candidate != null) {
            $data = [];
            $data['Candidate ID'] = $candidate->candidate_id;
            $data['Link to Contact_ID'] = Auth::user()->CONTACTID;
            $data['Link to Candidate'] = $candidate->first_name . ' ' . $candidate->last_name;
            $data['Territory'] = $candidate->territory;
            $data['First Name'] = $candidate->first_name;
            $data['Last Name'] = $candidate->last_name;
            $data['Email'] = $candidate->email;
            $data['Telephone'] = $candidate->mobile_number;
            $data['Message'] = "For Profile Request";
            $data['Request Status'] = "New";

            $zohoQuery = new ZohoqueryController();

            $territory = "MY";
            if ($candidate->territory != null) {
                $territory = $candidate->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $zohoQuery->token = $result->token;

            //$response = $zohoQuery->createProfileRequest($data);
            //if (!empty($response)) {
                //if (isset($response['response']['result']['recorddetail']['FL'])) {
                    //$CUSTOMMODULE2_ID = $response['response']['result']['recorddetail']['FL'][0]['content'];
                    //$result = $zohoQuery->getProfileRequestById($CUSTOMMODULE2_ID);
                    //if (!empty($result)) {
                        $requestProfile = new Candidateprofilerequests();
                        //$requestProfile->CUSTOMMODULE2_ID = $result[0]['CUSTOMMODULE2_ID'];
                        //$requestProfile->request_id = $result[0]['Request ID'];
                        $requestProfile->CONTACTID = Auth::user()->CONTACTID;
                        $requestProfile->CANDIDATEID = $candidate->CANDIDATEID;
                        $requestProfile->first_name = $candidate->first_name;
                        $requestProfile->last_name = $candidate->last_name;
                        $requestProfile->email = $candidate->email;
                        $requestProfile->created_time = date("Y-m-d h:i:s");
                        $requestProfile->modified_time = date("Y-m-d h:i:s");
                        $requestProfile->last_activity_time = date("Y-m-d h:i:s");
                        if ($candidate->territory == "MY") {
                            $requestProfile->currency = "MYR";
                        } else {
                            $requestProfile->currency = "PHP";
                        }
                        $requestProfile->exchange_rate = $candidate->base_rate;
                        $requestProfile->candidate_name = $candidate->first_name . ' ' . $candidate->last_name;
                        $requestProfile->contact_name = $client->client_name;
                        $requestProfile->request_status = "New";
                        $requestProfile->candidate_uid = $candidate->candidate_id;
                        $requestProfile->territory = $candidate->territory;
                        $requestProfile->is_synced = 0;
                        $requestProfile->created_at = date("Y-m-d h:i:s");
                        $requestProfile->updated_at = date("Y-m-d h:i:s");
                        $requestProfile->save();
                    //}
                //}
            // }
            /* Send Notification to Admin */
            if ($client->territories == "PH") {
                $setting = Settings::where('settingkey', "ph_email")->first();
            } else {
                $setting = Settings::where('settingkey', "my_email")->first();
            }
            $data['db_sender_id'] = Auth::user()->contact_tbl_id;
            $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
            $data['sender_id'] = Auth::user()->CONTACTID;
            $data['receiver_id'] = "";
            $data['message'] = $client->client_name . ' has requested access of ' . $candidate->first_name . ' ' . $candidate->last_name . 'View Profile here.(<a target="_blank" href="' . route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]) . '">' . $candidate->CANDIDATEID . '</a>)';
            $data['type'] = "request_profile";
            $data['to'] = "Admin";
            $data['territory'] = $client->territories;

            $this->sendNotification($data);
            $template = EmailTemplate::where('name', 'client_request_profile')->first();
            if ($template != null) {
                $emaildata['client_name'] = $client->client_name;
                $emaildata['email'] = $setting->settingvalue;
                $emaildata['candidate_name'] = $candidate->first_name . ' ' . $candidate->last_name;
                $emaildata['profile_link'] = route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]);
                $emaildata['candidate_id'] = $candidate->CANDIDATEID;
                $emaildata['subject'] = $client->client_name . " Requests Candidate Profile";
                Mail::send([], [], function ($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                        ->subject($emaildata['subject'])
                        ->setBody($template->parse($emaildata), 'text/html');
                });
            }
            return response(array(
                'message' => 'Success',
             ), 202);
        } else {
            return response(array(
                'message' => 'Not Found',
             ), 410);
        }
        //return redirect()->route('client-home', ['tab' => 'wishlist']);
    }

    // Download attchemnt
    public function downloadfile()
    {
        // download file
        $url = 'https://recruit.zoho.com/recruit/private/xml/Candidates/downloadFile?authtoken=' . $this->token . '&scope=recruitapi&version=2&id=401682000000171016';
        // Initialise a cURL handle
        $ch = curl_init();
        // Set any other cURL options that are required
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        curl_close($ch);      // Closing the cURL handle
        // write file
        file_put_contents(public_path("/cv") . "/" . "test.pdf", $response, FILE_APPEND);
        echo "<pre>";
        print_r($response);
        exit;
    }

    public function getcandidatemissingdata()
    {
        // download file paste this url in browser to download xml file
        $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/getTabularRecords?authtoken=e21643e62b9d0f2d98e8e50db83e0f12&scope=recruitapi&id=401682000002785194&tabularNames=(Experience Details, Educational Details, Contract History, References)';
        // Initialise a cURL handle
        $url = str_replace(" ", '%20', $url);
        $ch = curl_init();
        // Set any other cURL options that are required
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request
        $response = json_decode($response, true);
        curl_close($ch);      // Closing the cURL handle

        echo "<pre>";
        print_r($response);
        exit;
    }

    public function downloadprofileimage()
    {
        // download file paste this url in browser to download xml file
        $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/downloadPhoto?authtoken=e21643e62b9d0f2d98e8e50db83e0f12&scope=recruitapi&id=401682000002785194&version=2';
        // Initialise a cURL handle
        $ch = curl_init();
        // Set any other cURL options that are required
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);  // Execute a cURL request        
        curl_close($ch);      // Closing the cURL handle
        // write file
        if ($this->isJSON($response)) {
            echo "It's JSON";
        } else {
            echo "Donwload";
            file_put_contents(public_path("/cv") . "/" . "test.png", $response, FILE_APPEND);
        }
    }

    public function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    public function aboutUs()
    {
        return view('users::about_us');
    }

    public function contactus()
    {
        return view('users::contact_us');
    }

    public function contactUsThankyou()
    {
        return view('users::contactus_thnakyou');
    }

    public function howItWorks()
    {
        return view('users::how_it_works');
    }

    public function atapVirtualOffice()
    {
        if (Session::get('territory') == "MY") {
            return view('users::the_atap_virtual_office');
        } else {
            return redirect('/' . Session::get('territory'));
        }
    }

    public function termsConditions()
    {
        return view('users::terms_conditions');
    }

    public function privacyPolicy()
    {
        return view('users::privacy_policy');
    }

    public function clientThankyou()
    {
        return view('users::thankyou');
    }

    public function consultantThankyou()
    {
        return view('users::thankyou');
    }

    /**
     * Get Search consultant auto population 
     * 
     */
    public function getSearchConsultant(Request $request)
    {
        $searchterm = $request->search;
        $query = Consultant::where([
            'is_delete' => 0,
            'status' => 'Active',
            'available_for_contract' => 0,
            'block' => 0,
            'internal_hire' => 0,
            'publish_in_us' => 1,
            'territory' => Session::get('territory')
        ])->whereNotIn('candidate_status', ['Unqualified', 'Qualified Expat', 'Unqualified Expat']);
        $query->where(function ($query) use ($searchterm) {
            $query->where('sap_job_title', 'LIKE', "%" . $searchterm . "%");
            // $query->orWhere('skill_set', 'LIKE', "%" . $searchterm . "%");
            // $query->orWhere('category', 'LIKE', "%" . $searchterm . "%");
            // $query->orWhere('candidate_id', 'LIKE', "%" . $searchterm . "%");
        });
        $getjobTitle = $query->groupBy('sap_job_title')->take(5)->pluck('sap_job_title')->toArray();
        $output = '';
        $response = array();
        if (count($getjobTitle)) {
            foreach ($getjobTitle as $row) {
                $response[] = $row;
            }
            $status = 200;
        } else {
            $status = 400;
        }
        $response = json_encode($response);
        return response()->json([
            'output' => $response,
            'status' => $status
        ]);
    }

    /**
     * Get Search Job auto population 
     * 
     */
    public function getSearchjobs(Request $request)
    {
        $searchterm = $request->search;


        $query = Jobopenings::where([
            'job_opening_status' => "In-progress",
            'publish_in_us' => 1,
            'block' => 0,
            'is_delete' => 0,
            'territory' => Session::get('territory')
        ]);
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }
        $query->where(function ($query) use ($searchterm) {
            $query->where('posting_title', 'LIKE', "%" . $searchterm . "%");
            // $query->orWhere('Job_ID', 'LIKE', "%" . $searchterm . "%");
            // $query->orWhere('key_skills', 'LIKE', "%" . $searchterm . "%");
        });
        $getjobTitle = $query->groupBy('posting_title')->take(5)->pluck('posting_title')->toArray();

        $response = array();
        if (count($getjobTitle)) {
            foreach ($getjobTitle as $row) {
                $response[] = $row;
            }
            $status = 200;
        } else {
            $status = 400;
        }
        $response = json_encode($response);
        return response()->json([
            'output' => $response,
            'status' => $status
        ]);
    }

    /*
     * Getting Interview Notes
     * 
     */

    public function getInterviewNotes(Request $request)
    {

        $client = new Client($this->token);
        $interviewId = $request->interview_id;
        $containerId = $request->continer_id;
        $notes = $client->getRelatedRecords(
            'Notes',
            "Interview",
            $request->interview_id
        );
        $status = 200;
        if (empty($notes)) {
            $status = 400;
        }
        $html = view('interview-notes', compact('notes', 'interviewId', 'containerId'))->render();
        return response()->json([
            'html' => $html,
            'status' => $status
        ]);
    }

    /*
     * Read Notification and Remove Counter From Bell
     * 
     */

    public function readNotification(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            $userID = Auth::user()->CONTACTID;
        } else {
            $userID = Auth::user()->zoho_id;
        }
        $unreadNotification = Notification::where('receiver_id', $userID)->where('is_read', 0)->get();
        /* Mark as read */
        if (count($unreadNotification)) {
            foreach ($unreadNotification as $key => $value) {
                $value->is_read = 1;
                $value->save();
            }
        }
        return;
    }

    /**
     * Add to Compare
     * 
     */
    public function addCompare(Request $request)
    {
        $client = Addcompare::where('CLIENTID', Auth::user()->zoho_id)->first();
        // $data['sender_id'] = Auth::user()->CONTACTID;
        $data['receiver_id'] = $request->consultant_id;
        // $data['db_sender_id'] = Auth::user()->contact_tbl_id;
        $data['db_receiver_id'] = $request->db_id;
        $isExists = Addcompare::where([
            'CLIENTID' => Auth::user()->zoho_id,
            'contact_id' => Auth::user()->contact_tbl_id,
            'candidate_id' => $data['db_receiver_id']
        ])->exists();
        if ($isExists) {
            // return "fail";
            $status = 400;
        } else {
            $client = Clients::where('CLIENTID', Auth::user()->zoho_id)->first();
            // echo Auth::user()->zoho_id;
            // print_r(Auth::user());
            // print_r($client->toArray());
            // exit;
            $comapreCount = 0;
            if ($client != null) {
                $maxQty = Addcompare::where([
                    'CLIENTID' => Auth::user()->zoho_id,
                    'contact_id' => Auth::user()->contact_tbl_id
                ])->get();
                if (count($maxQty) < 3) {
                    $addcompare = new Addcompare();
                    $addcompare->client_id = $client->id;
                    $addcompare->candidate_id = $data['db_receiver_id'];
                    $addcompare->CLIENTID = Auth::user()->zoho_id;
                    $addcompare->CANDIDATEID = $data['receiver_id'];
                    $addcompare->contact_id = Auth::user()->contact_tbl_id;
                    $addcompare->CONTACTID = Auth::user()->CONTACTID;
                    $addcompare->created_at = date('Y-m-d H:i:s', time());
                    $addcompare->updated_at = date('Y-m-d H:i:s', time());
                    $addcompare->save();
                    $comapreCount = Addcompare::where([
                        'CLIENTID' => Auth::user()->zoho_id,
                        'contact_id' => Auth::user()->contact_tbl_id
                    ])->count();
                    $status = 200;
                } else {
                    $status = 400;
                }
            } else {
                $status = 400;
            }
            return response()->json([
                'status' => $status,
                'counter' => $comapreCount,
                'redirect_url' => route('addComparepage')
            ]);
        }
    }

    /*     * *
     * Add to Compare page 
     * 
     */

    public function addToCompare(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            // $candidateDetailarray = $this->getConsultantProfile();
            $wishlist = [];
            if (Auth::check()) {
                $wishlist = Clientwishlist::where([
                    'CLIENTID' => Auth::user()->zoho_id,
                    'contact_id' => Auth::user()->contact_tbl_id
                ])->pluck('CANDIDATEID')->toArray();
            }
            $addTocompare = Addcompare::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id,
            ])->get();
            return view('users::add_to_compare', compact('addTocompare', 'wishlist'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect('/' . Session::get('territory'));
        }
    }

    /**
     * 
     * Remove add to compare consultant 
     */
    public function removeToCompare(Request $request)
    {
        $cosutantcomapre = Addcompare::where('CANDIDATEID', $request->id)->where('CLIENTID', Auth::user()->zoho_id)->first();
        $response = 200;
        $comapreCount = 0;
        if (count($cosutantcomapre)) {
            $cosutantcomapre->delete();
            $comapreCount = Addcompare::where([
                'CLIENTID' => Auth::user()->zoho_id,
                'contact_id' => Auth::user()->contact_tbl_id
            ])->count();
        } else {
            $response = 400;
        }
        return response()->json([
            'status' => $response,
            'counter' => $comapreCount
        ]);
    }

    /*
     * FAQ
     * 
     */

    public function frontFaq(Request $request)
    {
        // $faqs = Faq::with(['faqcategory' => function ($query)  {
        //     $query->where('name', 'Consultants');
        // }])->whereIn('territory',[Session::get('territory'),'BOTH'])->get();
        // $faqs = Faq::with('faqcategory' )->whereIn('territory',[Session::get('territory'),'BOTH'])->get();
        // echo '<pre>';
        // print_r($faqs->toArray());
        // exit;
        $consultant = Category::with(['faq' => function ($query) {
            $query->whereIn('territory', [Session::get('territory'), 'BOTH']);
        }])->where('name', 'Consultants')->first();
        $priceConsultant = Category::with(['faq' => function ($query) {
            $query->whereIn('territory', [Session::get('territory'), 'BOTH']);
        }])->where('name', 'Pricing Related (Consultant)')->first();
        $client = Category::with(['faq' => function ($query) {
            $query->whereIn('territory', [Session::get('territory'), 'BOTH']);
        }])->where('name', 'Clients')->first();
        $priceClient = Category::with(['faq' => function ($query) {
            $query->whereIn('territory', [Session::get('territory'), 'BOTH']);
        }])->where('name', 'Pricing Related (Client)')->first();

        $categories = Category::pluck('id', 'name')->toArray();
        return view('frontfaqs', compact('consultant', 'priceConsultant', 'client', 'priceClient'));
    }

    /**
     * Send Conatct us mail
     */
    /*
     * Send Registration Email
     * 
     */

    public function contactUsForm(Request $request)
    {
        $template = EmailTemplate::where('name', 'contact_us')->first();
        $settingEmail = '';
        if (Session::get('territory') == "PH") {
            $settingEmail = Settings::where('settingkey', "ph_email")->first()->settingvalue;
        } else {
            $settingEmail = Settings::where('settingkey', "my_email")->first()->settingvalue;
        }
        if ($template != null && $settingEmail != null) {
            $emaildata['first_name'] = $request->first_name;
            $emaildata['last_name'] = $request->last_name;
            $emaildata['email'] = $request->email;
            $emaildata['phone'] = $request->phone;
            $emaildata['user_type'] = $request->user_type;
            $emaildata['subject'] = $request->subject;
            $emaildata['message'] = $request->message;
            $emaildata['adminEmail'] = $settingEmail;

            Mail::send([], [], function ($messages) use ($template, $emaildata) {
                $messages->to($emaildata['adminEmail'])
                    ->subject($template->subject)
                    ->setBody($template->parse($emaildata), 'text/html');
            });
            Session::flash('sucesss', 'Your Email sent succesfully.');
            return redirect()->route('contactusThankyou');
        } else {
            Session::flash('error', 'something went wrong please try after some times.');
            return redirect()->route('contactus');
        }
    }

    public function uploadProfilePic(Request $request)
    {
        if ($request->file('file') != null) {
            $user = User::where('id', Auth::user()->id)->first();
            $filename = time() . '-' . $request->file('file')->getClientOriginalName();
            $path = public_path('profile_image/' . $filename);
            Image::make($request->file('file')->getRealPath())->save($path);
            $user->profile_pic = $filename;
            $this->uploadProfilePicInZoho($user->CONTACTID, $path);
            $user->save();
        }
        return;
    }



    // filter realtime
    public function searchGeneralJobBackup($request)
    {
        $search_type = $request->search_type;
        $jobsearchterm = $job_type = $job_type2 = $job_baserate = $job_baserate_from = $job_baserate_to = $job_baserate2_from = $job_baserate2_to = $work_experience = $work_experience2 = $job_start_date = $job_start_date2 = $job_duration = $job_duration2 = $job_industry = $job_industry2 = "";
        $jobcategory = [];
        $jobcategory2 = [];
        $job_mode1 = [];
        $job_mode2 = [];
        $job_state = [];
        $job_state2 = [];
        $query = Jobopenings::with(['client', 'contact', 'jobopeningcategory'])
            ->whereHas('contact', function ($query) {
                $query->where('is_delete', 0);
            });
        /* Category */
        if ((isset($request->jobcategory) && !empty($request->jobcategory)) || (isset($request->job_category2) && !empty($request->job_category2))) {
            $jobcategory = $request->jobcategory;
            $jobcategory2 = $request->job_category2;
            if (!empty($jobcategory2)) {
                // $query = $query->whereIn('category', $request->job_category2);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory2) {
                    $query = $query->whereIn('category', $jobcategory2);
                });
            } else {
                $jobcategory2 = $jobcategory;
                // $query = $query->whereIn('category', $request->jobcategory);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory) {
                    $query = $query->whereIn('category', $jobcategory);
                });
            }
        }
        if (isset($request->jobsearchterm) && $request->jobsearchterm != "") {
            $jobsearchterm = $request->jobsearchterm;
            $query = $query->where(function ($query2) use ($jobsearchterm) {
                $query2->where('Job_ID', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('posting_title', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('key_skills', 'LIKE', "%" . $jobsearchterm . "%");
            });
        }
        /* Job Mode */
        if ((isset($request->job_mode1) && !empty($request->job_mode1)) || (isset($request->job_mode2) && !empty($request->job_mode2))) {
            $job_mode1 = $request->job_mode1;
            $job_mode2 = $request->job_mode2;
            if (!empty($job_mode2)) {
                $query = $query->whereIn('job_mode', $job_mode2);
            } else {
                $job_mode2 = $job_mode1;
                $query = $query->whereIn('job_mode', $job_mode1);
            }
        }
        /* Job Type */
        if ((isset($request->job_type) && $request->job_type != "") || isset($request->job_type2) && $request->job_type2 != "") {
            $job_type = $request->job_type;
            $job_type2 = $request->job_type2;
            if ($job_type2 != null) {
                $query = $query->whereIn('job_type', $request->job_type2);
            } else {
                $job_type2 = $job_type;
                $query = $query->whereIn('job_type', $request->job_type);
            }
        }
        /* Job Base Rate */
        if ((isset($request->job_baserate_from) || isset($request->job_baserate_to)) || (isset($request->job_baserate2_from) || isset($request->job_baserate2_to))) {
            $job_baserate_from = $request->job_baserate_from;
            $job_baserate_to = $request->job_baserate_to;
            $job_baserate2_from = $request->job_baserate2_from;
            $job_baserate2_to = $request->job_baserate2_to;

            if ($job_baserate2_from != null && $job_baserate2_to != null) {
                $query = $query->whereBetween('job_base_rate', [$job_baserate2_from, $job_baserate2_to]);
            } else if ($job_baserate_from != null && $job_baserate_to != null) {
                $job_baserate2_from = $job_baserate_from;
                $job_baserate2_to = $job_baserate_to;
                $query = $query->whereBetween('job_base_rate', [$job_baserate_from, $job_baserate_to]);
            } else if ($job_baserate2_from != null) {
                $query = $query->where('job_base_rate', '>=', $job_baserate2_from);
            } else if ($job_baserate2_to != null) {
                $query = $query->where('job_base_rate', '<=', $job_baserate2_to);
            } else if ($job_baserate_from != null) {
                $job_baserate2_from = $job_baserate_from;
                $query = $query->where('job_base_rate', '>=', $job_baserate_from);
            } else if ($job_baserate_to != null) {
                $job_baserate2_to = $job_baserate_to;
                $query = $query->where('job_base_rate', '<=', $job_baserate_to);
            }
        }

        /* Work Experience */
        if ((isset($request->work_experience) && $request->work_experience != "") || isset($request->work_experience2)) {
            $work_experience = $request->work_experience;
            $work_experience2 = $request->work_experience2;
            if ($work_experience2 != null) {
                if ($work_experience2 == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience2 == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience2 == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience2 == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            } else {
                $work_experience2 = $work_experience;
                if ($work_experience == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            }
        }
        /* Start Date */
        if ((isset($request->job_start_date) && $request->job_start_date != "") || isset($request->job_start_date2)) {
            $job_start_date = $request->job_start_date;
            $job_start_date2 = $request->job_start_date2;
            if ($job_start_date2 != null) {
                if ($job_start_date2 == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date2 == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            } else {
                $job_start_date2 = $job_start_date;
                if ($job_start_date == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            }
        }
        /* Duration */
        if ((isset($request->job_duration) && $request->job_duration != "") || isset($request->job_duration2)) {
            $job_duration = $request->job_duration;
            $job_duration2 = $request->job_duration2;
            if ($job_duration2 != null) {
                if ($job_duration2 == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration2 == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration2 == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            } else {
                $job_duration2 = $job_duration;
                if ($job_duration == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            }
        }
        /* Industry */
        if ((isset($request->job_industry) && $request->job_industry != "") || isset($request->job_industry2)) {
            $job_industry = $request->job_industry;
            $job_industry2 = $request->job_industry2;
            if ($job_industry2 != null) {
                $query = $query->where('industry', $job_industry2);
            } else {
                $job_industry2 = $job_industry;
                $query = $query->where('industry', $job_industry);
            }
        }
        /* State */
        if ((isset($request->job_state) && !empty($request->job_state)) || (isset($request->job_state2) && !empty($request->job_state2))) {
            $job_state = $request->job_state;
            $job_state2 = $request->job_state2;
            if (!empty($job_state2)) {
                $query = $query->whereIn('state', $job_state2);
            } else {
                $job_state2 = $job_state;
                $query = $query->whereIn('state', $job_state);
            }
        }
        /* Teritorries */
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }
        $jobs = $query->where([
            'job_opening_status' => "In-progress",
            'publish_in_us' => 1,
            'block' => 0,
            'is_delete' => 0
        ])->get();

       

        // $jobs->appends([
        //     'search_type' => $search_type,
        //     'jobcategory' => $jobcategory,
        //     'jobcategory2' => $jobcategory2,
        //     'jobsearchterm' => $jobsearchterm,
        //     'job_type' => $job_type,
        //     'job_type2' => $job_type2,
        //     'job_baserate_from' => $job_baserate_from,
        //     'job_baserate_to' => $job_baserate_to,
        //     'job_baserate2_from' => $job_baserate2_from,
        //     'job_baserate2_to' => $job_baserate2_to,
        //     'work_experience' => $work_experience,
        //     'work_experience2' => $work_experience2,
        //     'job_start_date' => $job_start_date,
        //     'job_start_date2' => $job_start_date2,
        //     'job_mode1' => $job_mode1,
        //     'job_mode2' => $job_mode2,
        //     'job_duration' => $job_duration,
        //     'job_duration2' => $job_duration2,
        //     'job_industry' => $job_industry,
        //     'job_industry2' => $job_industry2,
        //     'job_state' => $job_state,
        //     'job_state2' => $job_state2,
            
        // ])->render();

        $displayJobMightLike = 1;
        if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
                $displayJobMightLike = 0;
            }
            $jobsMightLike = [];
            if (Auth::user()->role_id == 2) {
                /* Jobs you might Like */
                $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                $categoryArray[] = "";
                $categoryArray = explode(';', $candidateDetailarray->category);
                $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
                $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
            }
        } else {
            $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
        }
        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];
        return ($jobs);
        // return view('general-search-job', compact('displayJobMightLike', 'jobsMightLike', 'jobs', 'search_type', 'jobcategory', 'jobcategory2', 'jobsearchterm', 'job_mode1', 'job_mode2', 'job_type', 'job_type2', 'job_baserate_from', 'job_baserate_to', 'job_baserate2_from', 'job_baserate2_to', 'work_experience', 'work_experience2', 'job_start_date', 'job_start_date2', 'job_duration', 'job_duration2', 'job_industry', 'job_industry2', 'job_state', 'job_state2', 'candidateScreeningStatus', 'total'));
    }
    // end realtime

    public function generalSerchView($request)
    {
        // $search_type = $request->search_type;
        $jobsearchterm = $job_type = $job_type2 = $job_baserate = $job_baserate_from = $job_baserate_to = $job_baserate2_from = $job_baserate2_to = $work_experience = $work_experience2 = $job_start_date = $job_start_date2 = $job_duration = $job_duration2 = $job_industry = $job_industry2 = "";
        $jobcategory = [];
        $jobcategory2 = [];
        $job_mode1 = [];
        $job_mode2 = [];
        $job_state = [];
        $job_state2 = [];
        $query = Jobopenings::with(['client', 'contact', 'jobopeningcategory'])
            ->whereHas('contact', function ($query) {
                $query->where('is_delete', 0);
            });
        /* Category */
        if ((isset($request->jobcategory) && !empty($request->jobcategory)) || (isset($request->job_category2) && !empty($request->job_category2))) {
            $jobcategory = $request->jobcategory;
            $jobcategory2 = $request->job_category2;
            if (!empty($jobcategory2)) {
                // $query = $query->whereIn('category', $request->job_category2);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory2) {
                    $query = $query->whereIn('category', $jobcategory2);
                });
            } else {
                $jobcategory2 = $jobcategory;
                // $query = $query->whereIn('category', $request->jobcategory);
                $query = $query->whereNotNull('category');
                $query = $query->whereHas('jobopeningcategory', function ($query) use ($jobcategory) {
                    $query = $query->whereIn('category', $jobcategory);
                });
            }
        }
        if (isset($request->jobsearchterm) && $request->jobsearchterm != "") {
            $jobsearchterm = $request->jobsearchterm;
            $query = $query->where(function ($query2) use ($jobsearchterm) {
                $query2->where('Job_ID', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('posting_title', 'LIKE', "%" . $jobsearchterm . "%");
                $query2->orWhere('key_skills', 'LIKE', "%" . $jobsearchterm . "%");
            });
        }
        /* Job Mode */
        if ((isset($request->job_mode1) && !empty($request->job_mode1)) || (isset($request->job_mode2) && !empty($request->job_mode2))) {
            $job_mode1 = $request->job_mode1;
            $job_mode2 = $request->job_mode2;
            if (!empty($job_mode2)) {
                $query = $query->whereIn('job_mode', $job_mode2);
            } else {
                $job_mode2 = $job_mode1;
                $query = $query->whereIn('job_mode', $job_mode1);
            }
        }
        /* Job Type */
        if ((isset($request->job_type) && $request->job_type != "") || isset($request->job_type2) && $request->job_type2 != "") {
            $job_type = $request->job_type;
            $job_type2 = $request->job_type2;
            if ($job_type2 != null) {
                $query = $query->whereIn('job_type', $request->job_type2);
            } else {
                $job_type2 = $job_type;
                $query = $query->whereIn('job_type', $request->job_type);
            }
        }
        /* Job Base Rate */
        if ((isset($request->job_baserate_from) || isset($request->job_baserate_to)) || (isset($request->job_baserate2_from) || isset($request->job_baserate2_to))) {
            $job_baserate_from = $request->job_baserate_from;
            $job_baserate_to = $request->job_baserate_to;
            $job_baserate2_from = $request->job_baserate2_from;
            $job_baserate2_to = $request->job_baserate2_to;

            if ($job_baserate2_from != null && $job_baserate2_to != null) {
                $query = $query->whereBetween('job_base_rate', [$job_baserate2_from, $job_baserate2_to]);
            } else if ($job_baserate_from != null && $job_baserate_to != null) {
                $job_baserate2_from = $job_baserate_from;
                $job_baserate2_to = $job_baserate_to;
                $query = $query->whereBetween('job_base_rate', [$job_baserate_from, $job_baserate_to]);
            } else if ($job_baserate2_from != null) {
                $query = $query->where('job_base_rate', '>=', $job_baserate2_from);
            } else if ($job_baserate2_to != null) {
                $query = $query->where('job_base_rate', '<=', $job_baserate2_to);
            } else if ($job_baserate_from != null) {
                $job_baserate2_from = $job_baserate_from;
                $query = $query->where('job_base_rate', '>=', $job_baserate_from);
            } else if ($job_baserate_to != null) {
                $job_baserate2_to = $job_baserate_to;
                $query = $query->where('job_base_rate', '<=', $job_baserate_to);
            }
        }

        /* Work Experience */
        if ((isset($request->work_experience) && $request->work_experience != "") || isset($request->work_experience2)) {
            $work_experience = $request->work_experience;
            $work_experience2 = $request->work_experience2;
            if ($work_experience2 != null) {
                if ($work_experience2 == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience2 == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience2 == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience2 == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            } else {
                $work_experience2 = $work_experience;
                if ($work_experience == '0') {
                    $query = $query->whereNotNull('work_experience');
                    $query = $query->where('work_experience', 'Fresh');
                } else if ($work_experience == '3') {
                    $query = $query->where('work_experience', '1-3 years');
                } else if ($work_experience == '5') {
                    $query = $query->where('work_experience', '4-5 years');
                } else if ($work_experience == '10') {
                    $query = $query->where('work_experience', '5-10 years');
                } else {
                    $query = $query->where('work_experience', '10+ years');
                }
            }
        }
        /* Start Date */
        if ((isset($request->job_start_date) && $request->job_start_date != "") || isset($request->job_start_date2)) {
            $job_start_date = $request->job_start_date;
            $job_start_date2 = $request->job_start_date2;
            if ($job_start_date2 != null) {
                if ($job_start_date2 == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date2 == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            } else {
                $job_start_date2 = $job_start_date;
                if ($job_start_date == "today") {
                    $query = $query->where('target_date', date('Y-m-d'));
                } else if ($job_start_date == "week") {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+7 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                } else {
                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d', strtotime('+30 day'));
                    $query = $query->whereNotNull('target_date')->whereBetween('target_date', [$startDate, $endDate]);
                }
            }
        }
        /* Duration */
        if ((isset($request->job_duration) && $request->job_duration != "") || isset($request->job_duration2)) {
            $job_duration = $request->job_duration;
            $job_duration2 = $request->job_duration2;
            if ($job_duration2 != null) {
                if ($job_duration2 == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration2 == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration2 == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            } else {
                $job_duration2 = $job_duration;
                if ($job_duration == "3") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [1, 3]);
                } else if ($job_duration == "6") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [4, 6]);
                } else if ($job_duration == "9") {
                    $query = $query->whereNotNull('job_duration')->whereBetween('job_duration', [6, 12]);
                } else {
                    $query = $query->whereNotNull('job_duration')->where('job_duration', '>', 12);
                }
            }
        }
        /* Industry */
        if ((isset($request->job_industry) && $request->job_industry != "") || isset($request->job_industry2)) {
            $job_industry = $request->job_industry;
            $job_industry2 = $request->job_industry2;
            if ($job_industry2 != null) {
                $query = $query->where('industry', $job_industry2);
            } else {
                $job_industry2 = $job_industry;
                $query = $query->where('industry', $job_industry);
            }
        }
        /* State */
        if ((isset($request->job_state) && !empty($request->job_state)) || (isset($request->job_state2) && !empty($request->job_state2))) {
            $job_state = $request->job_state;
            $job_state2 = $request->job_state2;
            if (!empty($job_state2)) {
                $query = $query->whereIn('state', $job_state2);
            } else {
                $job_state2 = $job_state;
                $query = $query->whereIn('state', $job_state);
            }
        }
        /* Teritorries */
        if (Session::get('territory') != null) {
            $query = $query->where('territory', Session::get('territory'));
        }
        $jobs = $query->where([
            'job_opening_status' => "In-progress",
            'publish_in_us' => 1,
            'block' => 0,
            'is_delete' => 0
        ])->get();

       

        // $jobs->appends([
        //     'search_type' => $search_type,
        //     'jobcategory' => $jobcategory,
        //     'jobcategory2' => $jobcategory2,
        //     'jobsearchterm' => $jobsearchterm,
        //     'job_type' => $job_type,
        //     'job_type2' => $job_type2,
        //     'job_baserate_from' => $job_baserate_from,
        //     'job_baserate_to' => $job_baserate_to,
        //     'job_baserate2_from' => $job_baserate2_from,
        //     'job_baserate2_to' => $job_baserate2_to,
        //     'work_experience' => $work_experience,
        //     'work_experience2' => $work_experience2,
        //     'job_start_date' => $job_start_date,
        //     'job_start_date2' => $job_start_date2,
        //     'job_mode1' => $job_mode1,
        //     'job_mode2' => $job_mode2,
        //     'job_duration' => $job_duration,
        //     'job_duration2' => $job_duration2,
        //     'job_industry' => $job_industry,
        //     'job_industry2' => $job_industry2,
        //     'job_state' => $job_state,
        //     'job_state2' => $job_state2,
            
        // ])->render();

        $displayJobMightLike = 1;
        if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
                $displayJobMightLike = 0;
            }
            $jobsMightLike = [];
            if (Auth::user()->role_id == 2) {
                /* Jobs you might Like */
                $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                $categoryArray[] = "";
                $categoryArray = explode(';', $candidateDetailarray->category);
                $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
                $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
            }
        } else {
            $jobsMightLike = Jobopenings::where('job_opening_status', 'In-progress')->orderBy('updated_at', 'desc')->take(3)->get();
        }
        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];
        //return ($jobs);
        return view('general-search-job', compact('displayJobMightLike', 'jobsMightLike', 'jobs', 'search_type', 'jobcategory', 'jobcategory2', 'jobsearchterm', 'job_mode1', 'job_mode2', 'job_type', 'job_type2', 'job_baserate_from', 'job_baserate_to', 'job_baserate2_from', 'job_baserate2_to', 'work_experience', 'work_experience2', 'job_start_date', 'job_start_date2', 'job_duration', 'job_duration2', 'job_industry', 'job_industry2', 'job_state', 'job_state2', 'candidateScreeningStatus', 'total'));
    }
}