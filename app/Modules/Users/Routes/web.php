<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your module. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
Route::middleware(['DefaultLocale'])->prefix('{locale}')->group(function () {
    // Registration
    Route::any('consultant/', 'UsersController@consultantRegistration')->name('consultant-registration');
    Route::any('consultant/registration', 'UsersController@consultantNewRegistration')->name('new-consultant-registration');//create consultant registration
    Route::any('client/1', 'UsersController@clientRegistration')->name('client-registration');
    Route::any('client/', 'UsersController@clientRegistration')->name('client-reg');
    Route::any('client/registration', 'UsersController@clientNewRegistration')->name('new-client-registration'); //create new route register for client
// Check for email exists
    Route::post('user/emailexists', 'UsersController@emailExists')->name('email-exists');
// Generate Password
    Route::any('user/generate-password/{id}', 'UsersController@generatePassword')->name('generate-password');
// Login
    Route::any('user/login', 'UsersController@login')->name('login');
// Forgot Passowrd
    Route::any('user/forgotpassword', 'UsersController@forgotPassword')->name('forgotpassword');
// Rest Password from
    Route::get('user/reseturl/{id}', 'UsersController@getResetPassword')->name('getresetpassword');
// post Rest Password from
    Route::post('user/reseturl/{id}', 'UsersController@postResetPassword')->name('postresetpassword');
//change consultant and client password
    Route::post('user/changepassword', 'UsersController@postChangePassword')->name('changepassword');
//Delete Account
    Route::post('user/deleteaccount', 'UsersController@postDeleteAccount')->name('deleteaccount');
//Notifications Account
    Route::post('user/notifications', 'UsersController@postNotifications')->name('notifications');

    // Logout
    Route::get('/logout', 'UsersController@logout')->name('logout');
    // About Us
    Route::get('about-us', 'UsersController@aboutUs')->name('aboutus');
    // Contact Us
    Route::get('contact-us', 'UsersController@contactUs')->name('contactus');
    //contact us  Thnak you Page
    Route::get('contactus-thankyou', 'UsersController@contactUsThankyou')->name('contactusThankyou');
    // Contact Us post
    Route::post('contact-us-form', 'UsersController@contactUsForm')->name('contactusform');
    // how it works
    Route::get('how-it-works', 'UsersController@howItWorks')->name('howitworks');
    // Terms And Conditions
    Route::get('terms-and-conditions', 'UsersController@termsConditions')->name('termsConditions');
    // Privacy Policy
    Route::get('privacy-policy', 'UsersController@privacyPolicy')->name('privacyPolicy');
     //Thank you page for client
     Route::get('client/thank-you', 'UsersController@clientThankyou')->name('clientThankyou');
     //Thank you page for Consultant
     Route::get('consultant/thank-you', 'UsersController@consultantThankyou')->name('consultantThankyou');
    // Search consultant auto population
    Route::any('getsearchconsultant', 'UsersController@getSearchConsultant')->name('getSearchConsultant');
    // Search job posting auto population
    Route::any('getsearchjob', 'UsersController@getSearchjobs')->name('getJobConsultant');
    
    // The Atap Virtual Office
    Route::get('the-atap-virtual-office', 'UsersController@atapVirtualOffice')->name('atapVirtualOffice');
    // Search Consultant/Job
    Route::any('/general-search', 'UsersController@generalSearch')->name('general-search');
//View Job Detail
    Route::any('/job-details/{id}', 'UsersController@viewJobDetail')->name('job-details');
//view Consultants Detail
    Route::any('/consultants-details/{id}', 'UsersController@viewConsultantDetail')->name('consultant-detail');

// Working
    Route::get('/downloadfile', 'UsersController@downloadfile');
// Not working
    Route::get('/downloadimage', 'UsersController@downloadimage');
    // Get Interview Notes
    Route::post('/interviewnotes', 'UsersController@getInterviewNotes')->name('getinterviewnotes');
    // Read and Remove Notification Counter
    Route::post('/readnotification', 'UsersController@readNotification')->name('readnotification');

    Route::get('/downloadprofileimage', 'UsersController@downloadprofileimage');
    
    Route::get('/faq', 'UsersController@frontFaq')->name('frontfaq');

    Route::group(['prefix' => 'client', 'middleware' => ['auth','CheckAccountDeleted']], function () {
        // Home
        Route::get('/home', 'UsersController@clientHome')->name('client-home');
        // Profile
        Route::get('/profile', 'UsersController@clientProfile')->name('client-profile');
        // Jobs
        Route::any('/jobs', 'UsersController@clientJobs')->name('client-jobs');
        // Interviews
        Route::get('/interviews', 'UsersController@clientInterviews')->name('client-interviews');
        // Edit Profile
        Route::any('/edit-profile/{id}', 'UsersController@editClientProfile')->name('client-edit-profile');
        // City List
        Route::post('/city-list', 'UsersController@getCityList')->name('city-list');
        // Edit Job
        Route::any('/job/edit/{id}', 'UsersController@clientEditJob')->name('client-edit-job');
        // Delete Job
        Route::get('/job/delete/{id}', 'UsersController@clientDeleteJob')->name('client-delete-job');
        // Add job note
        Route::post('/job/addnote', 'UsersController@clientAddJobNote')->name('client-add-jobnote');
        // Search Consultant/Job
        // Route::any('/search', 'UsersController@clientSearch')->name('search');
        // Search Consultant/Job
        Route::get('/settings', 'UsersController@clientSettings')->name('clientSettings');
        // Save to wishlist
        Route::post('/savetowishlist', 'UsersController@addToWishList')->name('client-saveto-wishlist');
        // Remove to wishlist ajax
        Route::any('/wishlist/removetowishlist', 'UsersController@removeToWhishlist')->name('client-removeto-wishlist');
        // Delete Consultant From Wishlist
        Route::get('/wishlist/delete/{id}', 'UsersController@deleteConsultantFromWishlist')->name('client-remove-wishlist');
        // Delete Consultant From requested profile
        Route::get('/request/delete/{id}', 'UsersController@deleteConsultantFromRequest')->name('client-remove-requestedprofile');
        // Client Notifications
        Route::get('/notifications', 'UsersController@clientNotifications')->name('client-notifications');
        // Schedule interview
        Route::any('/schedule-interview/{id}', 'UsersController@scheduleInterview')->name('schedule-interview');
        // Client request candidate profile
        Route::get('user/request-candidate-profile/{id}', 'UsersController@requestCandidateProfile')->name('request-candidate-profile');
        Route::get('user/request-candidate-profile/api/{id}', 'UsersController@requestCandidateProfileAPI')->name('request-candidate-profile-api');
        // Cancelled request candidate profile
        Route::get('user/cancelled-candidate-profile/{id}', 'UsersController@cancelCandidateProfile')->name('cancelled-candidate-profile');
        Route::get('user/cancelled-candidate-profile/api/{id}', 'UsersController@cancelCandidateProfileAPI')->name('cancelled-candidate-profile-api');
        // Download CV
        Route::get('user/downloadcv/{id}', 'UsersController@downloadCandidateCv')->name('downloadcv');
        // Client Update Profile
        Route::post('/update-profile', 'UsersController@updateClientProfile')->name('client_update_profile');
        // Successful hire list
        Route::get('successful-hires', 'UsersController@successfulHires')->name('successful-hires');
        // Getting client profile to edit
        Route::post('/getcontactprofile', 'UsersController@editContactProfile')->name('getcontactprofile');
        // Getting client profile to edit and view
        Route::any('/viewgetcontactprofile', 'UsersController@viewContactProfile')->name('viewcontactprofile');
        // Client profile image uplaod 
        Route::any('/uploadcontactprofilepic', 'UsersController@uploadContactProfilePicture')->name('uploadContactProfilePicture');
        // Removing profile pic
        Route::post('/removecontactprofilepic', 'UsersController@removeContactProfilePicture')->name('remove-contact-profile-pic');
        // Consultant add to compare ajax
        Route::any('/add-tocompare', 'UsersController@addCompare')->name('addCompare');
        //consultant add tocompare page
        Route::get('/add-to-compare', 'UsersController@addToCompare')->name('addComparepage');
        //consultant Remove to compare ajax
        Route::any('/remove-to-compare', 'UsersController@removeToCompare')->name('removeComparepage');
        // Client profile pic upload
        Route::post('/upload-profilepic', 'UsersController@uploadProfilePic')->name('upload-profilepic');
    });
});