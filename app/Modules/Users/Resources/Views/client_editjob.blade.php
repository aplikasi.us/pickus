@extends('Layouts.Client.base_new')
@section('content')
@section('title','Edit Job')

<div class="row floated-div">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        {{ Session::get('success') }}
    </div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissable">
        {{ Session::get('error') }}
    </div>
    @endif
    <div class="col-md-8">
        <h2>Edit Job</h2>
        <p class="pb-3">
            <a href="{{ url()->previous() }}" title="Go Back" class="btnBack">
                <i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
        </p>
        <div class="tabsInterviews tabJobs">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="postJob">
                    <div class="boxForm pt-4">
                        <form method="post" action="{{route('client-edit-job',[\Crypt::encryptString($job->id)])}}"
                            id="client-job-post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Job Title</label>
                                        <input name="job_title" type="text" class="form-control"
                                            value="{{$job->posting_title}}" placeholder="Enter job title">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row rs-5">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Budget</label>
                                                <div class="input-group formborder-left">
                                                    <span class="input-group-addon ">MYR</span>
                                                    <!-- <div class="textMyr sticky-image">MYR</div> -->
                                                    <input name="budget" type="text" id="job-budget"
                                                        class="form-control inputMyr" maxlength="10"
                                                        value="{{$job->actual_revenue}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Calculated Base Rate
                                                    <a href="#" class="iconInfo">
                                                        <img src="{{admin_asset('images/iconInfo.png')}}" alt=""
                                                            title="">
                                                    </a>
                                                </label>
                                                <div class="formgroup">
                                                    <div class="form-icon-big text-red">
                                                        <!-- <span class="cus-br-icon-big">BR</span> -->
                                                        <img class="cus-br-icon-mid" src="{{admin_asset('images/cus-br-icon.png')}}">
                                                        <span
                                                            class="inputBase job-calculated-baserate text-red">{{$job->job_base_rate}}</span>
                                                        <input type="hidden" class="job-calculated-baserate-input"
                                                            name="baserate" value="{{$job->job_base_rate}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Experience Required (Years)</label>
                                        {{Form::select('experiece',[null => '--Select Experience--'] + $experienceList,$job->work_experience,['class' => 'form-control select2','required'])}}
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Industry</label>
                                        {{Form::select('industry',[null => '--Select Industry--'] + $industryList,$job->industry,['class' => 'form-control select2'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row rs-5">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Start Date</label>
                                                <div class="input-group formborder-right">
                                                    <!-- <div class="textMyr sticky-image">MYR</div> -->
                                                    <input name="start_date" id="job-start-date" type="text"
                                                        class="form-control"
                                                        value="{{date('d-m-Y',strtotime($job->date_opened))}}"
                                                        readonly="" placeholder="Select Start Date">
                                                    <span class="input-group-addon "><i
                                                            class="fa fa-calendar colorSkyBlue"
                                                            aria-hidden="true"></i></span>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Duration</label>
                                                <input name="duration" type="text" class="form-control"
                                                    value="{{$job->job_duration}}" maxlength="10"
                                                    placeholder="Enter month(s)">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row rs-5">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">State</label>
                                                {{Form::select('state',[null => '--Select State--'] + $stateList,$stateRecord->id,['class' => 'form-control select2','id' => 'job-state','required' => true])}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">City</label>
                                                {{Form::select('city',[null => '--Select City--'] + $city,$job->city,['class' => 'form-control select2','id' => 'job-city','required'=> true])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row rs-5">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Job Type</label>
                                                {{Form::select('job_type',[null => '--Select Job Type--'] + $jobTypeList,$job->job_type,['class' => 'form-control select2'])}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Job Mode</label>
                                                {{Form::select('job_mode',[null => '--Select Job Mode--'] + $jobModeList,$job->job_mode,['class' => 'form-control select2'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Categories</label>
                                        <!-- {{Form::select('category',[null => '--Select Category--'] + $jobCategoryList,$job->category,['class' => 'form-control select2'])}} -->
                                        {{Form::select('category[]',$categories,explode(',',$job->category),['class'=>'form-control select2','id' => 'candidate-category','multiple' => true])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Key Skills</label>
                                        <!-- <textarea name="key_skills" class="form-control textareaSmall" placeholder="Please enter comma separated key skills. e.g Node, Angular, Php etc...">{{$job->key_skills}}</textarea> -->
                                        <input data-skill="" data-role="tagsinput" name="key_skills"
                                            value="{{$job->key_skills}}">
                                        <small class="colorBlue mt-2">Type key skill and press enter to add new key
                                            skill</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Job Description</label>
                                        <textarea name="job_description" class="form-control textareaSmall"
                                            id="job_description">{{$job->job_description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Requirements</label>
                                        <textarea name="requirements" class="form-control textareaSmall"
                                            id="job_requirements">{{$job->job_requirements}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Benefits</label>
                                        <textarea name="benefits" class="form-control textareaSmall"
                                            id="job_benefits">{{$job->job_benefits}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center pb-4 pt-2">You can add notes once the job has been approved, isn’t
                                that good?</p>
                            <p class="text-right pb-0">
                                <a href="{{route('client-jobs')}}" class="btn btn-secondary border-ra-5 btn-sm">X</a>
                                <input type="submit" value="Save" class="btn form-submit-btn btn-sm btn-submit">
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="commanContent boxHowmuch">
            <h3>Doing Your Budget?</h3>
            <div class="contentHowmuch">
                <p class="pb-1">Enter Base Rate</p>
                <div class="text-red inlineText">
                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                    <span class="ml-1">MYR </span>
                </div>
                <input type="text" class="text-red text-right" value="" id="c3-base-rate-perday" />
                <span class="text-red">/day</span>
            </div>
            <div class="row f-12 rs-5">
                <div class="col-md-6">
                    <p class="pb-2">Length of contract
                        <a href="javascript:void(0)" class="iconInfo">
                            <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                        </a>
                    </p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5 months
                        </li>
                        <li>
                            <input type="radio" name="months" class="months" value="2" title="6-11 months" checked="" />
                            6-11 months</li>
                        <li>
                            <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                            months</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <p class="pb-2">Fees</p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                checked="" disabled="" /> Admin fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                        <li>
                            <input type="checkbox" class="fees" name="professional_fee" title="Professional fee"
                                id="professional_fee" checked="" /> Professional fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="boxDoyouKnow mb-0">
                <h6 class="f-14 mb-0">The Billing Rate is</h6>
                <h2 class="text-red" id="billing-rate">MYR 0.00/day</h2>
            </div>
        </div>
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
@endsection