@extends('Layouts.General.base')
@section('content')
@section('title','Generate Password')
<section class="secContent secJoinus">
    <div class="container">
        <h3 class="text-center pb-1">Generate Your Password</h3>
        <div class="formJoinus">
            <form method="post" method="post" action="{{route('generate-password',[$id])}}" id="consultant-generate-password">
                {{csrf_field()}}
                <h6 class="text-green">Once you generate your password, your account will be verified successfully!</h6>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Enter password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="password" name="confirm_password" class="form-control" placeholder="Re-enter your password">
                        </div>
                    </div>
                </div>	
                <input type="submit" class="btn btn-block btn-green" value="GENERATE NOW!"/>			
            </form>
        </div>
    </div>
</section>
@endsection