@extends('Layouts.General.base_new')
@section('content')
@section('title','About Us')
<article class="pageHowitWorks">
    <section class="howitworksection1 howitworksection1-mobile">
        <div class="container">
            <div class="row rowHomeTopcontent">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <h2>How it works</h2>
                    <!-- <p class="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel vulputate nunc, ut rhoncus ante. Duis diam orci, pellentesque ut lorem scelerisque, pharetra mollis lorem. Aliquam ipsum urna, sodales nec diam sed.</p>
                    <p> -->
                        <!-- <a href="javascript:;" class="btnVideo" title="What is aplikasi.us?"><img src="http://localhost/aplikasi-php/public/images/icon-play.png" alt="" title="">How does it Works?</a> -->
                        <a href="#" class="btnVideo youtube-link-pop" title="How does it Works?" youtubeid="YUQAVqb5SP0"><img src="{{admin_asset('images/icon-play.png')}}" alt="" title=""/>How does it Work?</a>
                    </p>
                </div>
               
            </div>
            <div class="row mobile-show">
                <div class="col-12 ">
                        <img class="img-right-mob" src="{{admin_asset('images/howitwork_right_mobile2.png')}}"/>
                </div>
            </div>
        </div>
    </section>
    <section class="howitworksection2 mobile-hide">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center pr-100">
                    <h2 class="bor-top-bot">Consultants</h2>
                    <div class="right-timeline-cross">
                        <div class="right-timeline">
                            <div class="cus-timiline-box">
                                <div class="cus-timiline-content text-right">
                                    <h3 class="colorWhite pb-3">Register</h3>
                                    <p class="WhitColor">Fill your particulars in the form provided</p>
                                </div>
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element1.png')}}" alt="" title=""/>
                                </div>
                            </div>
                            <div class="cus-timiline-box">
                                <div class="cus-timiline-content text-right">
                                    <h3 class="colorWhite pb-3">Screening</h3>
                                    <p class="WhitColor">your profile will be screened and optimized</p>
                                </div>
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element2.png')}}" alt="" title=""/>
                                </div>
                            </div>
                            <div class="cus-timiline-box">
                                <div class="cus-timiline-content text-right">
                                    <h3 class="colorWhite pb-3">Publish</h3>
                                    <p class="WhitColor">Once elevated, your profile will be published</p>
                                </div>
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element3.png')}}" alt="" title=""/>
                                </div>
                            </div>
                            <div class="cus-timiline-box">
                                <div class="cus-timiline-content text-right">
                                    <h3 class="colorWhite pb-3">Client Request</h3>
                                    <p class="WhitColor">Client will request your profile when they intersted</p>
                                </div>
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element4.png')}}" alt="" title=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center pl-100">
                    <h2 class="bor-top-bot">Clients</h2>
                    <div class="left-timeline-cross">
                        <div class="left-timeline">
                            <div class="cus-timiline-box text-left">
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element5.png')}}" alt="" title=""/>
                                </div>
                                <div class="cus-timiline-content">
                                    <h3 class="colorWhite pb-3">Search</h3>
                                    <p class="WhitColor">Look for your consultants by the job title and skill set</p>
                                </div>
                            </div>
                            <div class="cus-timiline-box text-left">
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element6.png')}}" alt="" title=""/>
                                </div>
                                <div class="cus-timiline-content">
                                    <h3 class="colorWhite pb-3">Request</h3>
                                    <p class="WhitColor">Request the candidate's profile of your choice</p>
                                </div>
                            </div>
                            <div class="cus-timiline-box text-left">
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element7.png')}}" alt="" title=""/>
                                </div>
                                <div class="cus-timiline-content">
                                    <h3 class="colorWhite pb-3">Get Profile</h3>
                                    <p class="WhitColor">We will send your the profile for your consideration</p>
                                </div>
                            </div>
                            <div class="cus-timiline-box text-left">
                                <div class="cus-timiline-icon">
                                    <img src="{{admin_asset('images/how-element8.png')}}" alt="" title=""/>
                                </div>
                                <div class="cus-timiline-content">
                                    <h3 class="colorWhite pb-3">Interview</h3>
                                    <p class="WhitColor">Interview session will be setup accordingly</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row center-timleine pt-5">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="center-left-timeline">
                        <div class="cus-timiline-box text-left">
                            <div class="cus-timiline-icon">
                                <img src="{{admin_asset('images/how-element9.png')}}" alt="" title=""/>
                            </div>
                            <div class="cus-timiline-content">
                                <h3 class="colorWhite pb-3">Quotation</h3>
                                <p class="WhitColor">Get the agreed quotion</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="center-right-timeline">
                        <div class="cus-timiline-box text-right">    
                            <div class="cus-timiline-content">
                                    <h3 class="colorWhite pb-3">Contract</h3>
                                    <p class="WhitColor">Settle and sign contract</p>
                            </div>
                            <div class="cus-timiline-icon">
                                <img src="{{admin_asset('images/how-element10.png')}}" alt="" title=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="center-left-timeline">
                        <div class="cus-timiline-box text-left">
                            <div class="cus-timiline-icon">
                                <img src="{{admin_asset('images/how-element11.png')}}" alt="" title=""/>
                            </div>
                            <div class="cus-timiline-content">
                                <h3 class="colorWhite pb-3">Onboard</h3>
                                <p class="WhitColor">Onboarding process in no time and hassle   </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="howitworksection2-mobile mobile-show">
        <div class="container">
            <div class="row ">
                
                <div class="tab-home-black how-it-works-tab">
                    <ul class="nav nav-tabs nav-fill">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#consultantProfiles" role="tab" aria-selected="true">For Consultants</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#clientProfiles" role="tab" aria-selected="false">For Clients</a>
                        </li>
                    </ul>
                    <div class="tab-content border-ra-5">
                        <div class="tab-pane fade active show" id="consultantProfiles">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="center-timeline-mobile">
                                        <div class="center-timeline-cus-mobile">
                                            <div class="cus-timiline-box">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Register</h3>
                                                    <p class="WhitColor">Fill your particulars in the form provided</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element1.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Screening</h3>
                                                    <p class="WhitColor">your profile will be screened and optimized</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element2.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Publish</h3>
                                                    <p class="WhitColor">Once elevated, your profile will be published</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element3.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Client Request</h3>
                                                    <p class="WhitColor">Client will request your profile when they intersted</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element4.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Quotation</h3>
                                                    <p class="WhitColor">Get the agreed quotion</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element9.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box">   
                                                <div class="cus-timiline-content">
                                                        <h3 class="colorWhite pb-3">Contract</h3>
                                                        <p class="WhitColor">Settle and sign contract</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element10.png')}}" alt="" title=""/>
                                                </div> 
                                            </div>
                                            <div class="cus-timiline-box">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Onboard</h3>
                                                    <p class="WhitColor">Onboarding process in no time and hassle   </p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element11.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane fade " id="clientProfiles">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="center-timeline-mobile">
                                        <div class="center-timeline-cus-mobile">
                                            <div class="cus-timiline-box text-left">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Search</h3>
                                                    <p class="WhitColor">Look for your consultants by the job title and skill set</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element5.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box text-left">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Request</h3>
                                                    <p class="WhitColor">Request the candidate's profile of your choice</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element6.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box text-left">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Get Profile</h3>
                                                    <p class="WhitColor">We will send your the profile for your consideration</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element7.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box text-left">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Interview</h3>
                                                    <p class="WhitColor">Interview session will be setup accordingly</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element8.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box text-left">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Quotation</h3>
                                                    <p class="WhitColor">Get the agreed quotion</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element9.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                            <div class="cus-timiline-box text-left">   
                                                <div class="cus-timiline-content">
                                                        <h3 class="colorWhite pb-3">Contract</h3>
                                                        <p class="WhitColor">Settle and sign contract</p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element10.png')}}" alt="" title=""/>
                                                </div> 
                                            </div>
                                            <div class="cus-timiline-box text-left">
                                                <div class="cus-timiline-content">
                                                    <h3 class="colorWhite pb-3">Onboard</h3>
                                                    <p class="WhitColor">Onboarding process in no time and hassle   </p>
                                                </div>
                                                <div class="cus-timiline-icon">
                                                    <img src="{{admin_asset('images/how-element11.png')}}" alt="" title=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="howitworksection3">
        <div class="container">
            <div class="row rowHomeTopcontent">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="sliderAboutus cusHowitSlider">
                        <div id="carouselAboutus" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <p class="testimonyText-how" style="margin-left: -6%;margin-top: -5%;">"Freelancing is already hard as it is but what is worse is when we don’t get paid on time. Joining us was one of the best decisions I have made in my freelancing career. Better yet, they always champion our own Malaysians for every job they have. They buffer the risks for me and I always get paid on time. Definitely the one place I will go for future jobs."</p>
                                    <p class="testimonyText">J. Y. Liew</p>
                                    <p class="testimonyText">SAP PI/PO & ABAP Consultant</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="imghowit_Element15">
                        <img src="{{admin_asset('images/how-element12.png')}}" alt="" title=""/>
                    </div>	
                </div>
            </div>
        </div>
    </section>
    <section class="howitworksection4">
        <div class="container">
            <div class="row rowHomeTopcontent">
                
                <div class="col-12 col-sm-12 col-md-5 col-lg-5  tab-hide">
                    <div class="imghowit_Element16">
                        <img src="{{admin_asset('images/how-element13.png')}}" alt="" title=""/>
                    </div>	
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="sliderAboutus cusHowitSlider2">
                        <div id="carouselHowitworks" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <p class="testimonyText-how" style="margin-left: -6%;margin-top: -5%;">"If you want fast and quality service, us is definitely the one place you can go to get great SAP consultants. The process is simple and I get my consultants when I need them."</p>
                                    <p class="testimonyText" >K. Kumar</P>
                                    <p class="testimonyText" > Vantage Point Consulting Sdn Bhd</P>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 tab-show">
                    <div class="imghowit_Element16">
                        <img src="{{admin_asset('images/how-element13.png')}}" alt="" title=""/>
                    </div>	
                </div>
            </div>
        </div>
    </section>
</article>
@endsection
@section('javascript')
<script type="text/javascript">
     $(document).ready(function () {
       
        $(".youtube-link-pop").grtyoutube();
    });
</script>
@endsection