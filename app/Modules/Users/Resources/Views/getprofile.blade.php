<link rel="stylesheet" href="{{URL::asset('/backend/css/style.css')}}">
<div class="row flex-row-reverse">
    <div class="col-12 col-sm-12">
        <h3>Update Profile</h3>
    </div>

    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <div class="form-group">
            {{Form::open(['route' => 'upload-profilepic','class' => 'dropzone','id' => 'profile-pic-form'])}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="dz-message needsclick">
                Drop files here or click to upload.<br>
            </div>
            {{Form::close()}}

            @if($user != null)
            <div class="client-profile-image mt-20">
                @if(isset($user->profile_pic))
                <img src="{{admin_asset('profile_image/'.$user->profile_pic)}}" id="profile_image" />
                @else
                <p>No image uploaded</p>
                @endif
            </div>
            @endif
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">

        {{Form::open(['route' => 'client_update_profile','id' => 'contact-profile-form','enctype' => 'multipart/form-data'])}}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">First Name: <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->first_name}}" name="firstname" class="form-control firstname">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Last Name: <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->last_name}}" name="lastname" class="form-control lastname">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Job Title: <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->client_name}}" name="title" class="form-control title">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Department: </label>
                    <input type="text" value="{{$contact->department}}" name="department"
                        class="form-control department">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Contact Number: <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->mobile}}" name="mobile" class="form-control mobile">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Street: <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->mailing_street}}" name="mailing_street"
                        class="form-control mailing_street">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">City : <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->mailing_city}}" name="mailing_city" class="form-control city">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Zipcode : <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->mailing_zip}}" name="mailing_zip"
                        class="form-control mailing_zip">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">State : <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->mailing_state}}" name="mailing_state" class="form-control state">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label style="font-size: 15px;font-weight: normal;">Country: <span
                            class="input-required">*</span></label>
                    <input type="text" value="{{$contact->mailing_country}}" name="mailing_country"
                        class="form-control mailing_country">
                </div>
            </div>
        </div>

        <div class="form-group">
            <a href="{{route('client-profile')}}" class="btn btn-danger btn-sm cancel-contact-profile">Close</a>
            <button type="submit" value="Save" class="btn btn-green  btn-sm contact-profile-ajax">Save</button>
        </div>
        {{Form::close()}}
    </div>
</div>
<script>
$(document).ready(function() {
    // Consultant profile update
    
    
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                firstname: {
                    validators: {
                        notEmpty: {
                            message: "First name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "First name max length is 50 character"
                        }
                    }
                },
                lastname: {
                    validators: {
                        notEmpty: {
                            message: "Last name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Last name max length is 50 character"
                        }
                    }
                },
                title: {
                    validators: {
                        notEmpty: {
                            message: "Job title is required"
                        },
                        stringLength: {
                            max: 100,
                            message: "Job title max length is 100 character"
                        }
                    }
                },
                department: {
                    validators: {
                        notEmpty: {
                            message: "Department is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Department max length is 50 character"
                        }
                    }
                },
                mobile: {
                    validators: {
                        notEmpty: {
                            message: "Contact number is required"
                        },
                        regexp: {
                            regexp: /^[0-9-+()]*$/,
                            message: 'Contact number must be digits only'
                        },
                        stringLength: {
                            min: 10,
                            max: 15,
                            message: "Contact number must be minimum 10 digits and maximum 15 digits"
                        }
                    }
                },
                mailing_city: {
                    validators: {
                        notEmpty: {
                            message: "City is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "City max length is 50 character"
                        }
                    }
                },
                profile_pic: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            message: 'Please select only image'
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data('bv.messages')
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]').show();
        });
    $("#profile-pic-form").dropzone({
        url: $("#profile-pic-form").attr('action'),
        maxFiles: 1,
        acceptedFiles: 'image/*',
        addRemoveLinks: true
    });
});
</script>