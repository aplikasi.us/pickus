@extends('Layouts.General.base_new')
@section('content')
@section('title','The Atap Virtual Office')
<article class="pageAtap " >
    <section class="atapsection1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="contentSapconsultant">
                        <h2>The First Virtual Ofﬁce for SAP Consultant</h2>
                        <p class="f-16">With a direct link to Subang Jaya LRT/KTM Interchange Station, cheap parking rates, various eating places to choose from, our Virtual Ofﬁce is the perfect for you! This is the chance to get that business presence you have always wanted. </p>
                        <p  class="f-16">Enjoy insanely low price for every package and even more privileges when you become our Annual Member! And how do we differ from the others? Well, for one, no virtual ofﬁce is offered exclusively to SAP consultants only, that you can be sure.</p>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">
                                <a title="JUST LOGGING IN, PEOPLE" target="_blank" href="https://theatap.campfyrex.com/login" class="btn bgGreen">JUST LOGGING IN, PEOPLE</a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <a title="I CAN’T WAIT TO REGISTER" target="_blank" href="https://theatap.campfyrex.com/register" class="btn bgOrange">I CAN’T WAIT TO REGISTER</a>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>		
    <section class="atapsection2">
        <div class="container">
            <div class="imgAtap_Element2">
                <img src="{{admin_asset('images/atap_element2.png')}}" alt="" title=""/>
            </div>
            
            <div class="headingPackages mtb-80">
                <h2>Packages</h2>
                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel vulputate nunc, ut rhoncus ante. Duis diam orci, pellentesque ut lorem scelerisque, pharetra mollis lorem. Aliquam ipsum urna, sodales nec diam sed.</p> -->
            </div>
            <div class="boxPackages">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4">
                        <div class="contentPackages">
                            <div class="headingPlan">
                                <h4>Annual Membership <br> (Preferred)</h4>
                            </div>
                            <div class="innerContentPackages">
                                <div class="imgPlan">
                                    <img src="{{admin_asset('images/atap_element4.png')}}" title="" alt="" />
                                </div>
                                <div class="planRate">
                                    <h2><sup>RM</sup>696<span>/year</span></h2>
                                </div>
                                <ul class="listPlan">
                                    <li>24-hour access, 7 days/week.</li>
                                    <li>Snacks, tea and coffee</li>
                                    <li>24/7 Internet access</li>
                                    <li>Hot desk (upgradable to Fixed desk)</li>
                                    <li>1 Company placard</li>
                                    <li>1 Designated pigeonhole</li>
                                    <li>May add monthly members</li>
                                    <li>Eligible to apply parking and building pass</li>
                                </ul>
                                <p class="text-center pb-0"><a title="GET STARTED!" href="https://theatap.campfyrex.com/register" target="_blank" class="btn btnRed">GET STARTED!<i class="fa fa-angle-double-down" aria-hidden="true"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4">
                        <div class="contentPackages">
                            <div class="headingPlan">
                                <h4>Walk-in</h4>
                            </div>
                            <div class="innerContentPackages">
                                <div class="imgPlan">
                                    <img src="{{admin_asset('images/atap_element6.png'
                                    )}}" title="" alt="" />
                                </div>
                                <div class="planRate">
                                    <h2><sup>RM</sup>12<span>/day</span></h2>
                                </div>
                                <ul class="listPlan">
                                    <li>24-hour access, 7 days/week.</li>
                                    <li>Snacks, tea and coffee</li>
                                    <li>24/7 Internet access</li>
                                    <li>Hot desk (non-upgradable)</li>
                                    <li>No company placard</li>
                                    <li>No pigeonhole</li>
                                    <li>Cannot add monthly members</li>
                                    <li>Non-eligible to apply parking and building pass</li>
                                </ul>
                                <p class="text-center pb-0"><a title="GET STARTED!" href="https://theatap.campfyrex.com/register"  target="_blank" class="btn btnRed">GET STARTED!<i class="fa fa-angle-double-down" aria-hidden="true"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4">
                            <div class="contentPackages">
                                <div class="headingPlan">
                                    <h4>Meeting Room<br/> (The White Bunker)</h4>
                                </div>
                                <div class="innerContentPackages">
                                    <div class="imgPlan">
                                        <img src="{{admin_asset('images/atap_element5.png')}}" title="" alt="" />
                                    </div>
                                    <div class="planRate planRateSmall">
                                        <h2><sup>RM</sup>22<span>/hour</span></h2>
                                        <span class="textPlus">+</span>
                                        <h2><sup>RM</sup>12<span>/access*</span></h2>
                                    </div>
                                    <ul class="listPlan">
                                        <li>24-hour access, 7 days/week.</li>
                                        <li>Internet access</li>
                                        <li>Presentation tools</li>
                                        <li>Video conferencing needs</li>
                                        <li>Non-eligible to apply parking and building pass</li>
                                        <li>*charged to non-members only</li>
                                    </ul>
                                    <p class="text-center pb-0"><a title="GET STARTED!" href="https://theatap.campfyrex.com/register" target="_blank" class="btn btnRed">GET STARTED!<i class="fa fa-angle-double-down" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="imgAtap_Element1">
                <img src="{{admin_asset('images/atap_element1.png')}}" alt="" title=""/>
            </div>
        </div>
    </section>		
    <section class="atapsection3">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="boxWhatcancontent">
                        <h2>What can you expect?</h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel vulputate nunc, ut rhoncus ante. Duis diam orci, pellentesque ut lorem scelerisque, pharetra mollis lorem. Aliquam ipsum urna, sodales nec diam sed.</p> -->
                    </div>
                </div>
                <div class="imgAtap_Element7">
                    <img src="{{admin_asset('images/atap_element7.png')}}" alt="" title=""/>
                </div>	
            </div>
            <div class="rowBusinessPresence">
                <div class="sliderAboutus sliderBusinessPresence">
                    <div id="carouselAboutus" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <h3>Instant Business Presence</h3>
                                <p class="f-16">"Well that is if you become our Annual Member. For that price, you bet it's worth it. Tell us, just tell us, where else can you get a good space at this price? In an MSC-status building, no less."</p>
                            </div>
                            <div class="carousel-item">
                                <h3>Modern & Relaxing Environment</h3>
                                <p class="f-16">Our office has been designed to fit the contemporary theme, making everybody here feeling young again, regardless of how old they actually are."</p>
                            </div>
                            <div class="carousel-item">
                                <h3>Endless Supply of Snacks</h3>
                                <p class="f-16">Well, it's not endless per se. It is endless until it runs out. Have a cup of coffee, or two, snack yourself up, but don't blame us when the weighing scale shocks you. If that's not enough, we got a 7E down here.</p>
                            </div>
                            <div class="carousel-item">
                                <h3>Networking Opportunity</h3>
                                <p class="f-16">Yes, because everyone here is SAP consultants! Except for us, obviously, but hey, we do know hundreds of them still, that should count, right? Who knows, here might be the place you'll find your next big project?</p>
                            </div>
                            <div class="carousel-item">
                                <h3>Easy Public Transport Access</h3>
                                <p class="f-16">Linked directly to the Subang LRT and KTM interchange station, (also connected to Subang Airport), our office can't be more accessible for you!</p>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselAboutus" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a>
                        <a class="carousel-control-next" href="#carouselAboutus" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="imgAtap_Element8">
                    <img src="{{admin_asset('images/atap_element8.png')}}" alt="" title=""/>
                </div>
            </div>
        </div>
    </section>		
    <section class="atapsection4">
            <div class="container">
                <div class="headingContact">
                    <h2>Contact <span><img src="{{admin_asset('images/textUs.png')}}" title="" alt="" /></span></h2>
                    <p>Want more information? Well, just contact us at <a href="mailto:social@aplikasi.us" title="social@aplikasi.us">social@aplikasi.us</a>, or call us at <a href="mailto:60350321896"  title="+603-5032 1896">+603-5032 1896</a>. or for a faster response, use our chat box here and our team will gladly help you!</p>
                </div>
                <div class="boxmap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.033824668795!2d101.58687775111453!3d3.085647454387286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4c4312080b19%3A0xefcee70b251c9eeb!2sKumpulan+Aplikasi+Sempurna+Sdn+Bhd!5e0!3m2!1sen!2sin!4v1554425005052!5m2!1sen!2sin" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <p class="textWhyKeep">Why keep it all to yourself?</p>
                <?php
                    $url = url()->current();
                    $twitterUrl = 'http://twitter.com/share?text=The%20Atap%20Virtual%20Office&url=' . $url;
                    ?>
                <ul class="listSocialMediaNew">
                    <li>
                        <!-- <a target="_blank" href="#" title="Facebook"> -->
                        <a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php echo $url; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)">
                            <img src="{{admin_asset('images/atap_element9.png')}}" title="Facebook" alt="Facebook" />
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a target="_blank" href="#" title="Google+"> -->
                        <a onClick="window.open('https://plus.google.com/share?url=<?php echo $url; ?>','sharer','toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)">
                            <img src="{{admin_asset('images/atap_element10.png')}}" title="Google+" alt="Google+" />
                            <span>Google+</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a target="_blank" href="#" title="Twitter"> -->
                        <a onClick="window.open('<?php echo $twitterUrl; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)">
                            <img src="{{admin_asset('images/atap_element11.png')}}" title="Twitter" alt="Twitter" />
                            <span>Twitter</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a target="_blank" href="#" title="Linkedin"> -->
                        <a onClick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=The%20Atap%20Virtual%20Office%20Developer%20Network&source=LinkedIn','sharer','toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)">
                            <img src="{{admin_asset('images/atap_element12.png')}}" title="Linkedin" alt="Linkedin" />
                            <span>Linkedin</span>
                        </a>
                    </li>
                </ul>
            </div>
    </section>				
</article>
@endsection