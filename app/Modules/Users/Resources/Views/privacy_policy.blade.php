@extends('Layouts.General.base_new')
@section('content')
@section('title','Privacy Policy')
<article class="pageprivacy bgLightGray">
    <section class="privacysection1 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="colorWhite boder-top-bot d-inline-block">Privacy Policy</h2>
                </div>
            <div>
        </div>            
    </section>
    <section class="privacysection2 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="">
                        <h3 class="Black">PERSONAL DATA PROTECTION NOTICE</h3>
                        <p>
                            We at KUMPULAN APLIKASI SEMPURNA SDN BHD. recognize the importance of privacy and the sensitivity of personal information. As required under the Personal Data Protection Act 2010, the purpose of this Personal data Protection Notice (“Notice”) is to inform you how we collect and process your personal data when you appoint or engage us to provide services, when you access and use our website.
                        </p>
                        <p>
                            By appointing or engaging us to provide services to you, by accessing and using our website or by interacting with us, you consent to us using, collecting and processing your personal data in the manner contemplated in this Notice.
                        </p>
                        <p>
                            This Notice may be amended or updated from time to time. By continuing to use or engage our services, to use our website or to communicate with us subsequent to any amendments or updates to this Notice, it would confirm and indicate your acceptance to the amendments or updates to this Notice.
                        </p>
                        <p>What Personal Data Do We Collect?</p>
                        <p>
                            Personal Data generally means any information that identifies you. Your personal data is collected and processed by us at the start of our services or interaction and, from time to time, in the course of our engagement or interaction.
                        </p>
                        <p>
                            The type of personal data collected and processed by us includes;
                        </p>
                        <ul class="alphabet-number">
                            <li>
                            name, mobile number & email address;
                            </li>
                            <li>
                            employment category (private sector, government sector, self-employed, non-working or others), name of employer and office address;
                            </li>
                            <li>
                                such other information relevant or required for;
                                <ol class="roman-number">
                                    <li> our engagement;
                                    </li>
                                    <li>
                                        provision of our services for the purpose of what have been informed to you;
                                    </li>
                                    <li>
                                        compliance with legal and regulatory requirements; and
                                    </li>
                                </ol>
                            </li>
                            <li>
                                any additional information provided by you or third parties about you.
                            </li>
                        </ul>
                        <h4 class="Black">Source of Personal Data</h4>
                        <p>
                            Personal data collected will mainly be from you. However, some personal data may also be collected by us from other available sources including but not limited to credit reporting agencies, government department or agencies, public registries, websites, social media, publications, events, seminars, conferences.
                        </p>
                        <h4 class="Black">How Do We Use Your Personal Data</h4>
                        <p>
                            Your personal data is collected and processed by us for our business activities including the following purposes:
                        </p>
                        <ul class="alphabet-number">
                            <li>
                                to verify your identity;
                            </li>
                            <li>
                            to communicate with you including responding to your enquiries;
                            </li>
                            <li>
                            all purposes related to or in connection with our engagement;
                            </li>
                            <li>
                            to provide our services for the purposes of the display towards our potential client(s);
                            </li>
                            
                            <li>
                            to comply with legal and/or regulatory requirements including court orders;
                            </li>
                            <li>
                            for our day to day operations and administrative purposes including file management, billing and collection, audits, reporting, investigations etc.;
                            </li>
                            <li>
                            for the purpose of enforcing or defending our legal rights and/or obtaining legal advice;
                            </li>
                            <li>
                            to send you materials or publications including newsletter, articles or updates or information about events, conferences, seminars, talks which may be of interest to you;
                            </li>
                            <li>
                            to promote, offer or market our services to you;
                            </li>
                            <li>
                            to send as potential referees to clients / potential clients who request for referees of past work in relation to any proposal, submissions, tenders or bids;
                            </li>
                            <li>
                            to assist in the preventions, detections or investigation of crime or possible criminal activities or for the administration of justice;
                            </li>
                            <li>
                            for security and internal audit purposes;
                            </li>
                            <li>
                            for such other purposes as may be directed or consented to by you; and
                            </li>
                            <li>
                            for all other purposes in relation to or incidental to the above.
                            </li>


                        </ul>
                        <h4 class="Black">Disclosure of Your Personal Data</h4>
                        <p>
                            Under certain circumstances, we may be required to disclose your personal data to third parties. Third parties to whom your personal data may be disclosed by us are as follows:
                        </p>
                        <ul class="alphabet-number">
                            <li>
                            any persons directed by or consented by you;
                            </li>
                            <li>
                            any persons required for the purposes of the legal engagements and/or legal transactions including but not limited to counter parties, other advisors, financial institutions, regulatory bodies etc.;
                            </li>
                            <li>
                            any person for the purposes of compliance with legal and regulatory requirements;
                            </li>
                            <li>
                            our data processors i.e. third party who we engage to process personal data on our behalf including but not limited to archival storage, data entry service providers, computer backup services, disaster recovery services, banks and financial institutions etc.; and
                            </li>
                            <li>
                            our professional advisors including but not limited to legal advisors, tax advisors, financial advisors, auditors, insurance brokers etc.
                            </li>
                        </ul>
                        <p>
                            Further, we may also be required to transfer your personal data outside of Malaysia for the purposes and to such third parties stated in this Notice. The transfer of your personal data outside Malaysia would also be required if you are traveling, residing or based outside Malaysia.
                        </p>
                        <h4 class="Black">Obligation to Provide Personal Data</h4>
                        <p>
                            We acknowledge that you have the right in deciding the information you wish to provide to us. The provision of the information listed above is voluntary in nature. However, please note that if you do not provide the information above or limit the way such information is to be processed, it may result in us not being able to:
                        </p>
                        <ul class="alphabet-number">
                            <li>
                            communicate or correspond with you;
                            </li>
                            <li>
                                undertake the engagement or transaction and/or to provide our services to you; and/or
                            </li>
                            <li>
                                grant you access to our website.
                            </li>
                        </ul>
                        <h4 class="Black">Data Access and Data Correction</h4>
                        <p>
                            If you wish to request;
                        </p>
                        <ul class="alphabet-number">
                            <li>
                                for access to your personal information;
                            </li>
                            <li>
                                for your personal data to be corrected or updated; or
                            </li>
                            <li>
                            to withdraw your consent or limit the use of your personal data;
                            </li>
                        </ul>
                        <p>
                            Please submit your written request to:  
                        </p>
                        <p>
                        KUMPULAN APLIKASI SEMPURNA SDN BHD.</br>
                        No 2-2B Level 2, Tower 9, UOA Business Park,</br>
                        No.1, Jalan Pengaturcara U1/51A,</br>
                        Seksyen U1, 40150, Shah Alam,</br>
                        Selangor Darul Ehsan</br>
                        </p>
                        <p>
                            An administrative charge may be imposed for any request under paragraph (a) and (b) above.
                        </p>
                        <p>
                            Further, we reserve the right to refuse your requests for any reasons permitted under law.
                        </p>
                        <h4 class="Black">Disclaimer</h4>
                        <p>
                            The accuracy and completeness of your personal data depends on the information you provide. We assume that the information you have provided is accurate, up to date and complete unless you inform us otherwise.
                            </p>
                        <p>
                            Where you provide any third party information to us including but not limited to employer, colleagues, family information, it is our assumption that such information is accurate, up to date and complete and that you have obtained the necessary consent to disclose the same.
                            </p>
                        <p>
                        <h4 class="Black">Conflict</h4>
                        <p>
                            In the event that there is any conflict between the English and national language version of this Notice, the English language version shall prevail.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection