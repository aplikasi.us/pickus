@extends('Layouts.Client.base_new')
@section('content')
@section('title','Client-Home')

<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="commanContent commanContentBig border-ra-5">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                    <h5 class="pb-1 colorGray text-we-noraml">
                        {{($profileArray != null) ? $profileArray->job_title : ""}}</h5>
                    <h2><span
                            class="Black">{{($profileArray != null) ? $profileArray->first_name .' '. $profileArray->last_name : ""}}</span>
                    </h2>
                    <ul class="listContract mb-3">
                        <li class="text-we-noraml">{{($profileArray != null) ? $profileArray->department : ""}}</li>
                    </ul>
                    <div class="row rowContract">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="cus-iconBox cus-iconMail">
                                <a class="cusGray" href="mailto:{{Auth::user()->email}}"
                                    title="{{Auth::user()->email}}">{{Auth::user()->email}}</a>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="cus-iconBox cus-iconPhone">
                                <a href="tel:{{($profileArray != null) ? $profileArray->mobile : ""}}" class="cusGray"
                                    title="{{($profileArray != null) ? $profileArray->mobile : ""}}">{{($profileArray != null) ? $profileArray->mobile : ""}}</a>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="cus-iconBox cus-iconplace cusGray mt-2">
                                {{($profileArray != null) ? $profileArray->mailing_city : ""}}</div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="cus-iconBox cus-iconUpdated cusGray mt-2">Last updated:
                                {{ ($profileArray != null) ? date('d M. Y',strtotime($profileArray->updated_at)) : ""}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 border-before-left mob-border-none">
                    <div class="client-box-main d-flex">
                        <div class="client-box-inner dark-gray-bg">
                            <div class="box-heightFull">
                                <a href="{{route('client-jobs',['tab' => 'history'])}}" class="2">
                                    <h2 class="text-number colorWhite">{{$jobs['concludedJobs']}}</h2>
                                    <h5 class="pb-0 colorWhite">Concluded Jobs</h5>
                                </a>
                            </div>
                        </div>
                        <div class="client-box-inner dark-green-bg">
                            <div class="box-heightFull">
                                <a href="{{route('successful-hires')}}" class="2">
                                    <h2 class="text-number colorWhite">{{$interviews['successfulHires']}}</h2>
                                    <h5 class="pb-0 colorWhite">Successful Hires</h5>
                                </a>
                            </div>
                        </div>
                        <div class="client-box-inner dark-red-bg">
                            <div class="box-heightFull">
                                <a href="{{route('client-jobs',['tab' => 'activeJobs'])}}">
                                    <h2 class="text-number colorWhite">{{$jobs['activeJobs']}}</h2>
                                    <h5 class="pb-0 colorWhite">Active Jobs</h5>
                                </a>
                            </div>
                        </div>
                        <div class="client-box-inner dark-blue-bg">
                            <div class="box-heightFull ">
                                <a href="{{route('client-interviews',['tab','upcoming'])}}">
                                    <h2 class="text-number colorWhite">{{$interviews['upcomingInterview']}}</h2>
                                    <h5 class="pb-0 colorWhite">Upcoming Interviews</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tabsInterviews tabsJobs">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link {{(isset($_GET['tab']) && $_GET['tab'] == "requested_profile") ? 'active' : ''}}"
                        data-toggle="tab" href="#requestedProfiles" role="tab">Requested Profiles
                        ({{count($data['requested_profiles'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link linkWishlist {{(isset($_GET['tab']) && $_GET['tab'] == "wishlist") ? 'active' : ''}} {{(!isset($_GET['tab'])) ? "active" : ""}}"
                        data-toggle="tab" data-toggle="tab" href="#myWishlist" role="tab">My Wishlist
                        @php $wishlistCounter = 0; @endphp
                        @if(!empty($data['wishlist']))

                        @foreach($data['wishlist'] as $wkey => $wvalue)
                        @if(!in_array($wvalue->candidate->CANDIDATEID,$data['requested_profiles_candidate_ids']))
                        @php $wishlistCounter++; @endphp
                        @endif
                        @endforeach
                        @endif

                        ({{$wishlistCounter}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{(isset($_GET['tab']) && $_GET['tab'] == "vieweds") ? 'active' : ''}}"
                        data-toggle="tab" data-toggle="tab" href="#recentlyVieweds" role="tab">Recently
                        Viewed</a>
                </li>
            </ul>
            <div class="tab-content border-ra-5">
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "requested_profile") ? 'show active' : ''}}"
                    id="requestedProfiles">
                    @if(count($data['requested_profiles']))
                    <div class="rowJobs row rs-10">
                        @foreach($data['requested_profiles'] as $requestedKey => $requestedValue)
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="commanContent resulttiles">
                                <a href="{{route('consultant-detail',[\Crypt::encryptString($requestedValue->candidate->CANDIDATEID)])}}"
                                    class="home-jobs-listing job-tile-link"></a>
                                @if($requestedValue->candidate->available_for_contract == 1)
                                <div class="status online z-index-2" data-toggle="tooltip" title="Available"></div>
                                @else
                                <div class="status offline z-index-2" data-toggle="tooltip" title="Unavailable"></div>
                                @endif
                                <a href="{{route('client-remove-requestedprofile',[$requestedValue->id])}}"
                                    class="btnMinus delete-from-request mt-2 mr-3    ">
                                    <i class="fa fa-trash-o text-light" aria-hidden="true"></i>
                                </a>
                                <div style="padding-left: 10px;">
                                    <ul class="listContract">

                                        <li class="font">{{$requestedValue->candidate->candidate_id}}
                                            <div class="box-rating consultant-start-rating">
                                                @if(Auth::check())
                                                @if(Auth::user()->role_id == 1)

                                                @for($i = 1; $i <= 5; $i++) @if($requestedValue->candidate->rating < $i)
                                                        <span class="fa fa-star unchecked"></span>
                                                        @else
                                                        <span class="fa fa-star checked"></span>
                                                        @endif
                                                        @endfor

                                                        @endif
                                                        @endif
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row rowContract space">
                                    <h3 class="titles">{{$requestedValue->candidate->sap_job_title}}</h3>
                                    <div class="col-md-12">
                                        <div class="cus-verticale-center">
                                            <div class="text-red inlineText">

                                                <img class="cus-br-icon-small"
                                                    src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1">MYR </span>
                                                <span class="c-job-base-rate"
                                                    id="con-copy-0{{$requestedKey}}">{{($requestedValue->candidate->base_rate != null) ? $requestedValue->candidate->base_rate : "0.00"}}</span>/day
                                                <div
                                                    class="iconBox iconCopy inlineText copy-to-clipboard job-tile-button-right ml-2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row rowContract">
                                    <!-- 1st  -->
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{$requestedValue->candidate->experience_in_years}} years exp.</div>
                                    </div>
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($requestedValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-group-6.png')}}"
                                            class="icon-cus-img will-travel-img">
                                        <div
                                            class="ml-1 Black d-inline {{($requestedValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                                            {{($requestedValue->candidate->willing_to_travel == 1) ? "Ok to Travel" : "Travel"}}
                                        </div>
                                    </div>

                                </div>
                                <div class="row rowContract">

                                    <!-- 2nd -->

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{$requestedValue->candidate->notice_period_days}} days</div>
                                    </div>

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($requestedValue->candidate->project == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-9.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($requestedValue->candidate->project == 1) ? '' : 'disabled'}}">
                                            {{($requestedValue->candidate->project == 1) ? "Project" : "Project"}}</div>
                                    </div>

                                </div>
                                <div class="row rowContract">
                                    <!-- 3rd -->
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{date('d M. Y',strtotime($requestedValue->candidate->availability_date))}}
                                        </div>
                                    </div>

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($requestedValue->candidate->support == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-3.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($requestedValue->candidate->support == 1) ? '' : 'disabled'}}">
                                            {{($requestedValue->candidate->support == 1) ? "Support" : "No support"}}
                                        </div>
                                    </div>

                                </div>

                                <div class="row rowContract">
                                    <!-- 4th -->
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale  {{($requestedValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($requestedValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                                            {{($requestedValue->candidate->full_time == 1) ? "Full time" : "Full time"}}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline ">{{$requestedValue->candidate->city}}</div>
                                    </div>

                                </div>

                                <div class="row rowContract">
                                    <!-- 5th -->
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($requestedValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-8.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($requestedValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                                            {{($requestedValue->candidate->part_time == 1) ? "Part time" : "Part time"}}
                                        </div>
                                    </div>
                                </div>
                                <div class="sendtoBottom">
                                    <div class="boxbtns job-tile-button-right">
                                        <div class="row no-gutters border-top-btn">
                                            @php
                                            $status = $requestedValue->request_status;

                                            $btnCompare = 'can-btnRemoveCompare';
                                            $btntext = 'Remove Compare';
                                            if(!in_array($requestedValue->candidate->CANDIDATEID,$addTocompare)){
                                            $btnCompare = 'can-btnCompare';
                                            $btntext = 'Compare';
                                            }

                                            @endphp
                                            <div
                                                class="col-12 col-sm-12 col-md-12 col-lg-{{($status == 'Fake request' ? 12 : 6)}} col-xl-{{($status == 'Fake request' ? 12 : 6)}} border-right-btn">
                                                <a title="{{$btntext}}" href="javascript:;"
                                                    data-id="{{$requestedValue->candidate->CANDIDATEID}}"
                                                    data-candidate-id="{{$requestedValue->candidate->id}}"
                                                    data-reload="false"
                                                    class="btn btn-block btnCompare btn-custom-trans colorBlue {{$btnCompare}} addcompare-icon ">{{$btntext}}</a>
                                                <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading"
                                                    style="display: none;">

                                            </div>
                                            @if($status != "Fake request")
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 ">
                                                @if($status == "New")
                                                <a title="Cancel Requested"
                                                    href="{{route('cancelled-candidate-profile',[$requestedValue->candidate->id])}}"
                                                    class="btn btn-custom-trans colorRed btn-block  cancelbtnProfilerequest">Cancel
                                                    Requested
                                                </a>
                                                @else
                                                <a title="Download CV"
                                                    href="{{route('downloadcv',[$requestedValue->candidate->CANDIDATEID])}}"
                                                    class="btn btn-block downloadcv btn-custom-trans colorRed">Download
                                                    CV
                                                </a>
                                                @endif
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "wishlist") ? 'show active' : ''}} {{(!isset($_GET['tab'])) ? "show active" : ""}}"
                    id="myWishlist">
                    @if(count($data['wishlist']))
                    <div class="rowJobs row rs-10">
                        @foreach($data['wishlist'] as $wishlistKey => $wishlistValue)
                        @if(!in_array($wishlistValue->candidate->CANDIDATEID,$data['requested_profiles_candidate_ids']))
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="commanContent resulttiles">
                                @if($wishlistValue->candidate->available_for_contract == 1)
                                <div class="status online  z-index-2" data-toggle="tooltip" title="Available"></div>
                                @else
                                <div class="status offline  z-index-2" data-toggle="tooltip" title="Unavailable"></div>
                                @endif
                                <a href="{{route('client-remove-wishlist',[$wishlistValue->id])}}"
                                    class="btnMinus delete-from-wishlist mt-2 mr-3">
                                    <i class="fa fa-trash-o text-light" aria-hidden="true"></i>
                                </a>
                                <a href="{{route('consultant-detail',[\Crypt::encryptString($wishlistValue->candidate->CANDIDATEID)])}}"
                                    class="home-jobs-listing job-tile-link"></a>
                                <div style="padding-left: 10px;">
                                    <ul class="listContract">
                                        <li class="font">{{$wishlistValue->candidate->candidate_id}}
                                            <div class="box-rating consultant-start-rating">
                                                @if(Auth::check())
                                                @if(Auth::user()->role_id == 1)

                                                @for($i = 1; $i <= 5; $i++) @if($wishlistValue->candidate->rating < $i)
                                                        <span class="fa fa-star unchecked"></span>
                                                        @else
                                                        <span class="fa fa-star checked"></span>
                                                        @endif
                                                        @endfor

                                                        @endif
                                                        @endif
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row rowContract space">
                                    <h3 class="titles">{{$wishlistValue->candidate->sap_job_title}}</h3>


                                    <div class="col-md-12">
                                        <div class="cus-verticale-center">
                                            <div class="text-red inlineText">

                                                <img class="cus-br-icon-small"
                                                    src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1 gap">MYR</span>
                                                <span class="c-job-base-rate"
                                                    id="con-copy-0{{$wishlistKey}}">{{($wishlistValue->candidate->base_rate != null) ? $wishlistValue->candidate->base_rate : "0.00"}}</span>/day
                                                <div
                                                    class="iconBox iconCopy inlineText copy-to-clipboard job-tile-button-right ml-2">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline gap">
                                            {{$wishlistValue->candidate->experience_in_years}} years exp.</div>
                                    </div>

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($wishlistValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-group-6.png')}}"
                                            class="icon-cus-img will-travel-img">
                                        <div
                                            class="ml-1 Black d-inline gap {{($wishlistValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                                            {{($wishlistValue->candidate->willing_to_travel == 1) ? "Ok to Travel" : "Travel"}}
                                        </div>
                                    </div>


                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline gap">
                                            {{$wishlistValue->candidate->notice_period_days}} days</div>
                                    </div>

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($wishlistValue->candidate->project == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-9.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline gap {{($wishlistValue->candidate->project == 1) ? '' : 'disabled'}}">
                                            {{($wishlistValue->candidate->project == 1) ? "Project" : "Project"}}</div>
                                    </div>

                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline gap">
                                            {{date('d M. Y',strtotime($wishlistValue->candidate->availability_date))}}
                                        </div>
                                    </div>


                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($wishlistValue->candidate->support == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-3.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline gap {{($wishlistValue->candidate->support == 1) ? '' : 'disabled'}}">
                                            {{($wishlistValue->candidate->support == 1) ? "Support" : "Support"}}</div>
                                    </div>

                                </div>

                                <div class="row rowContract">
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($wishlistValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline gap {{($wishlistValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                                            {{($wishlistValue->candidate->full_time == 1) ? "Full time" : "Full time"}}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline gap">{{$wishlistValue->candidate->city}}</div>
                                    </div>

                                </div>

                                <div class="row rowContract">
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($wishlistValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-8.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline gap {{($wishlistValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                                            {{($wishlistValue->candidate->part_time == 1) ? "Part time" : "Part time"}}
                                        </div>
                                    </div>

                                </div>
                                <div class="sendtoBottom">
                                    <div class="boxbtns job-tile-button-right">
                                        <div class="row no-gutters border-top-btn">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 border-right-btn">
                                                @php
                                                $btnCompare = 'can-btnRemoveCompare';
                                                $btntext = 'Remove Compare';
                                                if(!in_array($wishlistValue->candidate->CANDIDATEID,$addTocompare)){
                                                $btnCompare = 'can-btnCompare';
                                                $btntext = 'Add to Compare';
                                                }
                                                @endphp

                                                <a title="{{$btntext}}" href="javascript:;"
                                                    data-id="{{$wishlistValue->candidate->CANDIDATEID}}"
                                                    data-candidate-id="{{$wishlistValue->candidate->id}}"
                                                    data-reload="false"
                                                    class="btn btn-block btnCompare btn-custom-trans colorBlue {{$btnCompare}} addcompare-icon ">{{$btntext}}</a>
                                                <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading"
                                                    style="display: none;">
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 ">
                                                <a title="Request Profile"
                                                    href="{{route('request-candidate-profile',[$wishlistValue->candidate->id])}}"
                                                    class="btn btn-custom-trans colorRed btn-block btnProfilerequest">Request
                                                    Profile
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    @endif
                </div>
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "vieweds") ? 'show active' : ''}}"
                    id="recentlyVieweds">
                    @if(count($data['recently_viewed']))
                    <div class="rowJobs row rs-10">
                        @foreach($data['recently_viewed'] as $recentKey => $recentValue)
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="commanContent resulttiles">
                                <a href="{{route('consultant-detail',[\Crypt::encryptString($recentValue->candidate->CANDIDATEID)])}}"
                                    class="home-jobs-listing job-tile-link"></a>
                                @if($recentValue->candidate->available_for_contract == 1)
                                <div class="status online " data-toggle="tooltip" title="Available"></div>
                                @else
                                <div class="status offline" data-toggle="tooltip" title="Unavailable"></div>
                                @endif
                                <div style="padding-left: 10px;">
                                    <ul class="listContract">
                                        <li class="font">{{$recentValue->candidate->candidate_id}}
                                            <div class="box-rating consultant-start-rating">
                                                @if(Auth::check())
                                                @if(Auth::user()->role_id == 1)

                                                @for($i = 1; $i <= 5; $i++) @if($recentValue->candidate->rating < $i)
                                                        <span class="fa fa-star unchecked"></span>
                                                        @else
                                                        <span class="fa fa-star checked"></span>
                                                        @endif
                                                        @endfor

                                                        @endif
                                                        @endif
                                            </div>
                                        </li>
                                        <li>
                                            <!-- Icon for share and wishlist -->
                                            <div class="row position">
                                                <a class="btnMinus delete-from-wishlist mt-2 mr-4"><img
                                                		src="{{admin_asset('images/new-design/share.png')}}"
                                                        class="icon-cus-img icon-share-gap margin-right">
                                                </a>
                                                <div>
                                                    <a class="btnMinus delete-from-wishlist mt-2 mr-1">
                                                        <img src="{{admin_asset('images/new-design/wishlist-1.png')}}"
                                                            class="icon-cus-img icon-wish-gap margin-right">
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>

                                </div>
                                <div class="row rowContract space">
                                    <h3 class="titles">{{$recentValue->candidate->sap_job_title}}</h3>
                                    <div class="col-md-12">
                                        <div class="cus-verticale-center">
                                            <div class="text-red inlineText">

                                                <img class="cus-br-icon-small"
                                                    src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1">MYR </span>
                                                <span class="c-job-base-rate"
                                                    id="con-copy-0{{$recentKey}}">{{($recentValue->candidate->base_rate != null) ? $recentValue->candidate->base_rate : "0.00"}}</span>/day
                                                <div
                                                    class="iconBox iconCopy inlineText copy-to-clipboard job-tile-button-right ml-2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{$recentValue->candidate->experience_in_years}} years exp.</div>
                                    </div>

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-group-6.png')}}"
                                            class="icon-cus-img will-travel-img">
                                        <div
                                            class="ml-1 Black d-inline {{($recentValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                                            {{($recentValue->candidate->willing_to_travel == 1) ? "Ok to Travel" : "Travel"}}
                                        </div>
                                    </div>

                                </div>

                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">{{$recentValue->candidate->notice_period_days}}
                                            days</div>
                                    </div>
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->project == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-9.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($recentValue->candidate->project == 1) ? '' : 'disabled'}}">
                                            {{($recentValue->candidate->project == 1) ? "Project" : "Project"}}</div>
                                    </div>

                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{date('d M. Y',strtotime($recentValue->candidate->availability_date))}}
                                        </div>
                                    </div>

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->support == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-3.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($recentValue->candidate->support == 1) ? '' : 'disabled'}}">
                                            {{($recentValue->candidate->support == 1) ? "Support" : "No support"}}</div>
                                    </div>
                                </div>

                                <div class="row rowContract">

                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale  {{($recentValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($recentValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                                            {{($recentValue->candidate->full_time == 1) ? "Full time" : "Full time"}}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline ">{{$recentValue->candidate->city}}</div>
                                    </div>

                                </div>
                                <div class="row rowContract">
                                    <div
                                        class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                                        <img src="{{admin_asset('images/cus-Group-8.png')}}" class="icon-cus-img">
                                        <div
                                            class="ml-1 Black d-inline {{($recentValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                                            {{($recentValue->candidate->part_time == 1) ? "Part time" : "Part time"}}
                                        </div>
                                    </div>
                                </div>

                                <div class="sendtoBottom">
                                    <div class="boxbtns job-tile-button-right ">
                                        <div class="row no-gutters border-top-btn">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 border-right-btn">
                                                @php
                                                $btnCompare = 'can-btnRemoveCompare';
                                                $btntext = 'Remove Compare';
                                                if(!in_array($recentValue->candidate->CANDIDATEID,$addTocompare)){
                                                $btnCompare = 'can-btnCompare';
                                                $btntext = 'Add to Compare';
                                                }
                                                @endphp

                                                <a title="{{$btntext}}" href="javascript:;"
                                                    data-id="{{$recentValue->candidate->CANDIDATEID}}"
                                                    data-candidate-id="{{$recentValue->candidate->id}}"
                                                    data-reload="false"
                                                    class="btn btn-custom-trans colorBlue btn-block btnCompare {{$btnCompare}} addcompare-icon ">{{$btntext}}</a>
                                                <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading"
                                                    style="display: none;">
                                            </div>
                                            <!-- Request profile -->
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
	                                            <a title="Request Profile"
	                                                href="{{route('request-candidate-profile',[$wishlistValue->candidate->id])}}"
	                                                class="btn btn-custom-trans colorRed btn-block btnProfilerequest">Request
	                                                Profile
	                                            </a>
                                            </div>
                                            <!-- Remove wishlist -->
                                            <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                @if(in_array($recentValue->candidate->id,$wishlist))
                                                <a title="Remove Wishlist" href="javascript:;"
                                                    class="btn btn-secondary btn-block btnSave btn-save-redIcon remove-to-wish-list-con colorRed btn-custom-trans"
                                                    data-id="{{$recentValue->candidate->CANDIDATEID}}"
                                                    data-candidate-id="{{$recentValue->candidate->id}}"
                                                    data-reloadtab="true">Remove Wishlist Hello</a>
                                                @else
                                                <a title="Save to Wishlist" href="javascript:void(0)"
                                                    class="btn btn-custom-trans colorRed  btn-block btnSave btn-save-redIcon save-to-wish-list"
                                                    data-id="{{$recentValue->candidate->CANDIDATEID}}"
                                                    data-candidate-id="{{$recentValue->candidate->id}}"
                                                    data-reload="true" data-reloadtab="true">Save to Wishlist</a>
                                                @endif
                                                <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading"
                                                    style="display: none;">
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @if(count($notifications))
        <div class="commanContent contentNotifications">
            <h6>Notifications</h6>
            <ul class="listContractHistory">
                @foreach($notifications as $notificationKey => $notoficationValue)
                @if($notificationKey < 3) <li>
                    <div class="row rowHistory align-items-center">
                        <div class="col-md-12">
                            <span class="textDate">{{date('d M. Y',strtotime($notoficationValue->created_at))}}</span>
                            @if($notoficationValue->redirect_url != null)
                            <a href="{{$notoficationValue->redirect_url}}">
                                <p>{{$notoficationValue->message}}</p>
                            </a>
                            @else
                            <p>{{$notoficationValue->message}}</p>
                            @endif
                        </div>
                    </div>
                    </li>
                    @endif
                    @endforeach
            </ul>
            <div class="boxbtns">
                <div class="row no-gutters">
                    <div class="col-md-12 text-center">
                        <a title="View all" href="{{route('client-notifications')}}"
                            class="btn btn-secondary mb-20 cus-btn-blue">View all</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="commanContent border-ra-5 pd-0">
            <h3 class="header-red-bg colorWhite text-center pd-5 how-much-left">Doing Your Budget?</h3>
            <div class="boxHowmuch">
                <div class="contentHowmuch">
                    <p class="pb-1">Enter Base Rate</p>
                    <div class="text-red inlineText">
                        <!-- <span class="cus-br-icon">BR</span> -->
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                    </div>
                    <input type="text" class="text-red text-right" value="" id="c3-base-rate-perday" maxlength="10" />
                    <span class="text-red">/day</span>
                </div>
                <div class="row f-12 rs-5">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <p class="pb-2">Length of contract
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5
                                months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="2" title="6-11 months"
                                    checked="" /> 6-11 months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                                months</li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <p class="pb-2">Fees</p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                    checked="" disabled="" /> Admin fee
                                <a href="javascript:void(0)" class="iconInfo">
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                            <li>
                                <input type="checkbox" class="fees" name="professional_fee" title="Professional fee"
                                    id="professional_fee" checked="" /> Professional fee
                                <a href="javascript:void(0)" class="iconInfo">
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="boxDoyouKnow mb-0">
                    <h6 class="f-14 mb-0">The Billing Rate is</h6>
                    <h2 class="text-red" id="billing-rate">MYR 0.00/day</h2>
                </div>
            </div>
        </div>
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
@endsection