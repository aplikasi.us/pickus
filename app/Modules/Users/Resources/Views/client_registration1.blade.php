@extends('Layouts.General.base_new')
@section('content')
@section('title','Consultant Registration')
<article class="client-page main-client-page">
    <section class="clientsection1 ">
        <div class="container">
            <div class="row welcome-text">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <h2>welcome <span>To The Draft</span></h2>
                    <p>Congratulations you are one step closer to being a <span>superstar! </span>we draft only the best. Are you one of them?</p>
                </div>
                <div class="col-6 col-sm-6 col-md-5 col-lg-5">
                    <div class="row consultant_area">
                            @if(!Auth::check())
                            <div class="col-12 col-sm-6 col-md-6 col-lg-5">
                                {{-- <a href="{{route('consultant-registration')}}" class="btn cons-reg" title="Make Me A Superstar"><p>Make Me A Superstar</p></a> --}}
                                <button onclick="location.href='{{route('new-consultant-registration')}}'" class="btn btn-blueone btn-landing"><p>Make me a Superstar</p></button>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-5">
                                {{-- <a href="{{route('client-registration')}}" class="btn about-aplikasi" title="Make Me A Superstar"><p>Hold on, what is aplikasi.us?</p></a> --}}
                                <button onclick="location.href='{{route('aboutus')}}'" class="btn btn-redone btn-landing"><p>Hold on, what is pickus.io?</p></button>
                            </div>
                            @endif
                    </div>
                </div>
            </div>
            <div class="row drafted-row">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 drafted-text">
                    <h2><span>Drafted? This Is New To Me!</span></h2>
                    <p>Embarking on a new road few have traversed, it is never easy for independent SAP consultants such as ourselves to look for the job that fits us well and somebody to represent our interest sincerely. The struggle is real and we understand it all too well. That’s why we have this platform especially for all of us. If you share the same feeling, come and  join us here, where we are striving everyday to make your life easier.</p>
                    <a href="javascript:;" title="I’M GEARED UP AND READY TO GO!" class="btn cons-bottom-form">I’M GEARED UP AND READY TO GO! <img src="{{admin_asset('images/arrow-down.png')}}" alt="" title=""></a>
                </div>
            </div>
    </section>
    <section class="clientsection2">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5"></div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 heading-drafted"><h2><span>Perks Of Being</span>Drafted</h2></div>
            </div>
            <ul class="listSteps">
                <li>
                    <div class="col-left"></div>
                    <div class="col-right"> 
                        <h6>connect</h6>
                        <p><span>with a large pool of peers</span></p>
                        <span class="img-connect1"><img src="{{admin_asset('images/img-connect1.png')}}" alt="" title=""></span>
                        <span class="img-connect2"><img src="{{admin_asset('images/img-connect2.png')}}" alt="" title=""></span>
                    </div>
                </li>
                <li>    
                    <div class="col-left">
                        <h6>Opportunity</h6>
                        <p><span>chance to work with reputable companies</span></p>
                        <span class="img-opportunity1"><img src="{{admin_asset('images/img-opportunity1.png')}}" alt="" title=""></span>
                    </div>
                    <div class="col-right"> 
                    </div>
                </li>
                <li>    
                    <div class="col-left">  
                        <span class="img-work-play2"><img src="{{admin_asset('images/img-work-play2')}}.png" alt="" title=""></span>
                    </div>
                    <div class="col-right">
                        <h6>work & play</h6>
                        <p><span>in a virtual office</span></p>
                        <span class="img-work-play"><img src="{{admin_asset('images/img-work-play')}}.png" alt="" title=""></span>
                    </div>
                </li>
                <li>    
                    <div class="col-left">
                        <h6>insurance plan</h6>
                        <p><span>access to affordable plan</span></p>
                        <span class="img-insurance-plan"><img src="{{admin_asset('images/img-insurance-plan')}}.png" alt="" title=""></span>
                    </div>
                    <div class="col-right"> 
                        <span class="img-insurance-plan2"><img src="{{admin_asset('images/img-insurance-plan2')}}.png" alt="" title=""></span>
                    </div>
                </li>
                <li>    

                    <div class="col-left">  
                        <span class="img-everything-taken1"><img src="{{admin_asset('images/img-connect2.png')}}" alt="" title=""></span>   
                    </div>
                    <div class="col-right everything-taken-box">
                        <h6>everything that is taken care of</span></h6>
                        <ul class="everything-list">
                            <li><span>Documentations</span></li>
                            <li><span>bureaucracies</span></li>
                            <li><span>paperworks</span></li>
                            <li><span>invoices</span></li>
                        </ul>
                        <span class="img-everything-taken"><img src="{{admin_asset('images/img-everything-taken')}}.png" alt="" title=""></span>
                    </div>
                </li>
                <li>    
                    <div class="col-left">
                        <h6>fast payment</h6>
                        <p><span>fast and hassle free</span></p>
                        <span class="img-fast-payment"><img src="{{admin_asset('images/img-fast-payment')}}.png" alt="" title=""></span>
                    </div>
                    <div class="col-right"> 
                        <span class="img-fast-payment1"><img src="{{admin_asset('images/img-fast-payment1')}}.png" alt="" title=""></span>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <section class="clientsection3">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <h2><span>Our <img class="img-our-services" src="{{admin_asset('images/img3star.png')}}" alt="" title=""></br> Services</span></h2>
                </div>
                <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8"><span class="img-services"><img src="{{admin_asset('images/img-services.png')}}" alt="" title=""></span></div>
            </div>
            <div class="services-box">
                <ul class="services-list">
                    <li>
                        <span class="img-payroll-hosting"><img src="{{admin_asset('images/img-payroll-hosting')}}.png" alt="" title=""></span>  
                        <h5>payroll <span> hosting</span></h5>
                    </li>                       
                    <li>
                        <span class="img-virtual-office"><img src="{{admin_asset('images/img-virtual-office')}}.png" alt="" title=""></span>    
                        <h5>Virtual <span> Office</span></h5>   
                    </li>
                    <li>
                        <span class="img-medical-concierge"><img src="{{admin_asset('images/img-medical-concierge')}}.png" alt="" title=""></span>  
                        <h5>medical <span>  concierge</span></h5>   
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="clientsection4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <h2><span> What People</br> Say <img class="img-our-services" src="{{admin_asset('images/img-our-services.png')}}" alt="" title=""> </span></h2>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 box-testimonial">
                    <div class="inner-testimonial">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active"> 
                               <p class="testimonyText"> “SAP jobs are not advertised that easily like other jobs. That constant worry of not having a project after my current one will always be one of the biggest downside in this line of work. us however, connects me to some of the biggest clients. I can find another job easily through them.”</p>
                               <p class="testimonyText">H.L. Wong</p>
                               <p class="testimonyText">SAP ABAP Consultant</p>
                                </div>
                                
                            </div>
                            <a class="carousel-control-prev" style="visibility:hidden" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" style="visibility:hidden" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div></div>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="clientsection5">
        <div class="container">
            <form role="form" method="post" action="{{route('consultant-registration')}}" id="consultant-registration" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <!-- <div class="admission-ticket-box">
                            <div class="heading-box"><h2>Admission Ticket</h2></div>
                            <div class="box-Pumped-up">
                                <div class="inner-box-Pumped-up">
                                    <img src="{{admin_asset('images/img-admission-ticket')}}.png"/>
                                    <h3>Make Me A </br>Superstar</h3>
                                    <img class="barcode-img" src="{{admin_asset('images/img-barcode.png')}}"/>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 contact-form">
                        <div class="inner-contact-box">
                            <h2>Ready To Get</br> <span>Drafted</span>?</h2>
                            <!-- <form id="contact-form" name="contact-form" action="mail.php" method="POST"> -->
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="hidden" name="role_id" value="2" />
                                    <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="Email Address">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="Phone" name="phone" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="Job-title" name="sap_job_title" class="form-control" placeholder="SAP Job Title">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <select style="font-family:Futura PT Book" name="hear" class="form-control input-sm">
                                        <option value="">How Did You Hear About aplikasi.us?</option>
                                        <option value="Social Network">Social Network</option>
                                        <option value="Search Engine">Search Engine</option>
                                        <option value="Event">Event</option>
                                        <option value="Blog">Blog</option>
                                        <option value="Advertisement">Advertisement</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 box-browse-cv">
                                    <span>Browse CV<input type="file" id="cv" name="cv" class="form-control" placeholder="Browse Cv"></a></span>
                                    <div class="cv-append-html"><div class="in-html-append"></div></div>
                                    <label class="file-note">Please select only docx or pdf and select file size below 2 mb</label>
                                </div>
                                <div class="col-6 col-md-6  text-right">
                                    <div class="custom-control custom-radio">
                                        <input type="checkbox" id="customRadio1" name="t_n_c" class="custom-control-input">
                                        <label class="custom-control-label" style="font-family:Futura PT Book" for="customRadio1">I Agree to the <a href="{{route('privacyPolicy')}}"  target="_blank">Privacy Policy</a> and <a href="{{route('termsConditions')}}" target="_blank">Terms of Service</a></label>
                                    </div>
                                </div>
                            </div> 

                            <button type="submit" class="btn btn-postion-bottom" title="Get Started"><img src="{{admin_asset('images/get-start-img')}}.png"/> Get Started <img src="{{admin_asset('images/get-start-img')}}.png"/></button>
                            <!-- </form> -->
                        </div>                          
                    </div>
                </div>
            </form>
        </div>
    </section>
</article> --}}
<!-- <section class="sap-consultants-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="panel panel-default" style="padding-top: 50px;padding-bottom: 50px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Join us now!</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="{{route('consultant-registration')}}"
                            id="consultant-registration" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" name="role_id" value="2" />
                                        <input type="text" name="first_name" id="first_name"
                                            class="form-control input-sm" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm"
                                            placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="email" id="email" class="form-control input-sm"
                                            placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="phone" id="phone" class="form-control input-sm"
                                            placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="sap_job_title" id="sap_job_title"
                                            class="form-control input-sm" placeholder="Sap Job Title">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select name="hear" class="form-control input-sm">
                                            <option value="">How did you hear about Aplikasi?</option>
                                            <option value="Social Network">Social Network</option>
                                            <option value="Search Engine">Search Engine</option>
                                            <option value="Event">Event</option>
                                            <option value="Blog">Blog</option>
                                            <option value="Advertisement">Advertisement</option>
                                            <option value="Friend">Friend</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="file" name="cv" id="cv" class="form-control input-sm"
                                            placeholder="Browse CV">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="checkbox" name="t_n_c" id="t_n_c" class="input-sm">I agree to the
                                        Privacy Policy and Terms & Conditions
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2 col-sm-2 col-md-2 pull-right">
                                    <input type="submit" value="Register" class="btn btn-block btn-success">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
@endsection