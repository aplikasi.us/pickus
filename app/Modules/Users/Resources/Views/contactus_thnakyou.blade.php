@extends('Layouts.General.base_new')
@section('content')
@section('title','Contact Us Thank You')
<article class="thankyoupage min-height-auto" >
    <section class="thankyousection1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="imgThankElement1">
                        <img src="{{admin_asset('images/Artboard-2.png')}}" alt="" title="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="text-center">
                        <div class="d-block text-center">
                            <img class="mb-3" src="{{admin_asset('images/logo.png')}}">
                        </div>
                        <div class="border-top-botom">
                            <h2 class="colorWhite font-weight-bold">Thank you for contacting us!</h2>
                            <p class="colorWhite">We appreciate that you’ve taken the time to write us. We’ll get back to you very soon. Please come back and see us often.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="imgThankElement2">
                        <img src="{{admin_asset('images/Artboard-3.png')}}" alt="" title="">
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection