@extends('Layouts.Client.base_new')
@section('content')
@section('title','Successful Hires')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
    @endif
        <!-- <p class="pb-3">
            <a href="{{route('client-home')}}" title="Next" class="btnBack"><img src="{{admin_asset('images/icon-back.png')}}" alt="" title=""> Back</a>
        </p> -->
        <div class="tabsInterviews">
            <h2 class="successful-hire-label">SUCCESSFUL HIRES</h2>
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#current" role="tab" aria-selected="true">Current</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#past" role="tab" aria-selected="false">Past</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active show" id="current">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th scope="col">Candidate ID</th>
                                <th scope="col">Candidate Name</th>
                                <th scope="col">Job ID</th>
                                <th scope="col">Job Title</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($current))
                            @foreach($current as $currentKey => $currentValue)
                            <tr>
                                <td>
                                    @if($currentValue->candidate != null)
                                    <a href="{{route('consultant-detail',[\Crypt::encryptString($currentValue->candidate->CANDIDATEID)])}}">{{$currentValue->candidate->candidate_id}}</a>
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                                <td>
                                    @if($currentValue->candidate != null)
                                    {{$currentValue->candidate->first_name .' '.$currentValue->candidate->last_name}}
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                                <td>
                                    @if($currentValue->job != null)
                                    <a href="{{route('job-details',[\Crypt::encryptString($currentValue->job->id)])}}">{{$currentValue->job->Job_ID}}</a>
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                                <td>
                                    @if($currentValue->job != null)
                                    {{$currentValue->job->posting_title}}
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4" class="text-red text-center">No data found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>	
                <div class="tab-pane fade" id="past">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th scope="col">Candidate ID</th>
                                <th scope="col">Candidate Name</th>
                                <th scope="col">Job ID</th>
                                <th scope="col">Job Title</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($past))
                            @foreach($past as $pastKey => $pastValue)
                            <tr>
                                <td>
                                    @if($pastValue->candidate != null)
                                    <a href="{{route('consultant-detail',[\Crypt::encryptString($pastValue->candidate->CANDIDATEID)])}}">{{$pastValue->candidate->candidate_id}}</a>
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                                <td>
                                    @if($pastValue->candidate != null)
                                    {{$pastValue->candidate->first_name .' '.$pastValue->candidate->last_name}}
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                                <td>
                                    @if($pastValue->job != null)
                                    <a href="{{route('job-details',[\Crypt::encryptString($pastValue->job->id)])}}">{{$pastValue->job->Job_ID}}</a>
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                                <td>
                                    @if($pastValue->job != null)
                                    {{$pastValue->job->posting_title}}
                                    @else
                                    {{'-'}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4" class="text-red text-center">No data found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>	
                </div>
            </div>	
        </div>	
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>
@endsection