@extends('Layouts.General.base_new')
@section('content')
@section('title','Client-Registration')
<article class="client-page consultant-Page">
    
<section class="clientsection1 ">
        <div class="container">
            <div class="row welcome-text">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <h2>welcome <span>To The Draft</span></h2>
                    <p>the window is open and it is time to get your <span>superstars!</span></p>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                </div>
                <div class="col-6 col-sm-6 col-md-5 col-lg-12">
                    <div class="row client_area">
                        @if(!Auth::check())
                        <div class="col-12 col-sm-6 col-md-6 col-lg-5">
                            {{-- <a href="{{route('consultant-registration')}}" class="btn cons-reg" title="Make Me A Superstar"><p>Make Me A Superstar</p></a> --}}
                            <button onclick="location.href='{{route('new-client-registration')}}'" class="btn btn-yellowone btn-landing"><p>Post jobs now - it’s free!</p></button>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-5">
                            {{-- <a href="{{route('client-registration')}}" class="btn about-aplikasi" title="Make Me A Superstar"><p>Hold on, what is aplikasi.us?</p></a> --}}
                            <button onclick="location.href='{{route('aboutus')}}'" class="btn btn-redone1 btn-landing"><p>Hold on, what is pickus.io?</p></button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row row-rafted">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6"></div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 drafted-text">
                <h2><span>Drafting Sap Consultants? </span></h2>
                <p>You heard it. Let’s face it, SAP consultants are very important for our projects, for our gameplans. Having sub-par players will satisfy your budget, but it can never win you the game. So, what can you do? That’s right, you draft the best ones you can get out there. And we will help you do just that.</p>
                <p>It means that you can look at all our superstars here, what they will cost you, their proficiency, and you  can even make your own comparison here. Because at the end of the day, you are the one who will be getting your players and they will either make you or break you. Us? Well, we’re just here to make your life easier.</p>
                <a href="#" title="I’M GEARED UP AND READY TO GO!" class="btn client-bottom-form">LET’S GET ‘EM <i class="fa fa-angle-double-down"></i></a>
            </div>
            </div>
        </div>
    </section>
    <section class="clientsection2">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7"><h2><span>Why Draft With</span>aplikasi.us</h2></div>
                <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5"></div>
            </div>
            <ul class="listSteps">
                <li>
                    <div class="col-left">
                        <h6>Excellent Superstars</h6>
                        <p><span>To choose from </span></p>
                        <span class="img-excellent-superstars1"><img src="{{admin_asset('images/img-excellent-superstars1')}}.png" alt="" title=""></span>
                        <span class="img-excellent-superstars2"><img src="{{admin_asset('images/img-excellent-superstars2')}}.png" alt="" title=""></span>
                    </div>
                <div class="col-right"><span class="img-excellent-superstars"><img src="{{admin_asset('images/img-excellent-superstars')}}.png" alt="" title=""></span></div>
                </li>
                <li>    
                    <div class="col-left"></div>
                <div class="col-right"> 
                    <h6>Fast & Reliable</h6>
                    <p><span>Talent Matching </span></p>
                    <span class="img-fast-reliable"><img src="{{admin_asset('images/img-fast-reliable')}}.png" alt="" title=""></span>
                </div>
                </li>
                <li>    
                    <div class="col-left">  
                        <h6>low & transparent</br> margin</h6>
                        <span class="img-low-transparent-margin"><img src="{{admin_asset('images/img-low-transparent')}}-margin.png" alt="" title=""></span>
                    </div>
                    <div class="col-right"><span class="img-low-transparent-margin1"><img src="{{admin_asset('images/img-low-transparent')}}-margin1.png" alt="" title=""></span></div>
                </li>
                <li>    
                    <div class="col-left"><span class="img-sap-job-posting"><img src="{{admin_asset('images/img-sap-job')}}-posting.png" alt="" title=""></span></div>
                <div class="col-right"> 
                    <h6>free sap </br>job posting</h6>
                    <span class="img-free-sap-job-posting"><img src="{{admin_asset('images/img-free-sap')}}-job-posting.png" alt="" title=""></span>
                </div>
                </li>
                <li>    
                <div class="col-left">
                    <h6>everything that </br>is taken care of</span></h6>
                        <ul class="everything-list">
                            <li><span>Documentations</span></li>
                            <li><span>bureaucracies</span></li>
                            <li><span>paperworks</span></li>
                            <li><span>invoices</span></li>
                        </ul><span class="img-everything-that-taken"><img src="{{admin_asset('images/img-everything-that')}}-taken.png" alt="" title=""></span></div>
                    <div class="col-right"><span class="img-everything-that-taken1"><img src="{{admin_asset('images/img-everything-that')}}-taken1.png" alt="" title=""></span> </div>
                </li>
                <li>    
                    <div class="col-left"></div>
                <div class="col-right"> 
                        <h6>Customisable</h6>
                    <p><span>Training Module</span></p>
                    <span class="img-customisable"><img src="{{admin_asset('images/img-customisable.png')}}" alt="" title=""></span>
                </div>
                
                </li>
            </ul>
        </div>
    </section>
    <section class="clientsection3">
        <div class="container">
            <div class="row rowServicesconsultant">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7"><span class="consultant-our-services"><img src="{{admin_asset('images/consultant-our-services')}}.png" alt="" title=""></span></div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                    <h2><span>Our <img class="img-our-services" src="{{admin_asset('images/img3star.png')}}" alt="" title=""></br> Services</span></h2>
                </div>
            </div>
            <div class="services-box">
                <ul class="services-list">
                    <li>
                    <span class="img-superstar-drafting"><img src="{{admin_asset('images/img-superstar-drafting')}}.png" alt="" title=""></span>    
                    <h5>Superstar <span> Drafting</span></h5>
                    </li>                       
                    <li>
                    <span class="img-sap-consulting"><img src="{{admin_asset('images/img-sap-consulting')}}.png" alt="" title=""></span>    
                    <h5>Sap <span> Consulting</span></h5>   
                    </li>
                    <li>
                    <span class="img-bespoke-training-module"><img src="{{admin_asset('images/img-bespoke-training')}}-module.png" alt="" title=""></span>  
                    <h5>Bespoke <span> Training Module</span></h5>  
                    </li>
                </ul>
            </div>
        </div>
        <div class="clientsectiontestimonial">
            <div class="container">
                <div class="row mobile-reveserv">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 box-testimonial">
                    <div class="inner-testimonial">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active"> 
                            <p style="font-family:Futura PT Book">"I have known Aplikasi for some years now and I have to say they have managed to fulfill my consultancy needs by giving the best local talents out there. They are flexible, transparent and above all, thorough. I had excellent experience working with them and I look forward for future collaborations." </p>
                            <p style="font-family:Futura PT Book">I. Tan,</p>
                            <p style="font-family:Futura PT Book">SAP Resourcing Manager, HCL</p>
                            </div>
                            
                    </div>
                <a class="carousel-control-prev" style="visibility:hidden" href="#carouselExampleIndicators" role="button" data-slide="prev"> 
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                     <a class="carousel-control-next" style="visibility:hidden" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div></div></div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-right heading-services mb-5">
                        <h2><span> What People</br> Say <img class="img-our-services" src="{{admin_asset('images/img-our-services')}}.png" alt="" title=""> </span></h2>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="clientsection5">
        {{-- <div class="container">

            <form method="post" action="{{route('client-registration')}}" id="consultant-registration" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <!-- <div class="admission-ticket-box">
                            <div class="heading-box"><h2>Admission Ticket</h2></div>
                            <div class="box-Pumped-up">
                                <div class="inner-box-Pumped-up">
                                <img src="{{admin_asset('images/img-admission-ticket')}}.png"/>
                                <h3>i,M Pumped Up!</h3>
                                <img class="barcode-img" src="{{admin_asset('images/qr-code.png')}}"/>
                            </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 contact-form">
                        <div class="inner-contact-box client-form">
                            <h2 class="double-br-wid">Ready To Start</br> Your <span>Season?</span></h2>
                            <div class="row">
                                <div class="col-6 col-md-6">
                                    <input type="hidden" name="role_id" value="1" />
                                    <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name">
                                </div>
                                <div class="col-6 col-md-6">
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6">
                                    <input type="hidden" name="role_id" value="1" />
                                    <input type="text" id="company_name" name="company_name" class="form-control" placeholder="Company">
                                </div>
                                <div class="col-6 col-md-6">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-6 col-md-6">
                                    <input type="text" id="Phone" name="phone" class="form-control" placeholder="Phone Number">
                                </div>
                                <div class="col-6 col-md-6">
                                    <input type="text" id="Job-title" name="job_title" class="form-control" placeholder="Job Title">
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-6 col-md-6">
                                    <select name="hear" class="form-control">
                                        <option value="">How Did You Hear About aplikasi.us?</option>
                                        <option value="Social Network">Social Network</option>
                                        <option value="Search Engine">Search Engine</option>
                                        <option value="Event">Event</option>
                                        <option value="Blog">Blog</option>
                                        <option value="Advertisement">Advertisement</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="col-6 col-md-6  text-left">
                                    <div class="custom-control custom-radio">
                                        <input type="checkbox" id="customRadio1" name="t_n_c" class="custom-control-input">
                                        <label style="font-family:Futura PT Book" class="custom-control-label" for="customRadio1">I Agree to the <a href="{{route('privacyPolicy')}}" target="_blank">Privacy Policy</a> and <a href="{{route('termsConditions')}}" target="_blank">Terms of Service</a></label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-postion-bottom" title="Get Started"><img src="{{admin_asset('images/get-start-img')}}.png"/> Get Started <img src="{{admin_asset('images/get-start-img')}}.png"/></button>
                            <!-- </form> -->
                        </div>                          
                    </div>
                   
                </div>
            </form>
        </div> --}}
    </section>
</article>
@endsection