@extends('Layouts.General.base')
@section('content')
@section('title','Reset Password')
<section class="secJoinus">
    <div class="container">
        <h3 class="text-center pb-1">Reset Your Password</h3>
        <div class="boxForm pt-4">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <form method="post" method="post" action="{{route('getresetpassword',[$id])}}" id="reset-password">
                        {{csrf_field()}}
                        <h6 class="text-red">Once you reset your password, your account will be verified successfully!</h6>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Password: </label>
                                    <input type="password" name="password" class="form-control" placeholder=" Enter password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Confirm Password: </label>
                                    <input type="password" name="confirm_password" class="form-control" placeholder=" Re-enter your password">
                                </div>
                            </div>
                        </div>	
                        <input type="submit" class="btn btn-block btn-danger" value="RESET NOW!"/>			
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection