@extends('Layouts.Client.base_new')
@section('content')
@section('title','Client-Notifications')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 mt-3">
        <h2 class="text-uppercase Black">Notifications</h2>
        <div class="commanContent contentNotifications mainNotifications border-ra-5">
            @if(count($notifications))
            <ul class="listContractHistory">
                @foreach($notifications as $notificationKey => $notoficationValue)
                
                <li>
                    <div class="row rowHistory align-items-center">
                        <div class="col-md-12">
                            <span class="textDate">{{date('d M. Y',strtotime($notoficationValue->created_at))}}</span>
                            @if($notoficationValue->redirect_url != null)
                            <a href="{{$notoficationValue->redirect_url}}"><p>{{$notoficationValue->message}}</p></a>
                            @else
                            <p>{{$notoficationValue->message}}</p>
                            @endif
                        </div>
                    </div>
                </li>
                @endforeach          
            </ul>
            {{$notifications->links()}}
            @else
            <span class="text-red">No notifications found</span>
            @endif
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @include('Layouts.General.recentlyviewed')
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
@endsection