@extends('Layouts.Client.base_new')
@section('content')
@section('title','Client-Notifications')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif

        <h2>Comparison</h2>
        <div class="commanContent contentNotifications mainNotifications border-ra-5">
            <div class="row row-eq-height">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                    <table class="table-responsive table table-borderless compare-table text-left">
                        <thead>
                            <tr>
                                <th width="20%">ID</th>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <th width="25%">
                                    {{($value->candidate->candidate_id) ? $value->candidate->candidate_id : '-' }}</th>
                                @endforeach
                                @endif
                                @if(count($addTocompare) != 3)
                                <th width="25%" class="compare-rowspan" rowspan="6"><span class="opunvisible">Addtocompare</span></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="cus-iconBox cus-iconjobtitle">Job Title</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>{{($value->candidate->sap_job_title) ? $value->candidate->sap_job_title : '-' }}
                                </td>
                                @endforeach
                                @endif
                                @if(count($addTocompare) != 3)
                                <td class="compare-rowspan" rowspan="10">
                                    <div class="add-to-comaprerow">
                                        <a class="colorBlue " href="{{route('general-search')}}"> <i
                                                class="fas fa-exchange-alt"></i>
                                            <div>Add To Compare</div>
                                        </a>
                                    </div>
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td>
                                    <!-- <span class="cus-br-icon">BR</span> -->
                                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">  Base Rate</td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td class="colorRed">
                                    {{($value->candidate->base_rate) ? $value->candidate->base_rate.'/day' : '-'}}</td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="iconBox iconYears"> Yrs To Exp</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>{{($value->candidate->experience_in_years) ? $value->candidate->experience_in_years : '-'}}
                                </td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="iconBox iconDate">Date Of Availability</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>{{($value->candidate->availability_date) ? $value->candidate->availability_date : '-' }}
                                </td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="cus-iconBox cus-iconsnoticeperiod">Notice Period</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>{{($value->candidate->notice_period_days) ? $value->candidate->notice_period_days.' Days' : '-' }}
                                </td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="iconBox iconTime">Job Mode</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>
                                    @php
                                    if($value->candidate->full_time == 1 ){
                                    echo 'Full Time';
                                    }elseif($value->candidate->part_time == 1 ){
                                    echo 'Part Time';
                                    }else{
                                    echo '-';
                                    }
                                    @endphp
                                </td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="iconBox iconTravel">Travel</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>{{($value->candidate->willing_to_travel) ? 'Okay to Travel' : "-" }}</td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="iconBox iconProject">Job Type</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>@php
                                    if($value->candidate->project == 1 ){
                                    echo 'Project';
                                    }elseif($value->candidate->support == 1 ){
                                    echo 'Support';
                                    }else{
                                    echo '-';
                                    }
                                    @endphp</td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="iconBox iconplace">Locations</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>{{($value->candidate->state) ? $value->candidate->state : "-" }}</td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td><span class="cus-iconBox cus-iconskill">Skillset</span></td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                @php
                                $skills = [];
                                if(isset($value->candidate->skill_set)){
                                $skills = explode(",",$value->candidate->skill_set);
                                }
                                @endphp
                                <td>
                                    <ul class="client-listSkills client-listSkillsBig">
                                        @if(count($skills))
                                        @foreach($skills as $skillValue)
                                        <li>
                                            <a href="javascript:void(0)">{{$skillValue}}</a>
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </td>
                                @endforeach
                                @endif
                            </tr>
                            <tr>
                                <td>
                                </td>
                                @if(count($addTocompare))
                                @foreach($addTocompare as $key => $value)
                                <td>
                                    <div class="d-block text-center mb-10">
                                        @if(!in_array($value->CANDIDATEID,$wishlist))
                                        <a title="Save" href="javascript:;"
                                            class="btn btn-tp-bdr btnSave btn-save-redIcon save-to-wish-list colorRed "
                                            data-id="{{$value->candidate->CANDIDATEID}}"
                                            data-candidate-id="{{$value->candidate->id}}" data-reload=""
                                            data-reloadtab="">Save to Wishlist</a>
                                        @else
                                        <a title="Remove" href="javascript:;"
                                            class="btn btn-secondary btn-block btnSave btn-save-redIcon remove-to-wish-list-con colorRed btn-tp-bdr "
                                            data-id="{{$value->candidate->CANDIDATEID}}"
                                            data-candidate-id="{{$value->candidate->id}}" data-reloadtab="">Remove
                                            Wishlist</a>
                                        @endif
                                        <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading"
                                            style="display: none;">
                                    </div>
                                    <div class="d-block text-center ">
                                        <a href="javascript:;" class="btn com-btn-blue btn-block can-btnRemoveCompare"
                                            data-id="{{$value->CANDIDATEID}}" data-reload="true">Remove</a>
                                    </div>
                                </td>
                                @endforeach
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @include('Layouts.General.recentlyviewed')
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
@endsection