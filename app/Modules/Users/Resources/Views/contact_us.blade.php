@extends('Layouts.General.base_new')
@section('content')
@section('title','About Us')
<article class="pageConatct ">
    <!-- <section class="contactsection1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="contentContactTop">
                        <h2 class="headingContactus title-border-b">Get in touch with <img
                                src="{{admin_asset('images/contact-element-1.png')}}" alt=""
                                title="Championing Local" /> </h2>
                        <form method="post" action="{{route('contactusform')}}" class="form-traparent-field"
                            id="contact-us-form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="first_name" name="first_name" class="form-control"
                                            placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="last_name" name="last_name" class="form-control"
                                            placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="email" name="email" class="form-control"
                                            placeholder="Email Adrress (Username)">
                                    </div>
                                </div>
                                <div class="col-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="Phone" name="phone" class="form-control"
                                            placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label class="mb-2">Type :</label>
                                        <div class="select-icon-change">
                                            <select name="user_type" class="form-control">
                                                <option value="Client">Client</option>
                                                <option value="Consultant">Consultant</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <input type="text" id="subject" name="subject" class="form-control"
                                            placeholder="Subject">
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <textarea rows="7" cols="50" class="message" id="message" name="message"
                                            placeholder="Message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 text-right">
                                    <button type="submit" class="btn btn-red btn-pd-sum" title="Submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->
                <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="imgContactElement1">
                        <img src="{{admin_asset('images/contact-element-7.png')}}" alt="" title="" />
                    </div> -->
            <!-- </div>
        </div>
        </div>
    </section> -->
    <section class="contactsection1 ">
        <div class="container">
            <div class="row">
                <!-- <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                </div> -->
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-md-offset-3 col-lg-offset-3">
                    <h2 class="headingContactus mb-3">Contact <img src="{{admin_asset('images/contact-element-1.png')}}"
                            alt="" title="Championing Local" /></h2>
                    <!-- <div class="contact-text whiteColor text-right">
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nisi ut aliquip ex ea
                            commodo consequat. </p>
                    </div> -->
                    <div class="contactAddressBox">
                        <div class="contactAddressBoxInner">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="d-flex align-center cus-d-flex-cener">
                                        <div class="cus-contact-icon">
                                            <img src="{{admin_asset('images/contact-element-3.png')}}" alt=""
                                                title="Championing Local" />
                                        </div>
                                        <div class="cus-contact-content">
                                            <h5 class="colorWhite pb-2">Telephone</h5>
                                            <a href="tel:60350321896" class="colorWhite">+603-5032 1896</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="d-flex align-center cus-d-flex-cener">
                                        <div class="cus-contact-icon">
                                            <img src="{{admin_asset('images/contact-element-4.png')}}" alt=""
                                                title="Championing Local" />
                                        </div>
                                        <div class="cus-contact-content">
                                            <h5 class="colorWhite pb-2">Office Hours</h5>
                                            <p class="colorWhite">Monday to Friday 9.00 am to 6.00 pm GMT</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="d-flex align-center cus-d-flex-cener">
                                        <div class="cus-contact-icon">
                                            <img src="{{admin_asset('images/contact-element-5.png')}}" alt=""
                                                title="Championing Local" />
                                        </div>
                                        <div class="cus-contact-content">
                                            <h5 class="colorWhite pb-2">Email</h5>
                                            @if(Session::get('territory') == "MY")
                                            <a href="mailto:connect@aplikasi.us"
                                                class="colorWhite">connect@aplikasi.us</a>
                                             @else
                                             <a href="mailto:aplikasi.us@service101plus.com"
                                                class="colorWhite">aplikasi.us@service101plus.com</a>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="d-flex align-center cus-d-flex-cener">
                                        <div class="cus-contact-icon">
                                            <img src="{{admin_asset('images/contact-element-6.png')}}" alt=""
                                                title="Championing Local" />
                                        </div>
                                        <div class="cus-contact-content">
                                            <h5 class="colorWhite pb-2">Address</h5>
                                            @if(Session::get('territory') == "MY")
                <a href="https://goo.gl/maps/k5wzMCPF9WB2" style="color: white;" title="Kumpulan Aplikasi Sempurna Sdn Bhd 2-2B, Tower 9, UOA Business Park, 1 Jalan Pengaturcara U1/51A, 40150 Shah Alam, Selangor,
                   Malaysia" target="_blank">Kumpulan Aplikasi Sempurna Sdn Bhd</br> 2-2B, Tower 9, UOA Business Park,</br> 1
                    Jalan Pengaturcara U1/51A,</br> 40150 Shah Alam, Selangor,
                    Malaysia</a>
                    @else
                    <a href="https://goo.gl/maps/aTdSMyPNrSTiAqTA9" style="color: white;" title="Service 101 plus Consulting Inc, 1F Clock In BGC Bonifacio Technology Center. 31st Corner 2nd Avenue, Bonifacio Global City, Taguig City, Philippines 1634" target="_blank">Service 101 plus Consulting Inc.</br>1F Clock In BGC Bonifacio Technology</br>Center, 31st Corner 2nd Avenue,</br> Bonifacio Global City,Taguig City, Philippines 1634</a>
                    @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <p class="pt-4">Please note that call may be recorded for training and monitoring purposes.</p> -->
                </div>
            </div>
        </div>
    </section>
    <section class="contactsection3">
        <div class="conatct-map">
            @if(Session::get('territory') == "MY")
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.033824668795!2d101.58687775111453!3d3.085647454387286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4c4312080b19%3A0xefcee70b251c9eeb!2sKumpulan+Aplikasi+Sempurna+Sdn+Bhd!5e0!3m2!1sen!2sin!4v1554425005052!5m2!1sen!2sin"
                width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
                @else
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.7766910631954!2d121.04227281537312!3d14.554758782099244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c8fa01c2d577%3A0x5b48c24b8f021228!2sClock+In+BGC+at+Bonifacio+Technology+Center!5e0!3m2!1sen!2smy!4v1562566694771!5m2!1sen!2smy"
                width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
                @endif
        </div>
        </div>
    </section>
</article>
@endsection