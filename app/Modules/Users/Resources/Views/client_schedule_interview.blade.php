@extends('Layouts.Client.base')
@section('content')
@section('title','Client-Schedule Interview')
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    {{ Session::get('error') }}
</div>
@endif
<div class="row">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <h2>Schedule Interview For Job {{$job->posting_title}}</h2>
        <div class="tabsInterviews tabJobs">
            <div class="tab-content">
                <div class="boxForm pt-4">
                    <form method="post" action="{{route('schedule-interview',[\Crypt::encryptString($job->id)])}}" id="client-schedule-interview">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Interview Name</label>
                                    {{Form::text('name','',['class' => 'form-control'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Candidate Name</label>
                                    {{Form::select('candidate',[null => '--Select Candidate--'] + $candidates,'',['class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Posting Title</label>
                                    {{Form::text('posting_title',$job->posting_title,['class' => 'form-control','readonly' => true])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type</label>
                                    {{Form::select('type',[null => '--Select Type--'] + $interviewType,'',['class' => 'form-control'])}}
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Location</label>
                                    {{Form::text('location','',['class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>From</label>
                                    {{Form::text('from',"",['class' => 'form-control','readonly' => true])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>To</label>
                                    {{Form::text('to','',['class' => 'form-control','readonly' => true])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Reminder</label>
                                    {{Form::select('reminder',$interviewReminders,"",['class' => 'form-control','readonly' => true])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Territory</label>
                                    {{Form::select('territory',["None" => "None","MY" => "MY","PH" => "PH"],"None",['class' => 'form-control','readonly' => true])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Schedule Comments</label>
                                    {{Form::text('comment','',['class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                        <p class="text-right pb-0">
                            <a href="{{route('client-jobs')}}" class="btn btn-secondary btn-sm btn-submit">Cancel</a>
                            <input type="submit" value="Submit" class="btn btn-secondary btn-sm btn-submit">
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="commanContent boxHowmuch">
            <h3>Doing Your Budget?</h3>
            <div class="contentHowmuch">
                <p class="pb-1">Enter Base Rate</p>
                <div class=" text-red inlineText">
                    <!-- <span class="cus-br-icon">BR</span> MYR -->
                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                    <span class="ml-1">MYR </span>
                </div>
                <input type="text" class="text-red text-right" value="" id="c3-base-rate-perday"/>
                <span class="text-red">/day</span>
            </div>
            <div class="row f-12 rs-5">
                <div class="col-md-6">
                    <p class="pb-2">Length of contract
                        <a href="javascript:void(0)" class="iconInfo">
                            <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                        </a>
                    </p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5 months</li>
                        <li>
                            <input type="radio" name="months" class="months" value="2" title="6-11 months" checked=""/> 6-11 months</li>
                        <li>
                            <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12 months</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <p class="pb-2">Fees</p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee" checked=""/> Admin fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                        <li>
                            <input type="checkbox" class="fees" name="professional_fee" title="Professional fee" id="professional_fee" checked=""/> Professional fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="boxDoyouKnow mb-0">
                <h6 class="f-14 mb-0">The Billing Rate is</h6>
                <h2 class="text-red" id="billing-rate">MYR 0.00/day</h2>
            </div>
        </div>
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
@endsection