@extends('Layouts.General.base_new')
@section('content')
@section('title','About Us')
<article class="pageAbout ">
    <section class="aboutsection1 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="imgAboutElement1">
                        <img src="{{admin_asset('images/about_element1.png')}}" alt="" title="" />
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="contentAboutusTop">
                        <h2 class="headingWithus"><img src="{{admin_asset('images/textUs.png')}}" alt="" title="Championing Local" /> Redefining SAP</br> Resourcing</h2>
                        <p>We looked out of the window one day and thought, “isn&#39;t there a better way to do this?”. Where consultants don&#39;t have to deal with job uncertainties and to finally have someone who really represents them, and clients don&#39;t have to scratch their heads to look for bona fide consultants. There must be.</p>
                        <p>There must be a way to this more efficiently and effectively. So, here we are. After all the blood, sweat and tears, we present you, aplikasi.us, your one stop solution centre for all your problems.</p>
                        <div class="row">
                            @if(!Auth::check())
                            <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                                <a href="{{route('consultant-registration')}}" class="btn btn-darkgreen btn-home-s btn-block" title="Make Me A Superstar">Make Me A Superstar</a>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                                <a href="{{route('client-reg')}}" class="btn btn-orange btn-block btn-home-s" title="Make Me A Superstar">I Want To Be A Client</a>
                            </div>
                            @endif
                        </div>
                        <!-- <ul class="listAboutContent">
                            <li>World-class local talents</li>
                            <li>Internationally recognised<br/> SAP consultants </li>
                            <li>By locals, for locals</li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aboutsection2 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="contentAboutBeliever">
                        <h3>A Believer, Disruptor,<span> a  Game Changer</span></h3>
                        <p>Our team consists of a bunch of adept misfits who despite excessive playfulness, deliver with acute precision. Although we’re all different, everybody here is an important part of the whole machine; no matter how old or young we are. This is where talents are appreciated, knowledge is practised, and success is nurtured.</p>
                        <p>In just 3 years, we have grown exponentially and the family is now bigger, better, and stronger; and we couldn’t have done it without you. In recognition of that, we would like to offer our deepest gratitude to all of you who have been supportive and crazy enough to put your faith in us.</p>
                        <p>Now, it’s time to enjoy the fruits of your labour. Your trust hasn’t been wasted on us. Please continue to support us and we promise to reach a greater height.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="imgAboutElement2">
                        <img src="{{admin_asset('images/about_element2.png')}}" alt="" title="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aboutsection3 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 hideOnmobie">
                    <div class="imgAboutElement3">
                        <img src="{{admin_asset('images/about_element3.png')}}" alt="" title="" />
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="contentMissions">
                        <h3>Missions</h3>
                        <p>To deliver awesome services to our SAP consultants and clients so they can sit back and relax while we bring about
                            a fresh and new way of doing SAP talent representation through technology.</p>
                        <p>To fuel up best practices and boost business efficiency like a butter on a hot pan so they can soar past even the silver
                            lining by filling them with bespoke SAP Consultation.</p>
                    </div>
                    <div class="contentVisions">
                        <h3>Visions</h3>
                        <p>To be the place to go for SAP consultants and clients whenever they are looking for what they need and want and to be
                            recognised widely by the SAP Community.</p>
                        <p>To expand our revolutionary ways of doing things to other regions as well, effectively redefining how SAP talents are
                            managed and represented.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 showInmobie">
                    <div class="imgAboutElement3">
                        <img src="{{admin_asset('images/about_element3.png')}}" alt="" title="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aboutsection4 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="contentExpanding">
                        <h3>Expanding the Horizon</h3>
                        <p>Aplikasi had a moderate start. It was never meant to be this disruptive, so much so that it could shake the balance
                            in this business.</p>
                        <p>However, with the right idea, the right people and the right trajectory, Aplikasi has grown exponentially and continues
                            to exhibit constant excellence in just a few years.</p>
                        <p>Today, Aplikasi has span its wing and flown to the neighbouring countries as well. It is a manifestation of how it
                            has been recognised as the new future of SAP resourcing; an amalgamation and synergy between intelligent technology
                            and good-old human touch. </p>
                        <p>Will you be on the ship with us, or will you be watching from the sideline? We are going to a road no man can go,
                            no man has went. We are treading on a revolutionary journey, creating new paths, discovering new riches.</p>
                        <p>Will you take the leap with us?</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="imgAboutElement4">
                        <img src="{{admin_asset('images/about_element4.png')}}" alt="" title="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aboutsection5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="imgAboutElement5">
                        <img src="{{admin_asset('images/about_element5.png')}}" alt="" title="" />
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="contentMastermind">
                        <h3>Meet The Mastermind</h3>
                        <p>Meet Azlan, our beloved founder and friend. With more than 10 years of experience as an SAP consultant himself, he
                            is no stranger to this playing field. Azlan is visionary, so much so that we have to bring him back to the stratosphere
                            whenever he flew too high, But that what makes us so motivated to work everyday. He pushes us (in a good way) forward
                            and keeps us afloat.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection