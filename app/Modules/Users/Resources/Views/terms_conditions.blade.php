@extends('Layouts.General.base_new')
@section('content')
@section('title','Terms of Service')
<article class="pageprivacy bgLightGray">
    <section class="privacysection1 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="colorWhite boder-top-bot d-inline-block">Terms of Service</h2>
                </div>
            <div>
        </div>            
    </section>
    <section class="privacysection2 ">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="">
                        <p>
                            Please read these Terms of Service [collectively with KUMPULAN APLIKASI SEMPURNA SDN BHD.’s (“APLIKASI”) “Privacy Policy” and the “Terms of Service”] fully and carefully before using aplikasi.us/my (the “Site”) and the services, features, content or applications (whether from website or mobile applications) offered by APLIKASI (“we”, “us” or “our”) (together with the Site, the “Services”). Our third party consultants, vendors, partners or providers shall be described as “Provider”. These Terms of Service set forth the legally binding terms and conditions for your use of the Site and the Services. Acceptance of Terms of Service.
                        </p>
                        <p>
                            By registering for and/or using the Services in any manner, including but not limited to visiting or browsing the Site, you agree to these Terms of Service and all other operating rules, policies and procedures that may be published from time to time on the Site by us, each of which is incorporated by reference and each of which may be updated from time to time without notice to you.
                        </p>
                        <p>
                            Certain of the Services may be subject to additional terms and conditions specified by us from time to time; your use of such Services is subject to those additional terms and conditions, which are incorporated into these Terms of Service by this reference.
                        </p>
                        <p>
                            These Terms of Service apply to all users of the Services, including, without limitation, users who are contributors of content, information, and other materials or services, registered or otherwise.
                        </p>
                        <h3 class="Black">1. ELIGIBILITY</h3>
                        <p>
                            You represent and warrant that you are at least 18 years of age. If you are under age of 18, you may not, under any circumstances or for any reason, use the Services. We may, in our sole discretion, refuse to offer the Services to any person or entity and change its eligibility criteria at any time. You are solely responsible for ensuring that these Terms of Service are in compliance with all laws, rules and regulations applicable to you and the right to access the Services is revoked where these Terms of Service or use of the Services is prohibited or to the extent offering, sale or provision of the Services conflicts with any applicable law, rule or regulation. Further, the Services are offered only for your use, and not for the use or benefit of any third party.
                        </p>

                        <h3 class="Black">2. REGISTRATION</h3>
                        <p>
                            To sign up for the Services, you must register for an account on the Services (an “Account”). You must provide accurate and complete information and keep your Account information updated. You shall not:
                        </p>
                        <ol class="roman-number">
                            <li>
                                select or use as a username a name of another person with the intent to impersonate that person;
                            </li>
                            <li>
                                use as a username a name subject to any rights of a person other than you without appropriate authorization; or
                            </li>
                            <li>
                                use, as a username, a name that is otherwise offensive, vulgar or obscene. You are solely responsible for the activity that occurs on your Account, and for keeping your Account password secure. You may never use another person’s user account or registration information for the Services without permission. You must notify us immediately of any change in your eligibility to use the Services (including any changes to or revocation of any licenses from state authorities), breach of security or unauthorized use of your Account. You should never publish, distribute or post login information for your Account. You shall have the ability to delete your Account, either directly or through a request made to one of our employees or affiliates.
                            </li>
                        </ol>
                        <h3 class="Black">3. CONTENT</h3>
                        <h4 class="Black">Definition.</h4>
                        <p>
                            For purposes of these Terms of Service, the term “Content” includes, without limitation, information, data, text, photographs, videos, audio clips, written posts and comments, software, scripts, graphics, and interactive features generated, provided, or otherwise made accessible on or through the Services. For the purposes of this Agreement, “Content” also includes all User Content (as defined below).
                        </p>
                        <h4 class="Black">User Content.</h4>
                        <p>
                            All Content added, created, uploaded, submitted, distributed, or posted to the Services by users (collectively “User Content”), whether publicly posted or privately transmitted, is the sole responsibility of the person who originated such User Content. You represent that all User Content provided by you is accurate, complete, up-to-date, and in compliance with all applicable laws, rules and regulations. You acknowledge that all Content, including User Content, accessed by you using the Services is at your own risk and you will be solely responsible for any damage or loss to you or any other party resulting therefrom. We do not guarantee that any Content you access on or through the Services is or will continue to be accurate.
                        </p>
                        <h4 class="Black">Notices and Restrictions.</h4>
                        <p>
                            The Services may contain Content specifically provided by us, our partners or our users and such Content is protected by copyrights, trademarks, service marks, patents, trade secrets or other proprietary rights and laws. You shall abide by and maintain all copyright notices, information, and restrictions contained in any Content accessed through the Services.
                        </p>
                        <h4 class="Black">Use License.</h4>
                        <p>
                            Subject to these Terms of Service, we grant each user of the Services a worldwide, non-exclusive, non-sublicensable and non-transferable license to use (i.e., to download and display locally) Content solely for purposes of using the Services. Use, reproduction, modification, distribution or storage of any Content for other than purposes of using the Services is expressly prohibited without prior written permission from us. You shall not sell, license, rent, or otherwise use or exploit any Content for commercial use or in any way that violates any third party right.
                        </p>
                        <h4 class="Black">License Grant.</h4>
                        <p>
                            By submitting User Content through the Services, you hereby do and shall grant us a worldwide, non-exclusive, perpetual, royalty-free, fully paid, sublicensable and transferable license to use, edit, modify, truncate, aggregate, reproduce, distribute, prepare derivative works of, display, perform, and otherwise fully exploit the User Content in connection with the Site, the Services and our (and our successors’ and assigns’) businesses, including without limitation for promoting and redistributing part or all of the Site or the Services (and derivative works thereof) in any media formats and through any media channels (including, without limitation, third party websites and feeds), and including after your termination of your Account or the Services. You also hereby do and shall grant each user of the Site and/or the Services a non-exclusive, perpetual license to access your User Content through the Site and/or the Services, and to use, edit, modify, reproduce, distribute, prepare derivative works of, display and perform such User Content, including after your termination of your Account or the Services. For clarity, the foregoing license grants to us and our users does not affect your other ownership or license rights in your User Content, including the right to grant additional licenses to your User Content, unless otherwise agreed in writing. You represent and warrant that you have all rights to grant such licenses to us without infringement or violation of any third party rights, including without limitation, any privacy rights, publicity rights, copyrights, trademarks, contract rights, or any other intellectual property or proprietary rights.
                        </p>
                        <h4 class="Black">Availability of Content.</h4>
                        <p>
                            We do not guarantee that any Content will be made available on the Site or through the Services. We reserve the right to, but do not have any obligation to:
                        </p>
                        <ol class="roman-number">
                            <li>
                                remove, edit or modify any Content in our sole discretion, at any time, without notice to you and for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content or if we are concerned that you may have violated these Terms of Service), or for no reason at all; and
                            </li>
                            <li>
                                to remove or block any Content from the Services.
                            </li>
                        </ol>
                        <h3 class="Black">4. RULES OF CONDUCT</h3>
                        <p> 
                            As a condition of use, you promise not to use the Services for any purpose that is prohibited by these Terms of Service. You are responsible for all of your activity in connection with the Services.
                        </p>
                        <p>
                            You shall not (and shall not permit any third party to) either:
                        </p>
                        <ul class="alphabet-number">
                            <li>
                                take any action; or
                            </li>
                            <li>
                                upload, download, post, submit or otherwise distribute or facilitate distribution of any Content on or through the Service, including without limitation any User Content, that:
                                <ul class="ul-dash">
                                    <li>
                                        infringes any patent, trademark, trade secret, copyright, right of publicity or other right of any other person or entity or violates any law or contractual duty;
                                    </li>
                                    <li>
                                        you know is false, misleading, untruthful or inaccurate;
                                    </li>
                                    <li>
                                        is unlawful, threatening, abusive, harassing, defamatory, libelous, deceptive, fraudulent, invasive of another’s privacy, tortious, obscene, vulgar, pornographic, offensive, profane, contains or depicts nudity, contains or depicts sexual activity, or is otherwise inappropriate as determined by us in our sole discretion;
                                    </li>
                                    <li>
                                        constitutes unauthorized or unsolicited advertising, junk or bulk e-mail (“spamming”);
                                    </li>
                                    <li>
                                        contains software viruses or any other computer codes, files, or programs that are designed or intended to disrupt, damage, limit or interfere with the proper function of any software, hardware, or telecommunications equipment or to damage or obtain unauthorized access to any system, data, password or other information of ours or of any third party;
                                    </li>
                                    <li>
                                        impersonates any person or entity, including any of our employees or representatives; or
                                    </li>
                                    <li>
                                        includes anyone’s identification documents or sensitive financial information.
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <p>
                            You shall not:
                        </p>
                        <ol class="roman-number">
                            <li>
                                take any action that imposes or may impose (as determined by us in our sole discretion) an unreasonable or disproportionately large load on our (or our third party providers’) infrastructure;
                            </li>
                            <li>
                                interfere or attempt to interfere with the proper working of the Services or any activities conducted on the Services;
                            </li>
                            <li>
                                bypass, circumvent or attempt to bypass or circumvent any measures we may use to prevent or restrict access to the Services (or other accounts, computer systems or networks connected to the Services);
                            </li>
                            <li>
                                run any form of auto-responder or “spam” on the Services;
                            </li>
                            <li>
                                use manual or automated software, devices, or other processes to “crawl” or “spider” any page of the Site;
                            </li>
                            <li>
                                harvest or scrape any Content from the Services; or (vii) otherwise take any action in violation of our guidelines and policies.
                            </li>
                        </ol>
                        <p>
                            You shall not (directly or indirectly):
                        </p>
                        <ol class="roman-number">
                            <li>
                                decipher, decompile, disassemble, reverse engineer or otherwise attempt to derive any source code or underlying ideas or algorithms of any part of the Services (including without limitation any application), except to the limited extent applicable laws specifically prohibit such restriction,
                            </li>
                            <li>
                                modify, translate, or otherwise create derivative works of any part of the Services, or
                            </li>
                            <li>
                                copy, rent, lease, distribute, or otherwise transfer any of the rights that you receive hereunder. You shall abide by all applicable local, state, national and international laws and regulations.
                            </li>
                        </ol>
                        <p>
                            We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to:
                        </p>
                        <ol class="roman-number">
                            <li>
                                satisfy any applicable law, regulation, legal process or governmental request,
                            </li>
                            <li>
                                enforce these Terms of Service, including investigation of potential violations hereof,
                            </li>
                            <li>
                                detect, prevent, or otherwise address fraud, security or technical issues,
                            </li>
                            <li>
                                respond to user support requests, or
                            </li>
                            <li>
                                protect the rights, property or safety of us, our users and the public.
                            </li>
                        </ol>
                        <h3 class="Black">5. THIRD PARTY SERVICES</h3>
                        <p>    
                            The Services may permit you to link to other websites, services or resources on the Internet, and other websites, services or resources may contain links to the Services. When you access third party resources on the Internet, you do so at your own risk. These other resources are not under our control, and you acknowledge that we are not responsible or liable for the content, functions, accuracy, legality, appropriateness or any other aspect of such websites or resources. The inclusion of any such link does not imply our endorsement or any association between us and their operators. You further acknowledge and agree that we shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, goods or services available on or through any such website or resource.
                        </p>
                        <h3 class="Black">6. PAID SERVICES</h3>
                        <p>
                            Paid Services.
                        </p>
                        <p>
                            Certain of our Services may be subject to payments now or in the future (the “Paid Services”). Please note that any payment terms presented to you in the process of using or signing up for a Paid Service are deemed part of this Agreement. NOTWITHSTANDING the above, APLIKASI shall not be responsible for any unauthorised payment and/or paid services that is not within APLIKASI and/or aplikasi.us/aplikasi-2’s control. APLIKASI shall update its terms of payment accordingly to its users and/or clients.
                        </p>
                        <p>
                            Free Trials and Other Promotions.
                        </p>
                        <p>
                            Any free trial or other promotion that provides access to a Paid Service must be used within the specified time of the trial. You must stop using a Paid Service before the end of the trial period in order to avoid being charged for that Paid Service. If you cancel prior to the end of the trial period and are inadvertently charged for a Paid Service, please contact us at <a href="mailto:admin@aplikasi.us/aplikasi-2">admin@aplikasi.us/aplikasi-2</a>.
                        </p>
                        <h3 class="Black">7. TERMINATION.</h3>
                        <p>
                            We may terminate your access to all or any part of the Services at any time, with or without cause, with or without notice, effective immediately, which may result in the forfeiture and destruction of all information associated with your membership. If you wish to terminate your Account, you may do so by following the instructions on the Site or through the Services. Any fees paid hereunder are non-refundable. All provisions of these Terms of Service which by their nature should survive termination shall survive termination, including, without limitation, licenses of User Content, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
                        </p>
                        <h3 class="Black">8. TERMS OF USAGE</h3>
                        <h4 class="Black">General.</h4>
                            <p>
                                APLIKASI SHALL NOT BE RESPONSIBLE for any claims associated with the description of the Provider Offerings, Services, or Products. The package related to Provider Offerings, Products, Services, and other programs on the Site may change at any time at APLIKASI’ sole discretion without notice. A Provider (such as our Consultants) may advertise goods, services, or experiences on the Site, or with respect to Products, Services, or Offerings, supply items/services/activities to APLIKASI, that require Provider to have an up-to-date regulatory authorization, license, or certification. APLIKASI does not verify, validate, or collect evidence of any regulatory authorization, license, or certification from any Provider. You should make whatever investigation you deem necessary and appropriate before purchase of any Service, Product, or Offerings. Providers are solely responsible for the Services, Products, or Offerings being provided or the care and quality of those items. No monetary refund shall be issued towards you on the cancellation/termination of the particular Service, Product or Offerings by the Provider.
                            </p>
                        <h4 class="Black">Warranty Disclaimer.</h4>
                        <p>
                            We have no special relationship with or fiduciary duty to you. You acknowledge that APLIKASI has no duty to take any action regarding:
                        </p>
                            <ul class="ul-dash">
                                <li>
                                    which users gain access to the Services;
                                </li>
                                <li>
                                    what Content you access via the Services; or
                                </li>
                                <li>
                                    how you may interpret or use the Content.
                                </li>
                            </ul>
                        <p>
                            You release us from all liability for you having acquired or not acquired Content through the Services. We make no representations concerning any Content contained in or accessed through the Services, and we will not be responsible or liable for the accuracy, copyright compliance, or legality of material or Content contained in or accessed through the Services.
                        </p>
                        <p>
                            The services and content are provided “AS IS”, “AS AVAILABLE” AND WITHOUT WARRANTY of any kind, express or implied, including, but not limited to, the implied warranties of title, non-infringement, merchantability and fitness for a particular purpose, and any warranties implied by any course of performance or usage of trade, all of which are expressly disclaimed. we, and our directors, employees, agents, suppliers, partners and content providers do not warrant that:
                        </p>
                        <ol class="roman-number">
                            <li>
                                the services will be secure or available at any particular time or location;
                            </li>
                            <li>
                                any defects or errors will be corrected;
                            </li>
                            <li>
                                any content or software available at or through the services is free of viruses or other harmful components; or
                            </li>
                            <li>
                                the results of using the services will meet your requirements. your use of the services is solely at your own risk.
                            </li>
                        </ol>
                        <h3 class="Black">9. INDEMNIFICATION</h3>
                        <p>
                            You shall defend, indemnify, and hold harmless us, our affiliates and each of our and their respective employees, contractors, directors, suppliers and representatives from all liabilities, claims, and expenses, including reasonable attorneys’ fees, that arise from or relate to your use or misuse of, or access to, the Services, Content, or otherwise from your User Content, violation of these Terms of Service, or infringement by you, or any third party using your Account or identity in the Services, of any intellectual property or other right of any person or entity. We reserve the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will assist and cooperate with us in asserting any available defenses. You shall also indemnify us on any liabilities, personal injuries, claims and any legal action that arise from the activities occurred in the Provider’s premises.
                        </p>
                        <h3 class="Black">10. LIMITATION OF LIABILITY</h3>
                        <p>
                            In no event shall we, nor our directors, employees, agents, partners, suppliers or content providers, be liable under contract, tort, strict liability, negligence or any other legal or equitable theory with respect to the services:
                        </p>
                        <ol class="roman-number">
                            <li>
                                for any lost profits, data loss, cost of procurement of substitute goods or services, or special, indirect, incidental, punitive, compensatory or consequential damages of any kind whatsoever (however arising), or
                            </li>
                            <li>
                                (II) for any bugs, viruses, trojan horses, or the like (regardless of the source of origination).
                            </li>
                        </ol>
                        <h3 class="Black">11. ARBITRATION CLAUSE & CLASS ACTION WAIVER</h3>
                            <p>
                                Arbitration.
                            </p>
                            <p>
                                YOU AGREE THAT ALL DISPUTES BETWEEN YOU AND US (WHETHER OR NOT SUCH DISPUTE INVOLVES A THIRD PARTY) WITH REGARD TO YOUR RELATIONSHIP WITH US, INCLUDING WITHOUT LIMITATION DISPUTES RELATED TO THESE TERMS OF SERVICE, YOUR USE OF THE SERVICES, AND/OR RIGHTS OF PRIVACY AND/OR PUBLICITY, WILL BE RESOLVED BY BINDING, AS STATED UNDER THE MALAYSIAN ARBITRATION ACT 2005 (REVISED 2011); PROVIDED, HOWEVER, THAT TO THE EXTENT THAT YOU HAVE IN ANY MANNER VIOLATED OR THREATENED TO VIOLATE OUR INTELLECTUAL PROPERTY RIGHTS, WE MAY SEEK INJUNCTIVE OR OTHER APPROPRIATE RELIEF IN UNDER THE JURISDICTION OF HIGH COURT OF MALAYA, SABAH & SARAWAK ARE GENERALLY MORE LIMITED THAN IN A LAWSUIT, AND OTHER RIGHTS THAT YOU AND WE WOULD HAVE IN COURT MAY NOT BE AVAILABLE IN ARBITRATION. As an alternative, you may bring your claim in the Malaysian “small claims” court, if permitted by that small claims court’s rules and if within such court’s jurisdiction, unless such action is transferred, removed or appealed to a different court. You may bring claims only on your own behalf. Neither you nor we will participate in a class action or class-wide arbitration for any claims covered by this agreement to arbitrate. YOU ARE GIVING UP YOUR RIGHT TO PARTICIPATE AS A CLASS REPRESENTATIVE OR CLASS MEMBER ON ANY CLASS CLAIM YOU MAY HAVE AGAINST US INCLUDING ANY RIGHT TO CLASS ARBITRATION OR ANY CONSOLIDATION OF INDIVIDUAL ARBITRATIONS. You also agree not to participate in claims brought in a private attorney general or representative capacity, or consolidated claims involving another person’s account, if we are a party to the proceeding. This dispute resolution provision will be governed by the MALAYSIAN ARBITRATION ACT 2005 (REVISED 2011);. In the event the Kuala Lumpur Regional Centre Arbitration is unwilling or unable to set a hearing date within ninety (90) days of filing the case, then either we or you can elect to have the arbitration administered instead by the Kuala Lumpur Regional Centre Arbitration. Judgment on the award rendered by the arbitrator may be entered in any court having competent jurisdiction. Any provision of applicable law notwithstanding, the arbitrator will not have authority to award damages, remedies or awards that conflict with these Terms of Service. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of, related to or connected with the use of the Services or these Terms of Services must be filed within one (1) year after such claim of action arose or be forever banned.
                            </p>
                            <h4 class="Black">Severability.</h4>
                            <p>
                                If the prohibition against class actions and other claims brought on behalf of third parties contained above is found to be unenforceable, then all of the preceding language in this Arbitration section will be null and void. This arbitration agreement will survive the termination of your relationship with us.
                            </p>
                            <h4 class="Black">Governing Law and Jurisdiction.</h4>
                            <p>
                                These Terms of Service shall be governed by and construed in accordance with the laws of Malaysia, including Sabah & Sarawak. You agree that any dispute arising from or relating to the subject matter of these Terms of Service shall be governed by the exclusive jurisdiction of High Court of Malaya.
                            </p>


                        <h3 class="Black">12. MODIFICATION</h3>
                            <p>
                                We reserve the right, in our sole discretion, to modify or replace any of these Terms of Service, or change, suspend, or discontinue the Services (including without limitation, the availability of any feature, database, or content) at any time by posting a notice on the Site or by sending you notice through the Services, via e-mail or by another appropriate means of electronic communication. We may also impose limits on certain features and services or restrict your access to parts or all of the Services without notice or liability. While we will timely provide notice of modifications, it is also your responsibility to check these Terms of Service periodically for changes. Your continued use of the Services following notification of any changes to these Terms of Service constitutes acceptance of those changes, which will apply to your continued use of the Services going forward. Your use of the Services is subject to the Terms of Service in effect at the time of such use.
                            </p>
                    
                        <h3 class="Black">13. MISCELLANEOUS</h3>
                        <p>
                            Entire Agreement and Severability.
                        </p>
                        <p>
                            These Terms of Service are the entire agreement between you and us with respect to the Services, including use of the Site, and supersede all prior or contemporaneous communications and proposals (whether oral, written or electronic) between you and us with respect to the Services. If any provision of these Terms of Service is found to be unenforceable or invalid, that provision will be limited or eliminated to the minimum extent necessary so that these Terms of Service will otherwise remain in full force and effect and enforceable. The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights hereunder
                        </p>
                        <h4 class="Black">Force Majeure.</h4>
                        <p>
                            We shall not be liable for any failure to perform our obligations hereunder where such failure results from any cause beyond our reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation.
                        </p>
                        <h4 class="Black">Assignment.</h4>
                        <p>
                            These Terms of Service are personal to you, and are not assignable, transferable or sublicensable by you except with our prior written consent. We may assign, transfer or delegate any of our rights and obligations hereunder without consent.
                        </p>
                        <h4 class="Black">Agency.</h4>
                        <p>
                            No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Service and neither party has any authority of any kind to bind the other in any respect.
                        </p>
                        <h4 class="Black">Notices.</h4>
                        <p>
                            Unless otherwise specified in these Term of Service, all notices under these Terms of Service will be in writing and will be deemed to have been duly given when received, if personally delivered or sent by certified or registered mail, return receipt requested; when receipt is electronically confirmed, if transmitted by facsimile or e-mail; or the day after it is sent, if sent for next day delivery by recognized overnight delivery service. Electronic notices should be sent to <a href="mailto:admin@aplikasi.us/aplikasi-2">admin@aplikasi.us/aplikasi-2</a>.
                        </p>
                        <h4 class="Black">No Waiver.</h4>
                        <p>
                            Our failure to enforce any part of these Terms of Service shall not constitute a waiver of our right to later enforce that or any other part of these Terms of Service. Waiver of compliance in any particular instance does not mean that we will waive compliance in the future. In order for any waiver of compliance with these Terms of Service to be binding, we must provide you with written notice of such waiver through one of our authorized representatives.
                        </p>
                        <h4 class="Black">Headings.</h4>
                        <p>
                            The section and paragraph headings in these Terms of Service are for convenience only and shall not affect their interpretation.
                        </p>
                        <p>
                            Effective Date of Terms of Service: 4th June 2018
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection