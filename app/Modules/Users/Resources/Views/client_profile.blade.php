@extends('Layouts.Client.base_new')
@section('content')
@section('title','Client-Profile')
<div class="row floated-div">

    @if(Session::has('success'))
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
    </div>
    @endif

    @if(Session::has('error'))
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
    </div>
    @endif
    {{csrf_field()}}
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <div class="textlastUpdate">Last updated:
            {{ ($profileArray != null) ? date('d M. Y',strtotime($profileArray->updated_at)) : ""}}</div>
        <div class="commanContent commanContentBig" id="profile-data">
        <a href="javascript:void(0)" class="btnEdit" style="float: right;" id="edit-client-profile"></a>    
        <h2><span class="text-red">{{($profileArray != null) ? $profileArray->first_name : ''}}</span>
                {{($profileArray != null) ? $profileArray->last_name : ''}}</h2>
            <ul class="listContract mb-3">
                <li>{{($profileArray != null) ? $profileArray->department : ''}} Department</li>
                <li>{{($profileArray != null) ? $profileArray->client_name : ''}}</li>
            </ul>
            <div class="row rowContract mb-4">
                <div class="col-md-6">
                    <div class="iconBox iconMail">
                        <a href="mailto:{{($profileArray != null) ? $profileArray->email : ''}}"
                            title="{{($profileArray != null) ? $profileArray->email : ''}}">{{($profileArray != null) ? $profileArray->email : ''}}</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="iconBox iconPhone">
                        <a href="tel:{{($profileArray != null) ? $profileArray->mobile : ''}}"
                            title="{{($profileArray != null) ? $profileArray->mobile : ''}}">{{($profileArray != null) ? $profileArray->mobile : ''}}</a>
                    </div>
                </div>
                <!-- <div class="col-md-4">
                    <div class="iconBox iconplace">{{($profileArray!= null) ? $profileArray->mailing_city : ''}}</div>
                </div> -->
            </div>
            <h5 class="pb-2">Address Information</h5>
            <div class="boxAddress">
                <address>
                    {{($profileArray != null) ? $profileArray->mailing_street : ""}} , <br>
                    {{($profileArray != null) ? $profileArray->mailing_city : ""}} , 
                    {{($profileArray != null) ? $profileArray->mailing_zip : ""}} ,
                    {{($profileArray != null) ? $profileArray->mailing_state : ""}} , 
                    {{($profileArray != null) ? $profileArray->mailing_country : ""}}
                </address>
                
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
@endsection