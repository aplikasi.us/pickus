@extends('Layouts.General.base_new')
@section('content')
@section('title','Thank You')
<article class="thankyoupage min-height-auto" >
    <section class="thankyousection1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="imgThankElement1">
                        <img src="{{admin_asset('images/Artboard-2.png')}}" alt="" title="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="text-center">
                        <div class="d-block text-center">
                            <img class="mb-3" src="{{admin_asset('images/logo.png')}}">
                        </div>
                        <div class="border-top-botom">
                            <h2 class="colorWhite font-weight-bold">Thank you for joining us!</h2>
                            <p class="colorWhite">We've sent you a verification email. Make sure you verify your account before doing anything else!</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="imgThankElement2">
                        <img src="{{admin_asset('images/Artboard-3.png')}}" alt="" title="">
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection