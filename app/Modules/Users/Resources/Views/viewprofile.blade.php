<h2><span class="text-red">{{($profileArray != null) ? $profileArray->first_name : ''}}</span>
    {{($profileArray != null) ? $profileArray->last_name : ''}}</h2>
<ul class="listContract mb-3">
    <li>{{($profileArray != null) ? $profileArray->job_title :  ''}}</li>
    <li>{{($profileArray != null) ? $profileArray->department : ''}} Department</li>
    <li>{{($profileArray != null) ? $profileArray->client_name : ''}}</li>
</ul>
<div class="row rowContract mb-4">
    <div class="col-md-4">
        <div class="iconBox iconMail">
            <a href="mailto:{{($profileArray != null) ? $profileArray->email : ''}}"
                title="{{($profileArray != null) ? $profileArray->email : ''}}">{{($profileArray != null) ? $profileArray->email : ''}}</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="iconBox iconPhone">
            <a href="tel:{{($profileArray != null) ? $profileArray->mobile : ''}}"
                title="{{($profileArray != null) ? $profileArray->mobile : ''}}">{{($profileArray != null) ? $profileArray->mobile : ''}}</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="iconBox iconplace">{{($profileArray!= null) ? $profileArray->mailing_city : ''}}</div>
    </div>
</div>
<h5 class="pb-2">Address Information</h5>
<div class="boxAddress">
    <address>
        {{($profileArray != null) ? $profileArray->billing_code : ""}}
        {{($profileArray != null) ? $profileArray->billing_street : ""}}
        {{($profileArray != null) ? $profileArray->billing_city: ""}}
        {{($profileArray != null) ? $profileArray->billing_state : ""}}
        {{($profileArray != null) ? $profileArray->billing_country : ""}}
    </address>
    <a href="javascript:void(0)" class="btnEdit" style="float: right;" id="edit-client-profile"></a>
</div>