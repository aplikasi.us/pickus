@extends('Layouts.Client.base_new')
@section('content')
@section('title','Client-Settings')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <h2>Settings</h2>
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="tabsInterviews">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#changepassword" role="tab">Change Password</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" data-toggle="tab" href="#deleteaccount"
                       role="tab">Deactivate Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" data-toggle="tab" href="#notifications"
                       role="tab">Notifications</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="changepassword">
                    <div class="boxForm pt-4">
                        <div class="row">

                            <div class="col-md-12">
                                <form method="post" method="post" action="{{route('changepassword')}}" id="change-password">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Old Password: </label>
                                                <input type="password" name="old_password" class="form-control" placeholder=" Enter password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">New Password: </label>
                                                <input type="password" name="new_password" class="form-control" placeholder=" Enter password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password: </label>
                                                <input type="password" name="confirm_password" class="form-control" placeholder=" Re-enter your password">
                                            </div>
                                        </div>
                                    </div>	
                                    <input type="submit" class="btn btn-block btn-danger" value="Change Password!"/>			
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade " id="deleteaccount">
                    <p class="">Are you sure you want to deactivate your account? Deactivating will not delete your data. It will simply make your account invisible to others. </p>
                    <a href="#" class="btn btn-block btn-danger" data-toggle="modal" data-target="#delete-account">Deactivate Account</a>
                </div>
                <div class="tab-pane fade show " id="notifications">
                    <form method="post" method="post" action="{{route('notifications',[$id])}}">
                        {{csrf_field()}}
                        <?php
                        $client_job_wishlist = $client_applied_job = $all_checked = "";
                        if ($user->client_job_wishlist == 1 && $user->client_applied_job == 1) {
                            $all_checked = 'checked';
                        }
                        if ($user->client_job_wishlist == 1) {
                            $client_job_wishlist = 'checked';
                        }
                        if ($user->client_applied_job == 1) {
                            $client_applied_job = 'checked';
                        }
                        ?>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">All Notifications: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="all_checked" {{$all_checked}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Availability status change for consultants in wishlist: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checked_all" value='1' name='client_job_wishlist' {{$client_job_wishlist}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Job application from consultant: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checked_all" value='1' name='client_applied_job' {{$client_applied_job}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-block btn-danger" value="Save Settings"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @include('Layouts.General.recentlyviewed')
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>


</div>

<!----------------Delete account model up ------------------>

<div class="modal fade" id="delete-account" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="boxForm">
                <form method="post" method="post" action="{{route('deleteaccount')}}" id="delete-account-form">
                    {{csrf_field()}}
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Deactivate Account</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Password: </label>
                                    <input type="password" name="password" class="form-control" placeholder=" Enter password">
                                </div>
                            </div>
                        </div>
                    </div>	
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-secondary" value="Deactivate Now"/>	
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection