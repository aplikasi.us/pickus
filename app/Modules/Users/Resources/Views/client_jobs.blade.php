@extends('Layouts.Client.base_new')
@section('content')
@section('title','Client-Jobs')
<div class="row floated-div">
    <div class="col-12">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
    </div>
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <div class="tabsInterviews tabJobs">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link  {{(isset($_GET['tab']) && $_GET['tab'] == "postJob") ? 'active' : ''}} {{ !isset($_GET['tab']) ? 'active' : ''}}"
                        data-toggle="tab" href="#postJob" role="tab">Post a Job</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{(isset($_GET['tab']) && $_GET['tab'] == "pendingJobs") ? 'active' : ''}}"
                        data-toggle="tab" data-toggle="tab" href="#pendingJobs" role="tab">Pending Jobs
                        ({{count($jobList['pendingJobs'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{(isset($_GET['tab']) && $_GET['tab'] == "activeJobs") ? 'active' : ''}}"
                        data-toggle="tab" data-toggle="tab" href="#activeJobs" role="tab">Active Jobs
                        ({{count($jobList['activeJobs']['list'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{(isset($_GET['tab']) && $_GET['tab'] == "history") ? 'active' : ''}}"
                        data-toggle="tab" data-toggle="tab" href="#history" role="tab">History
                        ({{count($jobList['history']['list'])}})</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "postJob") ? 'show active' : ''}}{{ !isset($_GET['tab']) ? 'show active' : ''}}"
                    id="postJob">
                    <div class="boxForm pt-4">
                        <form method="post" action="{{route('client-jobs')}}" id="client-job-post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Job Title</label>
                                        <input name="job_title" type="text" class="form-control"
                                            placeholder="Enter job title">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="row rs-5">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Daily Budget</label>
                                                <div class="input-group formborder-left">
                                                    <span class="input-group-addon ">MYR</span>
                                                    <!-- <div class="textMyr sticky-image">MYR</div> -->
                                                    <input name="budget" type="text" class="form-control inputMyr"
                                                        id="job-budget" maxlength="10">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group mb-2">
                                                <label class="mb-2 colorBlue f-14">Calculated Base Rate
                                                    <a href="#" class="iconInfo" data-toggle="tooltip" data-placement="right" title="This figure is inclusive of our margin and will be shown to the candidates in the job posting">
                                                        <img src="{{admin_asset('images/iconInfo.png')}}" alt=""
                                                            title="">
                                                    </a>

                                                </label>
                                                <div class="formgroup">
                                                    <div class="form-icon-big text-red">
                                                        <!-- <span class="cus-br-icon-big">BR</span> -->
                                                        <img class="cus-br-icon-mid" style="height:20px!important;" src="{{admin_asset('images/new-design/dollar.png')}}">
                                                        <span class="inputBase job-calculated-baserate text-red"></span>
                                                        <input type="hidden" class="job-calculated-baserate-input"
                                                            name="baserate" val="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Experience Required (Years)</label>
                                        {{Form::select('experiece',[null => '--Select Experience--'] + $experienceList1,'',['class' => 'form-control select2','required'])}}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Industry</label>
                                        {{Form::select('industry',[null => '--Select Industry--'] + $industryList,'',['class' => 'form-control select2'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="row rs-5">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Start Date</label>
                                                <div class="input-group formborder-right">
                                                    <!-- <div class="textMyr sticky-image">MYR</div> -->
                                                    <input name="start_date" id="job-start-date" type="text"
                                                        class="form-control" readonly=""
                                                        placeholder="Select Start Date">
                                                    <span class="input-group-addon "><i
                                                            class="fa fa-calendar colorSkyBlue"
                                                            aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Duration</label>
                                                <input name="duration" type="text" class="form-control" maxlength="10"
                                                    placeholder="Enter month(s)" min="1">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="row rs-5">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">State</label>
                                                {{Form::select('state',[null => '--Select State--'] + $stateList,'',['class' => 'form-control select2','id' => 'job-state','required' => true])}}
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">City</label>
                                                {{Form::select('city',[null => '--Select City--'],'',['class' => 'form-control select2','id' => 'job-city','required'=> true])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="row rs-5">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Job Type</label>
                                                {{Form::select('job_type',[null => '--Select Job Type--'] + $jobTypeList,'',['class' => 'form-control select2'])}}
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label class="mb-2 colorBlue f-14">Job Mode</label>
                                                {{Form::select('job_mode',[null => '--Select Job Mode--'] + $jobModeList,'',['class' => 'form-control select2'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Categories</label>
                                        <!-- {{Form::select('category',[null => '--Select Category--'] + $jobCategoryList,'',['class' => 'form-control select2'])}} -->
                                        {{Form::select('category[]',$jobCategoryList,'',['class'=>'form-control select2','id' => 'candidate-category','multiple' => true])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Key Skills</label>
                                        <!-- <textarea name="key_skills" class="form-control textareaSmall" placeholder="Please enter comma separated key skills. e.g Node, Angular, Php etc..."></textarea> -->
                                        <input data-skill="" data-role="tagsinput" name="key_skills"
                                            placeholder="Enter key skills">
                                        <small class="colorBlue mt-2">Type key skill and press enter to add new key
                                            skill</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Job Description</label>
                                        <textarea name="job_description" class="form-control textareaSmall"
                                            id="job_description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Requirements</label>
                                        <textarea name="requirements" class="form-control textareaSmall"
                                            id="job_requirements"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label class="mb-2 colorBlue f-14">Benefits</label>
                                        <textarea name="benefits" class="form-control textareaSmall"
                                            id="job_benefits"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <p class="colorBlue pb-4 pt-2">You can add notes once the job has been approved,
                                        isn’t that good?</p>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <p class="text-right pb-0">
                                        <input type="submit" value="Submit"
                                            class="btn form-submit-btn btn-sm btn-submit">
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "pendingJobs") ? 'show active' : ''}}"
                    id="pendingJobs">
                    @if(count($jobList['pendingJobs']))
                    @foreach($jobList['pendingJobs'] as $pendingJobKey => $pendingJobValue)
                    <div class="commanContent">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                                <ul class="listContract">
                                    @if(isset($pendingJobValue['Job_ID']) && !empty($pendingJobValue['Job_ID']))
                                    <li>{{$pendingJobValue['Job_ID']}}</li>
                                    @endif
                                    <li>{{$pendingJobValue['job_type']}}</li>
                                </ul>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <a href="{{route('job-details',[\Crypt::encryptString($pendingJobValue['id'])])}}"><h5 class="d-inline">{{$pendingJobValue['posting_title']}}</h5></a>
                                        <span class="btn-sm btn-tag-md status-associated ml-3">{{$pendingJobValue
                                        ['job_opening_status']}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                <div class="btnsRight btn-job-right">
                                    <a title="Edit"
                                        href="{{route('client-edit-job',[\Crypt::encryptString($pendingJobValue['id'])])}}"
                                        class=" "><i class="fa fa-pencil-square-o text-light f-20"
                                            aria-hidden="true"></i></a>
                                    <a title="Delete" href="{{route('client-delete-job',[$pendingJobValue['id']])}}"
                                        class="ml-2 delete-job"><i class="fa fa-trash-o text-light f-20"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row rowContract">
                            <div class="col-md-3">
                                <div class="iconBox iconDoller Black">MYR
                                    {{($pendingJobValue['actual_revenue'] != "") ? $pendingJobValue['actual_revenue'] : "0.00"}}/day
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="text-red">
                                    <!-- <span class="cus-br-icon">BR</span> MYR  -->
                                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                        <span class="ml-1">MYR </span>
                                    <span class="c-job-base-rate">{{($pendingJobValue['job_base_rate'] != "") ? $pendingJobValue['job_base_rate'] : "0.00"}}</span>/day
                                    <div
                                        class="iconBox iconCopy inlineText copy-to-clipboard job-tile-button-right ml-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row rowContract">
                            <div class="col">
                                <div class="iconBox iconTime Black">{{$pendingJobValue['job_mode']}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconYears Black">{{$pendingJobValue['work_experience']}} exp.</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconDate Black">
                                    {{date('d M. Y',strtotime($pendingJobValue['date_opened']))}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconMonths Black">{{$pendingJobValue['job_duration']}} months</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconGas Black">{{$pendingJobValue['industry']}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconplace Black">{{$pendingJobValue['city']}}</div>
                            </div>
                        </div>
                        <div class="collapse" id="collapse-pendingjob-{{$pendingJobKey}}">
                            <hr />
                            <p class="pb-2 Black">Skill Sets</p>
                            @php
                            $skills = [];
                            if(isset($pendingJobValue['key_skills'])){
                            $skills = explode(",",$pendingJobValue['key_skills']);
                            }
                            @endphp
                            <ul class="listSkills listSkillsBig">
                                @if(count($skills))
                                @foreach($skills as $skillValue)
                                <li>
                                    <a href="javascript:void(0)">{{$skillValue}}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                            <!-- <hr/> -->
                            <!-- <p class="pb-2">Job Description</p> -->
                            <div class="interview-note-show">
                                <div class="notes-container">
                                    <div class="commanContent spacing20">
                                        <h2 class="headingBorder">
                                            <a data-toggle="collapse" class="collapseIcon"
                                                href="#jobDesciption-{{$pendingJobKey}}" role="button"
                                                aria-expanded="true"> Description</a>
                                            <div class="icon-down-up">
                                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </div>
                                            <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='0'><i class="fa fa-times"></i></a> -->
                                        </h2>
                                        <div class="multi-collapse collapse show" id="jobDesciption-{{$pendingJobKey}}"
                                            style="">
                                            <ul class="listContent listBullets listExperience withoutLine">
                                                @if(isset($pendingJobValue['job_description']))
                                                <li class="Black">{!! $pendingJobValue['job_description'] !!}</li>
                                                @else
                                                <li class="Black">No description found</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="interview-note-show mt-3">
                                <div class="notes-container">
                                    <div class="commanContent spacing20">
                                        <h2 class="headingBorder">
                                            <a data-toggle="collapse" class="collapseIcon"
                                                href="#jobRequirements-{{$pendingJobKey}}" role="button"
                                                aria-expanded="true"> Requirements</a>
                                            <div class="icon-down-up">
                                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </div>
                                            <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='0'><i class="fa fa-times"></i></a> -->
                                        </h2>
                                        <div class="multi-collapse collapse show"
                                            id="jobRequirements-{{$pendingJobKey}}" style="">
                                            <ul class="listContent listBullets listExperience withoutLine">
                                                @if(isset($pendingJobValue['job_requirements']))
                                                <li class="Black">{!! $pendingJobValue['job_requirements'] !!}</li>
                                                @else
                                                <li class="Black">No Requirements found</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="interview-note-show mt-3 mb-3">
                                <div class="notes-container">
                                    <div class="commanContent spacing20">
                                        <h2 class="headingBorder">
                                            <a data-toggle="collapse" class="collapseIcon"
                                                href="#jobBenefits-{{$pendingJobKey}}" role="button"
                                                aria-expanded="true"> Benefits</a>
                                            <div class="icon-down-up">
                                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </div>
                                            <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='0'><i class="fa fa-times"></i></a> -->
                                        </h2>
                                        <div class="multi-collapse collapse show" id="jobBenefits-{{$pendingJobKey}}"
                                            style="">
                                            <ul class="listContent listBullets listExperience withoutLine">
                                                @if(isset($pendingJobValue['job_benefits']))
                                                <li class="Black">{!! $pendingJobValue['job_benefits'] !!}</li>
                                                @else
                                                <li class="Black">No Benefits found</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="btncollapse collapsed job-viewMOre" data-toggle="collapse"
                            href="#collapse-pendingjob-{{$pendingJobKey}}" role="button" aria-expanded="false">VIEW
                            MORE</a>
                    </div>
                    @endforeach
                    @endif

                </div>
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "activeJobs") ? 'show active' : ''}}"
                    id="activeJobs">
                    @if(count($jobList['activeJobs']['list']))
                    @foreach($jobList['activeJobs']['list'] as $activeJobKey => $activeJobValue)
                    <div class="commanContent">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <ul class="listContract">
                                    @if(isset($activeJobValue['data']['Job_ID']) &&
                                    !empty($activeJobValue['data']['Job_ID']))
                                    <li>{{$activeJobValue['data']['Job_ID']}}</li>
                                    @endif
                                    <li>{{$activeJobValue['data']['job_type']}}</li>
                                </ul>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <a href="{{route('job-details',[\Crypt::encryptString($activeJobValue['data']['id'])])}}"><h5 class="d-inline">{{$activeJobValue['data']['posting_title']}}</h5></a>
                                        <span
                                            class="btn-sm btn-tag-md status-associated ml-3 bg-blue-on">{{$activeJobValue['data']['job_opening_status']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row rowContract">
                            <div class="col-md-3">
                                <div class="iconBox iconDoller Black">MYR
                                    {{($activeJobValue['data']['actual_revenue'] != "") ? $activeJobValue['data']['actual_revenue'] : "0.00"}}
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="text-red">
                                    <!-- <span class="cus-br-icon">BR</span> MYR  -->
                                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                        <span class="ml-1">MYR </span>
                                    <span class="c-job-base-rate">{{($activeJobValue['data']['job_base_rate'] != "") ? $activeJobValue['data']['job_base_rate'] : "0.00"}}/day</span>
                                    <div
                                        class="iconBox iconCopy inlineText copy-to-clipboard job-tile-button-right ml-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row rowContract pb-4">
                            <div class="col">
                                <div class="iconBox iconTime Black">{{$activeJobValue['data']['job_mode']}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconYears Black">{{$activeJobValue['data']['work_experience']}} exp.
                                </div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconDate Black">
                                    {{date('d M. Y',strtotime($activeJobValue['data']['date_opened']))}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconMonths Black">{{$activeJobValue['data']['job_duration']}} months
                                </div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconGas Black">{{$activeJobValue['data']['industry']}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconplace Black">{{$activeJobValue['data']['city']}}</div>
                            </div>
                        </div>
                        <div class="myjobInnerTab">
                            <ul class="nav nav-tabs ">
                                <li class="nav-item">
                                    <a class="nav-link active bg-yellow-job" data-toggle="tab"
                                        href="#screeningTab-{{$activeJobKey}}" role="tab">
                                        <span>{{isset($activeJobValue['screening']) ? count($activeJobValue['screening']) : 0}}</span>
                                        Screening</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-orange-job" data-toggle="tab" data-toggle="tab"
                                        href="#submissionTab-{{$activeJobKey}}"
                                        role="tab"><span>{{isset($activeJobValue['submission']) ? count($activeJobValue['submission']) : 0}}</span>
                                        Submission</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-purple-job" data-toggle="tab" data-toggle="tab"
                                        href="#InterviewTab-{{$activeJobKey}}"
                                        role="tab"><span>{{isset($activeJobValue['interview']) ? count($activeJobValue['interview']) : 0}}</span>
                                        Interview</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-skyblue-job" data-toggle="tab" data-toggle="tab"
                                        href="#offerTab-{{$activeJobKey}}"
                                        role="tab"><span>{{isset($activeJobValue['offer']) ? count($activeJobValue['offer']) : 0}}</span>
                                        Offer</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-grren-job" data-toggle="tab" data-toggle="tab"
                                        href="#hireTab-{{$activeJobKey}}"
                                        role="tab"><span>{{isset($activeJobValue['hire']) ? count($activeJobValue['hire']) : 0}}</span>
                                        Hiring</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active table-border-pd"
                                    id="screeningTab-{{$activeJobKey}}">
                                    @if(isset($activeJobValue['screening']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">
                                                    <!-- <span class="cus-br-icon">BR</span> -->
                                                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                                     Base Rate</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($activeJobValue['screening']))
                                            @foreach($activeJobValue['screening'] as $screeningKey => $screeningValue)
                                            <tr>
                                                <td>{{$screeningValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($screeningValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$screeningValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($activeJobValue['data']['associatecandidates']))
                                                    
                                                    @foreach($activeJobValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $screeningValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                    
                                                </td>
                                                <td class="Black">MYR <span
                                                        class="c-job-base-rate">{{($screeningValue['base_rate'] != "") ? $screeningValue['base_rate'] : "0.00"}}</span>
                                                    <!-- <div class="iconBox iconCopy inlineText copy-to-clipboard job-tile-button-right ml-2"></div> -->
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No screening found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No screening found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="submissionTab-{{$activeJobKey}}">
                                    @if(isset($activeJobValue['submission']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($activeJobValue['submission']))
                                            @foreach($activeJobValue['submission'] as $submissionKey =>
                                            $submissionValue)
                                            <tr>
                                                <td>{{$submissionValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($submissionValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$submissionValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($activeJobValue['data']['associatecandidates']))
                                                    
                                                    @foreach($activeJobValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $submissionValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{($submissionValue['base_rate'] != "") ? $submissionValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No submissions found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No submissions found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="InterviewTab-{{$activeJobKey}}">
                                    @if(isset($activeJobValue['interview']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($activeJobValue['interview']))
                                            @foreach($activeJobValue['interview'] as $interviewKey => $interviewValue)
                                            <tr>
                                                <td>{{$interviewValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($interviewValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$interviewValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($activeJobValue['data']['associatecandidates']))
                                                    
                                                    @foreach($activeJobValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $interviewValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{isset($interviewValue['base_rate']) ? $interviewValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No interviews found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No interviews found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="offerTab-{{$activeJobKey}}">
                                    @if(isset($activeJobValue['offer']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($activeJobValue['offer']))
                                            @foreach($activeJobValue['offer'] as $offerKey => $offerValue)
                                            <tr>
                                                <td>{{$offerValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($offerValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$offerValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($activeJobValue['data']['associatecandidates']))
                                                    
                                                    @foreach($activeJobValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $offerValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{isset($offerValue['base_rate']) ? $offerValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No offers found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No offers found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="hireTab-{{$activeJobKey}}">
                                    @if(isset($activeJobValue['hire']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($activeJobValue['hire']))
                                            @foreach($activeJobValue['hire'] as $hireKey => $hireValue)
                                            <tr>
                                                <td>{{$hireValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($hireValue['rating'] < $i) <span
                                                            class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$hireValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($activeJobValue['data']['associatecandidates']))
                                                    
                                                    @foreach($activeJobValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $hireValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{isset($hireValue['base_rate']) ? $hireValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No hire found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No hire found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="collapse" id="collapse-activejob-{{$activeJobKey}}">

                            <hr />
                            <p class="pb-2 Black">Skill Sets</p>
                            @php
                            $skills = [];
                            if(isset($activeJobValue['data']['key_skills'])){
                            $skills = explode(",",$activeJobValue['data']['key_skills']);
                            }
                            @endphp
                            <ul class="listSkills listSkillsBig">
                                @if(count($skills))
                                @foreach($skills as $skillValue)
                                <li>
                                    <a href="javascript:void(0)">{{$skillValue}}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                            <div class="interview-note-show">
                                <div class="notes-container">
                                    <div class="commanContent spacing20">
                                        <h2 class="headingBorder">
                                            <a data-toggle="collapse" class="collapseIcon"
                                                href="#activeDesciption-{{$activeJobKey}}" role="button"
                                                aria-expanded="true"> Job Description</a>
                                            <div class="icon-down-up">
                                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </div>
                                            <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='0'><i class="fa fa-times"></i></a> -->
                                        </h2>
                                        <div class="multi-collapse collapse show"
                                            id="activeDesciption-{{$activeJobKey}}" style="">
                                            <ul class="listContent listBullets listExperience withoutLine">
                                                @if(isset($activeJobValue['data']['job_description']))
                                                <li class="Black">{!! $activeJobValue['data']['job_description'] !!}
                                                </li>
                                                @else
                                                <li class="Black">No description found</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="interview-note-show mt-3 mb-3">
                                <div class="notes-container">
                                    <div class="commanContent spacing20">
                                        <h2 class="headingBorder">
                                            <a data-toggle="collapse" class="collapseIcon colorRed"
                                                href="#NoteDesciption-{{$activeJobKey}}" role="button"
                                                aria-expanded="true"> Notes</a>
                                            <div class="icon-down-up">
                                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </div>
                                            <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='0'><i class="fa fa-times"></i></a> -->
                                        </h2>
                                        <div class="multi-collapse collapse show" id="NoteDesciption-{{$activeJobKey}}"
                                            style="">
                                            <ul class="listContent listBullets listExperience withoutLine">
                                                @if(!empty($activeJobValue['data']['notes']))
                                                @foreach($activeJobValue['data']['notes'] as $notKey => $noteValue)
                                                <li class="Black">{!! $noteValue['note_content'] !!}</li>
                                                @endforeach
                                                @else
                                                <li class="Black">No notes found</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a class="col-12 btncollapse collapsed job-viewMOre activejob-viewMOre" data-toggle="collapse"
                            href="#collapse-activejob-{{$activeJobKey}}" role="button" aria-expanded="false">VIEW
                            MORE</a>
                        @if(isset($activeJobValue['data']['JOBOPENINGID']))
                        <a title="Add Note" href="javascript:void(0)" data-id='{{$activeJobValue['data']['id']}}'
                            class="btn btn-danger border-ra-5 f-14 font-weight-normal btnAdd add-note add-notedesgn-button mb-3 ">Add
                            Your Notes</a>
                        <div class="note-container" id="note_add_container">
                            <div class="">
                                <form role="form" method="post" action="{{route('client-add-jobnote')}}"
                                    id="consultant-login">
                                    <div class="note-body">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="job_id" id="job_id" />
                                                    <textarea name="note" class="form-control" rows="4"
                                                        placeholder="Enter job note" required=""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex note-footer mb-3">
                                        <button class="btn btn-sm btn-green border-ra-5" type="submit">Save
                                            Note</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endif
                </div>
                <div class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == "history") ? 'show active' : ''}}"
                    id="history">
                    @if(count($jobList['history']['list']))
                    @foreach($jobList['history']['list'] as $historyKey => $historyValue)
                    <div class="commanContent">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">

                                <ul class="listContract">
                                    @if(isset($historyValue['data']['Job_ID']) &&
                                    !empty($historyValue['data']['Job_ID']))
                                    <li>{{$historyValue['data']['Job_ID']}}</li>
                                    @endif
                                    <li>{{$historyValue['data']['job_type']}}</li>
                                </ul>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <a href="{{route('job-details',[\Crypt::encryptString($historyValue['data']['id'])])}}"><h5 class="d-inline">{{$historyValue['data']['posting_title']}}</h5></a>
                                        @if($historyValue['data']['job_opening_status'] == "Filled")
                                        <span
                                            class="btn-sm btn-tag-md btnLightGreen ml-3">{{$historyValue['data']['job_opening_status']}}</span>
                                        @else
                                        <span
                                            class="btn-sm btn-tag-md status-danger ml-3">{{$historyValue['data']['job_opening_status']}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row rowContract">
                            <div class="col-md-3">
                                <div class="iconBox iconDoller Black">MYR
                                    {{($historyValue['data']['actual_revenue'] != "") ? $historyValue['data']['actual_revenue'] : "0.00"}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="text-red">
                                    <!-- <span class="cus-br-icon">BR</span> MYR -->
                                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                        <span class="ml-1">MYR </span>
                                    {{($historyValue['data']['job_base_rate'] != "") ? $historyValue['data']['job_base_rate'] : "0.00"}}/day
                                </div>
                            </div>
                        </div>
                        <div class="row rowContract pb-4">
                            <div class="col">
                                <div class="iconBox iconTime Black">{{$historyValue['data']['job_mode']}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconYears Black">{{$historyValue['data']['work_experience']}} exp.
                                </div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconDate Black">
                                    {{date('d M. Y',strtotime($historyValue['data']['date_opened']))}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconMonths Black">{{$historyValue['data']['job_duration']}} months
                                </div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconGas Black">{{$historyValue['data']['industry']}}</div>
                            </div>
                            <div class="col">
                                <div class="iconBox iconplace Black">{{$historyValue['data']['city']}}</div>
                            </div>
                        </div>
                        <div class="myjobInnerTab">
                            <ul class="nav nav-tabs ">
                                <li class="nav-item">
                                    <a class="nav-link active bg-yellow-job" data-toggle="tab" href="#historyscreeningTab-{{$historyKey}}"
                                        role="tab">
                                        <span>{{isset($historyValue['screening']) ? count($historyValue['screening']) : 0}}</span>
                                        Screening</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-orange-job" data-toggle="tab" data-toggle="tab"
                                        href="#historysubmissionTab-{{$historyKey}}"
                                        role="tab"><span>{{isset($historyValue['submission']) ? count($historyValue['submission']) : 0}}</span>
                                        Submission</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-purple-job" data-toggle="tab" data-toggle="tab"
                                        href="#historyInterviewTab-{{$historyKey}}"
                                        role="tab"><span>{{isset($historyValue['interview']) ? count($historyValue['interview']) : 0}}</span>
                                        Interview</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-skyblue-job" data-toggle="tab" data-toggle="tab"
                                        href="#historyofferTab-{{$historyKey}}"
                                        role="tab"><span>{{isset($historyValue['offer']) ? count($historyValue['offer']) : 0}}</span>
                                        Offer</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link bg-grren-job " data-toggle="tab" data-toggle="tab"
                                        href="#historyhireTab-{{$historyKey}}"
                                        role="tab"><span>{{isset($historyValue['hire']) ? count($historyValue['hire']) : 0}}</span>
                                        Hiring</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active table-border-pd" id="historyscreeningTab-{{$historyKey}}">
                                    @if(isset($historyValue['screening']))

                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate 
                                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <!-- <span class="cus-br-icon">BR</span>  -->
                                            </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($historyValue['screening']))
                                            @foreach($historyValue['screening'] as $screeingKey => $screeningValue)
                                            <tr>
                                                <td>{{$screeningValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($screeningValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$screeningValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($historyValue['data']['associatecandidates']))
                                                    
                                                    @foreach($historyValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $screeningValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{($screeningValue['base_rate'] != "") ? $screeningValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No screening found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No screening found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="historysubmissionTab-{{$historyKey}}">
                                    @if(isset($historyValue['submission']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($historyValue['submission']))
                                            @foreach($historyValue['submission'] as $submissionKey => $submissionValue)
                                            <tr>
                                                <td>{{$submissionValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($submissionValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$submissionValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($historyValue['data']['associatecandidates']))
                                                    
                                                    @foreach($historyValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $submissionValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{($submissionValue['base_rate'] != "") ? $submissionValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No submission found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No submission found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="historyInterviewTab-{{$historyKey}}">
                                    @if(isset($historyValue['interview']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($historyValue['interview']))
                                            @foreach($historyValue['interview'] as $interviewKey => $interviewValue)
                                            <tr>
                                                <td>{{$interviewValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($interviewValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$interviewValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($historyValue['data']['associatecandidates']))
                                                    
                                                    @foreach($historyValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $interviewValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{isset($interviewValue['base_rate']) ? $interviewValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No interview found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No interview found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="historyofferTab-{{$historyKey}}">
                                    @if(isset($historyValue['offer']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($historyValue['offer']))
                                            @foreach($historyValue['offer'] as $offerKey => $offerValue)
                                            <tr>
                                                <td>{{$offerValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($offerValue['rating'] < $i)
                                                            <span class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$offerValue['sap_job_title']}}</td>
                                                <td>
                                                    @if(count($historyValue['data']['associatecandidates']))
                                                    
                                                    @foreach($historyValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $offerValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{isset($offerValue['base_rate']) ? $offerValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No offer found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No offer found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane fade table-border-pd" id="historyhireTab-{{$historyKey}}">
                                    @if(isset($historyValue['hire']))
                                    <table class="table table-borderless mob-table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Candidate ID</th>
                                                <th scope="col">SAP Job Title</th>
                                                <th scope="col">Candidate Status</th>
                                                <th scope="col">Base Rate <img
                                                        src="{{admin_asset('images/cus-br-icon.png')}}" alt="" title="" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($historyValue['hire']))
                                            @foreach($historyValue['hire'] as $hireKey => $hireValue)
                                            <tr>
                                                <td>{{$hireValue['candidate_id']}}
                                                    <div class="box-rating consultant-start-rating">
                                                        @for($i = 1; $i <= 5; $i++) @if($hireValue['rating'] < $i) <span
                                                            class="fa fa-star unchecked"></span>
                                                            @else
                                                            <span class="fa fa-star checked"></span>
                                                            @endif
                                                            @endfor
                                                    </div>
                                                </td>
                                                <td>{{$hireValue['sap_job_title']}}</td>
                                                <td>
                                                     @if(count($historyValue['data']['associatecandidates']))
                                                    
                                                    @foreach($historyValue['data']['associatecandidates'] as $assKey => $assValue)
                                                    
                                                    @if($assValue['candidate_id'] == $hireValue['id'])
                                                    {{$assValue['status']}}
                                                    @endif
                                                    @endforeach
                                                    
                                                    @endif
                                                </td>
                                                <td class="text-red">MYR
                                                    {{isset($hireValue['base_rate']) ? $hireValue['base_rate'] : "0.00"}}
                                                    <div class="iconBox iconCopy inlineText ml-2"></div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td>No hire found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>No hire found</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="collapse" id="collapse-history-{{$historyKey}}">

                            <hr />
                            <p class="pb-2">Skill Sets</p>
                            @php
                            $skills = [];
                            if(isset($historyValue['data']['key_skills'])){
                            $skills = explode(",",$historyValue['data']['key_skills']);
                            }
                            @endphp
                            <ul class="listSkills listSkillsBig">
                                @if(count($skills))
                                @foreach($skills as $skillValue)
                                <li>
                                    <a href="javascript:void(0)">{{$skillValue}}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                            <div class="interview-note-show">
                                <div class="notes-container">
                                    <div class="commanContent spacing20">
                                        <h2 class="headingBorder">
                                            <a data-toggle="collapse" class="collapseIcon"
                                                href="#hireDesciption-{{$historyKey}}" role="button"
                                                aria-expanded="true"> Description</a>
                                            <div class="icon-down-up">
                                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </div>
                                            <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='0'><i class="fa fa-times"></i></a> -->
                                        </h2>
                                        <div class="multi-collapse collapse show" id="hireDesciption-{{$historyKey}}"
                                            style="">
                                            <ul class="listContent listBullets listExperience withoutLine">
                                                @if(isset($historyValue['data']['job_description']))
                                                <li class="Black">{!! $historyValue['data']['job_description'] !!}</li>
                                                @else
                                                <li class="Black">No description found</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="col-12 btncollapse collapsed job-viewMOre activejob-viewMOre" data-toggle="collapse"
                            href="#collapse-history-{{$historyKey}}" role="button" aria-expanded="false">VIEW MORE</a>
                        <!-- <a class="btncollapse collapsed" data-toggle="collapse" href="#collapse-history-{{$historyKey}}" role="button" aria-expanded="false"></a> -->
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h3 class="header-red-bg colorWhite text-center pd-5 how-much-left">Doing Your Budget?</h3>
        <div class="commanContent boxHowmuch">
            <div class="contentHowmuch">
                <p class="pb-1">Enter Base Rate</p>
                <div class="text-red inlineText">
                    <!-- <span class="cus-br-icon">BR</span> MYR -->
                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                </div>
                <input type="text" class="text-red text-right" value="" id="c3-base-rate-perday" maxlength="10" />
                <span class="text-red">/day</span>
            </div>
            <div class="row f-12 rs-5">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <p class="pb-2">Length of contract
                        <a href="javascript:void(0)" class="iconInfo">
                            <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                        </a>
                    </p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5 months
                        </li>
                        <li>
                            <input type="radio" name="months" class="months" value="2" title="6-11 months" checked="" />
                            6-11 months</li>
                        <li>
                            <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                            months</li>
                    </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <p class="pb-2">Fees</p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                checked="" disabled="" /> Admin fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                        <li>
                            <input type="checkbox" class="fees" name="professional_fee" title="Professional fee"
                                id="professional_fee" checked="" /> Professional fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="boxDoyouKnow mb-0">
                <h6 class="f-14 mb-0">The Billing Rate is</h6>
                <h2 class="text-red" id="billing-rate">MYR 0.00/day</h2>
            </div>
        </div>
        @include('Layouts.General.recentlyviewed')
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @include('Layouts.General.rating')
    </div>
</div>
<div class="modal fade" id="add-note-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold text-green">Add Note</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" method="post" action="{{route('client-add-jobnote')}}" id="consultant-login">
                <div class="modal-body mx-3">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" name="job_id" id="job_id" />
                                <textarea name="note" class="form-control" rows="4" placeholder="Enter job note"
                                    required=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-block btn-green" type="submit">Save Note</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection