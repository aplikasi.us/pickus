@extends('Layouts.General.base_new')
@section('content')
@section('title','Consultant Registration')
<article class="bg-color">
{{-- 
    <section class="clientsection5">
        <div class="container">
            <form role="form" method="post" action="{{route('consultant-registration')}}" id="consultant-registration" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <!-- <div class="admission-ticket-box">
                            <div class="heading-box"><h2>Admission Ticket</h2></div>
                            <div class="box-Pumped-up">
                                <div class="inner-box-Pumped-up">
                                    <img src="{{admin_asset('images/img-admission-ticket')}}.png"/>
                                    <h3>Make Me A </br>Superstar</h3>
                                    <img class="barcode-img" src="{{admin_asset('images/img-barcode.png')}}"/>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 contact-form">
                        <div class="inner-contact-box">
                            <h2>Ready To Get</br> <span>Drafted</span>?</h2>
                            <!-- <form id="contact-form" name="contact-form" action="mail.php" method="POST"> -->
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="hidden" name="role_id" value="2" />
                                    <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="Email Address">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="Phone" name="phone" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <input type="text" id="Job-title" name="sap_job_title" class="form-control" placeholder="SAP Job Title">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <select style="font-family:Futura PT Book" name="hear" class="form-control input-sm">
                                        <option value="">How Did You Hear About aplikasi.us?</option>
                                        <option value="Social Network">Social Network</option>
                                        <option value="Search Engine">Search Engine</option>
                                        <option value="Event">Event</option>
                                        <option value="Blog">Blog</option>
                                        <option value="Advertisement">Advertisement</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 box-browse-cv">
                                    <span>Browse CV<input type="file" id="cv" name="cv" class="form-control" placeholder="Browse Cv"></a></span>
                                    <div class="cv-append-html"><div class="in-html-append"></div></div>
                                    <label class="file-note">Please select only docx or pdf and select file size below 2 mb</label>
                                </div>
                                <div class="col-6 col-md-6  text-right">
                                    <div class="custom-control custom-radio">
                                        <input type="checkbox" id="customRadio1" name="t_n_c" class="custom-control-input">
                                        <label class="custom-control-label" style="font-family:Futura PT Book" for="customRadio1">I Agree to the <a href="{{route('privacyPolicy')}}"  target="_blank">Privacy Policy</a> and <a href="{{route('termsConditions')}}" target="_blank">Terms of Service</a></label>
                                    </div>
                                </div>
                            </div> 

                            <button type="submit" class="btn btn-postion-bottom" title="Get Started"><img src="{{admin_asset('images/get-start-img')}}.png"/> Get Started <img src="{{admin_asset('images/get-start-img')}}.png"/></button>
                            <!-- </form> -->
                        </div>                          
                    </div>
                </div>
            </form>
        </div>
    </section>
</article> --}}
<section class="presentation1">

    <div class="cover">
        <img src="{{admin_asset('images/chooseus')}}.png" data-pagespeed-url-hash="1688370513"
            onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
    </div>
    <div class="introduction">
        <div class="intro-text register-box">
            <div class="col-md-12 py-4 border">
                <h4 class="pb-4 font-register-header">LET’S FIND YOUR NEXT BIG JOB</h4>
                <form role="form" method="post" action="{{route('consultant-registration')}}" id="consultant-registration" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="hidden" name="role_id" value="2" />
                            <input id="first_name" name="first_name" placeholder="First Name" class="form-control" required="required"
                                type="text">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="last_name" name="last_name" placeholder="Last Name" class="form-control"
                                type="text" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="Phone" name="phone" placeholder="Mobile No." class="form-control"
                                required="required" type="text">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <select id="inputState" class="form-control" style="font-family:Futura PT Book" name="hear">
                                <option value="">How Did You Hear About aplikasi.us?</option>
                                <option value="Social Network">Social Network</option>
                                <option value="Search Engine">Search Engine</option>
                                <option value="Event">Event</option>
                                <option value="Blog">Blog</option>
                                <option value="Advertisement">Advertisement</option>
                                <option value="Friend">Friend</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row align-left">
                            <div class="col-12 col-md-12 box-browse-cv align-left">
                                <span>Browse CV<input type="file" id="cv" name="cv" class="form-control" placeholder="Browse Cv"></a></span>
                                <div class="cv-append-html"><div class="in-html-append"></div></div>
                                <label class="file-note">Please select only doc or pdf and select file size below 2 mb</label>
                            </div>
                    </div>
                    <div class="form-row">
                        <button class="btn btn-login-blue col-12">Yes, let’s start the journey!</button>
                    </div>
                </form>
                <div class="form-row">
                    <div class="form-group col-md-12 py-1">
                        <label class="form-check-label" for="invalidCheck2">
                            <small>By signing up you agree to our <a href="{{route('termsConditions')}}" target="_blank">Terms of Use</a> and <a href="{{route('privacyPolicy')}}" target="_blank">Privacy Policy</a>.</small>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="intro-text py-2"><h6><a href="{{route('new-client-registration')}}">Oops, you’re a client? Click here to sign up</a></h6></div>
    </div>
</section>
</article>
@endsection