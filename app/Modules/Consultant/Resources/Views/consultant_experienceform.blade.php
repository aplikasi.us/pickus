<div class="boxForm">
    <form method="post" action="{{route('consultantupdateProfileExprirence')}}" id="update-profileexprirence">
        <input type="hidden" name="id" value="{{$experience->id}}">
        <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold text-green">Update Profile Exprirence</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        {{csrf_field()}}
        <div class="col-md-12">
            <div class="row rs-5">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Company: </label>
                        {{ Form::text('company',$experience->company,['class' => 'form-control','id' => 'company','placeholder' => 'Enter your company name']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Industry: </label>
                        {{ Form::text('industry',$experience->industry,['class' => 'form-control','id' => 'industry','placeholder' => 'Enter your industry name']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Occupation: </label>
                        {{ Form::text('occupation',$experience->occupation,['class' => 'form-control','id' => 'occupation','placeholder' => 'Enter your occupation']) }}
                    </div>
                </div>
            </div>
            <div class="row rs-5">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Summary: </label>
                        {{ Form::textarea('summary',$experience->summary,['class' => 'form-control','id' => 'summary','placeholder' => 'Enter Summary']) }}
                    </div>
                </div>
            </div>
        </div>
        <!-- <p class="text-center pb-0">
            <input type="submit" value="Submit" class="btn btn-secondary btn-sm btn-submit">
        </p> -->
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary submit-button">Save</button>
        </div>
    </form>
</div>