@if($experience != null)
<div class="form-group">
    <label class="alignment">Company: </label>
    <input type="text" value="{{$experience->company}}" name="edited_experiencecompany"
        class="edited_experiencecompany size-space">
</div>
<div class="form-group">
    <label class="alignment">Occupation: </label>
    <input type="text" value="{{$experience->occupation}}" name="edited_experienceoccupation"
        class="edited_experienceoccupation size-space">
</div>
<div class="form-group">
    <label class="alignment">Industry: </label>
    <input type="text" value="{{$experience->industry}}" name="edited_experienceindustry "
        class="edited_experienceindustry size-space">
</div>
<div class="form-group">
<p style="font-size: 16px; font-weight: normal;">Period:</p>
    <label class="alignment">From: </label>

    @if($experience->work_duration != null || $experience->work_duration != "")
    @php
    $workDurationFrom = explode('-',$experience->work_duration);
    @endphp
    {{Form::select('work_duration_from_month',[null => "Select Month"] + $monthList,$workDurationFrom[0],['class' => 'work_duration_from_month'])}}
    {{Form::select('work_duration_from_year',[null => "Select Year"] + $yearList,$workDurationFrom[1],['class' => 'work_duration_from_year'])}}
    @else
    {{Form::select('work_duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_from_month'])}}
    {{Form::select('work_duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_from_year'])}}
    @endif
</div>
<div class="form-group">
    <label class="alignment">To: </label>
    @php $workDurationTo = []; @endphp
    @if(($experience->work_duration_to != null || $experience->work_duration_to != ""))
    @php
    $workDurationTo = explode('-',$experience->work_duration_to);
    @endphp

    @endif
    @if(!empty($workDurationTo))
    {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,$workDurationTo[0],['class' => 'work_duration_to','id' => 'work_duration_to_month'])}}
    {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,$workDurationTo[1],['class' => 'work_duration_to','id' => 'work_duration_to_year'])}}

    @elseif($experience->is_currently_working_here == "true")

    {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_to','id' => 'work_duration_to_month','disabled' => 'disabled'])}}
    {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_to','id' => 'work_duration_to_year','disabled' => 'disabled'])}}

    @else

    {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_to','id' => 'work_duration_to_month'])}}
    {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_to','id' => 'work_duration_to_year'])}}

    @endif
</div>
<div class="form-group">
    <label class="alignment">Presently Working: </label>
    @if($experience->is_currently_working_here == "true")
    <input type="checkbox" name="is_currently_working_here" class="is_currently_working_here" checked="">
    @else
    <input type="checkbox" name="is_currently_working_here" class="is_currently_working_here">
    @endif
</div>
<div class="form-group">
    <label class="alignment">Summary: </label>
    <textarea rows="4" cols="50" name="edited_experiencesummary"
        class="edited_experiencesummary">{{$experience->summary}}</textarea>
</div>
<div class="form-group">
<a href="{{route('consultantdeleteProfileExprirence',[$experience->id])}}"
                                            id="delete-experience-btn" class="buttonDefault del delete-experienceBtn"
                                            data-id="{{$experience->id}}">Delete</a>

    <input type="submit" value="Save" style="float:right;color: #4FDB73!important;" data-id="{{$experience->id}}"
        class="buttonDefault save updatebutton-consultantExperience spacing-left">
    <a href="{{route('consultant-profile')}}" style="float:right;" class="buttonDefault can">Cancel </a>
    <img src="{{admin_asset('images/loader.svg')}}" class="experience-loader" style="display: none;">
</div>
@else
<div class="form-group">
    <label class="alignment">Company: </label>
    <input type="text" name=" company" class="company size-space">
</div>
<div class="form-group">
    <label class="alignment">Occupation: </label>
    <input type="text" name="occupation" class="occupation size-space">
</div>
<div class="form-group">
    <label class="alignment">Industry: </label>
    <input type="text" name="industry" class="industry size-space">
</div>
<div class="form-group">
<p style="font-size: 16px; font-weight: normal;">Period:</p>
    <label class="alignment">From: </label>
    {{Form::select('work_duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_from_month'])}}
    {{Form::select('work_duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_from_year'])}}
</div>
<div class="form-group">
    <label class="alignment">To: </label>
    {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_to','id' => 'work_duration_to_month'])}}
    {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_to','id' => 'work_duration_to_year'])}}
</div>
<div class="form-group">
    <label class="alignment">Presently Working: </label>
    <input type="checkbox" name="is_currently_working_here" class="add_is_currently_working_here">
</div>
<div class="form-group" style="margin: 0px 0px 0px 0px!important;">
    <label class="alignment">Summary: </label>
    <textarea rows="3" cols="50" name="edited_experiencesummary" class="edited_experiencesummary"></textarea>
    
</div>
<div id="consultant-Experience-save" style="padding-top: 10px!important;">
<input type="submit" value="Save" style="float:right;color:#4FDB73!important;" 
    class="buttonDefault save savebutton-consultantExperience spacing-left">
    <a href="{{route('consultant-profile')}}"  style="float:right;" class="buttonDefault can">Cancel</a>
    <img src="{{admin_asset('images/loader.svg')}}" class="experience-loader" style="display: none;">
</div>
@endif