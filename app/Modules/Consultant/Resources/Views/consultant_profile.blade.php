@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Profile')
@section('content')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="commanContent commanContentBig border-ra-5">

            <div class="row align-items-center">
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                    <div id="profile-data">
                        <a href="javascript:;" class="btnEdit float-right consultant-profile-edit"></a>
                        <h5 class="pb-2 text-light">Candidate ID:
                            <span class="text-light">{{$candidateDetailarray['candidate_id']}}</span>
                        </h5>
                        <h2>
                            <span class=" consutant-fname"
                                data-fname="{{$candidateDetailarray['first_name']}}">{{$candidateDetailarray['first_name']}}</span>
                            <span class="consutant-lname" data-lname="{{$candidateDetailarray['last_name']}}">
                                {{$candidateDetailarray['last_name']}}</span></h2>
                        <ul class="listContract mb-1">
                            <li class="titles cons-title" data-ctitle="{{$candidateDetailarray['sap_job_title']}}">
                                {{(isset($candidateDetailarray['sap_job_title'])) ? $candidateDetailarray['sap_job_title'] : ""}}
                            </li>
                            <li class="cons-experience"
                                data-cexperience="{{(isset($candidateDetailarray['experience_in_years'])) ? $candidateDetailarray['experience_in_years'] : '' }}">
                                {{(isset($candidateDetailarray['experience_in_years'])) ? $candidateDetailarray['experience_in_years'] : "" }}
                                years experience</li>
                            <li class=" cons-category Black" data-ccategory="{{$candidateDetailarray['category']}}"> {{$candidateDetailarray['category']}}
                            </li>
                        </ul>
                        
                        <div class="row rowContract">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                                <div class="cus-iconBox cus-iconMail cons-cemail max-character-fix"
                                    data-cemail="{{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : '' }}">
                                    <a class="text-light sizeUpFutura"
                                        href="mailto:{{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : '' }}"
                                        title="{{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : '' }}">
                                        {{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : "" }}</a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                <div class="cus-iconBox cus-iconPhone cons-cphone"
                                    data-cphone="{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : '' }}">
                                    <a class="text-light sizeUpFutura"
                                        href="tel:{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : '' }}"
                                        title="{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : '' }}">{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : "" }}</a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 state-div">
                                <div class="cus-iconBox cus-iconplace inlineText sizeUpFutura"> <span class="cons-ccity"
                                        data-ccity="{{(isset($candidateDetailarray['city'])) ? $candidateDetailarray['city'] : '' }}">
                                        {{(isset($candidateDetailarray['city'])) ? $candidateDetailarray['city'] : "" }}
                                        <span class="cons-cstate"
                                            data-cstate="{{(isset($candidateDetailarray['state'])) ? $candidateDetailarray['state'] : '' }}">{{(isset($candidateDetailarray['state'])) ? $candidateDetailarray['state'] : "" }}</span>
                                </div>
                                
                            </div>
                            @if($candidateDetailarray != null)
                            <div id="consultant-profile">
                                <a href="{{route('consultant-profile')}}" class="buttonDefault can">Close</a>
                                <input type="submit" value="Save" class="buttonDefault save savebutton-consultant">
                            </div>
                            @endif
                        </div>
                        <div class="custom-file boxFileupload">
                            <form method="POST" action="" accept-charset="UTF-8" id="resumeupload-form"
                                enctype="multipart/form-data" novalidate="novalidate" class="bv-form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="file" class="custom-file-input customResume" name="customResume"
                                    id="customResume">
                                <div class="loadertest"></div><label class="btnUploadFile" for="customResume">Upload
                                    CV</label>
                                <div class="success cv-consult-sucess">Your CV has been uploaded successfully.</div>
                            </form>
                            <small class="text-red">New document upload will support file format .doc, .docx and .pdf
                                only</small>
                        </div>
                    </div>
                    <div id="update-profile-form-div"></div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 border-before-left">
                    <div class="text-center ">
                        <div class=" toggle-red mb-4">
                            @php
                            $contractCheck = "";
                            $availableText = "Unavailable";
                            $availableclass = "red";
                            if($candidateDetailarray['available_for_contract'] == 1){
                            $contractCheck = "checked";
                            $availableText = "Available";
                            $availableclass = "grey";
                            }
                            @endphp
                            <span class="avalibel-un float-left Black {{$availableclass}}">{{$availableText}}</span>
                            <label class="switch custom-switch">
                                <input type="checkbox" name='available_for_contract' class="consultant_avliable"
                                    vlaue="1" {{$contractCheck}} data-zohoid="{{$candidateDetailarray['CANDIDATEID']}}">
                                <span class="slider-round-big"></span>
                            </label>
                        </div>
                        <div class="boxUpdate">

                            @if($candidateDetailarray->publish_in_us == 1)
                            <a target="_blank"
                                href="{{route('consultant-detail',[\Crypt::encryptString($candidateDetailarray['CANDIDATEID'])])}}"
                                title="View public profile"
                                class="btn btn-block cus-btn-blue-profile btn-secondary mb-2">View Profile</a>
                            <a href="{{route('consultantupdateunpublish')}}" title="Unpublish"
                                class="btn btn-block btn-red cus-btn-red-profile mb-4">Unpublish</a>
                            @else
                            <a target="_blank"
                                href="{{route('consultant-detail',[\Crypt::encryptString($candidateDetailarray['CANDIDATEID'])])}}"
                                title="View public profile"
                                class="btn btn-block btn-secondary cus-btn-blue-profile mb-2">View profile</a>
                            <a href="{{route('consultantupdatepublish')}}" title="Publish me now!"
                                class="btn btn-block btn-green cus-btn-green-profile mb-4">Publish Me Now !</a>
                            @endif
                            <small class="text-center mt-4">Last updated:
                                {{$candidateDetailarray['updated_at']->format('d M. Y')}}</small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row row-eq-height mb-2">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="commanContent sizeUpFutura border-ra-5">
                    <h6 class="Black">Information
                       
                        <a href="#" class="btnEdit float-right  profile-infromation"></a>
              
                    </h6>
                    <ul class="listContent li-div-black pr-2">
                    
                        <li>Pay Rate/day:
                            <div class="text-red inlineText profile-br-input  float-right"
                                data-br="{{$candidateDetailarray['base_rate']}}"
                                data-zohoid="{{Auth::user()->zoho_id}}">
                                
                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                <span class="ml-1">MYR </span>
                                {{$candidateDetailarray['base_rate']}}</div>
                        </li>
                        <li>Calculated Billing Rate/day: <div class="inlineText profile-rbr-input float-right"
                                data-rbr="{{$candidateDetailarray['reserved_base_rate']}}">MYR
                                {{$candidateDetailarray['reserved_base_rate']}}</div>
                            
                        </li>
                        <li>Notice Period (days): <div class="inlineText profile-npr-input float-right"
                                data-npr="{{$candidateDetailarray['notice_period_days']}}">
                                {{$candidateDetailarray['notice_period_days']}} days</div>
                            <a href="#" class="btnUpload ml-3"></a>
                            
                        </li>
                        <li>Availability Date: <div class="inlineText  profile-date-input float-right"
                                data-date="{{($candidateDetailarray['availability_date']) ? date('d M. Y',strtotime($candidateDetailarray['availability_date'])) : '-'}}">
                                {{ ($candidateDetailarray['availability_date']) ? date('d M. Y',strtotime($candidateDetailarray['availability_date'])) : "-"}}
                            </div>
                            <a href="#" class="btnUpload ml-3"></a>
                            
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="commanContent border-ra-5 sizeUpFutura">
                    <h6 class="Black">Work Preferences
                      
                    </h6>
                    @php
                    $travelClass = $fulltimeClass = $parttimeClass = "disabled";
                    $tcheck = $fcheck = $pcheck = "checked";
                    if($candidateDetailarray->willing_to_travel == 0){
                    $tcheck ="";
                    }
                    if($candidateDetailarray->full_time == 0){
                    $fcheck = "";
                    }
                    if($candidateDetailarray->part_time == 0){
                    $pcheck = "";
                    }
                    @endphp
                    <ul class="listCheckbox profile-work">
                        <li>
                            <div class=" toggle-red">
                                <span class="  {{$fulltimeClass}}">Full time</span>
                                <label class="switch custom-switch cus-switch-small float-right">
                                    <input type="checkbox" name='fulltime' class="check-fulltime" value="full-time"
                                        title="Full time" {{$fcheck}}>
                                    <span class="slider-round-big"></span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class=" toggle-red">
                                <span class="  {{$parttimeClass}}">Part time</span>
                                <label class="switch custom-switch cus-switch-small float-right">
                                    <input type="checkbox" name='parttime' class="check-parttime" value="Part-time"
                                        title="Part time" {{$pcheck}}>
                                    <span class="slider-round-big"></span>
                                </label>
                            </div>
                           
                        </li>
                        <li>
                            <div class=" toggle-red">
                                <span class="  {{$travelClass}}">Travel</span>
                                <label class="switch custom-switch cus-switch-small float-right ml-0">
                                    <input type="checkbox" name='willingtravel' class="check-willingtravel"
                                        value="willing-travel" title="Willing to travel" {{$tcheck}}>
                                    <span class="slider-round-big"></span>
                                </label>
                            </div>
                            
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="commanContent border-ra-5 sizeUpFutura">
                    <h6 class="Black">Role Preferences
                        
                    </h6>
                    <ul class="listCheckbox profile-role">
                        @php
                        $projectCheck = $supportCheck = "";
                        $dsiableproject = $dsiablesupport = "disabled";
                        if($candidateDetailarray->project == 1){
                        $projectCheck = "checked";
                        }
                        if($candidateDetailarray->support == 1){
                        $supportCheck = "checked";
                        }
                        @endphp
                        <li>
                            <div class=" toggle-red">
                                <span class=" {{$travelClass}}">Project</span>
                                <label class="switch custom-switch cus-switch-small float-right ml-0">
                                    <input type="checkbox" name='project' class="check-project" value="project"
                                        title="Project" {{$projectCheck}}>
                                    <span class="slider-round-big"></span>
                                </label>
                            </div>
                            
                        <li>
                            <div class=" toggle-red">
                                <span class=" {{$travelClass}}">Support</span>
                                <label class="switch custom-switch cus-switch-small float-right ml-0">
                                    <input type="checkbox" name='support' class="check-support" value="support"
                                        title="Support" {{$supportCheck}}>
                                    <span class="slider-round-big"></span>
                                </label>
                            </div>
                            
                    </ul>
                </div>
            </div>
        </div>
        <div class="commanContent spacing20 border-top-red  border-ra-5">
            <h3 class="border-btm-40 colorRed">Summary

                <a href="#" class="btnUpload ml-2">
                    <img class="" src="{{admin_asset('images/three-dots.png')}}">
                </a>
                @if($candidateDetailarray['is_summary_requested'] == 1)
                <a href="javascript:void(0)" class="pending-approval float-right">Pending Approval </a>
                @else
                
                <a href="javascript:void(0)" class="btnEdit float-right" id="edit-summary-consutant">
                    
                </a>
                @endif
                
            </h3>
            <p class="titles edit-summary-value sizeUpFutura"
                data-summary="{{($candidateDetailarray['additional_info']) ? $candidateDetailarray['additional_info'] : ''}}">
                {!! nl2br($candidateDetailarray['additional_info']) !!}</p>
            <div id="consultant-summary-save" style="margin-bottom:8%!important;">
                <input type="submit" value="Save" style="float:right;color: #4FDB73!important;" class="savebutton-consultantsummary buttonDefault save spacing-left">
                <a href="{{route('consultant-profile')}}" style="float:right;" class="buttonDefault can ">Close</a>
                <img src="{{admin_asset('images/loader.svg')}}" class="profile-summary-loader" style="display: none;">
            </div>
        </div>
        
        <div class="commanContent spacing20  border-ra-5">
        <h3 class="border-btm-40 Experience-pluse">
        Skill Set
                <a href="#" class="btnUpload ml-2"></a>
                <a href="#" class="btnEdit float-right skill-value"></a>
            </h3>            
        @php
            $skills = [];
            if(isset($candidateDetailarray['skill_set'])){
            $skills = explode(",",$candidateDetailarray['skill_set']);
            }
            @endphp
            <ul class="listSkills listSkillsBig skill-profile-e">
                @if(count($skills))
                @foreach($skills as $skillValue)
                <li data-skill="{{$skillValue}}">
                    <a href="javascript:void(0)">{{$skillValue}}</a>
                </li>
                
                @endforeach
                @endif
            </ul>
            
        </div>

        <div class="commanContent spacing20  border-ra-5">
            <h3 class="border-btm-40 Experience-pluse">
                Experience <small class="text-light f-14">(10 Most Recent Only) </small>
               
                @if(count($expirenceDetails) < 10) <a href="javascript:void(0)"
                    class="f14 add-btn-right pulse-add add-tabular-data" data-type="experience"></a>
                    @endif
            </h3>
            <div class="collapse multi-collapse show mt-3" id="experienceCollapse">
                <div class="addExperience"></div>
                <div class="list-notification-style">
                    <ul class="listExperience">
                        @if($alreadyWorkingInCompany != null)
                        <input type="hidden" name="is_already_working_experience_id"
                            id="is_already_working_experience_id" value="{{$alreadyWorkingInCompany->TABULARROWID}}" />
                        @else
                        <input type="hidden" name="is_already_working_experience_id"
                            id="is_already_working_experience_id" value="" />
                        @endif
                        @php $experienceHidden = 0; @endphp
                        @if(count($expirenceDetails))

                        @foreach($expirenceDetails as $expkey => $expvalue)

                        @if($expkey > 1)
                        @php
                        $experienceHidden++;
                        @endphp
                        @endif
                        <li
                            class="{{($expkey < 3) ? 'shown-tabular' : 'hidden-tabular'}} tabular-data-list list-bullet-round">
                            <div class="experience-edit-block">
                                <div class="experience-edit-value">
                                    
                                    <!-- <a href="javascript:;" class="float-right dropdown-link-show">
                                        <img src="{{admin_asset('images/three-dots.png')}}">
                                    </a>
                                    <div class="dropdonw-link-icon">
                                        <a href="{{route('consultantdeleteProfileExprirence',[$expvalue->TABULARROWID])}}"
                                            id="delete-experience-btn" class=" float-top-right delete-experienceBtn"
                                            data-id="{{$expvalue->TABULARROWID}}"><i class="fa fa-trash"
                                                aria-hidden="true"></i></a> -->
                                        <a href="Javascript:void(0)" id="edit-experience-btn"
                                            class=" float-right edit-experienceBtn edit-tabular-data"
                                            data-id="{{$expvalue->id}}" data-type="experience"><i
                                                class="btnEdit" aria-hidden="true"></i></a>
                                    <!-- </div> -->
                                    

                                    <ul class="listContract mb-1 f-15 ">
                                        <li class=" edit-expOccupation sizeUpFutura">
                                            {{($expvalue->occupation) ? $expvalue->occupation : "-"}}</li>
                                        <li class="edit-expIndustry sizeUpFutura">
                                            {{($expvalue->industry) ? $expvalue->industry : "-"}}</li>
                                        <li class="edit-expWorkDuration sizeUpFutura">
                                            {{date('M. Y' , strtotime('01-'.$expvalue->work_duration))}}
                                            @if(!empty($expvalue->is_currently_working_here) &&
                                            $expvalue->is_currently_working_here == 'true')
                                            To Till now
                                            @else
                                            To {{date('M. Y' , strtotime('01-'.$expvalue->work_duration_to))}}
                                            @endif
                                        </li>
                                    </ul>
                                    <a href="#" class="btnUpload ml-4"></a>
                                    <p class="edit-expSumary sizeUpFutura"
                                        data-summary="{!! ($expvalue->summary) ? nl2br($expvalue->summary) : '-' !!}">
                                        {!! ($expvalue->summary) ? nl2br($expvalue->summary) : "-" !!}</p>
                                </div>
                                <div class="consultant-Experience-update">
                                    <a href="{{route('consultant-profile')}}" class="buttonDefault can">Close</a>
                                    
                                    <input type="submit" value="Update" data-id="{{$expvalue->TABULARROWID}}"
                                        class="buttonDefault save updatebutton-consultantExperience">
                                </div>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                    @if(count($expirenceDetails) > 3)
                    <div class="exprinces-show cusGray">Show {{count($expirenceDetails) - 3}} more experience <i
                            class="fa fa-chevron-down" aria-hidden="true"></i>
                    </div>
                    <div class="exprinces-hide cusGray" style="display:none;">Show less <i class="fa fa-chevron-up"
                            aria-hidden="true"></i>
                    </div>
                    @endif

                </div>
            </div>
         
        </div>
        <div class="commanContent spacing20  border-ra-5">
            <h3 class="border-btm-40 Experience-pluse">
            Certification & Training
                <a href="#" class="btnUpload ml-2"></a>
                <a href="#" class="btnEdit float-right certi-profile"></a>
            </h3>
            <div class="collapse multi-collapse show mt-3" id="certCollapse">
                <div class="addCertification"></div>
                <div class="list-notification-style">
                
            </h3>
            <div class="certi-profile-value sizeUpFutura" data-certi="{!!$candidateDetailarray['certification_and_training']!!}">
                {!! nl2br($candidateDetailarray['certification_and_training'])!!}
                
            </div>
            <div class="edit-cert"></div>
                </div>
                
            </div>
            
        </div>
<!-- end certification -->
<!-- education -->
<div class="commanContent spacing20  border-ra-5">
            <h3 class="border-btm-40 Experience-pluse">
            Education
                        @if(count($educationDetails) < 10) <a href="javascript:void(0)"
                            class="f14 add-btn-right pulse-add add-tabular-data" data-type="education"></a>
                            @endif
            </h3>
            <div class="collapse multi-collapse show mt-3" id="certCollapse">
                <div class="addCertification"></div>
                <div class="list-notification-style">
            
            <div class="collapse multi-collapse show  mt-3" id="educationCollapse">
                        <div class="addEducation">
                        </div>
                        <div class="list-notification-style">
                            @if($alreadyPersuing != null)
                            <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id"
                                value="{{$alreadyPersuing->TABULARROWID}}" />
                            @else
                            <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id" value="" />
                            @endif
                            <ul class="listExperience">
                                @if(count($educationDetails))
                                @foreach ($educationDetails as $edukey => $eduvalue)
                                <li class="{{($edukey < 2) ? 'shown-tabular' : 'hidden-tabular'}} tabular-data-list"
                                    data-id="{{$eduvalue->institute}}">
                                    <div class="education-edit-block">
                                        <div class="education-edit-value">
                                            <!-- <a href="javascript:;" class="float-right dropdown-link-show">
                                                <img src="{{admin_asset('images/three-dots.png')}}">
                                            </a> -->
                                            <!-- <div class="dropdonw-link-icon"> -->
                                                <!-- <a href="{{route('deleteProfileEducation',[$eduvalue->TABULARROWID])}}"
                                                    id="delete-experience-btn"
                                                    class=" float-top-right delete-educationBtn dropdonw-icon-inline"
                                                    data-id="{{$eduvalue->TABULARROWID}}"><i class="fa fa-trash"
                                                        aria-hidden="true"></i></a>  -->
                                                <a href="Javascript:void(0)" id="float-right"
                                                    class=" float-right edit-educationBtn edit-tabular-data"
                                                    data-id="{{$eduvalue->id}}" data-type="education"><i
                                                        class="btnEdit sendtoTop"></i></a>
                                            <!-- </div> -->
                                            <span class="pb-0 edit-eduInstitute sizeUpFutura deep-color"
                                                data-institute="{{$eduvalue->institute}}">{{$eduvalue->institute}}</span><br>
                                            <span class="pb-0 edit-eduDepartment sizeUpFutura "
                                                data-department="{{$eduvalue->department}}">{{$eduvalue->department}}</span><br>
                                            <span class="pb-0 edit-eduDegree sizeUpFutura"
                                                data-degree="{{$eduvalue->degree}}">{{$eduvalue->degree}}</span><br>
                                            <p> <span class="edit-eduDurationForm sizeUpFutura"
                                                    data-durationForm="{{date('Y',strtotime('01-'.$eduvalue->duration_from))}}">{{date('Y',strtotime('01-'.$eduvalue->duration_from))}}</span>
                                                - <span class="edit-eduDurationTo sizeUpFutura"
                                                    data-durationTo="{{($eduvalue->duration_to) ? date('Y',strtotime('01-'.$eduvalue->duration_to)): 'Current'}}">{{($eduvalue->duration_to) ? date('Y',strtotime('01-'.$eduvalue->duration_to)): "Current"}}</span>
                                            </p>
                                        </div>
                                        <div class="consultant-Education-update">
                                            <a href="{{route('consultant-profile')}}"
                                                class="btn btn-danger btn-sm">Close</a>
                                                
                                            <input type="submit" value="Update" data-id="{{$eduvalue->TABULARROWID}}"
                                                class="btn btn-green btn-sm updatebutton-consultantEducation">
                                        </div>
                                    </div>
                                    
                                </li>
                                @endforeach
                                @endif
                            </ul>
                            @if(count($educationDetails) > 2)
                            <div class="education-show cusGray">Show {{count($educationDetails) - 2 }} more education <i
                                    class="fa fa-chevron-down" aria-hidden="true"></i>
                            </div>
                            <div class="education-hide cusGray" style="display:none;">Show less <i
                                    class="fa fa-chevron-up" aria-hidden="true"></i>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
<!-- sni -->
<div class="commanContent spacing20  border-ra-5">
            <h3 class="border-btm-40 Experience-pluse">
            References     
                        @if(count($referenceDetails) < 10) <a href="javascript:void(0)"
                            class="f14 add-btn-right pulse-add add-tabular-data" data-type="reference"></a>
                            @endif
            </h3>
            
            <div class="collapse multi-collapse show mt-3" id="referencesCollapse">
                        <div class="addReference"></div>
                        <div class="list-notification-style">
                            <ul class="listExperience">
                                @if(count($referenceDetails))
                                @foreach ($referenceDetails as $refkey => $refevalue)

                                <li class="{{($refkey < 2) ? 'shown-tabular' : 'hidden-tabular'}} tabular-data-list">
                                    <div class="refernce-edit-block">
                                        <div class="refernce-edit-value">
                                            <!-- <a href="javascript:;" class="float-right dropdown-link-show">
                                                <img src="{{admin_asset('images/three-dots.png')}}">
                                            </a>
                                            <div class="dropdonw-link-icon">
                                                <a href="{{route('consultantdeleteProfileReference',[$refevalue->TABULARROWID])}}"
                                                    id="delete-refrence-btn"
                                                    class="float-top-right delete-referenceBtn dropdonw-icon-inline"
                                                    data-id="{{$refevalue->TABULARROWID}}">
                                                    
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a> -->
                                                <a href="Javascript:void(0)" id="edit-referencesBtn"
                                                    class=" float-right edit-referencesBtn edit-tabular-data dropdonw-icon-inline"
                                                    data-id="{{$refevalue->id}}" data-type="reference"><i
                                                        class="btnEdit" aria-hidden="true"></i></a>
                                            <!-- </div> -->
                                            <p class="pb-0 edit-refName Black sizeUpFutura" data-name="{{$refevalue->name}}">
                                                {{$refevalue->name}}</p>
                                            <p class="pb-0 sizeUpFutura"><span class="edit-refPosition"
                                                    data-position="{{$refevalue->position}}">{{$refevalue->position}}</span><span
                                                    class="edit-refCompany" data-company="{{$refevalue->company}}">,
                                                    {{$refevalue->company}}</span></p>
                                            <p class="pb-0 sizeUpFutura"><a class="text-light" href="tel: {{$refevalue->phone}}"
                                                    title="{{$refevalue->phone}}" class="edit-refPhone"
                                                    data-phone="{{$refevalue->phone}}">{{$refevalue->phone}} </a> <a
                                                    class="text-light" href="mailto:{{$refevalue->email}}"
                                                    title="{{$refevalue->email}}" class="edit-refEmail"
                                                    data-email="{{$refevalue->email}}">| {{$refevalue->email}}</a></p>
                                           
                                        </div>
                                        <div class="consultant-Reference-update">
                                            <a href="{{route('consultant-profile')}}"
                                                class="btn btn-danger btn-sm">Close</a>
                                            <input type="submit" value="Update" data-id="{{$refevalue->id}}"
                                                class="buttonDefault save updatebutton-consultantReference">
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                            @if(count($referenceDetails) > 2)
                            <div class="reference-show cusGray">Show {{count($referenceDetails) - 2}} more references <i
                                    class="fa fa-chevron-down" aria-hidden="true"></i>
                            </div>
                            <div class="reference-hide cusGray" style="display:none;">Show less <i
                                    class="fa fa-chevron-up" aria-hidden="true"></i>
                            </div>
                            @endif
                        </div>
                    </div>
            
        </div>
<!-- end -->

    </div>
    
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h3 class="header-red-bg colorWhite text-left pd-5 how-much-left text-center">What's Your Pay Rate/day?</h3>
        <div class="commanContent boxHowmuch">
            <div class="contentHowmuch">
                <p class="pb-1">Enter Your Expected Daily Rate (DR) <a href="javascript:;" class="iconInfo">
                        <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                    </a></p>
                <div class="iconBox inlineText">MYR</div>
                <input type="text" class="text-right" value="" id="c2-daily-rate" maxlength="10" />
                <span>/day</span>
                <p class="pt-1 pb-0">
                    <span class="f-12">What You Will Actually Get</span>
                </p>
                <h5 class="text-red f-19 pb-0" id="c2-actual-rate">MYR 0.00</h5>
                <small class="text-light f-9">subject to Length of Contract</small>
            </div>
            <div class="row f-12 rs-5">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <p class="pb-2 f-14">Length of contract
                        <a href="#" class="iconInfo">
                            <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                        </a>
                    </p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="radio" name="months" class="c2_months" value="1" title="0-5 months" /> 0-5
                            months</li>
                        <li>
                            <input type="radio" name="months" class="c2_months" value="2" title="6-11 months"
                                checked="" /> 6-11 months</li>
                        <li>
                            <input type="radio" name="months" class="c2_months" value="3" title="> 12 months" /> > 12
                            months</li>
                    </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <p class="pb-2 f-14">Fees</p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="checkbox" class="c2-fees" name="c2_admin_fee" title="Admin fee"
                                id="c2-admin-fee" checked="" disabled="" /> Admin fee
                            <a href="#" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                        <li>
                            <input type="checkbox" class="c2-fees" name="c2_guaranteed_payment"
                                title="Guaranteed on-time payment" id="c2_guaranteed_payment" checked="" /> Guaranteed
                            on-time payment
                            <a href="#" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="boxDoyouKnow mb-0">
                <h6 class="f-14 mb-0">Your Pay Rate/day is</h6>
                <h2 class="text-red">
                    
                    <div class="cus-vertical-table text-center">
                        
                        <img class="cus-br-icon-mid" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span id="c2-base-rate" class="text-red">MYR 0.00</span>
                    </div>
                </h2>
                <small class="text-light">Use this BR in your profile!</small>
            </div>
        </div>
        
        
        

        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>

<!--------------------edit profile--------------->

<div class="modal fade" id="edit-profile" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">

            <div class="boxForm">
                <form method="post" action="{{route('consultanteditprofile')}}" id="candidateprofile">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Update Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    {{csrf_field()}}
                    <div class="col-md-12">
                        <div class="row rs-5">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label class="control-label">First Name: </label>
                                    {{ Form::text('first_name',$candidateDetailarray->first_name,['class' => 'form-control','id' => 'first_name','placeholder' => 'Enter First Name']) }}
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label class="control-label">Last Name: </label>
                                    {{ Form::text('last_name',$candidateDetailarray->last_name,['class' => 'form-control','id' => 'first_name','placeholder' => 'Enter Last Name']) }}
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label class="control-label">Mobile Number: </label>
                                    {{ Form::text('mobile_number',$candidateDetailarray->mobile_number,['class' => 'form-control','id' => 'mobile_number','placeholder' => 'Enter Mobile Number','maxlength' => 10]) }}
                                </div>
                            </div>
                        </div>
                        <div class="row rs-5">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Sap Job Title: </label>
                                    {{ Form::text('sap_job_title',$candidateDetailarray->sap_job_title,['class' => 'form-control','id' => 'sap_job_title','placeholder' => 'Enter Sap Job Title']) }}
                                </div>
                            </div>

                        </div>
                        <div class="row rs-5">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label class="control-label">Category: </label>
                                    {{Form::select('category',$jobCategoryList,$candidateDetailarray->category,['class' => 'form-control select2','required'])}}
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label>Experience Required (Years)</label>
                                    {{ Form::text('experience_in_years',$candidateDetailarray->experience_in_years,['class' => 'form-control','id' => 'experience_in_years','placeholder' => 'Enter Experience in year']) }}
                                </div>
                            </div>
                        </div>
                        
                        <div class="row rs-5">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>Country</label>
                                    {{ Form::text('country',$candidateDetailarray->country,['class' => 'form-control','id' => 'country','placeholder' => 'Enter Country']) }}
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label class="control-label">State: </label>
                                    {{ Form::text('state',$candidateDetailarray->state,['class' => 'form-control','id' => 'state','placeholder' => 'Enter State']) }}
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>City</label>
                                    {{ Form::text('city',$candidateDetailarray->city,['class' => 'form-control','id' => 'city','placeholder' => 'Enter city']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary submit-button">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!--------------------edit Summary--------------->

<div class="modal fade" id="edit-summary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">

            <div class="boxForm">
                <form method="post" action="{{route('consultanteditprofilesummary')}}" id="candidateprofilesummary">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Update Profile Summary</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    {{csrf_field()}}
                    <div class="col-md-12">
                        <div class="row rs-5">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Summary: </label>
                                    {{ Form::textarea('additional_info',$candidateDetailarray->additional_info,['class' => 'form-control','id' => 'additional_info','placeholder' => 'Enter Summary']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary submit-button">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>




<!--------------------add new Experience--------------->
<div class="modal fade" id="add-experience" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div id="add-experience-content">
                <div class="boxForm">
                    <form method="post" action="{{route('addProfileExprirence')}}" id="update-profileexprirence">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold text-green">Update Profile Summary</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <div class="row rs-5">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">: </label>
                                        {{ Form::text('company','',['class' => 'form-control','id' => 'company','placeholder' => 'Enter your company name']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Industry: </label>
                                        {{ Form::text('industry','',['class' => 'form-control','id' => 'industry','placeholder' => 'Enter your industry name']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Occupation: </label>
                                        {{ Form::text('occupation','',['class' => 'form-control','id' => 'occupation','placeholder' => 'Enter your occupation']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row rs-5">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Summary: </label>
                                        {{ Form::textarea('summary','',['class' => 'form-control','id' => 'summary','placeholder' => 'Enter Summary']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary submit-button">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------------------end  Experience--------------->
<!--------------------edit Experience--------------->

<div class="modal fade" id="edit-experience" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div id="edit-experience-content">

            </div>
        </div>
    </div>
</div>

<!--------------------add new education--------------->
<div class="modal fade" id="add-education" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div id="add-education-content">
                <div class="boxForm">
                    <form method="post" action="{{route('addProfileEducation')}}" id="update-profileeducation">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold text-green">Add Educations</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <div class="row rs-5">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Institute: </label>
                                        {{ Form::text('institute','',['class' => 'form-control','id' => 'institute','placeholder' => 'Enter your Institute name']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Department: </label>
                                        {{ Form::text('department','',['class' => 'form-control','id' => 'department','placeholder' => 'Enter your Department name']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Degree: </label>
                                        {{ Form::text('degree','',['class' => 'form-control','id' => 'degree','placeholder' => 'Enter your degree']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row rs-5">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="control-label">Duration From : </label>
                                        {{ Form::text('duration_from','',['class' => 'form-control','id' => 'duration_from','placeholder' => 'Enter your duration from date','autocomplete' => 'off','readonly']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="control-label">Duration To: </label>
                                        {{ Form::text('duration_to','',['class' => 'form-control','id' => 'duration_to','placeholder' => 'Enter your duration to date','autocomplete' => 'off','readonly']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary submit-button">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------------------end  education--------------->

<!--------------------education Experience--------------->

<div class="modal fade" id="edit-education" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div id="edit-education-content">

            </div>
        </div>
    </div>
</div>

<!--------------------add new References--------------->
<div class="modal fade" id="add-references" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div id="add-references-content">
                <div class="boxForm">
                    <form method="post" action="{{route('addProfileReferences')}}" id="update-profileexprirence">

                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold text-green">Add Profile References</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <div class="row rs-5">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Name: </label>
                                        {{ Form::text('name','',['class' => 'form-control','id' => 'name','placeholder' => 'Enter your name']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Position: </label>
                                        {{ Form::text('position','',['class' => 'form-control','id' => 'position','placeholder' => 'Enter your position']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label class="control-label">Company: </label>
                                        {{ Form::text('company','',['class' => 'form-control','id' => 'company','placeholder' => 'Enter your company']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row rs-5">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="control-label">Mobile Number : </label>
                                        {{ Form::text('phone','',['class' => 'form-control','id' => 'phone','placeholder' => 'Enter your mobile number']) }}
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label class="control-label">Email: </label>
                                        {{ Form::text('email','',['class' => 'form-control','id' => 'email','placeholder' => 'Enter your email date']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary submit-button">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------------------end  References--------------->

<!--------------------education References--------------->

<div class="modal fade" id="edit-references" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div id="edit-references-content">

            </div>
        </div>
    </div>
</div>


@endsection