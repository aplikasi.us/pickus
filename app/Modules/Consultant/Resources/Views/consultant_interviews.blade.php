@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Interviews')
@section('content')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <h2>INTERVIEW</h2>
        <div class="tabsInterviews">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#upcomingInterviews" role="tab">Upcoming
                        Interviews
                        ({{$interviews['upComingInterviewCount']}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-toggle="tab" href="#concludedInterviews"
                        role="tab">Concluded Interviews
                        ({{count($interviews['interviewList']['concluded_interviews'])}})</a>
                </li>
            </ul>
            <div class="tab-content border-ra-5">
                <div class="tab-pane fade show active" id="upcomingInterviews">
                    @if(count($interviews['interviewList']['upcoming_interviews']))
                    @foreach($interviews['interviewList']['upcoming_interviews'] as $upcomingKey => $upcomingValue)
                    <div class="boxInterviewscontent">
                        <div class="row rowInterviews">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5">
                                <span title="In Progress" class="btn-sm btn-tag-md btnBlue">In Progress</span>
                                <ul class="listContract">
                                    <li>{{$upcomingValue['job']['Job Opening ID']}}</li>
                                    <li>{{(isset($upcomingValue['job']['Job Type'])) ? $upcomingValue['job']['Job Type'] : ""}}
                                    </li>
                                </ul>
                                <h4 class="titles Black text-we-noraml pb-2">{{$upcomingValue['job']['Posting Title']}}
                                </h4>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                        <div class="text-red">
                                            <!-- <span class="cus-br-icon">BR</span>-->
                                            <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                            <span class="ml-1">MYR </span>
                                            {{(isset($upcomingValue['job']['Job Base Rate'])) ? $upcomingValue['job']['Job Base Rate'] : "0.00"}}/day
                                        </div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($upcomingValue['job']['Job Mode'])) ? $upcomingValue['job']['Job Mode'] : ""}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="ml-1 Black d-inline">
                                            <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                            {{(isset($upcomingValue['job']['Work Experience'])) ? $upcomingValue['job']['Work Experience'] : ""}}
                                            exp.</div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($upcomingValue['job']['Date Opened'])) ? date('d M. Y',strtotime($upcomingValue['job']['Date Opened']->format('Y-m-d'))) : ""}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($upcomingValue['job']['Job Duration (months)'])) ? $upcomingValue['job']['Job Duration (months)'] : ""}}
                                            months</div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($upcomingValue['job']['Industry'])) ? $upcomingValue['job']['Industry'] : ""}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($upcomingValue['job']['City'])) ? $upcomingValue['job']['City'] : ""}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 contentHCL ">

                                <div id="carouselInterviews-{{$upcomingKey}}" class="carousel slide carouselHCL"
                                    data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">

                                            <h6 class="pb-2 Black">Interview with <span
                                                    class="Black">{{$upcomingValue['candidate']['First Name'].' '.$upcomingValue['candidate']['Last Name']}}</span>
                                            </h6>
                                            <ul class="listSimple">
                                                <li>
                                                    <span class="textLight interview-right">What:</span> <span
                                                        class="interview-right Black">{{$upcomingValue['Type']}}</span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Where:</span>
                                                    <span class="interview-right Black">
                                                        @if(isset($upcomingValue['Venue']))
                                                        {{$upcomingValue['Venue']}}
                                                        @else
                                                        -
                                                        @endif
                                                    </span></li>
                                                <li>
                                                    <span class="textLight">Client:</span>
                                                    <span
                                                        class="interview-right Black">{{$upcomingValue['job']['Client Name']}}</span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Contact Name:</span> <span
                                                        class="interview-right Black">{{$upcomingValue['candidate']['First Name'].' '.$upcomingValue['candidate']['Last Name']}}</span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Contact No.:</span>
                                                    <span class="interview-right Black">
                                                        <a href="tel:{{(isset($upcomingValue['candidate']['Mobile'])) ? $upcomingValue['candidate']['Mobile'] : ""}}"
                                                            title="{{(isset($upcomingValue['candidate']['Mobile'])) ? $upcomingValue['candidate']['Mobile'] : ""}}">{{ (isset($upcomingValue['candidate']['Mobile'])) ? $upcomingValue['candidate']['Mobile'] : ""}}</a>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Email:</span>
                                                    <span class="interview-right Black">
                                                        <a href="javascript:void(0)"
                                                            title="{{(isset($upcomingValue['candidate']['Email'])) ? $upcomingValue['candidate']['Email'] : ""}}">{{(isset($upcomingValue['candidate']['Email'])) ? $upcomingValue['candidate']['Email'] : ""}}</a>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--                                        <div class="carousel-item">
                                                                                    <p class="pb-2">Interview with {{$upcomingValue['candidate']['First Name'].' '.$upcomingValue['candidate']['Last Name']}}</p>
                                                                                    <ul class="listSimple">                                                
                                                                                        <li>
                                                                                            <span class="textLight">Note:</span> 
                                                                                            {{'Note Goes Here'}}
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>-->
                                    </div>

                                    <!--                                    <ol class="carousel-indicators">
                                                                            <li data-target="#carouselInterviews-{{$upcomingKey}}" data-slide-to="0" class="active"></li>
                                                                            <li data-target="#carouselInterviews-{{$upcomingKey}}" data-slide-to="1"></li>
                                                                        </ol>-->
                                </div>

                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 boxCalander ">
                                <h5 class="textLight">
                                    {{ (isset($upcomingValue['Start DateTime'])) ? date('h:i A, l',strtotime($upcomingValue['Start DateTime']->format('Y-m-d H:i:s'))) : ""}}
                                </h5>
                                <h2 class="Black">
                                    {{ (isset($upcomingValue['Start DateTime'])) ? date('d',strtotime($upcomingValue['Start DateTime']->format('Y-m-d H:i:s'))) : ""}}
                                </h2>
                                <h5 class="textLight font-bold">
                                    {{ (isset($upcomingValue['Start DateTime'])) ? date('F , Y',strtotime($upcomingValue['Start DateTime']->format('Y-m-d H:i:s'))) : ""}}
                                </h5>
                                <a title="Add to Calendar" href="javascript:void(0)"
                                    class="btn btn-block btn-primary add-to-celender btnSkyblue"
                                    data-title='{{$upcomingValue['job']['Posting Title']}}'
                                    data-date='{{date('Ymd',strtotime($upcomingValue['Start DateTime']->format('Y-m-d')))}}T133000Z'
                                    data-description='Interview with {{$upcomingValue['candidate']['First Name'].' '.$upcomingValue['candidate']['Last Name']}}'
                                    data-location='@if(isset($upcomingValue[' Venue'])) {{$upcomingValue['Venue']}}
                                    @else - @endif'>Add to
                                    Calendar</a>
                            </div>
                            <div class="col-12 interview-note-show">

                                <a title="Notes" href="javascript:void(0)"
                                    class=" interview-note-link btn-sm btn-tag-md btnRed"
                                    data-id='{{$upcomingValue['INTERVIEWID']}}' data-container-id='{{$upcomingKey}}'
                                    data-container-module='upcoming'>Notes</a>
                                <img src="{{admin_asset('images/loader.svg')}}" class="interview-loader"
                                    style="display: none;">
                                <div class="notes-container" id="notes-container-up-upcoming-{{$upcomingKey}}">

                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
                <div class="tab-pane fade" id="concludedInterviews">
                    @if(count($interviews['interviewList']['concluded_interviews']))
                    @foreach($interviews['interviewList']['concluded_interviews'] as $concludedKey => $concludedValue)
                    <div class="boxInterviewscontent">
                        <div class="row rowInterviews">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5">
                                @if(isset($concludedValue['Interview Status']))
                                @if($concludedValue['Interview Status'] == "Selected")
                                <a title="Selected" href="#" class=" btn-tag-md btnLightGreen colorWhite">Selected</a>
                                @elseif($concludedValue['Interview Status'] == "Rejected")
                                <a title="Rejected" href="#" class="btn-tag-md btnBlack colorWhite ">Rejected</a>
                                @elseif($concludedValue['Interview Status'] == "No Show")
                                <a title="Selected" href="#" class="btn-tag-md btnGray colorWhite ">No Show</a>
                                @else
                                <a title="Cancelled" href="#" class="btn-tag-md btnRed colorWhite ">Cancelled</a>
                                @endif
                                @else
                                <a title="Pending" href="javascript:;"
                                    class="btn-tag-md status-associated colorWhite ">Pending</a>
                                @endif
                                <ul class="listContract">
                                    <li>{{$concludedValue['job']['Job Opening ID']}}</li>
                                    <li>{{(isset($concludedValue['job']['Job Type'])) ? $concludedValue['job']['Job Type'] : ""}}
                                    </li>
                                </ul>
                                <h5 class="titles Black">{{$concludedValue['job']['Posting Title']}}</h5>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                        <div class=" text-red">
                                            <!-- <span class="cus-br-icon">BR</span>-->
                                            <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                            <span class="ml-1">MYR </span>
                                            {{(isset($concludedValue['job']['Job Base Rate'])) ? $concludedValue['job']['Job Base Rate'] : "0.00"}}/day
                                        </div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($concludedValue['job']['Job Mode'])) ? $concludedValue['job']['Job Mode'] : ""}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($concludedValue['job']['Work Experience'])) ? $concludedValue['job']['Work Experience'] : ""}}
                                            exp.</div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($concludedValue['job']['Date Opened'])) ? date('d M. Y',strtotime($concludedValue['job']['Date Opened']->format('Y-m-d'))) : ""}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($concludedValue['job']['Job Duration (months)'])) ? $concludedValue['job']['Job Duration (months)'] : ""}}
                                            months</div>
                                    </div>
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($concludedValue['job']['Industry'])) ? $concludedValue['job']['Industry'] : ""}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{(isset($concludedValue['job']['City'])) ? $concludedValue['job']['City'] : ""}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 contentHCL">
                                <!-- <img src="{{admin_asset('images/loader.svg')}}" class="interview-loader" style="display: none;">
                                    <a title="Notes" href="javascript:void(0)" class=" btn-tag-md btnRed interview-note-link" data-id='{{$concludedValue['INTERVIEWID']}}' data-container-id='{{$concludedKey}}' data-container-module='concluded'>Notes</a> -->
                                <div id="carouselInterviews-c1{{$concludedKey}}" class="carousel slide carouselHCL"
                                    data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <p class="pb-2 Black">Interview with <span
                                                    class="Black">{{$concludedValue['candidate']['First Name'].' '.$concludedValue['candidate']['Last Name']}}</span>
                                            </p>
                                            <ul class="listSimple">
                                                <li>
                                                    <span class="textLight">What:</span>
                                                    <span class="interview-right Black">
                                                        {{$concludedValue['Type']}}</span> </li>
                                                <li>
                                                    <span class="textLight">Where:</span>
                                                    <span class="interview-right Black">
                                                        @if(isset($concludedValue['Venue']))
                                                        {{$concludedValue['Venue']}}
                                                        @else
                                                        -
                                                        @endif
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Contact Name:</span>
                                                    <span class="interview-right Black">
                                                        {{$concludedValue['candidate']['First Name'].' '.$concludedValue['candidate']['Last Name']}}</span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Contact No.:</span>
                                                    <span class="interview-right Black"> <a
                                                            href="tel:{{(isset($concludedValue['candidate']['Mobile'])) ? $concludedValue['candidate']['Mobile'] : ""}}"
                                                            title="{{(isset($concludedValue['candidate']['Mobile'])) ? $concludedValue['candidate']['Mobile'] : ""}}">{{ (isset($concludedValue['candidate']['Mobile'])) ? $concludedValue['candidate']['Mobile'] : ""}}</a></span>
                                                </li>
                                                <li>
                                                    <span class="textLight">Email:</span>
                                                    <span class="interview-right Black">
                                                        <a href="javascript:void(0)"
                                                            title="{{(isset($concludedValue['candidate']['Email'])) ? $concludedValue['candidate']['Email'] : ""}}">{{(isset($concludedValue['candidate']['Email'])) ? $concludedValue['candidate']['Email'] : ""}}</a>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--                                        <div class="carousel-item">
                                                                                    <p class="pb-2">Interview with {{$concludedValue['candidate']['First Name'].' '.$concludedValue['candidate']['Last Name']}}</p>
                                                                                    <ul class="listSimple">                                                
                                                                                        <li>
                                                                                            <span class="textLight">Note:</span> 
                                                                                            {{'Note Goes Here'}}
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>-->
                                    </div>
                                    <!--                                    <ol class="carousel-indicators">
                                                                            <li data-target="#carouselInterviews-c1{{$concludedKey}}" data-slide-to="0" class="active"></li>
                                                                            <li data-target="#carouselInterviews-c1{{$concludedKey}}" data-slide-to="1"></li>
                                                                        </ol>-->
                                </div>

                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 boxCalander">
                                <h5 class="textLight">
                                    {{ (isset($concludedValue['Start DateTime'])) ? date('h:i A, l',strtotime($concludedValue['Start DateTime']->format('Y-m-d H:i:s'))) : ""}}
                                </h5>
                                <h2 class="Black">
                                    {{ (isset($concludedValue['Start DateTime'])) ? date('d',strtotime($concludedValue['Start DateTime']->format('Y-m-d H:i:s'))) : ""}}
                                </h2>
                                <h5 class="textLight font-bold">
                                    {{ (isset($concludedValue['Start DateTime'])) ? date('F , Y',strtotime($concludedValue['Start DateTime']->format('Y-m-d H:i:s'))) : ""}}
                                </h5>
                            </div>
                            <!-- <div class="col-12 interview-note-show">
                                <a title="Notes" href="javascript:void(0)"
                                    class=" interview-note-link btn-sm btn-tag-md btnRed"
                                    data-id='{{$concludedValue['INTERVIEWID']}}' data-container-id='{{$concludedKey}}'
                                    data-container-module='upcoming'>Notes</a>
                                <img src="{{admin_asset('images/loader.svg')}}" class="interview-loader"
                                    style="display: none;">
                                <div class="notes-container" id="notes-container-up-upcoming-{{$concludedKey}}">

                                </div>
                            </div> -->
                        </div>
                        <!-- <div class="boxbtns">
                            <div class="row no-gutters">
                                <div class="col-md-12 text-center">
                                    <img src="{{admin_asset('images/loader.svg')}}" class="interview-loader"
                                        style="display: none;">
                                    <a title="Notes" href="javascript:void(0)"
                                        class="btn btn-danger btn-block interview-note-link"
                                        data-id='{{$concludedValue['INTERVIEWID']}}'
                                        data-container-id='{{$concludedKey}}'
                                        data-container-module='concluded'>Notes</a>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="notes-container" id="notes-container-up-concluded-{{$concludedKey}}">

                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h5 class="text-center pt-2 textDarkBlue">Jobs You Might Like...</h5>
        @if(count($jobsMightLike))
        @foreach($jobsMightLike as $jobKey => $jobValue)
        <div class="commanContent mb-3">
            <a href="{{route('consultantjobdetails',[\Crypt::encryptString($jobValue->id)])}}">
                <ul class="listContract">
                    <li class="Black">{{$jobValue->Job_ID}}</li>
                    <li class="Black">{{$jobValue->job_type}}</li>
                </ul>
                <h5 class="titles Black font-bold">{{$jobValue->posting_title}}</h5>
            </a>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="cus-verticale-center">  
                        <div class="text-red">
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                        {{$jobValue->job_base_rate}}/day</div>
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconCopy"></div>
                </div> -->
            </div>
            <div class="row rowContract">
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->job_mode}}</div>
                </div>
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                        <div class="ml-1 d-inline Black">{{$jobValue->job_duration}} months</div>
                    </div>

            </div>

            <div class="row rowContract">
                
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->work_experience}} exp.</div>
                </div>    
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->industry}}</div>
                </div>
                    
            </div>

            <div class="row rowContract">

                

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                        <div class="ml-1 d-inline Black">{{date('d M. Y',strtotime($jobValue->date_opened))}}</div>
                    </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->city}}</div>
                </div>
        
            </div>

            <div class="boxbtns job-tile-button-right">
                <div class="row no-gutters border-top-btn">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 border-right-btn">
                        <a title="Save" href="JavaScript:void(0);"
                            class="btn btn-secondary btn-block btnSave consultant-job-saved consultant-job-saved colorBlue btn-custom-trans"
                            data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}"
                            data-redirect="true">Save</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                        <a title="Apply Now!" href="javaScript:void(0)"
                            class="btn btn-danger btn-block btnApply consultant-job-applied btn-custom-trans colorRed"
                            data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="text-right mb-4">
            <a href="{{route('general-search')}}" class="linkSeealla text-red" title="See All">See All ></a>
            <!-- <a href="{{url('/consultant/search?search_type=job&job_might_like=1&jobcategory='.$jobValue->category)}}" class="linkSeealla text-red" title="See All">See All ></a> -->
        </div>
        @endif
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>
<div class="modal fade" id="add-to-calender-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="boxForm">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold text-green">Add to Calendar</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3 calender-body">
                    <div class="row text-center">
                        <div class="col-md-4">
                            <a href="" id="google-link" target="_blank">
                                <img src="{{admin_asset('images/google.png')}}" class="socail-icon" />
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="" id="yahoo-link" target="_blank">
                                <img src="{{admin_asset('images/yahoo.png')}}" class="socail-icon" />
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="" id="ical-link" target="_blank">
                                <img src="{{admin_asset('images/ical.png')}}" class="socail-icon" />
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection