@extends('Layouts.Consultant.base')
@section('title', 'Consultant - Job Details')
@section('content')
<div class="container mb-4">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <p class="pb-3">
                <a href="{{ url()->previous() }}" title="Go Back" class="btnBack">
                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
            </p>
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                {{ Session::get('success') }}
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                {{ Session::get('error') }}
            </div>
            @endif

            @php
            $is_applied = 0;
            @endphp
            @foreach ($job->associatecandidates as $key1=>$v)
            @if ($v->CANDIDATEID == Auth::user()->zoho_id)
            @if(!empty($v->status))

            @php
            $is_applied = 1;
            @endphp
            <div class="boxProgress">
                <h5>Job Application Progress</h5>
                <ul class="listProgress">
                    <?php
                    $ScreactiveClass = $SubactiveClass = $InteactiveClass = $StaactiveClass = $HireactiveClass = "disabled";
                    $ScreshowClass = $subshowClass = $InteshowClass = $StashowClass = $HireshowClass = "hide";
                    if ($v->CANDIDATEID == Auth::user()->zoho_id) {
                        if (in_array($v->status, $candidateScreeningStatus)) {
                            $ScreactiveClass = "active";
                            $ScreshowClass = "show";
                        }
                        if (in_array($v->status, $candidateSubmissionStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $subshowClass = "show";
                        }
                        if (in_array($v->status, $candidateInterviewStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $InteactiveClass = "active";
                            $InteshowClass = "show";
                        }
                        if (in_array($v->status, $candidateOfferStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $InteactiveClass = "active";
                            $StaactiveClass = "active";
                            $StashowClass = "show";
                        }
                        if (in_array($v->status, $candidateHireStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $InteactiveClass = "active";
                            $StaactiveClass = "active";
                            $HireactiveClass = "active";
                            $HireshowClass = "show";
                        }
                    }
                    ?>
                    <li class="{{$ScreactiveClass}}">
                        <span>Screening</span>
                        <span class="text-red text-block {{$ScreshowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$SubactiveClass}}">
                        <span>Submission</span>
                        <span class="text-red text-block {{$subshowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$InteactiveClass}}">
                        <span> Interview </span>
                        <span class="text-red text-block {{$InteshowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$StaactiveClass}}">
                        <span> Offer</span>
                        <span class="text-red text-block {{$StashowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$HireactiveClass}}">
                        <span> Hire</span>
                        <span class="text-red text-block {{$HireshowClass}}">{{$v->status}}</span>
                    </li>
                </ul>
            </div>
            @endif
            @endif
            @endforeach


        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="commanContent">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <ul class="listContract">
                            <li>{{$job->Job_ID}}</li>
                            <li>{{$job->job_type}}</li>
                        </ul>
                        <h5 class="titles">{{$job->posting_title}}</h5>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        @php
                        $is_applied = 0;
                        $is_saved = 0;
                        @endphp
                        @foreach ($job->associatecandidates as $key1=>$value1)
                        @if ($value1->CANDIDATEID == Auth::user()->zoho_id)
                        @if(!empty($value1->status))
                        @php
                        $is_applied = 1;
                        @endphp
                        <div class="btnsRight">
                            <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                            @if($value1->status == "Associated")
                            <a title="Withdraw Application" href="#"
                                class="btn btn-danger btn-md btnApply withdraw-application"
                                data-zohoid="{{$job->JOBOPENINGID}}" data-id="{{$job->id}}">Withdraw Application</a>
                            @else
                            <a title="Application Withdrawn" href="javascript:void(0);"
                                class="btn btn-danger btn-md btnApply disabled">Application Withdrawn</a>
                            @endif
                        </div>
                        @endif
                        @endif
                        @endforeach
                        @if($is_applied == 0)
                        <div class="btnsRight">
                            @foreach ($job->associatecandidates as $key1=>$value1)
                            @if ($value1->CANDIDATEID == Auth::user()->zoho_id)
                            @php
                            $is_saved = 1;
                            @endphp
                            <div class="btnsRight">
                                <a title="Save" href="javascript:void(0)"
                                    class="btn btn-secondary btn-md btnSave savejob-delete"
                                    data-zohoid="{{$job->JOBOPENINGID}}">Unsave</a>
                                <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                                <a title="Apply Now!" href="JavaScript:void(0)"
                                    class="btn btn-danger btn-md btnApply consultant-job-applied"
                                    data-zohoid="{{$job->JOBOPENINGID}}" data-id="{{$job->id}}">Apply Now!</a>
                            </div>
                            @endif
                            @endforeach
                            @if($is_saved == 0)
                            <a title="Save" href="JavaScript:void(0)"
                                class="btn btn-secondary btn-md btnSave consultant-job-saved"
                                data-zohoid="{{$job->JOBOPENINGID}}" data-id="{{$job->id}}">Save</a>
                            <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                            <a title="Apply Now!" href="JavaScript:void(0)"
                                class="btn btn-danger btn-md btnApply consultant-job-applied"
                                data-zohoid="{{$job->JOBOPENINGID}}" data-id="{{$job->id}}">Apply Now!</a>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
                <div class="row rowContract">
                    @if(Auth::user())
                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-4 mob-detai-res">
                        <p class="pb-2 color6Gray"><span>Base Rate</span></p>
                        <div class="text-red">
                            <!-- <span class="cus-br-icon">BR</span> -->
                            <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                            <span class="ml-1">MYR </span>
                            <span class="c-job-base-rate"
                                id="con-copy">{{($job->job_base_rate != null) ? $job->job_base_rate : "0.00"}}</span>/day
                            <div class="iconBox iconCopy copy-to-clipboard job-tile-button-right d-inline">

                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="row rowContract">
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Job Mode</span></p>
                        <div class="iconBox iconTime">{{$job->job_mode}}</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Experience</span></p>
                        <div class="iconBox iconYears">{{$job->work_experience}} exp.</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Availability Date</span></p>
                        <div class="iconBox iconDate">{{date('d M. Y',strtotime($job->date_opened))}}</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Job Duration</span></p>
                        <div class="iconBox iconMonths">{{$job->job_duration}} months</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Industry</span></p>
                        <div class="iconBox iconGas">{{$job->industry}}</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Location</span></p>
                        <div class="iconBox iconplace">{{$job->city}}</div>
                    </div>
                </div>
                <div class="textAgo">
                    <span class="iconWeek">{{ time_elapsed_string($job->last_activity_time) }}</span>
                </div>
            </div>
            @if(isset($job['key_skills']) && !empty($job['key_skills']))
            <div class="commanContent spacing20">
                <h2 class="headingBorder text-dark">Key Skills</h2>
                @php
                $skills = [];
                if(isset($job['key_skills'])){
                $skills = explode(",",$job['key_skills']);
                }
                @endphp
                <ul class="listSkills listSkillsBig">
                    @if(count($skills))
                    @foreach($skills as $skillValue)
                    @if(!empty($skillValue))
                    <li>
                        <a href="javascript:void(0)">{{$skillValue}}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ul>
            </div>
            @endif
            <div class="commanContent spacing20 mb-3">
                <h2 class="headingBorder text-dark">Job Details</h2>
                <p class="pb-2">Job Description</p>
                <ul class="listContent listBullets">
                    @if(isset($job['job_description']) && !empty($job['job_description']))
                    <li>{!! $job['job_description'] !!}</li>
                    @endif
                </ul>
                @if(isset($job['job_requirements']) && !empty($job['job_requirements']))
                <hr />
                <p class="pb-2">Requirements</p>
                <ul class="listContent listBullets">
                    @if(isset($job['job_requirements']) && !empty(($job['job_requirements'])))
                    <li>{!! $job['job_requirements'] !!}</li>
                    @endif
                </ul>
                @endif
                @if(isset($job['job_benefits']) && !empty($job['job_benefits']))
                <hr />
                <p class="pb-2">Benefits</p>
                <ul class="listContent listBullets">
                    @if(isset($job['job_benefits']) && !empty(($job['job_benefits'])))
                    <li>{!! $job['job_benefits'] !!}</li>
                    @endif
                </ul>
                @endif
            </div>
            <ul class="listSocialMedia listSocialMediaSmall mb-4">
                <li>
                    <a href="#" title="twitter">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="facebook">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="linkedin">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <div class="commanContent spacing20 mb-4">
                <h2 class="headingBorder text-dark">Ask
                    <img src="{{admin_asset('images/textUs.png')}}" alt="" title="" />about the job!</h2>
                {{Form::open(['route' => 'job-enquiry','id' => 'enquiry-form'])}}
                {{Form::hidden('job_link',Request::url())}};
                <div class="form-group">
                    <textarea name="content" class="form-control"></textarea>
                </div>
                <p class="text-right pb-0">
                    <input type="submit" value="Submit" class="btn btn-secondary btn-sm btn-submit" />
                </p>
                {{Form::close()}}
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="commanContent boxHowmuch">
                <h3>How Much
                    <span class="text-red">Will You</span> Get?</h3>
                <div class="contentHowmuch">
                    <p class="pb-1">Enter the Job’s Base Rate</p>
                    <div class="text-red inlineText">
                        <!-- <span class="cus-br-icon">BR</span>  -->
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                    </div>
                    <input type="text" class="text-red text-right" value="" id="c1-base-rate-perday" maxlength="10" />
                    <span class="text-red">/day</span>
                </div>
                <div class="row f-12 rs-5">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Length of contract
                            <a href="#" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5
                                months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="2" title="6-11 months"
                                    checked="" /> 6-11 months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                                months</li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Fees</p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                    checked="" disabled="" /> Admin fee
                                <a href="#" class="iconInfo">
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                            <li>
                                <input type="checkbox" class="fees" name="guaranteed_payment"
                                    title="Guaranteed on-time payment" id="guaranteed_payment" checked="" /> Guaranteed
                                on-time payment
                                <a href="#" class="iconInfo">
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="boxDoyouKnow mb-0">
                    <h6 class="f-14 mb-0">Your Daily Rate Will Be</h6>
                    <h2 class="text-red" id="daily-rate">MYR 0.00</h2>
                </div>
            </div>
            @include('Layouts.General.doyouknow')
            @include('Layouts.General.tips')
        </div>
    </div>
</div>
<section class="contributesection">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="htagf">Want to contribute?</h3>
				<p class="ptagf">Feel free to drop us an email</p>
				<div class="btntagf"><a href="{{route('contactus')}}">Contact Us</a></div>
			</div>
		</div>
	</div>
</section>
<?php

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>
@endsection