@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Attachments')
@section('content')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 ">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <h2 class="d-inline text-uppercase Black">Attachments</h2>
            </div>
            <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 pull-right">
                <p class="text-right pb-0 d-inline">
                    <form method="POST" action="{{route('uploadAttachment')}}" accept-charset="UTF-8" id="customattachment-form" enctype="multipart/form-data" novalidate="novalidate" class="bv-form text-right">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="file" class="custom-file-input customattachment" name="customattachment" id="customattachment">
                        <div class="loadertest"></div><label class="btnUploadFile" for="customattachment">Upload New</label>
                    </form>
                    <p class="text-red text-right">New document upload will support file format .doc, .docx and .pdf only</p>
                </p>
            </div>
        </div>
        <div class="commanContent border-ra-5">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th scope="col">File Name</th>
                        <th scope="col" class="text-center">Attached By</th>
                        <th scope="col" class="text-center">Date Added</th>
                        <th scope="col" class="text-center">Size</th>
                        <th scope="col" class="text-center">Category</th>
                        <th scope="col" class="text-center tablewid-50"></th>
                    </tr>
                </thead>
                <tbody >
                    @if(count($attchements))
                    @foreach($attchements as $key => $value)
                    <!-- <?php 
                    echo '<pre>';
                    print_r($attchements->toArray());
                    echo '</pre>';
                    ?> -->
                    <tr>
                        <td>
                            <a href="{{route('downloadattachment',[$value->id,'attachment'])}}">{{$value->filename}}</a>
                        </td>
                        <td class="text-center">{{$value->attach_by}}</td>
                        <td class="text-center">{{date('d M, Y',strtotime($value->created_at))}}</td>
                        <td class="text-center">{{$value->size}}</td>
                        <td class="text-center">{{$value->category}}</td>
                        <td class="text-center"><a href="Javascript:;" class="three-dots attachment-dropdown"><img src="{{admin_asset('images/three-dots.png')}}"></a><div class="attachments-links"><ul class="attachments-ul"><li><a href="{{route('downloadattachment',[$value->id,'attachment'])}}" class="colorRed"><i class="fa fa-download" aria-hidden="true"></i></a></li><li><a href="{{route('deleteattachment',[$value->id,'attachment'])}}" class="colorRed delete-attachment" ><i class="fa fa-trash" aria-hidden="true"></i></a></li></ul></div></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td class="text-red text-center" colspan="5">No attachments found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <h2 class="mt-60 text-uppercase Black" >Service Agreements</h2>
        <div class="commanContent border-ra-5">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th scope="col">File Name</th>
                        <th scope="col" class="text-center">Attached By</th>
                        <th scope="col" class="text-center">Date Added</th>
                        <th scope="col" class="text-center">Size</th>
                        <th scope="col" class="text-center">Category</th>
                        <th scope="col" class="text-center tablewid-50"></th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($serviceAgreements))
                    @foreach($serviceAgreements as $key => $value)
                    @foreach($value->attachments as $key1 => $value1)
                    <tr>
                        <td>
                            <a href="{{route('downloadattachment',[$value1->attachment_id,'service-agreement'])}}">{{$value1->filename}}</a>
                        </td>
                        <td class="text-center">{{$value1->attach_by}}</td>
                        <td class="text-center">{{date('d M, Y',strtotime($value1->modified_time))}}</td>
                        <td class="text-center">{{$value1->size}}</td>
                        <td class="text-center">{{$value1->category}}</td>
                        <td class="text-center"><a href="Javascript:;" class="three-dots attachment-dropdown"><img src="{{admin_asset('images/three-dots.png')}}"></a><div class="attachments-links"><ul class="attachments-ul"><li><a href="{{route('downloadattachment',[$value1->id,'attachment'])}}" class="colorRed"><i class="fa fa-download" aria-hidden="true"></i></a></li><li><a href="{{route('deleteattachment',[$value->id,'service-agreement'])}}" class="colorRed delete-attachment" ><i class="fa fa-trash" aria-hidden="true"></i></a></li></ul></div></td>
                    </tr>
                    @endforeach
                    @endforeach
                    @else
                    <tr>
                        <td class="text-red text-center" colspan="5">No attachments found</td>
                    </tr>
                    @endif
                </tbody>
            </table>            
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h5 class="text-left pt-2 textDarkBlue">Jobs You Might Like...</h5>
        @if(count($jobsMightLike))
        @foreach($jobsMightLike as $jobKey => $jobValue)
        <div class="commanContent mb-3">
            <a href="{{route('consultantjobdetails',[\Crypt::encryptString($jobValue->id)])}}">
                <ul class="listContract">
                    <li>{{$jobValue->Job_ID}}</li>
                    <li>{{$jobValue->job_type}}</li>
                </ul>
                <h5 class="titles  font-bold">{{$jobValue->posting_title}}</h5>
            </a>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="cus-verticale-center">
                        <div class="text-red">
                            <!-- <span class="cus-br-icon">BR</span>  -->
                            <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                            <span class="ml-1">MYR </span>
                            {{$jobValue->job_base_rate}}/day</div>
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconCopy"></div>
                </div> -->
            </div>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->job_mode}}</div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->job_duration}} months</div>
                </div>
                
                
            </div>
            <div class="row rowContract">
                
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->work_experience}} exp.</div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->industry}}</div>
                </div>
                
            </div>

            <div class="row rowContract">
            
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{date('d M. Y',strtotime($jobValue->date_opened))}}</div>
                </div>
            
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->city}}</div>
                </div>
            </div>
            <div class="boxbtns job-tile-button-right">
                <div class="row no-gutters border-top-btn">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 border-right-btn">
                        <a title="Save" href="JavaScript:void(0);" class="btn btn-secondary btn-block btnSave consultant-job-saved consultant-job-saved colorBlue btn-custom-trans" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}" data-redirect="true">Save</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                        <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block btnApply consultant-job-applied btn-custom-trans colorRed" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="text-left mb-4">
            <a href="{{route('general-search')}}" class="linkSeealla text-red" title="See All">See All ></a>
            <!-- <a href="{{url('/consultant/search?search_type=job&job_might_like=1&jobcategory='.$jobValue->category)}}" class="linkSeealla text-red" title="See All">See All ></a> -->
        </div>
        @endif
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>
@endsection