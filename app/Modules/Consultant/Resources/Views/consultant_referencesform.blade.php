<div class="boxForm">
    <form method="post" action="{{route('consultantupdateProfileReference')}}" id="update-profilereferences">
        <input type="hidden" name="id" value="{{$reference->id}}">
        <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold text-green">Update Profile References</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        {{csrf_field()}}
        <div class="col-md-12">
            <div class="row rs-5">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Name: </label>
                        {{ Form::text('name',$reference->name,['class' => 'form-control','id' => 'name','placeholder' => 'Enter your name']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Position: </label>
                        {{ Form::text('position',$reference->position,['class' => 'form-control','id' => 'position','placeholder' => 'Enter your position']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Company: </label>
                        {{ Form::text('company',$reference->company,['class' => 'form-control','id' => 'company','placeholder' => 'Enter your company']) }}
                    </div>
                </div>
            </div>
            <div class="row rs-5">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label class="control-label">Mobile Number : </label>
                        {{ Form::text('phone',$reference->phone,['class' => 'form-control','id' => 'phone','placeholder' => 'Enter your mobile number']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label class="control-label">Email: </label>
                        {{ Form::text('email',$reference->email,['class' => 'form-control','id' => 'email','placeholder' => 'Enter your email date']) }}
                    </div>
                </div>
            </div>
        </div>
        <!-- <p class="text-center pb-0">
            <input type="submit" value="Submit" class="btn btn-secondary btn-sm btn-submit">
        </p> -->
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary submit-button">Save</button>
        </div>
    </form>
</div>