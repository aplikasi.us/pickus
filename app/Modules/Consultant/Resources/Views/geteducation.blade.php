@if($education != null)
<div class="form-group">
    <label class="alignment">Institute: </label>
    <input type="text" value="{{$education->institute}}" name="edited_educationinstitute"
        class="edited_educationinstitute size-space deep-color">
</div>
<div class="form-group">
    <label class="alignment">Department: </label>
    <input type="text" value="{{$education->department}}" name="edited_educationdepartment"
        class="edited_educationdepartment size-space deep-color">
</div>
<div class="form-group">
    <label class="alignment">Degree: </label>
    <input type="text" value="{{$education->degree}}" name="edited_educationdegree" class="edited_educationdegree size-space deep-color">
</div>
<div class="form-group">
    <p style="font-size: 16px; font-weight: normal;">Duration:</p>
    <label class="alignment">From: </label>
    @if($education->duration_from != null || $education->duration_from != "")
    @php
    $durationFrom = explode('-',$education->duration_from);
    @endphp
    {{Form::select('education_duration_from_month',[null => "Select Month"] + $monthList,$durationFrom[0],['class' => 'education_duration_from_month'])}}
    {{Form::select('education_duration_from_year',[null => "Select Year"] + $yearList,$durationFrom[1],['class' => 'education_duration_from_year'])}}
    @else
    {{Form::select('education_duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'education_duration_from_month'])}}
    {{Form::select('education_duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'education_duration_from_year'])}}
    @endif
</div>
<div class="form-group">
    <label class="alignment">To: </label>
    @php $durationTo = []; @endphp
    @if($education->duration_to != null || $education->duration_to != "")
    @php $durationTo = explode('-',$education->duration_to); @endphp
    @endif

    @if(!empty($durationTo))
    {{Form::select('education_duration_to_month',[null => "Select Month"] + $monthList,$durationTo[0],['class' => 'education_duration_to','id' => 'education_duration_to_month'])}}
    {{Form::select('education_duration_to_year',[null => "Select Year"] + $yearList,$durationTo[1],['class' => 'education_duration_to','id' => 'education_duration_to_year'])}}

    @elseif($education->currently_pursuing == "true")

    {{Form::select('education_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'education_duration_to','id' => 'education_duration_to_month','disabled' => 'disabled'])}}
    {{Form::select('education_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'education_duration_to','id' => 'education_duration_to_year','disabled' => 'disabled'])}}

    @else

    {{Form::select('education_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'education_duration_to','id' => 'education_duration_to_month'])}}
    {{Form::select('education_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'education_duration_to','id' => 'education_duration_to_year'])}}

    @endif
</div>
<div class="form-group">
    <label class="alignment" >Enrolling: </label>
    @if($education->currently_pursuing == "true")
    <input type="checkbox" name="currently_pursuing" class="currently_pursuing" checked="">
    @else
    <input type="checkbox" name="currently_pursuing" class="currently_pursuing">
    @endif
</div>
<div class="form-group">
<a href="{{route('deleteProfileEducation',[$education->id])}}" id="delete-experience-btn" class="delete-educationBtn buttonDefault del" data-id="{{$education->id}}">Delete</a>
    <input type="submit" value="Save" style="float:right;color: #4FDB73!important;" data-id="{{$education->id}}"
        class="buttonDefault save  updatebutton-consultantEducation spacing-left">
    <a href="{{route('consultant-profile')}}"  style="float:right;" class=" buttonDefault can ">Cancel</a>
    <img src="{{admin_asset('images/loader.svg')}}"  class="education-loader" style="display: none;">

</div>
@else
<div class="form-group">
    <label class="alignment">Institute: </label>
    <input type="text" name="edited_educationinstitute" class="edited_educationinstitute size-space">
</div>
<div class="form-group">
    <label class="alignment">Department: </label>
    <input type="text" name="edited_educationdepartment" class="edited_educationdepartment size-space">
</div>
<div class="form-group">
    <label class="alignment">Degree: </label>
    <input type="text" name="edited_educationdegree" class="edited_educationdegree size-space">
</div>
<div class="form-group">
<p style="font-size: 16px; font-weight: normal;">Duration<p>
    <label class="alignment">From: </label>
    {{Form::select('education_duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'education_duration_from_month'])}}
    {{Form::select('education_duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'education_duration_from_year'])}}
</div>
<div class="form-group">
    <label class="alignment">To: </label>
    {{Form::select('education_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'education_duration_to','id' => 'education_duration_to_month'])}}
    {{Form::select('education_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'education_duration_to','id' => 'education_duration_to_year'])}}
</div>
<div class="form-group">
    <label class="alignment">Enrolling: </label>
    <input type="checkbox" name="currently_pursuing" class="add_currently_pursuing">
</div>
<!-- <div id="consultant-Education-save">
    <input type="submit" value="Save" class="btn btn-green btn-sm savebutton-consultantEducation">
    <a href="{{route('consultant-profile')}}" class="btn btn-danger btn-sm">X</a>
    <img src="{{admin_asset('images/loader.svg')}}" class="education-loader" style="display: none;">

    
</div> -->
<div class="consultant-Education-save" style="margin-bottom:8%!important;">

    <input type="submit" value="Save" style="float:right;color: #4FDB73!important;"
        class="savebutton-consultantEducation buttonDefault save spacing-left">
    <a href="{{route('consultant-profile')}}"  style="float:right;" class=" buttonDefault can ">Cancel</a>
    <img src="{{admin_asset('images/loader.svg')}}"  class="education-loader" style="display: none;">

</div>
@endif