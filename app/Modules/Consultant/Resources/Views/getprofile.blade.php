{{Form::open(['route' => 'consultantupdateprofile','id' => 'consultant-profile-form','enctype' => 'multipart/form-data'])}}
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">Profile Image: </label>
            @if($user != null)

            @if($user->profile_pic != null)
            <div class="consultant-profile-image">
                <input type="file" name="profile_pic" class="hidden_profile_pic" style="display: none;border: none;">
                <img src="{{admin_asset('profile_image/'.$user->profile_pic)}}" id="profile_image" />
                <a href="{{route('remove-profile-pic')}}" id="remove-consultant-profile-pic" title="Remove">X</a>
            </div>
            @else
            <input type="file" name="profile_pic" class="profile_pic" style="border: none;">
            @endif

            @else
            <input type="file" name="profile_pic" class="profile_pic" style="border: none;">
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">First Name: <span
                    class="input-required">*</span></label>
            <input type="text" value="{{$candidate->first_name}}" name="firstname" placeholder="Enter first name"
                class="form-control firstname">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">Last Name: <span class="input-required">*</span></label>
            <input type="text" value="{{$candidate->last_name}}" name="lastname" placeholder="Enter last name"
                class="form-control lastname">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">Mobile Number: <span
                    class="input-required">*</span></label>
            <input type="text" value="{{$candidate->mobile_number}}" name="mobile_number"
                placeholder="Enter mobile number" class="form-control mobile_number">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">Title: <span class="input-required">*</span></label>
            <input type="text" value="{{$candidate->sap_job_title}}" name="title" placeholder="Enter title"
                class="form-control title">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">Experience: <span
                    class="input-required">*</span></label>
            <input type="text" value="{{$candidate->experience_in_years}}" placeholder="Experience in years"
                name="experience" class="form-control experience">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">Category: <span class="input-required">*</span></label>
            {{Form::select('category[]',$categories,explode(',',$candidate->category),['class' => 'form-control category select2','id' => 'candidate-category','multiple' => true])}}
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">State: <span class="input-required">*</span></label>
            {{Form::select('state',[null => '--Select State--'] + $stateList,$stateRecord,['class' => 'form-control select2','id' => 'cons-profile-state','required' => true])}}
            <!-- <input type="text" value="{{$candidate->state}}" name="state" placeholder="Enter state name" 
                class="form-control state">-->
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label style="font-size: 15px;font-weight: normal;">City: <span class="input-required">*</span></label>
            {{Form::select('city',[null => '--Select City--'] + $city,$candidate->city,['class' => 'form-control select2','id' => 'cons-profile-city','required'=> true])}}
            <!-- <input type="text" value="{{$candidate->city}}" name="city" placeholder="Enter city name"
                class="form-control city"> -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group" style="right:0;">
            <input type="submit" value="Save" style="float:right;color: #4FDB73!important;" class="buttonDefault save spacing-left">
            <a href="{{route('consultant-profile')}}" style="float:right;" class="buttonDefault can"> Cancel</a>
        </div>
    </div>
</div>
{{Form::close()}}
<script>
$(document).ready(function() {
    // Consultant profile update
    $("#consultant-profile-form")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                firstname: {
                    validators: {
                        notEmpty: {
                            message: "First name is required"
                        }
                    }
                },
                lastname: {
                    validators: {
                        notEmpty: {
                            message: "Last name is required"
                        }
                    }
                },
                title: {
                    validators: {
                        notEmpty: {
                            message: "Sap job title is required"
                        }
                    }
                },
                experience: {
                    validators: {
                        notEmpty: {
                            message: "Experience is required"
                        },
                        regexp: {
                            regexp: /^[0-9]{1,10}$/,
                            message: 'Experience must be digits only'
                        },
                        stringLength: {
                            min: 1,
                            max: 2,
                            message: "Experience must be minimum 1 digits and maximum 2 digits"
                        }
                    }
                },
                'category[]': {
                    validators: {
                        notEmpty: {
                            message: "Category is required"
                        }
                    }
                },
                mobile_number: {
                    validators: {
                        notEmpty: {
                            message: "Mobile number is required"
                        },
                        regexp: {
                            regexp: /^[0-9-+()]*$/,
                            message: 'Mobile number must be digits only'
                        },
                        stringLength: {
                            min: 10,
                            max: 15,
                            message: "Mobile number must be minimum 10 digits and maximum 15 digits"
                        }
                    }
                },
                state: {
                    validators: {
                        notEmpty: {
                            message: "State is required"
                        }
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: "City is required"
                        }
                    }
                },
                profile_pic: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            message: 'Please select only image'
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data('bv.messages')
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]').show();
        });

    $("#candidate-category").select2();
})
</script>