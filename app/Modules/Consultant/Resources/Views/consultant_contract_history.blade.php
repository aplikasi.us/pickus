@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Contract History')
@section('content')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <h2 class="mb-20 text-uppercase Black">Contract History</h2>
        <div class="commanContent border-ra-5">
            <ul class="listContractHistory">
                @if(count($contracthistory))
                @foreach($contracthistory as $key => $value)
                <li>
                    <div class="row rowHistory align-items-center ">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 pd-l30 border-before-right-full">
                            <span class="textDate mb-10 colorDarkGray">
                            {{ date('d M. Y',strtotime($value->contract_period_from))}} - {{ (isset($value->contract_period_to)) ? date('d M. Y',strtotime( $value->contract_period_to)) : "Till now"}}
                                <!-- {{ date('d M. Y',strtotime(str_replace('/', '-', $value->contract_period_from)))}} - {{ (isset($value->contract_period_to)) ? date('d M. Y',strtotime(str_replace('/', '-', $value->contract_period_to))) : "Till now"}} -->
                            </span>
                            <div class="pd-b20">
                                <div class="frow">
                                    <div>Client: </div>
                                    <div>{{$value->past_client}}</div>
                                </div>
                                <div class="frow">
                                    <div>Project:</div>
                                    <div>{{$value->past_project}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 pd-l30">
                            <h6 class="colorGray font-weight-normal">Rate:-</h6>
                            <h4 class="text-red font-weight-normal">MYR {{number_format((float)$value->pay_rate, 2, '.', '')}}/day</h4>
                        </div>
                    </div>
                </li>
                @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h5 class=" pt-2 textDarkBlue">Jobs You Might Like...</h5>
        @if(count($jobsMightLike))
        @foreach($jobsMightLike as $jobKey => $jobValue)
        <div class="commanContent mb-3">
            <a href="{{route('consultantjobdetails',[\Crypt::encryptString($jobValue->id)])}}">
                <ul class="listContract">
                    <li>{{$jobValue->Job_ID}}</li>
                    <li>{{$jobValue->job_type}}</li>
                </ul>
                <h5 class="titles font-bold">{{$jobValue->posting_title}}</h5>
            </a>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="cus-verticale-center">
                        <div class="text-red">
                            <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                            <span class="ml-1">MYR </span>
                            {{$jobValue->job_base_rate}}/day</div>
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconCopy"></div>
                </div> -->
            </div>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->job_mode}}</div>
                </div>
                

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->job_duration}} months</div>
                </div>



            </div>
            <div class="row rowContract">
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->work_experience}} exp.</div>
                </div>


                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                    <div class="ml-1 Black d-inline">{{$jobValue->industry}}</div>
                </div>
                
                
            </div>

            <div class="row rowContract">
            
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                            <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                            <div class="ml-1 Black d-inline">{{date('d M. Y',strtotime($jobValue->date_opened))}}</div>
                        </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                            <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                            <div class="ml-1 Black d-inline">{{$jobValue->city}}</div>
                    </div>

            </div>

            <div class="boxbtns job-tile-button-right">
                <div class="row no-gutters border-top-btn">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 border-right-btn">
                        <a title="Save" href="JavaScript:void(0);" class="btn btn-secondary btn-block btnSave consultant-job-saved consultant-job-saved colorBlue btn-custom-trans" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}" data-redirect="true">Save</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                        <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block btnApply consultant-job-applied btn-custom-trans colorRed" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="text-right mb-4">
            <a href="{{route('general-search')}}" class="linkSeealla text-red" title="See All">See All ></a>
            <!-- <a href="{{url('/consultant/search?search_type=job&job_might_like=1&jobcategory='.$jobValue->category)}}" class="linkSeealla text-red" title="See All">See All ></a> -->
        </div>
        @endif
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>
@endsection