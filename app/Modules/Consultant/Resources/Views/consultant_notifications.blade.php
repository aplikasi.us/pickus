@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Notifications')
@section('content')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <h2 class="mb-20 text-uppercase Black">Notifications</h2>
        <div class="commanContent contentNotifications mainNotifications border-ra-5 notification-modi">
            @if(count($notifications))
            <ul class="listContractHistory">
                @foreach($notifications as $notificationKey => $notoficationValue)
                
                <li>
                    <div class="row rowHistory align-items-center">
                        <div class="col-md-12">
                            <span class="textDate colorDarkGray">{{date('d M. Y',strtotime($notoficationValue->created_at))}} ({{date('h:i',strtotime($notoficationValue->created_at))}})</span>
                            @if($notoficationValue->redirect_url != null)
                            <a href="{{$notoficationValue->redirect_url}}"><p>{{$notoficationValue->message}}</p></a>
                            @else
                            <p>{{$notoficationValue->message}}</p>
                            @endif
                        </div>
                    </div>
                </li>
                
                @endforeach                
            </ul>
            {{$notifications->links()}}
            @else
            <span class="text-red">No notifications found</span>
            @endif
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h5 class="text-center pt-2 textDarkBlue">Jobs You Might Like...</h5>
        @if(count($jobsMightLike))
        @foreach($jobsMightLike as $jobKey => $jobValue)
        <div class="commanContent mb-3">
            <a href="{{route('consultantjobdetails',[\Crypt::encryptString($jobValue->id)])}}">
                <ul class="listContract">
                    <li>{{$jobValue->Job_ID}}</li>
                    <li>{{$jobValue->job_type}}</li>
                </ul>
                <h5 class="titles  font-bold">{{$jobValue->posting_title}}</h5>
            </a>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="text-red">
                        <!-- <span class="cus-br-icon">BR</span> -->
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                        {{$jobValue->job_base_rate}}/day</div>
                </div>
                <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconCopy"></div>
                </div> -->
            </div>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline  Black">{{$jobValue->job_mode}}</div>
                </div>


                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline  Black">{{$jobValue->job_duration}} months</div>
                </div>

                
                
            </div>
            <div class="row rowContract">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline  Black">{{$jobValue->work_experience}} exp.</div>
                </div>
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline  Black">{{$jobValue->industry}}</div>
                </div>
                
            </div>
            <div class="row rowContract">

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline  Black">{{date('d M. Y',strtotime($jobValue->date_opened))}}</div>
                </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline  Black">{{$jobValue->city}}</div>
                </div>
        </div>
            <div class="boxbtns job-tile-button-right">
                <div class="row no-gutters border-top-btn">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 border-right-btn">
                        <a title="Save" href="JavaScript:void(0);" class="btn btn-secondary btn-block btnSave consultant-job-saved consultant-job-saved colorBlue btn-custom-trans" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}" data-redirect="true">Save</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                        <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block btnApply consultant-job-applied btn-custom-trans colorRed" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="text-left mb-4">
            <a href="{{route('general-search')}}" class="linkSeealla text-red" title="See All">See All ></a>
            <!-- <a href="{{url('/consultant/search?search_type=job&job_might_like=1&jobcategory='.$jobValue->category)}}" class="linkSeealla text-red" title="See All">See All ></a> -->
        </div>
        @endif
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>
@endsection