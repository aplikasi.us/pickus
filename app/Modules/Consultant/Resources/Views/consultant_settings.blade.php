@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Settings')
@section('content')
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <h2 classs="text-uppercase">SETTINGS</h2>
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="tabsInterviews table-w-60">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#changepassword" role="tab">Change Password</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" data-toggle="tab" href="#deleteaccount"
                       role="tab">Deactivate Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" data-toggle="tab" href="#notifications"
                       role="tab">Notifications</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="changepassword">
                    <div class="boxForm pt-4">
                        <div class="row">

                            <div class="col-md-12">
                                <form method="post" method="post" action="{{route('changepassword')}}" id="change-password">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Old Password: </label>
                                                <input type="password" name="old_password" class="form-control" placeholder=" Enter password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">New Password: </label>
                                                <input type="password" name="new_password" class="form-control" placeholder=" Enter password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password: </label>
                                                <input type="password" name="confirm_password" class="form-control" placeholder=" Re-enter your password">
                                            </div>
                                        </div>
                                    </div>	
                                    <input type="submit" class="btn btn-block btn-danger" value="Change Password!"/>			
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade " id="deleteaccount">
                    <p class="">Are you sure you want to deactivate your account? Deactivating will not delete your data. It will simply make your account invisible to others. </p>
                    <a href="#" class="btn btn-block btn-danger" data-toggle="modal" data-target="#delete-account">Deactivate Account</a>
                </div>
                <div class="tab-pane fade show " id="notifications">
                    <form method="post" method="post" action="{{route('notifications',[$id])}}" id="notification-settings">
                        {{csrf_field()}}
                        <?php
                        $job_checked = $whishlist_checked = $applied_checked = $profile_checked = $all_checked = "";
                        if ($user->job_alerts == 1 && $user->job_wishlist == 1 && $user->applied_job == 1 && $user->profile_show == 1) {
                            $all_checked = 'checked';
                        }
                        if ($user->job_alerts == 1) {
                            $job_checked = 'checked';
                            $chek_alert = "";
                        } else {
                            $chek_alert = "job-hide-tags";
                        }
                        // echo $user->job_wishlist;
                        if ($user->job_wishlist == 1) {
                            $whishlist_checked = 'checked';
                        }
                        if ($user->applied_job == 1) {
                            $applied_checked = 'checked';
                        }
                        if ($user->profile_show == 1) {
                            $profile_checked = 'checked';
                        }
                        if ($user->job_alerts_tags != null) {
                            $job_alerts_tags_a = json_decode($user->job_alerts_tags, true);
                        } else {
                            $job_alerts_tags_a = [];
                        }
                        ?>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">All Notifications: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="all_checked" {{$all_checked}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Job Alerts: </label>
                                    <label class="switch">
                                        <input type="checkbox" class=" set_job_alerts" value='1' name='job_alerts' {{$job_checked}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="form-group jobalerts_tags {{$chek_alert}}">
                                    <label>Job Tags</label>

                                    <ul class="job-tags-ul">
                                        @foreach($jobAlertsTags as $tags)
                                        <li class="job_alerts_tags_li">
                                            @if(!empty($job_alerts_tags_a))
                                            @if(in_array($tags,$job_alerts_tags_a))
                                            <input type="checkbox" class="job_alerts_tags" checked="" name="job_alerts_tags[]" id="{{$tags}}" value="{{$tags}}" style="margin-right: 5px;"/><label for="{{$tags}}">{{$tags}}</label>    
                                            @else
                                            <input type="checkbox" class="job_alerts_tags" name="job_alerts_tags[]" id="{{$tags}}" value="{{$tags}}" style="margin-right: 5px;"/><label for="{{$tags}}">{{$tags}}</label>    
                                            @endif
                                            @else
                                            <input type="checkbox" class="job_alerts_tags" name="job_alerts_tags[]" id="{{$tags}}" value="{{$tags}}" style="margin-right: 5px;"/><label for="{{$tags}}">{{$tags}}</label>    
                                            @endif
                                        </li>
                                        @endforeach                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Status change for jobs in wishlist: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checked_all" value='1' name='job_wishlist' {{$whishlist_checked}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Status change for applied jobs: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checked_all" value='1' name='applied_job' {{$applied_checked}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Profile request notification: </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checked_all" value='1' name='profile_show' {{$profile_checked}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-block btn-danger" id="setting-submit" value="Save Settings"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <h5 class="text-center pt-2 textDarkBlue">Jobs You Might Like...</h5>
        @if(count($jobsMightLike))
        @foreach($jobsMightLike as $jobKey => $jobValue)
        <div class="commanContent mb-3">
            <a href="{{route('consultantjobdetails',[\Crypt::encryptString($jobValue->id)])}}">
                <ul class="listContract">
                    <li>{{$jobValue->Job_ID}}</li>
                    <li>{{$jobValue->job_type}}</li>
                </ul>
                <h5 class="titles  font-bold">{{$jobValue->posting_title}}</h5>
            </a>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="cus-verticale-center">
                        <div class="text-red">
                            <!-- <span class="cus-br-icon">BR</span> MYR -->
                            <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                            <span class="ml-1">MYR </span>
                             {{$jobValue->job_base_rate}}/day</div>
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconCopy"></div>
                </div> -->
            </div>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->job_mode}}</div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->job_duration}} months</div>
                </div>
                

            </div>
            <div class="row rowContract">
                
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->work_experience}} exp.</div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                    <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->industry}}</div>
                </div>
                
            </div>

            <div class="row rowContract">

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{date('d M. Y',strtotime($jobValue->date_opened))}}</div>
                </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                    <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                    <div class="ml-1 d-inline Black">{{$jobValue->city}}</div>
                </div>
        </div>
            <div class="boxbtns job-tile-button-right">
                <div class="row no-gutters border-top-btn">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 border-right-btn">
                        <a title="Save" href="JavaScript:void(0);" class="btn btn-secondary btn-block btnSave consultant-job-saved consultant-job-saved colorBlue btn-custom-trans" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}" data-redirect="true">Save</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                        <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block btnApply consultant-job-applied btn-custom-trans colorRed" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}">Apply Now!</a>                        
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="text-left mb-4">
            <a href="{{route('general-search')}}" class="linkSeealla text-red" title="See All">See All ></a>
            <!-- <a href="{{url('/consultant/search?search_type=job&job_might_like=1&jobcategory='.$jobValue->category)}}" class="linkSeealla text-red" title="See All">See All ></a> -->
        </div>
        @endif
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>


</div>

<!----------------Delete account model up ------------------>

<div class="modal fade" id="delete-account" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="boxForm">
                <form method="post" method="post" action="{{route('deleteaccount')}}" id="delete-account-form">
                    {{csrf_field()}}
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Deactivate Account</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Password: </label>
                                    <input type="password" name="password" class="form-control" placeholder=" Enter password">
                                </div>
                            </div>
                        </div>
                    </div>	
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-secondary" value="Deactivate Now"/>	
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection