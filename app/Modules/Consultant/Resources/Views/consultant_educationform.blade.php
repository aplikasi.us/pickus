<div class="boxForm">
    <form method="post" action="{{route('consultantupdateProfileEducation')}}" id="update-profileeducation">
        <input type="hidden" name="id" value="{{$education->id}}">
        <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold text-green">Update Profile Summary</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        {{csrf_field()}}
        <div class="col-md-12">
            <div class="row rs-5">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Institute: </label>
                        {{ Form::text('institute',$education->institute,['class' => 'form-control','id' => 'institute','placeholder' => 'Enter your Institute name']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Department: </label>
                        {{ Form::text('department',$education->department,['class' => 'form-control','id' => 'department','placeholder' => 'Enter your Department name']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label class="control-label">Degree: </label>
                        {{ Form::text('degree',$education->degree,['class' => 'form-control','id' => 'degree','placeholder' => 'Enter your degree']) }}
                    </div>
                </div>
            </div>
            <div class="row rs-5">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label class="control-label">Duration From : </label>
                        {{ Form::text('duration_from',$education->duration_from,['class' => 'form-control','id' => 'duration_from','placeholder' => 'Enter your duration from date','autocomplete' => 'off','readonly']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label class="control-label">Duration To: </label>
                        {{ Form::text('duration_to',$education->duration_to,['class' => 'form-control','id' => 'duration_to','placeholder' => 'Enter your duration to date','autocomplete' => 'off','readonly']) }}
                    </div>
                </div>
            </div>
        </div>
        <!-- <p class="text-center pb-0">
            <input type="submit" value="Submit" class="btn btn-secondary btn-sm btn-submit">
        </p> -->
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary submit-button">Save</button>
        </div>
    </form>
</div>
<script>
$("body").delegate("#duration_from", "focusin", function(){
    $(this).datepicker({
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months"
    });
});
$("body").delegate("#duration_to", "focusin", function(){
    $(this).datepicker({
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months"
    });
});
</script>