@extends('Layouts.Consultant.base_new')
@section('title', 'Consultant - Home')
@section('content')
<script>
    var savejobUrl = "{!! route('consultant-home') !!}";
</script>
<div class="row floated-div">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="commanContent commanContentBig border-ra-5">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                    <h3 class="d-inline">
                        <span class="">{{$candidateDetailarray['first_name']}}</span>
                        {{$candidateDetailarray['last_name']}}
                    </h3>
                    <!-- <h5 class="pb-2 d-inline colorGray">Candidate ID:
                        <span class="text-light">{{$candidateDetailarray['candidate_id']}}</span>
                    </h5> -->
                    <ul class="listContract mb-3">
                        <li class="titles">
                            {{(isset($candidateDetailarray['sap_job_title'])) ? $candidateDetailarray['sap_job_title'] : ""}}
                        </li>
                        <li>{{(isset($candidateDetailarray['experience_in_years'])) ? $candidateDetailarray['experience_in_years'] : 0}}
                            years experience</li>
                    </ul>
                    <div class="row rowContract">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class=" cusGray">
                                <!-- <span class="cus-br-icon">BR</span> -->
                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                <span class="ml-1">
                                    {{(isset($candidateDetailarray['currency'])) ? $candidateDetailarray['currency'] : ""}}
                                </span>

                                {{(isset($candidateDetailarray['base_rate'])) ? $candidateDetailarray['base_rate'] : ""}}/day
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">

                            <div class="cus-iconBox cus-iconMail max-character-fix">
                                <a href="mailto:{{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : " "}}"
                                    title="{{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : " "}}"
                                    class="cusGray">{{(isset($candidateDetailarray['email'])) ? $candidateDetailarray['email'] : ""}}</a>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="cus-iconBox cus-iconPhone">
                                <a href="tel:{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : " "}}"
                                    title="{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : " "}}"
                                    class="cusGray">{{(isset($candidateDetailarray['mobile_number'])) ? $candidateDetailarray['mobile_number'] : ""}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="row rowContract pb-0">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="cus-iconBox cus-iconplace cusGray">
                                {{(isset($candidateDetailarray['city'])) ? $candidateDetailarray['city'] : ""}}</div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="cus-iconBox cus-iconRequests cusGray">{{$clientRequest}} Client Requests</div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="cus-iconBox cus-iconUpdated cusGray">Last updated:
                                {{$candidateDetailarray['updated_at']->format('d M. Y')}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 border-before-left mob-border-none">
                    <div class="commanContent text-center bg-skyblue vertical-table border-ra-5 mob-full-width">
                        <a href="{{route('consultant-interviews')}}">
                            <h2 class="colorWhite text-number">{{$interviewCounts['upcomingInterview']}}</h2>
                            <h5 class="colorWhite pb-0">Upcoming Interviews</h5>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div class="tabsInterviews tabsJobs table-w-60">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link <?php
                    if (isset($_GET['tab'])) {
                        if ($_GET['tab'] == "appliedjob") {
                            echo "active";
                        }
                    } if (!isset($_GET['tab'])) {
                        echo "active";
                    }
                    ?>" data-toggle="tab" href="#appliedJobs" role="tab">Applied Jobs ({{count($jobopenings)}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php
                    if (isset($_GET['tab'])) {
                        if ($_GET['tab'] == "savedjobs") {
                            echo "active";
                        }
                    }
                    ?>" data-toggle="tab" data-toggle="tab" href="#savedJobs" role="tab">Saved Jobs
                        (<span id="saved-job-counter">{{count($jobopeningsSave)}}</span>)</a>
                </li>
                <!-- Temporary disable for recommended jobs 19/12/19 - advice by Nik -->
                <!-- <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-toggle="tab" href="#recommendedJobs"
                        role="tab">Recommended Jobs </a>
                </li> -->
            </ul>
            <div class="tab-content border-ra-5">
                <div class="tab-pane fade <?php
                if (isset($_GET['tab'])) {
                    if ($_GET['tab'] == "appliedjob") {
                        echo "show active";
                    }
                } if (!isset($_GET['tab'])) {
                    echo "show active";
                }
                ?>" id="appliedJobs">
                    <div class="rowJobs row rs-10">
                        @if(count($jobopenings))
                        <?php $count = 0;
                        ?>
                        @foreach($jobopenings as $key => $value)
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="commanContent">
                                <?php 
                                $status = str_replace("-"," ",$value->status);
                                $btnColor = 'bg-yellow-status';
                                if(in_array($value->status,$candidateScreeningStatus)){
                                    $btnColor = 'bg-yellow-status';
                                }elseif(in_array($value->status,$candidateSubmissionStatus)){
                                    $btnColor = 'bg-orange-status';
                                }elseif(in_array($value->status,$candidateInterviewStatus)){
                                    $btnColor = 'bg-purple-status';
                                }elseif(in_array($value->status,$candidateOfferStatus)){
                                    $btnColor = 'bg-skyblue-status';
                                }elseif(in_array($value->status,$candidateHireStatus)){
                                    $btnColor = 'bg-green-status';
                                }
                                ?>
                                <span
                                    class="btn-sm btn-tag-md {{$btnColor}} border-rad-20 colorWhite  d-inline-block mb-2">{{$status}}</span>
                                <a href="{{route('job-details',[\Crypt::encryptString($value->job->id)])}}"
                                    class="home-jobs-listing">
                                    <ul class="listContract">
                                        @if(isset($value->job->Job_ID) && !empty($value->job->Job_ID))
                                        <li>{{$value->job->Job_ID}}</li>
                                        @endif
                                        <li>{{$value->job->job_type}}</li>
                                    </ul>
                                    <h5 class="titles">{{$value->job->posting_title}}</h5>
                                </a>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="cus-verticale-center">
                                            <div class="text-red">

                                                <img class="cus-br-icon-small"
                                                    src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1">{{$value->job->currency}} </span> <span
                                                    class="c-job-base-rate"
                                                    id="con-copy-{{$key}}">{{($value->job->job_base_rate != null) ? $value->job->job_base_rate : 0}}/day</span>
                                                <div
                                                    class="d-inline iconBox iconCopy copy-to-clipboard job-tile-button-right m-l-10">
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline Black">{{$value->job->job_mode}}</div>
                                    </div>


                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline  Black">{{$value->job->job_duration}} months</div>
                                    </div>



                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline Black">{{$value->job->work_experience}} exp.</div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                                        <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline Black">{{$value->job->industry}}</div>
                                    </div>

                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline  Black">
                                            {{date('d M. Y',strtotime($value->job->date_opened))}}</div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black  d-inline">{{$value->job->city}}</div>
                                    </div>

                                </div>


                                <div class="boxbtns job-tile-button-right">
                                    <div class="row no-gutters">
                                        <div class="col-md-12 text-center border-top-btn">
                                            <img src="{{admin_asset('images/loader.svg')}}"
                                                class="consultant-img-loader">
                                            @if($value->status == "Associated")
                                            <a title="Withdraw Application" href="javascript:void(0)"
                                                class="btn btn-bg-red colorWhite btn-block withdraw-application "
                                                data-zohoid="{{$value->job->JOBOPENINGID}}"
                                                data-id="{{$value->job->id}}">Withdraw Application</a>
                                            @endif
                                            @if($value->status == "Application Withdrawn" || $value->status ==
                                            "Withdrawn")
                                            <a title="Application Withdrawn" href="javascript:void(0)"
                                                class="btn  btn-bg-lightGray colorWhite btn-block disabled">Application
                                                Withdrawn</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($key%2 == 1)
                    </div>
                    <div class="rowJobs row rs-10">
                        @endif

                        @endforeach @endif
                    </div>
                </div>
                <div class="tab-pane fade <?php
                if (isset($_GET['tab'])) {
                    if ($_GET['tab'] == "savedjobs") {
                        echo "show active";
                    }
                }
                ?>" id="savedJobs">
                    <div class="rowJobs row rs-10">
                        @if(count($jobopeningsSave))
                        <?php $count1 = 0;
                        ?>
                        @foreach($jobopeningsSave as $key1 => $value1)
                        <!-- Header Saved Jobs card -->
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="commanContent box-shadow-job">
                                <span class="candidate-job-status">{{$value1->status}}</span>
                                <a href="{{route('job-details',[\Crypt::encryptString($value1->job->id)])}}"
                                    class="home-jobs-listing">
                                    <ul class="listContract">
                                        <li>{{$value1->job->Job_ID}}</li>
                                        <li>{{$value1->job->job_type}}</li>
                                        <li>
                                            <!-- Icon for share and wishlist -->
                                            <div class="row position">
                                                <a class="btnMinus delete-from-wishlist mt-2 mr-4"><img
                                                        src="{{admin_asset('images/new-design/share.png')}}"
                                                        class="icon-cus-img icon-share-gap margin-right">
                                                </a>
                                                <div>
                                                    <a title="Unsave" href="javascript:void(0);"
                                                        class="btnMinus savejob-delete mt-2 mr-1"
                                                        data-zohoid="{{$value1->JOBOPENINGID}}"
                                                        data-id="{{$value1->job_id}}" data-reloadtab="true">
                                                        <img src="{{admin_asset('images/new-design/wishlist-1.png')}}"
                                                            class="icon-cus-img icon-wish-gap margin-right">
                                                    </a>
                                                </div>

                                                <!-- Start -->
                                                <!-- <div
                                            class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center border-right-btn">
                                            <a title="Unsave" href="javascript:void(0);"
                                                class="btn btn-secondary btn-custom-trans btn-block btnSave savejob-delete colorBlue"
                                                data-zohoid="{{$value1->JOBOPENINGID}}"
                                                data-id="{{$value1->job_id}}" data-reloadtab="true">Unsave</a>
                                        </div> -->

                                                <!-- End -->
                                            </div>
                                        </li>
                                    </ul>
                                    <h5 class="titles">{{$value1->job->posting_title}}</h5>
                                </a>
                                <!-- The real raw for saved jobs -->
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="cus-verticale-center">
                                            <div class=" text-red">
                                                <!-- <span class="cus-br-icon">BR</span> -->
                                                <img class="cus-br-icon-small"
                                                    src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1"> {{$value1->job->currency}} </span>
                                                <span class="c-job-base-rate"
                                                    id="con-copy-1{{$key1}}">{{($value1->job->job_base_rate != null) ? $value1->job->job_base_rate : 0}}/day</span>
                                                <div
                                                    class="iconBox iconCopy copy-to-clipboard job-tile-button-right d-inline m-l-10">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline Black">{{$value1->job->job_mode}}</div>
                                    </div>


                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline  Black">{{$value1->job->job_duration}} months</div>
                                    </div>

                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline Black">{{$value1->job->work_experience}} exp.</div>
                                    </div>


                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                                        <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline Black">{{$value1 ->job->industry}}</div>
                                    </div>




                                </div>

                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline  Black">
                                            {{date('d M. Y',strtotime($value1->job->date_opened))}}</div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black  d-inline">{{$value1->job->city}}</div>
                                    </div>

                                </div>

                                <div class="boxbtns job-tile-button-right">
                                    <div class="row no-gutters border-top-btn">
                                        <!-- <div
                                            class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center border-right-btn">
                                            <a title="Unsave" href="javascript:void(0);"
                                                class="btn btn-secondary btn-custom-trans btn-block btnSave savejob-delete colorBlue"
                                                data-zohoid="{{$value1->JOBOPENINGID}}"
                                                data-id="{{$value1->job_id}}" data-reloadtab="true">Unsave</a>
                                        </div> -->
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                                            <img src="{{admin_asset('images/loader.svg')}}"
                                                class="consultant-img-loader">
                                            <a title="Apply Now!" href="JavaScript:void(0)"
                                                class="btn btn-danger btn-block btn-custom-trans btnApply consultant-job-applied colorRed "
                                                data-zohoid="{{$value1->JOBOPENINGID}}" data-reload="true"
                                                data-id="{{$value1->job_id}}">Apply Now!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($key1%2 == 1)
                    </div>
                    <div class="rowJobs row rs-10">
                        @endif

                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="tab-pane fade" id="recommendedJobs">
                    <div class="rowJobs row rs-10">
                        <?php $count3 = 0;
                        ?>
                        @if(count($jobRecommended))
                        @foreach($jobRecommended as $k => $v)
                        <!-- <?php
                        echo '<pre>';
                        print_r($v->toArray());
                        echo '</pre>';
                        ?> -->
                        @php
                        $is_applied1 = 0;
                        @endphp
                        @foreach ($v->associatecandidates as $k2=>$v2)
                        @if ($v2->CANDIDATEID == Auth::user()->zoho_id)
                        @if(!empty($v2->status) || $v2->job_save == 1)
                        @php
                        $is_applied1 = 1;
                        @endphp
                        @endif
                        @endif
                        @endforeach
                        @if($is_applied1 == 0)
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="commanContent">
                                <a href="{{route('job-details',[\Crypt::encryptString($v->id)])}}"
                                    class="home-jobs-listing">
                                    <ul class="listContract">
                                        <li>{{$v->Job_ID}}</li>
                                        <li>{{$v->job_type}}</li>
                                    </ul>
                                    <h5 class="titles">{{$v->posting_title}}</h5>
                                </a>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="cus-verticale-center">
                                            <div class=" text-red">
                                                <!-- <span class="cus-br-icon">BR</span> -->
                                                <img class="cus-br-icon-small"
                                                    src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1">MYR </span>
                                                <span class="c-job-base-rate"
                                                    id="con-copy-1{{$k}}">{{($v->job_base_rate != null) ? $v->job_base_rate : 0}}/day</span>
                                                <div
                                                    class="iconBox iconCopy copy-to-clipboard job-tile-button-right d-inline m-l-10">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                        <div class="iconBox iconCopy copy-to-clipboard job-tile-button-right">

                                        </div>
                                    </div> -->
                                </div>
                                <div class="row rowContract">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">{{$v->job_mode}}</div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">{{$v->job_duration}} months</div>
                                    </div>

                                </div>
                                <div class="row rowContract">


                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">{{$v->work_experience}} exp.</div>
                                    </div>


                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                                        <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                                        <div class="ml-1 d-inline  Black">{{$v->industry}}</div>
                                    </div>

                                </div>
                                <div class="row rowContract">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black d-inline">
                                            {{date('d M. Y',strtotime($v->date_opened))}}</div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 titles">
                                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                        <div class="ml-1 Black  d-inline">{{$v->city}}</div>
                                    </div>


                                </div>

                                <div class="boxbtns job-tile-button-right">
                                    <div class="row no-gutters border-top-btn">

                                        @php
                                        $is_applied = 0;
                                        $is_saved = 0;
                                        @endphp
                                        @foreach ($v->associatecandidates as $k1=>$v1)
                                        @if ($v1->CANDIDATEID == Auth::user()->zoho_id)
                                        @if(!empty($v1->status))
                                        @php
                                        $is_applied = 1;
                                        @endphp
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <a title="Withdraw Application" href="JavaScript:Void(0);"
                                                class="btn btn-danger btn-block btnApply withdraw-application"
                                                data-zohoid="{{$v->JOBOPENINGID}}" data-id="{{$v->id}}">Withdraw
                                                Application</a>
                                        </div>
                                        @endif
                                        @endif
                                        @endforeach
                                        @if($is_applied == 0)
                                        @foreach ($v->associatecandidates as $k1=>$v1)
                                        @if ($v1->CANDIDATEID == Auth::user()->zoho_id)
                                        @php
                                        $is_saved = 1;
                                        @endphp
                                        <div
                                            class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center border-right-btn border-right-btn">
                                            <img src="{{admin_asset('images/loader.svg')}}"
                                                class="consultant-img-loader">
                                            <a title="Unsave" href="JavaScript:void(0)"
                                                class="btn btn-secondary btn-custom-trans btn-block btnSave savejob-delete colorBlue"
                                                data-zohoid="{{$v->JOBOPENINGID}}">Unsave</a>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center">
                                            <img src="{{admin_asset('images/loader.svg')}}"
                                                class="consultant-img-loader">
                                            <a title="Apply Now!" href="JavaScript:void(0)"
                                                class="btn btn-danger btn-block btnApply btn-custom-trans consultant-job-applied colorRed "
                                                data-reload="true" data-zohoid="{{$v->JOBOPENINGID}}"
                                                data-id="{{$v->id}}">Apply Now!</a>
                                        </div>
                                        @endif
                                        @endforeach
                                        @if($is_saved == 0)

                                        <div
                                            class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center border-right-btn">
                                            <a title="Save" href="JavaScript:void(0);"
                                                class="btn btn-secondary btn-custom-trans btn-block btnSave consultant-job-saved colorBlue"
                                                data-zohoid="{{$v->JOBOPENINGID}}" data-id="{{$v->id}}"
                                                data-redirecttab="true">Save</a>
                                            <img src="{{admin_asset('images/loader.svg')}}"
                                                class="consultant-img-loader">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center">
                                            <img src="{{admin_asset('images/loader.svg')}}"
                                                class="consultant-img-loader">
                                            <a title="Apply Now!" href="javaScript:void(0)"
                                                class="btn btn-danger btn-custom-trans btn-block btnApply consultant-job-applied colorRed "
                                                data-reload="true" data-zohoid="{{$v->JOBOPENINGID}}"
                                                data-id="{{$v->id}}">Apply Now!</a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                                <!-- <div class="boxbtns">
                                    <div class="row no-gutters">
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                            <a title="Save" href="JavaScript:Void(0);" class="btn btn-secondary btn-custom-trans btn-block btnSave consultant-job-saved colorBlue">Save</a>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                            <a title="Apply Now!" href="JavaScript:Void(0);" class="btn btn-danger btn-custom-trans btn-block btnApply consultant-job-applied colorRed">Apply Now!</a>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        @if($count3%2 == 1)
                    </div>
                    <div class="rowJobs row rs-10">
                        @endif
                        <?php $count3 ++ ?>
                        @endif

                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @if(count($notifications))
        <div class="commanContent contentNotifications">
            <h6>Notifications</h6>
            <ul class="listContractHistory">
                @foreach($notifications as $notificationKey => $notoficationValue)
                @if($notificationKey < 3) <li>
                    <div class="row rowHistory align-items-center">
                        <div class="col-md-12">
                            <span class="textDate">{{date('d M. Y',strtotime($notoficationValue->created_at))}}
                                ({{date('h:i',strtotime($notoficationValue->created_at))}})</span>
                            @if($notoficationValue->redirect_url != null)
                            <a href="{{$notoficationValue->redirect_url}}">
                                <p>{{$notoficationValue->message}}</p>
                            </a>
                            @else
                            <p>{{$notoficationValue->message}}</p>
                            @endif
                        </div>
                    </div>
                    </li>
                    @endif
                    @endforeach
            </ul>
            <div class="boxbtns ">
                <div class="row no-gutters">
                    <div class="col-md-12 text-center">
                        <a title="View all" href="{{route('consultant-notifications')}}"
                            class="btn btn-secondary mb-20 cus-btn-blue">View all</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="commanContent border-ra-5 pd-0">
            <h3 class="header-red-bg colorWhite text-left pd-5 how-much-left text-center">How Much Will You Get?</h3>
            <div class="boxHowmuch">
                <div class="contentHowmuch">
                    <p class="pb-1">Enter the Job’s Base Rate</p>
                    <!-- <div class="iconBox iconMYR text-red inlineText">MYR</div> -->
                    <!-- <span class="cus-br-icon">BR</span> -->
                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                    <span class="ml-1">MYR </span>
                    <input type="text" class="text-red text-right" value="" id="c1-base-rate-perday" maxlength="10" />
                    <span class="text-red">/day</span>
                </div>
                <div class="row f-12 rs-5">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Length of contract
                            <a href="#" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5
                                months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="2" title="6-11 months"
                                    checked="" /> 6-11 months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                                months</li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Fees</p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                    checked="" disabled="" /> Admin fee
                                <a href="#" class="iconInfo">
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                            <li>
                                <input type="checkbox" class="fees" name="guaranteed_payment"
                                    title="Guaranteed on-time payment" id="guaranteed_payment" checked="" /> Guaranteed
                                on-time payment
                                <a href="#" class="iconInfo">
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="boxDoyouKnow mb-0">
                    <h6 class="f-14 mb-0 Black">Your Daily Rate Will Be <a href="javascript:;" class="iconInfo">
                            <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                        </a></h6>
                    <h2 class="colorRed" id="daily-rate">MYR 0.00</h2>
                </div>
            </div>
        </div>
        <!-- @if(count($jobsMightLike))
        <h5 class="text-center pt-2 textDarkBlue">Jobs You Might Like...</h5>        
        @foreach($jobsMightLike as $jobKey => $jobValue)
        <div class="commanContent mb-3">
            <a href="{{route('consultantjobdetails',[\Crypt::encryptString($jobValue->id)])}}">
                <ul class="listContract">
                    <li>{{$jobValue->Job_ID}}</li>
                    <li>{{$jobValue->job_type}}</li>
                </ul>
                <h5 class="titles">{{$jobValue->posting_title}}</h5>
            </a>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconMYR text-red">MYR <span class="c-job-base-rate" id="con-copy-11{{$jobKey}}">{{($jobValue->job_base_rate != null) ? $jobValue->job_base_rate : 0}}/day</span></div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconCopy copy-to-clipboard job-tile-button-right"></div>
                </div>
            </div>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconTime">{{$jobValue->job_mode}}</div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconYears">{{$jobValue->work_experience}} exp.</div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconDate">{{date('d M. Y',strtotime($jobValue->date_opened))}}</div>
                </div>
            </div>
            <div class="row rowContract">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="iconBox iconMonths">{{$jobValue->job_duration}} months</div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 titles">
                    <div class="iconBox iconGas">{{$jobValue->industry}}</div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 titles">
                    <div class="iconBox iconplace">{{$jobValue->city}}</div>
                </div>
            </div>
            <div class="boxbtns job-tile-button-right">
                <div class="row no-gutters">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <a title="Save" href="JavaScript:void(0);" class="btn btn-secondary btn-custom-trans btn-block btnSave consultant-job-saved consultant-job-saved colorBlue" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}" data-redirect="true">Save</a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block btnApply consultant-job-applied btn-custom-trans colorRed" data-zohoid="{{$jobValue->JOBOPENINGID}}" data-id="{{$jobValue->id}}">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="text-right mb-4">
            <a href="{{url('/consultant/search?search_type=job&job_might_like=1&jobcategory='.$jobValue->category)}}" class="linkSeeall" title="See All">See All</a>
        </div>
        @endif -->
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
    </div>
</div>
@endsection