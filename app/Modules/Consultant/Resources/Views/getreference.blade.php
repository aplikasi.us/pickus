@if($reference != null)
<div class="form-group">
    <label class="alignment">Name: </label>
    <input type="text" value="{{$reference->name}}" name="edited_refrencename" class="edited_refrencename size-space">
</div>
<div class="form-group">
    <label class="alignment">Position: </label>
    <input type="text" value="{{$reference->position}}" name="edited_refrenceposition" class="edited_refrenceposition size-space">
</div>
<div class="form-group">
    <label class="alignment">Company: </label>
    <input type="text" value="{{$reference->company}}" name="edited_refrencecompany" class="edited_refrencecompany size-space">
</div>
<div class="form-group">
    <label class="alignment">Phone: </label>
    <input type="number" value="{{$reference->phone}}" name="edited_refrencephone" class="edited_refrencephone size-space">
</div>
<div class="form-group">
    <label class="alignment">Email: </label>
    <input type="text" value="{{$reference->email}}" name="edited_constantemail" class="edited_constantemail size-space">
</div>
<div class="form-group">
<a href="{{route('consultantdeleteProfileReference',[$reference->id])}}"
                                                    id="delete-refrence-btn"
                                                    class="buttonDefault del delete-referenceBtn"
                                                    data-id="{{$reference->id}}">Delete</a>
    <input type="submit" value="Save" style="float:right;color: #4FDB73!important;" data-id="{{$reference->id}}" class="buttonDefault save updatebutton-consultantReference spacing-left">
    <a href="{{route('consultant-profile')}}" style="float:right;" class="buttonDefault can">Cancel</a>
    <img src="{{admin_asset('images/loader.svg')}}" class="reference-loader" style="display: none;">
</div>
@else
<div class="form-group">
    <label class="alignment">Name: </label>
    <input type="text" name="edited_refrencename" class="edited_refrencename size-space">
</div>
<div class="form-group">
    <label class="alignment">Position: </label>
    <input type="text" name="edited_refrenceposition" class="edited_refrenceposition size-space">
</div>
<div class="form-group">
    <label class="alignment">Company: </label>
    <input type="text" name="edited_refrencecompany" class="edited_refrencecompany size-space">
</div>
<div class="form-group">
    <label class="alignment">Phone: </label>
    <input type="number" name="edited_refrencephone" class="edited_refrencephone size-space">
</div>
<div class="form-group">
    <label class="alignment">Email: </label>
    <input type="text" name="edited_constantemail" class="edited_constantemail size-space">
</div>
<div id="consultant-Reference-save" style="padding-bottom: 8%;">
    <input type="submit" value="Save" style="float:right;color: #4FDB73!important;" class="buttonDefault save savebutton-consultantReference spacing-left">
    <a href="{{route('consultant-profile')}}" style="float:right;" class="buttonDefault can">Cancel</a>
    <img src="{{admin_asset('images/loader.svg')}}" class="reference-loader" style="display: none;">
</div>
@endif