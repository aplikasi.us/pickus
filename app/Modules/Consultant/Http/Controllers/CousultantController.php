<?php

namespace App\Modules\Consultant\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Humantech\Zoho\Recruit\Api\Client\AuthenticationClient;
use Humantech\Zoho\Recruit\Api\Client\Client;
use Illuminate\Support\Facades\Crypt;
use App\Token;
use CURLFile;
use App\Industry;
use App\States;
use App\Cities;
use App\Clients;
use App\Jobopenings;
use App\Interviews;
use App\Candidates;
use App\Consultant;
use App\Jobassociatecandidates;
use App\Notification;
use App\Clientwishlist;
use App\Settings;
use App\Emailtemplate;
use App\Attachment;
use App\Contracthistory;
use App\Experience;
use App\Education;
use App\Reference;
use App\Signeddocument;
use App\Signeddocumentattachment;
use App\Candiatecategory;
use Input;
use App\Http\Controllers\ZohoqueryController;
use Intervention\Image\ImageManagerStatic as Image;
use App\Console\Commands\Syncdatatozoho;
use App\Candidateprofilerequests;

class CousultantController extends Controller {

    //

    public $token;

    public function __construct() {
        $result = Token::first();
        $this->token = $result->token;
    }

    public function getConsultantProfile() {
        if (empty(Session::get('profile'))) {
            $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            if ($candidate != null) {
                $territory = "MY";
                if ($candidate->territory != null) {
                    $territory = $candidate->territory;
                }
                $result = Token::where('territory', $territory)->first();
                $this->token = $result->token;
            }

            $client = new Client($this->token);
            $profile = $client->getRecordById('Candidates', Auth::user()->zoho_id);
            Session::push('profile', $profile[0]);
            return $profile[0];
        } else {
            return Session::get('profile')[0];
        }
    }

    /*
     * Consultant Homepage
     *
     */

    public function consultantHome(Request $request) {
        if (Auth::user()->role_id == 2) {
            // $candidateId = '401682000002813001';
            $candidateId = Auth::user()->zoho_id;
            $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $interviewCounts = $this->interviewCounts();
            // $jobopenings = Jobopenings::with('associatecandidates')->whereHas('associatecandidates', function ($query) use($candidateId) {
            //     $query->where('CANDIDATEID', $candidateId);
            // })->first();
            // echo '<pre>';
            // print_r($candidateDetailarray->toArray());
            // echo '</pre>';

            $categoryArray[] = "";
            $categoryArray = explode(';', $candidateDetailarray->category);
            // print_r($categoryArray);
            // $category = $candidateDetailarray->category;
            $jobopenings = Jobassociatecandidates::with('job')
                    ->whereHas('job', function ($query) {
                        $query->where('is_delete', 0);
                    })
                    ->whereNotNull('job_id')
                    ->whereNotNull('candidate_id')
                    ->where('CANDIDATEID', $candidateId)
                    ->whereNotNull('status')
                    ->orderBy('updated_at', 'DESC')
                    ->get();
            $jobopeningsSave = Jobassociatecandidates::with('job')
                    ->whereHas('job', function ($query) {
                        $query->where('is_delete', 0);
                    })
                    ->where('CANDIDATEID', $candidateId)
                    ->where('job_save', '1')
                    ->orderBy('updated_at', 'DESC')
                    ->get();
            $jobRecommended = Jobopenings::with('client', 'associatecandidates')->where('is_delete', 0);
            $clientRequest = Candidateprofilerequests::where('CANDIDATEID', Auth::user()->zoho_id)->whereIn('request_status', ['New', 'Approved'])->count();
            foreach ($categoryArray as $category) {
                $jobRecommended->Where('category', 'LIKE', '%' . $category . '%');
                
            }
            $jobRecommended = $jobRecommended->distinct()->orderBy('last_activity_time', 'DESC')->take(10)->get();

            $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
            $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();

            $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected", " Withdrawn"];
            $candidateSubmissionStatus = ["Submitted-to-client", "Approved by client", "Rejected by client"];
            $candidateInterviewStatus = ["Interview-to-be-Scheduled", "Interview-Scheduled", "Interview-in-Progress", "Interviewed-Accepted", "Interviewed-Rejected"];
            $candidateOfferStatus = ["To-be-Offered", "Offer-Accepted", "Offer-Made", "Offer-Declined", "Offer-Withdrawn"];
            $candidateHireStatus = ["No-Show", "Converted - Employee", "Converted - Contractor"];

            return view('consultant::consultant_home', compact('candidateDetailarray', 'interviewCounts', 'jobopenings', 'jobopeningsSave', 'jobRecommended', 'clientRequest', 'jobsMightLike', 'candidateScreeningStatus', 'candidateSubmissionStatus', 'candidateInterviewStatus', 'candidateOfferStatus', 'candidateHireStatus'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /**
     * 
     * interview count
     * 
     */
    public function interviewCounts() {
        $upcomingInterview = Interviews::where('CANDIDATEID', Auth::user()->zoho_id)->where('territory', Session::get('territory'))->where('start_datetime', '>=', date('Y-m-d H:i:s'))->count();
        $successfulHires = Interviews::where('CANDIDATEID', Auth::user()->zoho_id)->where('territory', Session::get('territory'))->whereNotNull('interview_status')->count();
        return ['upcomingInterview' => $upcomingInterview, 'successfulHires' => $successfulHires];
    }

    /*
     * Consultant Profile
     *
     */

    public function consultantProfile(Request $request) {
        if (Auth::user()->role_id == 2) {
            // $candidateDetailarray = $this->getConsultantProfile();
            $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $expirenceDetails = Experience::where('CANDIDATEID', Auth::user()->zoho_id)->take(10)->get();
            $alreadyWorkingInCompany = Experience::where([
                        'CANDIDATEID' => Auth::user()->zoho_id,
                        'is_currently_working_here' => "true"
                    ])->whereNotNull('is_currently_working_here')->where('is_currently_working_here', '!=', "")->first();

            $alreadyPersuing = Education::where([
                        'CANDIDATEID' => Auth::user()->zoho_id,
                        'currently_pursuing' => "true"
                    ])->whereNotNull('currently_pursuing')->where('currently_pursuing', '!=', "")->first();
            $educationDetails = Education::where('CANDIDATEID', Auth::user()->zoho_id)->take(10)->get();
            $referenceDetails = Reference::where('CANDIDATEID', Auth::user()->zoho_id)->orderBy('created_at', 'desc')->take(10)->get();
            $experienceList = $this->experienceList();
            $jobCategoryList = $this->jobCategoryList();
            $stateList = $this->stateList();
            return view('consultant::consultant_profile', compact('candidateDetailarray', 'experienceList', 'jobCategoryList', 'expirenceDetails', 'educationDetails', 'referenceDetails', 'alreadyWorkingInCompany', 'stateList', 'alreadyPersuing'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    public function stateList() {
        $userTerritory = User::where('id', Auth::user()->id)->pluck('territory')->first();
        return States::where('country_name', $userTerritory)->pluck('state_name', 'id')->toArray();
    }

    public function getCityList(Request $request) {
        $city = Cities::where('state_id', $request->id)->pluck('city_name', 'city_name')->toArray();
        return response()->json([
                    'city' => $city
        ]);
    }

    /*     * *
     * 
     * Consultant profile changes
     */

    public function consultantProfileChange(Request $request) {
        if (Auth::user()->role_id == 2) {
            $base_rate = $request->base_rate;
            $rbase_rate = $request->rbase_rate;
            $nprvalue = $request->nprvalue;
            $skillValue = $request->skillValue;
            $certiValue = $request->certiValue;
            $fulltime = $request->fulltime;
            $parttime = $request->parttime;
            $willingtravel = $request->willingtravel;
            $project = $request->project;
            $support = $request->support;
            $datevalue = $request->datevalue;
            // echo $project;
            // exit;

            $id = $request->id;
            $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            if ($candidate != null) {
                if (isset($base_rate)) {
                    $candidate->base_rate = $base_rate;
                }
                if (isset($rbase_rate)) {
                    $candidate->reserved_base_rate = $rbase_rate;
                }
                if (isset($nprvalue)) {
                    $candidate->notice_period_days = $nprvalue;
                }
                if (isset($request->type) && $request->type == "skill_set") {
                    $candidate->skill_set = $skillValue;
                }
                if (isset($request->type) && $request->type == "certificate") {
                    $candidate->certification_and_training = $certiValue;
                }
                if (isset($fulltime)) {
                    $candidate->full_time = $fulltime;
                }
                if (isset($parttime)) {
                    $candidate->part_time = $parttime;
                }
                if (isset($willingtravel)) {
                    $candidate->willing_to_travel = $willingtravel;
                }
                if (isset($project)) {
                    $candidate->project = $project;
                }
                if (isset($support)) {
                    $candidate->support = $support;
                }
                if (isset($datevalue)) {
                    $candidate->availability_date = date('Y-m-d', strtotime($datevalue));
                }
                $candidate->is_synced = 0;
                $candidate->save();
            }
            return $candidate;
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /*     * *
     * 
     * Consultant profile upadte ajax 
     * 
     */

    public function updateProfile(Request $request) {
        if (Auth::user()->role_id == 2) {
            $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            if ($candidate != null) {
                $state = States::where('id', $request->state)->first()->state_name;
                $candidate->first_name = $request->firstname;
                $candidate->last_name = $request->lastname;
                $candidate->experience_in_years = $request->experience;
                $candidate->category = implode(',', $request->category);
                $candidate->sap_job_title = $request->title;
                $candidate->mobile_number = $request->mobile_number;
                $candidate->state = $state;
                $candidate->city = $request->city;
                $candidate->is_synced = 0;
                $candidate->save();

                if (isset($request->category)) {
                    Candiatecategory::where([
                        'candidate_id' => $candidate->id
                    ])->delete();
                    foreach ($request->category as $catKey => $catValue) {
                        $category = new Candiatecategory();
                        $category->candidate_id = $candidate->id;
                        $category->CANDIDATEID = $candidate->CANDIDATEID;
                        $category->category = $catValue;
                        $category->created_at = date('Y-m-d H:i:s', time());
                        $category->updated_at = date('Y-m-d H:i:s', time());
                        $category->save();
                    }
                }
            }
            $user = User::where('id', Auth::user()->id)->first();
            if ($user != null) {
                $user->first_name = $request->firstname;
                $user->last_name = $request->lastname;
                $user->phone = $request->mobile_number;
                if ($request->file('profile_pic') != null) {
                    $filename = time() . '-' . $request->file('profile_pic')->getClientOriginalName();
                    $path = public_path('profile_image/' . $filename);
                    Image::make($request->file('profile_pic')->getRealPath())->save($path);
                    $user->profile_pic = $filename;
                    $this->uploadProfilePicInZoho($user->zoho_id, $path);
                }
                $user->save();
            }
            Session::flash('success', 'Profile has been updated successfully.');
            return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    public function uploadProfilePicInZoho($recordId, $filePath) {
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if ($candidate != null) {
            $territory = "MY";
            if ($candidate->territory != null) {
                $territory = $candidate->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $this->token = $result->token;
        }

        $ch = curl_init();
        $cFile = new CURLFile($filePath, 'image/png');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://recruit.zoho.com/recruit/private/xml/Candidates/uploadPhoto?authtoken=" . $this->token . "&scope=recruitapi&version=2");
        curl_setopt($ch, CURLOPT_POST, true);
        $post = array("id" => $recordId, "content" => $cFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        return $response;
    }

    /*
     * Conustant publaish now
     */

    public function updatePublish(Request $request) {
        if (Auth::user()->role_id == 2) {
            $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            if ($candidate != null) {
                $candidate->publish_in_us = 1;
                $candidate->is_synced = 0;
                $candidate->save();
                $data['Publish in Us'] = 'true';
                // echo $this->token;
                $territory = "MY";
                if ($candidate->territory != null) {
                    $territory = $candidate->territory;
                }
                $result = Token::where('territory', $territory)->first();
                $this->token = $result->token;

                //$client = new Client($this->token);
                // $filds = $client->getFields('Candidates');
                //$response = $client->updateRecords('Candidates', $candidate->CANDIDATEID, $data);
                // echo '<pre>';
                // print_r($response);
                // echo '</pre>';
                // exit;
                Session::flash('success', 'Your Superstar profile has been published!');
                return redirect()->route('consultant-profile');
            } else {
                Session::flash('error', 'something went wrong please try to after few minutes.');
                return redirect()->route('consultant-profile');
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /**
     * 
     * 
     */
    /*
     * Conustant publaish now
     */
    public function updateUnpublish(Request $request) {
        if (Auth::user()->role_id == 2) {
            $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            if ($candidate != null) {
                $candidate->publish_in_us = 0;
                $candidate->is_synced = 0;
                $candidate->save();
                $data['Publish in Us'] = 'false';

                $territory = "MY";
                if ($candidate->territory != null) {
                    $territory = $candidate->territory;
                }
                $result = Token::where('territory', $territory)->first();
                //$this->token = $result->token;

                //$client = new Client($this->token);
                //$client->updateRecords('Candidates', $candidate->CANDIDATEID, $data);
                Session::flash('error', "so sad to see you off. We'll be waiting for you!");
                return redirect()->route('consultant-profile');
            } else {
                Session::flash('error', 'something went wrong please try to after few minutes.');
                return redirect()->route('consultant-profile');
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /*     * *
     * 
     * Consultant publish fileds zoho 
     */

    public function syncCandidatePubllish() {
        $candidate = Candidates::where([
                    'zoho_id' => Auth::user()->zoho_id, // Remove this line
                ])->get();
        if (count($candidate)) {
            $zohoQuery = new ZohoqueryController();
            foreach ($attachments as $key => $value) {
                $response = $zohoQuery->updateAttachment($value->zoho_id, $value->filename, "Others");
                if (count($response)) {
                    $value->attachment_id = $response['response']['result']['recorddetail']['FL'][0]['content'];
                    $value->is_synced = 1;
                    $value->save();
                }
            }
        }
        return;
    }

    public function updateCandidate($candidateId, $data) {
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $territory = "MY";
        if ($candidate->territory != null) {
            $territory = $candidate->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        $client = new Client($this->token);
        $queryResult = $client->updateRecords('Candidates', $candidateId, $data);
        return $queryResult;
    }

    /*
     * Consultant Interviews
     *
     */

    public function interviews() {
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $territory = "MY";
        if ($candidate->territory != null) {
            $territory = $candidate->territory;
        }
        $result = Token::where('territory', $territory)->first();
        // $this->token = $result->token;
        // $client = new Client($this->token);
        // $candidateId = Auth::user()->zoho_id;
        // $interViews = $client->getSearchRecords(
        //         'Interviews', 'Interviews(Posting Title,Job Opening ID,Interview Name,Candidate Name,Type,Interview Owner,From,To,Interview Status,Venue)', '(CANDIDATEID|is|' . $candidateId . ')', ['sortColumnString' => 'Last Activity Time', 'sortOrderString' => 'desc']
        // );
        $upComingInterviewCount = 0;
        $successfulHiresCount = 0;
        $interviewList['upcoming_interviews'] = [];
        $interviewList['concluded_interviews'] = [];
        // if (count($interViews)) {
        //     foreach ($interViews as $key1 => $value1) {
        //         if (!isset($value1['JOBOPENINGID'])) {
        //             continue;
        //         }
        //         $job = $candidate = [];
        //         $job = $client->getRecordById('JobOpenings', $value1['JOBOPENINGID']);
        //         $candidate = $client->getRecordById('Candidates', $value1['CANDIDATEID']);
        //         if (count($job)) {
        //             $value1['job'] = $job[0];
        //         }
        //         if (count($candidate)) {
        //             $value1['candidate'] = $candidate[0];
        //         }
        //         if (date("Y-m-d H:i:s") < $value1['Start DateTime']->format('Y-m-d H:i:s')) {
        //             $upComingInterviewCount++;

        //             $interviewList['upcoming_interviews'][] = $value1;
        //         }
        //         if (date("Y-m-d H:i:s") > $value1['Start DateTime']->format('Y-m-d H:i:s')) {
        //             $successfulHiresCount++;
        //             $interviewList['concluded_interviews'][] = $value1;
        //         }
        //     }
        // }
        return [
            'upComingInterviewCount' => $upComingInterviewCount,
            'successfulHiresCount' => $successfulHiresCount,
            'interviewList' => $interviewList
        ];
    }

    public function consultantInterviews(Request $request) {
        if (Auth::user()->role_id == 2) {
            $candidateId = Auth::user()->zoho_id;
//            $upcomingInterview = Interviews::with([
//                        'job',
//                        'client',
//                        'notes' => function ($query) {
//                            $query->latest('created_time')->first();
//                        }
//                    ])->where('territory', Session::get('territory'))->where('CANDIDATEID', $candidateId)->where('start_datetime', '>=', date('Y-m-d') . ' 00:00:00')->get();
//            $concludedInterview = Interviews::with([
//                        'job',
//                        'client',
//                        'notes' => function ($query) {
//                            $query->latest('created_time')->first();
//                        }
//                    ])->where('territory', Session::get('territory'))->where(function ($query) {
//                        $query->where('interview_status', '<>', "");
//                        $query->whereNotNull('interview_status');
//                    })->where('CANDIDATEID', Auth::user()->zoho_id)->get();
//            $concludedInterviewcount = Interviews::with(['job', 'client'])->where('territory', Session::get('territory'))->where(function ($query) {
//                        $query->where('interview_status', '<>', "");
//                        $query->whereNotNull('interview_status');
//                    })->where('CANDIDATEID', Auth::user()->zoho_id)->count();
//            $interviewCounts = $this->interviewCounts();
            $interviews = $this->interviews();
            /* Jobs you might Like */
            $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $categoryArray[] = "";
            $categoryArray = explode(';', $candidateDetailarray->category);
            $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
            $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->where('territory', Session::get('territory'))->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
            return view('consultant::consultant_interviews', compact('interviews', 'jobsMightLike'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /*
     * Consultant Contract History
     *
     */

    public function consultantContractHistory(Request $request) {
        if (Auth::user()->role_id == 2) {
            $contracthistory = Contracthistory::where('CANDIDATEID', Auth::user()->zoho_id)->get();

            /* Jobs you might Like */
            $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $categoryArray[] = "";
            $categoryArray = explode(';', $candidateDetailarray->category);
            $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
            $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->where('territory', Session::get('territory'))->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();

            return view('consultant::consultant_contract_history', compact('contracthistory', 'jobsMightLike'));
//            $url = 'https://recruit.zoho.com/recruit/private/json/Candidates/getTabularRecords?authtoken=' . $this->token . '&scope=recruitapi&id=' . Auth::user()->zoho_id . '&tabularNames=(Contract History)';
//            // Initialise a cURL handle
//            $url = str_replace(" ", '%20', $url);
//            $ch = curl_init();
//            // Set any other cURL options that are required
//            curl_setopt($ch, CURLOPT_HEADER, FALSE);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//            curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
//            curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//            curl_setopt($ch, CURLOPT_URL, $url);
//            $response = curl_exec($ch);  // Execute a cURL request
//            curl_close($ch);      // Closing the cURL handle
//            $contracthistory = json_decode($response, true);
//            $data = $this->prepareHistoryArray($contracthistory);
//            return view('consultant::consultant_contract_history', compact('data'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

//    public function prepareHistoryArray($contracthistory) {
//        $data = [];
//        unset($contracthistory['response']['uri']);
//        foreach ($contracthistory['response']['result']['Candidates']['FL']['TR'] as $key3 => $value3) {
//            if (isset($value3['TL'])) {
//                if (key_exists(0, $value3['TL'])) { // Getting improper data so
//                    $data1 = [];
//                    foreach ($value3['TL'] as $tdkey3 => $tdvalue3) {
//                        if ($tdvalue3['val'] == "Past Client" && isset($tdvalue3['content'])) {
//                            $data1['past_client'] = $tdvalue3['content'];
//                        }
//                        if ($tdvalue3['val'] == "Past Project" && isset($tdvalue3['content'])) {
//                            $data1['past_project'] = $tdvalue3['content'];
//                        }
//                        if ($tdvalue3['val'] == "Pay Rate" && isset($tdvalue3['content'])) {
//                            $data1['pay_rate'] = $tdvalue3['content'];
//                        }
//                        if ($tdvalue3['val'] == "Contract Period_From" && isset($tdvalue3['content'])) {
//                            $data1['contract_period_from'] = $tdvalue3['content'];
//                        }
//                        if ($tdvalue3['val'] == "Contract Period_To" && isset($tdvalue3['content'])) {
//                            $data1['contract_period_to'] = $tdvalue3['content'];
//                        }
//                        if ($tdvalue3['val'] == "Additional Notes" && isset($tdvalue3['content'])) {
//                            $data1['additional_note'] = $tdvalue3['content'];
//                        }
//                    }
//                    $data[] = $data1;
//                }
//            }
//        }
//        return $data;
//    }

    /*
     * Consultant Attachments
     *
     */

    public function consultantAttachments(Request $request) {
        if (Auth::user()->role_id == 2) {
            $attchements = Attachment::where('zoho_id', Auth::user()->zoho_id)->whereIn('category', array('Resume', 'Others'))->get();
            // $serviceAgreements = Signeddocument::with(['attachments' => function($query) {
            //                 $query->where('filename', 'LIKE', '%signed%');
            //             }])->where('CANDIDATEID', Auth::user()->zoho_id)->get();
            $serviceAgreements = Signeddocument::with('attachments')->where('CANDIDATEID', Auth::user()->zoho_id)->get();
            /* Jobs you might Like */
            $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $categoryArray[] = "";
            $categoryArray = explode(';', $candidateDetailarray->category);
            $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
            $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();

            return view('consultant::consultant_attachments', compact('attchements', 'serviceAgreements', 'jobsMightLike'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    public function downloadAttachment(Request $request) {
        if (Auth::user()->role_id == 2) {
            $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $territory = "MY";
            if ($candidate->territory != null) {
                $territory = $candidate->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $this->token = $result->token;
            if ($request->type == "attachment") {
                $attachment = Attachment::where('id', $request->id)->first();
                /* Downloading file from local storage */
                $destination_folder = public_path('cv/' . $attachment->filename);
                if (file_exists($destination_folder)) {
                    return response()->download($destination_folder);
                } else { // Downloading file from zoho signed attachment
                    $url = "https://recruit.zoho.com/recruit/private/json/Candidates/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $attachment->zoho_id;
                    file_put_contents("$destination_folder", fopen($url, 'r'));
                }

                if (file_exists($destination_folder)) {
                    return response()->download($destination_folder);
                } else {
                    Session::flash('error', 'Attachment does not exists');
                    return redirect()->route('consultant-attachments');
                }
            } else {
                $attachment = Signeddocumentattachment::where('attachment_id', $request->id)->first();
                /* Downloading file from local storage */
                $destination_folder = public_path('signed_attachments/' . $attachment->filename);
                if (file_exists($destination_folder)) {
                    return response()->download($destination_folder);
                } else { // Downloading file from zoho signed attachment
                    $url = "https://recruit.zoho.com/recruit/private/json/CustomModule5/downloadFile?authtoken=" . $this->token . "&scope=recruitapi&version=2&id=" . $request->id;
                    file_put_contents("$destination_folder", fopen($url, 'r'));
                }
                if (file_exists($destination_folder)) {
                    return response()->download($destination_folder);
                } else {
                    Session::flash('error', 'Signed document does not exists');
                    return redirect()->route('consultant-attachments');
                }
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /**
     * 
     * Delete Attachement
     * 
     */
    public function deleteAttachment(Request $request) {
        if (Auth::user()->role_id == 2) {
            $zohoQuery = new ZohoqueryController();
            if ($request->type == "attachment") {
                $attachment = Attachment::where('id', $request->id)->first();
                if (count($attachment)) {
                    $attachment->delete();


                    $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                    $territory = "MY";
                    if ($Candidates->territory != null) {
                        $territory = $Candidates->territory;
                    }
                    $result = Token::where('territory', $territory)->first();
                    $zohoQuery->token = $result->token;


                    $zohoQuery->deleteCandidateAttachment($attachment->attachment_id);
                    Session::flash('success', 'Attachement deleted successfully');
                } else {
                    Session::flash('error', 'Please try again after sometimes');
                }
                return redirect()->route('consultant-attachments');
            } else {
                $Signeddocument = Signeddocument::where('id', $request->id)->first();
                /* Downloading file from local storage */
                if (count($Signeddocument)) {
                    $Signeddocumentattachment = Signeddocumentattachment::where('signed_document_id', $Signeddocument->id)->get();
                    $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
                    $territory = "MY";
                    if ($Candidates->territory != null) {
                        $territory = $Candidates->territory;
                    }
                    $result = Token::where('territory', $territory)->first();
                    $zohoQuery->token = $result->token;

                    $zohoQuery->deletesignedAttachment($Signeddocument->CUSTOMMODULE5_ID);
                    foreach ($Signeddocumentattachment as $key => $value) {
                        $value->delete();
                    }
                    $Signeddocument->delete();
                    Session::flash('success', 'Attachement delete successfully');
                } else {
                    Session::flash('error', 'Please try again after sometimes');
                }
                return redirect()->route('consultant-attachments');
            }
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    public function ResumeAttachment(Request $request) {
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $file = $_FILES["customResume"]["tmp_name"];
        $file_des = $_FILES["customResume"]["name"];
        move_uploaded_file($file, public_path("cv/" . $file_des));
        $file = public_path("uploads/" . $file_des);
        if (isset($file_des) && !empty($file_des)) {
            $Candidates->resume = $file_des;
        }
        $Candidates->is_synced = 0;
        $Candidates->save();

        $territory = "MY";
        if ($Candidates->territory != null) {
            $territory = $Candidates->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;


        $client = new Client($this->token);
        $token = $this->token;
        $file = public_path('cv/' . $file_des);
        // $file = public_path('uploads/1549027470-clients-dashboard-phase-2-v4.4_numbered.pdf');
        $ch = curl_init();
        $cFile = new CURLFile("$file", 'application/pdf');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://recruit.zoho.com/recruit/private/xml/Candidates/uploadFile?authtoken=$token&scope=recruitapi&version=2&type=Resume");
        curl_setopt($ch, CURLOPT_POST, true);
        $post = array("id" => Auth::user()->zoho_id, "content" => $cFile);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);

        Attachment::where('zoho_id', Auth::user()->zoho_id)->where('category', 'Resume')->delete();


        $size = filesize(public_path('cv/' . $Candidates->resume));
        $attachment = new Attachment();
        $attachment->candidate_id = $Candidates->id;
        $attachment->zoho_id = $Candidates->CANDIDATEID;
        $attachment->filename = $file_des;
        $attachment->size = $this->human_filesize($size);
        $attachment->category = "Resume";
        if ($territory == "MY") {
            $attachment->attach_by = "Talent Admin";
        } else {
            $attachment->attach_by = "Talent PH Admin";
        }
        $attachment->created_at = date("Y-m-d h:i:s", time());
        $attachment->updated_at = date("Y-m-d h:i:s", time());
        $attachment->save();
        return $file_des;
        // Session::flash('success', 'success full update your file!!');
        // return redirect()->route('consultant-profile');
    }
    public function human_filesize($bytes, $decimals = 2) {
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor > 0)
            $sz = 'KMGT';
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
    }
    public function uploadAttachment(Request $request) {
        $extension = $request->file()['customattachment']->getClientOriginalExtension();
        $filename = $request->file()['customattachment']->getClientOriginalName();
        $size = $this->formatSizeUnits($request->file()['customattachment']->getClientSize());
        if (in_array($extension, ['docx', 'doc', 'pdf'])) {
            $candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            if ($candidates != null) {
                $attachment = new Attachment();
                $attachment->candidate_id = $candidates->id;
                $attachment->zoho_id = $candidates->CANDIDATEID;
                $attachment->filename = $filename;
                $attachment->size = $size;
                $attachment->category = "Others";
                $attachment->attach_by = "Talent Admin";
                $attachment->created_at = date("Y-m-d h:i:s", time());
                $attachment->updated_at = date("Y-m-d h:i:s", time());

                /* Saving file */
                $file = $request->file()['customattachment'];
                $file->move(public_path('cv/'), $file->getClientOriginalName());
                /* Saving attachment in zoho */
                $attachmentId = $this->saveAttachmentToZoho($attachment);
                $attachment->attachment_id = $attachmentId;
                $attachment->save();
                /* Sending Attachemnt Notification */
                $this->attachmentNotification($candidates, $filename);
                Session::flash('success', 'Attachment has been uploaded successfully');
                return redirect()->route('consultant-attachments');
            } else {
                Session::flash('error', 'Session expired. Please login again');
                return redirect()->route('consultant-attachments');
            }
        } else {
            Session::flash('error', 'Invalid file format. Please upload your resume in .docx, .doc or .pdf format.');
            return redirect()->route('consultant-attachments');
        }
    }

    /* Save candidate attachments in zoho */

    public function saveAttachmentToZoho($attachment) {
        $attachmentId = "";
        $attachments = Attachment::where('zoho_id', Auth::user()->zoho_id)->get();
        if ($attachments != null) {
            $zohoQuery = new ZohoqueryController();
            $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $territory = "MY";
            if ($Candidates->territory != null) {
                $territory = $Candidates->territory;
            }
            $result = Token::where('territory', $territory)->first();
            $zohoQuery->token = $result->token;

            $response = $zohoQuery->updateAttachment($attachment->zoho_id, $attachment->filename, "Others");
            if (!empty($response)) {
                if (isset($response['response']['result']['recorddetail']['FL'])) {
                    $attachmentId = $response['response']['result']['recorddetail']['FL'][0]['content'];
                }
            }
        }
        return $attachmentId;
    }

    public function attachmentNotification($candidates, $filename) {
        /* Send Notification to Admin */
        if ($candidates->territory == "PH") {
            $setting = Settings::where('settingkey', "ph_email")->first();
        } else {
            $setting = Settings::where('settingkey', "my_email")->first();
        }
        $data['db_sender_id'] = $candidates->id;
        $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
        $data['sender_id'] = $candidates->CANDIDATEID;
        $data['receiver_id'] = "";
        $data['message'] = $candidates->first_name . ' ' . $candidates->last_name . ' uploaded resume to his profile';
        $data['type'] = "candidate_upload_attachment";
        $data['to'] = "Admin";
        $data['territory'] = $candidates->territory;
        $this->sendNotification($data);
        $template = EmailTemplate::where('name', 'candidate_upload_attachment')->first();
        if ($template != null) {
            $emaildata['email'] = $setting->settingvalue;
            $emaildata['candidate_name'] = $candidates->first_name . ' ' . $candidates->last_name;
            $emaildata['subject'] = $candidates->first_name . ' ' . $candidates->last_name . " has upload new document";
            Mail::send([], [], function($messages) use ($template, $emaildata, $filename) {
                $messages->to($emaildata['email'], "Admin")
                        ->subject($emaildata['subject'])
                        ->setBody($template->parse($emaildata), 'text/html')
                        ->attach(\Swift_Attachment::fromPath(public_path('/cv/' . $filename)));
            });
        }
        return;
    }

    public function formatSizeUnits($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     *  consultant settings 
     * 
     */
    public function consultantSettings(Request $request) {
        if (Auth::user()->role_id == 2) {
            // $candidateDetailarray = $this->getConsultantProfile();
            $user = User::where('id', Auth::user()->id)->first();
            $id = Crypt::encryptString($user->id);
            $experienceList = $this->experienceList();
            $jobCategoryList = $this->jobCategoryList();
            $jobAlertsTags = $this->jobAlertsTags();

            /* Jobs you might Like */
            $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $categoryArray[] = "";
            $categoryArray = explode(';', $candidateDetailarray->category);
            $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
            $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->where('territory', Session::get('territory'))->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
            return view('consultant::consultant_settings', compact('jobsMightLike', 'user', 'experienceList', 'jobCategoryList', 'id', 'jobAlertsTags'));
        } else {
            Session::flash('error', 'Your session has been expired. Please login again');
            return redirect()->route('home');
        }
    }

    /**
     * Job Alerts tags
     */
    public function jobAlertsTags() {
        return [
            'All' => 'All',
            'Basis/S&A' => 'Basis/S&A',
            'Technical' => 'Technical',
            'Functional' => 'Functional',
            'Project Manager/Solution Architect' => 'Project Manager/Solution Architect'
        ];
    }

    /* View Job Details */

    public function viewJobDetail(Request $request) {
        $decrypted = Crypt::decryptString($request->id);
        $candidateScreeningStatus = ["New", "Waiting-for-Evaluation", "Contacted", "Contact in Future", "Not Contacted", "Attempted to Contact", "Qualified", "Unqualified", "Qualified Expat", "Unqualified Expat", "Junk candidate", "Associated", "Application Withdrawn", "Rejected"];
        $candidateSubmissionStatus = ["Submitted-to-client", "Approved by client", "Rejected by client"];
        $candidateInterviewStatus = ["Interview-to-be-Scheduled", "Interview-Scheduled", "Interview-in-Progress", "Interviewed-Accepted", "Interviewed-Rejected"];
        $candidateOfferStatus = ["To-be-Offered", "Offer-Accepted", "Offer-Made", "Offer-Declined", "Offer-Withdrawn"];
        $candidateHireStatus = ["No-Show", "Converted - Employee", "Converted - Contractor"];

        $job = Jobopenings::with('associatecandidates')->where('id', $decrypted)->where('is_delete', '0')->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();

        $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $categoryArray[] = "";
        $categoryArray = explode(';', $candidateDetailarray->category);
        $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
        $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
        return view("consultant::consultant_job_details", compact('jobsMightLike', 'job', 'Candidates', 'candidateScreeningStatus', 'candidateSubmissionStatus', 'candidateInterviewStatus', 'candidateOfferStatus', 'candidateHireStatus'));
    }

    /**
     * 
     * popup edit profiles
     * 
     */
    public function editProfile(Request $request) {
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (isset($request->first_name) && !empty($request->first_name)) {
            $Candidates->first_name = $request->first_name;
        }
        if (isset($request->last_name) && !empty($request->last_name)) {
            $Candidates->last_name = $request->last_name;
        }
        if (isset($request->sap_job_title) && !empty($request->sap_job_title)) {
            $Candidates->sap_job_title = $request->sap_job_title;
        }
        if (isset($request->experience_in_years) && !empty($request->experience_in_years)) {
            $Candidates->experience_in_years = $request->experience_in_years;
        }
        if (isset($request->category) && !empty($request->category)) {
            $Candidates->category = $request->category;
        }
        if (isset($request->mobile_number) && !empty($request->mobile_number)) {
            $Candidates->mobile_number = $request->mobile_number;
        }
        if (isset($request->state) && !empty($request->state)) {
            $Candidates->state = $request->state;
        }
        if (isset($request->country) && !empty($request->country)) {
            $Candidates->country = $request->country;
        }
        if (isset($request->country) && !empty($request->country)) {
            $Candidates->country = $request->country;
        }
        if (isset($request->city) && !empty($request->city)) {
            $Candidates->city = $request->city;
        }
        $Candidates->is_synced = 0;
        $Candidates->updated_at = date('Y-m-d H:i:s', time());
        $Candidates->save();
        Session::flash('success', 'Profile has been updated successfully.');
        return redirect()->route('consultant-profile');
    }

    /**
     * 
     * popup edit profiles
     * 
     */
    public function editProfileSummary(Request $request) {
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (isset($request->additional_info) && !empty($request->additional_info)) {
            $Candidates->additional_info_2 = $request->additional_info;
            $Candidates->is_summary_requested = 1;
        }
        $Candidates->is_synced = 0;
        $Candidates->save();

        if ($Candidates != null) {
            /* Send Notification to Admin */
            if ($Candidates->territory == "PH") {
                $setting = Settings::where('settingkey', "ph_email")->first();
            } else {
                $setting = Settings::where('settingkey', "my_email")->first();
            }
            $data['db_sender_id'] = $Candidates->id;
            $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
            $data['sender_id'] = $Candidates->CANDIDATEID;
            $data['receiver_id'] = "";
            $data['message'] = $Candidates->first_name . ' ' . $Candidates->last_name . ' has submitted profile summary. Click here(<a href="' . route('editConsultant', [$Candidates->CANDIDATEID]) . '">' . $Candidates->CANDIDATEID . '</a>) to review. ';
            $data['type'] = "candidate_submit_summary";
            $data['to'] = "Admin";
            $data['territory'] = $Candidates->territory;

            $this->sendNotification($data);
            $template = EmailTemplate::where('name', 'candidate_submit_summary')->first();
            if ($template != null) {
                $emaildata['email'] = $setting->settingvalue;
                $emaildata['candidate_name'] = $Candidates->first_name . ' ' . $Candidates->last_name;
                $emaildata['profile_link'] = route('editConsultant', [$Candidates->CANDIDATEID]);
                $emaildata['candidate_id'] = $Candidates->CANDIDATEID;
                $emaildata['subject'] = $Candidates->first_name . ' ' . $Candidates->last_name . " has request for summary approval";
                Mail::send([], [], function($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                            ->subject($emaildata['subject'])
                            ->setBody($template->parse($emaildata), 'text/html');
                });
            }
        }

        Session::flash('success', 'Your profile summary has been successfully submitted for admin approval. You will be notified once approved.');
        return response()->json([
                    'status' => 200
        ]);
        // return redirect()->route('consultant-profile');
    }

    /**
     * 
     * popup edit profiles
     * 
     */
    public function editProfileExprirence(Request $request) {
        $experience = Experience::where('id', $request->id)->first();
        if (count($experience)) {
            $html = view('consultant::consultant_experienceform', compact('experience'))->render();
            return response()->json([
                        'code' => 200,
                        'html' => $html
            ]);
        } else {
            return response()->json([
                        'code' => 401,
                        'html' => ""
            ]);
        }
        Session::flash('success', 'Experience has been updated successfully.');
        return redirect()->route('consultant-profile');
    }

    /**
     * 
     * popup add new profiles Exprirence
     * 
     */
    public function addProfileExprirence(Request $request) {
        $zoho_id = Auth::user()->zoho_id;
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();

        if (count($Candidates)) {
            $Candidates->is_synced = 0;
            $Candidates->save();

            $request->work_duration_from_month = $request->duration_from_month;
            $request->work_duration_from_year = $request->duration_from_year;
            $request->work_duration_to_month = $request->duration_to_month;
            $request->work_duration_to_year = $request->duration_to_year;
            //$zohoQuery = new ZohoqueryController();

            $territory = "MY";
            if ($Candidates->territory != null) {
                $territory = $Candidates->territory;
            }
            $result = Token::where('territory', $territory)->first();
            //$zohoQuery->token = $result->token;

            $experienceList = Experience::where(['CANDIDATEID' => Auth::user()->zoho_id])->get();
            //$xmlData = $zohoQuery->createUpdateExperienceXmlData($experienceList, $request, "add");
            //$response = $zohoQuery->addCandidateTabularRecord(Auth::user()->zoho_id, $xmlData);

            //$tabularData = $zohoQuery->getCandidateTabularRecords(Auth::user()->zoho_id, "Experience Details");
            /* Removing old experience */

                    $experience = new Experience();
                    $experience->candidate_id = $Candidates->id;
                    $experience->CANDIDATEID = Auth::user()->zoho_id;
                    $experience->is_currently_working_here = $request->currently_working_here;
                    $experience->occupation = $request->occupation;
                    $experience->company = $request->company;
                    $experience->industry = $request->industry;
                    $experience->work_duration = $request->duration_from_month . "-" . $request->duration_from_year;
                    $experience->work_duration_to = $request->duration_to_month . "-" . $request->duration_to_year;
                    $experience->summary = $request->summary;
                    $experience->created_at = date('Y-m-d h:i:s', time());
                    $experience->updated_at = date('Y-m-d h:i:s', time());
                    $experience->save();

            // Experience::where(['CANDIDATEID' => Auth::user()->zoho_id])->delete();

            // if (key_exists(0, $tabularData['response']['result']['Candidates']['FL']['TR'])) {
            //     $data = $tabularData['response']['result']['Candidates']['FL']['TR'];
            // } else {
            //     $data[0] = $tabularData['response']['result']['Candidates']['FL']['TR'];
            // }
            // foreach ($data as $trkey2 => $trvalue2) {
            //     if (key_exists('TL', $trvalue2)) {
            //         if (key_exists(0, $trvalue2['TL'])) { // Getting improper data so
            //             foreach ($trvalue2['TL'] as $tdkey2 => $tdvalue2) {
            //                 if ($tdvalue2['val'] == "TABULARROWID" && isset($tdvalue2['content'])) {
            //                     $experience = new Experience();
            //                     $experience->candidate_id = $Candidates->id;
            //                     $experience->CANDIDATEID = Auth::user()->zoho_id;
            //                     $experience->TABULARROWID = $tdvalue2['content'];
            //                     $experience->is_currently_working_here = "false";
            //                 }
            //                 if ($tdvalue2['val'] == "Occupation / Title" && isset($tdvalue2['content'])) {
            //                     $experience->occupation = $tdvalue2['content'];
            //                 }
            //                 if ($tdvalue2['val'] == "Company" && isset($tdvalue2['content'])) {
            //                     $experience->company = $tdvalue2['content'];
            //                 }
            //                 if ($tdvalue2['val'] == "Industry" && isset($tdvalue2['content'])) {
            //                     $experience->industry = $tdvalue2['content'];
            //                 }
            //                 if ($tdvalue2['val'] == "Work Duration_From" && isset($tdvalue2['content'])) {
            //                     $experience->work_duration = $tdvalue2['content'];
            //                 }
            //                 if ($tdvalue2['val'] == "Work Duration_To" && isset($tdvalue2['content'])) {
            //                     $experience->work_duration_to = $tdvalue2['content'];
            //                 }
            //                 if ($tdvalue2['val'] == "I currently work here" && $tdvalue2['content'] != null) {
            //                     /* [ Note : Zoho returning false if candidate is currently working here ] */
            //                     $experience->is_currently_working_here = "true";
            //                 }
            //                 if ($tdvalue2['val'] == "Summary" && isset($tdvalue2['content'])) {
            //                     $experience->summary = $tdvalue2['content'];
            //                 }
            //             }
            //             $experience->created_at = date('Y-m-d h:i:s', time());
            //             $experience->updated_at = date('Y-m-d h:i:s', time());
            //             $experience->save();
            //         }
            //     }
            // }
            Session::flash('success', 'Experience has been saved successfully.');
            return response()->json([
                        'status' => 200
            ]);
            // return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Experience has been not added');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * popup update profiles
     * 
     */
    public function updateProfileExprirence(Request $request) {
        $experience = Experience::where('id', $request->id)->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (count($experience)) {
            $Candidates->is_synced = 0;
            $Candidates->save();
            $experience->occupation = $request->occupation;
            $experience->summary = $request->summary;
            $experience->industry = $request->industry;
            $experience->company = $request->company;
            $experience->work_duration = $request->work_duration_from_month . '-' . $request->work_duration_from_year;
            if ($request->is_curretly_work_here == "true") {
                $experience->is_currently_working_here = "true"; // True it is reverse due to zoho return false if currently working in company
            } else {
                $experience->is_currently_working_here = "false";
                $experience->work_duration_to = $request->work_duration_to_month . '-' . $request->work_duration_to_year;
            }

            $experience->save();

            // $zohoQuery = new ZohoqueryController();

            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;

            // $xmlData = $zohoQuery->createUpdateExperienceXmlData($experience, $request, "update");
            // $response = $zohoQuery->updateCandidateTabularRecord($experience->CANDIDATEID, $xmlData);
            // Session::flash('success', 'Experience has been updated successfully.');
            return response()->json([
                        'status' => 200
            ]);
            // return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Experience data has been synced from zoho. Please try again to update');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * Delete expirence
     */
    public function deleteProfileExprirence(Request $request) {
        $experience = Experience::where('id', $request->id)->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (count($experience)) {
            $Candidates->is_synced = 0;
            $Candidates->save();
            //$zohoQuery = new ZohoqueryController();

            //$territory = "MY";
            //if ($Candidates->territory != null) {
            //    $territory = $Candidates->territory;
            //}
            //$result = Token::where('territory', $territory)->first();
            //$zohoQuery->token = $result->token;
            // Deleting experience From ZOHO
            //$zohoQuery->deleteCandidateTabularRecord($experience->CANDIDATEID, $experience->TABULARROWID, 'Experience Details');
            // Deleting experience From DB
            $experience->delete();

            Session::flash('success', 'Experience has been deleted successfully.');
            // return response()->json([
            //     'status' => 200
            // ]);
            return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Experience data has been synced from zoho. Please try again to delete');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * popup add new profiles eductions
     * 
     */
    public function addProfileEducation(Request $request) {
        $zoho_id = Auth::user()->zoho_id;
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();

        if (count($Candidates)) {
            $Candidates->is_synced = 0;
            $Candidates->save();

            $request->is_currently_pursuing = $request->isCurrentlyPursuing;

            // $zohoQuery = new ZohoqueryController();

            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;

            //$educationList = Education::where(['CANDIDATEID' => Auth::user()->zoho_id])->get();
            // $xmlData = $zohoQuery->createUpdateEducationXmlData($educationList, $request, "add");
            // $response = $zohoQuery->addCandidateTabularRecord(Auth::user()->zoho_id, $xmlData);

            // $tabularData = $zohoQuery->getCandidateTabularRecords(Auth::user()->zoho_id, "Educational Details");

            /* Removing old educations */
            //ducation::where(['CANDIDATEID' => Auth::user()->zoho_id])->delete();
            // if (key_exists(0, $tabularData['response']['result']['Candidates']['FL']['TR'])) {
            //     $data = $tabularData['response']['result']['Candidates']['FL']['TR'];
            // } else {
            //     $data[0] = $tabularData['response']['result']['Candidates']['FL']['TR'];
            // }


            $education = new Education();
            $education->candidate_id = $Candidates->id;
            $education->CANDIDATEID = Auth::user()->zoho_id;
            //$education->TABULARROWID = $tdvalue1['content'];
            $education->currently_pursuing = $request->isCurrentlyPursuing;
            $education->institute = $request->institute;
            $education->department = $request->department;
            $education->degree = $request->degree;
            $education->duration_from = $request->duration_from_month . "-" . $request->duration_from_year;
            $education->duration_to = $request->duration_to_month . "-" . $request->duration_to_year;
            $education->currently_pursuing = $request->isCurrentlyPursuing;
            $education->created_at = date('Y-m-d h:i:s', time());
            $education->updated_at = date('Y-m-d h:i:s', time());
            $education->save();

            // foreach ($data as $trkey2 => $trvalue1) {
            //     if (key_exists('TL', $trvalue1)) {
            //         if (key_exists(0, $trvalue1['TL'])) { // Getting improper data so
            //             foreach ($trvalue1['TL'] as $tdkey1 => $tdvalue1) {
            //                 if ($tdvalue1['val'] == "TABULARROWID" && isset($tdvalue1['content'])) {
            //                     $education = new Education();
            //                     $education->candidate_id = $Candidates->id;
            //                     $education->CANDIDATEID = Auth::user()->zoho_id;
            //                     $education->TABULARROWID = $tdvalue1['content'];
            //                     $education->currently_pursuing = "false";
            //                 }
            //                 if ($tdvalue1['val'] == "Institute / School" && isset($tdvalue1['content'])) {
            //                     $education->institute = $tdvalue1['content'];
            //                 }
            //                 if ($tdvalue1['val'] == "Major / Department" && isset($tdvalue1['content'])) {
            //                     $education->department = $tdvalue1['content'];
            //                 }
            //                 if ($tdvalue1['val'] == "Degree" && isset($tdvalue1['content'])) {
            //                     $education->degree = $tdvalue1['content'];
            //                 }
            //                 if ($tdvalue1['val'] == "Duration_From" && isset($tdvalue1['content'])) {
            //                     $education->duration_from = $tdvalue1['content'];
            //                 }
            //                 if ($tdvalue1['val'] == "Duration_To" && isset($tdvalue1['content'])) {
            //                     $education->duration_to = $tdvalue1['content'];
            //                 }
            //                 if ($tdvalue1['val'] == "Currently pursuing" && isset($tdvalue1['content'])) {
            //                     $education->currently_pursuing = "true";
            //                 }
            //             }
            //             $education->created_at = date('Y-m-d h:i:s', time());
            //             $education->updated_at = date('Y-m-d h:i:s', time());
            //             $education->save();
            //         }
            //     }
            // }

            Session::flash('success', 'Education has been add successfully.');
            return response()->json([
                        'status' => 200
            ]);
            // return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Education has been not added');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * popup edit profiles education
     * 
     */
    public function editProfileEducation(Request $request) {
        $education = Education::where('id', $request->id)->first();
        if (count($education)) {
            $html = view('consultant::consultant_educationform', compact('education'))->render();
            return response()->json([
                        'code' => 200,
                        'html' => $html
            ]);
        } else {
            return response()->json([
                        'code' => 401,
                        'html' => ""
            ]);
        }
        Session::flash('success', 'Education has been updated successfully.');
        return redirect()->route('consultant-profile');
    }

    /**
     * 
     * popup update profiles eductions
     * 
     */
    public function updateProfileEducation(Request $request) {
        $education = Education::where('id', $request->id)->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (count($education)) {

            $Candidates->is_synced = 0;
            $Candidates->save();

            $education->institute = $request->institute;
            $education->department = $request->department;
            $education->degree = $request->degree;
            $education->duration_from = $request->duration_from_month . '-' . $request->duration_from_year;
            if ($request->is_currently_pursuing == "true") {
                $education->currently_pursuing = "true";
            } else {
                $education->currently_pursuing = "false";
                $education->duration_to = $request->duration_to_month . '-' . $request->duration_to_year;
            }
            $education->save();

            // $zohoQuery = new ZohoqueryController();

            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;

            // $xmlData = $zohoQuery->createUpdateEducationXmlData($education, $request, "update");
            // $response = $zohoQuery->updateCandidateTabularRecord($education->CANDIDATEID, $xmlData);

            Session::flash('success', 'Education has been updated successfully.');
            return response()->json([
                        'status' => 200
            ]);
            // return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Education data has been synced from zoho. Please try again to update');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * Delete profiles eductions
     */
    public function deleteProfileEductions(Request $request) {
        $education = Education::where('id', $request->id)->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (count($education)) {
            $Candidates->is_synced = 0;
            $Candidates->save();

            // $zohoQuery = new ZohoqueryController();

            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;

            // Deleting education From ZOHO
            //$zohoQuery->deleteCandidateTabularRecord($education->CANDIDATEID, $education->TABULARROWID, 'Educational Details');
            // Deleting education From DB
            $education->delete();

            Session::flash('success', 'Education has been deleted successfully.');
            // return response()->json([
            //     'status' => 200
            // ]);
            return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Education data has been synced from zoho. Please try again to delete');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * popup add new profiles reference
     * 
     */
    public function addProfileReferences(Request $request) {

        $zoho_id = Auth::user()->zoho_id;
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();

        if (count($Candidates)) {
            $Candidates->is_synced = 0;
            $Candidates->save();
            // $zohoQuery = new ZohoqueryController();

            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;

            // $references = Reference::where(['CANDIDATEID' => Auth::user()->zoho_id])->get();
            // $xmlData = $zohoQuery->createUpdateReferenceXmlData($references, $request, "add");
            // $response = $zohoQuery->addCandidateTabularRecord(Auth::user()->zoho_id, $xmlData);

            // $tabularData = $zohoQuery->getCandidateTabularRecords(Auth::user()->zoho_id, "References");

            // /* Removing old reference */
            // Reference::where(['CANDIDATEID' => Auth::user()->zoho_id])->delete();
            // if (key_exists(0, $tabularData['response']['result']['Candidates']['FL']['TR'])) {
            //     $data = $tabularData['response']['result']['Candidates']['FL']['TR'];
            // } else {
            //     $data[0] = $tabularData['response']['result']['Candidates']['FL']['TR'];
            // }


            $reference = new Reference();
            $reference->candidate_id = $Candidates->id;
            $reference->CANDIDATEID = Auth::user()->zoho_id;
            $reference->name = $request->name;
            $reference->position = $request->position;
            $reference->company = $request->company;
            $reference->phone = $request->phone;
            $reference->email = $request->email;
            $reference->created_at = date('Y-m-d h:i:s', time());
            $reference->updated_at = date('Y-m-d h:i:s', time());
            $reference->save();

            // foreach ($data as $trkey2 => $trvalue4) {
            //     if (key_exists('TL', $trvalue4)) {
            //         if (key_exists(0, $trvalue4['TL'])) { // Getting improper data so
            //             foreach ($trvalue4['TL'] as $tdkey4 => $tdvalue4) {
            //                 if ($tdvalue4['val'] == "TABULARROWID" && isset($tdvalue4['content'])) {
            //                     $reference = new Reference();
            //                     $reference->candidate_id = $Candidates->id;
            //                     $reference->CANDIDATEID = Auth::user()->zoho_id;
            //                     $reference->TABULARROWID = $tdvalue4['content'];
            //                 }
            //                 if ($tdvalue4['val'] == "Reference Name" && isset($tdvalue4['content'])) {
            //                     $reference->name = $tdvalue4['content'];
            //                 }
            //                 if ($tdvalue4['val'] == "Reference Position" && isset($tdvalue4['content'])) {
            //                     $reference->position = $tdvalue4['content'];
            //                 }
            //                 if ($tdvalue4['val'] == "Reference Company" && isset($tdvalue4['content'])) {
            //                     $reference->company = $tdvalue4['content'];
            //                 }
            //                 if ($tdvalue4['val'] == "Reference Phone no." && isset($tdvalue4['content'])) {
            //                     $reference->phone = $tdvalue4['content'];
            //                 }
            //                 if ($tdvalue4['val'] == "Reference Email" && isset($tdvalue4['content'])) {
            //                     $reference->email = $tdvalue4['content'];
            //                 }
            //             }
            //             $reference->created_at = date('Y-m-d h:i:s', time());
            //             $reference->updated_at = date('Y-m-d h:i:s', time());
            //             $reference->save();
            //         }
            //     }
            // }

            Session::flash('success', 'Education has been add successfully.');
            return response()->json([
                        'status' => 200
            ]);
            // return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Education has been not added');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * popup edit profiles References
     * 
     */
    public function editProfileReference(Request $request) {
        $reference = Reference::where('id', $request->id)->first();
        if (count($reference)) {
            $html = view('consultant::consultant_referencesform', compact('reference'))->render();
            return response()->json([
                        'code' => 200,
                        'html' => $html
            ]);
        } else {
            return response()->json([
                        'code' => 401,
                        'html' => ""
            ]);
        }
        Session::flash('success', 'References has been updated successfully.');
        return redirect()->route('consultant-profile');
    }

    /**
     * 
     * popup update profiles reference
     * 
     */
    public function updateProfileReference(Request $request) {
        $reference = Reference::where('id', $request->id)->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (count($reference)) {
            $Candidates->is_synced = 0;
            $Candidates->save();
            $reference->name = $request->name;
            $reference->position = $request->position;
            $reference->company = $request->company;
            $reference->phone = $request->phone;
            $reference->email = $request->email;
            $reference->save();

            // $zohoQuery = new ZohoqueryController();

            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;

            // $xmlData = $zohoQuery->createUpdateReferenceXmlData($reference, $request, "update");
            // $response = $zohoQuery->updateCandidateTabularRecord($reference->CANDIDATEID, $xmlData);

            Session::flash('success', 'References has been updated successfully.');
            return response()->json([
                        'status' => 200
            ]);
            // return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Reference data has been synced from zoho. Please try again to update');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * Delete profiles reference
     */
    public function deleteProfileReference(Request $request) {
        $reference = Reference::where('id', $request->id)->first();
        $Candidates = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if (count($reference)) {
            $Candidates->is_synced = 0;
            $Candidates->save();
            // $zohoQuery = new ZohoqueryController();
            // $territory = "MY";
            // if ($Candidates->territory != null) {
            //     $territory = $Candidates->territory;
            // }
            // $result = Token::where('territory', $territory)->first();
            // $zohoQuery->token = $result->token;
            // // Deleting education From ZOHO
            // $zohoQuery->deleteCandidateTabularRecord($reference->CANDIDATEID, $reference->TABULARROWID, 'References');
            // Deleting education From DB
            $reference->delete();

            Session::flash('success', 'Reference has been deleted successfully.');
            // return response()->json([
            //     'status' => 200
            // ]);
            return redirect()->route('consultant-profile');
        } else {
            Session::flash('error', 'Reference data has been synced from zoho. Please try again to delete');
            return redirect()->route('consultant-profile');
        }
    }

    /**
     * 
     * Applied Job associated 
     * 
     */
    public function consultantAppliedJob(Request $request) {
        $zoho_job_id = $request->zoho_id;
        $id = $request->job_id;
        if ($zoho_job_id != "") {
            $Jobassociatecandidate = Jobassociatecandidates::where('JOBOPENINGID', $zoho_job_id)->where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $Job = Jobopenings::where('JOBOPENINGID', $zoho_job_id)->where('is_delete', '0')->first();
        } else {
            $Jobassociatecandidate = Jobassociatecandidates::where('job_id', $id)->where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $Job = Jobopenings::where('id', $id)->where('is_delete', '0')->first();
        }
        if (count($Jobassociatecandidate)) {
            $Jobassociatecandidates = $Jobassociatecandidate;
            $Jobassociatecandidates->job_save = 0;
        } else {
            $Jobassociatecandidates = new Jobassociatecandidates;
        }
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        // Direct Entry In Zoho
        $dataToSendZoho = [];
        $dataToSendZoho['jobIds'] = $Job->JOBOPENINGID;
        $dataToSendZoho['candidateIds'] = $candidate->CANDIDATEID;
        if ($Jobassociatecandidates->status == "Withdrawn") {
            $status = "Application Withdrawn";
        } else {
            $status = $Jobassociatecandidates->status;
        }
        $dataToSendZoho['status'] = $status;
        $zohoQuery = new ZohoqueryController();

        $territory = "MY";
        if ($Job->territory != null) {
            $territory = $Job->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $zohoQuery->token = $result->token;

        $response = $zohoQuery->addAssociatedCandidateToJob($dataToSendZoho);
        $response = json_decode($response, true);
        if ($candidate != null) {
            $Jobassociatecandidates->job_id = $id;
            $Jobassociatecandidates->candidate_id = $candidate->id;
            $Jobassociatecandidates->JOBOPENINGID = $zoho_job_id;
            $Jobassociatecandidates->CANDIDATEID = $candidate->CANDIDATEID;
            $Jobassociatecandidates->status = 'Associated';
            $Jobassociatecandidates->is_synced = 0;
            $Jobassociatecandidates->save();
            $check = User::where('zoho_id', $Job->CLIENTID)->where('client_applied_job', 1)->first();
            $Job->job_opening_status = "In-progress";
            $Job->save();
            /* Sending notification to client */
            if ($check != null) {
                $client = Clients::where('CLIENTID', $Job->CLIENTID)->first();
                if ($client != null) {
                    $notifications = new Notification;
                    $notifications->s_id = $candidate->id;
                    $notifications->r_id = $client->id;
                    $notifications->sender_id = $candidate->CANDIDATEID;
                    $notifications->receiver_id = $client->CLIENTID;
                    $notifications->message = "You have new application on your job post " . $Job->posting_title . "by " . $candidate->first_name . " " . $candidate->lastname;
                    $notifications->type = "job_applied";
                    $notifications->to = "Client";
                    $notifications->redirect_url = route('job-details', [Crypt::encryptString($Job->id)]);
                    $notifications->save();
                }
            }
            /* Sending notification to Admin */
            if ($candidate->territory == "PH") {
                $setting = Settings::where('settingkey', "ph_email")->first();
            } else {
                $setting = Settings::where('settingkey', "my_email")->first();
            }
            $data['db_sender_id'] = $candidate->id;
            $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
            $data['sender_id'] = $candidate->CANDIDATEID;
            $data['receiver_id'] = "";
            $data['message'] = "You have new application on your job post " . $Job->posting_title . "by " . $candidate->first_name . " " . $candidate->lastname;
            $data['type'] = "job_applied";
            $data['to'] = "Admin";
            $data['territory'] = $candidate->territory;

            $this->sendNotification($data);
            /* Sending email to admin */
            $template = EmailTemplate::where('name', 'candidate_apply_job')->first();
            if ($template != null) {
                $emaildata['candidate_name'] = $candidate->first_name . " " . $candidate->lastname;
                $emaildata['email'] = $setting->settingvalue;
                $emaildata['job_title'] = $Job->posting_title;
                $emaildata['job_link'] = route('job-details', [Crypt::encryptString($Job->id)]);
                $emaildata['profile_link'] = route('consultant-detail', [Crypt::encryptString($candidate->CANDIDATEID)]);
                $emaildata['subject'] = "You have new application on your job post " . $Job->posting_title . "by " . $candidate->first_name . " " . $candidate->lastname;
                Mail::send([], [], function($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                            ->subject($emaildata['subject'])
                            ->setBody($template->parse($emaildata), 'text/html');
                });
            }
            $status = 200;
            $message = "Your application has been submitted successfully.";
        } else {
            $status = 400;
            $message = "Something problem in zoho to apply for this job.";
        }
        return response()->json([
                    'redirect_url' => route('consultant-home', ['tab' => 'appliedjob']),
                    'status' => $status,
                    'message' => $message
        ]);
    }

    /**
     * 
     * save Job associated 
     * 
     */
    public function consultantSavedJob(Request $request) {
        // $decryptedId = Crypt::decryptString($request->id);
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        if ($candidate != null) {
            $Jobassociatecandidates = new Jobassociatecandidates;
            $Jobassociatecandidates->job_id = $request->job_id;
            $Jobassociatecandidates->candidate_id = $candidate->id;
            $Jobassociatecandidates->JOBOPENINGID = $request->zoho_id;
            $Jobassociatecandidates->CANDIDATEID = Auth::user()->zoho_id;
            $Jobassociatecandidates->job_save = 1;
            $Jobassociatecandidates->is_synced = 0;
            $Jobassociatecandidates->save();
            return response()->json([
                        'redirect_url' => route('consultant-home', ['tab' => 'savedjobs'])
            ]);
        } else {
            return 'tak jadi';
        }
        // Session::flash('success', 'successfully applied job :)');
        // return redirect()->back();
    }

    /**
     * 
     * Delete save Job associated 
     * 
     */
    public function consultantDeleteJob(Request $request) {
        // $decryptedId = Crypt::decryptString($request->id);
        $decryptedId = $request->zoho_id;
        $Jobassociatecandidate = Jobassociatecandidates::where('JOBOPENINGID', $decryptedId)->where('CANDIDATEID', Auth::user()->zoho_id)->delete();
        Session::flash('success', 'Job has been successfully unsaved.');
        // return redirect()->back();
    }

    /**
     * Consultant avalibale for contract 
     * 
     */
    public function consultantProfileAvaliable(Request $request) {
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $candidateId = $candidate->id;
        $candidate->available_for_contract = $request->value;
        $candidate->save();
        if ($request->value == 1) {
            $data['Available for Contract'] = 'true';
        } else {
            $data['Available for Contract'] = 'false';
        }
        $territory = "MY";
        if ($candidate->territory != null) {
            $territory = $candidate->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $this->token = $result->token;

        // $client = new Client($this->token);
        //$response = $client->updateRecords('Candidates', $candidate->CANDIDATEID, $data);
        /* Send notification to client if he already added in wishlist */
        $wishList = Clientwishlist::where('candidate_id', $candidateId)->get();
        if (count($wishList)) {
            foreach ($wishList as $key => $value) {
                $client = Clients::where('id', $value->client_id)->first();
                if ($client != null) {
                    $notifications = new Notification;
                    $notifications->s_id = $candidate->id;
                    $notifications->r_id = $client->id;
                    $notifications->sender_id = $candidate->CANDIDATEID;
                    $notifications->receiver_id = $client->CLIENTID;
                    if ($request->value == 1) {
                        $notifications->message = $candidate->first_name . " " . $candidate->lastname . " is available for contract";
                    } else {
                        $notifications->message = $candidate->first_name . " " . $candidate->lastname . " is not available for contract";
                    }
                    $notifications->type = "contract_status";
                    $notifications->to = "Client";
                    $notifications->save();
                }
            }
        }
    }

    /**
     * experince array
     */
    public function experienceList() {
        return [
            'None' => 'None',
            'Fresh' => 'Fresh',
            '1-3 years' => '1-3 years',
            '4-5 years' => '4-5 years',
            '5-10 years' => '5-10 years',
            '10+ years' => '10+ years'
        ];
    }

    /**
     * Job categorry array 
     */
    public function jobCategoryList() {
        return [
            'Basis/S & A' => 'Basis/S & A',
            'Functional' => 'Functional',
            'Technical' => 'Technical',
            'Project Manager/Solution Architect' => 'Project Manager/Solution Architect',
            'Technical' => 'Others'
        ];
    }

    /* Consultant Notifications */

    public function consultantNotifications(Request $request) {
        $unreadNotification = Notification::where('receiver_id', Auth::user()->zoho_id)->where('is_read', 0)->get();
        /* Mark as read */
        if (count($unreadNotification)) {
            foreach ($unreadNotification as $key => $value) {
                $value->is_read = 1;
                $value->save();
            }
        }
        /* Jobs you might Like */
        $candidateDetailarray = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $categoryArray[] = "";
        $categoryArray = explode(';', $candidateDetailarray->category);
        $mightLikesJobIds = Jobassociatecandidates::where('CANDIDATEID', Auth::user()->zoho_id)->pluck('job_id', 'job_id')->toArray();
        $jobsMightLike = Jobopenings::whereNotIn('id', $mightLikesJobIds)->where('territory', Session::get('territory'))->whereIn('category', $categoryArray)->where('job_opening_status', 'In-progress')->take(3)->get();
        return view('consultant::consultant_notifications', compact('jobsMightLike'));
    }

    public function sendNotification($data = []) {
        $notification = new Notification();

        $notification->s_id = $data['db_sender_id'];
        $notification->sender_id = $data['sender_id'];
        if (isset($data['db_sender_id'])) {
            $notification->r_id = $data['db_receiver_id'];
            $notification->receiver_id = $data['receiver_id'];
        }
        if (isset($data['redirect_url'])) {
            $notification->redirect_url = $data['redirect_url'];
        }
        $notification->message = $data['message'];
        $notification->type = $data['type'];
        $notification->to = $data['to'];
        $notification->territory = (isset($data['territory']) ? $data['territory'] : "");
        $notification->created_at = date('Y-m-d H:i:s', time());
        $notification->updated_at = date('Y-m-d H:i:s', time());
        $notification->save();
        return;
    }

    /* Withdraw applied job */

    public function consultantWithdrawJob(Request $request) {
        $zoho_job_id = $request->zoho_id;
        $id = $request->id;
        if ($zoho_job_id != "") {
            $Jobassociatecandidate = Jobassociatecandidates::where('JOBOPENINGID', $zoho_job_id)->where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $Job = Jobopenings::where('JOBOPENINGID', $zoho_job_id)->where('is_delete', '0')->first();
        } else {
            $Jobassociatecandidate = Jobassociatecandidates::where('job_id', $id)->where('CANDIDATEID', Auth::user()->zoho_id)->first();
            $Job = Jobopenings::where('id', $id)->where('is_delete', '0')->first();
        }
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        // Direct Entry In Zoho
        $dataToSendZoho = [];
        $dataToSendZoho['jobIds'] = $Job->JOBOPENINGID;
        $dataToSendZoho['candidateIds'] = $candidate->CANDIDATEID;
        $dataToSendZoho['status'] = "Application Withdrawn";
        $zohoQuery = new ZohoqueryController();
        $territory = "MY";
        if ($Job->territory != null) {
            $territory = $Job->territory;
        }
        $result = Token::where('territory', $territory)->first();
        $zohoQuery->token = $result->token;

        $response = $zohoQuery->withdrawJobApplication($dataToSendZoho);
        $response = json_decode($response, true);
        if ($candidate != null) {
            $Jobassociatecandidate->status = 'Application Withdrawn';
            $Jobassociatecandidate->is_synced = 0;
            $Jobassociatecandidate->save();
            $check = User::where('zoho_id', $Job->CLIENTID)->where('client_applied_job', 1)->first();
            $Job->job_opening_status = "In-progress";
            $Job->save();
            /* Sending notification to client */
            if ($check != null) {
                $client = Clients::where('CLIENTID', $Job->CLIENTID)->first();
                if ($client != null) {
                    $notifications = new Notification;
                    $notifications->s_id = $candidate->id;
                    $notifications->r_id = $client->id;
                    $notifications->sender_id = $candidate->CANDIDATEID;
                    $notifications->receiver_id = $client->CLIENTID;
                    $notifications->message = "Job application has been withdrawn by " . $candidate->first_name . " " . $candidate->lastname . " on your job post of " . $Job->posting_title;
                    $notifications->type = "withdraw_application";
                    $notifications->to = "Client";
                    $notifications->redirect_url = route('job-details', [Crypt::encryptString($Job->id)]);
                    $notifications->save();
                }
            }
            /* Sending notification to Admin */
            if ($candidate->territory == "PH") {
                $setting = Settings::where('settingkey', "ph_email")->first();
            } else {
                $setting = Settings::where('settingkey', "my_email")->first();
            }
            $data['db_sender_id'] = $candidate->id;
            $data['db_receiver_id'] = ""; // Receiver Id Blank for admin because all system user can view the notifcation
            $data['sender_id'] = $candidate->CANDIDATEID;
            $data['receiver_id'] = "";
            $data['message'] = "Job application has been withdrawn by " . $candidate->first_name . " " . $candidate->lastname . " on your job post of " . $Job->posting_title;
            $data['type'] = "withdraw_application";
            $data['to'] = "Admin";
            $data['territory'] = $candidate->territory;
            $this->sendNotification($data);
            /* Sending email to admin */
            $template = EmailTemplate::where('name', 'candidate_withdraw_appication')->first();
            if ($template != null) {
                $emaildata['receiver'] = "Admin";
                $emaildata['candidate_name'] = $candidate->first_name . " " . $candidate->lastname;
                $emaildata['email'] = $setting->settingvalue;
                $emaildata['job_title'] = $Job->posting_title;
                $emaildata['job_link'] = route('job-details', [Crypt::encryptString($Job->id)]);
                $emaildata['subject'] = "Job application has been withdrawn by " . $candidate->first_name . " " . $candidate->lastname . " on your job post of " . $Job->posting_title;
                Mail::send([], [], function($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                            ->subject($emaildata['subject'])
                            ->setBody($template->parse($emaildata), 'text/html');
                });
                // Sending email to client
                $client = Clients::where('CLIENTID', $Job->CLIENTID)->first();
                if ($client != null) {
                    $userClient = User::where('zoho_id', $Job->CLIENTID)->first();
                    if ($userClient != null) {
                        if ($userClient->email != null) {
                            $emaildata['receiver'] = $client->client_name;
                            $emaildata['email'] = $userClient->email;
                            Mail::send([], [], function($messages) use ($template, $emaildata) {
                                $messages->to($emaildata['email'], $emaildata['receiver'])
                                        ->subject($emaildata['subject'])
                                        ->setBody($template->parse($emaildata), 'text/html');
                            });
                        }
                    }
                }
            }
            return response()->json([
                        'status' => 200,
                        'message' => 'Your application has been withdrawn successfully.'
            ]);
        }
        return response()->json([
                    'status' => 400,
                    'message' => 'Something problem in zoho to withdraw application.'
        ]);
    }

    public function getTabularData(Request $request) {
        $status = 200;
        $html = "";
        if ($request->data_type == "experience") {
            $experience = Experience::where('id', $request->id)->first();
            $monthList = $this->getMonths();
            $yearList = $this->getYears();
            if ($request->id != '0' && $experience == null) { // Reload page
                $status = 401;
            } else {
                $html = view('consultant::getexperience', compact('experience', 'monthList', 'yearList'))->render();
            }
        } else if ($request->data_type == "education") {
            $education = Education::where('id', $request->id)->first();
            $monthList = $this->getMonths();
            $yearList = $this->getYears();
            if ($request->id != '0' && $education == null) { // Reload page                
                $status = 401;
            } else {
                $html = view('consultant::geteducation', compact('education', 'monthList', 'yearList'))->render();
            }
        } else {
            $reference = Reference::where('id', $request->id)->first();
            if ($request->id != '0' && $reference == null) { // Reload page                
                $status = 401;
            } else {
                $html = view('consultant::getreference', compact('reference'))->render();
            }
        }
        return response()->json([
                    'html' => $html,
                    'data_type' => $request->data_type,
                    'status' => $status
        ]);
    }

    public function getMonths() {
        return [
            "01" => "Jan",
            "02" => "Feb",
            "03" => "Mar",
            "04" => "Apr",
            "05" => "May",
            "06" => "Jun",
            "07" => "Jul",
            "08" => "Aug",
            "09" => "Sep",
            "10" => "Oct",
            "11" => "Nov",
            "12" => "Dec"
        ];
    }

    public function getYears() {
        $years = [];
        for ($i = 1950; $i <= 2050; $i++) {
            $years[$i] = $i;
        }
        return $years;
    }

    /*
     * Upload consultant profile picture
     * 
     */

    public function getConsultantProfileToEdit(Request $request) {
        $candidate = Candidates::where('CANDIDATEID', Auth::user()->zoho_id)->first();
        $user = User::where('zoho_id', Auth::user()->zoho_id)->first();
        $categories = ['Basis/S & A' => 'Basis/S & A', 'Functional' => 'Functional', 'Technical' => 'Technical', 'Project Manager/Solution Architect' => 'Project Manager/Solution Architect', 'Others' => 'Others'];
        $stateList = $this->stateList();
        $stateRecord = States::where('state_name', $candidate->state)->pluck('id')->first();
        if (empty($stateRecord)) {
            $stateRecord == 1;
        }
        $city = Cities::where('state_id', $stateRecord)->pluck('city_name', 'city_name')->toArray();
        $html = view('consultant::getprofile', compact('candidate', 'categories', 'user', 'stateList', 'stateRecord', 'city'))->render();
        return response()->json([
                    'html' => $html
        ]);
    }

    /*
     * Remove profile picture
     * 
     */

    public function removeProfilePicture(Request $request) {
        $user = User::where('zoho_id', Auth::user()->zoho_id)->first();
        if ($user != null) {
            if (file_exists(public_path('profile_image/' . $user->profile_pic))) {
                unlink(public_path('profile_image/' . $user->profile_pic));
            }
            $user->profile_pic = "";
            $path = public_path('profile_image/zoho_default.png');
            $this->uploadProfilePicInZoho($user->zoho_id, $path);
            $user->save();
            return response()->json([
                        'status' => 200,
                        'message' => 'Profile image has been deleted successfully'
            ]);
        } else {
            return response()->json([
                        'status' => 401,
                        'message' => 'Profile image can not delete. Please try again'
            ]);
        }
    }

    /**
     * Job Enquiry
     * 
     */
    public function jobEnqury(Request $request) {
        if (Auth::user()->territory == "PH") {
            $receiverEmail = Settings::where('settingkey', 'ph_email')->first()->settingvalue;
        } else {
            $receiverEmail = Settings::where('settingkey', 'my_email')->first()->settingvalue;
        }
        $template = EmailTemplate::where('name', 'job_enquiry')->first();
        if ($template != null) {
            if ($receiverEmail != "") {
                $emaildata['email'] = $receiverEmail;
                $emaildata['candidate_name'] = Auth::user()->first_name . ' ' . Auth::user()->last_name;
                $emaildata['content'] = $request->content;
                $emaildata['job_link'] = $request->job_link;
                Mail::send([], [], function($messages) use ($template, $emaildata) {
                    $messages->to($emaildata['email'], "Admin")
                            ->subject($template->subject . '-' . $emaildata['candidate_name'])
                            ->setBody($template->parse($emaildata), 'text/html');
                });
                return response()->json([
                            'status' => 200,
                            'message' => 'your question has been sent, we appreciate your patience in the mean time'
                ]);
//                Session::flash('success', 'your question has been sent, we appreciate your patience in the mean time');
//                return redirect()->back();
            } else {
                return response()->json([
                            'status' => 200,
                            'message' => 'Your enquiry could not send. Please try after some time'
                ]);
//                Session::flash('error', 'Your enquiry could not send. Please try after some time');
//                return redirect()->back();
            }
        } else {
            return response()->json([
                        'status' => 200,
                        'message' => 'Your enquiry could not send. Please try after some time'
            ]);
//            Session::flash('error', 'Your enquiry could not send. Please try after some time');
//            return redirect()->back();
        }
    }

}
