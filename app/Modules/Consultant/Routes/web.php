<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your module. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
Route::middleware(['DefaultLocale'])->prefix('{locale}')->group(function () {
    /* Consultant Routes */
    Route::group(['prefix' => 'consultant', 'middleware' => ['auth','CheckAccountDeleted']], function () {
        // Home
        Route::get('/home', 'CousultantController@consultantHome')->name('consultant-home');
        // Profile
        Route::get('/profile', 'CousultantController@consultantProfile')->name('consultant-profile');
        //Consultant avalibale for contract
        Route::any('/profileavaliable', 'CousultantController@consultantProfileAvaliable')->name('consultantprofileavaliable');
        // Interviews
        Route::get('/interviews', 'CousultantController@consultantInterviews')->name('consultant-interviews');
        // Contract History
        Route::get('/contract-history', 'CousultantController@consultantContractHistory')->name('consultant-contract-history');
        // Attachments
        Route::get('/attachements', 'CousultantController@consultantAttachments')->name('consultant-attachments');
        // Attachments
        Route::any('upload/resume', 'CousultantController@ResumeAttachment')->name('ResumeAttachment');
        // upload Attachment 
        Route::any('upload/attachment', 'CousultantController@uploadAttachment')->name('uploadAttachment');
        //consultant Frontend Search  
        Route::any('/search', 'CousultantController@consultantSearch')->name('consultantsearch');
        //consultant view job Search  
        Route::any('/job-details/{id}', 'CousultantController@viewJobDetail')->name('consultantjobdetails');
        //consultant Edit profile pop-up  
        Route::any('/edit-profile', 'CousultantController@editProfile')->name('consultanteditprofile');
        //consultant Edit profile pop-up  
        Route::any('/edit-profilesummary', 'CousultantController@editProfileSummary')->name('consultanteditprofilesummary');
        //consultant exprirence edit ajax
        Route::any('/add-profileexprirence', 'CousultantController@addProfileExprirence')->name('addProfileExprirence');
        Route::any('/edit-profileexprirence', 'CousultantController@editProfileExprirence')->name('consultanteditProfileExprirence');
        //consultant exprirence edit ajax update
        Route::any('/update-profileexprirence', 'CousultantController@updateProfileExprirence')->name('consultantupdateProfileExprirence');
        //consultant exprirence delete 
        Route::any('/delete-profileexprirence/{id}', 'CousultantController@deleteProfileExprirence')->name('consultantdeleteProfileExprirence');

        //consultant Education edit ajax
        Route::any('/edit-profileeducation', 'CousultantController@editProfileEducation')->name('consultanteditProfileEducation');
        //consultant add Education 
        Route::any('/add-profileeducation', 'CousultantController@addProfileEducation')->name('addProfileEducation');
        //consultant Education delete 
        Route::any('/delete-profileeducation/{id}', 'CousultantController@deleteProfileEductions')->name('deleteProfileEducation');
        //consultant Education edit upadte form
        Route::any('/update-profileeducation', 'CousultantController@updateProfileEducation')->name('consultantupdateProfileEducation');
        Route::any('/update-profile', 'CousultantController@updateProfile')->name('consultantupdateprofile');
        //consultant profile publish now
        Route::any('/edit-update', 'CousultantController@updatePublish')->name('consultantupdatepublish');
        //consultant profile unpublish now
        Route::any('/edit-unpublish', 'CousultantController@updateUnpublish')->name('consultantupdateunpublish');

        //consultant References edit ajax
        Route::any('/edit-profilereferences', 'CousultantController@editProfileReference')->name('consultanteditProfileReference');
        //consultant add References 
        Route::any('/add-profilereferences', 'CousultantController@addProfileReferences')->name('addProfileReferences');
        //consultant References edit update form 
        Route::any('/update-profilereferences', 'CousultantController@updateProfileReference')->name('consultantupdateProfileReference');
        //consultant References delete 
        Route::any('/delete-profilereferences/{id}', 'CousultantController@deleteProfileReference')->name('consultantdeleteProfileReference');

        // Consultant Settings
        Route::get('/settings', 'CousultantController@consultantSettings')->name('consultantsettings');
        //Consultant Applied job 
        Route::any('/appliedjob', 'CousultantController@consultantAppliedJob')->name('consultantAppliedJob');
        //Consultant Applied job remove
        Route::any('/appliedjobdelete/{id}', 'CousultantController@consultantAppliedJobDelete')->name('consultantAppliedJobdelete');
        //Consultant Job saved
        Route::any('/savejob', 'CousultantController@consultantSavedJob')->name('consultantsavejob');
        //Consultant Job Delete 
        Route::any('/deletejob', 'CousultantController@consultantDeleteJob')->name('consultantdeletejob');
        // Consultant Notifications
        Route::get('/notifications', 'CousultantController@consultantNotifications')->name('consultant-notifications');
        //Consultant withdraw job
        Route::any('/withdrawjob', 'CousultantController@consultantWithdrawJob')->name('consultantwithdrawjob');

        //Consultant base rate change 
        Route::any('/profilechange', 'CousultantController@consultantProfileChange')->name('consultantProfileChange');
        // Download Attachment
        Route::get('/download-attachment/{id}/{type}', 'CousultantController@downloadAttachment')->name('downloadattachment');
        // delete Attachment
        Route::any('/delete-attachment/{id}/{type}', 'CousultantController@deleteAttachment')->name('deleteattachment');
        // Getting tabular data
        Route::post('/gettabulardata', 'CousultantController@getTabularData')->name('gettabulardata');
        // Getting consultant profile to edit
        Route::post('/getconsultantprofile', 'CousultantController@getConsultantProfileToEdit')->name('getconsultantprofile');
        // Removing profile pic
        Route::post('/removeprofilepic', 'CousultantController@removeProfilePicture')->name('remove-profile-pic');
        // Job Enquiry
        Route::post('/job-enquiry', 'CousultantController@jobEnqury')->name('job-enquiry');
    });
});