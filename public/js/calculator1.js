/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*  Validations on Input*/
$("#c1-base-rate-perday").on("keypress keyup blur", function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
$('#c1-base-rate-perday').bind('copy paste cut', function (e) {
    e.preventDefault(); //disable cut,copy,paste
});
/* Calculator C1 */
$("#c1-base-rate-perday").keyup(function () {
    var base_rate = parseFloat($(this).val());
    var dailyRate = 0;
    if (!isNaN(base_rate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            dailyRate = zeroToFiveBillingRate(base_rate);
        } else if (duration == 2) { // 6-11 Months
            dailyRate = sixToElevenBillingRate(base_rate);
        } else { // > 12 Months
            dailyRate = greaterThenTwelveBillingRate(base_rate);
        }
        $("#daily-rate").html("MYR " + parseFloat(dailyRate).toFixed(2));
    } else { // Setting base rate 0 if base_rate not valid
        $("#daily-rate").html("MYR " + parseFloat(0).toFixed(2));
    }
});
$(".months").change(function () {
    var base_rate = parseFloat($("#c1-base-rate-perday").val());
    var dailyRate = 0;
    if (!isNaN(base_rate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            dailyRate = zeroToFiveBillingRate(base_rate);
        } else if (duration == 2) { // 6-11 Months
            dailyRate = sixToElevenBillingRate(base_rate);
        } else { // > 12 Months
            dailyRate = greaterThenTwelveBillingRate(base_rate);
        }
        $("#daily-rate").html("MYR " + parseFloat(dailyRate).toFixed(2));
    } else { // Setting base rate 0 if base_rate not valid
        $("#daily-rate").html("MYR " + parseFloat(0).toFixed(2));
    }
});
$(".fees").change(function () {
    var base_rate = parseFloat($("#c1-base-rate-perday").val());
    var dailyRate = 0;
    if (!isNaN(base_rate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            dailyRate = zeroToFiveBillingRate(base_rate);
        } else if (duration == 2) { // 6-11 Months
            dailyRate = sixToElevenBillingRate(base_rate);
        } else { // > 12 Months
            dailyRate = greaterThenTwelveBillingRate(base_rate);
        }
        $("#daily-rate").html("MYR " + parseFloat(dailyRate).toFixed(2));
    } else { // Setting base rate 0 if base_rate not valid
        $("#daily-rate").html("MYR " + parseFloat(0).toFixed(2));
    }
});
/* Daily Rate For 0-5 Months*/
function zeroToFiveBillingRate(base_rate) {
    /* Adding 5% Extra In Base Rate */
    var fivePercent = (base_rate * 5) / 100;
    base_rate += parseFloat(fivePercent);
    /* Show 95% out of 100% */
    if($("#guaranteed_payment").is(":checked")){
        var daily_rate = ((base_rate * 90) / 100);
    }else{
        var daily_rate = ((base_rate * 97) / 100);
    }
    return daily_rate;
}
/* Daily Rate For 6-11 Months*/
function sixToElevenBillingRate(base_rate) {
    /* Show 95% out of 100% */
    if($("#guaranteed_payment").is(":checked")){
        var daily_rate = ((base_rate * 90) / 100);
    }else{
        var daily_rate = ((base_rate * 97) / 100);
    }
    return daily_rate;
}
/* Daily Rate For > 12 Months */
function greaterThenTwelveBillingRate(base_rate) {
    /* Minus 3% From Base Rate */
    var threePercent = (base_rate * 3) / 100;
    base_rate -= parseFloat(threePercent);
    /* Show 95% out of 100% */
    if($("#guaranteed_payment").is(":checked")){
        var billing_rate = ((base_rate * 90) / 100);
    }else{
        var billing_rate = ((base_rate * 97) / 100);
    }
    return billing_rate;
}