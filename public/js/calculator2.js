/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*  Validations on Input*/
$("#c2-daily-rate").on("keypress keyup blur", function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
$('#c2-daily-rate').bind('copy paste cut', function (e) {
    e.preventDefault(); //disable cut,copy,paste
});
/* Calculator C1 */
$("#c2-daily-rate").keyup(function () {
    var dailyRate = parseFloat($(this).val());
    var data;
    if (!isNaN(dailyRate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            data = c2_zeroToFiveBillingRate(dailyRate);
        } else if (duration == 2) { // 6-11 Months
            data = c2_sixToElevenBillingRate(dailyRate);
        } else { // > 12 Months
            data = c2_greaterThenTwelveBillingRate(dailyRate);
        }
        $("#c2-actual-rate").html("MYR " + parseFloat(data.actualBaseRate).toFixed(2));
        $("#c2-base-rate").html("MYR " + parseFloat(data.baserate).toFixed(2));
    } else { // Setting base rate 0 if daily rate not valid
        $("#c2-actual-rate").html("MYR " + parseFloat(0).toFixed(2));
        $("#c2-base-rate").html("MYR " + parseFloat(0).toFixed(2));
    }
});
$(".c2_months").change(function () {
    var dailyRate = parseFloat($("#c2-daily-rate").val());
    var data;
    if (!isNaN(dailyRate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            data = c2_zeroToFiveBillingRate(dailyRate);
        } else if (duration == 2) { // 6-11 Months
            data = c2_sixToElevenBillingRate(dailyRate);
        } else { // > 12 Months
            data = c2_greaterThenTwelveBillingRate(dailyRate);
        }
        $("#c2-actual-rate").html("MYR " + parseFloat(data.actualBaseRate).toFixed(2));
        $("#c2-base-rate").html("MYR " + parseFloat(data.baserate).toFixed(2));
    } else { // Setting base rate 0 if daily rate not valid
        $("#c2-actual-rate").html("MYR " + parseFloat(0).toFixed(2));
        $("#c2-base-rate").html("MYR " + parseFloat(0).toFixed(2));
    }
});
$("#c2_guaranteed_payment").change(function () {
    var dailyRate = parseFloat($("#c2-daily-rate").val());
    var data;
    if (!isNaN(dailyRate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            data = c2_zeroToFiveBillingRate(dailyRate);
        } else if (duration == 2) { // 6-11 Months
            data = c2_sixToElevenBillingRate(dailyRate);
        } else { // > 12 Months
            data = c2_greaterThenTwelveBillingRate(dailyRate);
        }
        $("#c2-actual-rate").html("MYR " + parseFloat(data.actualBaseRate).toFixed(2));
        $("#c2-base-rate").html("MYR " + parseFloat(data.baserate).toFixed(2));
    } else { // Setting base rate 0 if daily rate not valid
        $("#c2-actual-rate").html("MYR " + parseFloat(0).toFixed(2));
        $("#c2-base-rate").html("MYR " + parseFloat(0).toFixed(2));
    }
});
/* Daily Rate For 0-5 Months*/
function c2_zeroToFiveBillingRate(dailyRate) {
    /* Adding 5% Extra In New Daily Rate */
    var fivePercent = (parseFloat(dailyRate) * 5) / 100;
    var actualBaseRate =  parseFloat(dailyRate) + fivePercent;
    var baseRate = calculateFees(actualBaseRate);
    var data = {};
    data.baserate = baseRate;
    data.actualBaseRate = actualBaseRate;
    return data;
}
/* Daily Rate For 6-11 Months*/
function c2_sixToElevenBillingRate(dailyRate) {
    var actualBaseRate = parseFloat(dailyRate);
    var baseRate = calculateFees(actualBaseRate);
    var data = {};
    data.baserate = baseRate;
    data.actualBaseRate = actualBaseRate;
    return data;
}
/* Daily Rate For > 12 Months */
function c2_greaterThenTwelveBillingRate(dailyRate) {
    /* Minus 3% From New Daily Rate */
    var threePercent = (parseFloat(dailyRate) * 3) / 100;
    var actualBaseRate =  parseFloat(dailyRate) - threePercent;
    var baseRate = calculateFees(actualBaseRate);
    var data = {};
    data.baserate = baseRate;
    data.actualBaseRate = actualBaseRate;
    return data;
}
/* Calculate Fees */
function calculateFees(dailyRate) {
    var baserate = parseFloat(0);
    if ($("#c2_guaranteed_payment").is(":checked")) {
        baserate = parseFloat(dailyRate) / 0.90;
    } else {
        baserate = parseFloat(dailyRate) / 0.93;
    }
    return baserate;
}