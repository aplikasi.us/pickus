var $grid = $(".grid").isotope({
  itemSelector: ".single-job",
  layoutMode: "fitRows"
});

var filterKeywords = [];

$("input.checkbox").change(function() {
  var keyword = $(this).val();

  // If keyword available in array
  if (filterKeywords.includes(keyword)) {
    filterKeywords = filterKeywords.filter(function(key) {
      return key !== keyword;
    });
  } else {
    filterKeywords.push(keyword);
  }

  filterResult(filterKeywords);
});

// change is-checked class on buttons
$(".checkbox").each(function(i, checkbox) {
  var checkboxValue = $(checkbox).val();
  if (filterKeywords.includes(checkboxValue)) {
    $(checkbox).prop("checked", true);
  } else {
    $(checkbox).prop("checked", false);
  }
});

// Filter result using isotope
function filterResult(collectionOfKeyword) {
  collectionOfKeyword = collectionOfKeyword.map(function(keyword) {
    return ".data-" + keyword;
  });
  $grid.isotope({ filter: collectionOfKeyword.join() });
}

// Pagination
// jQuery Plugin: http://flaviusmatis.github.io/simplePagination.js/

var items = $(".single-job");
var numItems = items.length;
var perPage = 9;

items.slice(perPage).hide();

$("#pagination-container").pagination({
  items: numItems,
  itemsOnPage: perPage,
  prevText: "&laquo;",
  nextText: "&raquo;",
  onPageClick: function(pageNumber) {
    var showFrom = perPage * (pageNumber - 1);
    var showTo = showFrom + perPage;

    items
      .hide()
      .slice(showFrom, showTo)
      .show();
  }
});
