$(document).ready(function(){
	//responsive mobile menu
  	if ($(window).width() <= 991) {
	    $('.menuicon').on('click', function(){
	      $('.navigation-section').addClass('openmenu');
	      $('.header-top').addClass('header-top-open');
	    });
	    $('.close-menu').on('click', function(){
	      	$('.navigation-section').removeClass('openmenu');
			$('.nav-submenu').removeClass('open');
			$('.nav-sub-submenu').removeClass('open');
			$('.header-top').removeClass('header-top-open');
	    });
	    $('.usericon').on('click', function(){
	      $('.navigation-user-section').addClass('openmenu');
	      $('.header-top').addClass('header-top-open');
	    });
	    $('.close-menu').on('click', function(){
	      	$('.navigation-user-section').removeClass('openmenu');
			$('.nav-submenu').removeClass('open');
			$('.nav-sub-submenu').removeClass('open');
			$('.header-top').removeClass('header-top-open');
	    });
	    $('.nav-link-submenu').on('click',function(){
	    	var submenu = $(this).next('.nav-submenu');
	    	if (submenu.hasClass('open')) {
	    		submenu.removeClass('open');
	    	} else {
	    		$('.nav-submenu').removeClass('open');
	    		submenu.addClass('open');
	    	}
	    });
	    $('.nav-link-subsubmenu').on('click',function(){
	    	var subsubmenu = $(this).next('.nav-sub-submenu');
	    	if(subsubmenu.hasClass('open')){
	    		subsubmenu.removeClass('open');
	    	}
	    	else{
	    		$('.nav-sub-submenu').removeClass('open');
	    		subsubmenu.addClass('open');
	    	}
	    });
	}
	//Initialize Select2 Elements
	$('.select2').select2();
	 //using document ready...
    //initialising fullpage.js in the jQuery way
//    $('#fullpage').fullpage({
//        sectionSelector: 'section',
//        navigation: true,
//        slidesNavigation: true,
//    });
//
//    // calling fullpage.js methods using jQuery
//    $('#moveSectionUp').click(function(e){
//        e.preventDefault();
//        $.fn.fullpage.moveSectionUp();
//    });
//
//    $('#moveSectionDown').click(function(e){
//        e.preventDefault();
//        $.fn.fullpage.moveSectionDown();
//    });
});