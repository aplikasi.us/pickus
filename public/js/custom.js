/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Example starter JavaScript for disabling form submissions if there are invalid fields
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
});
$(document).ready(function() {
    $(document)
        .find(".alert-dismissable")
        .delay(5000)
        .fadeOut("slow");
    // Consultant Sign Up
    $("#consultant-registration")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                company_name: {
                    validators: {
                        notEmpty: {
                            message: "Company name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Company name max length is 50 character"
                        }
                    }
                },
                first_name: {
                    validators: {
                        notEmpty: {
                            message: "First name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "First name max length is 50 character"
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: "Last name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Last name max length is 50 character"
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: "Email address is required"
                        },
                        emailAddress: {
                            message: "Please enter valid email address"
                        },
                        remote: {
                            data: {
                                _token: $("input[name=_token]").val(),
                                action: "add",
                                userid: "0"
                            },
                            url: emailExists,
                            type: "POST",
                            message: 'This email is already exists, <a href="javascript:;" title="Login" class="login-form-poup">login?</a>'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: "Phone number is required"
                        },
                        regexp: {
                            regexp: /^[0-9-+()]*$/,
                            message: "Only number required"
                        },
                        stringLength: {
                            min: 10,
                            max: 15,
                            message: "Phone number must be minimum 10 digits and maximum 15 digits"
                        }
                    }
                },
                cv: {
                    validators: {
                        notEmpty: {
                            message: "CV is required"
                        },
                        file: {
                            extension: "doc,docx,pdf",
                            type: "application/msword,application/pdf",
                            maxSize: 2048 * 1024,
                            message: "Please select only doc or pdf and select file size below 2 mb"
                        }
                    }
                },
                sap_job_title: {
                    validators: {
                        notEmpty: {
                            message: "Sap job title is required"
                        }
                    }
                },
                job_title: {
                    validators: {
                        notEmpty: {
                            message: "Job title is required"
                        }
                    }
                },
                t_n_c: {
                    validators: {
                        notEmpty: {
                            message: "You must agree before submitting"
                        }
                    }
                },
                hear: {
                    validators: {
                        notEmpty: {
                            message: "Please select reference"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    // Update Profile
    $("#update-profile")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                first_name: {
                    validators: {
                        notEmpty: {
                            message: "First name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "First name max length is 50 character"
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: "Last name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Last name max length is 50 character"
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: "Email address is required"
                        },
                        emailAddress: {
                            message: "Please enter valid email address"
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: "Phone number is required"
                        },
                        integer: {
                            message: "Please enter valid phone number"
                        },
                        stringLength: {
                            max: 10,
                            message: "Phone number must be 10 digits"
                        }
                    }
                },
                cv: {
                    validators: {
                        notEmpty: {
                            message: "CV is required"
                        },
                        file: {
                            extension: "doc,docx,pdf",
                            type: "application/msword,application/pdf",
                            maxSize: 2048 * 1024,
                            message: "Please select only doc or pdf"
                        }
                    }
                },
                t_n_c: {
                    validators: {
                        notEmpty: {
                            message: "You must agree before submitting"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    //Start conact us form

    $("#contact-us-form")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                first_name: {
                    validators: {
                        notEmpty: {
                            message: "First name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "First name max length is 50 character"
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: "Last name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Last name max length is 50 character"
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: "Email address is required"
                        },
                        emailAddress: {
                            message: "Please enter valid email address"
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: "Phone number is required"
                        },
                        regexp: {
                            regexp: /^[0-9-+()]*$/,
                            message: "Only number required"
                        },
                        stringLength: {
                            min: 10,
                            max: 15,
                            message: "Phone number must be minimum 10 digits and maximum 15 digits"
                        }
                    }
                },
                type: {
                    validators: {
                        notEmpty: {
                            message: "Please select Type"
                        }
                    }
                },
                subject: {
                    validators: {
                        notEmpty: {
                            message: "Subject is required"
                        }
                    }
                },
                message: {
                    validators: {
                        notEmpty: {
                            message: "Message is required"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    //end contact us form
    // Generate Password
    $("#consultant-generate-password")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Password minimum length is six character"
                        },
                        identical: {
                            field: "confirm_password",
                            message: "Password and confirm password must be same"
                        }
                    }
                },
                confirm_password: {
                    validators: {
                        notEmpty: {
                            message: "Confirm password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Confirm password minimum length is six character"
                        },
                        identical: {
                            field: "password",
                            message: "Password and confirm password must be same"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        })
        .on("success.form.bv", function(e) {
            $("#generate-password-loading").show();
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data("bootstrapValidator");

            // Use Ajax to submit form data
            $.post(
                $form.attr("action"),
                $form.serialize(),
                function(result) {
                    if (bv.getSubmitButton()) {
                        bv.disableSubmitButtons(false);
                    }
                    $("#generate-password-loading").hide();
                    $("#generate-password-message").empty();
                    $("#login-error-message").append(
                        "<div class='alert alert-success alert-dismissable'>" +
                        result.message +
                        "</div>"
                    );
                    $(".generate-password-form-block").hide();
                    $(".login-form-poup").click();
                    $("#login-error-message")
                        .find(".alert-dismissable")
                        .delay(5000)
                        .fadeOut("slow");
                },
                "json"
            );
        });
    // Reset Password Form
    $("#reset-password")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Password minimum length is six character"
                        },
                        identical: {
                            field: "confirm_password",
                            message: "Password and confirm password must be same"
                        }
                    }
                },
                confirm_password: {
                    validators: {
                        notEmpty: {
                            message: "Confirm password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Confirm password minimum length is six character"
                        },
                        identical: {
                            field: "password",
                            message: "Password and confirm password must be same"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    ///rest email id
    $("#forgotpassword")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: "Email address is required"
                        },
                        emailAddress: {
                            message: "Please enter valid email address"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        })
        .on("success.form.bv", function(e) {
            $("#forgot-loading").show();
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data("bootstrapValidator");

            // Use Ajax to submit form data
            $.post(
                $form.attr("action"),
                $form.serialize(),
                function(result) {
                    if (bv.getSubmitButton()) {
                        bv.disableSubmitButtons(false);
                    }
                    $("#forgot-loading").hide();
                    $("#forgot-message").empty();
                    if (result.status == 200) {
                        $("#forgot-message").append(
                            "<div class='alert alert-success alert-dismissable'>" +
                            result.message +
                            "</div>"
                        );
                    } else {
                        $("#forgot-message").append(
                            "<div class='alert alert-danger alert-dismissable'>" +
                            result.message +
                            "</div>"
                        );
                    }
                    $("#forgot-message")
                        .find(".alert-dismissable")
                        .delay(5000)
                        .fadeOut("slow");
                },
                "json"
            );
        });
    //Change password
    $("#change-password")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                old_password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Password minimum length is six character"
                        }
                    }
                },
                new_password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Password minimum length is six character"
                        },
                        identical: {
                            field: "confirm_password",
                            message: "Password and confirm password must be same"
                        }
                    }
                },
                confirm_password: {
                    validators: {
                        notEmpty: {
                            message: "Confirm password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Confirm password minimum length is six character"
                        },
                        identical: {
                            field: "new_password",
                            message: "Password and confirm password must be same"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    //Delete account
    $("#delete-account-form")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Password minimum length is six character"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    //Job Enquiry
    $("#enquiry-form")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                content: {
                    validators: {
                        notEmpty: {
                            message: "Please enter content"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        })
        .on("success.form.bv", function(e) {
            $("#enquiry-img-loader").show();
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data("bootstrapValidator");

            // Use Ajax to submit form data
            $.post(
                $form.attr("action"),
                $form.serialize(),
                function(result) {
                    $("#enquiry-img-loader").hide();
                    if (result.status == 200) {
                        toastr.success(result.message);
                    } else {
                        toastr.error(result.message);
                    }
                },
                "json"
            );
        });
    $("#delete-account").on("hidden.bs.modal", function() {
        $("#delete-account-form").bootstrapValidator("resetForm", true);
    });
    // Setting notification
    //    $("#notification-settings")
    //            .bootstrapValidator({
    //                framework: "bootstrap",
    //                excluded: ":disabled",
    //                fields: {
    //                    'job_alerts_tags[]': {
    //                        validators: {
    //                            notEmpty: {
    //                                message: "You have to select at least one!"
    //                            }
    //                        }
    //                    },
    //                }
    //            })
    //            .on("error.validator.bv", function (e, data) {
    //                data.element
    //                        .data("bv.messages")
    //                        // Hide all the messages
    //                        .find('.help-block[data-bv-for="' + data.field + '"]')
    //                        .hide()
    //                        // Show only message associated with current validator
    //                        .filter('[data-bv-validator="' + data.validator + '"]')
    //                        .show();
    //            });
    $("#notification-settings").on("hidden.bs.modal", function() {
        $("#notification-settings").bootstrapValidator("resetForm", true);
    });

    // Consultant Login

    $("#consultant-login")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: "Please enter email address"
                        },
                        emailAddress: {
                            message: "Please enter valid email address"
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: "Please enter password"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        })
        .on("success.form.bv", function(e) {
            $("#login-loading").show();
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data("bootstrapValidator");

            // Use Ajax to submit form data
            $.post(
                $form.attr("action"),
                $form.serialize(),
                function(result) {
                    $("#login-loading").hide();
                    if (result.status == 200) {
                        location.href = result.redirecturl;
                    } else {
                        if (bv.getSubmitButton()) {
                            bv.disableSubmitButtons(false);
                        }
                        $("#login-error-message").empty();
                        $("#login-error-message").append(
                            "<div class='alert alert-danger alert-dismissable'>" +
                            result.message +
                            "</div>"
                        );
                        $("#login-error-message")
                            .find(".alert-dismissable")
                            .delay(5000)
                            .fadeOut("slow");
                    }
                },
                "json"
            );
        });
    ///Candidate update profile

    $("#candidateprofile")
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                first_name: {
                    validators: {
                        notEmpty: {
                            message: "First name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "First name max length is 50 character"
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: "Last name is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Last name max length is 50 character"
                        }
                    }
                },
                sap_job_title: {
                    validators: {
                        notEmpty: {
                            message: "Sap job title is required"
                        },
                        stringLength: {
                            max: 50,
                            message: "Sap job title max length is 50 character"
                        }
                    }
                },
                experience_in_years: {
                    validators: {
                        notEmpty: {
                            message: "Experience is required"
                        },
                        integer: {
                            message: "Please enter valid numeric value"
                        },
                        stringLength: {
                            max: 2,
                            message: "Experience max length is 2 character"
                        }
                    }
                },
                mobile_number: {
                    validators: {
                        notEmpty: {
                            message: "Phone number is required"
                        },
                        integer: {
                            message: "Please enter valid phone number"
                        },
                        stringLength: {
                            max: 11,
                            message: "First name max length is 11 character"
                        }
                    }
                },
                state: {
                    validators: {
                        notEmpty: {
                            message: "State is required"
                        },
                        stringLength: {
                            max: 30,
                            message: "State max length is 30 character"
                        },
                        regexp: {
                            regexp: /^[a-z\s]+$/i,
                            message: "State consist of alphabetical characters and spaces only"
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: "Country is required"
                        },
                        stringLength: {
                            max: 30,
                            message: "Country max length is 30 character"
                        },
                        regexp: {
                            regexp: /^[a-z\s]+$/i,
                            message: "Country consist of alphabetical characters and spaces only"
                        }
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: "City is required"
                        },
                        stringLength: {
                            max: 30,
                            message: "City max length is 30 character"
                        },
                        regexp: {
                            regexp: /^[a-z\s]+$/i,
                            message: "City consist of alphabetical characters and spaces only"
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    /*-Upload attachment --*/
    $("#customResume").change(function(e) {
        $this = this;
        myfile = $(this).val();
        var ext = myfile.split(".").pop();
        if (ext == "pdf" || ext == "docx" || ext == "doc") {} else {
            bootbox.alert(
                "New document upload will support file format .doc, .docx and .pdf only"
            );
            return false;
        }
        var form_data = new FormData();
        // console.log(form_data);
        if ($(this).prop("files").length > 0) {
            file = $(this).prop("files")[0];
            form_data.append("customResume", file);
        }
        $(".loadertest").addClass("active");
        $.ajax({
            type: "POST",
            url: uploadattachment, // point to server-side PHP script
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(response) {
                // alert(response);
                $(".loadertest").removeClass("active");
                $(".cv-consult-sucess").addClass("active");
            }
        });
    });
    $(".close-loginform").on("click", function() {
        $("#modalLoginForm").modal("hide");
    });

    $("#customattachment").change(function(e) {
        //        $this = this;
        //        var form_data = new FormData();
        //        if ($(this).prop('files').length > 0) {
        //            file = $(this).prop('files')[0];
        //            form_data.append("customattachment", file);
        //        }
        //        $('.loadertest').addClass('active');
        //        $('.btnUploadFile').addClass('disable');
        //        $.ajax({
        //            type: 'POST',
        //            url: uploadattachment, // point to server-side PHP script
        //            cache: false,
        //            contentType: false,
        //            processData: false,
        //            data: form_data,
        //            success: function (response) {
        //                console.log(response);
        ////                location.reload();
        //            }
        //        });
        $("#customattachment-form").submit();
    });

    function validateEditor() {
        // Revalidate the content when its value is changed by Summernote
        $("#client-job-post").bootstrapValidator("revalidateField", "benefits");
    }
    // Client Job Post
    $("#client-job-post")
        .find('[name="key_skills"]')
        // Revalidate the cities field when it is changed
        .change(function(e) {
            $("#client-job-post").bootstrapValidator("revalidateField", "key_skills");
        })
        .end()
        .bootstrapValidator({
            framework: "bootstrap",
            excluded: ":disabled",
            fields: {
                job_title: {
                    validators: {
                        notEmpty: {
                            message: "Job title is required"
                        }
                    }
                },
                budget: {
                    validators: {
                        notEmpty: {
                            message: "Budget is required"
                        },
                        numeric: {
                            message: "Enter number only",
                            // The default separators
                            thousandsSeparator: "",
                            decimalSeparator: "."
                        }
                    }
                },
                // baserate: {
                //     validators: {
                //         notEmpty: {
                //             message: "Base rate is required"
                //         },
                //         numeric: {
                //             message: "Enter number only",
                //             // The default separators
                //             thousandsSeparator: '',
                //             decimalSeparator: '.'
                //         },
                //     }
                // },
                experiece: {
                    validators: {
                        notEmpty: {
                            message: "Experience is required"
                        }
                    }
                },
                job_type: {
                    validators: {
                        notEmpty: {
                            message: "Job type is required"
                        }
                    }
                },
                job_mode: {
                    validators: {
                        notEmpty: {
                            message: "Job mode is required"
                        }
                    }
                },
                start_date: {
                    validators: {
                        notEmpty: {
                            message: "Start date is required"
                        }
                    }
                },
                duration: {
                    validators: {
                        notEmpty: {
                            message: "Duration is required"
                        },
                        numeric: {
                            message: "Enter number only",
                            // The default separators
                            thousandsSeparator: ""
                        }
                    }
                },
                state: {
                    validators: {
                        callback: {
                            message: " ",
                            callback: function(value, validator) {
                                // Get the selected options
                                var options = validator.getFieldElements("state").val();
                                return options != null;
                            }
                        }
                    }
                },
                city: {
                    validators: {
                        callback: {
                            message: " ",
                            callback: function(value, validator) {
                                // Get the selected options
                                var options = validator.getFieldElements("city").val();
                                return options != null;
                            }
                        }
                    }
                },
                industry: {
                    validators: {
                        notEmpty: {
                            message: "Industry is required"
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: "Category is required"
                        }
                    }
                },
                key_skills: {
                    validators: {
                        notEmpty: {
                            message: "Key skills required"
                        }
                    }
                },
                job_description: {
                    validators: {
                        callback: {
                            message: "Job description is required",
                            callback: function(input) {
                                const code = $('[name="job_description"]').summernote("code");
                                // <p><br></p> is code generated by Summernote for empty content
                                return code !== "" && code !== "<p><br></p>";
                            }
                        }
                    }
                },
                requirements: {
                    validators: {
                        callback: {
                            message: "Requirement is required",
                            callback: function(input) {
                                const code = $('[name="requirements"]').summernote("code");
                                // <p><br></p> is code generated by Summernote for empty content
                                return code !== "" && code !== "<p><br></p>";
                            }
                        }
                    }
                },
                benefits: {
                    validators: {
                        callback: {
                            message: "Benefits is required",
                            callback: function(input) {
                                const code = $('[name="benefits"]').summernote("code");
                                // <p><br></p> is code generated by Summernote for empty content
                                return code !== "" && code !== "<p><br></p>";
                            }
                        }
                    }
                }
            }
        })
        .on("error.validator.bv", function(e, data) {
            data.element
                .data("bv.messages")
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]')
                .hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]')
                .show();
        });
    $("#job-start-date")
        .datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-0d"
        })
        .on("changeDate", function(selected) {
            $("#client-job-post").bootstrapValidator("revalidateField", "start_date");
        });

    /****************ajax save job****************/
    $(".consultant-img-loader").hide();
    $(document).on("click", ".consultant-job-saved", function(e) {
        $this = this;
        var zoho_id = $(this).attr("data-zohoid");
        var job_id = $(this).attr("data-id");
        var locationReload = $(this).attr("data-redirecttab");
        //        var redirect = $(this).attr('data-redirect');
        // return bootbox.confirm("Are you sure you want to save this job?", function (result) {
        //     if (result) {
        $(this)
            .next(".consultant-img-loader")
            .show();
        $(this).hide();
        $.ajax({
            type: "PUT",
            url: consultantsavejob, // point to server-side PHP script
            cache: false,
            data: {
                _token: $("input[name=_token]").val(),
                zoho_id: zoho_id,
                job_id: job_id
            },
            success: function(data) {
                $($this)
                    .next(".consultant-img-loader")
                    .hide();
                $($this).show();
                $($this).text("Unsave");
                $($this).removeClass("consultant-job-saved");
                $($this).addClass("savejob-delete");
                $($this).prop("title", "Unsave");
                toastr.success("Job has been saved successfully");
                $savedCounter = parseInt($("#saved-job-counter").text());
                $savedCounter = $savedCounter + 1;
                $("#saved-job-counter").text($savedCounter);
                //                location.href = data.redirect_url;
                //                if (redirect = true) {
                //                    location.reload();
                //                }
                if (locationReload == "true") {
                    setTimeout(function() {
                        var urlLoaction = savejobUrl + "?tab=savedjobs";
                        window.location.href = urlLoaction;
                    }, 750);
                }
            }
        });
        //     }
        // });
    });

    /****************ajax Apply  job****************/
    // $(".consultant-job-applied").click(function(e) {
    //   $this = this;
    //   var zoho_id = $(this).attr("data-zohoid");
    //   var id = $(this).attr("data-id");
    //   var locationReload = $(this).attr("data-reload");
    //   // return bootbox.confirm("Are you sure you want to apply for this job?", function (result) {
    //   //     if (result) {
    //   function reloadUrl() {
    //     if (locationReload == "true") {
    //       location.reload();
    //     }
    //   }

    //   $($this).hide();
    //   $($this)
    //     .prev(".consultant-img-loader")
    //     .show();
    //   $.ajax({
    //     type: "PUT",
    //     url: consultantappliedjob, // point to server-side PHP script
    //     cache: false,
    //     data: {
    //       _token: $("input[name=_token]").val(),
    //       zoho_id: zoho_id,
    //       id: id
    //     },
    //     success: function(data) {
    //       $($this)
    //         .prev(".consultant-img-loader")
    //         .hide();
    //       $($this).show();
    //       if (data.status == 200) {
    //         $($this)
    //           .prev()
    //           .prev(".btnSave")
    //           .hide();
    //         $($this).removeClass("consultant-job-applied");
    //         $($this).unbind();
    //         $($this).addClass("withdraw-application");
    //         $($this)
    //           .closest(".job-tile-button-right")
    //           .find(".btn-save-apply")
    //           .hide();
    //         $($this)
    //           .closest(".btn-apply-full")
    //           .addClass("col-lg-12");
    //         $($this).trigger("withdraw-application");
    //         $($this).text("Withdraw Application");
    //         $($this).prop("title", "Withdraw Application");
    //         toastr.success(data.message);
    //         setTimeout(reloadUrl, 750);
    //       } else {
    //         toastr.error(data.message);
    //       }
    //       //                location.href = data.redirect_url;
    //       //                        location.reload();
    //     }
    //   });
    //   //     }
    //   // });
    // });

    /*********************Save job Delete **********************/
    $(document).on("click", ".savejob-delete", function(e) {
        $this = this;
        var zoho_id = $(this).attr("data-zohoid");
        var locationReload = $(this).attr("data-reloadtab");

        return bootbox.confirm({
            message: "Are you sure you want to unsave this job?",
            buttons: {
                confirm: {
                    label: "Yes",
                    className: "btn btn-secondary btn-md"
                },
                cancel: {
                    label: "No",
                    className: "btn btn-danger btn-md"
                }
            },
            callback: function(result) {
                if (result == true) {
                    $(this)
                        .prev(".consultant-img-loader")
                        .show(200);
                    $.ajax({
                        type: "PUT",
                        url: consultantDeleteJob, // point to server-side PHP script
                        cache: false,
                        data: {
                            _token: $("input[name=_token]").val(),
                            zoho_id: zoho_id
                        },
                        success: function(response, data) {
                            //                        location.reload();
                            $($this).text("Save");
                            $($this).removeClass("savejob-delete");
                            $($this).addClass("consultant-job-saved");
                            $($this).prop("title", "Save");
                            toastr.success("Job has been unsaved successfully");
                            $savedCounter = parseInt($("#saved-job-counter").text());
                            $savedCounter = $savedCounter - 1;
                            $("#saved-job-counter").text($savedCounter);
                            if (locationReload == "true") {
                                setTimeout(function() {
                                    var urlLoaction = savejobUrl + "?tab=savedjobs";
                                    window.location.href = urlLoaction;
                                    //   location.reload();
                                }, 750);
                            }
                        }
                    });
                }
            }
        });
    });
    /*------------------consultant profile available --------------*/
    $(".consultant_avliable").change(function(e) {
        e.preventDefault();
        if ($(this).prop("checked") == true) {
            var value = 1;
            var message = "Yeay, you're back! Glad to see you!";
        } else {
            var value = 0;
            var message = "We will miss you! Come back to us fast!";
        }
        $(this).addClass("disabled");
        // return bootbox.confirm("Are you sure you want to available for job?", function (result) {
        //     if (result) {
        $.ajax({
            type: "PUT",
            url: consultantprofileavaliable, // point to server-side PHP script
            cache: false,
            data: {
                _token: $("input[name=_token]").val(),
                value: value
            },
            success: function(response, data) {
                $(this).removeClass("disabled");
                if (value == 0) {
                    $(".avalibel-un").text("Unavailable");
                    $(".avalibel-un").addClass("red");
                    toastr.error(message);
                }
                if (value == 1) {
                    $(".avalibel-un").text("Available");
                    $(".avalibel-un").removeClass("red");
                    toastr.success(message);
                }
            }
        });
        //     }
        // });
    });
});
var clicked = false;
var clicked1 = false;
$(".all_checked").change(function() {
    // $('.checked_all').trigger('click');
    $(".set_job_alerts").trigger("click", !clicked);
    $(".checked_all").prop("checked", !clicked1);
    clicked = !clicked;
    clicked1 = !clicked1;
});
$(".set_job_alerts").change(function() {
    $(".jobalerts_tags").toggleClass("job-hide-tags");
    clicked = !clicked;
});

$("#job-state").change(function() {
    $.ajax({
        type: "POST",
        url: getCityList,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: $(this).val()
        },
        success: function(data) {
            $("#job-city").empty();
            $("#job-city").append(
                $("<option value=''></option>").text("--Select City--")
            );
            $.each(data.city, function(key, value) {
                $("#job-city").append(
                    $("<option></option>")
                    .attr("value", key)
                    .text(value)
                );
            });
            $("#client-job-post").bootstrapValidator("revalidateField", "city");
        }
    });
});
/*---For consltant profiel change ----*/
$(document).on("change", "#cons-profile-state", function(e) {
    $.ajax({
        type: "POST",
        url: getCityList,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: $(this).val()
        },
        success: function(data) {
            $("#cons-profile-city").empty();
            $("#cons-profile-city").append(
                $("<option value=''></option>").text("--Select City--")
            );
            $.each(data.city, function(key, value) {
                $("#cons-profile-city").append(
                    $("<option></option>")
                    .attr("value", key)
                    .text(value)
                );
            });
            $("#consultant-profile-form").bootstrapValidator(
                "revalidateField",
                "city"
            );
        }
    });
});

// Delete single record [ this can use for all single deltetion ]
$(".delete-job").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to delete this job?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});

// delete attachment
$(".delete-attachment").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to delete this attachment?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});
// Delete single record [ this can use for all single deltetion ]
$(".consultant-applied-job").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    console.log(href);
    return bootbox.confirm(
        "Are you sure you want to applied for this job?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});

// Add Job Note
$(".add-note").click(function() {
    var id = $(this).attr("data-id");
    // $("#add-note-modal").modal('show');
    $(".note-container").hide("100");
    $(this)
        .closest(".commanContent")
        .find("#job_id")
        .val(id);
    $(this)
        .closest(".commanContent")
        .find(".note-container")
        .show("100");
    // $("#job_id").val(id);
});
$(".boxTabsearch #search-consultants").click(function() {
    $("#search_type").val("consultant");
});
$(".boxTabsearch #search-jobs").click(function() {
    $("#search_type").val("job");
});

$(".boxTabsearch #search-consultants").click(function() {
    $(".both-search_type").val("consultant");
});
$(".boxTabsearch #search-jobs").click(function() {
    $(".both-search_type").val("job");
});
//$(".view-all").click(function () {
//    $('#search-form').find('input:text, input:password, select, textarea').val('');
//    $('#search-form').find('.select2').val("").trigger('change');
//    $('#search-form').find('input:radio, input:checkbox').prop('checked', false);
//    $("#search-form").submit();
//});
/* Save to wishlist */
$(document).on("click", ".save-to-wish-list", function(e) {
    var $this = $(this);
    var $loader = $(this).next(".wishlist-loading");
    $this.hide();
    $loader.show();
    var consultant_id = $(this).attr("data-id");
    var reload = $(this).attr("data-reload");
    var reloadtab = $(this).attr("data-reloadtab");
    var db_id = $(this).attr("data-candidate-id");
    // console.log(reload);
    // return;
    $.ajax({
        type: "POST",
        url: saveToWishlist,
        cache: false,
        data: { consultant_id: consultant_id, db_id: db_id, reload: reload },
        success: function(response) {
            if (response.status == "success") {
                $loader.hide();
                $this.show();
                $this.text("Remove Wishlist");
                $this.attr("title", "Remove Wishlist");
                $this.removeClass("save-to-wish-list");
                $this.addClass("remove-to-wish-list-con");
                // $this.closest(".boxbtns").find('.add-compare').addClass("full-width");
                toastr.success("Profile has been added to wishlist successfully.");
                // if (reload == 'true') {
                //     location.reload();
                // } else {
                //     location.href = response.redirect_url;
                // }
                if (reloadtab == "true") {
                    $this.show();
                    location.reload();
                }
            } else {
                $loader.hide();
                $this.show();
                toastr.error("This consultant is already added into your wishlist");
            }
        }
    });
});

/* Remove ajax to wishlist */
$(document).on("click", ".remove-to-wish-list-con", function(e) {
    var $this = $(this);
    var $loader = $(this).next(".wishlist-loading");
    // $this.hide();
    // $loader.show();
    var consultant_id = $(this).attr("data-id");
    // var reload = $(this).attr('data-reload');
    var reloadtab = $(this).attr("data-reloadtab");
    // var db_id = $(this).attr('data-candidate-id');
    // return
    $.ajax({
        type: "POST",
        url: removeToWishlist_cons,
        cache: false,
        data: {
            consultant_id: consultant_id,
            _token: $("input[name=_token]").val()
        },
        success: function(response) {
            if (response.status == "200") {
                $loader.hide();
                $this.show();
                $this.text("Save To Wishlist");
                $this.attr("title", "Save To Wishlist");
                $this.removeClass("remove-to-wish-list-con");
                $this.addClass("save-to-wish-list");
                toastr.error("Profile has been removed to your wishlist.");
                if (reloadtab == "true") {
                    // location.href = response.redirect_url;
                    location.reload();
                }
            } else {
                $loader.hide();
                $this.show();
                toastr.error("Profile alredy removed to your wishlist.");
            }
        }
    });
});

/* Add to Compare */
$(document).on("click", ".can-btnCompare", function(e) {
    var $this = $(this);
    var $loader = $(this).next(".wishlist-loading");
    $this.hide();
    $loader.show();
    var consultant_id = $(this).attr("data-id");
    var reload = $(this).attr("data-reload");
    var db_id = $(this).attr("data-candidate-id");

    // console.log(reload);
    // return;
    $.ajax({
        type: "POST",
        url: addCompare,
        cache: false,
        data: {
            consultant_id: consultant_id,
            db_id: db_id,
            reload: reload,
            _token: $("input[name=_token]").val()
        },
        success: function(response) {
            if (response.status == 200) {
                $loader.hide();
                $this.show();
                $this.text("Remove Compare");
                $this.attr("title", "Remove Compare");
                $this.removeClass("can-btnCompare");
                $this.addClass("can-btnRemoveCompare");
                // $this.addClass('can-btnRemoveCompare');
                $(".comparecount").show();
                $(".comparecount").text("Compare Now (" + response.counter + ")!");
                // $this.closest(".boxbtns").find('.add-compare').addClass("full-width");
                toastr.success("consultant has been added to compare.");
                // location.reload();
                if (reload == "true") {
                    location.reload();
                } else if (reload == "false") {} else {
                    setTimeout(function() {
                        location.href = response.redirect_url;
                    }, 1000);
                }
            } else {
                $loader.hide();
                $this.show();
                toastr.error("Maximum add three consultant");
            }
        }
    });
});

/**Remove from ad to compare */

$(document).on("click", ".can-btnRemoveCompare", function(e) {
    var $this = $(this);
    var $loader = $(this).next(".wishlist-loading");
    $this.hide();
    $loader.show();
    var id = $(this).attr("data-id");
    var reload = $(this).attr("data-reload");
    // console.log(reload);
    // return;
    $.ajax({
        type: "POST",
        url: removeComparepage,
        cache: false,
        data: { id: id, _token: $('input[name="_token"]').val() },
        success: function(response) {
            if (response.status == "200") {
                $loader.hide();
                $this.show();
                $this.text("Add to Compare");
                $this.attr("title", "Add to Compare");
                $this.removeClass("can-btnRemoveCompare");
                $this.addClass("can-btnCompare");
                $(".comparecount").show();
                $(".comparecount").text("Compare Now (" + response.counter + ")!");
                toastr.error("consultant has been Remove to compare.");
                if (reload == "true") {
                    location.reload();
                }
                // location.reload();
            } else {
                toastr.error("Consultant not founded");
                $loader.hide();
                $this.show();
            }
        }
    });
});
/* General Save to Wishlist */
$(".general-saveto-wishlist").click(function(event) {
    //    var consultant_id = $(this).attr('data-id');
    event.stopPropagation();
    var redirect_url = $(this).attr("data-redirect");

    $(".login-form-toggle")
        .find("#redirect_url")
        .val(redirect_url);
    $("html, body").animate({ scrollTop: 0 }, "slow");

    $(".forgot-form-block").hide();
    $(".login-form-toggle").show(200);
    $(".login-form-block").show(200);
});

$(".general-saveto-compare").click(function() {
    //var consultant_id = $(this).attr('data-id');
    event.stopPropagation();
    var redirect_url = $(this).attr("data-redirect");

    $(".login-form-toggle")
        .find("#redirect_url")
        .val(redirect_url);
    $("html, body").animate({ scrollTop: 0 }, "slow");

    $(".forgot-form-block").hide();
    $(".login-form-toggle").show(200);
    $(".login-form-block").show(200);
});

/* General Save job */
$(".general-save-job").click(function() {
    //var consultant_id = $(this).attr('data-id');
    event.stopPropagation();
    var redirect_url = $(this).attr("data-redirect");

    $(".login-form-toggle")
        .find("#redirect_url")
        .val(redirect_url);
    $("html, body").animate({ scrollTop: 0 }, "slow");

    $(".forgot-form-block").hide();
    $(".login-form-toggle").show(200);
    $(".login-form-block").show(200);
});
$("#modalLoginForm").on("hidden.bs.modal", function() {
    //    $("#modalLoginForm").find('#wishlist_consultant_id').val("");
    $("#modalLoginForm")
        .find("#redirect_url")
        .val("");
});
$(".delete-from-wishlist").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to remove this consultant from wishlist?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});
$(".delete-from-request").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to remove this consultant from requested profiles?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});
/* General Apply Job */
$(".general-apply-job").click(function() {
    // var redirect_url = $(this).attr('data-redirect');
    // $("#modalLoginForm").modal('show');
    // $("#modalLoginForm").find("#redirect_url").val(redirect_url);
    event.stopPropagation();
    var redirect_url = $(this).attr("data-redirect");

    $(".login-form-toggle")
        .find("#redirect_url")
        .val(redirect_url);
    $("html, body").animate({ scrollTop: 0 }, "slow");

    $(".forgot-form-block").hide();
    $(".login-form-toggle").show(200);
    $(".login-form-block").show(200);
});
$(".btnProfilerequest").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to submit request to admin for this profile access?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});

$(".cancelbtnProfilerequest").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to cancelled request this candidate profile?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});
$(".btnNonEditSummary").click(function() {
    return bootbox.alert(
        "<span class='text-red'>You are summary is pending for approval. You will be notified once summary approved</span>"
    );
});
$(document).on("click", ".withdraw-application", function() {
    $this = $(this);
    $loader = $(this).prev(".consultant-img-loader");
    var zoho_id = $(this).attr("data-zohoid");
    var id = $(this).attr("data-id");

    return bootbox.confirm({
        message: "Are you sure you want to withdraw your application for this job posting?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                $this.hide();
                $loader.show();
                $.ajax({
                    type: "PUT",
                    url: consultantWithdrawjob, // point to server-side PHP script
                    cache: false,
                    data: {
                        _token: $("input[name=_token]").val(),
                        zoho_id: zoho_id,
                        id: id
                    }
                }).done(function(data) {
                    $this.show();
                    $loader.hide();
                    if (data.status == 200) {
                        $this.addClass("disabled");
                        $this.text("Application Withdrawn");
                        $this.prop("title", "Application Withdrawn");
                        toastr.success(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                });
            }
        }
    });
});
$(document).on("click", ".profile-infromation", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    var baserate = $(".profile-br-input").attr("data-br");
    var zohoid = $(".profile-br-input").attr("data-zohoid");

    var rbaserate = $(".profile-rbr-input").attr("data-rbr");

    var npr = $(".profile-npr-input").attr("data-npr");

    var date = $(".profile-date-input").attr("data-date");

    $(".profile-br-input").replaceWith(
        '<div class="replace-brdiv side-bar d-inline"><input type="number" placeholder="MYR" value="' +
        baserate +
        '" data-cvalue="' +
        baserate +
        '" data-zohoid="' +
        zohoid +
        '" class="submit-brvalue" maxlength="10">'
    );

    $(".profile-rbr-input").replaceWith(
        '<div class="replace-rbrdiv side-bar d-inline"><input type="number" placeholder="MYR" value="' +
        rbaserate +
        '" data-cvalue="' +
        rbaserate +
        // '" class="submit-rbrvalue" disabled></div>'
        '" class="submit-rbrvalue"></div>'
    );

    $(".profile-npr-input").replaceWith(
        '<div class="replace-nprdiv side-bar d-inline"><input type="number" placeholder="Days" value="' +
        npr +
        '" data-cvalue="' +
        npr +
        '" class="submit-nprvalue"></div>'
    );

    $(".profile-date-input").replaceWith(
        '<div class="replace-datediv side-bar d-inline" ><input type="text" placeholder="dd-mm-yyyy" style="margin-left: 12%;" value="' +
        date +
        '" data-cvalue="' +
        date +
        '" class="submit-datevalue" readonly><div class="mt-3"><a href="javascript:;" style="float:right;color: #4FDB73!important;" class="save-profile-information buttonDefault save ">Save</a><a href="javascript:;"  class="cancel-profile-infromation buttonDefault can">Cancel</a><div></div>'
    );
});
$(document)
    .on("keypress", ".submit-brvalue", function(event) {
        var value = $(this).val();
        var maxLength = 10;
        if (value.length > maxLength) {
            return false;
        }
        if (
            (event.which != 46 ||
                (event.which == 46 && $(this).val() == "") ||
                $(this)
                .val()
                .indexOf(".") != -1) &&
            (event.which < 48 || event.which > 57)
        ) {
            event.preventDefault();
        }
    })
    .on("paste", function(event) {
        event.preventDefault();
    });
$(document)
    .on("keypress", ".submit-rbrvalue", function(event) {
        var value = $(this).val();
        var maxLength = 10;
        if (value.length > maxLength) {
            return false;
        }
        if (
            (event.which != 46 ||
                (event.which == 46 && $(this).val() == "") ||
                $(this)
                .val()
                .indexOf(".") != -1) &&
            (event.which < 48 || event.which > 57)
        ) {
            event.preventDefault();
        }
    })
    .on("paste", function(event) {
        event.preventDefault();
    });
$(document)
    .on("keypress", ".submit-nprvalue", function(event) {
        var value = $(this).val();
        var maxLength = 2;
        if (value.length > maxLength) {
            return false;
        }
        if (
            (event.which != 46 ||
                (event.which == 46 && $(this).val() == "") ||
                $(this)
                .val()
                .indexOf(".") != -1) &&
            (event.which < 48 || event.which > 57)
        ) {
            event.preventDefault();
        }
    })
    .on("paste", function(event) {
        event.preventDefault();
    });
// var minLength = 3;
// var maxLength = 10;
// $(document).on("keypress", ".submit-brvalue", function(event) {
//   var value = $(this).val();
//   if (value.length > maxLength){
//       return false;
//   }
// });

$("body").on("click", ".cancel-profile-infromation", function(e) {
    e.preventDefault();
    var br_value = $(".submit-brvalue").attr("data-cvalue");

    var rbr_value = $(".submit-rbrvalue").attr("data-cvalue");

    var npr_value = $(".submit-nprvalue").attr("data-cvalue");

    var date = $(".submit-datevalue").attr("data-cvalue");

    $(".replace-brdiv").replaceWith(
        '<div class=" text-red inlineText profile-br-input float-right " data-br="' +
        br_value +
        '"><span class="cus-br-icon">BR</span> MYR ' +
        br_value +
        "</div>"
    );

    $(".replace-rbrdiv").replaceWith(
        '<div class="inlineText profile-rbr-input float-right" data-rbr="' +
        rbr_value +
        '">MYR ' +
        rbr_value +
        "</div>"
    );

    $(".replace-nprdiv").replaceWith(
        '<div class="inlineText profile-npr-input float-right" data-npr="' +
        npr_value +
        '">' +
        npr_value +
        " days</div>"
    );

    $(".replace-datediv").replaceWith(
        '<div class="inlineText  profile-date-input float-right" data-date="' +
        date +
        '">' +
        date +
        "</div>"
    );

    $(".profile-infromation").show();
});
$("body").on("click", ".save-profile-information", function(e) {
    e.preventDefault();

    var rbr_value = $(".submit-rbrvalue").val();

    var br_value = $(".submit-brvalue").val();

    var nprvalue = $(".submit-nprvalue").val();

    var datevalue = $(".submit-datevalue").val();

    if ($.trim(rbr_value) == "") {
        return bootbox.alert("Hello");
    } else {
        if (parseFloat(rbr_value) < parseInt(0)) {
            return bootbox.alert("Reserved base rate must be positive number");
        }
        // if (!rbr_value.match(/^\d.+$/)) {
        //     return bootbox.alert("Reserved base rate must be digits");
        // }
    }

    if ($.trim(br_value) == "") {
        return bootbox.alert("Please enter base rate");
    } else {
        if (parseFloat(br_value) < parseInt(0)) {
            return bootbox.alert("Base rate must be positive number");
        }
        // if (!br_value.match(/^\d.+$/)) {
        //     return bootbox.alert("Base rate must be digits");
        // }
    }

    if ($.trim(nprvalue) == "") {
        return bootbox.alert("Please enter notice period");
    } else {
        if (parseFloat(nprvalue) < parseInt(0)) {
            return bootbox.alert("Notice period must be positive number");
        }
        if (!nprvalue.match(/^\d+$/)) {
            return bootbox.alert("Notice period must be digits");
        }
    }
    if ($.trim(datevalue) == "") {
        return bootbox.alert("Please enter availability date");
    }

    var zohoid = $(".submit-rbrvalue").attr("data-zohoid");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            base_rate: br_value,
            rbase_rate: rbr_value,
            nprvalue: nprvalue,
            datevalue: datevalue,
            zohoid: zohoid
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            // var br_value = $('.submit-brvalue').val();
            $(".replace-brdiv").replaceWith(
                '<div class="text-red inlineText profile-br-input float-right" data-br="' +
                br_value +
                '"><span class="cus-br-icon">BR</span> MYR ' +
                br_value +
                "</div>"
            );

            $(".replace-rbrdiv").replaceWith(
                '<div class="inlineText profile-rbr-input float-right" data-rbr="' +
                rbr_value +
                '">MYR ' +
                rbr_value +
                "</div>"
            );

            $(".replace-nprdiv").replaceWith(
                '<div class="inlineText profile-npr-input float-right" data-npr="' +
                nprvalue +
                '">' +
                nprvalue +
                " days</div>"
            );

            $(".replace-datediv").replaceWith(
                '<div class="inlineText  profile-date-input float-right" data-date="' +
                datevalue +
                '">' +
                datevalue +
                "</div>"
            );

            $(".profile-infromation").show();
            toastr.success("Your information has been saved successfully!!");
        }
    });
});

$(".check-fulltime").change(function(e) {
    e.preventDefault();
    if ($(this).prop("checked") == true) {
        var fulltime = 1;
        var message = "You are now available for full time job";
    } else {
        var fulltime = 0;
        var message = "You are no longer available for full time job";
    }
    $(this).addClass("disabled");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            fulltime: fulltime
        },
        success: function(response, data) {
            $(this).removeClass("disabled");
            if (fulltime == 0) {
                toastr.error(message);
            }
            if (fulltime == 1) {
                toastr.success(message);
            }
        }
    });
});

$(".check-willingtravel").change(function(e) {
    e.preventDefault();
    if ($(this).prop("checked") == true) {
        var willingtravel = 1;
        var message = "You are now available for willing to travel time job";
    } else {
        var willingtravel = 0;
        var message = "You are no longer available for willing to travel time job";
    }
    $(this).addClass("disabled");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            willingtravel: willingtravel
        },
        success: function(response, data) {
            $(this).removeClass("disabled");
            if (willingtravel == 0) {
                toastr.error(message);
            }
            if (willingtravel == 1) {
                toastr.success(message);
            }
        }
    });
});

$(".check-parttime").change(function(e) {
    e.preventDefault();
    if ($(this).prop("checked") == true) {
        var parttime = 1;
        var message = "You are now available for part time job";
    } else {
        var parttime = 0;
        var message = "You are no longer available for part time job";
    }
    $(this).addClass("disabled");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            parttime: parttime
        },
        success: function(response, data) {
            $(this).removeClass("disabled");
            if (parttime == 0) {
                toastr.error(message);
            }
            if (parttime == 1) {
                toastr.success(message);
            }
        }
    });
});

$(".check-support").change(function(e) {
    e.preventDefault();
    if ($(this).prop("checked") == true) {
        var support = 1;
        var message = "You are now available for support role";
    } else {
        var support = 0;
        var message = "You are no longer available for support role";
    }
    $(this).addClass("disabled");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            support: support
        },
        success: function(response, data) {
            $(this).removeClass("disabled");
            if (support == 0) {
                toastr.error(message);
            }
            if (support == 1) {
                toastr.success(message);
            }
        }
    });
});

$(".check-project").change(function(e) {
    e.preventDefault();
    if ($(this).prop("checked") == true) {
        var project = 1;
        var message = "You are now available for project role";
    } else {
        var project = 0;
        var message = "You are no longer available for project role";
    }
    $(this).addClass("disabled");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            project: project
        },
        success: function(response, data) {
            $(this).removeClass("disabled");
            if (project == 0) {
                toastr.error(message);
            }
            if (project == 1) {
                toastr.success(message);
            }
        }
    });
});

$(document).on("click", ".profile-base-rate", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    var baserate = $(".profile-br-input").attr("data-br");
    var zohoid = $(".profile-br-input").attr("data-zohoid");
    $(".profile-br-input").replaceWith(
        '<div class="replace-brdiv side-bar"><input type="number" value="' +
        baserate +
        '" data-cvalue="' +
        baserate +
        '" data-zohoid="' +
        zohoid +
        '" class="submit-brvalue"><a href="javascript:void(0)" class="save-brbtn save-green">Save</a><a href="javascript:void(0)" class="cancel-brbtn cancel-red">X</a></div>'
    );
});

$("body").on("click", ".cancel-brbtn", function(e) {
    e.preventDefault();
    var br_value = $(".submit-brvalue").attr("data-cvalue");
    $(".replace-brdiv").replaceWith(
        '<div class="iconBox iconMYR text-red inlineText profile-br-input" data-br="' +
        br_value +
        '">MYR ' +
        br_value +
        "</div>"
    );
    $(".profile-base-rate").show();
});
$("body").on("click", ".save-brbtn", function(e) {
    e.preventDefault();
    var br_value = $(".submit-brvalue").val();
    if ($.trim(br_value) == "") {
        return bootbox.alert("Please enter base rate");
    } else {
        if (parseFloat(br_value) < parseInt(0)) {
            return bootbox.alert("Base rate must be positive number");
        }
        if (!br_value.match(/^\d.+$/)) {
            return bootbox.alert("Base rate must be digits");
        }
    }
    // var zohoid = $('.submit-brvalue').attr("data-zohoid");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            base_rate: br_value
                // "zohoid": zohoid
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            var br_value = $(".submit-brvalue").val();
            $(".replace-brdiv").replaceWith(
                '<div class="iconBox iconMYR text-red inlineText profile-br-input" data-br="' +
                br_value +
                '">MYR ' +
                br_value +
                "</div>"
            );
            $(".profile-base-rate").show();
            toastr.success("You are base rate has been saved successfully!!");
        }
    });
});

$(document).on("click", ".profile-rbase-rate", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    var rbaserate = $(".profile-rbr-input").attr("data-rbr");
    // var zohoid = $('.profile-rbr-input').attr("data-zohoid");
    $(".profile-rbr-input").replaceWith(
        '<div class="replace-rbrdiv side-bar"><input type="number" value="' +
        rbaserate +
        '" data-cvalue="' +
        rbaserate +
        '" class="submit-rbrvalue"><a href="javascript:void(0)" class="save-rbrbtn save-green">Save</a><a href="javascript:void(0)" class="cancel-rbrbtn cancel-red">X</a></div>'
    );
});
$("body").on("click", ".cancel-rbrbtn", function(e) {
    e.preventDefault();
    var rbr_value = $(".submit-rbrvalue").attr("data-cvalue");
    $(".replace-rbrdiv").replaceWith(
        '<div class="inlineText profile-rbr-input" data-rbr="' +
        rbr_value +
        '">' +
        rbr_value +
        "</div>"
    );
    $(".profile-rbase-rate").show();
});
$("body").on("click", ".save-rbrbtn", function(e) {
    e.preventDefault();

    var rbr_value = $(".submit-rbrvalue").val();

    if ($.trim(rbr_value) == "") {
        return bootbox.alert("Please enter reserved base rate");
    } else {
        if (parseFloat(rbr_value) < parseInt(0)) {
            return bootbox.alert("Reserved base rate must be positive number");
        }
        if (!rbr_value.match(/^\d.+$/)) {
            return bootbox.alert("Reserved base rate must be digits");
        }
    }
    var zohoid = $(".submit-rbrvalue").attr("data-zohoid");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            rbase_rate: rbr_value,
            zohoid: zohoid
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            $(".replace-rbrdiv").replaceWith(
                '<div class="inlineText profile-rbr-input" data-rbr="' +
                rbr_value +
                '">' +
                rbr_value +
                "</div>"
            );
            $(".profile-rbase-rate").show();
            toastr.success(
                "You are reserved base rate has been saved successfully!!"
            );
        }
    });
});

$(document).on("click", ".profile-npr", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    var npr = $(".profile-npr-input").attr("data-npr");
    $(".profile-npr-input").replaceWith(
        '<div class="replace-nprdiv side-bar"><input type="number" value="' +
        npr +
        '" data-cvalue="' +
        npr +
        '" class="submit-nprvalue"><a href="javascript:void(0)" class="save-nprbtn save-green">Save</a><a href="javascript:void(0)" class="cancel-nprbtn cancel-red">X</a></div>'
    );
});
$("body").on("click", ".cancel-nprbtn", function(e) {
    e.preventDefault();
    var npr_value = $(".submit-nprvalue").attr("data-cvalue");
    $(".replace-nprdiv").replaceWith(
        '<div class="inlineText profile-npr-input" data-rbr="' +
        npr_value +
        '">' +
        npr_value +
        "</div>"
    );
    $(".profile-npr").show();
});
$("body").on("click", ".save-nprbtn", function(e) {
    e.preventDefault();

    var nprvalue = $(".submit-nprvalue").val();

    if ($.trim(nprvalue) == "") {
        return bootbox.alert("Please enter notice period");
    } else {
        if (parseFloat(nprvalue) < parseInt(0)) {
            return bootbox.alert("Notice period must be positive number");
        }
        if (!nprvalue.match(/^\d+$/)) {
            return bootbox.alert("Notice period must be digits");
        }
    }

    var zohoid = $(".submit-nprvalue").attr("data-zohoid");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            nprvalue: nprvalue,
            zohoid: zohoid
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            $(".replace-nprdiv").replaceWith(
                '<div class="inlineText profile-npr-input" data-npr="' +
                nprvalue +
                '">' +
                nprvalue +
                "</div>"
            );
            $(".profile-npr").show();
            toastr.success("You are notice period has been saved successfully!!");
        }
    });
});

/*----------------avalibel Date ---------------------*/

$(document).on("click", ".profile-date-btn", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    var date = $(".profile-date-input").attr("data-date");
    $(".profile-date-input").replaceWith(
        '<div class="replace-datediv side-bar"><input type="text" value="' +
        date +
        '" data-cvalue="' +
        date +
        '" class="submit-datevalue" readonly><a href="javascript:void(0)" class="save-datebtn save-green">Save</a><a href="javascript:void(0)" class="cancel-datebtn cancel-red">X</a></div>'
    );
});
$("body").on("click", ".cancel-datebtn", function(e) {
    e.preventDefault();
    var date = $(".submit-datevalue").attr("data-cvalue");
    $(".replace-datediv").replaceWith(
        '<div class="inlineText  profile-date-input" data-date="' +
        date +
        '">' +
        date +
        "</div>"
    );
    $(".profile-date-btn").show();
});
$("body").on("click", ".save-datebtn", function(e) {
    e.preventDefault();

    var datevalue = $(".submit-datevalue").val();
    // console.log(datevalue)
    // var zohoid = $('.submit-datediv').attr("data-zohoid");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            datevalue: datevalue
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            $(".replace-datediv").replaceWith(
                '<div class="inlineText  profile-date-input" data-date="' +
                datevalue +
                '">' +
                datevalue +
                "</div>"
            );
            $(".profile-date-btn").show();
            toastr.success("You are availability date has been saved successfully!!");
        }
    });
});
$("body").delegate(".submit-datevalue", "focusin", function() {
    $(this).datepicker({
        format: "dd MM.yyyy",
        startDate: "-0d"
    });
});
/*----------------End available date------------------*/
$(document).on("click", ".skill-value", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    $(".skill-profile-e li").each(function() {
        var skillvalue = $(this).attr("data-skill");
        //   alert(skillvalue);
    });
    var dataList = $(".skill-profile-e li")
        .map(function() {
            return $(this).attr("data-skill");
        })
        .get();
    var skillList = dataList.join(",");
    // var npr = $('.profile-npr-input').attr("data-npr");

    $(".skill-profile-e").replaceWith(
        '<div class="replace-skilldiv side-bar" style="padding-bottom:3%!important;"><textarea class="keyEnter submit-skill" data-cvalue="' +
        skillList +
        '" rows="2" cols="10" class="submit-skill">' +
        skillList +
        '</textarea><a href="javascript:void(0)" style="float:right;color: #4FDB73!important;" class="save-skillbtn buttonDefault save">Save</a><a href="javascript:void(0)" style="float:right;" class="cancel-skillbtn  buttonDefault can">Cancel</a></div>'
    );

});
// sni

$("body").on("click", ".cancel-skillbtn", function(e) {
    e.preventDefault();
    var skillValue = $(".submit-skill").attr("data-cvalue");
    var skillValueArray = skillValue.split(",");
    var statusHTML = '<ul class="listSkills listSkillsBig skill-profile-e">';
    for (let i = 0; i < skillValueArray.length; i++) {
        statusHTML +=
            '<li data-skill="' +
            skillValueArray[i] +
            '" ><a href="javascript:void(0)" >' +
            skillValueArray[i] +
            "</a></li>";
    }
    statusHTML += "</ul>";
    $(".replace-skilldiv").replaceWith(statusHTML);
    $(".skill-value").show();
});
$("body").on("click", ".save-skillbtn", function(e) {
    e.preventDefault();
    var skillValue = $(".submit-skill").val();
    var skillValueArray = skillValue.split(",");
    skillValue = "";
    for (let i = 0; i < skillValueArray.length; i++) {
        var skilllegtnth = skillValueArray[i].trim();
        // console.log(skilllegtnth);
        if (skilllegtnth.length > 0) {
            skillValue += skillValueArray[i];
            skillValue += ",";
        }
    }
    var lastChar = skillValue.slice(-1);
    if (lastChar == ",") {
        skillValue = skillValue.slice(0, -1);
    }
    // $.each(skillValue,function(i){
    //     alert(skillValue[i]);
    //  });
    // var zohoid = $('.submit-nprvalue').attr("data-zohoid");
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            skillValue: skillValue,
            type: "skill_set"
                // "zohoid": zohoid
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            var statusHTML = '<ul class="listSkills listSkillsBig skill-profile-e">';
            for (let i = 0; i < skillValueArray.length; i++) {
                var skilllegtnth = skillValueArray[i].trim();
                // console.log(skilllegtnth);
                if (skilllegtnth.length > 0) {
                    statusHTML +=
                        '<li data-skill="' +
                        skillValueArray[i] +
                        '" ><a href="javascript:void(0)" >' +
                        skillValueArray[i] +
                        "</a></li>";
                }
            }
            statusHTML += "</ul>";
            $(".replace-skilldiv").replaceWith(statusHTML);
            $(".skill-value").show();
            toastr.success("Your skill has been saved successfully!!");
        }
    });
});

/*for certification and training */

$(document).on("click", ".certi-profile", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    var certivalue = $(".certi-profile-value").attr("data-certi");
    var res = certivalue.split(",");
    res.forEach(displayCert);
    console.log(res);
    $(".certi-profile-value").hide();
    // var fieldHTML = '<label>Certification / Training Name:</label><input>'+ res +'</input>'; 

    var x = 1; //Initial field counter is 1
    function displayCert(item, index) {
        index++;
        if (res.length == index) {

            field = '<div style="margin-bottom:18px; class="cert' + index + '"><label style="margin-right:3%"></label><input class="size-space" name="cert[]" id=cert' + index + ' value=' + item + '></input><i class="fas fa-minus"  onclick="remove_field(' + index + ')" style="margin-left:5px;color:red;"></i><i class="fas fa-plus" id="last-index" onclick="add_new(' + index + ')" style="margin-left:5px;color:green;"></i></div>';
        } else {

            field = '<div style="margin-bottom:18px;" class="cert' + index + '"><label style="margin-right:3%"></label><input class="size-space" name="cert[]" id=cert' + index + ' value=' + item + '></input><i class="fas fa-minus"  onclick="remove_field(' + index + ')" style="margin-left:5px;color:red;"></i></div>';
        }
        $(".edit-cert").append(field);

    }


    $(".edit-cert").append('<div class="save-cont" style="margin-bottom:4%;"><a href="javascript:void(0)" style="float:right;color: #4FDB73!important;" class="save-certbtn buttonDefault save spacing-left">Save</a><a href="javascript:void(0)" style="float:right;" class="cancel-certbtn buttonDefault can">Cancel</a></div>');
});
// add field
function add_new(index) {
    index++;
    $("#last-index").hide();
    $(".save-cont").hide();
    newfield = '<div style="margin-bottom:18px;"><label style="margin-right:3%"></label><input name="cert[]" class="size-space"></input><i class="fas fa-minus" onclick="remove_field(' + index + ')" style="margin-left:5px;color:red;"></i><i class="fas fa-plus add-field" onclick="add_new(' + index + ')" style="margin-left:5px;color:green;"></i></div>';
    $(".edit-cert").append(newfield);
    $(".edit-cert").append('<div class="save-cont" style="margin-bottom:4%;"><a href="javascript:void(0)" style="float:right;color: #4FDB73!important;" class="save-certbtn buttonDefault save spacing-left">Save</a><a href="javascript:void(0)" style="float:right;" class="cancel-certbtn buttonDefault can">Cancel</a><div>');
};

// delete field 
function remove_field(index) {
    $(".cert" + index).remove();
};


$("body").on("click", ".cancel-certbtn", function(e) {
    location.reload();
});
$("body").on("click", ".save-certbtn", function(e) {
    e.preventDefault();
    var value = [];
    $("input[name='cert[]']").each(function() {
        console.log($(this).val());
        value.push($(this).val());
    });
    var strVal = value.toString();
    console.log(strVal);
    // var certiValue = $(".submit-certi").val();
    var certiValue = strVal;
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            certiValue: certiValue,
            type: "certificate"
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            $(".edit-cert").hide();
            $(".certi-profile").show();
            $(".certi-profile-value").replaceWith(
                '<div class="certi-profile-value" data-certi="' +
                certiValue +
                '">' +
                certiValue +
                "</div>"
            );
            // $(".certi-profile").show();
            toastr.success(
                "Certification and training has been saved successfully!!"

            );

        }
    });
});
/*----profile Work prefenrce -------*/
$(document).on("click", ".profile-work-edit", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    // var certivalue = $('.certi-profile-value').attr("data-certi");
    // var zohoid = $('.profile-rbr-input').attr("data-zohoid");
    $(".check-fulltime").removeAttr("disabled");
    $(".check-parttime").removeAttr("disabled");
    $(".check-willingtravel").removeAttr("disabled");
    if ($(".check-fulltime").is(":checked")) {
        var fulltime = 1;
    } else {
        var fulltime = 0;
    }
    if ($(".check-parttime").is(":checked")) {
        var parttime = 1;
    } else {
        var parttime = 0;
    }
    if ($(".check-willingtravel").is(":checked")) {
        var willingtravel = 1;
    } else {
        var willingtravel = 0;
    }
    $(".profile-work").append(
        '<div class="checkbox-save-work"><a href="javascript:void(0)" class="save-workbtn save-green">Save</a><a data-fulltime="' +
        fulltime +
        '" data-willingtravel="' +
        willingtravel +
        '"  data-parttime="' +
        parttime +
        '" href="javascript:void(0)" class="cancel-workbtn cancel-red">X</a></div>'
    );
});

$("body").on("click", ".cancel-workbtn", function(e) {
    e.preventDefault();
    var fulltime = $(this).attr("data-fulltime");
    var parttime = $(this).attr("data-parttime");
    var willingtravel = $(this).attr("data-willingtravel");
    $(".checkbox-save-work").remove();
    $(".profile-work-edit").show();
    $(".check-fulltime").attr("disabled", true);
    $(".check-parttime").attr("disabled", true);
    $(".check-willingtravel").attr("disabled", true);
    if (fulltime == 1) {
        $(".check-fulltime").prop("checked", true);
    } else {
        $(".check-fulltime").prop("checked", false);
    }
    if (parttime == 1) {
        $(".check-parttime").prop("checked", true);
    } else {
        $(".check-parttime").prop("checked", false);
    }
    if (willingtravel == 1) {
        $(".check-willingtravel").prop("checked", true);
    } else {
        $(".check-willingtravel").prop("checked", false);
    }
});
$("body").on("click", ".save-workbtn", function(e) {
    e.preventDefault();

    if ($(".check-fulltime").is(":checked")) {
        var fulltime = 1;
    } else {
        var fulltime = 0;
    }
    if ($(".check-parttime").is(":checked")) {
        var parttime = 1;
    } else {
        var parttime = 0;
    }
    if ($(".check-willingtravel").is(":checked")) {
        var willingtravel = 1;
    } else {
        var willingtravel = 0;
    }
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            fulltime: fulltime,
            parttime: parttime,
            willingtravel: willingtravel
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            $(".checkbox-save-work").remove();
            $(".profile-work-edit").show();
            $(".check-fulltime").attr("disabled", true);
            $(".check-parttime").attr("disabled", true);
            $(".check-willingtravel").attr("disabled", true);

            toastr.success("You are work preferences has been saved successfully!!");
        }
    });
});

/*----End profile and work prefenrc-------*/

/*----Role prefenrce -------*/
$(document).on("click", ".profile-role-edit", function(e) {
    e.preventDefault();
    $this = $(this);
    $this.hide();
    // var certivalue = $('.certi-profile-value').attr("data-certi");
    // var zohoid = $('.profile-rbr-input').attr("data-zohoid");
    $(".check-project").removeAttr("disabled");
    $(".check-support").removeAttr("disabled");
    if ($(".check-project").is(":checked")) {
        var project = 1;
    } else {
        var project = 0;
    }
    if ($(".check-support").is(":checked")) {
        var support = 1;
    } else {
        var support = 0;
    }
    $(".profile-role").append(
        '<div class="checkbox-save"><a href="javascript:void(0)" class="save-rolebtn  save-green">Save</a><a data-project="' +
        project +
        '" data-support="' +
        support +
        '" href="javascript:void(0)" class="cancel-rolebtn cancel-red">X</a></div>'
    );
});

$("body").on("click", ".cancel-rolebtn", function(e) {
    e.preventDefault();
    var project = $(this).attr("data-project");
    var support = $(this).attr("data-support");
    $(".checkbox-save").remove();
    $(".profile-role-edit").show();
    $(".check-project").attr("disabled", true);
    $(".check-support").attr("disabled", true);
    if (project == 1) {
        $(".check-project").prop("checked", true);
    } else {
        $(".check-project").prop("checked", false);
    }
    if (support == 1) {
        $(".check-support").prop("checked", true);
    } else {
        $(".check-support").prop("checked", false);
    }
});
$("body").on("click", ".save-rolebtn", function(e) {
    e.preventDefault();

    if ($(".check-project").is(":checked")) {
        var project = 1;
    } else {
        var project = 0;
    }
    if ($(".check-support").is(":checked")) {
        var support = 1;
    } else {
        var support = 0;
    }
    $.ajax({
        type: "PUT",
        url: consultantProfileChange, // point to server-side PHP script
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            project: project,
            support: support
        },
        success: function(response, data) {
            // $this.show();
            // $loader.hide();
            $(".checkbox-save").remove();
            $(".profile-role-edit").show();
            $(".check-project").attr("disabled", true);
            $(".check-support").attr("disabled", true);

            toastr.success("You are role preferences has been saved successfully!!");
        }
    });
});

/*----End Role  prefenrc-------*/

/**----References edit popup  ---------*/
$("body").on("click", "#edit-references-btn", function(e) {
    var data_id = $(this).attr("data-id");
    $.ajax({
        type: "POST",
        url: consultanteditref,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: data_id
        },
        success: function(data) {
            console.log(data.html);
            $("#edit-references").modal("show");
            $("#edit-references-content").empty();
            $("#edit-references-content").html(data.html);
        }
    });
});
/**----end Expirence edit popup  ---------*/

/*----consultant profile button --*/

$(".consultant-profile-edit").click(function() {
    $.ajax({
        type: "POST",
        url: getConsultantProfile,
        cache: false,
        data: {
            _token: $("input[name=_token]").val()
        },
        success: function(data) {
            $("#profile-data").empty();
            $("#update-profile-form-div").append(data.html);
        }
    });
});

/*----end consultant profile button---*/

$(document).on("click", ".savebutton-consultantReference", function() {
    var name = $(document)
        .find(".edited_refrencename")
        .val();
    var position = $(document)
        .find(".edited_refrenceposition")
        .val();
    var company = $(document)
        .find(".edited_refrencecompany")
        .val();
    var phone = $(document)
        .find(".edited_refrencephone")
        .val();
    var email = $(document)
        .find(".edited_constantemail")
        .val();

    if ($.trim(name) == "") {
        return bootbox.alert("Please enter your name");
    }
    if ($.trim(position) == "") {
        return bootbox.alert("Please enter your position");
    }
    if ($.trim(company) == "") {
        return bootbox.alert("Please enter your company");
    }
    if ($.trim(phone) == "") {
        return bootbox.alert("Please enter your phone");
    } else {
        if (!phone.match(/^\d+$/)) {
            return bootbox.alert("Phone number must be digits");
        }
        if (phone.length < 10 || phone.length > 15) {
            return bootbox.alert(
                "Phone number must be minimum 10 and maximum 15 digits"
            );
        }
    }
    if ($.trim(email) == "") {
        return bootbox.alert("Please enter your email ");
    } else {
        if (!isValidEmailAddress(email)) {
            return bootbox.alert("Please enter valid email address");
        }
    }
    $loader = $(document).find(".reference-loader");
    $loader.show();
    $.ajax({
        type: "POST",
        url: addProfileReferences,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            name: name,
            position: position,
            company: company,
            phone: phone,
            email: email
        },
        success: function(data) {
            $loader.hide();
            if (data.status == 200) {
                toastr.success("Reference has been saved successfully");
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr.error("Profile reference could not save");
            }
        }
    });
});

/*----end add consultant Refrence---*/

$(document).on("click", ".updatebutton-consultantReference", function() {
    var id = $(this).attr("data-id");
    var name = $(this)
        .closest("li")
        .find(".edited_refrencename")
        .val();
    var position = $(this)
        .closest("li")
        .find(".edited_refrenceposition")
        .val();
    var company = $(this)
        .closest("li")
        .find(".edited_refrencecompany")
        .val();
    var phone = $(this)
        .closest("li")
        .find(".edited_refrencephone")
        .val();
    var email = $(this)
        .closest("li")
        .find(".edited_constantemail")
        .val();

    if ($.trim(name) == "") {
        return bootbox.alert("Please enter your name");
    }
    if ($.trim(position) == "") {
        return bootbox.alert("Please enter your position");
    }
    if ($.trim(company) == "") {
        return bootbox.alert("Please enter your company");
    }
    if ($.trim(phone) == "") {
        return bootbox.alert("Please enter your phone");
    } else {
        if (!phone.match(/^\d+$/)) {
            return bootbox.alert("Phone number must be digits");
        }
        if (phone.length < 10 || phone.length > 15) {
            return bootbox.alert(
                "Phone number must be minimum 10 and maximum 15 digits"
            );
        }
    }
    if ($.trim(email) == "") {
        return bootbox.alert("Please enter your email ");
    } else {
        if (!isValidEmailAddress(email)) {
            return bootbox.alert("Please enter valid email address");
        }
    }
    $loader = $(document).find(".reference-loader");
    $loader.show();
    $.ajax({
        type: "POST",
        url: updateProfileReferences,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: id,
            name: name,
            position: position,
            company: company,
            phone: phone,
            email: email
        },
        success: function(data) {
            $loader.show();
            if (data.status == 200) {
                toastr.success("Reference has been updated successfully");
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr.error("Profile reference could not save");
            }
        }
    });
});

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(
        /^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i
    );
    return pattern.test(emailAddress);
}
/*----End edit consultant refrence--*/

$(document).on("click", ".savebutton-consultantEducation", function() {
    var institute = $(document)
        .find(".addEducation .edited_educationinstitute")
        .val();
    var department = $(document)
        .find(".addEducation .edited_educationdepartment")
        .val();
    var degree = $(document)
        .find(".addEducation .edited_educationdegree")
        .val();
    var duration_from_month = $(document)
        .find(".addEducation .education_duration_from_month")
        .val();
    var duration_from_year = $(document)
        .find(".addEducation .education_duration_from_year")
        .val();
    var duration_to_month = $(document)
        .find(".addEducation #education_duration_to_month")
        .val();
    var duration_to_year = $(document)
        .find(".addEducation #education_duration_to_year")
        .val();
    if (
        $(this)
        .closest(".addEducation")
        .find(".add_currently_pursuing")
        .is(":checked")
    ) {
        var isCurrentlyPursuing = "true";
    } else {
        var isCurrentlyPursuing = "false";
    }
    if ($.trim(institute) == "") {
        return bootbox.alert("Please enter your institute");
    }
    if ($.trim(department) == "") {
        return bootbox.alert("Please enter your department");
    }
    if ($.trim(degree) == "") {
        return bootbox.alert("Please enter your degree");
    }
    if (duration_from_month == "" || duration_from_year == "") {
        return bootbox.alert("Please select From duration");
    }
    if (isCurrentlyPursuing == "false") {
        if (duration_to_month == "" || duration_to_year == "") {
            return bootbox.alert("Please select To duration");
        }
        if (parseInt(duration_to_year) < parseInt(duration_from_year)) {
            return bootbox.alert(
                "Duration To year must be greater than duration From year"
            );
        } else {
            if (
                parseInt(duration_to_year) == parseInt(duration_from_year) &&
                parseInt(duration_to_month) < parseInt(duration_from_month)
            ) {
                return bootbox.alert(
                    "Duration To month must be greater than duration From month"
                );
            }
        }
    }
    $loader = $(document).find(".education-loader");
    $loader.show();
    $.ajax({
        type: "POST",
        url: addProfileEducation,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            institute: institute,
            department: department,
            degree: degree,
            duration_from_month: duration_from_month,
            duration_from_year: duration_from_year,
            duration_to_month: duration_to_month,
            duration_to_year: duration_to_year,
            isCurrentlyPursuing: isCurrentlyPursuing
        },
        success: function(data) {
            $loader.hide();
            if (data.status == 200) {
                toastr.success("Education has been saved successfully");
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr.error("Profile education could not save");
            }
        }
    });
});

/*----end add consultant Education---*/

$(".add-tabular-data").click(function() {
    var data_id = 0;
    var data_type = $(this).attr("data-type");
    $this = $(this);
    $.ajax({
        type: "POST",
        url: getTabularData,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: data_id,
            data_type: data_type
        },
        success: function(data) {
            if (data.data_type == "experience") {
                $(".addExperience").empty();
                $(".addExperience").append(data.html);
            } else if (data.data_type == "education") {
                $(".addEducation").empty();
                $(".addEducation").append(data.html);
            } else {
                $(".addReference").empty();
                $(".addReference").append(data.html);
            }
        }
    });
});
// Add Experience
$(document).on("change", ".add_is_currently_working_here", function() {
    if ($(this).is(":checked")) {
        var currentlyWorkingExperienceId = $(
            "#is_already_working_experience_id"
        ).val();
        if (currentlyWorkingExperienceId == "") {
            $(this)
                .closest(".addExperience")
                .find(".work_duration_to")
                .attr("disabled", true);
        } else {
            toastr.error(
                "You are not allowed to select this option. Because you are currently working on some other company."
            );
            $(this).prop("checked", false);
        }
    } else {
        $(this)
            .closest(".addExperience")
            .find(".work_duration_to")
            .removeAttr("disabled");
    }
});
$(document).on("change", ".add_currently_pursuing", function() {
    if ($(this).is(":checked")) {
        var currentlyPersuingId = $("#is_already_persuing_id").val();
        $(this)
            .closest(".addEducation")
            .find(".education_duration_to")
            .attr("disabled", true);
        if (currentlyPersuingId != "") {
            toastr.error(
                "You are not allowed to select this option. Because you are currently persuing in some other institute."
            );
            $(this).prop("checked", false);
        }
    } else {
        $(this)
            .closest(".addEducation")
            .find(".education_duration_to")
            .removeAttr("disabled");
    }
});
/*----Edit Tabular Data----*/
$(".edit-tabular-data").click(function() {
    var data_id = $(this).attr("data-id");
    var data_type = $(this).attr("data-type");
    $this = $(this);
    $.ajax({
        type: "POST",
        url: getTabularData,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: data_id,
            data_type: data_type
        },
        success: function(data) {
            if (data.status == 200) {
                if (data.data_type == "experience") {
                    $this.closest(".experience-edit-block").replaceWith(data.html);
                } else if (data.data_type == "education") {
                    $this.closest(".education-edit-block").replaceWith(data.html);
                } else {
                    $this.closest(".refernce-edit-block").replaceWith(data.html);
                }
            } else {
                if (data.data_type == "experience") {
                    toastr.error(
                        "Experience data has been synced. Please try again to update"
                    );
                } else if (data.data_type == "education") {
                    toastr.error(
                        "Education data has been synced. Please try again to update"
                    );
                } else {
                    toastr.error(
                        "Reference data has been synced. Please try again to update"
                    );
                }
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }
        }
    });
});
// Edit Experience
$(document).on("change", ".is_currently_working_here", function() {
    if ($(this).is(":checked")) {
        var currentlyWorkingExperienceId = $(
            "#is_already_working_experience_id"
        ).val();
        var editExperienceId = $(this)
            .closest("li")
            .find(".updatebutton-consultantExperience")
            .attr("data-id");
        if (
            editExperienceId == currentlyWorkingExperienceId ||
            currentlyWorkingExperienceId == ""
        ) {
            $(this)
                .closest("li")
                .find(".work_duration_to")
                .attr("disabled", true);
        } else {
            toastr.error(
                "You are not allowed to select this option. Because you are currently working on some other company."
            );
            $(this).prop("checked", false);
        }
    } else {
        $(this)
            .closest("li")
            .find(".work_duration_to")
            .removeAttr("disabled");
    }
});
$(document).on("change", ".currently_pursuing", function() {
    if ($(this).is(":checked")) {
        var currentlyPersuingId = $("#is_already_working_experience_id").val();
        if (currentlyPersuingId == "") {
            $(this)
                .closest("li")
                .find(".education_duration_to")
                .attr("disabled", true);
        } else {
            toastr.error(
                "You are not allowed to select this option. Because you are currently persuing in some other institute."
            );
            $(this).prop("checked", false);
        }
    } else {
        $(this)
            .closest("li")
            .find(".education_duration_to")
            .removeAttr("disabled");
    }
});
$(document).on("click", ".updatebutton-consultantEducation", function() {
    var id = $(this).attr("data-id");
    var institute = $(this)
        .closest("li")
        .find(".edited_educationinstitute")
        .val();
    var department = $(this)
        .closest("li")
        .find(".edited_educationdepartment")
        .val();
    var degree = $(this)
        .closest("li")
        .find(".edited_educationdegree")
        .val();
    var duration_from_month = $(this)
        .closest("li")
        .find(".education_duration_from_month")
        .val();
    var duration_from_year = $(this)
        .closest("li")
        .find(".education_duration_from_year")
        .val();
    var duration_to_month = $(this)
        .closest("li")
        .find("#education_duration_to_month")
        .val();
    var duration_to_year = $(this)
        .closest("li")
        .find("#education_duration_to_year")
        .val();
    if (
        $(this)
        .closest("li")
        .find(".currently_pursuing")
        .is(":checked")
    ) {
        var is_currently_pursuing = "true";
    } else {
        var is_currently_pursuing = "false";
    }
    if ($.trim(institute) == "") {
        return bootbox.alert("Please enter your institute name");
    }
    if ($.trim(department) == "") {
        return bootbox.alert("Please enter your department name");
    }
    if ($.trim(degree) == "") {
        return bootbox.alert("Please enter your degree");
    }
    if (duration_from_month == "" || duration_from_year == "") {
        return bootbox.alert("Please select From duration");
    }
    if (is_currently_pursuing == "false") {
        if (duration_to_month == "" || duration_to_year == "") {
            return bootbox.alert("Please select To duration");
        }
        if (parseInt(duration_to_year) < parseInt(duration_from_year)) {
            return bootbox.alert(
                "Duration To year must be greater than work duration From year"
            );
        } else {
            if (
                parseInt(duration_to_year) == parseInt(duration_from_year) &&
                parseInt(duration_to_month) < parseInt(duration_from_month)
            ) {
                return bootbox.alert(
                    "Duration To month must be greater than duration From month"
                );
            }
        }
    }
    $loader = $(document).find(".education-loader");
    $loader.show();
    $.ajax({
        type: "POST",
        url: updateProfileEducation,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: id,
            institute: institute,
            department: department,
            degree: degree,
            duration_from_month: duration_from_month,
            duration_from_year: duration_from_year,
            duration_to_month: duration_to_month,
            duration_to_year: duration_to_year,
            is_currently_pursuing: is_currently_pursuing
        },
        success: function(data) {
            $loader.hide();
            if (data.status == 200) {
                toastr.success("Education has been saved successfully");
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr.error("Profile education could not save");
            }
        }
    });
});
/*----End edit consultant Education----*/

$(document).on("click", ".savebutton-consultantExperience", function() {
    var company = $(document)
        .find(".addExperience .company")
        .val();
    var industry = $(document)
        .find(".addExperience .industry")
        .val();
    var occupation = $(document)
        .find(".addExperience .occupation")
        .val();
    var summary = $(document)
        .find(".addExperience .edited_experiencesummary")
        .val();
    var duration_from_month = $(document)
        .find(".addExperience .work_duration_from_month")
        .val();
    var duration_from_year = $(document)
        .find(".addExperience .work_duration_from_year")
        .val();
    var duration_to_month = $(document)
        .find(".addExperience #work_duration_to_month")
        .val();
    var duration_to_year = $(document)
        .find(".addExperience #work_duration_to_year")
        .val();
    if (
        $(this)
        .closest(".addExperience")
        .find(".add_is_currently_working_here")
        .is(":checked")
    ) {
        var currently_working_here = "true";
    } else {
        var currently_working_here = "false";
    }
    if ($.trim(company) == "") {
        return bootbox.alert("Please enter your company name");
    }
    if ($.trim(occupation) == "") {
        return bootbox.alert("Please enter your occupation");
    }
    if ($.trim(industry) == "") {
        return bootbox.alert("Please enter your industry");
    }
    if (duration_from_month == "" || duration_from_year == "") {
        return bootbox.alert("Please select From duration");
    }
    if (currently_working_here == "false") {
        if (duration_to_month == "" || duration_to_year == "") {
            return bootbox.alert("Please select To work duration");
        }
        if (parseInt(duration_to_year) < parseInt(duration_from_year)) {
            return bootbox.alert(
                "Work duration To year must be greater than work duration From year"
            );
        } else {
            if (
                parseInt(duration_to_year) == parseInt(duration_from_year) &&
                parseInt(duration_to_month) < parseInt(duration_from_month)
            ) {
                return bootbox.alert(
                    "Work duration To month must be greater than work duration From month"
                );
            }
        }
    }
    if ($.trim(summary) == "") {
        return bootbox.alert("Please enter your summary");
    }
    $loader = $(document).find(".experience-loader");
    $loader.show();
    $.ajax({
        type: "POST",
        url: addProfileExprirence,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            company: company,
            industry: industry,
            occupation: occupation,
            summary: summary,
            duration_from_month: duration_from_month,
            duration_from_year: duration_from_year,
            duration_to_month: duration_to_month,
            duration_to_year: duration_to_year,
            currently_working_here: currently_working_here
        },
        success: function(data) {
            $loader.hide();
            if (data.status == 200) {
                toastr.success("Experience has been saved successfully");
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr.error("Profile experience could not save");
            }
        }
    });
});

/*----end add consultant Experience---*/

$(document).on("click", ".updatebutton-consultantExperience", function() {
    var id = $(this).attr("data-id");
    var company = $(this)
        .closest("li")
        .find(".edited_experiencecompany")
        .val();
    var occupation = $(this)
        .closest("li")
        .find(".edited_experienceoccupation")
        .val();
    var industry = $(this)
        .closest("li")
        .find(".edited_experienceindustry")
        .val();
    var work_duration_from_month = $(this)
        .closest("li")
        .find(".work_duration_from_month")
        .val();
    var work_duration_from_year = $(this)
        .closest("li")
        .find(".work_duration_from_year")
        .val();
    var work_duration_to_month = $(this)
        .closest("li")
        .find("#work_duration_to_month")
        .val();
    var work_duration_to_year = $(this)
        .closest("li")
        .find("#work_duration_to_year")
        .val();
    var summary = $(this)
        .closest("li")
        .find(".edited_experiencesummary")
        .val();
    if (
        $(this)
        .closest("li")
        .find(".is_currently_working_here")
        .is(":checked")
    ) {
        var isCurrentlyWorkHere = "true";
    } else {
        var isCurrentlyWorkHere = "false";
    }
    if ($.trim(company) == "") {
        return bootbox.alert("Please enter your company name");
    }
    if ($.trim(occupation) == "") {
        return bootbox.alert("Please enter your occupation");
    }
    if ($.trim(industry) == "") {
        return bootbox.alert("Please enter your industry");
    }
    if (work_duration_from_month == "" || work_duration_from_year == "") {
        return bootbox.alert("Please select From work duration");
    }
    if (isCurrentlyWorkHere == "false") {
        if (work_duration_to_month == "" || work_duration_to_year == "") {
            return bootbox.alert("Please select To work duration");
        }
        if (parseInt(work_duration_to_year) < parseInt(work_duration_from_year)) {
            return bootbox.alert(
                "Work duration To year must be greater than work duration From year"
            );
        } else {
            if (
                parseInt(work_duration_to_year) == parseInt(work_duration_from_year) &&
                parseInt(work_duration_to_month) < parseInt(work_duration_from_month)
            ) {
                return bootbox.alert(
                    "Work duration To month must be greater than work duration From month"
                );
            }
        }
    }
    if ($.trim(summary) == "") {
        return bootbox.alert("Please enter your summary");
    }
    $loader = $(document).find(".experience-loader");
    $loader.show();
    $.ajax({
        type: "POST",
        url: updateProfileExprirence,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: id,
            company: company,
            occupation: occupation,
            industry: industry,
            work_duration_from_month: work_duration_from_month,
            work_duration_from_year: work_duration_from_year,
            work_duration_to_month: work_duration_to_month,
            work_duration_to_year: work_duration_to_year,
            is_curretly_work_here: isCurrentlyWorkHere,
            summary: summary
        },
        success: function(data) {
            $loader.hide();
            if (data.status == 200) {
                toastr.success("Experience has been saved successfully");
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr.error("Profile experience could not save");
            }
        }
    });
});
/*----End edit consultant Experience----*/

/*----Edit consutant summary---*/
$("#edit-summary-consutant").click(function() {
    var summary = $(document)
        .find(".edit-summary-value")
        .attr("data-summary");

    $(".edit-summary-value").replaceWith(
        '<div class="form-group"><textarea class="edit_conSummary" name="edit_conSummary">' +
        summary +
        "</textarea></div>"
    );
    //    $(document).find(".edit_conSummary").summernote({
    //        toolbar: [
    //            ['style', ['style']],
    //            ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
    //            ['fontname', ['fontname']],
    //            ['fontsize', ['fontsize']],
    //            ['para', ['ol', 'ul', 'paragraph', 'height']],
    //            ['table', ['table']],
    //            ['view', ['undo', 'redo', 'codeview', 'help']]
    //        ]
    //    });
    // $("#client-address-display").hide();
    $("#edit-summary-consutant").hide();
    $("#consultant-summary-save").show();
});

$(document).on("click", ".savebutton-consultantsummary", function() {
    var id = $(this).attr("data-id");
    var summary = $(".edit_conSummary").val();

    if (summary == "") {
        return bootbox.alert("Please enter your summary");
    }
    $(".profile-summary-loader").show();
    $.ajax({
        type: "POST",
        url: consultanteditprofilesummary,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            additional_info: summary
        },
        success: function(data) {
            if (data.status == 200) {
                $(".profile-summary-loader").hide();
                location.reload();
            } else {
                toastr.error("Profile summary could not save");
            }
        }
    });
});

$(".max-character-fix a").text(function(index, currentText) {
    var myStr = $(this).text();

    var trimStr = $.trim(myStr);
    console.log(trimStr);
    if (trimStr.length >= 19) {
        return trimStr.substr(0, 19) + "...";
    } else {
        return trimStr;
    }
});
// $(".cat-chareecter-limit").text(function(index, currentText) {
//   var myStr = $(this).text();

//   var trimStr = $.trim(myStr);
//   console.log(trimStr);
//   if (trimStr.length >= 4) {
//     return trimStr.substr(0, 4) + "..";
//   } else {
//     return trimStr;
//   }
// });

/*-----End Edit Consultant sumary--*/

$(".add-to-celender").click(function() {
    $("#add-to-calender-modal").modal("show");
    var title = $(this).attr("data-title");
    var date = $(this).attr("data-date");
    var description = $(this).attr("data-description");
    var location = $(this).attr("data-location");

    var google =
        "https://www.google.com/calendar/render?action=TEMPLATE&text=" +
        title +
        "&dates=" +
        date +
        "/" +
        date +
        "&details=" +
        description +
        "&location=" +
        location +
        "&sprop=&sprop=name";
    var yahoo =
        "http://calendar.yahoo.com/?v=60&view=d&type=20&title=" +
        title +
        "&st=" +
        date +
        "&&desc=" +
        description +
        "&in_loc=" +
        location;
    var ical =
        "data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0ADTSTART:" +
        date +
        "%0ADTEND:" +
        date +
        "%0ASUMMARY:" +
        title +
        "%0ADESCRIPTION:" +
        description +
        "%0ALOCATION:" +
        location +
        "%0AEND:VEVENT%0AEND:VCALENDAR";

    $("#google-link").prop("href", google);
    $("#yahoo-link").prop("href", yahoo);
    $("#ical-link").prop("href", ical);
});

$("#update-profileeducation #duration_from").datepicker({
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});
$("#update-profileeducation #duration_to").datepicker({
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});
$("#search-form .subfilter").change(function() {
    $(".boxTabsearch")
        .find(".select2")
        .val("");
    $(".boxTabsearch")
        .find("input[type=text]")
        .val("");
    $(".boxTabsearch")
        .find("input[type=checkbox]")
        .attr("checked", false);
    // $("#search-form").find(".category-options input,#baserate_from, #baserate_to,select[name='experience'],.work-preference-options input,.consultant-state-options input,select[name='availability_date'],#job_baserate_from, #job_baserate_to,.job-mode1-options input,.job-type1-options input,select[name='work_experience'],select[name='job_start_date'],select[name='job_duration'],select[name='job_industry'],.job-state-options input,.job-category-options input").val("");
    $("#search-form").submit();
});

/***For mobile on click change  ***/
$("#mobile-search-form .subfilter").change(function() {
    $(".boxTabsearch")
        .find(".select2")
        .val("");
    $(".boxTabsearch")
        .find("input[type=text]")
        .val("");
    $(".boxTabsearch")
        .find("input[type=checkbox]")
        .attr("checked", false);
    $("#mobile-search-form").submit();
});
$(document).on("click", ".close-consutant-state", function(event) {
    $(".consultant-state-options").hide(100);
});
$(document).on("click", ".close-job-state", function(event) {
    $(".job-state-options").hide(100);
});
$(document).on("click", ".clear-filter", function(event) {
    $("#search-form")[0].reset();
    $("#search-form")
        .find(".select2")
        .val("")
        .trigger("change");
    $("#search-form")
        .find(".baserate_number")
        .val("");
    $("#search-form")
        .find("input[type=checkbox]")
        .prop("checked", false); // Unchecks it
    $("#search-form").submit();
});

$("#search-consultants-tab .view-all").click(function() {
    $("#search-form")[0].reset();
    $("#search-form").submit();
});
// $("#clear-filter").click(function() {
//   $("#search-form")[0].reset();
//   $("#search-form")
//     .find(".select2")
//     .val("")
//     .trigger("change");
//   $("#search-form")
//     .find(".baserate_number")
//     .val("");
//   $("#search-form")
//     .find("input[type=checkbox]")
//     .prop("checked", false); // Unchecks it
//   $("#search-form").submit();
// });

$("#job_description")
    .summernote({
        toolbar: [
            ["style", ["style"]],
            [
                "font", [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "superscript",
                    "subscript",
                    "clear"
                ]
            ],
            ["fontname", ["fontname"]],
            ["fontsize", ["fontsize"]],
            ["para", ["ol", "ul", "paragraph", "height"]],
            ["table", ["table"]],
            ["view", ["undo", "redo", "codeview", "help"]]
        ]
    })
    .on("summernote.change", function(customEvent, contents, $editable) {
        // Revalidate the content when its value is changed by Summernote
        $("#client-job-post").bootstrapValidator(
            "revalidateField",
            "job_description"
        );
    });
$("#job_requirements")
    .summernote({
        toolbar: [
            ["style", ["style"]],
            [
                "font", [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "superscript",
                    "subscript",
                    "clear"
                ]
            ],
            ["fontname", ["fontname"]],
            ["fontsize", ["fontsize"]],
            ["para", ["ol", "ul", "paragraph", "height"]],
            ["table", ["table"]],
            ["view", ["undo", "redo", "codeview", "help"]]
        ]
    })
    .on("summernote.change", function(customEvent, contents, $editable) {
        // Revalidate the content when its value is changed by Summernote
        $("#client-job-post").bootstrapValidator("revalidateField", "requirements");
    });
$("#job_benefits")
    .summernote({
        toolbar: [
            ["style", ["style"]],
            [
                "font", [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "superscript",
                    "subscript",
                    "clear"
                ]
            ],
            ["fontname", ["fontname"]],
            ["fontsize", ["fontsize"]],
            ["para", ["ol", "ul", "paragraph", "height"]],
            ["table", ["table"]],
            ["view", ["undo", "redo", "codeview", "help"]]
        ]
    })
    .on("summernote.change", function(customEvent, contents, $editable) {
        // Revalidate the content when its value is changed by Summernote
        $("#client-job-post").bootstrapValidator("revalidateField", "benefits");
    });
/*----client edit profile --*/

$(document).on("click", "#edit-client-profile", function(event) {
    $.ajax({
        type: "POST",
        url: getContactProfile,
        cache: false,
        data: {
            _token: $("input[name=_token]").val()
        },
        success: function(data) {
            if (data.status == 200) {
                $("#profile-data").empty();
                $("#profile-data").append(data.html);
            } else {
                toastr.error(data.message);
            }
        }
    });
});
$(document).on("change", ".contact-profile-image-upload", function(event) {
    $this = this;
    myfile = $(this).val();
    var ext = myfile.split(".").pop();
    if (ext == "jpeg" || ext == "png" || ext == "jpg") {} else {
        bootbox.alert(
            "Image upload will support file format .jpg, .png and .Jpeg onlys"
        );
        return false;
    }
    var form_data = new FormData();
    // console.log(form_data);
    if ($(this).prop("files").length > 0) {
        file = $(this).prop("files")[0];
        form_data.append("profile_pic", file);
    }
    $.ajax({
        type: "POST",
        url: uploadContactProfilePicture, // point to server-side PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        success: function(response) {
            // alert(response);
            $(".loadertest").removeClass("active");
            $(".cv-consult-sucess").addClass("active");
        }
    });
});
$(document).on("click", ".contact-profile-ajax", function(event) {
    event.preventDefault();

    var firstname = $('#contact-profile-form input[name="firstname"]').val();
    var lastname = $('#contact-profile-form input[name="lastname"]').val();
    var title = $('#contact-profile-form input[name="title"]').val();
    var department = $('#contact-profile-form input[name="department"]').val();
    var mailing_street = $('#contact-profile-form input[name="mailing_street"]').val();
    var mailing_state = $('#contact-profile-form input[name="mailing_state"]').val();
    var mailing_zip = $('#contact-profile-form input[name="mailing_zip"]').val();
    var mailing_country = $('#contact-profile-form input[name="mailing_country"]').val();
    var mailing_city = $(
        '#contact-profile-form input[name="mailing_city"]'
    ).val();
    var mobile = $('#contact-profile-form input[name="mobile"]').val();

    $.ajax({
        type: "POST",
        url: viewContactProfile,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            firstname: firstname,
            lastname: lastname,
            title: title,
            department: department,
            mailing_city: mailing_city,
            mobile: mobile,
            mailing_street: mailing_street,
            mailing_state: mailing_state,
            mailing_zip: mailing_zip,
            mailing_country: mailing_country

            // "form_data": form_data,
        },
        // data: form_data,
        success: function(data) {
            if (data.status == 200) {
                $("#profile-data").empty();
                $("#profile-data").append(data.html);
                toastr.success("Profile has been updated successfully.");
                location.reload(true);
            } else {
                toastr.error(
                    "Profile not updated somthing wrong please try agin after some times."
                );
            }
        }
    });
});

$(document).on("click", ".cancel-contact-profile", function(event) {
    event.preventDefault();
    var requestCancel = "true";
    $.ajax({
        type: "POST",
        url: viewContactProfile,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            requestCancel: requestCancel
        },
        // data: form_data,
        success: function(data) {
            if (data.status == 200) {
                $("#profile-data").empty();
                $("#profile-data").append(data.html);
                toastr.error("Profile not saved.");
            } else {
                toastr.error("Profile not save.");
            }
        }
    });
});

$(document).on("click", ".savebutton-client", function() {
    var clientName = $(document)
        .find(".edited_clientname")
        .val();
    var phoneNumber = $(document)
        .find(".edited_phone")
        .val();

    var billing_code = $(document)
        .find(".billing_code")
        .val();
    var billing_street = $(document)
        .find(".billing_street")
        .val();
    var billing_city = $(document)
        .find(".billing_city")
        .val();
    var billing_state = $(document)
        .find(".billing_state")
        .val();
    var billing_country = $(document)
        .find(".billing_country")
        .val();
    if (clientName == "") {
        return bootbox.alert("Please enter client name");
    }
    if (phoneNumber == "") {
        return bootbox.alert("Please enter contact number");
    }
    $.ajax({
        type: "POST",
        url: clientUpdateProfile,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            clientName: clientName,
            phoneNumber: phoneNumber,
            billing_code: billing_code,
            billing_street: billing_street,
            billing_city: billing_city,
            billing_state: billing_state,
            billing_country: billing_country
        },
        success: function(data) {
            if (data.status == 200) {
                location.reload();
            } else {
                toastr.error("Profile could not save");
            }
        }
    });
});

$(function() {
    $("input[class*='baserate_number']").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if (
            (event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 ||
            event.keyCode == 9 ||
            event.keyCode == 37 ||
            event.keyCode == 39 ||
            event.keyCode == 46 ||
            event.keyCode == 190
        ) {} else {
            event.preventDefault();
        }

        if (
            $(this)
            .val()
            .indexOf(".") !== -1 &&
            event.keyCode == 190
        )
            event.preventDefault();
    });
});
// post a job cal budget in dashboard
$("#job-budget").on("keyup, input", function() {
    var baserate = parseFloat($(this).val() / 1.09).toFixed(2);
    $(".job-calculated-baserate").text("MYR " + baserate);
    $(".job-calculated-baserate-input").val(baserate);
});
// Consultant Category Top
$(".category-search1").click(function() {
    $(".category-options").toggle();
});
// // Consultant Category Bottom
// $(".category-search2").click(function() {
//   $(".category-options2").toggle();
// });
// // Job Category Top
$(".job-category-search1").click(function() {
    $(".job-category-options").toggle();
});
// //Consultant Base Rate Top
$(".baserate-1").click(function() {
    $(".baserate-1-options").toggle();
});
// //Consultant Base Rate Bottom
// $(".baserate-2-lable").click(function() {
//   $(".baserate-2-options").toggle();
// });
// //Job Base Rate Top
$(".job-baserate-1").click(function() {
    $(".job-baserate-1-options").toggle();
});
// //Job Base Rate Bottom
// $(".job-baserate-2-lable").click(function() {
//   $(".job-baserate-2-options").toggle();
// });
// // Work Preference Top
$("#work-preference-lable").click(function() {
    $(".work-preference-options").toggle();
});
// // Consultant State Top
$(".consultant-state-lable").click(function() {
    $(".consultant-state-options").toggle();
});
// // consultant mobile state
// $("#mobile-consultant-state-lable").click(function () {
//     $(".mobile-consultant-state-options").toggle();
// });
// Job State Top
$(".job-state-lable").click(function() {
    $(".job-state-options").toggle();
});
// // Job Mode Top
$("#job-mode1-lable").click(function() {
    $(".job-mode1-options").toggle();
});
// // Category Bottom
// $(".category-search2").click(function() {
//   $(".category2-options").toggle();
// });
// // Job Mode Bottom
// $(".job-mode2-lable").click(function() {
//   $(".job-mode2-options").toggle();
// });
// // Job Type Bottom
// $(".job-type2-lable").click(function() {
//   $(".job-type2-options").toggle();
// });
// // Job Type Top
$(".job-type1-lable").click(function() {
    $(".job-type1-options").toggle();
});
// // Job State Bottom
// $(".job-state2-lable").click(function() {
//   $(".job-state2-options").toggle();
// });
// // Hide toogle div on outside click
$(document).mouseup(function(e) {
    var container = $(".toogle-div");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
    }
});
$(".submit-search").click(function(e) {
    e.preventDefault();
    var isFromValid = validateFrom();
    if (isFromValid == true) {
        $("#search-form").submit();
    }
});
$(".mobilesubmit-search").click(function(e) {
    e.preventDefault();
    var isFromValid = validateFrom();
    if (isFromValid == true) {
        $("#mobile-search-form").submit();
    }
});

function validateFrom() {
    var isValid = true;
    if ($("#baserate_from").val() != "" && $("#baserate_to").val() != "") {
        if (
            parseFloat($("#baserate_from").val()) >
            parseFloat($("#baserate_to").val())
        ) {
            toastr.error("Base rate from must be lower than base rate to");
            isValid = false;
        }
    }
    if ($("#baserate2_from").val() != "" && $("#baserate2_to").val() != "") {
        if (
            parseFloat($("#baserate2_from").val()) >
            parseFloat($("#baserate2_to").val())
        ) {
            toastr.error("Base rate from must be lower than base rate to");
            isValid = false;
        }
    }
    if (
        $("#job_baserate_from").val() != "" &&
        $("#job_baserate_to").val() != ""
    ) {
        if (
            parseFloat($("#job_baserate_from").val()) >
            parseFloat($("#job_baserate_to").val())
        ) {
            toastr.error("Base rate from must be lower than base rate to");
            isValid = false;
        }
    }
    if (
        $("#job_baserate2_from").val() != "" &&
        $("#job_baserate2_to").val() != ""
    ) {
        if (
            parseFloat($("#job_baserate2_from").val()) >
            parseFloat($("#job_baserate2_to").val())
        ) {
            toastr.error("Base rate from must be lower than base rate to");
            isValid = false;
        }
    }
    return isValid;
}

$(document).on("click", ".delete-experienceBtn", function() {

    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to remove this experience details?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});

$(document).on("click", ".delete-educationBtn", function() {
    // e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to remove this education details?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});
$(".delete-referenceBtn").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to remove this reference details?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});
$("#setting-submit").click(function() {
    if ($(".jobalerts_tags").is(":visible")) {
        if ($(".job_alerts_tags:checked").length == 0) {
            bootbox.alert("Please select atleast one Job Tag.");
            return false;
        } else {
            $("#notification-settings").submit();
        }
    } else {
        $("#notification-settings").submit();
    }
});
$(".copy-to-clipboard").click(function() {
    var id = $(this)
        .closest(".commanContent")
        .find(".c-job-base-rate")
        .attr("id");
    var baserate = copyToClipboard("#" + id);
    toastr.success("Billing Rate/day copied successfully");
    $("#c1-base-rate-perday")
        .val(baserate)
        .trigger("keyup");
    $("#c3-base-rate-perday")
        .val(baserate)
        .trigger("keyup");
    $("html, body").animate({ scrollTop: $(".boxHowmuch").offset().top - 300 },
        1000
    );
});

function copyToClipboard(element) {
    var baserate = 0;
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    baserate = $temp.val();
    document.execCommand("copy");
    $temp.remove();
    return baserate;
}
$(".consultantsTab").click(function() {
    $('.boxTabsearch .nav-link[aria-controls="search-consultants-tab"]').trigger(
        "click"
    );
    $("html, body").animate({ scrollTop: 0 }, "slow");
    // $('html, body').animate({
    //     scrollTop: $("#search-job-consultant").offset().top
    // }, 1600);
});
$(".jobsTab").click(function() {
    // $(".search-jobs").trigger("click");
    $('.boxTabsearch .nav-link[aria-controls="search-jobs-tab"]').trigger(
        "click"
    );
    $("html, body").animate({ scrollTop: 0 }, "slow");
    // $('html, body').animate({
    //     scrollTop: $("#downword-search").offset().top
    // }, 1600);
});
$(".consultantTab").click(function() {
    $('.boxTabsearch .nav-link[aria-controls="search-consultants-tab"]').trigger(
        "click"
    );
    $("html, body").animate({ scrollTop: 0 }, "slow");
});
$(".interview-note-link").click(function() {
    $this = $(this);
    var interview_id = $(this).attr("data-id");
    var continer_id = $(this).attr("data-container-id");
    var continer_module = $(this).attr("data-container-module");
    // $this.hide();
    $this.next(".interview-loader").show();
    $.ajax({
        type: "POST",
        url: getInterviewNotes,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            interview_id: interview_id,
            continer_id: continer_id
        },
        success: function(data) {
            $this.next(".interview-loader").hide();

            if (data.status == 200) {
                $this.hide();
                $("#notes-container-up-" + continer_module + "-" + continer_id).empty();
                $("#notes-container-up-" + continer_module + "-" + continer_id).append(
                    data.html
                );
                toastr.success("Notes found successfully");
            } else {
                $this.show();
                toastr.error("Oops! No notes found for this interview");
            }
        }
    });
});
$(document).on("click", ".close-note", function() {
    $(this)
        .closest(".notes-container")
        .empty();
    toastr.success("Note closed successfully");
});
$(".read-notification").click(function() {
    $.ajax({
        type: "POST",
        url: readNotification,
        cache: false,
        data: {},
        success: function(data) {
            $(".read-notification small").remove();
        }
    });
});
// Submit Search Form On Enter
$("#search-form input[name=jobsearchterm]").keypress(function(event) {
    if (event.which == 13) {
        $(".submit-search").trigger("click");
    }
});
// Submit Search Form On Enter
$("#search-form input[name=searchterm]").keypress(function(event) {
    if (event.which == 13) {
        $(".submit-search").trigger("click");
    }
});
// mobile Submit Search Form On Enter
$("#mobile-search-form input[name=jobsearchterm]").keypress(function(event) {
    if (event.which == 13) {
        $(".mobilesubmit-search").trigger("click");
    }
});
// mobile Submit Search Form On Enter
$("#mobile-search-form input[name=searchterm]").keypress(function(event) {
    if (event.which == 13) {
        $(".mobilesubmit-search").trigger("click");
    }
});
$(".baserate_number").keyup(function() {
    var $this = $(this);
    $this.val($this.val().replace(/[^\d.]/g, ""));
});
$(document).on("click", "#remove-consultant-profile-pic", function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to remove profile picture?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "POST",
                    url: removeProfilePic,
                    cache: false,
                    data: {},
                    success: function(data) {
                        if (data.status == 200) {
                            $("#profile_image").remove();
                            $("#remove-consultant-profile-pic").remove();
                            $("#sidebar-profile-picture").remove();
                            $("#sidebar-profile-default").css("display", "block");
                            $(".hidden_profile_pic").css("display", "block");
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    }
                });
            }
        }
    });
});

$('input[type="file"]#cv').change(function(e) {
    var fileName = e.target.files[0].name;
    $(".in-html-append").html(fileName);
    // alert('The file "' + fileName +  '" has been selected.');
});
$(document).on("click", "#remove-contact-profile-pic", function(e) {
    e.preventDefault();
    return bootbox.confirm({
        message: "Are you sure you want to remove profile picture?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn btn-secondary btn-md"
            },
            cancel: {
                label: "No",
                className: "btn btn-danger btn-md"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "POST",
                    url: removeContactProfilePic,
                    cache: false,
                    data: { _token: $("input[name=_token]").val() },
                    success: function(data) {
                        if (data.status == 200) {
                            $("#profile_image").remove();
                            $("#remove-contact-profile-pic").remove();
                            $("#sidebar-profile-picture").remove();
                            $("#sidebar-profile-default").css("display", "block");
                            $(".hidden_profile_pic").css("display", "block");
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    }
                });
            }
        }
    });
});

// $(".search-consulresult").keyup(function () {
//     var search = $(this).val();
//     if (search != "") {
//         var _token = $('input[name="_token"]').val();
//         $.ajax({
//             url: getSearchConsultant,
//             method: "POST",
//             data: {search: search, _token: _token},
//             success: function (data) {
//                 //   console.log(data);
//                 if (data.status == 200) {
//                     $(".consutant-suggestions").fadeIn();
//                     $(".consutant-suggestions").html(data.output);
//                 }
//                 if (data.status == 400) {
//                     $(".consutant-suggestions").fadeOut();
//                 }
//             }
//         });
//     }
// });

$(document).on("click", "body", function() {
    $(".consutant-suggestions").fadeOut();
    $(".job-suggestions").fadeOut();
});
$(document).on("keydown", function(e) {
    if (e.keyCode === 27) {
        // ESC
        $(".consutant-suggestions").fadeOut();
        $(".job-suggestions").fadeOut();
    }
});
$(document).on("click", ".consutant-suggestions ul li", function() {
    $(".search-consulresult").val($(this).text());
    $(".consutant-suggestions").fadeOut();
});

// $(".search-jobresult").keyup(function() {
//   var search = $(this).val();
//   if (search != "") {
//     var _token = $('input[name="_token"]').val();
//     $.ajax({
//       url: getJobConsultant,
//       method: "POST",
//       data: { search: search, _token: _token },
//       success: function(data) {
//         //   console.log(data);
//         if (data.status == 200) {
//           $(".job-suggestions").fadeIn();
//           $(".job-suggestions").html(data.output);
//         }
//         if (data.status == 400) {
//           $(".job-suggestions").fadeOut();
//         }
//       }
//     });
//   }
// });

// $( ".search-jobresult" ).autocomplete({
//   source: availableTags
// });
$(".search-jobresult").autocomplete({
    source: function(request, response) {
        $.ajax({
            url: getJobConsultant,
            // dataType: "json",
            data: {
                search: request.term
            },
            success: function(data) {
                $data = JSON.parse(data.output);
                response($data);
            }
        });
    },
    minLength: 2
});

$(".search-consulresult").autocomplete({
    source: function(request, response) {
        $.ajax({
            url: getSearchConsultant,
            // dataType: "json",
            data: {
                search: request.term
            },
            success: function(data) {
                $data = JSON.parse(data.output);
                response($data);
            }
        });
    },
    minLength: 2
});

$(document).on("click", ".job-suggestions ul li", function() {
    $(".search-jobresult").val($(this).text());
    $(".job-suggestions").fadeOut();
});

// $(document).on('mouseover', '.iam-hover-effect', function () {
//     $('.page-cons-cli').toggle(200);
// });
// $(document).on('mouseout', '.iam-hover-effect', function () {
//     $('.page-cons-cli').toggle(200);
// });
$(".cons-bottom-form").click(function() {
    $("html, body").animate({ scrollTop: $("#consultant-registration").offset().top - 70 },
        1000
    );
});
$(".client-bottom-form").click(function() {
    $("html, body").animate({ scrollTop: $("#consultant-registration").offset().top - 70 },
        1000
    );
});
$(document).on("click", ".attachment-dropdown", function() {
    // $('.attachments-links').hide(200);
    $(this)
        .closest("td")
        .find(".attachments-links")
        .toggle(200);
});

$(document).on("click", ".dropdown-link-show", function() {
    // $('.attachments-links').hide(200);
    $(this)
        .closest(".tabular-data-list")
        .find(".dropdonw-link-icon")
        .toggle(200);
});
$(document).on("click", ".exprinces-show", function() {
    // $(this).hide();
    $(this)
        .closest(".list-notification-style")
        .find(".hidden-tabular")
        .show(function() {
            $(".exprinces-show").hide();
            $(".exprinces-hide").show();
        });
    // $('.hidden-tabular').toggle(200);
});
$(document).on("click", ".exprinces-hide", function() {
    // $(this).hide();
    $(this)
        .closest(".list-notification-style")
        .find(".hidden-tabular")
        .hide(function() {
            $(".exprinces-show").show();
            $(".exprinces-hide").hide();
        });
    // $('.hidden-tabular').toggle(200);
});
$(document).on("click", ".education-show", function() {
    // $(this).hide();
    $(this)
        .closest(".list-notification-style")
        .find(".hidden-tabular")
        .show(function() {
            $(".education-show").hide();
            $(".education-hide").show();
        });
    // $('.hidden-tabular').toggle(200);
});
$(document).on("click", ".education-hide", function() {
    // $(this).hide();
    $(this)
        .closest(".list-notification-style")
        .find(".hidden-tabular")
        .hide(function() {
            $(".education-show").show();
            $(".education-hide").hide();
        });
    // $('.hidden-tabular').toggle(200);
});
$(document).on("click", ".reference-show", function() {
    // $(this).hide();
    $(this)
        .closest(".list-notification-style")
        .find(".hidden-tabular")
        .show(function() {
            $(".reference-show").hide();
            $(".reference-hide").show();
        });
    // $('.hidden-tabular').toggle(200);
});
$(document).on("click", ".reference-hide", function() {
    // $(this).hide();
    $(this)
        .closest(".list-notification-style")
        .find(".hidden-tabular")
        .hide(function() {
            $(".reference-show").show();
            $(".reference-hide").hide();
        });
    // $('.hidden-tabular').toggle(200);
});
$(document).on("click", "body", function() {
    $(".login-form-toggle").fadeOut(200);
});
$(document).on("keydown", function(e) {
    if (e.keyCode === 27) {
        // ESC
        $(".login-form-toggle").fadeOut(200);
        $(".generate-password-form-block").fadeOut(200);
    }
});
$(document).on("click", "body", function() {
    $(".generate-password-form-block").fadeOut(200);
});
$(document).on("click", ".login-form-toggle", function(event) {
    event.stopPropagation();
});
$(document).on("click", ".generate-password-form-block form", function(event) {
    event.stopPropagation();
});
$(document).on("click", ".login-form-poup", function(event) {
    // $('.attachments-links').hide(200);
    $("html, body").animate({ scrollTop: 0 }, "slow");
    event.stopPropagation();
    $(".forgot-form-block").hide();
    $(".login-form-toggle").toggle(200);
    $(".login-form-block").show(200);
});
$(document).on("click", ".forgot-form-show", function() {
    $(".login-form-block").toggle(200);
    $(".forgot-form-block").toggle(200);
});
$(document).on("click", ".login-form-show", function() {
    $(".login-form-block").toggle(200);
    $(".forgot-form-block").toggle(200);
});
$(document).on("click", '.job-viewMOre[aria-expanded="false"]', function() {
    $(this).text("VIEW LESS");
});
$(document).on("click", '.job-viewMOre[aria-expanded="true"]', function() {
    $(this).text("VIEW MORE");
});

/*** bootstrap-tagsinput client-job-post enter disabled*/

$("#client-job-post").on("keyup keypress", function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});
// sni
/*** bootstrap-tagsinput consultant enter disabled*/

// $(".keyEnter").on("keyup keypress", function(e) {
//   var keyCode = e.keyCode || e.which;
//   if (keyCode === 13) {
//     e.preventDefault();

//     return false;
//   }
// });

/**-Header toggle */
$(document).on("click", ".cus-search-toggle", function(event) {
    $(this).toggleClass("cus-search-close");
    $(".header-top .navbar .navbar-collapse").removeClass("show");
    $(".pageLatest .newSidebar").hide(200);
    $(".cus-header-test").toggle(200);
});
/*----header sidebar toggle---*/
$(document).on("click", ".sidebar-open", function(event) {
    $(".cus-search-toggle").removeClass("cus-search-close");
    $(".cus-header-test").hide();
    $(".header-top .navbar .navbar-collapse").removeClass("show");
    $(".pageLatest .newSidebar").toggle(200);
});

/** For tooltip */
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});