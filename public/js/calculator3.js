/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Adjustment on individual consultant calculator


/*  Validations on Input*/
$("#c3-base-rate-perday").on("keypress keyup blur", function(event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
$('#c3-base-rate-perday').bind('copy paste cut', function(e) {
    e.preventDefault(); //disable cut,copy,paste
});
/* Calculator C3 */
$("#c3-base-rate-perday").keyup(function() {
    var base_rate = parseFloat($(this).val());
    var billing_rate = 0;
    if (!isNaN(base_rate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            billing_rate = c3_zeroToFiveBillingRate(base_rate);
        } else if (duration == 2) { // 6-11 Months
            billing_rate = c3_sixToElevenBillingRate(base_rate);
        } else { // > 12 Months
            billing_rate = c3_greaterThenTwelveBillingRate(base_rate);
        }
        $("#billing-rate").html("MYR " + parseFloat(billing_rate).toFixed(2) + "/day");
    } else { // Setting base rate 0 if base_rate not valid
        $("#billing-rate").html("MYR " + parseFloat(0).toFixed(2) + "/day");
    }
});
$(".months").change(function() {
    var base_rate = parseFloat($("#c3-base-rate-perday").val());
    var billing_rate = 0;
    if (!isNaN(base_rate)) {
        var duration = $(this).val();
        if (duration == 1) { // 0-5 Months
            billing_rate = c3_zeroToFiveBillingRate(base_rate);
        } else if (duration == 2) { // 6-11 Months
            billing_rate = c3_sixToElevenBillingRate(base_rate);
        } else { // > 12 Months
            billing_rate = c3_greaterThenTwelveBillingRate(base_rate);
        }
        $("#billing-rate").html("MYR " + parseFloat(billing_rate).toFixed(2) + "/day");
    } else { // Setting base rate 0 if base_rate not valid
        $("#billing-rate").html("MYR " + parseFloat(0).toFixed(2) + "/day");
    }
});
$(".fees").change(function() {
    var base_rate = parseFloat($("#c3-base-rate-perday").val());
    var billing_rate = 0;
    if (!isNaN(base_rate)) {
        var duration = $("input[name='months']:checked").val();
        if (duration == 1) { // 0-5 Months
            billing_rate = c3_zeroToFiveBillingRate(base_rate);
        } else if (duration == 2) { // 6-11 Months
            billing_rate = c3_sixToElevenBillingRate(base_rate);
        } else { // > 12 Months
            billing_rate = c3_greaterThenTwelveBillingRate(base_rate);
        }
        $("#billing-rate").html("MYR " + parseFloat(billing_rate).toFixed(2) + "/day");
    } else { // Setting base rate 0 if base_rate not valid
        $("#billing-rate").html("MYR " + parseFloat(0).toFixed(2) + "/day");
    }
});
/* Billing Rate For 0-5 Months*/
function c3_zeroToFiveBillingRate(base_rate) {
    /* Adding 5% Extra In Base Rate */
    var fivePercent = (base_rate * 5) / 100;
    base_rate += parseFloat(fivePercent);
    var fees = c3_calculateFees(base_rate);
    var billing_rate = parseFloat(base_rate) + parseFloat(fees);
    return billing_rate;
}
/* Billing Rate For 6-11 Months*/
function c3_sixToElevenBillingRate(base_rate) {
    var fees = c3_calculateFees(base_rate);
    var billing_rate = parseFloat(base_rate) + parseFloat(fees);
    return billing_rate;
}
/* Billing Rate For 6-11 Months*/
function c3_greaterThenTwelveBillingRate(base_rate) {
    /* Minus 3% From Base Rate */
    var threePercent = (base_rate * 3) / 100;
    base_rate -= parseFloat(threePercent);
    var fees = c3_calculateFees(base_rate);
    var billing_rate = parseFloat(base_rate) + parseFloat(fees);
    return billing_rate;
}
/* Calculate Fees */
function c3_calculateFees(base_rate) {
    var totalFees = parseFloat(0);
    if ($("#admin_fee").prop('checked') == true) {
        var admin_fee = (base_rate * 3) / 100;
        totalFees += parseFloat(admin_fee);
    }
    if ($("#professional_fee").prop('checked') == true) {
        var professional_fee = (base_rate * 4) / 100;
        totalFees += parseFloat(professional_fee);
    }
    return totalFees;
}