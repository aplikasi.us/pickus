/*job delete*/
$(document).on("click", ".job-delete", function() {
    var job_id = $(this).attr("data-id");
    bootbox.confirm({
        message: "Are you sure you want to delete this job?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: jobdelete,
                    data: {
                        _token: _token,
                        id: job_id
                    },
                    success: function(data) {
                        //alert(data);
                        if (data.success == true) {
                            location.reload();
                        } else {
                            bootbox.alert(data.message);
                        }
                    }
                });
            }
        }
    });
});
$(document).on("click", ".system-user-delete", function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    bootbox.confirm({
        message: "Are you sure you want to delete system user?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                location.href = href;
            }
        }
    });
});
/*active (inprogress) status change job openning*/
$(document).on("click", ".inactive-btn", function() {
    var job_id = $(this).attr("data-id");

    bootbox.confirm({
        message: "Are you sure you want to active this job?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: jobactivate,
                    data: {
                        _token: _token,
                        id: job_id
                    },
                    success: function(data) {
                        //alert(data);
                        if (data.success == true) {
                            location.reload();
                        } else {
                            bootbox.alert(data.message);
                        }
                    }
                });
            }
        }
    });
});

/*client status deactive code*/
$(document).on("click", ".client-active-btn", function() {
    var activate_id = $(this).attr("data-id");
    var status = $(this).attr("data-status");
    /*alert(status);*/
    if (status == "active") {
        var msg = "Are you sure you want to active this client?";
    } else {
        var msg = "Are you sure you want to inactive this client?";
    }
    bootbox.confirm({
        message: msg,
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: clinetactivateid,
                    data: {
                        _token: _token,
                        id: activate_id,
                        status: status
                    },
                    success: function(data) {
                        if (data.success == true) {
                            location.reload();
                        } else {
                            bootbox.alert(data.message);
                        }
                    }
                });
            }
        }
    });
});
/*consultant status deactive code*/
$(document).on("click", ".consultant-active-btn", function() {
    var activate_id = $(this).attr("data-id");
    var status = $(this).attr("data-status");
    /*alert(status);*/
    if (status == "active") {
        var msg = "Are you sure you want to active this consultant?";
    } else {
        var msg = "Are you sure you want to inactive this consultant?";
    }
    /* alert(msg);*/
    /* alert(activate_id);*/
    bootbox.confirm({
        message: msg,
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: consultant_activate,
                    data: {
                        _token: _token,
                        id: activate_id,
                        status: status
                    },
                    success: function(data) {
                        if (data.success == true) {
                            location.reload();
                        } else {
                            bootbox.alert(data.message);
                        }
                    }
                });
            }
        }
    });
});
/*add new system user validation*/
$("#system_user")
    .bootstrapValidator({
        trigger: "blur",
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: "Firstname is required"
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: "Lastname is required"
                    }
                }
            },
            email: {
                validators: {
                    remote: {
                        data: { _token: _token },
                        url: isUserEmailExists,
                        type: "POST"
                            //message: 'This email is already exists.'
                    },
                    notEmpty: {
                        message: "Email is required"
                    },

                    regexp: {
                        regexp: "^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$",
                        message: "The value is not a valid email address"
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: "Password is required"
                    },
                    regexp: {
                        regexp: /^(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/,
                        message: "The password should contain Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Special Character."
                    }
                }
            },
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: "Confirm Password is required"
                    },
                    identical: {
                        field: "password",
                        message: "The password and its confirm are not the same"
                    }
                }
            },
            systemadmin_type: {
                validators: {
                    notEmpty: {
                        message: "Please select user type"
                    }
                }
            },
            territory: {
                validators: {
                    notEmpty: {
                        message: "Please select territory"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
/*edit system user validation*/
$("#edit_cosultant_user")
    .bootstrapValidator({
        fields: {
            sap_job_title: {
                validators: {
                    notEmpty: {
                        message: "Sap job title required"
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: "Firstname is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Firstname be less than 50 character"
                    }
                }
            },
            "category[]": {
                validators: {
                    notEmpty: {
                        message: "Category is required"
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: "Lastname is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Lastname be less than 50 character"
                    }
                }
            },
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: "Mobile number is required"
                    },
                    regexp: {
                        regexp: /^[0-9-+()]*$/,
                        message: "Only number required"
                    },
                    stringLength: {
                        min: 10,
                        max: 15,
                        message: "Mobile number must be minimum 10 digits and maximum 15 digits"
                    }
                }
            },
            experience_in_years: {
                validators: {
                    notEmpty: {
                        message: "Experience is required"
                    },
                    integer: {
                        regexp: /^[0-9-+()]*$/,
                        message: "Only number required"
                    },
                    stringLength: {
                        max: 2,
                        message: "Experience be less than 2 character"
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: "State is required"
                    },
                    stringLength: {
                        max: 30,
                        message: "State must be less than 30 character"
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: "City name is required"
                    },
                    stringLength: {
                        max: 30,
                        message: "City name must be less than 30 character"
                    }
                }
            },
            skill_set: {
                validators: {
                    notEmpty: {
                        message: "Skill set is required"
                    }
                }
            },
            availability_date: {
                validators: {
                    notEmpty: {
                        message: "Availability date set is required"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
/*edit system user passqord*/
$("#edit_Pwd")
    .bootstrapValidator({
        trigger: "blur",
        fields: {
            password: {
                validators: {
                    notEmpty: {
                        message: "Password is required"
                    },
                    regexp: {
                        regexp: /^(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/,
                        message: "The password should contain Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Special Character."
                    }
                }
            },
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: "Confirm Password is required"
                    },
                    identical: {
                        field: "password",
                        message: "The password and its confirm are not the same"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
// Site Settings
$("#site-settings-form")
    .bootstrapValidator({
        framework: "bootstrap",
        excluded: ":disabled",
        fields: {
            site_name: {
                validators: {
                    notEmpty: {
                        message: "Site name is required"
                    },
                    stringLength: {
                        max: 15,
                        message: "Site name maximum 15 charcters"
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: "Address is required"
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: "City is required"
                    }
                }
            },
            zipcode: {
                validators: {
                    notEmpty: {
                        message: "Zipcode is required"
                    },
                    integer: {
                        message: "Invalid zipcode"
                    },
                    stringLength: {
                        min: 6,
                        max: 6,
                        message: "Zipcode length must be six characters"
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: "Phone number is required"
                    },
                    regexp: {
                        regexp: /^[0-9-+()]*$/,
                        message: "Only number required"
                    },
                    stringLength: {
                        min: 10,
                        max: 15,
                        message: "Phone number must be minimum 10 and maximum 15 digits"
                    }
                }
            },
            company_email: {
                validators: {
                    notEmpty: {
                        message: "Company email is required"
                    },
                    emailAddress: {
                        message: "Invalid email address"
                    }
                }
            },
            date_format: {
                validators: {
                    notEmpty: {
                        message: "Please select date format"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
$("#experience-form")
    .bootstrapValidator({
        framework: "bootstrap",
        excluded: ":disabled",
        fields: {
            company: {
                validators: {
                    notEmpty: {
                        message: "Company name is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Company name maximum 50 charcters allowed"
                    }
                }
            },
            occupation: {
                validators: {
                    notEmpty: {
                        message: "Occupation is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Occupation maximum 50 charcters allowed"
                    }
                }
            },
            industry: {
                validators: {
                    notEmpty: {
                        message: "Industry is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Industry maximum 50 charcters allowed"
                    }
                }
            },
            work_duration_from_month: {
                validators: {
                    notEmpty: {
                        message: "Duration from month is required"
                    }
                }
            },
            work_duration_from_year: {
                validators: {
                    notEmpty: {
                        message: "Duration from year is required"
                    }
                }
            },
            work_duration_to_month: {
                validators: {
                    notEmpty: {
                        message: "Duration to month is required"
                    }
                }
            },
            work_duration_to_year: {
                validators: {
                    notEmpty: {
                        message: "Duration to year is required"
                    }
                }
            },
            summary: {
                validators: {
                    notEmpty: {
                        message: "Summary is required"
                    },
                    stringLength: {
                        max: 200,
                        message: "Summary maximum 200 charcters allowed"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
$("#education-form")
    .bootstrapValidator({
        framework: "bootstrap",
        excluded: ":disabled",
        fields: {
            institute: {
                validators: {
                    notEmpty: {
                        message: "Institute name is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Iompany name maximum 50 charcters allowed"
                    }
                }
            },
            department: {
                validators: {
                    notEmpty: {
                        message: "Department is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Department maximum 50 charcters allowed"
                    }
                }
            },
            degree: {
                validators: {
                    notEmpty: {
                        message: "Degree is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Degree maximum 50 charcters allowed"
                    }
                }
            },
            duration_from_month: {
                validators: {
                    notEmpty: {
                        message: "Duration from month is required"
                    }
                }
            },
            duration_from_year: {
                validators: {
                    notEmpty: {
                        message: "Duration from year is required"
                    }
                }
            },
            duration_to_month: {
                validators: {
                    notEmpty: {
                        message: "Duration to month is required"
                    }
                }
            },
            duration_to_year: {
                validators: {
                    notEmpty: {
                        message: "Duration to year is required"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
$("#reference-form")
    .bootstrapValidator({
        framework: "bootstrap",
        excluded: ":disabled",
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: "Name is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Name maximum 50 charcters allowed"
                    }
                }
            },
            position: {
                validators: {
                    notEmpty: {
                        message: "Position is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Position maximum 50 charcters allowed"
                    }
                }
            },
            company: {
                validators: {
                    notEmpty: {
                        message: "Company is required"
                    },
                    stringLength: {
                        max: 50,
                        message: "Company maximum 50 charcters allowed"
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: "Phone number is required"
                    },
                    regexp: {
                        regexp: /^[0-9-+()]*$/,
                        message: "Only number required"
                    },
                    stringLength: {
                        min: 10,
                        max: 15,
                        message: "Phone number must be minimum 10 and maximum 15 digits"
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: "Email is required"
                    },
                    emailAddress: {
                        message: "Invalid email address"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
/* FAQ'S*/
$("#faqform")
    .bootstrapValidator({
        fields: {
            category: {
                validators: {
                    notEmpty: {
                        message: "Category is required"
                    }
                }
            },
            question: {
                validators: {
                    notEmpty: {
                        message: "Question is required"
                    },
                    stringLength: {
                        max: 200,
                        message: "Question maximum 200 charcters allowed"
                    }
                }
            },
            answer: {
                validators: {
                    notEmpty: {
                        message: "Answer is required"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });

/*system user status code*/
$(document).on("click", ".system-active-btn", function() {
    var activate_id = $(this).attr("data-id");
    var status = $(this).attr("data-status");
    /*alert(status);*/
    if (status == "active") {
        var msg = "Are you sure you want to inactive this system user?";
    } else {
        var msg = "Are you sure you want to active this system user?";
    }
    bootbox.confirm({
        message: msg,
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: systemuser_status,
                    data: {
                        _token: _token,
                        id: activate_id
                    },
                    success: function(data) {
                        if (data.success == true) {
                            location.reload();
                        } else {
                            bootbox.alert(data.message);
                        }
                    }
                });
            }
        }
    });
});

$("#admin-job-approve")
    .bootstrapValidator({
        framework: "bootstrap",
        excluded: ":disabled",
        fields: {
            job_title: {
                validators: {
                    notEmpty: {
                        message: "Job title is required"
                    },
                    stringLength: {
                        max: 70,
                        message: "Job title maximum 70 charcters allowed"
                    }
                }
            },
            budget: {
                validators: {
                    notEmpty: {
                        message: "Budget is required"
                    },
                    numeric: {
                        message: "Enter number only",
                        // The default separators
                        thousandsSeparator: "",
                        decimalSeparator: "."
                    }
                }
            },
            baserate: {
                validators: {
                    notEmpty: {
                        message: "Base rate is required"
                    },
                    numeric: {
                        message: "Enter number only",
                        // The default separators
                        thousandsSeparator: "",
                        decimalSeparator: "."
                    }
                }
            },
            experiece: {
                validators: {
                    notEmpty: {
                        message: "Experience is required"
                    }
                }
            },
            job_type: {
                validators: {
                    notEmpty: {
                        message: "Job type is required"
                    }
                }
            },
            job_mode: {
                validators: {
                    notEmpty: {
                        message: "Job mode is required"
                    }
                }
            },
            start_date: {
                validators: {
                    notEmpty: {
                        message: "Start date is required"
                    }
                }
            },
            duration: {
                validators: {
                    notEmpty: {
                        message: "Duration is required"
                    },
                    numeric: {
                        message: "Enter number only",
                        // The default separators
                        thousandsSeparator: ""
                    }
                }
            },
            state: {
                validators: {
                    callback: {
                        message: " ",
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements("state").val();
                            return options != null;
                        }
                    }
                }
            },
            city: {
                validators: {
                    callback: {
                        message: " ",
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements("city").val();
                            return options != null;
                        }
                    }
                }
            },
            industry: {
                validators: {
                    notEmpty: {
                        message: "Industry is required"
                    }
                }
            },
            category: {
                validators: {
                    notEmpty: {
                        message: "Category is required"
                    }
                }
            },
            key_skills: {
                validators: {
                    notEmpty: {
                        message: "Key skills required"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
/*edit system user passqord*/
$("#verify-password")
    .bootstrapValidator({
        trigger: "blur",
        fields: {
            password: {
                validators: {
                    notEmpty: {
                        message: "Password is required"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    })
    .on("success.form.bv", function(e) {
        // Prevent form submission
        e.preventDefault();
        // Get the form instance
        var $form = $(e.target);
        var formData = new FormData(this);
        console.log($form.attr("action"));
        $type = $form.find("#action-type").val();
        if ($type == "syncdata") {
            // Sync data
            $.ajax({
                type: "post",
                url: $form.attr("action"),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if (data.status == 200) {
                        $("#error-message").css("display", "none");
                        $("#verify-password-modal").modal("hide");
                        $("#syncnow").prop("disabled", true);
                        $("#sync-loader").css("display", "inline-block");

                        submitSyncData();
                    } else {
                        $("#error-message").css("display", "block");
                    }
                }
            });
        } else {
            // Deleting client contact/consultant account
            $.ajax({
                type: "POST",
                url: $form.attr("action"),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if (data.status == 200) {
                        location.href = $form.find("#redirect_url").val();
                    } else {
                        $("#error-message").css("display", "block");
                    }
                }
            });
        }
    });
/*edit system user passqord*/
$("#tagline-form")
    .bootstrapValidator({
        fields: {
            user_type: {
                validators: {
                    notEmpty: {
                        message: "Please select user type"
                    }
                }
            },
            category: {
                validators: {
                    notEmpty: {
                        message: "Please select category"
                    }
                }
            },
            content: {
                validators: {
                    notEmpty: {
                        message: "Please enter content"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });
$("#change-candidate-status-form")
    .bootstrapValidator({
        fields: {
            status: {
                validators: {
                    notEmpty: {
                        message: "Please select status"
                    }
                }
            }
        }
    })
    .on("error.validator.bv", function(e, data) {
        data.element
            .data("bv.messages")
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]')
            .hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]')
            .show();
    });

// $("#update-candidate-stats-btn").click(function(e) {
//   e.preventDefault();
//   return bootbox.confirm(
//     "Are you sure to approve this summary details",
//     function(result) {
//       if (result) {
//         $("#edit_cosultant_user").submit();
//       }
//     }
//   );
// });
function submitSyncData() {
    var $form = $("#syncform");
    var formData = $("#syncform").serialize();
    console.log(formData);
    $.ajax({
        type: "POST",
        url: $form.attr("action"),
        data: formData,
        success: function(data) {
            console.log(data)
            $("#sync-loader").css("display", "none");
            $("#syncnow").prop("disabled", false);
            if (data.status == 200) {
                toastr.success("Data synced successfully");
            }
        }
    });
}
// Reset change password Form
$("#verify-password-modal").on("hidden.bs.modal", function() {
    $("#error-message").css("display", "none");
    $("#verify-password").bootstrapValidator("resetForm", true);
});
$("#job-start-date")
    .datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd",
        startDate: "-0d"
    })
    .on("changeDate", function(selected) {
        $("#admin-job-approve").bootstrapValidator("revalidateField", "start_date");
    });
$("#availability_date")
    .datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd",
        startDate: "-0d"
    })
    .on("changeDate", function(selected) {
        $("#edit_cosultant_user").bootstrapValidator(
            "revalidateField",
            "availability_date"
        );
    });
$("#job-state").change(function() {
    $.ajax({
        type: "POST",
        url: getCityList,
        cache: false,
        data: {
            _token: $("input[name=_token]").val(),
            id: $(this).val()
        },
        success: function(data) {
            $("#job-city").empty();
            $("#job-city").append(
                $("<option value=''></option>").text("--Select City--")
            );
            $.each(data.city, function(key, value) {
                $("#job-city").append(
                    $("<option></option>")
                    .attr("value", key)
                    .text(value)
                );
            });
            $("#admin-job-approve").bootstrapValidator("revalidateField", "city");
        }
    });
});
$("#approve-summary").click(function(e) {
    e.preventDefault();
    return bootbox.confirm(
        "Are you sure to approve this summary details",
        function(result) {
            if (result) {
                $("#edit_cosultant_user").submit();
            }
        }
    );
});

$("#approve-job").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm("Are you sure to approve this job", function(result) {
        if (result) {
            window.location = href;
        }
    });
});

/*all checkbox checked*/
$(".i-checks").iCheck({
    checkboxClass: "icheckbox_square-green",
    radioClass: "iradio_square-green"
});

$(function() {
    var checkAll_1 = $("#example-select-all");
    var checkboxes_1 = $(".checkall");

    checkAll_1.on("ifChecked ifUnchecked", function(event) {
        if (event.type == "ifChecked") {
            checkboxes_1.iCheck("check");
        } else {
            checkboxes_1.iCheck("uncheck");
        }
    });

    checkboxes_1.on("ifChanged", function(event) {
        if (checkboxes_1.filter(":checked").length == checkboxes_1.length) {
            checkAll_1.prop("checked", "checked");
        } else {
            checkAll_1.prop("checked", "");
        }
        checkAll_1.iCheck("update");
    });
});
// For User Management
$("#example-select-all").on("ifChecked", function(event) {
    $(".checkall").iCheck("check");
    triggeredByChild = false;
});

$("#example-select-all").on("ifUnchecked", function(event) {
    if (!triggeredByChild) {
        $(".checkall").iCheck("uncheck");
    }
    triggeredByChild = false;
});

// Multiple user delete
$("#delete-selected-user").on("click", function(e) {
    var allVals = [];
    $("#sub_chk:checked").each(function() {
        allVals.push($(this).attr("data-id"));
    });
    if (allVals.length <= 0) {
        e.preventDefault();
        return bootbox.alert("Please select atleast one system user");
    } else {
        var message = "Are you sure want to delete this system user(s)?";
        e.preventDefault();
        href = $(this).attr("href");
        return bootbox.confirm(message, function(check) {
            if (check) {
                var join_selected_values = allVals.join(",");
                $.ajax({
                    type: "POST",
                    url: systemuser_delete,
                    cache: false,
                    data: {
                        /*"_method":'DELETE',*/
                        _token: _token,
                        ids: join_selected_values
                    },
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });
    }
});

/*---multiple esume download --*/

// Multiple cv download Status change
$(".multiple-cs-download").on("click", function(e) {
    var status = $(this).attr("data-status");
    var allVals = [];
    $("#sub_chk:checked").each(function() {
        allVals.push($(this).attr("data-id"));
    });
    if (allVals.length <= 0) {
        e.preventDefault();
        return bootbox.alert("Please select atleast one profile");
    } else {
        var message =
            "Are you sure you want to download formatted profile of this candidate?";
        e.preventDefault();
        href = $(this).attr("href");

        return bootbox.confirm(message, function(check) {
            if (check) {
                var join_selected_values = allVals.join(",");
                $.ajax({
                    type: "POST",
                    url: multipleFormattedResumeDownload,
                    cache: false,
                    data: {
                        _token: $("input[name=_token]").val(),
                        ids: join_selected_values,
                        status: status
                    },
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });
    }
});
/*--end--*/

// Multiple request profile deleted
$(".multiple-delete").on("click", function(e) {
    var status = $(this).attr("data-status");
    var allVals = [];
    $("#sub_chk:checked").each(function() {
        allVals.push($(this).attr("data-id"));
    });
    if (allVals.length <= 0) {
        e.preventDefault();
        return bootbox.alert("Please select atleast one profile");
    } else {
        var message =
            "Are you sure you want to delete this profile access request?";
        e.preventDefault();
        href = $(this).attr("href");

        return bootbox.confirm(message, function(check) {
            if (check) {
                var join_selected_values = allVals.join(",");
                $.ajax({
                    type: "POST",
                    url: multipleDeleteRequest,
                    cache: false,
                    data: {
                        _token: $("input[name=_token]").val(),
                        ids: join_selected_values,
                        status: status
                    },
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });
    }
});
/*--end--*/

$(".btnapprovereq").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to approve this profile access?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});
$(".btnapproveummary").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to approve this consultant profile?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});
$(".btncancelled").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to cancelled request this consultant profile?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});

// If Check module
$(".parent").on("ifChecked", function(event) {
    $(this)
        .closest(".parent-div")
        .find(".childs")
        .iCheck("check");
});
// If Uncheck module
$(".parent").on("ifUnchecked", function(event) {
    $(this)
        .closest(".parent-div")
        .find(".childs")
        .iCheck("uncheck");
});
// Checking if none submodule selected, then uncheck module
$(".childs").on("ifUnchecked", function(event) {
    var checkedCount = 0;
    $(this)
        .closest(".parent-div")
        .find(".childs:checked")
        .each(function(key, value) {
            checkedCount++;
        });
    if (checkedCount == 0) {
        $(this)
            .closest(".parent-div")
            .find(".parent")
            .iCheck("uncheck");
    }
});
// Checking if none submodule selected, then uncheck module
$(".childs").on("ifChecked", function(event) {
    var checkedCount = 0;
    var subCheckLength = $(this)
        .closest(".parent-div")
        .find(".childs").length;
    $(this)
        .closest(".parent-div")
        .find(".childs:checked")
        .each(function(key, value) {
            checkedCount++;
        });
    if (checkedCount == subCheckLength) {
        $(this)
            .closest(".parent-div")
            .find(".parent")
            .iCheck("check");
    }
});

$("#syncnow").click(function() {
    //    var isCheckedAtleastOne = 0;
    //    $(".childs:checked").each(function () {
    //        isCheckedAtleastOne++;
    //    });
    //    if (isCheckedAtleastOne == 0) {
    //        return bootbox.alert('Please select atleast one module');
    //    } else {
    $("#verify-password-modal").modal("show");
    //    }
});
$("#systemadmin_type").change(function() {
    if ($(this).val() == "Super Admin") {
        $("#territory-div").hide();
    } else {
        $("#territory-div").show();
    }
    $("#system_user").bootstrapValidator("revalidateField", "systemadmin_type");
    $("#system_user").bootstrapValidator("revalidateField", "territory");
});
$("#job-budget").on("keyup, input", function() {
    var baserate = parseFloat($(this).val() / 1.07).toFixed(2);
    $("#job-calculated-baserate").val(baserate);
});
$(".delete-tagline").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm(
        "Are you sure you want to delete this tagline?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});
$(".read-notification").click(function() {
    $.ajax({
        type: "POST",
        url: readAdminNotification,
        cache: false,
        data: {},
        success: function(data) {
            $(".read-notification span").remove();
        }
    });
});
$("#client-table .glyphicon").click(function() {
    if ($(this).hasClass("glyphicon-plus")) {
        $(this).addClass("glyphicon-minus");
        $(this).removeClass("glyphicon-plus");
    } else {
        $(this).addClass("glyphicon-plus");
        $(this).removeClass("glyphicon-minus");
    }
});
$(".contact-generate-password-link").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to generate password?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});
$("#is_currently_working_here").change(function() {
    if ($(this).is(":checked")) {
        var currentlyWorkingExperienceId = $(
            "#is_already_working_experience_id"
        ).val();
        if (currentlyWorkingExperienceId == "") {
            $("#experience-form").bootstrapValidator(
                "enableFieldValidators",
                "work_duration_to_month",
                false
            );
            $("#experience-form").bootstrapValidator(
                "enableFieldValidators",
                "work_duration_to_year",
                false
            );
            $(".work_duration_to").attr("disabled", true);
        } else {
            toastr.error(
                "You are not allowed to select this option. Because you are currently working on some other company."
            );
            $(this).prop("checked", false);
        }
    } else {
        $("#experience-form").bootstrapValidator(
            "enableFieldValidators",
            "work_duration_to_month",
            true
        );
        $("#experience-form").bootstrapValidator(
            "enableFieldValidators",
            "work_duration_to_year",
            true
        );
        $(".work_duration_to").removeAttr("disabled");
    }
});
$("#is_currently_pursuing").change(function() {
    if ($(this).is(":checked")) {
        var currentlyPersuingId = $("#is_already_persuing_id").val();
        if (currentlyPersuingId == "") {
            $("#education-form").bootstrapValidator(
                "enableFieldValidators",
                "duration_to_month",
                false
            );
            $("#education-form").bootstrapValidator(
                "enableFieldValidators",
                "duration_to_year",
                false
            );
            $(".duration_to").attr("disabled", true);
        } else {
            toastr.error(
                "You are not allowed to select this option. Because you are currently persuing in some other institute."
            );
            $(this).prop("checked", false);
        }
    } else {
        $("#education-form").bootstrapValidator(
            "enableFieldValidators",
            "duration_to_month",
            true
        );
        $("#education-form").bootstrapValidator(
            "enableFieldValidators",
            "duration_to_year",
            true
        );
        $(".duration_to").removeAttr("disabled");
    }
});
$(".restore-account").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm({
        message: "Are you sure you want to restore this account?",
        buttons: {
            confirm: {
                label: "Yes",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result == true) {
                window.location = href;
            }
        }
    });
});

$(".delete-tabulardata").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    var dataid = $(this).attr("data-title");
    return bootbox.confirm(
        "Are you sure to delete this " + dataid + "?",
        function(result) {
            if (result) {
                window.location = href;
            }
        }
    );
});

$(".delete-account").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    $("#verify-password-modal").modal("show");
    $("#verify-password-modal")
        .find("#redirect_url")
        .val(href);
});

$(document).ready(function() {
    $(".territory-storechange").on("change", function() {
        this.form.submit();
    });
});
$(".delete-faq").click(function(e) {
    e.preventDefault();
    var href = $(this).attr("href");
    return bootbox.confirm("Are you sure to delete this FAQ?", function(result) {
        if (result) {
            window.location = href;
        }
    });
});
$("#candidate-category").select2();

$(".view-more-associated-candidate").click(function() {
    var counter = $(this)
        .closest("td")
        .find("li").length;
    if (
        $(this)
        .closest("td")
        .find(".candidate-li")
        .hasClass("hide")
    ) {
        $(this)
            .closest("td")
            .find(".candidate-li")
            .removeClass("hide");
        $(this).text("View Less");
    } else {
        $(this)
            .closest("td")
            .find(".candidate-li")
            .addClass("hide");
        $(this).text("View More");
    }
});
$("#change-candidate-status").click(function() {
    var data = [];
    $("#sub_chk:checked").each(function() {
        data.push($(this).attr("data-job-id"));
    });
    if (data.length != 0) {
        var comma_seprated_ids = data.join(",");
        $("#change-candidate-status-modal").modal("show");
        $("#change-candidate-status-modal")
            .find("#job_ids")
            .val(comma_seprated_ids);
    } else {
        return bootbox.alert("Please select atleast one job");
    }
    console.log(data.length);
    //   if (allVals.length <= 0) {
    //     e.preventDefault();
    //     return bootbox.alert("Please select atleast one system user");
    //   } else {
    //     var message = "Are you sure want to delete this system user(s)?";
    //     e.preventDefault();
    //     href = $(this).attr("href");
    //     return bootbox.confirm(message, function(check) {
    //       if (check) {
    //         var join_selected_values = allVals.join(",");
    //         $.ajax({
    //           type: "POST",
    //           url: systemuser_delete,
    //           cache: false,
    //           data: {
    //             /*"_method":'DELETE',*/
    //             _token: _token,
    //             ids: join_selected_values
    //           },
    //           success: function(data) {
    //             location.reload();
    //           }
    //         });
    //       }
    //     });
    //   }
});