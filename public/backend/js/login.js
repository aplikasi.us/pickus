/*login**/

$('#loginform .login').click(function () {
    $('.new-block').remove();
});

$('#loginform #email').on('change',function(){
    $('.new-block').remove();
});

$('#loginform').bootstrapValidator({
    trigger: 'blur',
    fields: {
        
        email: {
            validators: {
                notEmpty: {
                    message: 'Email is required'
                },
                emailAddress: {
                    message: 'The value is not a valid email address.'
                }
            }
        },
        password: {
                validators: {
                    notEmpty: {
                        message: 'Password is required'
                    },
                    
                }
            }
        }
    }).on('error.validator.bv', function (e, data) {
        data.element
                .data('bv.messages')
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]').show();
    });
