<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
$current_blog_id = get_current_blog_id();
if($current_blog_id == 3){
	$svar = "PH";
}
else{
	$svar = "MY";
}
?>
<section class="contributesection">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="htagf">Want to contribute?</h3>
				<p class="ptagf">Feel free to drop us an email</p>
				<div class="btntagf"><a href="http://206.189.86.153/<?php echo $svar; ?>/contact-us">Contact Us</a></div>
			</div>
		</div>
	</div>
</section>
<footer class="siteFooter">
	<div class="footerTop">
		<div class="container">
			<div class="row">
				<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
					<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 footerColumn1 footer-logo">
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
					<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 footerColumn2 ">
						<?php dynamic_sidebar( 'sidebar-2' ); ?>
					</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
					<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 ">
						<?php dynamic_sidebar( 'sidebar-3' ); ?>
					</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
					<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 ">
						<?php dynamic_sidebar( 'sidebar-4' ); ?>
					</div>
				<?php endif; ?>

				<!-- <?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
					<div class="col-sm-6 col-md-6 col-lg-2 col-lg-2">
						<?php dynamic_sidebar( 'sidebar-5' ); ?>
					</div>
				<?php endif; ?> -->
			
			</div>
		</div>
	</div>
	<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
		<div class="container">
			<div class="footerbottom">
				<?php dynamic_sidebar( 'sidebar-6' ); ?>
			</div>
		</div>
	<?php endif; ?>
</footer>
<?php
 $var = network_site_url();
 $mainDomainurl = str_replace('blog/', '', $var);
 $territory = "MY";
 if(Session::get('territory') == 'my'){
	$territory = "MY";
	?>
	<script>
		jQuery( document ).ready(function() {
			jQuery('.ph-hide').hide();
		});
	</script>
	<?php
 }
 if(Session::get('territory') == 'ph'){
	$territory = "PH";
	?>
	<script>
		jQuery( document ).ready(function() {
			jQuery('.my-hide').hide();
		});
	</script>
	<?php
 }
$forgotUrl = $mainDomainurl.$territory.'/user/forgotpassword';
$loginUrl = $mainDomainurl.$territory.'/user/login';
$logoutUrl = $mainDomainurl.$territory.'/logout';
$mainsiteUrl = $mainDomainurl.$territory;
$joinUrl = $mainDomainurl.$territory.'?search=registration';
$consultantPopulateurl = $mainDomainurl.$territory.'/getsearchconsultant';
$jobPopulateurl = $mainDomainurl.$territory.'/getsearchjob';
$faqPageUrl = $mainDomainurl.$territory.'/faq';
$aboutusPageUrl = $mainDomainurl.$territory.'/about-us';
$howItPageUrl = $mainDomainurl.$territory.'/how-it-works';
$atpUrl = $mainDomainurl.$territory.'/the-atap-virtual-office';
$blogUrl = $mainDomainurl.$territory.'/blog';
$consultantsUrl = $mainDomainurl.$territory.'/consultant/registration';
$clientsUrl = $mainDomainurl.$territory.'/client/registration';
$termsUrl = $mainDomainurl.$territory.'/terms-and-conditions';
$privacyUrl = $mainDomainurl.$territory.'/privacy-policy';
?>
<script>
	jQuery( document ).ready(function() {
		var joinUrl = '<?php echo $joinUrl ?>';
		var faqPageUrl = '<?php echo $faqPageUrl ?>';
		var aboutusPageUrl = '<?php echo $aboutusPageUrl ?>';
		var howItPageUrl = '<?php echo $howItPageUrl ?>';
		var atpUrl = '<?php echo $atpUrl ?>';
		var blogUrl = '<?php echo $blogUrl ?>';
		var mainsiteUrl = '<?php echo $mainsiteUrl ?>';
		var consultantsUrl = '<?php echo $consultantsUrl ?>';
		var clientsUrl = '<?php echo $clientsUrl ?>';
		var termsUrl = '<?php echo $termsUrl ?>';
		var privacyUrl = '<?php echo $privacyUrl ?>';
		
		jQuery(".join-us-link a").attr("href", joinUrl);
		jQuery(".faq-url a").attr("href", faqPageUrl);
		jQuery(".about-url a").attr("href", aboutusPageUrl);
		jQuery(".howit-url a").attr("href", howItPageUrl);
		jQuery(".atp-url a").attr("href", atpUrl);
		jQuery(".blog-url a").attr("href", blogUrl);
		jQuery(".home-url a").attr("href", mainsiteUrl);
		jQuery(".consultants-url a").attr("href", consultantsUrl);
		jQuery(".clients-url a").attr("href", clientsUrl);
		jQuery(".terms-service-url a").attr("href", termsUrl);
		jQuery(".privacy-policy-url a").attr("href", privacyUrl);
	});
</script>
<?php if (Auth::check()) {
		$notificationUrl = $mainDomainurl.$territory.'/consultant/notifications';
		$homeUrl = $mainDomainurl.$territory.'/consultant';
		if (Auth::user()->role_id == "1"){
			$notificationUrl = $mainDomainurl.$territory.'/client/notifications';
			$homeUrl = $mainDomainurl.$territory.'/client';
		}
	?>
	<script>
		jQuery( document ).ready(function() {
			var logoutUrl = '<?php echo $logoutUrl ?>';
			var notificationUrl = '<?php echo $notificationUrl ?>';
			var homeUrl = '<?php echo $homeUrl ?>';
			jQuery('.login-url').hide();
			jQuery(".logout-url a").attr("href", logoutUrl);
			jQuery(".notification-icon a").attr("href", notificationUrl);
			jQuery(".home-url-set a").attr("href", homeUrl);
			// jQuery('.iam-hover-effect').hide();
			jQuery('.login-hide').hide();
		});
	</script>
<?php }else{ ?>
	<script>
		jQuery( document ).ready(function() {
			jQuery('.logout-url').hide();
			jQuery('.logout-hide').hide();
			jQuery(".notification-icon a").hide();
		});
	</script>
	
<?php } ?>


<script>

	jQuery( document ).ready(function() {
		// Fire on document ready.
		/*----search conultant job popult data --*/
		// jQuery(".search-consulresult").keyup(function() {
		// 	var search = jQuery(this).val();
		// 	var consultantPopulateurl = "<?php echo $consultantPopulateurl; ?>";
		// 	if (search != "") {
		// 		jQuery.ajax({
		// 		url: consultantPopulateurl,
		// 		method: "POST",
		// 		data: { search: search},
		// 		success: function(data) {
		// 			//   console.log(data);
		// 			if (data.status == 200) {
		// 			jQuery(".consutant-suggestions").fadeIn();
		// 			jQuery(".consutant-suggestions").html(data.output);
		// 			}
		// 			if (data.status == 400) {
		// 			jQuery(".consutant-suggestions").fadeOut();
		// 			}
		// 		}
		// 		});
		// 	}
		// });
		// jQuery(".search-jobresult").keyup(function() {
		// 	var search = jQuery(this).val();
		// 	var jobPopulateurl = "<?php echo $jobPopulateurl; ?>";
		// 	if (search != "") {
		// 		jQuery.ajax({
		// 		url: jobPopulateurl,
		// 		method: "POST",
		// 		data: { search: search },
		// 		success: function(data) {
		// 			//   console.log(data);
		// 			if (data.status == 200) {
		// 			jQuery(".job-suggestions").fadeIn();
		// 			jQuery(".job-suggestions").html(data.output);
		// 			}
		// 			if (data.status == 400) {
		// 			jQuery(".job-suggestions").fadeOut();
		// 			}
		// 		}
		// 		});
		// 	}
		// });
		jQuery( ".search-jobresult" ).autocomplete({
			source: function( request, response ) {
				jQuery.ajax( {
				url: "<?php echo $jobPopulateurl; ?>",
				// dataType: "json",
				data: {
					search: request.term
				},
				success: function( data ) {
					$data = JSON.parse(data.output);
					response( $data);
				}
				} );
			},
			minLength: 2,
		} );

		jQuery( ".search-consulresult" ).autocomplete({
			source: function( request, response ) {
				jQuery.ajax( {
				url: "<?php echo $consultantPopulateurl; ?>",
				// dataType: "json",
				data: {
					search: request.term
				},
				success: function( data ) {
					$data = JSON.parse(data.output);
					response( $data);
				}
				} );
			},
			minLength: 2,
		} );


		jQuery(document).on("click", "body", function() {
			jQuery(".consutant-suggestions").fadeOut();
			jQuery(".job-suggestions").fadeOut();
		});
		jQuery(document).on("keydown", function(e) {
			if (e.keyCode === 27) {
				// ESC
				jQuery(".consutant-suggestions").fadeOut();
				jQuery(".job-suggestions").fadeOut();
			}
		});
		jQuery(document).on("click", ".consutant-suggestions ul li", function() {
			jQuery(".search-consulresult").val(jQuery(this).text());
			jQuery(".consutant-suggestions").fadeOut();
		});
		jQuery(document).on("click", ".job-suggestions ul li", function() {
			jQuery(".search-jobresult").val(jQuery(this).text());
			jQuery(".job-suggestions").fadeOut();
		});

		var mainsiteUrl = '<?php echo $mainsiteUrl ?>';
		jQuery(".custom-logo-link").attr("href", mainsiteUrl);
		jQuery('.errormsg').hide();
		jQuery('.errormsg-ps').hide();
		jQuery('.sucessmsg').hide();
		jQuery( "#consultant-login" ).validate({
			rules: {
					email: {
						required: true,
						minlength: 3,
						maxlength: 30,
					},
					password: {
					required: true,
					maxlength: 50
					},
					
			},
			messages: {
			email: "Please enter email address",
			password: "Please enter password",
        	},
			submitHandler: function(form) {
					jQuery(".btn-login-blue").prop("disabled", true);
					jQuery("#login-loading").show();

					var formDatainvestors = {
					'email': jQuery('#consultant-login #email').val(),
					'password': jQuery('#consultant-login #password').val(),
					'_token': jQuery("input[name=_token]").val()
					
					};
					//console.log(formDatainvestors);

					jQuery.ajax({
						type: 'POST',
					//   dataType: 'json',
						url : '<?php echo $loginUrl; ?>',
						data: formDatainvestors,
						success: function(dataobj, status, xhr,data) {                      
							
						jQuery("#mr_register").prop("disabled", false);
						jQuery("#login-loading").hide();

						// jQuery( "#consultant-login" ).hide();
						if(dataobj.status == 200){                                   
							window.location.href = dataobj.redirecturl
						}
						else{                       
							// jQuery('#consultant-login')[0].reset();
							// jQuery( "#consultant-login" ).hide();
							jQuery(".errormsg").html(dataobj.message).show();
							jQuery(".btn-login-blue").prop("disabled", false);
						}
						},                    
						xhrFields: {
						withCredentials: true
						},
						crossDomain: true
					});
			}                                     
		});
		/*---------forgot password ---------*/
			jQuery( "#forgotpassword" ).validate({
				rules: {
						email: {
							required: true,
							minlength: 3,
							maxlength: 30,
						}
				},
				messages: {
				email: "Please enter email address",
				},
				
				submitHandler: function(form) {
						jQuery(".btn-login-blue").prop("disabled", true);
						jQuery("#forgot-loading").show();
						jQuery(".errormsg-ps").hide(200);
						jQuery(".sucessmsg").hide(200);
						var formDatainvestors = {
						'email': jQuery('#forgotpassword #email').val(),
						
						};
						// console.log(formDatainvestors);
						// return;
						jQuery.ajax({
							type: 'POST',
						//   dataType: 'json',
							url : '<?php echo $forgotUrl; ?>',
							data: formDatainvestors,
							success: function(dataobj, status, xhr,data) {    
							jQuery("#forgot-loading").hide(200);

							// jQuery( "#consultant-login" ).hide();
								if(dataobj.status == 200){                                   
									// window.location.href = dataobj.redirecturl
									jQuery(".sucessmsg").html(dataobj.message).show(200);
									jQuery(".btn-login-blue").prop("disabled", false);
								}
								else{                       
									// jQuery('#forgotpassword')[0].reset();
									// jQuery( "#forgotpassword" ).hide();
									jQuery(".errormsg-ps").html(dataobj.message).show(200);
									jQuery(".btn-login-blue").prop("disabled", false);
								}
							},                    
							xhrFields: {
							withCredentials: true
							},
							crossDomain: true
						});
				}                                     
			});
		});


		/*---------forgot password ---------*/
		jQuery( document ).ready(function() {
			
			jQuery.validator.addMethod( "minlengthNWS", function( value, element, param ) {
				return jQuery.trim(value).length >= param;
			}, jQuery.validator.format( "Please enter at least {0} characters" ) );
			jQuery( "#commentform" ).validate({
				rules: {
					comment: {
							required: true,
							minlengthNWS: true
						},
					email: {
						required: true,
						minlength: 3,
						maxlength: 30,
						minlengthNWS: true
					},
					author: {
						required: true,
						minlengthNWS: true
					},
				},
				messages: {
				comment: "Please enter your comment",
				email: "Please enter your email",
				author: "Please enter your name",
				},
				
				submitHandler: function(form) {
						jQuery(".btn-login-blue").prop("disabled", true);
						jQuery("#forgot-loading").show();
						jQuery(".errormsg-ps").hide(200);
						jQuery(".sucessmsg").hide(200);
						$("#commentform").submit(); 
				}                                     
			});
		});
</script>
<?php wp_footer(); ?>
<script type="text/javascript">
var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || 
{widgetcode:"d8b8c99bcae961f661ba84373821fb33170f5ed9be3bdf8985cce6480e1d54f7", values:{},ready:function(){}};
var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
</script>

</body>
</html>
