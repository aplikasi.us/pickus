<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
if ( have_posts() ) {
	the_post();

	$category_object = get_the_category(get_the_ID());
	if($category_object){
		$category_names = array();
		foreach ($category_object as $category) {
			$category_names[] = $category->cat_name;
		}
	}	
?>
<article>
	<section class="secColumn">
		<div class="row rowColumns no-gutters">
			<div class="col-md-12 columnRight">
				<div class="container pt-5">
					<div class="cfrow">						
						<?php
						if($category_names){
							echo "<div class='btndivsingle'><span>".implode(', ', $category_names)."</span></div>";
						}
						?>
						<div class="cfheadertag">
							<h2>
							<?php echo get_the_title() ?>
							</h2>
						</div>
						<div class="agodiv">
							<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
							<span class="iconWeek"><?php echo get_the_date(); ?></span>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-8">
							
							<?php
							// the_post_navigation(
							// 	array(
							// 		'screen_reader_text' => ' ',
							// 		'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next Post', 'twentynineteen' ) . '</span> <img class="flipit" src="'.get_theme_file_uri( '/img/icon-back.png' ).'">',
							// 		'prev_text' => '<img src="'.get_theme_file_uri( '/img/icon-back.png' ).'"> <span class="meta-nav" aria-hidden="true">' . __( 'Previous Post', 'twentynineteen' ) . '</span> ',
							// 	)
							// );
							?>
							
							<!-- <p class="pb-3">
								<a href="javascript:history.go(-1)" title="Next" class="btnBack">
									<img src="<?php echo get_theme_file_uri( '/img/icon-back.png' ) ?>" alt="" title=""> Back
								</a>
							</p> -->
						<?php
						get_template_part( 'template-parts/content/content', 'single' );
						// if ( have_posts() ) {
							// the_post();
							// get_template_part( 'template-parts/content/content', 'single' );
						// }
						?>

						<?php
						$my_title = get_the_title();
						$my_excerpt = wp_strip_all_tags(substr(get_the_content(), 0, 90));
						$my_link = get_permalink();
						?>
							<div class="sharingbtns">
								<ul class="list-inline">               
									<li class="list-inline-item">
										<a href="https://twitter.com/intent/tweet?source=<?php echo $my_link ?>&text=<?php echo $my_title; ?>: <?php echo $my_link ?>" target="_blank" title="Tweet"><i class="fa fa-twitter" aria-hidden="true"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $my_link ?>&quote=<?php echo $my_title; ?>" title="Share on Facebook" target="_blank"><i class="fa fa-facebook-f" aria-hidden="true"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $my_link ?>&title=<?php echo $my_title; ?>&summary=<?php echo $my_excerpt; ?>" target="_blank" title="Share on LinkedIn"><i class="fa fa-linkedin-in" aria-hidden="true"></i></a>
									</li>  
								</ul>
							</div>
							<?php
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							?>
							<div class="relevantarticle">
								<h3 class="rlhtag">
									Relevant Article	
								</h3>
								<?php
								$related = get_posts( 
									array( 
										'category__in' => wp_get_post_categories( get_the_ID() ), 
										'numberposts'  => 2, 
										'post__not_in' => array( get_the_ID() ) 
									)
								);
								
								if( $related ) { 
									echo "<div class='row'>";
									foreach( $related as $post ) {
										setup_postdata($post);
										/*whatever you want to output*/

										$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumbnail');
										?>
										<div class="col-lg-6 col-sm-12">
											<div class="row">
												<div class="col-md-4">
													<?php
													if($featured_img_url){
													?>			
														<img src="<?php echo $featured_img_url; ?>"/>
													<?php
													}
													else{				
														the_custom_logo(1);				
													}
													?>
												</div>
												<div class="col-md-8 leftpadzero">
													<div class="hdiv">
														<h4>
															<a title="Read more: <?php the_title() ?>" href="<?php echo esc_url( get_permalink() ) ?>"><?php the_title() ?></a>
														</h4>
													</div>
													<div class="agodiv">
														<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
														<span class="iconWeek"><?php echo get_the_date(); ?></span>
													</div>
												</div>
											</div>
										</div>
										<?php
									}
									echo "</div>";
									wp_reset_postdata();
								}
								?>
							</div>

						</div>
						<div class="col-md-4 sidebarinner">
							<?php dynamic_sidebar( 'sidebar-main' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</article>
<?php
}
get_footer();