<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>	

<div class="commanContent spacing20n mb-3">
	<?php
	if($featured_img_url){
	?>			
		<img class="d-block mb-4 mr-auto ml-auto" src="<?php echo $featured_img_url; ?>"/>
	<?php
	}
	?>
	<!-- <div class="row">
		<div class="col-md-6">
			<h2 class="pgtitle text-dark">
				<?php the_title() ?>
			</h2>
		</div>
		<div class="col-md-6">
			<div class="textAgo">
				<span class="iconWeek"><?php echo get_the_date(); ?></span>
			</div>
		</div>
	</div> -->
	<!-- <div class="clearfix headingBorder"></div> -->
	<div class="contdiv">
		<?php the_content(); ?>
	</div>
</div>
