<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
$category_object = get_the_category(get_the_ID());
if($category_object){
	$category_names = array();
	foreach ($category_object as $category) {
		$category_names[] = $category->cat_name;
	}
}
?>
<div id="post-<?php the_ID(); ?>" <?php post_class("commanBox col-lg-4 col-md-6"); ?>>
	<div class="shadowbox">
		<div class="row">
			<div class="col-md-12">
			<?php
			if($featured_img_url){
			?>			
				<img src="<?php echo $featured_img_url; ?>"/>
			<?php
			}
			else{				
				the_custom_logo(1);				
			}
			?>
			</div>	
		</div>
		<div class="boxcontentblog">
			<div class="hdiv">
				<h3>
					<a title="Read more: <?php the_title() ?>" href="<?php echo esc_url( get_permalink() ) ?>"><?php the_title() ?></a>
				</h3>
			</div>
			<div class="contdiv">
				<?php
				$my_excerpt = wp_strip_all_tags(substr(get_the_content(), 0, 90));
				echo $my_excerpt;	
				?>
			</div>
			<div class="agodiv">
				<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
				<span class="iconWeek"><?php echo get_the_date(); ?></span>
			</div>
			<?php 
			if($category_names){
			?>
			<div class="category_names">
				Category: <?php echo implode(', ', $category_names); ?>
			</div>
			<?php
			}
			?>
		</div>	
	</div>
</div>
