<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>
<div id="post-<?php the_ID(); ?>" <?php post_class("commanContent"); ?>>
	<div class="row">
		<div class="col-md-6">
			<h5><?php the_title() ?></h5>
		</div>
		<div class="col-md-6">
			<div class="btnsRight">
				<a title="Read more: <?php the_title() ?>" href="<?php echo esc_url( get_permalink() ) ?>" class="btn btn-danger btn-md btnApply">Read more</a>
			</div>
		</div>
	</div>
	<div class="mt-3"></div>
	<?php the_excerpt(); ?>
	<div class="textAgo">
		<span class="iconWeek"><?php echo get_the_date(); ?></span>
	</div>
</div>
