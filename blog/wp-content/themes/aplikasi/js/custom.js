jQuery('.select2').select2();
// jQuery(document).on('click', '.link-cons-cli', function () {
//    jQuery('.page-cons-cli').toggle(200);
// });
jQuery(".submit-search").click(function(e) {
    e.preventDefault();
    jQuery("#search-form").submit();
});
jQuery(".job-submit-search").click(function(e) {
    e.preventDefault();
    jQuery("#job-search-form").submit();
});

// Consultant Category Top
jQuery(".category-search1").click(function() {
    jQuery(".category-options").toggle();
});
// Consultant Category Bottom
jQuery(".category-search2").click(function() {
    jQuery(".category-options2").toggle();
});
// Job Category Top
jQuery(".job-category-search1").click(function() {
    jQuery(".job-category-options").toggle();
});
//Consultant Base Rate Top 
jQuery(".baserate-1").click(function() {
    jQuery(".baserate-1-options").toggle();
});
//Consultant Base Rate Bottom 
jQuery(".baserate-2-lable").click(function() {
    jQuery(".baserate-2-options").toggle();
});
//Job Base Rate Top 
jQuery(".job-baserate-1").click(function() {
    jQuery(".job-baserate-1-options").toggle();
});
//Job Base Rate Bottom 
jQuery(".job-baserate-2-lable").click(function() {
    jQuery(".job-baserate-2-options").toggle();
});
// Work Preference Top
jQuery("#work-preference-lable").click(function() {
    jQuery(".work-preference-options").toggle();
});
// Consultant State Top
jQuery(".consultant-state-lable").click(function() {
    jQuery(".consultant-state-options").toggle();
});
// // consultant mobile state
// jQuery("#mobile-consultant-state-lable").click(function () {
//     jQuery(".mobile-consultant-state-options").toggle();
// });
// Job State Top
jQuery(".job-state-lable").click(function() {
    jQuery(".job-state-options").toggle();
});
// Job Mode Top
jQuery("#job-mode1-lable").click(function() {
    jQuery(".job-mode1-options").toggle();
});
// Job Mode Bottom
jQuery(".job-mode2-lable").click(function() {
    jQuery(".job-mode2-options").toggle();
});
// Job Type Bottom
jQuery(".job-type2-lable").click(function() {
    jQuery(".job-type2-options").toggle();
});
// Job Type Top
jQuery(".job-type1-lable").click(function() {
    jQuery(".job-type1-options").toggle();
});
// Job State Bottom
jQuery(".job-state2-lable").click(function() {
    jQuery(".job-state2-options").toggle();
});
jQuery(".cus-search-toggle").click(function() {
    jQuery(this).toggleClass("cus-search-close");
    jQuery(".header-top .navbar .navbar-collapse").removeClass("show");
    jQuery(".mobile-toggle-show").toggle(200);
});
jQuery(document).on("click", ".close-job-state", function(event) {
    jQuery(".consultant-state-options").hide(100);
});
// Hide toogle div on outside click
jQuery(document).mouseup(function(e) {
    var container = jQuery(".toogle-div");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
    }
});

jQuery(document).on("click", ".login-form-poup", function(event) {

    // jQuery('.attachments-links').hide(200);
    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    event.stopPropagation();
    jQuery(".forgot-form-block").hide();
    jQuery(".login-form-toggle").toggle(200);
    jQuery(".login-form-block").show(200);
});
jQuery(document).on("click", ".forgot-form-show", function() {
    jQuery(".login-form-block").toggle(200);
    jQuery(".forgot-form-block").toggle(200);
});
jQuery(document).on("click", ".login-form-show", function() {
    jQuery(".login-form-block").toggle(200);
    jQuery(".forgot-form-block").toggle(200);
});
jQuery(document).on("click", "body", function() {
    jQuery(".login-form-toggle").hide(200);
});

jQuery(document).on("click", ".login-form-toggle", function(event) {
    event.stopPropagation();
});