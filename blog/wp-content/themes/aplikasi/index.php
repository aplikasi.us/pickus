<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
<article>
	<section class="secColumn">
		<div class="row rowColumns no-gutters">
			<div class="col-md-12 columnRight">
				<div class="mob-pt-5 pt-5">
					
						<!-- <div class="col-md-12"> -->
						<?php
						if ( have_posts() ) {

							global $wp_query;
							$paged = !empty($wp_query->query_vars['paged']) ? $wp_query->query_vars['paged'] : 1;
							$prev_posts = ( $paged - 1 ) * $wp_query->query_vars['posts_per_page'];
							$from = 1 + $prev_posts;
							$to = count( $wp_query->posts ) + $prev_posts;
							$of = $wp_query->found_posts;							

						?>
							<!-- <div class="row pb-3">
								<div class="col-md-6">
									<p class="pb-0"><?php printf( 'Showing %s to %s of %s', $from, $to, $of ); ?></p>
								</div>
							</div> -->
						
						<div class="row">	
						<?php
							// Load posts loop.
							while ( have_posts() ) {
								the_post();
								get_template_part( 'template-parts/content/content', 'newbox' );
							}
						?>
						</div>
						<?php
							
							fellowtuts_wpbs_pagination();
							//twentynineteen_the_posts_navigation();
						}
						?>
						<!-- </div> -->
						<!-- <div class="col-md-4">
							<?php // dynamic_sidebar( 'sidebar-main' ); ?>
						</div> -->
					
				</div>
			</div>
		</div>
	</section>
</article>
<?php
get_footer();
