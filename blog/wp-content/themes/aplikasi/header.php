<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <?php wp_head(); ?>
    </head>

    <?php
    $current_blog_id = get_current_blog_id();
    $blog_id = 1;
    switch_to_blog( $blog_id );
    //Do stuff
    $var = network_site_url();
    $mainDomainurl = str_replace('blog/', '', $var);
    $territory = "MY";
    if(Session::get('territory') == 'my'){
        $territory = "MY";
    }
    if(Session::get('territory') == 'ph'){
        $territory = "PH";
    }
    $searchurl = $mainDomainurl.$territory.'/general-search';
    
    $profileUrl = $mainDomainurl.$territory.'/general-search';
    
    ?>
    <body <?php body_class("blog_".$current_blog_id); ?>>
    <?php if (!Auth::check()) { ?>
        <div class="login-form-toggle">
            <div class="login-form-block">
                <form role="form" method="post" action="" id="consultant-login">
                    <?php echo csrf_field(); ?>
                    <div class="errormsg"></div>
                    <div class="header-login-toggle">
                        <h4 class="  w-100 font-weight-normal  text-black">Login</h4>
                    </div>
                    <div class="login-form-field">
                        <div id="login-error-message"></div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <input type="hidden" name="redirect_url" id="redirect_url" value="" />
                                    <!-- <label class="control-label">Email Address: </label> -->
                                    <?php if(Session::get('loginemail') != null) { ?>
                                    <input type="text" id="email" name="email" value="{{Session::get('loginemail')}}"
                                        class="form-control" placeholder="Enter email address">
                                    <?php }else { ?>
                                    <input type="text" id="email" name="email" class="form-control"
                                        placeholder="Enter email address">
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <!-- <label class="control-label">Password: </label> -->
                                    <input type="password" id="password" name="password" class="form-control"
                                        placeholder="Enter your password">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-login-blue" type="submit">Get Me In</button>
                        <img src="<?php echo get_site_url() ?>/wp-content/themes/aplikasi/img/loader.svg"" id="login-loading"
                            style="display: none;">
                        <p class="pb-0 pt-1"><a
                                class="text-light text-underline hvr_clrp d-block close-loginform forgot-form-show f-16 pd-0"
                                href="javascript:;"><small>Forgot password? Please click
                                    here</small></a></p>

                    </div>
                </form>
            </div>
            <div class="forgot-form-block">
                <form role="form" method="post" action="" id="forgotpassword">
                
                    <div class="errormsg-ps"></div>
                    <div class="sucessmsg"></div>
                    <div class="text-">
                        <h4 class="w-100 font-weight-normal text-black">Forgot Password</h4>
                    </div>
                    <div class="forgot-body">
                        <div id="forgot-message"></div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" id="email" name="email" class="form-control"
                                        placeholder="Enter email address">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-login-blue" type="submit">SUBMIT</button>
                        <img src="<?php echo get_site_url() ?>/wp-content/themes/aplikasi/img/loader.svg" id="forgot-loading"
                            style="display: none;">
                        <p class="pb-0"><a
                                class="text-light text-underline hvr_clrp d-block close-loginform login-form-show f-14 pd-0"
                                href="javascript:;"><small>Login?</small></a></p>
                    </div>
                </form>
            </div>

        </div>
    <?php } ?>
        <div class="tab-mobile-show mobile-header-search">
            <header>
                <div class="header-top">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg " id="mainNav">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                            </button>
                            <?php
                            $defaults = array(
                                'theme_location' => 'menu-1',
                                'container' => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id' => 'navbarSupportedContent',
                                'menu_class' => 'navbar-nav ml-auto',
                                'walker' => new Bootstrap_NavWalker(),
                                'fallback_cb' => 'Bootstrap_NavWalker::fallback',
                            );
                            wp_nav_menu($defaults);
                            ?>
                        </nav>
                        <div class="mobile-logo show cus-mobile-show">
                            <?php the_custom_logo(); ?>
                        </div>
                        <div class="iam-hover-effect">
                            <?php if (Auth::check()) { 
                                $profileUrl = $mainDomainurl.$territory.'/consultant';
                                if (Auth::user()->role_id == 1 ) {
                                    $profileUrl =  $mainDomainurl.$territory.'/client';
                                }
                                ?>
                                
                                <a class="btn btn-darkgreen btn-big  btnQuestions link-cons-cli " href="<?php echo $profileUrl; ?>">
                                    <i class="fas fa-user" aria-hidden="true"></i>
                                </a>
                            <?php } else{ ?>
                                <a class="btn btn-darkgreen btn-big  btnQuestions link-cons-cli " href="<?php echo $profileUrl; ?>">I am a..</a>
                                <div class="iam-box-hvr">
                                    <div class="open page-cons-cli">
                                        <ul class="i-con-cli">
                                            <li class="nav-item">
                                                <a class="nav-link" href="http://206.189.86.153/MY/consultant/registration" title="CONSULTANT">CONSULTANT</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="http://206.189.86.153/MY/client/registration" title="CLIENT">CLIENT</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="cus-search-toggle cus-mobile-show">
                            <i class="fas fa-search colorWhite"></i>
                        </div>
                        
                    </div>
                </div>
                <div class="headerMain mobile-toggle-show">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-8">
                                <div class="boxTabsearch">
                                    <ul class="nav nav-tabs" id="mobile-search-job-consultants-section" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link <?php 
                                    if (Auth::check()) { 
                                        if (Auth::user()->role_id == 1 ) {
                                            echo "active";
                                        }
                                    }
                                    ?>" id="mobile-search-consultants" data-toggle="tab" href="#mobile-search-consultants-tab" role="tab" aria-controls="search-consultants-tab"
                                            aria-selected="true">Search Consultants</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php 
                                    if (Auth::check()) { 
                                        if (Auth::user()->role_id != 1 ) {
                                            echo "active";
                                        }
                                    }else{
                                        echo "active";
                                    }
                                    ?>" id="mobile-search-jobs" data-toggle="tab" href="#mobile-search-jobs-tab" role="tab" aria-controls="search-jobs-tab"
                                            aria-selected="false">Search Jobs</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="mobile-search-job-consultants-content">
                                        <div class="tab-pane fade <?php 
                                    if (Auth::check()) { 
                                        if (Auth::user()->role_id == 1 ) {
                                            echo "show active";
                                        }
                                    }
                                    ?>" id="mobile-search-consultants-tab" role="tabpanel" aria-labelledby="search-consultants">
                                            <form  method="GET" action="<?php echo $searchurl; ?>" accept-charset="UTF-8" id="search-form" autocomplete="off" >
                                                <input type="hidden" name="search_type" id="search_type" class="both-search_type" value="consultant">
                                                <div class="row mb-2">
                                                    <div class="col-12 form-group mt-2 mb-2">
                                                        <div class="input-group">
                                                            <!-- <div class="input-group-prepend">
                                                                <div class="input-group-text">
                                                                    <i class="fas fa-search"></i>
                                                                </div>
                                                            </div> -->
                                                            <input class="form-control search-consulresult" name="searchterm" type="text" placeholder="Search by Consultant ID, Job Title or Skill" autocomplete="off">
                                                            <div class="consutant-suggestions"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 form-group mt-2 mb-2">
                                                        <!-- <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Categories</option>
                                                            <option>Franchise</option>
                                                            <option>Accounting</option>
                                                            <option>Marketing</option>
                                                            <option>Research</option>
                                                            <option>Real Estate</option>
                                                            <option>Telecommunications</option>
                                                        </select> -->
                                                        <span class="form-control category-search1" id="category-search1">Categories</span>
                                                        <img class="image-click-cat" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                        <div class="category-options form-control toogle-div" >
                                                            <ul>
                                                                <li>
                                                                    <input type="checkbox" name="category[]" value="Basis/S &amp; A">Basis/S &amp; A
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="category[]" value="Functional">Functional
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="category[]" value="Technical">Technical
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="category[]" value="Project Manager/Solution Architect">Project Manager/Solution Architect
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="category[]" value="Others">Others
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 form-group mt-2 mb-2">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <input type="text" name="baserate_from" value="<?php (isset($baserate_from)) ? $baserate_from : ''?>" id="baserate_from" class="baserate_number form-control" placeholder="Base Rate From"/>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="text" name="baserate_to" value="<?php (isset($baserate_to)) ? $baserate_to : '' ?>" id="baserate_to" class="baserate_number form-control" placeholder="Base Rate To"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 form-group mt-2 mb-2">
                                                        <!-- <div class="row">
                                                            <div class="col-sm-6 form-group">
                                                                <button type="submit" class="btn btn-danger btn-block" href="#">Search</button>
                                                            </div>
                                                            <div class="col-sm-6 form-group">
                                                                <a class="btn btn-outline-light btn-block" href="http://206.189.86.153/MY/general-search?search_type=consultant">View all</a>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <ul class="mobileheader-checkbox">
                                                            <li>
                                                                <input type="checkbox" name="work_preference[]" value="Full Time">Full Time
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="work_preference[]" value="Part Time">Part Time
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="work_preference[]" value="Willing to travel">Willing to travel
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-6 col-md-auto form-group selecttransparent">
                                                        <select class="form-control select2" name="experience" style="width: 100%">
                                                            <option value="" selected="selected">Experience</option>
                                                            <option value="0">Fresh</option>
                                                            <option value="3">1-3 Years</option>
                                                            <option value="5">4-5 Years</option>
                                                            <option value="10">5-10 Years</option>
                                                            <option value="10plus">10+ Years</option>
                                                        </select>
                                                        <select class="form-control select2" name="availability_date" style="width: 100%">
                                                            <option value="" selected="selected">Availability Date</option>
                                                            <option value="week">Within Week</option>
                                                            <option value="month">Within Month</option>
                                                            <option value="three">Within 3 Months</option>
                                                            <option value="six">Within 6 Months</option>
                                                            <option value="year">Within Year</option>
                                                        </select>
                                                    
                                                        <div class="form-group selecttransparent" style="">
                                                            
                                                            <span class="consultant-state-lable font-weight-normal pr-3" id="consultant-state-lable">State</span>
                                                            <img class=""  src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                            <div class="consultant-state-options form-control toogle-div" style="display: none;">
                                                                <ul>
                                                                    <li class="close-job-state">Close</li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Johor">Johor
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Kedah">Kedah
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Kelantan">Kelantan
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Melaka">Melaka
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Negeri Sembilan">Negeri Sembilan
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Pahang">Pahang
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Perak">Perak
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Perlis">Perlis
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Pulau Pinang">Pulau Pinang
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Sabah">Sabah
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Sarawak">Sarawak
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Selangor">Selangor
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Terengganu">Terengganu
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="W.P. Kuala Lumpur">W.P. Kuala Lumpur
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="W.P. Labuan">W.P. Labuan
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="W.P. Putrajaya">W.P. Putrajaya
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Kuala Lumpur">Kuala Lumpur
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="state[]" class="" value="Labuan">Labuan
                                                                    </li>    
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                        <div class="col-12 form-group mt-5">
                                                            <button type="submit" class="btn btn-danger btn-block border-rad-9" href="#">Search</button>
                                                        </div>
                                                        <div class="col-12 form-group mt-2">
                                                            <a class="btn btn-outline-light btn-block border-rad-9" href="<?php echo $searchurl; ?>?search_type=consultant">View all</a>
                                                        </div>
                                                    </div> 
                                            </form>
                                        </div>
                                        <div class="tab-pane fade  <?php 
                                    if (Auth::check()) { 
                                        if (Auth::user()->role_id != 1 ) {
                                            echo "show active";
                                        }
                                    }else{
                                        echo "show active";
                                    }
                                    ?>" id="mobile-search-jobs-tab" role="tabpanel" aria-labelledby="search-jobs">
                                            <form method="GET" action="<?php echo $searchurl; ?>" id="job-search-form">
                                            <input type="hidden" name="search_type" id="search_type" class="both-search_type" value="job">
                                                <div class="row mb-2">
                                                    <div class="col-12 form-group mt-2 mb-2">
                                                        <div class="input-group">
                                                            <!-- <div class="input-group-prepend">
                                                                <div class="input-group-text">
                                                                    <i class="fas fa-search"></i>
                                                                </div>
                                                            </div> -->
                                                            <input class="form-control search-jobresult" name="jobsearchterm" type="text" placeholder="Search by Job ID, Job Title or Skills" autocomplete="off">
                                                            <div class="job-suggestions"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 form-group mt-2 mb-2">
                                                            <span class="form-control category-search1" id="category-search1">Categories</span>
                                                            <img class="image-click-cat" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                        <div class="category-options form-control toogle-div" >
                                                            <ul>
                                                                <li>
                                                                    <input type="checkbox" name="jobcategory[]" value="Basis/S &amp; A">Basis/S &amp; A
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="jobcategory[]" value="Functional">Functional
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="jobcategory[]" value="Technical">Technical
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="jobcategory[]" value="Project Manager/Solution Architect">Project Manager/Solution Architect
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="jobcategory[]" value="Others">Others
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 form-group mt-2 mb-2">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <input type="text" name="job_baserate_from" value="<?php (isset($job_baserate_from)) ? $job_baserate_from : ''?>" id="job_baserate_from" class="baserate_number form-control" placeholder="Base Rate From"/>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="text" name="job_baserate_to" value="<?php (isset($job_baserate_to)) ? $job_baserate_to : '' ?>" id="job_baserate_to" class="baserate_number form-control" placeholder="Base Rate To"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <ul class="mobileheader-checkbox">
                                                            <li>
                                                                <input type="checkbox" name="job_mode1[]" class="subfilter" value="Full Time">Full Time
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="job_mode1[]" class="subfilter" value="Part Time">Part Time
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="job_type[]" class="" value="Contract">Contract
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="job_type[]" class="" value="Permanent">Permanent
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="job_type[]" class="" value="Internship">Internship
                                                            </li>    
                                                        </ul>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="col-12 col-md-auto form-group selecttransparent">
                                                            <select class="form-control select2" name="work_experience" style="width: 100%">
                                                                <option value="" selected="selected">Experience</option>
                                                                <option value="0">Fresh</option>
                                                                <option value="3">1-3 Years</option>
                                                                <option value="5">4-5 Years</option>
                                                                <option value="10">5-10 Years</option>
                                                                <option value="10plus">10+ Years</option>
                                                            </select>
                                                        </div>   
                                                        <div class="col-12 col-md-auto form-group selecttransparent" >
                                                            <select class="form-control select2 select2-hidden-accessible" name="job_start_date"  aria-hidden="true" style="width: 100%">
                                                                <option value="" selected="selected">Start Date</option>
                                                                <option value="week">Within Week</option>
                                                                <option value="month">Within Month</option>
                                                                <option value="three">Within 3 Months</option>
                                                                <option value="six">Within 6 Months</option>
                                                                <option value="year">Within Year</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12 col-md-auto form-group selecttransparent" >
                                                            <select class="form-control select2 select2-hidden-accessible" name="job_duration"  aria-hidden="true" style="width: 100%">
                                                                <option value="" selected="selected">Duration</option>
                                                                <option value="3">1-3 months</option>
                                                                <option value="6">4-6 months</option>
                                                                <option value="9">6-12 months</option>
                                                                <option value="12">&gt; 12 months</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12 col-md-auto form-group selecttransparent">
                                                                <select class="form-control select2 select2-hidden-accessible" style="width:100%;" name="job_industry" tabindex="-1" aria-hidden="true"><option value="" selected="selected">Industry</option><option value="Communications">Communications</option><option value="Technology">Technology</option><option value="Government/Military">Government/Military</option><option value="Manufacturing">Manufacturing</option><option value="Financial Services">Financial Services</option><option value="IT Services">IT Services</option><option value="Education">Education</option><option value="Pharma">Pharma</option><option value="Real Estate">Real Estate</option><option value="Consulting">Consulting</option><option value="Health Care">Health Care</option><option value="Administration">Administration</option><option value="Advertising">Advertising</option><option value="Agriculture">Agriculture</option><option value="Architecture &amp; Construction">Architecture &amp; Construction</option><option value="Arts &amp; Graphics">Arts &amp; Graphics</option><option value="Airline - Aviation">Airline - Aviation</option><option value="Accounting">Accounting</option><option value="Automotive">Automotive</option><option value="Banking">Banking</option><option value="Biotechnology">Biotechnology</option><option value="Broadcasting">Broadcasting</option><option value="Business Management">Business Management</option><option value="Charity">Charity</option><option value="Catering">Catering</option><option value="Customer Service">Customer Service</option><option value="Chemicals">Chemicals</option><option value="Construction">Construction</option><option value="Computer">Computer</option><option value="Consumer">Consumer</option><option value="Cosmetics">Cosmetics</option><option value="Design">Design</option><option value="Defence">Defence</option><option value="Electronics">Electronics</option><option value="Engineering">Engineering</option><option value="Energy and Utilities">Energy and Utilities</option><option value="Entertainment">Entertainment</option><option value="Employment - Recruiting - Staffing">Employment - Recruiting - Staffing</option><option value="Environmental">Environmental</option><option value="Exercise - Fitness">Exercise - Fitness</option><option value="Export/Import">Export/Import</option><option value="Fashion">Fashion</option><option value="FMCG/Foods/Beverage">FMCG/Foods/Beverage</option><option value="Fertilizers/Pesticides">Fertilizers/Pesticides</option><option value="Furniture">Furniture</option><option value="Grocery">Grocery</option><option value="Gas">Gas</option><option value="Government &amp; Public Sector">Government &amp; Public Sector</option><option value="Gems &amp; Jewellery">Gems &amp; Jewellery</option><option value="Human Resources">Human Resources</option><option value="Hospitality">Hospitality</option><option value="Hotels and Lodging">Hotels and Lodging</option><option value="HVAC">HVAC</option><option value="Hardware">Hardware</option><option value="Insurance">Insurance</option><option value="Installation">Installation</option><option value="Industrial">Industrial</option><option value="Internet Services">Internet Services</option><option value="Import - Export">Import - Export</option><option value="Legal">Legal</option><option value="Logistics">Logistics</option><option value="Landscaping">Landscaping</option><option value="Leisure and Sport">Leisure and Sport</option><option value="Library Science">Library Science</option><option value="Marketing">Marketing</option><option value="Management">Management</option><option value="Merchandising">Merchandising</option><option value="Medical">Medical</option><option value="Media">Media</option><option value="Metals">Metals</option><option value="Mining">Mining</option><option value="Mortgage">Mortgage</option><option value="Marine">Marine</option><option value="Maritime">Maritime</option><option value="Nonprofit Charitable Organizations">Nonprofit Charitable Organizations</option><option value="NGO/Social Services">NGO/Social Services</option><option value="Newspaper">Newspaper</option><option value="Oil &amp; Gas">Oil &amp; Gas</option><option value="Other">Other</option><option value="Other/Not Classified">Other/Not Classified</option><option value="Polymer / Plastic / Rubber">Polymer / Plastic / Rubber</option><option value="Pharma/Biotech/Clinical Research">Pharma/Biotech/Clinical Research</option><option value="Public Sector and Government">Public Sector and Government</option><option value="Printing/Packaging/Publishing">Printing/Packaging/Publishing</option><option value="Personal and Household Services">Personal and Household Services</option><option value="Property &amp; Real Estate">Property &amp; Real Estate</option><option value="Paper">Paper</option><option value="Pet Store">Pet Store</option><option value="Public Relations">Public Relations</option><option value="Retail">Retail</option><option value="Retail &amp; Wholesale">Retail &amp; Wholesale</option><option value="Recreation">Recreation</option><option value="Real Estate and Property">Real Estate and Property</option><option value="Recruitment/Employment Firm">Recruitment/Employment Firm</option><option value="Real Estate/Property Management">Real Estate/Property Management</option><option value="Restaurant/Food Services">Restaurant/Food Services</option><option value="Rental Services">Rental Services</option><option value="Research &amp; Development">Research &amp; Development</option><option value="Repair / Maintenance Services">Repair / Maintenance Services</option><option value="Sales - Marketing">Sales - Marketing</option><option value="Science &amp; Technology">Science &amp; Technology</option><option value="Security/Law Enforcement">Security/Law Enforcement</option><option value="Shipping/Marine">Shipping/Marine</option><option value="Security and Surveillance">Security and Surveillance</option><option value="Sports and Physical Recreation">Sports and Physical Recreation</option><option value="Staffing/Employment Agencies">Staffing/Employment Agencies</option><option value="Social Services">Social Services</option><option value="Sports Leisure &amp; Lifestyle">Sports Leisure &amp; Lifestyle</option><option value="Semiconductor">Semiconductor</option><option value="Services - Corporate B2B">Services - Corporate B2B</option><option value="Travel">Travel</option><option value="Training">Training</option><option value="Transportation">Transportation</option><option value="Telecommunications">Telecommunications</option><option value="Trade and Services">Trade and Services</option><option value="Travel and Tourism">Travel and Tourism</option><option value="Textiles/Garments/Accessories">Textiles/Garments/Accessories</option><option value="Tyres">Tyres</option><option value="Utilities">Utilities</option><option value="Wireless">Wireless</option><option value="Wood / Fibre / Paper">Wood / Fibre / Paper</option><option value="Waste Management">Waste Management</option><option value="Wholesale Trade/Import-Export">Wholesale Trade/Import-Export</option></select>
                                                        </div>
                                                        
                                                        <div class="col-12 col-md-auto form-group selecttransparent" style="">
                                                            <span class="consultant-state-lable font-weight-normal pr-3" id="consultant-state-lable">State</span>
                                                            <img class=""  src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                            <div class="consultant-state-options form-control toogle-div" style="display: none;">
                                                                <ul>
                                                                    <li class="close-job-state">Close</li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Johor">Johor
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Kedah">Kedah
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Kelantan">Kelantan
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Melaka">Melaka
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Negeri Sembilan">Negeri Sembilan
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Pahang">Pahang
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Perak">Perak
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Perlis">Perlis
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Pulau Pinang">Pulau Pinang
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Sabah">Sabah
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Sarawak">Sarawak
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Selangor">Selangor
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Terengganu">Terengganu
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="W.P. Kuala Lumpur">W.P. Kuala Lumpur
                                                                    </li>
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="W.P. Labuan">W.P. Labuan
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="W.P. Putrajaya">W.P. Putrajaya
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Kuala Lumpur">Kuala Lumpur
                                                                    </li>    
                                                                    <li>
                                                                        <input type="checkbox" name="job_state[]" class="" value="Labuan">Labuan
                                                                    </li>    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 form-group mt-2">
                                                        <button type="submit" class="btn btn-danger btn-block border-rad-9" href="#">Search</button>
                                                    </div>
                                                    <div class="col-sm-12 form-group mt-2">
                                                        <a class="btn btn-outline-light btn-block" href="<?php echo $searchurl; ?>?search_type=job border-rad-9">View all</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div class="desktop-mobile-show ">
            <header>
                <div class="header-top">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg " id="mainNav">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <?php
                            $defaults = array(
                                'theme_location' => 'menu-1',
                                'container' => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id' => 'navbarSupportedContent',
                                'menu_class' => 'navbar-nav ml-auto',
                                'walker' => new Bootstrap_NavWalker(),
                                'fallback_cb' => 'Bootstrap_NavWalker::fallback',
                            );
                            wp_nav_menu($defaults);
                            
                            ?>
                        </nav>
                    </div>
                </div>
            </header>
        </div>
            
        <div class="headerMain desktop-mobile-show sticky-header">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-2 text-right">
                        <h1 id="logo">
                            <?php the_custom_logo(); ?>
                        </h1>
                    </div>
                    <div class="col-md-8">
                        <div class="boxTabsearch">
                            <ul class="nav nav-tabs" id="search-job-consultants-section" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php 
                                    if (Auth::check()) { 
                                        if (Auth::user()->role_id == 1 ) {
                                            echo "active";
                                        }
                                    }

                                    ?>" id="search-consultants" data-toggle="tab" href="#search-consultants-tab" role="tab" aria-controls="search-consultants-tab"
                                    aria-selected="true">Search Consultants</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  <?php 
                                    if (Auth::check()) { 
                                        if (Auth::user()->role_id != 1 ) {
                                            echo "active";
                                        }
                                    }else{
                                        echo "active";
                                    }
                                    ?>" id="search-jobs" data-toggle="tab" href="#search-jobs-tab" role="tab" aria-controls="search-jobs-tab"
                                    aria-selected="false">Search Jobs</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="search-job-consultants-content">
                                <div class="tab-pane fade <?php 
                                if (Auth::check()) { 
                                    if (Auth::user()->role_id == 1 ) {
                                        echo "show active";
                                    }
                                } ?>" id="search-consultants-tab" role="tabpanel" aria-labelledby="search-consultants">
                                    <form  method="GET" action="<?php echo $searchurl; ?>" accept-charset="UTF-8" id="search-form" autocomplete="off" >
                                        <input type="hidden" name="search_type" id="search_type" class="both-search_type" value="consultant">
                                        <div class="row mb-2">
                                            <div class="col-sm-6 col-md-3">
                                                <!-- <select class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">Categories</option>
                                                    <option>Franchise</option>
                                                    <option>Accounting</option>
                                                    <option>Marketing</option>
                                                    <option>Research</option>
                                                    <option>Real Estate</option>
                                                    <option>Telecommunications</option>
                                                </select> -->
                                                <span class="form-control category-search1" id="category-search1">Categories</span>
                                                <img class="image-click-cat" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                <div class="category-options form-control toogle-div" >
                                                    <ul>
                                                        <li>
                                                            <input type="checkbox" name="category[]" value="Basis/S &amp; A">Basis/S &amp; A
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="category[]" value="Functional">Functional
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="category[]" value="Technical">Technical
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="category[]" value="Project Manager/Solution Architect">Project Manager/Solution Architect
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="category[]" value="Others">Others
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-search"></i>
                                                        </div>
                                                    </div>
                                                    <input class="form-control search-consulresult" name="searchterm" type="text" placeholder="Search by Consultant ID, Job Title or Skill">
                                                    <div class="consutant-suggestions"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-3">
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <button type="submit" class="btn btn-danger btn-block border-rad-9" href="#">Search</button>
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <a class="btn btn-outline-light btn-block border-rad-9" href="<?php echo $searchurl; ?>?search_type=consultant">View all</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <span class="baserate-1 font-weight-normal pr-3" id="baserate-1">Base Rate</span>
                                                <img class="image-click-ab" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                <div class="baserate-1-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        <li>
                                                            <input type="text" name="baserate_from" value="<?php (isset($baserate_from)) ? $baserate_from : ''?>" id="baserate_from" class="baserate_number" placeholder="Base Rate From"/>
                                                        </li>
                                                        <li>
                                                            <input type="text" name="baserate_to" value="<?php (isset($baserate_to)) ? $baserate_to : '' ?>" id="baserate_to" class="baserate_number" placeholder="Base Rate To"/>
                                                        </li>
                                                        <!-- <li>
                                                            <button type="button" class="btn btn-secondary clt-blue submit-search font-weight-normal">Apply</button>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <select class="form-control select2" name="experience" style="width: 100%">
                                                    <option value="" selected="selected">Experience</option>
                                                    <option value="0">Fresh</option>
                                                    <option value="3">1-3 Years</option>
                                                    <option value="5">4-5 Years</option>
                                                    <option value="10">5-10 Years</option>
                                                    <option value="10plus">10+ Years</option>
                                                </select>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <select class="form-control select2" name="availability_date" style="width: 100%">
                                                    <option value="" selected="selected">Availability Date</option>
                                                    <option value="week">Within Week</option>
                                                    <option value="month">Within Month</option>
                                                    <option value="three">Within 3 Months</option>
                                                    <option value="six">Within 6 Months</option>
                                                    <option value="year">Within Year</option>
                                                </select>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <span class="work-preference-lable font-weight-normal pr-3" id="work-preference-lable">Work Preferences</span>
                                                <img class="image-click-ab"  src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                <div class="work-preference-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        <li>
                                                            <input type="checkbox" name="work_preference[]" value="Full Time">Full Time
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="work_preference[]" value="Part Time">Part Time
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="work_preference[]" value="Willing to travel">Willing to travel
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="">
                                                <span class="consultant-state-lable font-weight-normal pr-3" id="consultant-state-lable">State</span>
                                                <img class="image-click-ab"  src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                <div class="consultant-state-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Johor">Johor
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Kedah">Kedah
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Kelantan">Kelantan
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Melaka">Melaka
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Negeri Sembilan">Negeri Sembilan
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Pahang">Pahang
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Perak">Perak
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Perlis">Perlis
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Pulau Pinang">Pulau Pinang
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Sabah">Sabah
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Sarawak">Sarawak
                                                        </li>    
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Selangor">Selangor
                                                        </li>    
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Terengganu">Terengganu
                                                        </li>    
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="W.P. Kuala Lumpur">W.P. Kuala Lumpur
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="W.P. Labuan">W.P. Labuan
                                                        </li>    
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="W.P. Putrajaya">W.P. Putrajaya
                                                        </li>    
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Kuala Lumpur">Kuala Lumpur
                                                        </li>    
                                                        <li>
                                                            <input type="checkbox" name="state[]" class="" value="Labuan">Labuan
                                                        </li>    
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade <?php
                                if (Auth::check()) { 
                                    if (Auth::user()->role_id != 1 ) {
                                        echo "show active";
                                    }
                                }else{
                                    echo "show active";
                                }
                                ?>" id="search-jobs-tab" role="tabpanel" aria-labelledby="search-jobs">
                                    <form method="GET" action="<?php echo $searchurl; ?>" id="job-search-form">
                                    <input type="hidden" name="search_type" id="search_type" class="both-search_type" value="job">
                                        <div class="row mb-2">
                                            <div class="col-sm-6 col-md-3 form-group">
                                                <!-- <select class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">Categories</option>
                                                    <option>Franchise</option>
                                                    <option>Accounting</option>
                                                    <option>Marketing</option>
                                                    <option>Research</option>
                                                    <option>Real Estate</option>
                                                    <option>Telecommunications</option>
                                                </select> -->
                                                <span class="form-control category-search1" id="category-search1">Categories</span>
                                                <img class="image-click-cat" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                <div class="category-options form-control toogle-div" >
                                                    <ul>
                                                        <li>
                                                            <input type="checkbox" name="jobcategory[]" value="Basis/S &amp; A">Basis/S &amp; A
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="jobcategory[]" value="Functional">Functional
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="jobcategory[]" value="Technical">Technical
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="jobcategory[]" value="Project Manager/Solution Architect">Project Manager/Solution Architect
                                                        </li>
                                                        <li>
                                                            <input type="checkbox" name="jobcategory[]" value="Others">Others
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-5 col-lg-6 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-search"></i>
                                                        </div>
                                                    </div>
                                                    <input class="form-control search-jobresult" name="jobsearchterm" type="text" placeholder="Search by Job ID, Job Title or Skills" autocomplete="off">
                                                    <div class="job-suggestions"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-3 p-md-l-5">
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <button type="submit" class="btn btn-danger btn-block border-rad-9" href="#">Search</button>
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <a class="btn btn-outline-light btn-block border-rad-9" href="<?php echo $searchurl; ?>?search_type=job">View all</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-12">
                                                <div class="row">
                                                    
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                        <!--{{Form::select('job_baserate',[null => 'Base Rate'] + $baserateArray,(isset($job_baserate) && $job_baserate != null) ? $job_baserate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                        <span class="job-baserate-1 font-weight-normal pr-3" id="job-baserate-1 ">Base Rate</span>
                                                        <img  class="image-click-ab" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                        <div class="job-baserate-1-options form-control toogle-div" style="display: none;">
                                                            <ul>
                                                                <li>
                                                                    <input type="text" name="job_baserate_from" value="<?php (isset($job_baserate_from)) ? $job_baserate_from : ''?>" id="job_baserate_from" class="baserate_number" placeholder="Base Rate From"/>
                                                                </li>
                                                                <li>
                                                                    <input type="text" name="job_baserate_to" value="<?php (isset($job_baserate_to)) ? $job_baserate_to : '' ?>" id="job_baserate_to" class="baserate_number" placeholder="Base Rate To"/>
                                                                </li>
                                                                <!-- <li>
                                                                    <button type="button" class="btn btn-secondary clt-blue job-submit-search font-weight-normal">Apply</button>
                                                                </li> -->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                        <!--{{Form::select('job_mode1',[null => 'Job Mode'] + $jobModeList, (isset($job_mode1) && $job_mode1 != null) ? $job_mode1 : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                        <span class="job-mode1-lable font-weight-normal pr-3" id="job-mode1-lable">Job Mode</span>
                                                        <img class="image-click-ab" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                        <div class="job-mode1-options form-control toogle-div" style="display: none;">
                                                            <ul>
                                                                <li>
                                                                    <input type="checkbox" name="job_mode1[]" class="subfilter" value="Full Time">Full Time
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_mode1[]" class="subfilter" value="Part Time">Part Time
                                                                </li>
                                                            </ul>   
                                                        </div>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                        <!--{{Form::select('job_mode1',[null => 'Job Mode'] + $jobModeList, (isset($job_mode1) && $job_mode1 != null) ? $job_mode1 : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                        <span class="job-type1-lable font-weight-normal pr-3" id="job-type1-lable">Job Type</span>
                                                        <img class="image-click-ab" src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                        <div class="job-type1-options form-control toogle-div" style="display: none;">
                                                            <ul>
                                                                <li>
                                                                    <input type="checkbox" name="job_type[]" class="" value="Contract">Contract
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_type[]" class="" value="Permanent">Permanent
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_type[]" class="" value="Internship">Internship
                                                                </li>    
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                        <select class="form-control select2" name="work_experience" style="width: 100%">
                                                            <option value="" selected="selected">Experience</option>
                                                            <option value="0">Fresh</option>
                                                            <option value="3">1-3 Years</option>
                                                            <option value="5">4-5 Years</option>
                                                            <option value="10">5-10 Years</option>
                                                            <option value="10plus">10+ Years</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" >
                                                        <select class="form-control select2 select2-hidden-accessible" name="job_start_date"  aria-hidden="true" style="width: 100%">
                                                            <option value="" selected="selected">Start Date</option>
                                                            <option value="week">Within Week</option>
                                                            <option value="month">Within Month</option>
                                                            <option value="three">Within 3 Months</option>
                                                            <option value="six">Within 6 Months</option>
                                                            <option value="year">Within Year</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" >
                                                        <select class="form-control select2 select2-hidden-accessible" name="job_duration"  aria-hidden="true" style="width: 100%">
                                                            <option value="" selected="selected">Duration</option>
                                                            <option value="3">1-3 months</option>
                                                            <option value="6">4-6 months</option>
                                                            <option value="9">6-12 months</option>
                                                            <option value="12">&gt; 12 months</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent job_industry-width">
                                                        <select class="form-control select2 select2-hidden-accessible" style="width:100%;" name="job_industry" tabindex="-1" aria-hidden="true"><option value="" selected="selected">Industry</option><option value="Communications">Communications</option><option value="Technology">Technology</option><option value="Government/Military">Government/Military</option><option value="Manufacturing">Manufacturing</option><option value="Financial Services">Financial Services</option><option value="IT Services">IT Services</option><option value="Education">Education</option><option value="Pharma">Pharma</option><option value="Real Estate">Real Estate</option><option value="Consulting">Consulting</option><option value="Health Care">Health Care</option><option value="Administration">Administration</option><option value="Advertising">Advertising</option><option value="Agriculture">Agriculture</option><option value="Architecture &amp; Construction">Architecture &amp; Construction</option><option value="Arts &amp; Graphics">Arts &amp; Graphics</option><option value="Airline - Aviation">Airline - Aviation</option><option value="Accounting">Accounting</option><option value="Automotive">Automotive</option><option value="Banking">Banking</option><option value="Biotechnology">Biotechnology</option><option value="Broadcasting">Broadcasting</option><option value="Business Management">Business Management</option><option value="Charity">Charity</option><option value="Catering">Catering</option><option value="Customer Service">Customer Service</option><option value="Chemicals">Chemicals</option><option value="Construction">Construction</option><option value="Computer">Computer</option><option value="Consumer">Consumer</option><option value="Cosmetics">Cosmetics</option><option value="Design">Design</option><option value="Defence">Defence</option><option value="Electronics">Electronics</option><option value="Engineering">Engineering</option><option value="Energy and Utilities">Energy and Utilities</option><option value="Entertainment">Entertainment</option><option value="Employment - Recruiting - Staffing">Employment - Recruiting - Staffing</option><option value="Environmental">Environmental</option><option value="Exercise - Fitness">Exercise - Fitness</option><option value="Export/Import">Export/Import</option><option value="Fashion">Fashion</option><option value="FMCG/Foods/Beverage">FMCG/Foods/Beverage</option><option value="Fertilizers/Pesticides">Fertilizers/Pesticides</option><option value="Furniture">Furniture</option><option value="Grocery">Grocery</option><option value="Gas">Gas</option><option value="Government &amp; Public Sector">Government &amp; Public Sector</option><option value="Gems &amp; Jewellery">Gems &amp; Jewellery</option><option value="Human Resources">Human Resources</option><option value="Hospitality">Hospitality</option><option value="Hotels and Lodging">Hotels and Lodging</option><option value="HVAC">HVAC</option><option value="Hardware">Hardware</option><option value="Insurance">Insurance</option><option value="Installation">Installation</option><option value="Industrial">Industrial</option><option value="Internet Services">Internet Services</option><option value="Import - Export">Import - Export</option><option value="Legal">Legal</option><option value="Logistics">Logistics</option><option value="Landscaping">Landscaping</option><option value="Leisure and Sport">Leisure and Sport</option><option value="Library Science">Library Science</option><option value="Marketing">Marketing</option><option value="Management">Management</option><option value="Merchandising">Merchandising</option><option value="Medical">Medical</option><option value="Media">Media</option><option value="Metals">Metals</option><option value="Mining">Mining</option><option value="Mortgage">Mortgage</option><option value="Marine">Marine</option><option value="Maritime">Maritime</option><option value="Nonprofit Charitable Organizations">Nonprofit Charitable Organizations</option><option value="NGO/Social Services">NGO/Social Services</option><option value="Newspaper">Newspaper</option><option value="Oil &amp; Gas">Oil &amp; Gas</option><option value="Other">Other</option><option value="Other/Not Classified">Other/Not Classified</option><option value="Polymer / Plastic / Rubber">Polymer / Plastic / Rubber</option><option value="Pharma/Biotech/Clinical Research">Pharma/Biotech/Clinical Research</option><option value="Public Sector and Government">Public Sector and Government</option><option value="Printing/Packaging/Publishing">Printing/Packaging/Publishing</option><option value="Personal and Household Services">Personal and Household Services</option><option value="Property &amp; Real Estate">Property &amp; Real Estate</option><option value="Paper">Paper</option><option value="Pet Store">Pet Store</option><option value="Public Relations">Public Relations</option><option value="Retail">Retail</option><option value="Retail &amp; Wholesale">Retail &amp; Wholesale</option><option value="Recreation">Recreation</option><option value="Real Estate and Property">Real Estate and Property</option><option value="Recruitment/Employment Firm">Recruitment/Employment Firm</option><option value="Real Estate/Property Management">Real Estate/Property Management</option><option value="Restaurant/Food Services">Restaurant/Food Services</option><option value="Rental Services">Rental Services</option><option value="Research &amp; Development">Research &amp; Development</option><option value="Repair / Maintenance Services">Repair / Maintenance Services</option><option value="Sales - Marketing">Sales - Marketing</option><option value="Science &amp; Technology">Science &amp; Technology</option><option value="Security/Law Enforcement">Security/Law Enforcement</option><option value="Shipping/Marine">Shipping/Marine</option><option value="Security and Surveillance">Security and Surveillance</option><option value="Sports and Physical Recreation">Sports and Physical Recreation</option><option value="Staffing/Employment Agencies">Staffing/Employment Agencies</option><option value="Social Services">Social Services</option><option value="Sports Leisure &amp; Lifestyle">Sports Leisure &amp; Lifestyle</option><option value="Semiconductor">Semiconductor</option><option value="Services - Corporate B2B">Services - Corporate B2B</option><option value="Travel">Travel</option><option value="Training">Training</option><option value="Transportation">Transportation</option><option value="Telecommunications">Telecommunications</option><option value="Trade and Services">Trade and Services</option><option value="Travel and Tourism">Travel and Tourism</option><option value="Textiles/Garments/Accessories">Textiles/Garments/Accessories</option><option value="Tyres">Tyres</option><option value="Utilities">Utilities</option><option value="Wireless">Wireless</option><option value="Wood / Fibre / Paper">Wood / Fibre / Paper</option><option value="Waste Management">Waste Management</option><option value="Wholesale Trade/Import-Export">Wholesale Trade/Import-Export</option></select>
                                                    </div>
                                                    <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="">
                                                        <span class="consultant-state-lable font-weight-normal pr-3" id="consultant-state-lable">State</span>
                                                        <img class="image-click-ab"  src="<?php bloginfo('template_url'); ?>/img/iconSelect.png">
                                                        <div class="consultant-state-options form-control toogle-div" style="display: none;">
                                                            <ul>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Johor">Johor
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Kedah">Kedah
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Kelantan">Kelantan
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Melaka">Melaka
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Negeri Sembilan">Negeri Sembilan
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Pahang">Pahang
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Perak">Perak
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Perlis">Perlis
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Pulau Pinang">Pulau Pinang
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Sabah">Sabah
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Sarawak">Sarawak
                                                                </li>    
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Selangor">Selangor
                                                                </li>    
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Terengganu">Terengganu
                                                                </li>    
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="W.P. Kuala Lumpur">W.P. Kuala Lumpur
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="W.P. Labuan">W.P. Labuan
                                                                </li>    
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="W.P. Putrajaya">W.P. Putrajaya
                                                                </li>    
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Kuala Lumpur">Kuala Lumpur
                                                                </li>    
                                                                <li>
                                                                    <input type="checkbox" name="job_state[]" class="" value="Labuan">Labuan
                                                                </li>    
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-2">
                        <!-- <a class="btn btn-warning  btnQuestions" href="#">Have any questions?</a> -->
                        <div class="iam-hover-effect login-hide">
                            <a class="btn btn-darkgreen btn-big  btnQuestions link-cons-cli " href="javascript:;">I am a..</a>
                            <div class="iam-box-hvr">
                                <div class="open page-cons-cli">
                                    <ul class="i-con-cli">
                                        <li class="nav-item">
                                            <a class="nav-link" href="http://206.189.86.153/MY/consultant/registration" title="CONSULTANT">CONSULTANT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="http://206.189.86.153/MY/client/registration" title="CLIENT">CLIENT</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        restore_current_blog();
        // switch_to_blog( $current_blog_id );
        ?>
