# About Application
<p align="center"><img src="https://trello-attachments.s3.amazonaws.com/5da52bce343f8a222f61e779/316x285/556588e0fd2611b3dd53b937441a88f4/us.png"></p>

This is a beta version of Aplikasi.us or well known as US, the Malaysia’s first online platform for SAP consultants. Aplikasi.us is currently operating and expanding its influence in Malaysia and the Philippines. 

Be part of evolution without the headhunters and strangers. You also don’t have to wait anymore to find the right fit; no matter if you are a consultant or someone who’s looking for one. Do it fast, with aplikasi.us.

# Install Application

This application was developed using Vue.js and Laravel. So some settings need to be made to make sure this application run smoothly. The settings are as belows:

## Install Node Js
Install from official website: [Click Here!](https://nodejs.org/en/)

## Install Composer
Install from official website: [Click Here!](https://getcomposer.org/download/)

## Install Laravel
Download the Laravel installer using Composer.
```
composer global require laravel/installer
```
## Install Vue
NPM is the recommended installation method when building large scale applications with Vue. It pairs nicely with module bundlers such as Webpack or Browserify. Vue also provides accompanying tools for authoring Single File Components.
```
npm install vue
```
## .env Setup

You can copy and paste ```.env.example``` and change rename it into ```.env``` file. For setup, you can make some minor adjustment to following setup:

## 1. Database setup
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=YOUR DEFAULT PORT
DB_DATABASE= YOUR DATABASE NAME
DB_USERNAME= YOUR USERNAME
DB_PASSWORD= YOUR PASSWORD
```
## 2. Mail setup
```
MAIL_DRIVER=smtp
MAIL_HOST=email-smtp.us-west-2.amazonaws.com
MAIL_PORT=587
MAIL_FROM_ADDRESS=rahimi@aplikasi.us
MAIL_FROM_NAME=aplikasi.us
MAIL_USERNAME=AKIAZ4V3IRTV5DKZTVER
MAIL_PASSWORD=BA1NSSqd4xMMuicUtV5ujbDwl+q9PNsovmy2hEHaDB51
MAIL_ENCRYPTION=tls

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

MIX_API_URL:http://localhost/aplikasi-laravel
```
## 3. Application key Setup
```
php artisan key:generate
```
Basically it tells Laravel to look for the key in the .env file first and if there isn't one there then to use ```SomeRandomString```. When you use this command, it will generate the new key to your ```.env``` file and not the ```app.php file```.

# End Product
<p align="center"><img src="https://trello-attachments.s3.amazonaws.com/5da52bce343f8a222f61e779/1199x612/b985653427c1b5b41f908a112647db90/aplikasi_beta.PNG.png"></p>
