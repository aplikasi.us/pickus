/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

import Notifications from "vue-notification";
import Clipboard from "v-clipboard";

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(Notifications);
Vue.use(Clipboard);

// Job Searh Result
Vue.component("SearchResult", require("./components/Jobs/SearchResult.vue").default);
Vue.component("SearchFilter", require("./components/Jobs/SearchFilter.vue"));
Vue.component(
  "SearchPagination",
  require("./components/Jobs/SearchPagination.vue")
);
Vue.component("SearchJobCard", require("./components/Jobs/SearchJobCard.vue"));
Vue.component(
  "RateCalculator",
  require("./components/Jobs/RateCalculator.vue")
);

// Consultant Search Result
Vue.component(
  "ConsultantSearchResult",
  require("./components/Consultant/SearchResult.vue").default
);
Vue.component(
  "ConsultantSearchFilter",
  require("./components/Consultant/SearchFilter.vue")
);
Vue.component(
  "ConsultantSearchPagination",
  require("./components/Consultant/SearchPagination.vue")
);
Vue.component(
  "ConsultantSearchJobCard",
  require("./components/Consultant/SearchJobCard.vue")
);
Vue.component(
  "ConsultantRateCalculator",
  require("./components/Consultant/ConsultantRateCalculator.vue")
);

Vue.filter("uppercase", function(value) {
  if (!value) return "";
  return value.toUpperCase();
});

const app = new Vue({
  el: "#app"
});
