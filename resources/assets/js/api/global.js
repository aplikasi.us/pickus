import axios from "../lib/axios";

export default {
  getDoYouKnow() {
    return axios.get("/api/doYouKnow");
  },

  getTips() {
    return axios.get("/api/tips");
  }
};
