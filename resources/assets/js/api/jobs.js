import axios from "../lib/axios";

export default {
  getJobs(query) {
    return axios.get("/api/job-search" + query);
  },

  saveJob(countryCode, data) {
    return axios.post(`/${countryCode}/consultant/savejob`, data);
  },

  deleteJob(countryCode, data) {
    return axios.put(`/${countryCode}/consultant/deletejob`, data);
  },

  appliedJob(countryCode, data) {
    return axios.put(`/${countryCode}/consultant/appliedjob`, data);
  },

  withdrawJob(countryCode, data) {
    return axios.put(`/${countryCode}/consultant/withdrawjob`, data);
  },

  getListOfIndustry() {
    return axios.get("/api/fetchIndustry");
  },

  getListOfState() {
    return axios.get("/api/fetchState");
  },

  getListOfCities() {
    return axios.get("/api/fetchCity");
  }
};
