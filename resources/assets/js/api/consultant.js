import axios from "../lib/axios";

export default {
  getConsultants(query) {
    return axios.get("/api/consultant-search" + query);
  },

  addToCompare(countryCode, data) {
    return axios.post(`/${countryCode}/client/add-tocompare`, data);
  },

  deleteCompare(countryCode, data) {
    return axios.post(`/${countryCode}/client/remove-to-compare`, data);
  },

  addWishlist(countryCode, data) {
    return axios.post(`/${countryCode}/client/savetowishlist`, data);
  },

  removeWishlist(countryCode, data) {
    return axios.post(`/${countryCode}/client/wishlist/removetowishlist`, data);
  },

  getCompareCount(userId) {
    return axios.get(`/api/${userId}/get-total-compare`);
  },

  requestProfile(countryCode, id) {
    return axios.get(
      `${countryCode}/client/user/request-candidate-profile/api/${id}`
    );
  },

  cancelRequestProfile(countryCode, id) {
    return axios.get(
      `${countryCode}/client/user/cancelled-candidate-profile/api/${id}`
    );
  }
};
