import axios from "axios";

const API_BASE_URL = process.env.MIX_API_URL;

/**
 * @param {import("axios").AxiosResponse} response
 */
export function getData(response) {
  return response.data;
}

const request = axios.create({
  baseURL: API_BASE_URL
});

export default request;
