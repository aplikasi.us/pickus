@if(count($notes))
<div class="commanContent spacing20">
    <h2 class="headingBorder">
        <a data-toggle="collapse" class="collapseIcon" href="#experienceCollapse-{{$interviewId}}" role="button" aria-expanded="true"> Notes</a>
        <div class="icon-down-up">
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </div>
        <!-- <a href="javascript:void(0)" title="Close" class="close-note" data-id='{{$containerId}}'><i class="fa fa-times"></i></a> -->
    </h2>
    <div class="multi-collapse collapse show" id="experienceCollapse-{{$interviewId}}" style="">
        <ul class="listExperience">
            @foreach($notes as $key => $value)
            <li>
                {{$value['Note Content']}}
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif