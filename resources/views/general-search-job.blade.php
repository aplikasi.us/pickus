@extends('Layouts.General.base_new')
@section('content')
@section('title','Search')

<!-- <div class="pt-5"> -->

    <div class="row">
        <div class="col-md-9">
        @if(count($jobs))
        <p style="color:black; top:-10px;bottom:-10px;">Showing <span style="color:#DEAB68!important;"> {{count($jobs)}} </span> of {{$total}}</p>
            <div class="row ">
                
                
                @foreach($jobs as $key => $value)
                <div class="col-md-4">
                    <div class="commanContent">

                        <div class="row rowContract" style="padding-bottom:0px!important;">                    

                            <div class="col-md-12">
                                <a href="{{route('job-details',[\Crypt::encryptString($value->id)])}}">
                                    <ul class="listContract">
                                        <li>{{$value->Job_ID}}</li>
                                        <li>{{$value->job_type}}</li>
                                    </ul>
                                    <h5 class="titles">{{$value->posting_title}}</h5>
                                </a>
                            </div>
                        </div>
                        @if(Auth::check())
                            @if(Auth::user()->role_id == 2)
                            <div class="row rowContract">
                                <div class="col-12">
                                        <div class="cus-verticale-center">
                                            <div class=" text-red">
                                               
                                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                                <span class="ml-1">MYR </span>
                                                <span
                                                class="c-job-base-rate"
                                                id="con-copy-1{{$key}}">{{($value->job_base_rate != null) ? $value->job_base_rate : 0}}/day</span>
                                                <div class="iconBox iconCopy copy-to-clipboard job-tile-button-right d-inline m-l-10"></div>
                                            </div>
                                                
                                        </div>
                                   
                                </div>
                            </div>
                            @endif
                        @else
                                <div class="row rowContract">
                                    <div class="col-md-12">
                                        <a href="javascript:;" class=" text-blue iconLogin  login-form-poup"  >
                                            <img class="cus-br-icon-small gap-right" src="{{admin_asset('images/cus-br-icon.png')}}">Login as a Consultant to view</a>
                                    </div>                    
                                </div>
                        @endif
                                

                        <div class="row rowContract">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                                <div class="ml-1 Black d-inline" style="padding-left:5px;">{{$value->job_mode}}</div>
                            </div>


                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                                <div class="ml-1 Black d-inline">{{$value->job_duration}} months</div>
                            </div>

                            
                            
                        </div>

                        <div class="row rowContract">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                                <div class="ml-1 Black d-inline">{{$value->work_experience}} exp.</div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                                <div class="ml-1 Black d-inline">{{$value->industry}}</div>
                            </div>
                            
                        </div>

                        
                        <div class="row rowContract">

                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                                <div class="ml-1 Black d-inline">{{date('d M. Y',strtotime($value->date_opened))}}</div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                                <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                                <div class="ml-1 Black d-inline" style="padding-left:5px;">{{$value->city}}</div>
                            </div>

                        </div>
                        <div class="boxbtns job-tile-button-right">
                            <div class="row no-gutters border-top-btn">
                                @if(Auth::check())
                                    @if(Auth::user()->role_id == 2)
                                        @php
                                            $is_applied = 0;
                                            $is_saved = 0;
                                        @endphp
                                        @foreach ($value->associatecandidates as $key1=>$value1)
                                            @if ($value1->CANDIDATEID == Auth::user()->zoho_id || $value1->candidate_id == Auth::user()->id)
                                                @if(!empty($value1->status) && in_array($value1->status ,$candidateScreeningStatus))
                                                    @php
                                                        $is_applied = 1;
                                                    @endphp
                                                    @if($value1->status != "Application Withdrawn") 
                                                    <div class="col-md-12  border-right-btn text-center">
                                                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader ">
                                                        <a title="Withdraw Application" href="javascript:void(0)" class="btn btn-block btn-danger btn-md  withdraw-application btn-custom-trans colorRed btn-apply-icon" data-zohoid="{{$value->JOBOPENINGID}}" data-id="{{$value->id}}">Withdraw Application</a>
                                                    </div>
                                                    @else
                                                    <div class="col-md-12  border-right-btn text-center">
                                                        <a title="Application Withdrawn" href="javascript:void(0)" class="btn btn-block btn-danger btn-md  btn-custom-trans colorRed btn-apply-icon">Application Withdrawn</a>
                                                    </div>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach
                                        @if($is_applied == 0)
                                            @foreach ($value->associatecandidates as $key1=>$value1)
                                                @if ($value1->CANDIDATEID == Auth::user()->zoho_id)
                                                    @php
                                                    $is_saved = 1;
                                                    @endphp
                                                    <div class="col-md-12 col-12 col-lg-6 border-right-btn text-center  btn-save-apply">
                                                        <a title="Unsave" href="javascript:void(0)" class="btn btn-secondary btn-block  savejob-delete btn-custom-trans colorBlue btn-save-icon" data-zohoid="{{$value->JOBOPENINGID}}">Unsave</a>
                                                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                                                    </div>
                                                    <div class="col-md-12 col-12 col-lg-6   text-center btn-apply-full">
                                                        <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block  consultant-job-applied btn-custom-trans colorRed btn-apply-icon" data-zohoid="{{$value->JOBOPENINGID}}" data-id="{{$value->id}}">Apply Now!</a>
                                                    </div>   
                                                @endif
                                            @endforeach
                                            @if($is_saved == 0)
                                                <div class="col-md-12 col-12 col-lg-6  border-right-btn text-center btn-save-apply">
                                                    <a title="Save" href="JavaScript:void(0);" class="btn btn-secondary btn-block btn-custom-trans colorBlue consultant-job-saved btn-save-icon" data-zohoid="{{$value->JOBOPENINGID}}" data-id="{{$value->id}}">Save</a>
                                                    <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                                                </div>
                                                <div class="col-md-12 col-12 col-lg-6   text-center btn-apply-full">
                                                    <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                                                    <a title="Apply Now!" href="javaScript:void(0)" class="btn btn-danger btn-block btn-custom-trans colorRed btn-apply-icon consultant-job-applied" data-zohoid="{{$value->JOBOPENINGID}}" data-id="{{$value->id}}">Apply Now!</a>
                                                </div>
                                            @endif
                                        <!-- </div>  -->
                                        @endif
                                    @endif
                                @else
                                    <div class="col-md-6  border-right-btn text-center">
                                        <a title="Save" href="JavaScript:void(0);" class="btn btn-block btn-secondary btn-md  general-save-job btn-custom-trans colorBlue btn-save-icon" data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Save</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a title="Apply Now!" href="javascript:void(0)" class="btn btn-block btn-danger btn-md  general-apply-job btn-custom-trans colorRed btn-apply-icon" data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Apply Now!</a>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>                                        
                @endforeach
                
            </div>
                {{$jobs->links()}}
                @else
                <div class="col-md-12">
                    <p class="text-center">
                        <span class="no-result">No result found</span>
                    </p>
                </div>
                </div> 
                @endif
        </div>
        <div class="col-md-3">
            <h3 class="header-red-bg colorWhite text-center pd-5 how-much-left">How Much Will You Get?</h3>
            <div class="commanContent boxHowmuch">
            
            <div class="contentHowmuch">
                <p class="pb-1">Enter the Job’s Base Rate</p>
                <div class=" text-red inlineText">
                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                    <span class="ml-1">MYR </span>
                </div>
                <input type="text" class="text-red text-right" value="" id="c1-base-rate-perday" maxlength="10"/>
                <span class="text-red">/day</span>
            </div>
            <div class="row f-12 rs-5">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Length of contract
                            <a href="#" class="iconInfo">
                                <span class="tooltiptext">Calculated based on 20 mandays/month. Surcharge of 5% is applicable for 3-month period and 3% discount will be given for 12-month period.</span>
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5
                                months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="2" title="6-11 months"
                                    checked="" /> 6-11 months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                                months</li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Fees</p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                    checked="" disabled="" /> Admin fee
                                <a href="#" class="iconInfo">
                                    <span class="tooltiptext1">For talent management, onboarding process, payroll hosting, basic administrations and other related handling process.</span>
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                            <li>
                                <input type="checkbox" class="fees" name="guaranteed_payment"
                                    title="Guaranteed on-time payment" id="guaranteed_payment" checked="" /> Guaranteed
                                on-time payment
                                <a href="#" class="iconInfo">
                                    <span class="tooltiptext1">Guarantee 30-day payment term.</span>
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            <div class="boxDoyouKnow mb-0">
                <h6 class="f-14 mb-0">Your Daily Rate Will Be</h6>
                <h2 class="text-red" id="daily-rate">MYR 0.00</h2>
            </div>
        </div>
           
            @include('Layouts.General.doyouknow')
            @include('Layouts.General.tips')
        </div>
    </div>
<!-- </div> -->
<?php

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>
@endsection