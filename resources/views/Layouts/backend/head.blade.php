    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Aplikasi | Dashboard</title>

    <link href="{{asset_public('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset_public('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{asset_public('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
    <!--Date Picker-->
    <link href="{{admin_asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!-- Gritter -->
    <link href="{{asset_public('js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">
    <link href="{{asset_public('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{admin_asset('css/summernote_editor.css')}}" rel="stylesheet">
    <link href="{{asset_public('select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset_public('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset_public('css/style.css')}}" rel="stylesheet">
    <link href="{{asset_public('css/custom.css')}}" rel="stylesheet">