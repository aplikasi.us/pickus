<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header logo-box">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block"> <strong class="font-bold">Aplikasi</strong>
                            </span></span> </a>
                </div>
                <div class="logo-element">
                    AP
                </div>
            </li>
            <li class="{{ (Request::is('admin/dashboard')) ? 'active' : '' }}">
                <a href="{{url('admin/dashboard')}}" title="DASHBOARD"><i class="fa fa-th-large"></i> <span
                        class="nav-label">DASHBOARD</span></a>
            </li>
            @if(Auth::user()->user_type == "Super Admin")
            <li
                class="{{ (Route::currentRouteName() == "systemuser.index" || Route::currentRouteName() == "systemuser.create" || Route::currentRouteName() == "systemuser.edit") ? 'active' : '' }}">
                <a href="{{route('systemuser.index')}}" title="SYSTEM USERS"><i class="fa fa-users"></i> <span
                        class="nav-label">SYSTEM
                        USERS</span></a>
            </li>
            @endif
            <li
                class="{{ (Route::currentRouteName() == "clients.index" || Route::currentRouteName() == "clients.create" || Route::currentRouteName() == "viewRequestProfile" || Route::currentRouteName() == "clients.edit") ? 'active' : '' }}">
                <a href="{{route('clients.index')}}" title="CLIENTS"><i class="fa fa-user"></i> <span
                        class="nav-label">CLIENTS</span></a>
            </li>
            <li class="{{ (Route::currentRouteName() == "indexRequestProfile") ? 'active' : '' }}">
                <a href="{{route('indexRequestProfile')}}" title="REQUESTED PROFILE"><i class="fa fa-user"></i> <span
                        class="nav-label">REQUESTED
                        PROFILE</span></a>
            </li>
            <li
                class="{{ (Route::currentRouteName() == "consultant.index" || Route::currentRouteName() == "consultant.create" || Route::currentRouteName() == "editConsultant" || Route::currentRouteName() == "consultant.edit" || Route::currentRouteName() == "updateexperience" || Route::currentRouteName() == "updateeducation" || Route::currentRouteName() == "updatereference" || Route::currentRouteName() == "addexperience" || Route::currentRouteName() == "addeducation" || Route::currentRouteName() == "addreference" || Route::currentRouteName() == "associate-job-openings") ? 'active' : '' }}">
                <a href="{{route('consultant.index')}}" title="CONSULTANTS"><i class="fa fa-user"></i> <span
                        class="nav-label">CONSULTANTS</span></a>
            </li>
            <li
                class="{{ (Route::currentRouteName() == "jobs.index" || Route::currentRouteName() == "jobs.create" || Route::currentRouteName() == "jobs.edit") ? 'active' : '' }}">
                <a href="{{route('jobs.index')}}" title="JOB OPENINGS"><i class="fa fa-file"></i> <span
                        class="nav-label">JOB
                        OPENINGS</span></a>
            </li>
            <li class="{{ (Route::currentRouteName() == "jobRequestedProfile" ) ? 'active' : '' }}">
                <a href="{{route('jobRequestedProfile')}}" title="JOB REQUEST"><i class="fa fa-file"></i> <span
                        class="nav-label">JOB
                        REQUEST</span></a>
            </li>
            <li class="{{ (Route::currentRouteName() == "upcoming_interviews" ) ? 'active' : '' }}">
                <a href="{{route('upcoming_interviews')}}" title="UPCOMING INTERVIEWS"><i class="fa fa-user"></i> <span
                        class="nav-label">UPCOMING
                        INTERVIEWS</span></a>
            </li>
            @if(Auth::user()->user_type == "Super Admin")
            <li class="{{ (Route::currentRouteName() == "synczohodata" ) ? 'active' : '' }}">
                <a href="{{route('synczohodata')}}" title="SYNC DATA"><i class="fa fa-retweet"></i> <span
                        class="nav-label">SYNC
                        DATA</span></a>
            </li>
            @endif
            <li class="{{(Route::currentRouteName() == "adminnotification") ? 'active' : ''}}"><a
                    href="{{route('adminnotification')}}" title="NOTIFICATIONS"><i class="fa fa-bell"></i><span
                        class="nav-label">NOTIFICATIONS</span> </a></li>
            <li
                class="{{ (Route::currentRouteName() == "emailtemplate" || Route::currentRouteName() == "editemailtemplate" || Route::currentRouteName() == "site" || Route::currentRouteName() == "taglines" || Route::currentRouteName() == "addtagline" || Route::currentRouteName() == 'updatetagline' || Route::currentRouteName() == "faq" || Route::currentRouteName() == "addfaq" || Route::currentRouteName() == 'updatefaq') ? 'active' : ''}}">
                <a href="javascript:void(0)" title="SETTINGS"><span class="nav-label"><i
                            class="fa fa-gears"></i>SETTINGS</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    @if(Auth::user()->user_type == "Super Admin")
                    <li
                        class="{{(Route::currentRouteName() == "emailtemplate" || Route::currentRouteName() == 'editemailtemplate') ? 'active' : ''}}">
                        <a href="{{route('emailtemplate')}}">EMAIL TEMPLATES</a></li>
                    <li class="{{(Route::currentRouteName() == "site") ? 'active' : ''}}"><a
                            href="{{route('site')}}">SITE </a></li>
                    <li
                        class="{{(Route::currentRouteName() == "taglines" || Route::currentRouteName() == "addtagline" || Route::currentRouteName() == 'updatetagline') ? 'active' : ''}}">
                        <a href="{{route('taglines')}}">TAGLINES </a></li>
                    <li
                        class="{{(Route::currentRouteName() == "faq" || Route::currentRouteName() == "addfaq" || Route::currentRouteName() == 'updatefaq') ? 'active' : ''}}">
                        <a href="{{route('faq')}}">FAQ </a></li>
                    @endif
                </ul>
            </li>
        </ul>

    </div>
</nav>