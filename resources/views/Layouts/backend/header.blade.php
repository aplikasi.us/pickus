<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            @if(Auth::user()->role_id == 3 && Auth::user()->user_type == "Super Admin" )
            <li class="">
                <form method="post" action="{{route('territorystore')}}" id="territory-store">
                    {{csrf_field()}}
                    @php
                    $sessionterri = Session::get('territoryadmin');
                    @endphp
                    <select class="form-control m-b territory-storechange" name="territory">
                        <option <?php  if($sessionterri == 'Global') {echo 'selected';}  ?>>Global</option>
                        <option <?php  if($sessionterri == 'MY') {echo 'selected';}  ?>>MY</option>
                        <option <?php  if($sessionterri == 'PH') {echo 'selected';}  ?>>PH</option>
                    </select>
                    <!-- <button class="btn" type='submit' >Save</button> -->
                </form>
            </li>
            @endif
            <li class="dropdown">
                <a class="dropdown-toggle count-info read-notification" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  @if(count($unread))<span class="label label-primary"> {{count($unread)}}</span>@endif
                </a>
                @if(count($unread))
                <ul class="dropdown-menu dropdown-alerts">
                    
                    @foreach($unread as $key => $value)
                        <li>
                            <div class="row">
                                <div class="col-md-8">
                                <i class="fa fa-envelope fa-fw"></i> {!! $value->message !!}
                                </div>
                                <div class="col-md-4">
                                    <span class="pull-right text-muted small">{{date('d M. Y',strtotime($value->created_at))}}</span>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                    @endforeach
                    @if(count($unread))
                    <li>
                        <div class="text-center link-block">
                            <a href="{{url('admin/settings/notification')}}">
                                <strong>See All Notification</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                    @endif
                </ul>
                @endif
            </li>
            <li>{{Auth::user()->first_name.Auth::user()->last_name}}</li>
            <li>
                <a href="{{ url('admin/logout') }}">
                    <i class="fa fa-sign-out"></i> Logout
                </a>
            </li>
        </ul>
    </nav>
</div>