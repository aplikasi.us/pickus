<!DOCTYPE html>
<html>

<head>
    @include('Layouts.backend.head')
</head>

<body>
    <div id="wrapper">
        @yield('content')
        @include('Layouts.backend.footer')
        @yield('javascript')
    </div>
</body>

</html>