<?php
if(Session::has('errorFails')){?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <?=Session::get('errorFails');?>
    </div>

<?php }
if(Session::has('errorSuccess')){?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?=Session::get('errorSuccess');?>
    </div>
    
<?php }
?>