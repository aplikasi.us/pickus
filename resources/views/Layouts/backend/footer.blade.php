<!-- Mainly scripts -->
<script>
var _token = "{{csrf_token()}}";
var jobdelete = "{{route('job_destroy')}}";
var jobactivate = "{{route('job_status')}}";
var clinetactivateid = "{{route('client_status')}}";
var consultant_activate = "{{route('consultant_status')}}";
var isUserEmailExists = "{{route('isUserEmailExists')}}";
var systemuser_status = "{{route('systemuser_status')}}";
var systemuser_delete = "{{route('systemuser_delete')}}";
var getCityList = "{{route('job-city-list')}}";
var multipleDeleteRequest = "{{route('multipledeleteRequestedprofile')}}";
var multipleFormattedResumeDownload = "{{route('multipleFormattedResumeDownload')}}";
var readAdminNotification = "{!! route('readadminnotification')!!}";
</script>
<script src="{{asset_public('js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset_public('js/bootstrap.min.js')}}"></script>
<script src="{{asset_public('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset_public('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Flot -->
<script src="{{asset_public('js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset_public('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset_public('js/plugins/flot/jquery.flot.spline.js')}}"></script>
<script src="{{asset_public('js/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset_public('js/plugins/flot/jquery.flot.pie.js')}}"></script>

<!-- Peity -->
<script src="{{asset_public('js/plugins/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset_public('js/demo/peity-demo.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset_public('js/inspinia.js')}}"></script>
<script src="{{asset_public('js/plugins/pace/pace.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{asset_public('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<!-- GITTER -->
<script src="{{asset_public('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

<!-- Sparkline -->
<script src="{{asset_public('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/summernote_editor.js')}}"></script>
<!-- Sparkline demo data  -->
<script src="{{asset_public('js/demo/sparkline-demo.js')}}"></script>
<script src="{{asset_public('js/plugins/select2/select2.full.min.js')}}" type="text/javascript"></script>
<!-- ChartJS-->
<script src="{{asset_public('js/plugins/chartJs/Chart.min.js')}}"></script>
<!-- ChartJS-->
<script src="{{asset_public('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{admin_asset('js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!-- Toastr -->
<script src="{{asset_public('js/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset_public('js/bootbox.js')}}" type="text/javascript"></script>
<script src="{{asset_public('js/validator.js')}}"></script>
<script src="{{asset_public('js/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset_public('js/custom.js')}}"></script>