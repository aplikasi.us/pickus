<div class="boxProfile">
    <div class="imgProfile">
        @if(Auth::user()->profile_pic != null)
        @if(file_exists(public_path('profile_image/'.Auth::user()->profile_pic)))
        <img src="{{admin_asset('profile_image/default.jpg')}}" alt="" title="" id="sidebar-profile-default" style="display: none;"/>
        <img src="{{admin_asset('profile_image/'.Auth::user()->profile_pic)}}" id="sidebar-profile-picture" alt="" title="" />
        @else
        <img src="{{admin_asset('profile_image/default.jpg')}}" alt="" title="" />
        @endif
        
        @else
        <img src="{{admin_asset('profile_image/default.jpg')}}" alt="" title="" />
        @endif
    </div>
    <h6>{{$candidate_us->candidate_id}}</h6>
    <h5>{{Auth::user()->first_name .' '.Auth::user()->last_name}}</h5>
    <span class="textlogged">Logged in as Consultant</span>
</div>
<ul class="menuProfile">
    <li class="{{ (Route::currentRouteName() == "consultant-home") ? 'active' : '' }}">
        <a href="{{route('consultant-home')}}" title="My Home" class="iconHome">My Home</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "consultant-profile") ? 'active' : '' }}">
        <a href="{{route('consultant-profile')}}" title="Profile" class="iconProfile">Profile</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "consultant-interviews") ? 'active' : '' }}">
        <a href="{{route('consultant-interviews')}}" title="Interviews" class="iconInterviews">Interviews</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "consultant-contract-history") ? 'active' : '' }}">
        <a href="{{route('consultant-contract-history')}}" title="Contract History" class="iconHistory">Contract
            History</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "consultant-attachments") ? 'active' : '' }}">
        <a href="{{route('consultant-attachments')}}" title="Attachments" class="iconAttachments">Attachments</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "consultant-notifications") ? 'active' : '' }}">
        <a href="{{route('consultant-notifications')}}" title="Notifications" class="iconNotificationSidebar">Notifications</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "consultantsettings") ? 'active' : '' }}">
        <a href="{{route('consultantsettings')}}" title="Settings" class="iconSettings">Settings</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "logout") ? 'active' : '' }}">
        <a class="nav-link iconLogin iconSettings" href="{{route('logout')}}" title="Logout" >Logout</a>
    </li>
</ul>