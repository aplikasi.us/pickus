<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
            <div class="boxForm">   
                <form role="form" method="post" action="{{route('login')}}" id="consultant-login">
                    {{csrf_field()}}
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Sign in</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="">

                        <div class="modal-body mx-3">

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <input type="hidden" name="redirect_url"  id="redirect_url" value="" />
                                        <label class="control-label">Email Address: </label>
                                        <input type="text" name="email" class="form-control" placeholder="Enter email address">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label class="control-label">Password: </label>
                                        <input type="password" name="password" class="form-control" placeholder="Enter your password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-block">
                            <button class="btn btn-block btn-green" type="submit">GET ME IN</button>
                            <p><a class="color_white hvr_clrp d-block close-loginform f-14 pd-0" href="javascript:;" data-toggle="modal" data-target="#modalforgotForm"><small>Forgot password?</small></a></p>
                        </div>
                    </div>
                </form>
            </div>	
        </div>
    </div>
</div>
<!------Forgot password model---->
<div class="modal fade" id="modalforgotForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="boxForm"> 
                <form role="form" method="post" action="{{route('forgotpassword')}}" id="forgotpassword">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Forgot Password</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="">

                        <div class="modal-body mx-3">

                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" placeholder="Enter email address">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button class="btn btn-block btn-green" type="submit">SUBMIT</button>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!------End Forgot password model---->
<footer class="footerNew">
    <div class="container">
        <div class="row">

            <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 footerColumn1">
                <p class="footerLogo">
                    <a href="{{url('/')}}" title="aplikasi.us">
                        <img src="{{admin_asset('images/logoFooter.png')}}" alt="aplikasi.us" title="aplikasi.us"/>
                    </a>
                </p>
                @if(Session::get('territory') == "MY")
                <a href="https://goo.gl/maps/k5wzMCPF9WB2" title="Kumpulan Aplikasi Sempurna Sdn Bhd 2-2B, Tower 9, UOA Business Park, 1 Jalan Pengaturcara U1/51A, 40150 Shah Alam, Selangor,
                   Malaysia" target="_blank">Kumpulan Aplikasi Sempurna Sdn Bhd</br> 2-2B, Tower 9, UOA Business Park,</br> 1
                    Jalan Pengaturcara U1/51A,</br> 40150 Shah Alam, Selangor,
                    Malaysia</a>
                    @else
                    <a href="https://goo.gl/maps/aTdSMyPNrSTiAqTA9" title="Service 101 plus Consulting Inc, 1F Clock In BGC Bonifacio Technology Center. 31st Corner 2nd Avenue, Bonifacio Global City, Taguig City, Philippines 1634" target="_blank">Service 101 plus Consulting Inc.</br>1F Clock In BGC Bonifacio Technology</br>Center, 31st Corner 2nd Avenue,</br> Bonifacio Global City,Taguig City, Philippines 1634</a>
                    @endif
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 footerColumn2">
                <h4>Menu</h4>
                <ul class="listMenu">
                    <li>
                        <a href="{{url('/')}}" title="Home">Home</a>
                    </li>
                    <li>
                        <a href="{{route('aboutus')}}" title="About us">About us</a>
                    </li>
                    <li>
                        <a href="{{route('consultant-registration')}}" title="CONSULTANT">Consultant</a>
                    </li>
                    <li>
                        <a href="{{route('client-reg')}}" title="CLIENT">Client</a>
                    </li>
                    <li>
                        <a href="{{route('termsConditions')}}" title="Terms of Service">Terms of Service</a>
                    </li>
                    <li>
                        <a href="{{route('privacyPolicy')}}" title="Privacy Policy">Privacy Policy</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                <h4>Useful Links</h4>
                <ul class="listMenu">
                    <li>
                        <a href="{{route('atapVirtualOffice')}}" title="The Atap Virtual Office">The Atap Virtual Office</a>
                    </li>
                    <li>
                        <a href="{{route('howitworks')}}" title="How It Works">How It Works</a>
                    </li>
                    <li>
                        <a href="{{route('frontfaq')}}" title="FAQ">FAQ</a>
                    </li>
                    <li><a href="http://206.189.86.153/blog/"  title="Blog">Blog</a></li>
                    @if(!Auth::check())
                    <li class="nav-item">
                        @if(Route::currentRouteName() == 'home')
                            <a class="join-link join-current " href="javascript:;">Join Us</a>
                        @else
                            <a class="join-link " href="{{route('home',['search' => 'registration'])}}">Join Us</a>
                        @endif
                    </li>
                    @endif
                    <li>
                        <a href="{{route('contactus')}}" title="Contact Us">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                <h4>Connect With Us </h4>
                <ul class="listMenu">
                    <li>
                        <a href="tel:60350321896" title="+603-5032 1896">+603-5032 1896</a>
                    </li>
                    <li>
                        <a href="mailto:connect@aplikasi.us" title="connect@aplikasi.us">connect@aplikasi.us</a>
                    </li>
                </ul>
                <ul class="listSocialicons">
                        
                    <li><a href="https://www.linkedin.com/company/aplikasi-us" title="linkedin" target="_blank"><i class="fa bg-red-i colorWhite fa-linkedin" aria-hidden="true"></i></a></li>
                    <li>
                        <a href="https://www.facebook.com/aplikasi.us/" title="facebook" target="_blank"><i class="fa bg-red-i colorWhite fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCxGX87T3P9WySGZeQaM9OtQ?reload=9" title="youtube" target="_blank"><i class="fa bg-red-i colorWhite fa-youtube-play" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/aplikasi_us" title="twitter" target="_blank"><i class="fa bg-red-i colorWhite fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/aplikasi.us/" title="instagram" target="_blank"><i class="bg-red-i colorWhite fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="boxCopyright">
            <p class="pb-0 colorWhite">Copyright 2019. All rights reserved. Kumpulan Aplikasi Sempurna Sdn Bhd</p>
        </div>
    </div>
</footer>
<!------Teritories Modal---->
<div class="modal fade" id="teritories-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="boxForm"> 
                <form role="form" method="post" action="{{route('setterritories')}}" id="territories">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold text-green">Select Your Territory</h4>    <button type="button" class="close" data-dismiss="modal">&times;</button>         
                    </div>
                    <div class="">
                        {{csrf_field()}}
                        <div class="modal-footer d-flex justify-content-center">
                            <button class="btn btn-block btn-green" name="territory" value="MY" type="submit">MY</button>
                            <button class="btn btn-block btn-red" name="territory" value="PH" type="submit" style="margin-top: 0px;">PH</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!------End Forgot password model---->
@section('javascript')
<script>
    $(document).ready(function () {
        var isTerritorySet = "<?php echo Session::get('territory'); ?>";
        if (isTerritorySet == "") {
            $("#teritories-modal").modal({
                // backdrop: 'static',
                // keyboard: false
            });
        }
        $("#territory-link").click(function () {
            $("#teritories-modal").modal({
                // backdrop: 'static',
                // keyboard: false
            });
        });
    });
</script>
@endsection