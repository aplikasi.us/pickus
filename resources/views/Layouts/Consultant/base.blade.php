<!DOCTYPE html>
<html>
@include('Layouts.Consultant.head')

<body class="pageLatest">
    @include('Layouts.General.header_new')
    <article>
        <section class="secColumn">
            <div class="row rowColumns no-gutters">
                @if(Route::currentRouteName() == "consultantsearch" || Route::currentRouteName() == "consultantjobdetails")
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 columnRight">
                        @yield('content')
                    </div>
                    @else
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 columnLeft">
                        @include('Layouts.Consultant.sidebar')
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9 columnRight">
                        @yield('content')
                    </div>
                @endif
            </div>
        </section>
    </article>
    @include('Layouts.Consultant.footer_new')
    @include('Layouts.Consultant.foot_new')
    @yield('javascript')
</body>

</html>