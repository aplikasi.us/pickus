<div class="generate-password-form-block">
    <form method="post" method="post" action="{{route('generate-password',[isset($id) ? $id : 0])}}"
        id="consultant-generate-password">
        <h4 class="w-100 font-weight-normal text-black">Generate Password</h4>
        <div class="generate-password-body">
            <div id="generate-password-message"></div>
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Enter password">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="password" name="confirm_password" class="form-control"
                            placeholder="Re-enter your password">
                    </div>
                </div>
            </div>
            <button class="btn btn-login-blue" type="submit">GENERATE NOW!</button>
            <img src="{{admin_asset('images/loader.svg')}}" id="generate-password-loading" style="display: none;">
        </div>
    </form>
</div>
<header class="headerNew">
    <div class="header-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg " id="mainNav">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                    <!-- <h1 id="logo">            
                    <a href="{{url('/')}}" title="aplikasi.us">
                        
                        <img src="{{ admin_asset('images/logo2.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                    </a>
                    </h1> -->
                        @if(Session::get('territory') != "")
                        @if(!Auth::check())
                        <li class="nav-item active"><a class="nav-link" href="javascript:void(0)"
                                id="territory-link">{{Session::get('territory')}}</a></li>
                        @else
                        <li class="nav-item active"><a class="nav-link"
                                href="javascript:void(0)">{{Session::get('territory')}}</a></li>
                        @endif
                        @endif
                        @if(Session::get('territory') == "MY")
                        <li class="nav-item {{ (Route::currentRouteName() == "atapVirtualOffice") ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('atapVirtualOffice')}}" title="The Atap">The Atap Hello</a>
                        </li>
                        @endif
                        <li class="nav-item {{ (Route::currentRouteName() == "aboutus") ? 'active' : '' }}"><a
                                class="nav-link" href="{{route('aboutus')}}">Know us More!</a></li>
                        <li class="nav-item {{ (Route::currentRouteName() == "howitworks") ? 'active' : '' }}"><a
                                class="nav-link" href="{{route('howitworks')}}">How it Works</a></li>
                        <li class="nav-item {{ (Route::currentRouteName() == "frontfaq") ? 'active' : '' }}"><a
                                class="nav-link" href="{{route('frontfaq')}}">FAQ</a></li>
                        <li class="nav-item">
                            @if(Session::get('territory') == "PH")
                            {{-- <a class="nav-link" href="/staging/blog/ph"> --}}
                                <a class="nav-link" href="./blog">
                                @else
                                {{-- <a class="nav-link" href="/staging/blog/my"> --}}
                                    <a class="nav-link" href="./blog/">
                                    @endif

                                    Blog</a></li>
                        @if(!Auth::check())
                        <li class="nav-item">
                            @if(Route::currentRouteName() == 'home')
                            <a class="nav-link join-link join-current colorGreen" href="javascript:;">JOIN US</a>
                            @else
                            <a class="nav-link join-link colorGreen"
                                href="{{route('home',['search' => 'registration'])}}">JOIN US</a>
                            @endif
                        </li>
                        @endif
                        @if(Auth::check())
                        @if(Auth::user()->role_id == 1)
                        <li class="nav-item {{ (Route::currentRouteName() == 'client-home') ? 'active' : '' }}"><a
                                class="nav-link iconHome" href="{{route('client-home')}}"> Home</a>
                        </li>
                        @else
                        <li class="nav-item {{ (Route::currentRouteName() == 'consultant-home') ? 'active' : '' }}"><a
                                class="nav-link iconHome" href="{{route('consultant-home')}}"> Home</a>
                        </li>
                        @endif
                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                        <li class="nav-item dropdown">
                            <a class="nav-link pl-1 p-0  no-hover" href="#" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <span class="iconNotification read-notification">
                                    @if(count($unread))
                                    <small>{{count($unread)}}</small>
                                    @endif
                                </span>
                            </a>

                            <div class="dropdown-menu dropdownNotification  dropdown-menu-right dropdown">
                            <div class="headerDropdown colorWhite">
                                    Notifications
                                </div>    
                                    @if(count($unread))                                
                                    <div class="dropdownContent">
                                        <ul class="listContractHistory">
                                            @foreach($unread as $notificationKey => $notoficationValue)
                                            <li>
                                                <div class="row rowHistory align-items-center">
                                                    <div class="col-md-12">
                                                        <span
                                                            class="textDate">{{date('d M. Y',strtotime($notoficationValue->created_at))}}</span>
                                                        @if($notoficationValue->redirect_url != null)
                                                        <a href="{{$notoficationValue->redirect_url}}">
                                                            <p>{{$notoficationValue->message}}</p>
                                                        </a>
                                                        @else
                                                        <p>{{$notoficationValue->message}}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                        <div class="boxbtns">
                                            <div class="row no-gutters">
                                                <div class="col-md-12">
                                                    @if(Auth::user()->role_id == 1)
                                                    <a title="View all" href="{{route('client-notifications')}}"
                                                        class="btn btn-secondary btn-block">View
                                                        all</a>
                                                    @endif
                                                    @if(Auth::user()->role_id == 2)
                                                        <a title="View all" href="{{route('consultant-notifications')}}"
                                                        class="btn btn-secondary btn-block">View
                                                        all</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                <!-- display 5 read recent notification -->
                                <div class="dropdownContent">
                                        <ul class="listContractHistory">
                                            @foreach($read as $notificationKey => $notoficationValue)
                                            <li>
                                                <div class="row rowHistory align-items-center">
                                                    <div class="col-md-12">
                                                        <span
                                                            class="textDate">{{date('d M. Y',strtotime($notoficationValue->created_at))}}</span>
                                                        @if($notoficationValue->redirect_url != null)
                                                        <a href="{{$notoficationValue->redirect_url}}">
                                                            <p>{{$notoficationValue->message}}</p>
                                                        </a>
                                                        @else
                                                        <p>{{$notoficationValue->message}}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                        <div class="boxbtns">
                                            <div class="row no-gutters">
                                                <div class="col-md-12">
                                                    @if(Auth::user()->role_id == 1)
                                                    <a title="View all" href="{{route('client-notifications')}}"
                                                        class="btn btn-secondary btn-block">View
                                                        all</a>
                                                    @endif
                                                    @if(Auth::user()->role_id == 2)
                                                        <a title="View all" href="{{route('consultant-notifications')}}"
                                                        class="btn btn-secondary btn-block">View
                                                        all</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                @endif
                            </div>
                        </li>
                        @endif
                        @endif
                        <!-- <li class="nav-item {{ (Route::currentRouteName() == "aboutus") ? 'active' : '' }}"><a class="nav-link" href="{{route('aboutus')}}">Know us More!</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('howitworks')}}">How it Works</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('frontfaq')}}">FAQ</a></li>
                        <li class="nav-item"><a class="nav-link" href="http://206.189.86.153/blog/">Blog</a></li> -->
                        <!-- <li class="nav-item {{ (Route::currentRouteName() == "atapVirtualOffice") ? 'active' : '' }}"><a class="nav-link" href="{{route('atapVirtualOffice')}}">Contact</a></li> -->
                        <li class="nav-item">
                            @if(Auth::check())
                            @if(Auth::user()->role_id == 1)
                            <!-- <a class="nav-link iconHome" href="{{route('client-home')}}" title="My Account">My Account</a> -->
                            <!-- <a class="nav-link iconLogout" href="{{route('logout')}}" title="Logout">
                                Logout</a> -->
                            <a style="color:white;" href="{{route('logout')}}" title="Logout">
                            <img border="0" src="{{admin_asset('images/logout-icon-for-web.png')}}" width="25" height="25">
                            Logout</a>

                            @elseif(Auth::user()->role_id == 2)
                            <!-- <a class="nav-link iconHome" href="{{route('consultant-home')}}" title="My Account">My Account</a> -->
                            <!-- <a class="nav-link iconLogout" href="{{route('logout')}}" title="Logout">Logout</a> -->
                            <a style="color:white;" href="{{route('logout')}}" title="Logout">
                            <img border="0" src="{{admin_asset('images/logout-icon-for-web.png')}}" width="25" height="25">
                            Logout</a>
                            @else
                            <!-- <a class="nav-link iconLogin login-form-poup" href="javascript:;" title="Login">Login</a> -->
                            <a class=" login-form-poup" style="color:white;" href="javascript:;" title="Login" data-toggle="modal" data-target="#modalLoginForm">
                            <img border="0" src="{{admin_asset('images/login-icon-for-web.png')}}" width="25" height="25">
                            Login</a>
                            @endif

                            @else
                        
                            <!-- <a class="nav-link iconLogin login-form-poup" href="javascript:;" title="Login">Login</a> -->
                            <a class=" login-form-poup" style="color:white;" href="javascript:;" title="Login" data-toggle="modal" data-target="#modalLoginForm">
                            <img border="0" src="{{admin_asset('images/login-icon-for-web.png')}}" width="25" height="25">
                            Login</a>

                            @endif
                        </li>

                    </ul>

                </div>
                <div class="login-form-toggle">
                    <div class="login-form-block">
                        <form role="form" method="post" action="{{route('login')}}" id="consultant-login">
                            {{csrf_field()}}
                            <div class="header-login-toggle">
                                <h4 class="  w-100 font-weight-normal  text-black">Login</h4>
                            </div>
                            <div class="login-form-field">
                                <div id="login-error-message"></div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="form-group">
                                            <input type="hidden" name="redirect_url" id="redirect_url" value="" />
                                            <!-- <label class="control-label">Email Address: </label> -->
                                            @if(Session::get('loginemail') != null)
                                            <input type="text" name="email" value="{{Session::get('loginemail')}}"
                                                class="form-control" placeholder="Enter email address">
                                            @else
                                            <input type="text" name="email" class="form-control"
                                                placeholder="Enter email address">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <!-- <label class="control-label">Password: </label> -->
                                            <input type="password" name="password" class="form-control"
                                                placeholder="Enter your password">
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-login-blue" type="submit">Get Me In</button>
                                <img src="{{admin_asset('images/loader.svg')}}" id="login-loading"
                                    style="display: none;">
                                <p class="pb-0 pt-1"><a
                                        class="text-light text-underline hvr_clrp d-block close-loginform forgot-form-show f-16 pd-0"
                                        href="javascript:;"><small>Forgot password? Please click
                                            here</small></a></p>

                            </div>
                        </form>
                    </div>
                    <div class="forgot-form-block">
                        <form role="form" method="post" action="{{route('forgotpassword')}}" id="forgotpassword">
                            <div class="text-">
                                <h4 class="w-100 font-weight-normal text-black">Forgot Password</h4>
                            </div>
                            <div class="forgot-body">
                                <div id="forgot-message"></div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" name="email" class="form-control"
                                                placeholder="Enter email address">
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-login-blue" type="submit">SUBMIT</button>
                                <img src="{{admin_asset('images/loader.svg')}}" id="forgot-loading"
                                    style="display: none;">
                                <p class="pb-0"><a
                                        class="text-light text-underline hvr_clrp d-block close-loginform login-form-show f-14 pd-0"
                                        href="javascript:;"><small>Login?</small></a></p>
                            </div>
                        </form>
                    </div>

                </div>

            </nav>
            <div class="mobile-logo show cus-mobile-show">
                
                <a href="{{url('/')}}" title="aplikasi.us">
                    <img src="{{ admin_asset('images/logo.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                </a>
            </div>
            <div class="iam-hover-effect cus-mobile-show">
                @if(Auth::check())
                @if(Route::currentRouteName() == "howitworks" || Route::currentRouteName() == "termsConditions" ||
                Route::currentRouteName() == "contactusform" || Route::currentRouteName() == "privacyPolicy"||
                Route::currentRouteName() == "clientThankyou" || Route::currentRouteName() == "consultantThankyou" ||
                Route::currentRouteName() == "aboutus" || Route::currentRouteName() == "atapVirtualOffice" ||
                Route::currentRouteName() == "frontfaq" || Route::currentRouteName() == "home" )
                @if(Auth::user()->role_id == 1)
                <a class="btn btn-darkgreen btn-big  btnQuestions sidebar-open" href="{{route('client-home')}}"><i
                        class="fa fa-user" aria-hidden="true"></i></a>
                @elseif(Auth::user()->role_id == 2)
                <a class="btn btn-darkgreen btn-big  btnQuestions sidebar-open" href="{{route('consultant-home')}}"><i
                        class="fa fa-user" aria-hidden="true"></i></a>
                @else
                <a class="btn btn-darkgreen btn-big  btnQuestions sidebar-open" href="javascript:;"><i
                        class="fa fa-user" aria-hidden="true"></i></a>
                @endif
                @else
                <a class="btn btn-darkgreen btn-big  btnQuestions sidebar-open" href="javascript:;"><i
                        class="fa fa-user" aria-hidden="true"></i></a>
                @endif

                @else
                <a class="btn btn-darkgreen btn-big  btnQuestions link-cons-cli " href="javascript:;">I am a..</a>
                <div class="iam-box-hvr">
                    <div class="open page-cons-cli">
                        <ul class="i-con-cli">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('consultant-registration')}}"
                                    title="CONSULTANT">CONSULTANT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('client-registration')}}" title="CLIENT">CLIENT</a>
                            </li>
                        </ul>
                    </div>
                </div>
                @endif

            </div>
            <div class="cus-search-toggle cus-mobile-show">
                <i class="fas fa-search colorWhite"></i>
            </div>
        </div>
    </div>
    {{--     @if(Route::currentRouteName() == "home")
    <div class="mainNavbar">
        <div class="container">
            <h1 id="logo">

                 
        <a href="{{url('/')}}" title="aplikasi.us">
            <!-- @endif -->
            <img src="{{ admin_asset('images/logo2.png')}}" alt="aplikasi.us" title="aplikasi.us" />
        </a>
        </h1>
        <ul class="navbar-nav navbarRight">
            @if(!Auth::check())
            <li class="nav-item active">
                <a class="nav-link" href="{{route('consultant-registration')}}" title="I'MA CONSULTANT">I'M A
                    CONSULTANT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('client-registration')}}" title="I'MA CLIENT">I'M A CLIENT</a>
            </li>
            @endif
            <li class="nav-item">
                @if(Route::currentRouteName() == 'home')
                <a class="btnSearch jobsTab" href="javascript:;" title="Search">
                    @else
                    <a class="btnSearch" href="{{route('home',['search' => 'search-job'])}}" title="Search">
                        @endif
                        Search</a>
            </li>
        </ul>
        </div>
        </div>
        @else --}}
        <div class="tab-mobile-show mobile-header-search">
            {{-- @if(Auth::check())
            @if(Auth::user()->role_id == 1)
                {{ Form::open(['route' => ['search'], 'method' => 'GET', 'id'=>'mobile-search-form','autocomplete' => 'off']) }}
            @elseif(Auth::user()->role_id == 2)
            {{ Form::open(['route' => ['consultantsearch'], 'method' => 'GET', 'id'=>'mobile-search-form','autocomplete' => 'off']) }}
            @else
            {{ Form::open(['route' => ['general-search'], 'method' => 'GET', 'id'=>'mobile-search-form','autocomplete' => 'off']) }}
            @endif
            @else
            {{ Form::open(['route' => ['general-search'], 'method' => 'GET', 'id'=>'mobile-search-form','autocomplete' => 'off']) }}
            @endif --}}
            {{ Form::open(['route' => ['general-search'], 'method' => 'GET', 'id'=>'mobile-search-form','autocomplete' => 'off']) }}
            <input type="hidden" name="search_type" id="search_type" class="both-search_type" value="<?php
                                                if (Route::currentRouteName() == 'client-registration') {
                                                    echo 'consultant';
                                                } else {
                                                    if (isset($search_type)) {
                                                        echo $search_type;
                                                    } else {
                                                        echo 'job';
                                                    }
                                                }
                                                ?>" />
            <div class="headerMain">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-8 cus-header-test">
                            <div class="boxTabsearch">
                                <ul class="nav nav-tabs" id="search-job-consultants-section" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link <?php  //tab opt consultant -mobile
                                                                if (isset($search_type)) {
                                                                    if ($search_type == "job") {
                                                                        echo "active";
                                                                        }
                                                                    }

                                                                        else{
                                                                            if(Auth::check()){ 
                                                                                if(Auth::user()->role_id == 2){
                                                                                    echo "active";
                                                                                    
                                                                                }
                                                                            }
                                                                            elseif (Route::currentRouteName() == 'home' || Route::currentRouteName() != 'consultant-detail' || Route::currentRouteName() != 'client-registration' || Route::currentRouteName() == consultant) {
                                                                                echo "active";
                                                                            }
                                                                            
                                                                        }

                                                                            ?>" id="search-jobs" data-toggle="tab"
                                            href="#search-jobs-tab-mobile" role="tab"
                                            aria-controls="search-jobs-tab-mobile" aria-selected="false">Search Jobs</a>
                                    </li>
                                    <li class="nav-item">  
                                        <a class="nav-link <?php  //tab opt client - mobile
                                                                            if (isset($search_type)) { 
                                                                                if ($search_type == "consultant") {
                                                                                    echo "active";
                                                                                }
                                                                            }

                                                                            else{
                                                                                if (Auth::check()) {
                                                                                    if(Auth::user()->role_id == 1){
                                                                                        echo "active";
                                                                                    }
                                                                                }
                                                                                elseif (Route::currentRouteName() == 'client-registration' || Route::currentRouteName() == 'consultant-detail') {
                                                                                    echo "active";
                                                                                }
                                                                            }
                                                                            ?>" id="search-consultants"
                                            data-toggle="tab" href="#search-consultants-tab-mobile" role="tab" test="<?php echo(Route::currentRouteName())  ?>"
                                            aria-controls="search-consultants-tab-mobile" aria-selected="true">Search
                                            Consultants</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="search-job-consultants-content">
                                <div class="tab-pane fade <?php  //tab input consultant - mobile
                                                                              if (isset($search_type)) {
                                                                                if ($search_type == "job") {
                                                                                    echo "show active";
                                                                                    }
                                                                                }
    
                                                                                    else{
                                                                                        if(Auth::check()){ 
                                                                                            if(Auth::user()->role_id == 2){
                                                                                                echo "show active";
                                                                                                
                                                                                            }
                                                                                        }
                                                                                        elseif (Route::currentRouteName() == 'home' || Route::currentRouteName() != 'consultant-detail' || Route::currentRouteName() != 'client-registration') {
                                                                                            echo "show active";
                                                                                        }
                                                                                        
                                                                                    }

                                                                        ?>" id="search-jobs-tab-mobile" role="tabpanel"
                                        aria-labelledby="search-jobs">
                                        <div class="row mb-2">
                                            <div class="col-12 form-group mt-2 mb-2">
                                                <div class="ui-widget">
                                                    <div class="input-group">
                                                        <!-- <div class="input-group-prepend">
                                                                                        <div class="input-group-text">
                                                                                            <i class="fas fa-search"></i>
                                                                                        </div>
                                                                                    </div> -->
                                                        {{Form::text('jobsearchterm','',['class' => 'form-control search-jobresult','placeholder' => 'Search by Job ID, Job Title or Skills'])}}
                                                        <div class="job-suggestions"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group mt-2 mb-2">
                                                <!--                                                        {{Form::select('jobcategory',$categories, (isset($jobcategory) && $jobcategory != null) ? $jobcategory : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                <span class="form-control job-category-search1"
                                                    id="job-category-search1">
                                                    <?php 
                                                                                      if(isset($jobcategory)){
                                                                                        if(count($jobcategory)){
                                                                                            $jc1 = implode(',',$jobcategory);
                                                                                            echo '<span class="cat-chareecter-limit" title="'.$jc1.'">';
                                                                                            echo (strlen($jc1) > 30) ? substr($jc1,0,30)."..." : $jc1;
                                                                                            echo '</span>';
                                                                                        }else{
                                                                                          echo 'Categories';
                                                                                        }
                                                                                      }else{
                                                                                        echo 'Categories';
                                                                                      } 
                                                                                      ?>
                                                </span>
                                                <img class="image-click-cat"
                                                    src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="job-category-options form-control toogle-div"
                                                    style="display: none;">
                                                    <ul>
                                                        @foreach($categories as $catkey => $catvalue)
                                                        <li>
                                                            @if(isset($jobcategory))
                                                            @if(in_array($catvalue,$jobcategory))
                                                            <input type="checkbox" name="jobcategory[]"
                                                                value="{{$catvalue}}" checked="" />{{$catvalue}}
                                                            @else
                                                            <input type="checkbox" name="jobcategory[]"
                                                                value="{{$catvalue}}" />{{$catvalue}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="jobcategory[]"
                                                                value="{{$catvalue}}" />{{$catvalue}}
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-12 col-lg-12 mt-2">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <input type="text" name="job_baserate_from"
                                                            value="{{(isset($job_baserate_from)) ? $job_baserate_from : ''}}"
                                                            id="job_baserate_from" class="baserate_number form-control"
                                                            placeholder="Base Rate From" />
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="text" name="job_baserate_to"
                                                            value="{{(isset($job_baserate_to)) ? $job_baserate_to : ''}}"
                                                            id="job_baserate_to" class="baserate_number form-control"
                                                            placeholder="Base Rate To" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                                                    <div class="col-sm-6 form-group">
                                                                                        <button type="button" class="btn btn-danger btn-block submit-search border-rad-9 ">Search</button>
                                                                                    </div>
                                                                                    
                                                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-12">
                                                <div class="row">
                                                    <div class="col-6 col-sm-6 form-group selecttransparent">
                                                        <!--{{Form::select('job_mode1',[null => 'Job Mode'] + $jobModeList, (isset($job_mode1) && $job_mode1 != null) ? $job_mode1 : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                        <ul class="mobileheader-checkbox">


                                                            @foreach($jobModeList as $jobmodekey => $jobmodeValue)
                                                            <li>
                                                                @if(isset($job_mode1))
                                                                @if(in_array($jobmodeValue,$job_mode1))
                                                                <input type="checkbox" name="job_mode1[]" class=""
                                                                    value="{{$jobmodeValue}}"
                                                                    checked="" />{{$jobmodeValue}}
                                                                @else
                                                                <input type="checkbox" name="job_mode1[]" class=""
                                                                    value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="job_mode1[]" class=""
                                                                    value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                                @endif
                                                            </li>
                                                            @endforeach
                                                            @foreach($jobTypeList as $jobtypekey => $jobtypeValue)
                                                            <li>
                                                                @if(isset($jobtype))
                                                                @if(in_array($jobtypeValue,$jobtype))
                                                                <input type="checkbox" name="job_type[]" class=""
                                                                    value="{{$jobtypeValue}}"
                                                                    checked="" />{{$jobtypeValue}}
                                                                @else
                                                                <input type="checkbox" name="job_type[]" class=""
                                                                    value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="job_type[]" class=""
                                                                    value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                                @endif
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div class="col-6 col-sm-6 form-group selecttransparent">
                                                        {{Form::select('work_experience',[null => 'Experience'] + $experienceList,(isset($work_experience) && $work_experience != null) ? $work_experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                        {{Form::select('job_start_date',[null => 'Start Date'] + $startDateList,(isset($job_start_date) && $job_start_date != null) ? $job_start_date : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                        {{Form::select('job_duration',[null => 'Duration'] + $durationList,(isset($job_duration) && $job_duration != null) ? $job_duration : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                        {{Form::select('job_industry',[null => 'Industry'] + $industryList,(isset($job_industry) && $job_industry != null) ? $job_industry : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                        <span class="job-state-lable font-weight-normal"
                                                            id="job-state-lable">State</span>
                                                        <img src="{{admin_asset('images/iconSelect.png')}}">
                                                        <div class="job-state-options form-control toogle-div"
                                                            style="display: none;">
                                                            <ul>
                                                                <li class="close-job-state">Close</li>
                                                                @foreach($states as $statekey1 => $statevalue1)
                                                                <li>
                                                                    @if(isset($job_state))
                                                                    @if(in_array($statevalue1,$job_state))
                                                                    <input type="checkbox" name="job_state[]" class=""
                                                                        value="{{$statevalue1}}"
                                                                        checked="" />{{$statevalue1}}
                                                                    @else
                                                                    <input type="checkbox" name="job_state[]" class=""
                                                                        value="{{$statevalue1}}" />{{$statevalue1}}
                                                                    @endif
                                                                    @else
                                                                    <input type="checkbox" name="job_state[]" class=""
                                                                        value="{{$statevalue1}}" />{{$statevalue1}}
                                                                    @endif
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <button type="button"
                                                        class="btn btn-danger btn-block  mobilesubmit-search border-rad-9 font-weight-normal">Search</button>
                                                </div>
                                                <div class="col-12 form-group">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade <?php  //tab input client - mobile

                                                       if (isset($search_type)) {
                                                                            if ($search_type == "consultant") {
                                                                                echo "show active";
                                                                            }
                                                                        }

                                                                        else{
                                                                            if(Auth::check()){ 
                                                                                if(Auth::user()->role_id == 1){
                                                                                    echo "show active";
                                                                                    
                                                                            }
                                                                        }
                                                                            if(Route::currentRouteName() == 'consultant-detail' || Route::currentRouteName() == 'client-registration'){
                                                                                echo "show active";
                                                                            }
    
                                                                        }
                                                                   
                                                                        ?>" id="search-consultants-tab-mobile"
                                        role="tabpanel" aria-labelledby="search-consultants">

                                        <div class="row mb-2">
                                            <div class="col-12 mt-2 mb-2">
                                                <div class="input-group">
                                                    
                                                    {{Form::text('searchterm',isset($searchterm) ? $searchterm : '',['class' => 'form-control search-consulresult','placeholder' => 'Search by Consultant ID, Job Title or Skill'])}}
                                                    <div class="consutant-suggestions"></div>
                                                </div>
                                            </div>
                                            <div class="col-12 mt-2">
                                                <span class="form-control category-search1" id="category-search1">
                                                    <?php 
                                                                            if(isset($category)){
                                                                                if(count($category)){
                                                                                    $cs1 = implode(',',$category);
                                                                                    echo '<span class="cat-chareecter-limit" title="'.$cs1.'">';
                                                                                    echo (strlen($cs1) > 30) ? substr($cs1,0,30)."..." : $cs1;
                                                                                    echo '</span>';                                                                                    
                                                                                }else{
                                                                                    echo 'Categories';
                                                                                }
                                                                            }else{
                                                                                echo 'Categories';
                                                                            }
                                                                        ?>
                                                </span>
                                                <img class="image-click-cat"
                                                    src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="category-options form-control toogle-div"
                                                    style="display: none;">
                                                    <ul>
                                                        @foreach($categories as $catkey => $catvalue)
                                                        <li>
                                                            @if(isset($category))
                                                            @if(in_array($catvalue,$category))
                                                            <input type="checkbox" name="category[]"
                                                                value="{{$catvalue}}" checked="" />{{$catvalue}}
                                                            @else
                                                            <input type="checkbox" name="category[]"
                                                                value="{{$catvalue}}" />{{$catvalue}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="category[]"
                                                                value="{{$catvalue}}" />{{$catvalue}}
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-12 mt-3">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <input type="text" name="baserate_from"
                                                            value="{{(isset($baserate_from)) ? $baserate_from : ''}}"
                                                            id="baserate_from" class="form-control  baserate_number"
                                                            placeholder="Base Rate From" />
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="text" name="baserate_to"
                                                            value="{{(isset($baserate_to)) ? $baserate_to : ''}}"
                                                            id="baserate_to" class="form-control baserate_number"
                                                            placeholder="Base Rate To" />
                                                    </div>
                                                    <!-- <li>
                                                                                        <button type="button" class="btn btn-secondary clt-blue submit-search">Apply</button>
                                                                                    </li> -->
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-12 order-md-4">
                                                <div class="row">
                                                    <div class="col-6 form-group selecttransparent">
                                                        <ul class="mobileheader-checkbox">
                                                            @foreach($workPrefrencesArray as $preferencekey =>
                                                            $preferencevalue)
                                                            <li>
                                                                @if(isset($workpreference))
                                                                @if(in_array($preferencevalue,$workpreference))
                                                                <input type="checkbox" name="work_preference[]"
                                                                    value="{{$preferencevalue}}"
                                                                    checked="" />{{$preferencevalue}}
                                                                @else
                                                                <input type="checkbox" name="work_preference[]"
                                                                    value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="work_preference[]"
                                                                    value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                                @endif
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div
                                                        class="col-6 col-sm-6 col-md-auto form-group selecttransparent">
                                                        {{Form::select('experience',[null => 'Experience'] + $experienceList,isset($experience) ? $experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                        {{Form::select('availability_date',[null => 'Availability Date'] + $avalabilityDateArray,isset($avalabilityDate) ? $avalabilityDate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                        <span class="consultant-state-lable"
                                                            id="consultant-state-lable">State</span>
                                                        <img src="{{admin_asset('images/iconSelect.png')}}">
                                                        <div class="consultant-state-options form-control toogle-div"
                                                            style="display: none;">
                                                            <ul>
                                                                <li class="close-consutant-state">Close</li>
                                                                @foreach($states as $statekey => $statevalue)
                                                                <li>
                                                                    @if(isset($state))
                                                                    @if(in_array($statevalue,$state))
                                                                    <input type="checkbox" name="state[]"
                                                                        value="{{$statevalue}}"
                                                                        checked="" />{{$statevalue}}
                                                                    @else
                                                                    <input type="checkbox" name="state[]"
                                                                        value="{{$statevalue}}" />{{$statevalue}}
                                                                    @endif
                                                                    @else
                                                                    <input type="checkbox" name="state[]"
                                                                        value="{{$statevalue}}" />{{$statevalue}}
                                                                    @endif
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col mt-5">
                                                <div class="col-12 form-group">
                                                    <button type="button"
                                                        class="btn btn-danger btn-block mobilesubmit-search border-rad-9 font-weight-normal"
                                                        name="search" value="search">Search</button>
                                                </div>
                                                <!-- <div class="col-12 form-group">
                                                    <img src="{{admin_asset('images/new-design/wishlist-1.png')}}" >
                                                    <a href="{{route('client-home')}}"
                                                        class="font-weight-normal"
                                                        name="wishlist">My Wishlist</a> 
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="boxFilters <?php
                                                            if (isset($search_type) && $search_type == "consultant" && (Route::currentRouteName() == "general-search" || Route::currentRouteName() == "consultantsearch")) {
                                                                echo "show";
                                                            } else {
                                                                echo "hide";
                                                            }
                                                            ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>You searched for:
                                            <span class="text-red">
                                                {{(isset($searchterm) && $searchterm != null) ? $searchterm : ""}}
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="listCheckbox checkboxinline">
                                            <li>
                                                @if(isset($job_title_matching) && $job_title_matching == "Yes")
                                                <input type="checkbox" class="subfilter" name="job_title_matching"
                                                    title="Keywords-matching Job Titles Only" checked="">
                                                Keywords-matching Job Titles Only
                                                @else
                                                <input type="checkbox" class="subfilter" name="job_title_matching"
                                                    title="Keywords-matching Job Titles Only"> Keywords-matching Job
                                                Titles Only
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <p>
                                            <a href="javascript:void(0)" class="text-red clear-filter"
                                                id="clear-filter">Clear all filters</a>
                                    </div>
                                </div>
                                <div class="row rs-5">
                                    <div class="col-6">
                                        <ul class="mobileheader-checkbox">
                                            <li>
                                                @if(isset($available) && $available == "Yes")
                                                <input type="checkbox" class="subfilter" name="available"
                                                    title="Available" checked=""> Available
                                                @else
                                                <input type="checkbox" class="subfilter" name="available"
                                                    title="Available"> Available
                                                @endif
                                            </li>
                                            <li>
                                                @if(isset($willing_to_travel) && $willing_to_travel == "Yes")
                                                <input type="checkbox" class="subfilter" name="willing_to_travel"
                                                    title="Willing to travel" checked=""> Willing to travel
                                                @else
                                                <input type="checkbox" class="subfilter" name="willing_to_travel"
                                                    title="Willing to travel"> Willing to travel
                                                @endif
                                            </li>
                                            <li>
                                                @if(isset($full_time) && $full_time == "Yes")
                                                <input type="checkbox" class="subfilter" name="full_time"
                                                    title="Full time" checked=""> Full time
                                                @else
                                                <input type="checkbox" class="subfilter" name="full_time"
                                                    title="Full time"> Full time
                                                @endif
                                            </li>
                                            <li>
                                                @if(isset($part_time) && $part_time == "Yes")
                                                <input type="checkbox" class="subfilter" name="part_time"
                                                    title="Part time" checked=""> Part time
                                                @else
                                                <input type="checkbox" class="subfilter" name="part_time"
                                                    title="Part time"> Part time
                                                @endif
                                            </li>
                                            <li>
                                                @if(isset($project) && $project == "Yes")
                                                <input type="checkbox" class="subfilter" name="project" title="Project"
                                                    checked=""> Project
                                                @else
                                                <input type="checkbox" class="subfilter" name="project" title="Project">
                                                Project
                                                @endif
                                            </li>
                                            <li>
                                                @if(isset($support) && $support == "Yes")
                                                <input type="checkbox" class="subfilter" name="support" title="Support"
                                                    checked=""> Support
                                                @else
                                                <input type="checkbox" class="subfilter" name="support" title="Support">
                                                Support
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-6">

                                        <ul class="mobileheader-checkbox">
                                            <li>
                                                <div class="dropdownTransparent">
                                                    <span class="category-search2" id="category-search2">
                                                        <?php 
                                                                                            if(isset($category2)){
                                                                                                if(count($category2)){
                                                                                                    $c2 = implode(',',$category2);
                                                                                                    echo '<span class="cat-chareecter-limit" title="'.$c2.'">';
                                                                                                    echo (strlen($c2) > 20) ? substr($c2,0,20)."..." : $c2;
                                                                                                    echo '</span>';
                                                                                                }else{
                                                                                                    echo 'Categories';
                                                                                                }
                                                                                            }else{
                                                                                                echo 'Categories';
                                                                                            }
                                                                                        ?>
                                                    </span>
                                                    <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                                                    <i class="fa fa-sort-desc colorWhite category-search2"
                                                        aria-hidden="true"></i>
                                                    <div class="form-control category-options2 toogle-div"
                                                        style="display: none;">
                                                        <ul>
                                                            @foreach($categories as $catkey => $catvalue)
                                                            <li class="category-options2-li">
                                                                @if(isset($category2))
                                                                @if(in_array($catvalue,$category2))
                                                                <input type="checkbox" name="category2[]"
                                                                    class="subfilter" value="{{$catvalue}}"
                                                                    checked="" />{{$catvalue}}
                                                                @else
                                                                <input type="checkbox" name="category2[]"
                                                                    class="subfilter"
                                                                    value="{{$catvalue}}" />{{$catvalue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="category2[]"
                                                                    class="subfilter"
                                                                    value="{{$catvalue}}" />{{$catvalue}}
                                                                @endif
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
 
                                            </li>
                                            <li>
                                                <div class="dropdownTransparent">
                                                    {{Form::select('experience2',[null => 'Experience'] + $experienceList,isset($experience2) ? $experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdownTransparent">
                                                    {{Form::select('availability_date2',[null => 'Availability Date'] + $avalabilityDateArray,isset($availability_date2) ? $availability_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                                                </div>
                                            </li>
                                        </ul>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}
        </div>
</header>
{{-- @if(Auth::check())
            @if(Auth::user()->role_id == 1)
                {{ Form::open(['route' => ['search'], 'method' => 'GET', 'id'=>'search-form','autocomplete' => 'off']) }}
@elseif(Auth::user()->role_id == 2)
{{ Form::open(['route' => ['consultantsearch'], 'method' => 'GET', 'id'=>'search-form','autocomplete' => 'off']) }}
@else
{{ Form::open(['route' => ['general-search'], 'method' => 'GET', 'id'=>'search-form','autocomplete' => 'off']) }}
@endif
@else
{{ Form::open(['route' => ['general-search'], 'method' => 'GET', 'id'=>'search-form','autocomplete' => 'off']) }}
@endif --}}
{{ Form::open(['route' => ['general-search'], 'method' => 'GET', 'id'=>'search-form','class'=>'headerNew  sticky-header','autocomplete' => 'off']) }}
<input type="hidden" name="search_type" id="search_type" class="both-search_type" value="<?php
                                            if (Route::currentRouteName() == 'client-registration') {
                                                echo 'consultant';
                                            } else {
                                                if (isset($search_type)) {
                                                    echo $search_type;
                                                } else {
                                                    echo 'job';
                                                }
                                            }
                                            ?>" />
<div class="headerMain cus-mobile-hide">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-2 text-right">
                <h1 id="logo">
                   
                    <a href="{{url('/')}}" title="aplikasi.us">
                        <img src="{{ admin_asset('images/logo.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                    </a>
                </h1>
            </div>
            <div class="col-md-8">
                <div class="boxTabsearch">
                    <ul class="nav nav-tabs" id="search-job-consultants-section" role="tablist">
                        
                    <li class="nav-item">
                            <a class="nav-link <?php //tab opt consultant
                                                                        if (isset($search_type)) {
                                                                            if ($search_type == "job") {
                                                                                echo "active";
                                                                                }
                                                                            }

                                                                                else{
                                                                                    if(Auth::check()){ 
                                                                                        if(Auth::user()->role_id == 2){
                                                                                            echo "active";
                                                                                            
                                                                                        }
                                                                                    }
                                                                                    elseif (Route::currentRouteName() == 'home' || Route::currentRouteName() != 'consultant-detail' || Route::currentRouteName() != 'client-registration' ) {
                                                                                        echo "active";
                                                                                    }
                                                                                    
                                                                                }
                                                                        
                                                                        ?>" id="search-jobs" data-toggle="tab"
                                href="#search-jobs-tab" role="tab" aria-controls="search-jobs-tab"
                                aria-selected="false">Search Jobs</a>
                        </li>
                    
                        <li class="nav-item"> 
                            <!-- tab menu -->
                            <a class="nav-link <?php //tab opt client
                                                                        if (isset($search_type)) {
                                                                            if ($search_type == "consultant") {
                                                                                echo "active";
                                                                            }
                                                                        }

                                                                        else{
                                                                            if(Auth::check()){ 
                                                                                if(Auth::user()->role_id == 1){
                                                                                    echo "active";
                                                                                    
                                                                            }
                                                                        }
                                                                            elseif(Route::currentRouteName() == 'consultant-detail' || Route::currentRouteName() == 'client-registration'){
                                                                                echo "active";
                                                                            }
    
                                                                        }
                                                                   
                                                                        ?>" id="search-consultants" data-toggle="tab"
                                href="#search-consultants-tab" role="tab" aria-controls="search-consultants-tab"
                                aria-selected="true">Search Consultants</a>
                        </li>
                        
                    </ul>
                    <div class="tab-content" id="search-job-consultants-content">
                        <div class="tab-pane fade <?php

                                                                    if (isset($search_type)) {
                                                                        if ($search_type == "consultant") {
                                                                            echo "show active";
                                                                        }
                                                                    }
                                                                    
                                                                    else{
                                                                        if(Auth::check()){ 
                                                                            if(Auth::user()->role_id == 1){
                                                                                echo "show active";
                                                                                
                                                                        }
                                                                    }

                                                                        elseif(Route::currentRouteName() == 'consultant-detail' || Route::currentRouteName() == 'client-registration'){
                                                                            echo "show active";
                                                                        }

                                                                    }
                                                                    
                                                                    
                                                                    ?>" id="search-consultants-tab" role="tabpanel"
                            aria-labelledby="search-consultants">

                            <div class="row mb-2">
                                <div class="col-sm-6 col-md-3">
                                    <span class="form-control category-search1" id="category-search1">
                                        <?php 
                                                                                        if(isset($category)){
                                                                                            if(count($category)){
                                                                                                $cc2 = implode(',',$category);
                                                                                                echo '<span class="cat-chareecter-limit" title="'.$cc2.'">';
                                                                                                echo (strlen($cc2) > 20) ? substr($cc2,0,20)."..." : $cc2;
                                                                                                echo '</span>';                                                                                                
                                                                                            }else{
                                                                                                echo 'Categories';
                                                                                            }
                                                                                        }else{
                                                                                            echo 'Categories';
                                                                                        }
                                                                                    ?>
                                    </span>
                                    <img class="image-click-cat" src="{{admin_asset('images/iconSelect.png')}}">
                                    <div class="category-options form-control toogle-div" style="display: none;">
                                        <ul>
                                            @foreach($categories as $catkey => $catvalue)
                                            <li>
                                                @if(isset($category))
                                                @if(in_array($catvalue,$category))
                                                <input type="checkbox" name="category[]" value="{{$catvalue}}"
                                                    checked="" />{{$catvalue}}
                                                @else
                                                <input type="checkbox" name="category[]"
                                                    value="{{$catvalue}}" />{{$catvalue}}
                                                @endif
                                                @else
                                                <input type="checkbox" name="category[]"
                                                    value="{{$catvalue}}" />{{$catvalue}}
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-7">
                                    <div class="ui-widget">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-search"></i>
                                                </div>
                                            </div>
                                            {{Form::text('searchterm',isset($searchterm) ? $searchterm : '',['class' => 'form-control search-consulresult','placeholder' => 'Search by Consultant ID, Job Title or Skill'])}}
                                            <div class="consutant-suggestions"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <button type="button"
                                                class="btn btn-danger btn-block submit-search border-rad-9 font-weight-normal"
                                                name="search" value="search">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 order-md-4">
                                    <div class="row">
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            <span class="baserate-1 font-weight-normal pr-3" id="baserate-1">Base
                                                Rate</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="baserate-1-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>
                                                    <li>
                                                        <input type="text" name="baserate_from"
                                                            value="{{(isset($baserate_from)) ? $baserate_from : ''}}"
                                                            id="baserate_from" class="baserate_number"
                                                            placeholder="Base Rate From" />
                                                    </li>
                                                    <li>
                                                        <input type="text" name="baserate_to"
                                                            value="{{(isset($baserate_to)) ? $baserate_to : ''}}"
                                                            id="baserate_to" class="baserate_number"
                                                            placeholder="Base Rate To" />
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            {{Form::select('experience',[null => 'Experience'] + $experienceList,isset($experience) ? $experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">

                                            {{Form::select('availability_date',[null => 'Availability Date'] + $avalabilityDateArray,isset($avalabilityDate) ? $avalabilityDate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">

                                            <span class="work-preference-lable font-weight-normal pr-3"
                                                id="work-preference-lable">Work Preferences</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="work-preference-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>
                                                    @foreach($workPrefrencesArray as $preferencekey =>
                                                    $preferencevalue)
                                                    <li>
                                                        @if(isset($workpreference))
                                                        @if(in_array($preferencevalue,$workpreference))
                                                        <input type="checkbox" name="work_preference[]"
                                                            value="{{$preferencevalue}}"
                                                            checked="" />{{$preferencevalue}}
                                                        @else
                                                        <input type="checkbox" name="work_preference[]"
                                                            value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="work_preference[]"
                                                            value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="">
                                            <!--{{Form::select('state',[null => 'State'] + $states,isset($state) ? $state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                            <span class="consultant-state-lable font-weight-normal pr-3"
                                                id="consultant-state-lable">State</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="consultant-state-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>
                                                    @foreach($states as $statekey => $statevalue)
                                                    <li>
                                                        @if(isset($state))
                                                        @if(in_array($statevalue,$state))
                                                        <input type="checkbox" name="state[]" value="{{$statevalue}}"
                                                            checked="" />{{$statevalue}}
                                                        @else
                                                        <input type="checkbox" name="state[]"
                                                            value="{{$statevalue}}" />{{$statevalue}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="state[]"
                                                            value="{{$statevalue}}" />{{$statevalue}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade <?php //tab consultant
                                                                    
                                                                    if (isset($search_type)) {
                                                                        if ($search_type == "job") {
                                                                            echo "show active";
                                                                            }
                                                                        }

                                                                            else{
                                                                                if(Auth::check()){ 
                                                                                    if(Auth::user()->role_id == 2){
                                                                                        echo "show active";
                                                                                        
                                                                                    }
                                                                                }
                                                                                elseif (Route::currentRouteName() == 'home' || Route::currentRouteName() != 'consultant-detail' || Route::currentRouteName() != 'client-registration') {
                                                                                    echo "show active";
                                                                                }
                                                                                
                                                                            }
                                                                    
                                                                    ?>" id="search-jobs-tab" role="tabpanel"
                            aria-labelledby="search-jobs">
                            <div class="row mb-2">
                                <div class="col-sm-6 col-md-3 form-group">
                                    <!--                                                        {{Form::select('jobcategory',$categories, (isset($jobcategory) && $jobcategory != null) ? $jobcategory : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                    <span class="form-control job-category-search1" id="job-category-search1">
                                        <?php 
                                                                                      if(isset($jobcategory)){
                                                                                        if(count($jobcategory)){
                                                                                            $jcm1 = implode(',',$jobcategory);
                                                                                            echo '<span class="cat-chareecter-limit" title="'.$jcm1.'">';
                                                                                            echo (strlen($jcm1) > 20) ? substr($jcm1,0,20)."..." : $jcm1;
                                                                                            echo '</span>';
                                                                                        }else{
                                                                                          echo 'Categories';
                                                                                        }
                                                                                      }else{
                                                                                        echo 'Categories';
                                                                                      } 
                                                                                      

                                                                                      ?>
                                    </span>
                                    <img class="image-click-cat" src="{{admin_asset('images/iconSelect.png')}}">
                                    <div class="job-category-options form-control toogle-div" style="display: none;">
                                        <ul>
                                            @foreach($categories as $catkey => $catvalue)
                                            <li>
                                                @if(isset($jobcategory))
                                                @if(in_array($catvalue,$jobcategory))
                                                <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}"
                                                    checked="" />{{$catvalue}}
                                                @else
                                                <input type="checkbox" name="jobcategory[]"
                                                    value="{{$catvalue}}" />{{$catvalue}}
                                                @endif
                                                @else
                                                <input type="checkbox" name="jobcategory[]"
                                                    value="{{$catvalue}}" />{{$catvalue}}
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-lg-7 form-group">

                                    <div class="ui-widget">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-search"></i>
                                                </div>
                                            </div>
                                            {{Form::text('jobsearchterm','',['class' => 'form-control search-jobresult','placeholder' => 'Search by Job ID, Job Title or Skills'])}}
                                            <div class="job-suggestions"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-2 p-md-l-5">
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <button type="button"
                                                class="btn btn-danger btn-block submit-search border-rad-9 font-weight-normal">Search</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="row">

                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            <!--{{Form::select('job_baserate',[null => 'Base Rate'] + $baserateArray,(isset($job_baserate) && $job_baserate != null) ? $job_baserate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                            <span class="job-baserate-1 font-weight-normal pr-3"
                                                id="job-baserate-1 ">Base Rate</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="job-baserate-1-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>
                                                    <li>
                                                        <input type="text" name="job_baserate_from"
                                                            value="{{(isset($job_baserate_from)) ? $job_baserate_from : ''}}"
                                                            id="job_baserate_from" class="baserate_number"
                                                            placeholder="Base Rate From" />
                                                    </li>
                                                    <li>
                                                        <input type="text" name="job_baserate_to"
                                                            value="{{(isset($job_baserate_to)) ? $job_baserate_to : ''}}"
                                                            id="job_baserate_to" class="baserate_number"
                                                            placeholder="Base Rate To" />
                                                    </li>
                                                    <!-- <li>
                                                                                                    <button type="button"
                                                                                                        class="btn btn-secondary clt-blue submit-search font-weight-normal">Apply</button>
                                                                                                </li> -->
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            <!--{{Form::select('job_mode1',[null => 'Job Mode'] + $jobModeList, (isset($job_mode1) && $job_mode1 != null) ? $job_mode1 : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                            <span class="job-mode1-lable font-weight-normal pr-3"
                                                id="job-mode1-lable">Job Mode</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="job-mode1-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>
                                                    @foreach($jobModeList as $jobmodekey => $jobmodeValue)
                                                    <li>
                                                        @if(isset($job_mode1))
                                                        @if(in_array($jobmodeValue,$job_mode1))
                                                        <input type="checkbox" name="job_mode1[]"
                                                            value="{{$jobmodeValue}}" checked="" />{{$jobmodeValue}}
                                                        @else
                                                        <input type="checkbox" name="job_mode1[]"
                                                            value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="job_mode1[]"
                                                            value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            <span class="job-type1-lable font-weight-normal pr-3"
                                                id="job-type1-lable">Job Type</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="job-type1-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>

                                                    @foreach($jobTypeList as $jobtypekey => $jobtypeValue)
                                                    <li>
                                                        @if(isset($jobtype))
                                                        @if(in_array($jobtypeValue,$jobtype))
                                                        <input type="checkbox" name="job_type[]" class=""
                                                            value="{{$jobtypeValue}}" checked="" />{{$jobtypeValue}}
                                                        @else
                                                        <input type="checkbox" name="job_type[]" class=""
                                                            value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="job_type[]" class=""
                                                            value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            {{Form::select('work_experience',[null => 'Experience'] + $experienceList,(isset($work_experience) && $work_experience != null) ? $work_experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            {{Form::select('job_start_date',[null => 'Start Date'] + $startDateList,(isset($job_start_date) && $job_start_date != null) ? $job_start_date : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                            {{Form::select('job_duration',[null => 'Duration'] + $durationList,(isset($job_duration) && $job_duration != null) ? $job_duration : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                        </div>
                                        <div
                                            class="col-4 col-sm-3 col-md-auto form-group selecttransparent job_industry-width">
                                            {{Form::select('job_industry',[null => 'Industry'] + $industryList,(isset($job_industry) && $job_industry != null) ? $job_industry : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                        </div>
                                        <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="">
                                            <!--{{Form::select('job_state',[null => 'State'] + $states,(isset($job_state) && $job_state != null) ? $job_state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                            <span class="job-state-lable font-weight-normal pr-3"
                                                id="job-state-lable">State</span>
                                            <img class="image-click-ab" src="{{admin_asset('images/iconSelect.png')}}">
                                            <div class="job-state-options form-control toogle-div"
                                                style="display: none;">
                                                <ul>
                                                    @foreach($states as $statekey1 => $statevalue1)
                                                    <li>
                                                        @if(isset($job_state))
                                                        @if(in_array($statevalue1,$job_state))
                                                        <input type="checkbox" name="job_state[]" class=""
                                                            value="{{$statevalue1}}" checked="" />{{$statevalue1}}
                                                        @else
                                                        <input type="checkbox" name="job_state[]" class=""
                                                            value="{{$statevalue1}}" />{{$statevalue1}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="job_state[]" class=""
                                                            value="{{$statevalue1}}" />{{$statevalue1}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-md-2">
                <!-- <a class="btn btn-warning  btnQuestions" href="{{route('atapVirtualOffice')}}">Have any questions?</a> -->
                <!-- <div class="iam-hover-effect cus-mobile-hide">
                    <a class="btn btn-darkgreen btn-big  btnQuestions link-cons-cli " href="javascript:;">I am
                        a..</a>
                    <div class="iam-box-hvr">
                        <div class="open page-cons-cli">
                            <ul class="i-con-cli">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('consultant-registration')}}"
                                        title="CONSULTANT">CONSULTANT</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('client-registration')}}"
                                        title="CLIENT">CLIENT</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                @if(Auth::check())
                @if(Auth::user()->role_id == 1)
                <a href="{{route('client-home')}}" class="font-weight-normal" name="wishlist" style="margin: 5px 5px 5px 5px; color:white"> <img src="{{admin_asset('images/new-design/wishlist-1.png')}}" style="height:25px;weight:25px;" >My Wishlist</a> 
                @elseif(Auth::user()->role_id == 2)
                <a href="{{route('consultant-home')}}" class="font-weight-normal" name="wishlist" style="margin: 5px 5px 5px 5px; color:white"> <img src="{{admin_asset('images/new-design/wishlist-1.png')}}" style="height:25px;weight:25px;" >My Wishlist</a> 
                @endif
                @else
                <a class="font-weight-normal login-form-poup" name="wishlist" style="margin: 5px 5px 5px 5px; color:white"> <img src="{{admin_asset('images/new-design/wishlist-1.png')}}" style="height:25px;weight:25px;" >My Wishlist</a> 
                @endif
            </div>
            
        </div>
    </div>
</div>
<input type="hidden" name="search_type" id="search_type" class="both-search_type" value="<?php
                                            if (Route::currentRouteName() == 'client-registration') {
                                                echo 'consultant';
                                            } else {
                                                if (isset($search_type)) {
                                                    echo $search_type;
                                                } else {
                                                    echo 'job';
                                                }
                                            }
                                            ?>" />
<!--Box Filer For Consultant-->

<!--Box Filter For Job-->

{{Form::close()}}

{{-- @endif --}}