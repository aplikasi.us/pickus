<div class="boxStar">
    <div id="carouselAboutus" class="carousel slide" data-ride="carousel">
        @if(count($rating))
            @foreach ($rating as $key => $value)
            <div class="carousel-item {{($key == 0 )  ? 'active' : ''}}">
                <p class="starBig">
                    @if($value != null)
                    @if($value->subcategory == "1 Star")
                    <img src="{{admin_asset('images/one-star.png')}}" alt="" title="">
                    @elseif($value->subcategory == "2 Star")
                    <img src="{{admin_asset('images/two-star.png')}}" alt="" title="">
                    @elseif($value->subcategory == "3 Star")
                    <img src="{{admin_asset('images/three-star-big.png')}}" alt="" title="">
                    @elseif($value->subcategory == "4 Star")
                    <img src="{{admin_asset('images/four-star.png')}}" alt="" title="">
                    @elseif($value->subcategory == "5 Star")
                    <img src="{{admin_asset('images/five-star.png')}}" alt="" title="">
                    @else
                    
                    @endif
                    @endif
                </p>
                <p class="Black">{{(count($value) ? $value->content : "Rating coming shortly")}}</p>
            </div>
            @endforeach
            <a class="carousel-control-prev" href="#carouselAboutus" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
            <a class="carousel-control-next" href="#carouselAboutus" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        @endif
    </div>
</div>