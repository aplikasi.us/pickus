<h5 class="pt-2">Recently Viewed...</h5>
<div class="recently-viewed-box">
    @if(count($recentlyViewed))
    @foreach($recentlyViewed as $recentKey => $recentValue)
    @if($recentKey < 3)
    <div class="commanContent commanContent-custom">
        @if($recentValue->candidate->available_for_contract == 1)
            <div class="status online" data-toggle="tooltip" title="Available"></div>
        @else
            <div class="status offline" data-toggle="tooltip" title="Unavailable"></div>
        @endif
        <!-- <div class="status online"></div> -->
        <ul class="listContract">
            <li>{{$recentValue->candidate->candidate_id}}
                <div class="box-rating consultant-start-rating">
                    @for($i = 1; $i <= 5; $i++)
                    @if($recentValue->candidate->rating < $i)
                    <span class="fa fa-star unchecked"></span>
                    @else
                    <span class="fa fa-star checked"></span>
                    @endif
                    @endfor
                </div>
            </li>
        </ul>
        <h5>{{$recentValue->candidate->sap_job_title}}</h5>
        <div class="row rowContract">
            <div class="col-md-12">
                <div class="cus-verticale-center">
                    <div class="text-red inlineText">
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                        {{$recentValue->candidate->base_rate}}/day
                        <!-- <div class="iconBox iconCopy inlineText"></div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row rowContract">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline">{{$recentValue->candidate->experience_in_years}} years exp.</div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">
                <img src="{{admin_asset('images/cus-group-6.png')}}" class="icon-cus-img will-travel-img">
                <div class="ml-1 Black d-inline {{($recentValue->candidate->willing_to_travel == 1) ? '' : 'disabled'}}">{{($recentValue->candidate->willing_to_travel == 1) ? "Ok to Travel" : "Travel"}}</div>
            </div>


            
            
        </div>
        <div class="row rowContract">

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline">{{$recentValue->candidate->notice_period_days - (int) $recentValue->candidate->notice_period_days}} months</div>
            </div>


            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->project == 1) ? '' : 'disabled'}}">
                <img src="{{admin_asset('images/cus-Group-9.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline {{($recentValue->candidate->project == 1) ? '' : 'disabled'}}">{{($recentValue->candidate->project == 1) ? "Project" : "Project"}}</div>
            </div>

        </div>
        <div class="row rowContract">
        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline">{{date('d M. Y',strtotime($recentValue->candidate->availability_date))}}</div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale">
                <img src="{{admin_asset('images/cus-Group-3.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline {{($recentValue->candidate->support == 1) ? '' : 'disabled'}}">{{($recentValue->candidate->support == 1) ? "Support" : "Support"}}</div>
            </div>
            
        </div>
        <div class="row rowContract">
        
        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->full_time == 1) ? '' : 'disabled'}}">
                <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline {{($recentValue->candidate->full_time == 1) ? '' : 'disabled'}}">{{($recentValue->candidate->full_time == 1) ? "Full time" : "Full time"}}</div>
            </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6">
                <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline">{{$recentValue->candidate->city}}</div>
            </div>

        </div>
        <div class="row rowContract">
        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-6 unavalibale {{($recentValue->candidate->part_time == 1) ? '' : 'disabled'}}">
                <img src="{{admin_asset('images/cus-Group-8.png')}}" class="icon-cus-img">
                <div class="ml-1 Black d-inline {{($recentValue->candidate->part_time == 1) ? '' : 'disabled'}}">{{($recentValue->candidate->part_time == 1) ? "Part time" : "Part time"}}</div>
            </div>
        </div>
        <div class="boxbtns">
            <div class="row no-gutters border-top-btn">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 border-right-btn">
                    <!-- <a title="Add to Compare" href="#"
                       class="btn btn-danger btn-block btnCompare btn-custom-trans colorRed">Add to Compare</a> -->
                    @php
                    $btnCompare = 'can-btnRemoveCompare';
                    $btntext = 'Remove Compare';
                    if(!in_array($recentValue->candidate->CANDIDATEID,$addTocompare_id)){
                    $btnCompare = 'can-btnCompare';
                    $btntext = 'Add to Compare';
                    }
                    @endphp

                    <a title="{{$btntext}}" href="javascript:;"  data-id="{{$recentValue->candidate->CANDIDATEID}}" data-candidate-id="{{$recentValue->candidate->id}}" data-reload="true" class="btn btn-custom-trans colorBlue btn-block addcompare-icon {{$btnCompare}}">{{$btntext}}</a>
                    <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading" style="display: none;">
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    @if(in_array($recentValue->candidate->CANDIDATEID,$wishlist))
                    <a title="Remove Wishlist" href="javascript:;" class="btn btn-secondary btn-block whishilst-herat-icon remove-to-wish-list-con colorRed btn-custom-trans" data-id="{{$recentValue->candidate->CANDIDATEID}}"  data-reloadtab=""  data-candidate-id="{{$recentValue->candidate->id}}">Remove Wishlist</a>
                    @else
                    <a title="Save to Wishlist" href="javascript:void(0)" class="btn btn-secondary btn-block whishilst-herat-icon save-to-wish-list colorRed btn-custom-trans" data-id="{{$recentValue->candidate->CANDIDATEID}}"  data-reloadtab=""  data-candidate-id="{{$recentValue->candidate->id}}">Save to Wishlist</a>
                    @endif
                    <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading" style="display: none;">
                </div>
            </div>
        </div>
    </div>
    @endif
    @endforeach

    <div class="mb-4">
        <a href="{{route('general-search')}}" class="recently-viewed-seeall" title="See All">See All ></a>
    </div>
</div>
@endif