<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>aplikasi.us - @yield('title')</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="fb:app_id" content="549828442206516">
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:image" content="http://aplikasi.us/public/images/fblogo.png">
    <meta property="og:url" content="{{url()->current()}}">

    <meta name="twitter:title" content="@yield('title')">
    <meta name="twitter:description" content="@yield('description')">
    <meta name="twitter:image" content="http://aplikasi.us/public/images/fblogo.png">
    <meta name="twitter:card" content="http://aplikasi.us/public/images/fblogo.png">

    <!-- <link href="{{admin_asset('css/style.min.css')}}" rel="stylesheet" type="text/css"> -->
    <link href="{{admin_asset('css/summernote_editor.css')}}" rel="stylesheet">
    <link href="{{admin_asset('css/custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{admin_asset('css/theme_custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{admin_asset('css/new_custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{admin_asset('css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
    <link href="{{admin_asset('css/grt-youtube-popup.css')}}" rel="stylesheet" type="text/css">
    <!--Date Picker-->
    <link href="{{admin_asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{admin_asset('css/toastr.min.css')}}" rel="stylesheet">
    <link href="{{admin_asset('css/toastr.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ admin_asset('Favicon.png') }}">
</head>