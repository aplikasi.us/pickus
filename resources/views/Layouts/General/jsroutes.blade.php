<!--Js Routes-->
<script>
    var emailExists = "{!! route('email-exists') !!}";
    var getCityList = "{!! route('city-list') !!}";
    var saveToWishlist = "{!! route('client-saveto-wishlist') !!}";
    var removeToWishlist_cons = "{!! route('client-removeto-wishlist') !!}";
    var uploadattachment = "{!! route('uploadAttachment') !!}";
    var uploadattachment = "{!! route('ResumeAttachment') !!}";
    var consultantsavejob = "{!! route('consultantsavejob') !!}";
    var consultantappliedjob = "{!! route('consultantAppliedJob') !!}";
    var consultantWithdrawjob = "{!! route('consultantwithdrawjob') !!}";
    var consultantprofileavaliable = "{!! route('consultantprofileavaliable') !!}";
    var consultantDeleteJob = "{!! route('consultantdeletejob')!!}";
    var clientUpdateProfile = "{!! route('client_update_profile')!!}";
    var getInterviewNotes = "{!! route('getinterviewnotes')!!}";
    var readNotification = "{!! route('readnotification')!!}";
    var getConsultantProfile = "{!! route('getconsultantprofile')!!}";
    var getContactProfile = "{!! route('getcontactprofile')!!}";
    var uploadContactProfilePicture = "{!! route('uploadContactProfilePicture')!!}";
    var viewContactProfile = "{!! route('viewcontactprofile')!!}";
    var removeProfilePic = "{!! route('remove-profile-pic')!!}";
    var removeContactProfilePic = "{!! route('remove-contact-profile-pic')!!}";
    var getSearchConsultant = "{!! route('getSearchConsultant') !!}";
    var getJobConsultant = "{!! route('getJobConsultant') !!}";
    var addCompare = "{!! route('addCompare') !!}";
    var removeComparepage = "{!! route('removeComparepage') !!}";
    
</script>