<!DOCTYPE html>
<html lang="en">
    @include('Layouts.General.head_new')

    <body class="pageLatest">
        @include('Layouts.General.header_new')
        @if(Route::currentRouteName() == "general-search" || Route::currentRouteName() == "job-details" || Route::currentRouteName() == "consultant-detail")
        <article>
            <section class="secColumn">
                <div class="row rowColumns no-gutters">
                    <div class="col-md-12 columnRight bg-cusgray">
                        {{-- @yield('content') --}}
                    </div>
                </div>
            </section>
        </article>
        @else
            @yield('content')
        @endif

        @include('Layouts.General.footer_new')

        @include('Layouts.General.foot_new')

        @yield('javascript')
    </body>

</html>