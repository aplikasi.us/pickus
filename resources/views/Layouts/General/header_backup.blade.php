<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="float-right">
                        <div class="navigation-section float-left">
                            <ul class="nav nav-pills">
                                <li class="close-menu"><span class="fa fa-close"></span></li>
                                @if(Session::get('territory') != "")
                                <li class="nav-item active"><a class="nav-link" href="javascript:void(0)" id="territory-link">{{Session::get('territory')}}</a></li>
                                @endif
                                <li class="nav-item"><a class="nav-link" href="{{route('frontfaq')}}">FAQ</a></li>
                                <li class="nav-item"><a class="nav-link" href="http://206.189.86.153/blog/">Blog</a></li>
                                <li class="nav-item {{ (Route::currentRouteName() == "aboutus") ? 'active' : '' }}"><a class="nav-link" href="{{route('aboutus')}}">Know us More!</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{route('howitworks')}}">How it works</a></li>
                            </ul>
                        </div>
                        <div class="navigation-user-section float-left">
                            <ul class="nav nav-pills">
                                <li class="close-menu"><span class="fa fa-close"></span></li>
                                @if(Auth::check())
                                @if(Auth::user()->role_id == 1)
                                <li class="nav-item"><a class="nav-link" href="{{route('client-home')}}">
                                        <img class="img-fluid mr-2" src="{{admin_asset('images/logout-icon.png')}}"
                                            alt="logout">My Account</a></li>
                                @else
                                <li class="nav-item"><a class="nav-link" href="{{route('consultant-home')}}">
                                        <img class="img-fluid mr-2" src="{{admin_asset('images/logout-icon.png')}}"
                                            alt="logout">My Account</a></li>
                                @endif
                                @else
                                <li class="nav-item"><a class="login-form-poup nav-link login-link" href="javascript:;" > <img
                                            class="img-fluid mr-2" src="{{admin_asset('images/logout-icon.png')}}"
                                            alt="login" title="">Login</a></li>
                                <li class="nav-item"><a class="nav-link join-link" href="{{route('client-registration')}}">JOIN US</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::check())
        @if(Auth::user()->role_id = 1)
            {{ Form::open(['route' => ['search'], 'method' => 'POST', 'id'=>'search-form','autocomplete' => 'off']) }}
        @elseif(Auth::user()->role_id = 2)
            {{ Form::open(['route' => ['consultantsearch'], 'method' => 'POST', 'id'=>'search-form','autocomplete' => 'off']) }}
        @else
            {{ Form::open(['route' => ['general-search'], 'method' => 'POST', 'id'=>'search-form','autocomplete' => 'off']) }}
        @endif
    @else
    {{ Form::open(['route' => ['general-search'], 'method' => 'POST', 'id'=>'search-form','autocomplete' => 'off']) }}
    @endif
    <input type="hidden" name="search_type" id="search_type" value="{{isset($search_type) ? $search_type : "consultant"}}"/>
    <div class="header-bottom">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-2 text-right">
                    <h1 id="logo">
                        @if(Auth::check())
                            @if(Auth::user()->role_id == 1)
                                <a href="{{route('client-home')}}" title="aplikasi.us">
                            @else
                                <a href="{{route('consultant-home')}}" title="aplikasi.us">
                            @endif
                        @else
                                <a href="{{url('/')}}" title="aplikasi.us">
                        @endif
                            <img src="{{ admin_asset('images/logo.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                        </a>
                    </h1>
                </div>
                <div class="col-md-8">
                    <div class="boxTabsearch">
                        <ul class="nav nav-tabs" id="search-job-consultants-section" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link <?php if(isset($search_type)) { if($search_type == "consultant") { echo "active"; } }else{ echo "active"; } ?>" id="search-consultants" data-toggle="tab"
                                   href="#search-consultants-tab" role="tab" aria-controls="search-consultants-tab"
                                   aria-selected="true">Search Consultants</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php if(isset($search_type) && $search_type == "job"){ echo "active"; } ?>" id="search-jobs" data-toggle="tab" href="#search-jobs-tab"
                                   role="tab" aria-controls="search-jobs-tab" aria-selected="false">Search Jobs</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="search-job-consultants-content">
                            <div class="tab-pane fade <?php if(isset($search_type)) { if($search_type == "consultant") { echo "show active"; } }else{ echo "show active"; } ?>" id="search-consultants-tab" role="tabpanel"
                                 aria-labelledby="search-consultants">

                                <div class="row mb-2">
                                    <div class="col-sm-6 col-md-3">                                        
                                        {{Form::select('category',$categories, (isset($category) && $category != null) ? $category : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-search"></i>
                                                </div>
                                            </div>
                                            {{Form::text('searchterm',isset($searchterm) ? $searchterm : '',['class' => 'form-control','placeholder' => 'Search by Job Title, Skills, or ID'])}}
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <button type="submit" class="btn btn-danger btn-block" name="search" value="search">Search</button>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <button type="button" class="btn btn-outline-light btn-block view-all" name="search" value="viewall">View all</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 order-md-4">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('baserate',[null => 'Base Rate'] + $baserateArray,isset($baserate) ? $baserate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('experience',[null => 'Experience'] + $experienceList,isset($experience) ? $experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('availability_date',[null => 'Availability Date'] + $avalabilityDateArray,isset($avalabilityDate) ? $avalabilityDate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('work_preference',[null => 'Work Preferences'] + $workPrefrencesArray,isset($workpreference) ? $workpreference : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                {{Form::select('state',[null => 'State'] + $states,isset($state) ? $state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade <?php if(isset($search_type) && $search_type == "job"){ echo "show active"; } ?>" id="search-jobs-tab" role="tabpanel"
                                 aria-labelledby="search-jobs">
                                <div class="row mb-2">
                                    <div class="col-sm-6 col-md-3 form-group">
                                        {{Form::select('jobcategory',$categories, (isset($jobcategory) && $jobcategory != null) ? $jobcategory : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                    </div>
                                    <div class="col-sm-6 col-md-5 col-lg-6 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-search"></i>
                                                </div>
                                            </div>
                                            {{Form::text('jobsearchterm','',['class' => 'form-control','placeholder' => 'Search job'])}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3 p-md-l-5">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <button type="submit" class="btn btn-danger btn-block">Search</button>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <a class="btn btn-outline-light btn-block view-all" href="javascript:void(0)">View all</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('job_type',[null => 'Job Type'] + $jobTypeList, (isset($jobtype) && $jobtype != null) ? $jobtype : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('job_baserate',[null => 'Base Rate'] + $baserateArray,(isset($job_baserate) && $job_baserate != null) ? $job_baserate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('work_experience',[null => 'Experience'] + $experienceList,(isset($work_experience) && $work_experience != null) ? $work_experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('job_start_date',[null => 'Start Date'] + $startDateList,(isset($job_start_date) && $job_start_date != null) ? $job_start_date : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                {{Form::select('job_duration',[null => 'Duration'] + $durationList,(isset($job_duration) && $job_duration != null) ? $job_duration : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                {{Form::select('job_industry',[null => 'Industry'] + $industryList,(isset($job_industry) && $job_industry != null) ? $job_industry : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                {{Form::select('job_state',[null => 'State'] + $states,(isset($job_state) && $job_state != null) ? $job_state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-2">
                    <a class="btn btn-warning  btnQuestions" href="#">Have any questions?</a>
                </div>
            </div>
        </div>
    </div>
    <!--Box Filer For Consultant--> 
    <div class="boxFilters <?php if(isset($search_type) && $search_type == "consultant" && Route::currentRouteName() == "general-search") { echo "show"; }else { echo "hide"; } ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="d-inline">You searched for:
                        <span class="text-red">
                            {{(isset($search_type)) ? $search_type : ""}}
                        </span>
                    </p>
                    <ul class="listCheckbox checkboxinline d-inline">
                        <li>
                            @if(isset($job_title_matching) && $job_title_matching == "Yes")
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only" checked=""> Keywords-matching Job Titles Only
                            @else
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only"> Keywords-matching Job Titles Only
                            @endif
                        </li>
                    </ul>
                </div>
                <!-- <div class="col-md-8">
                    <ul class="listCheckbox checkboxinline">
                        <li>
                            @if(isset($job_title_matching) && $job_title_matching == "Yes")
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only" checked=""> Keywords-matching Job Titles Only
                            @else
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only"> Keywords-matching Job Titles Only
                            @endif
                        </li>
                    </ul>
                </div> -->
            </div>
            <div class="row rs-5">
                <div class="col-md-6">
                    <ul class="listFilters">
                        <li>
                            <div class="dropdownTransparent">
                                {{Form::select('category2',$categories, (isset($category2) && $category2 != null) ? $category2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                            </div>
                        </li>
                        <li>
                            <div class="dropdownTransparent">
                                {{Form::select('baserate2',[null => 'Base Rate'] + $baserateArray,isset($baserate2) ? $baserate2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                            </div>
                        </li>
                        <li>
                            <div class="dropdownTransparent">
                                {{Form::select('experience2',[null => 'Experience'] + $experienceList,isset($experience2) ? $experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                            </div>
                        </li>
                        <li>
                            <div class="dropdownTransparent">
                                {{Form::select('availability_date2',[null => 'Availability Date'] + $avalabilityDateArray,isset($availability_date2) ? $availability_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="listCheckbox checkboxinline">
                        <li>
                            @if(isset($available) && $available == "Yes")
                            <input type="checkbox" class="subfilter" name="available" title="Available" checked=""> Available
                            @else
                            <input type="checkbox" class="subfilter" name="available" title="Available"> Available
                            @endif
                        </li>
                        <li>
                            @if(isset($willing_to_travel) && $willing_to_travel == "Yes")
                            <input type="checkbox" class="subfilter" name="willing_to_travel" title="Willing to travel" checked=""> Willing to travel
                            @else
                            <input type="checkbox" class="subfilter" name="willing_to_travel" title="Willing to travel"> Willing to travel
                            @endif
                        </li>
                        <li>
                            @if(isset($full_time) && $full_time == "Yes")
                            <input type="checkbox" class="subfilter" name="full_time" title="Full time" checked=""> Full time
                            @else
                            <input type="checkbox" class="subfilter" name="full_time" title="Full time"> Full time
                            @endif
                        </li>
                        <li>
                            @if(isset($part_time) && $part_time == "Yes")
                            <input type="checkbox" class="subfilter" name="part_time" title="Part time" checked=""> Part time
                            @else
                            <input type="checkbox" class="subfilter" name="part_time" title="Part time"> Part time
                            @endif
                        </li>
                        <li>
                            @if(isset($project) && $project == "Yes")
                            <input type="checkbox" class="subfilter" name="project" title="Project" checked=""> Project
                            @else
                            <input type="checkbox" class="subfilter" name="project" title="Project"> Project
                            @endif
                        </li>
                        <li>
                            @if(isset($support) && $support == "Yes")
                            <input type="checkbox" class="subfilter" name="support" title="Support" checked=""> Support
                            @else
                            <input type="checkbox" class="subfilter" name="support" title="Support"> Support
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Box Filter For Job-->
    <div class="boxFilters <?php if(isset($search_type) && $search_type == "job" && Route::currentRouteName() == "general-search") { echo "show"; }else { echo "hide"; } ?>">
        <div class="container ">
            <div class="row">
                <div class="col-md-6">
                    <p>You searched for:
                        <span class="text-red">
                            @if(isset($jobsearchterm) && $jobsearchterm != "")
                            {{$jobsearchterm}}
                            @elseif(isset($jobcategory) && $jobcategory != "")
                            {{$jobcategory. " Category"}}
                            @else
                            {{"All Jobs"}}
                            @endif
                        </span>
                    </p>
                </div>
                <div class="col-md-6 text-right">
                    <p>
                        <a href="javascript:void(0)" class="text-red" id="clear-filter">Clear all filters</a>
                </div>
            </div>
            <ul class="listFilters">
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_baserate2',[null => 'Base Rate'] + $baserateArray,isset($job_baserate2) ? $job_baserate2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_mode2',[null => 'Job Mode'] + $jobModeList,isset($job_mode2) ? $job_mode2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('work_experience2',[null => 'Experience'] + $experienceList,isset($work_experience2) ? $work_experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_start_date2',[null => 'Start Date'] + $startDateList,isset($job_start_date2) ? $job_start_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_duration2',[null => 'Duration'] + $durationList,isset($job_duration2) ? $job_duration2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_industry2',[null => 'Industry'] + $industryList,isset($job_industry2) ? $job_industry2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_state2',[null => 'State'] + $states,isset($job_state2) ? $job_state2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
            </ul>
        </div>
    </div>
    {{Form::close()}}
</header>