@include('Layouts.General.jsroutes')
<!--End Js Routes-->
<!-- Mainly scripts -->
<script>
    var getSearchConsultant = "{!! route('getSearchConsultant') !!}";
</script>
<script type="text/javascript" src="{{admin_asset('js/jquery-3.1.1.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/jquery-ui.js')}}"></script>
<!-- <script type="text/javascript" src="{{admin_asset('js/TweenMax.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/ScrollToPlugin.min.js')}}"></script> -->
<script type="text/javascript" src="{{admin_asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/summernote_editor.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/bootbox.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/main.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/validator.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/calculator1.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/calculator2.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/calculator3.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/custom.js')}}"></script>
<script type="text/javascript">
$("#testimonial-carousel").owlCarousel({
    smartSpeed: 2000,
    autoplay: true,
    autoplayTimeout: 15000,
    rtl: false,
    loop: true,
    mouseDrag: false,
    nav: true,
    navText: ["<img src='{{admin_asset('images/left-arrow.png')}}'>",
        "<img src='{{admin_asset('images/right-arrow.png')}}'>"
    ],
    dots: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    items: 1
});
$('#client-logo-carousel').owlCarousel({
    loop: true,
    margin: 10,
    navText: ['<img src="{{admin_asset("images/left-arrow.png")}}" alt="arrow">',
        '<img src="{{admin_asset("images/right-arrow.png")}}" alt="arrow">'
    ],
    responsiveClass: true,
    autoplay: true,
    smartSpeed: 1000,
    autoplayTimeout: 3000,
    transitionStyle: "fade",
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        568: {
            items: 3,
            nav: false
        },
        768: {
            items: 4,
            nav: false
        },
        1000: {
            items: 6,
            nav: false,
            loop: false,
            margin: 20
        }
    }
});
$(".login-link").click(function () {
    $("#login-button").trigger("click");
});
</script>
<script type="text/javascript">
    var $zoho = $zoho || {};
    $zoho.salesiq = $zoho.salesiq ||
            {widgetcode: "d8b8c99bcae961f661ba84373821fb33170f5ed9be3bdf8985cce6480e1d54f7", values: {}, ready: function () {}};
    var d = document;
    s = d.createElement("script");
    s.type = "text/javascript";
    s.id = "zsiqscript";
    s.defer = true;
    s.src = "https://salesiq.zoho.com/widget";
    t = d.getElementsByTagName("script")[0];
    t.parentNode.insertBefore(s, t);
    d.write("<div id='zsiqwidget'></div>");
    
    $(document).ready(function (){
        /* Open Generate Password Modal*/
        var route = '<?php echo Route::currentRouteName(); ?>';
        if(route == "generate-password"){
            $("#generate-password-modal").modal('show');
        }
    });
</script>
<!-- Mainly scripts -->