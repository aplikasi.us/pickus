<header class="headerNew ">
    <div class="header-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg " id="mainNav">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        @if(Session::get('territory') != null)
                        <li class="nav-item active {{ (Request::is('client')) ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('client-home')}}" title="{{Session::get('territory')}}">{{Session::get('territory')}}</a>
                        </li>
                        @endif
                        <li class="nav-item {{ (Route::currentRouteName() == "atapVirtualOffice") ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('atapVirtualOffice')}}" title="The Atap">The Atap</a>
                        </li>
                        <li class="nav-item {{ (Route::currentRouteName() == "aboutus") ? 'active' : '' }}"><a class="nav-link" href="{{route('aboutus')}}">Know us More!</a></li>
                        <li class="nav-item {{ (Route::currentRouteName() == "howitworks") ? 'active' : '' }}"><a class="nav-link" href="{{route('howitworks')}}">How it Works</a></li>
                        <li class="nav-item {{ (Route::currentRouteName() == "frontfaq") ? 'active' : '' }}"><a class="nav-link" href="{{route('frontfaq')}}">FAQ</a></li>
                        <li class="nav-item"><a class="nav-link" href="http://206.189.86.153/blog/">Blog</a></li>
                        <!-- <li class="nav-item">
                            <a class="nav-link iconHome" href="{{route('client-home')}}" title="My Home">My Home</a>
                        </li> -->
                        <li class="nav-item dropdown">
                            <a class="nav-link p-0  no-hover" href="#" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <span class="iconNotification read-notification">
                                    @if(count($unread))
                                    <small>{{count($unread)}}</small>
                                    @endif
                                </span>
                            </a>
                            <div class="dropdown-menu dropdownNotification  dropdown-menu-right dropdown">
                                @if(count($unread))
                                <div class="headerDropdown">
                                    Notifications
                                </div>
                                <div class="dropdownContent">
                                    <ul class="listContractHistory">
                                        @foreach($unread as $notificationKey => $notoficationValue)
                                        <li>
                                            <div class="row rowHistory align-items-center">
                                                <div class="col-md-12">
                                                    <span class="textDate">{{date('d M. Y',strtotime($notoficationValue->created_at))}}</span>
                                                    @if($notoficationValue->redirect_url != null)
                                                    <a href="{{$notoficationValue->redirect_url}}"><p>{{$notoficationValue->message}}</p></a>
                                                    @else
                                                    <p>{{$notoficationValue->message}}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach

                                    </ul>
                                    <div class="boxbtns">
                                        <div class="row no-gutters">
                                            <div class="col-md-12">
                                                <a title="View all" href="{{route('client-notifications')}}" class="btn btn-secondary btn-block">View
                                                    all</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link iconLogin" href="{{route('logout')}}" title="Logout" >Logout</a>
                        </li> 
                    </ul>
                </div>
            </nav>
            <div class="cus-search-toggle cus-mobile-show">
                <i class="fas fa-search colorWhite"></i>
            </div>
            <div class="mobile-logo show cus-mobile-show">
                @if(Auth::check())
                    @if(Auth::user()->role_id == 1)
                        <a href="{{route('client-home')}}" title="aplikasi.us">
                    @else
                        <a href="{{route('consultant-home')}}" title="aplikasi.us">
                    @endif
                @else
                    <a href="{{url('/')}}" title="aplikasi.us">
                @endif
                <img src="{{ admin_asset('images/logo.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                </a>
            </div>
            <div class="iam-hover-effect cus-mobile-show">
                <a class="btn btn-darkgreen btn-big  btnQuestions sidebar-open  " href="javascript:;"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="mobile-show mobile-header-search">
        {{ Form::open(['route' => ['search'], 'method' => 'GET', 'id'=>'mobile-search-form','autocomplete' => 'off']) }}
        <input type="hidden" name="search_type" class="both-search_type" id="search_type" value="{{isset($search_type) ? $search_type : "consultant"}}"/>
        {{csrf_field()}}
        <div class="headerMain">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-2 text-right">
                        <h1 id="logo">
                            <a href="{{url('/')}}" title="aplikasi.us">
                                <img src="{{ admin_asset('images/logo.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                            </a>
                        </h1>
                    </div>
                    <div class="col-md-8">
                        <div class="boxTabsearch">
                            <ul class="nav nav-tabs" id="search-job-consultants-section" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php
                                    if (isset($search_type)) {
                                        if ($search_type == "consultant") {
                                            echo "active";
                                        }
                                    } else {
                                        echo "active";
                                    }
                                    ?>" id="search-consultants" data-toggle="tab"
                                    href="#search-consultants-tab" role="tab" aria-controls="search-consultants-tab"
                                    aria-selected="true">Search Consultants</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php
                                    if (isset($search_type) && $search_type == "job") {
                                        echo "active";
                                    }
                                    ?>" id="search-jobs" data-toggle="tab" href="#search-jobs-tab"
                                    role="tab" aria-controls="search-jobs-tab" aria-selected="false">Search Jobs</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="search-job-consultants-content">
                                <div class="tab-pane fade <?php
                                if (isset($search_type)) {
                                    if ($search_type == "consultant") {
                                        echo "show active";
                                    }
                                } else {
                                    echo "show active";
                                }
                                ?>" id="search-consultants-tab" role="tabpanel"
                                    aria-labelledby="search-consultants">

                                    <div class="row mb-2">
                                        <div class="col-sm-6 col-md-6 mt-2 mb-2">
                                            <div class="input-group">
                                                <!-- <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <i class="fas fa-search"></i>
                                                    </div>
                                                </div> -->
                                                {{Form::text('searchterm',isset($searchterm) ? $searchterm : '',['class' => 'form-control search-consulresult','placeholder' => 'Search by Job Title, Skills, or ID'])}}
                                                <div class="consutant-suggestions"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3 mt-2">
                                            <span class="form-control category-search1" id="category-search1">Category</span>
                                            <div class="category-options form-control toogle-div" style="display: none;">
                                                <ul>
                                                    @foreach($categories as $catkey => $catvalue)
                                                    <li>
                                                        @if(isset($category))
                                                        @if(in_array($catvalue,$category))
                                                        <input type="checkbox" name="category[]" value="{{$catvalue}}" checked="" style="margin-right: 5px;"/>{{$catvalue}}
                                                        @else
                                                        <input type="checkbox" name="category[]" value="{{$catvalue}}" style="margin-right: 5px;" />{{$catvalue}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="category[]" value="{{$catvalue}}" style="margin-right: 5px;" />{{$catvalue}}
                                                        @endif
                                                    </li>    
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3 mt-3">
                                            <div class="row">
                                                <div class="col-6">
                                                    <input type="text" name="baserate_from" value="{{(isset($baserate_from)) ? $baserate_from : ''}}" id="baserate_from" class="form-control  baserate_number" placeholder="Base Rate From"/>
                                                </div>
                                                <div class="col-6">
                                                    <input type="text" name="baserate_to" value="{{(isset($baserate_to)) ? $baserate_to : ''}}" id="baserate_to" class="form-control baserate_number" placeholder="Base Rate To"/>
                                                </div>
                                                <!-- <li>
                                                    <button type="button" class="btn btn-secondary clt-blue submit-search">Apply</button>
                                                </li> -->
                                            </div>
                                        </div>
                                        <!-- <div class="col-sm-6 col-md-3">
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <button type="button" class="btn btn-danger btn-block submit-search" name="search" value="search">Search</button>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <a href="{{route('search',['search_type' => 'consultant'])}}"  class="btn btn-outline-light btn-block view-all" name="search" value="viewall">View all</a>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col-12 order-md-4">
                                            <div class="row">
                                                <div class="col-6 col-md-auto form-group selecttransparent">
                                                    <ul class="mobileheader-checkbox">
                                                        @foreach($workPrefrencesArray as $preferencekey => $preferencevalue)
                                                            <li>
                                                                @if(isset($workpreference))
                                                                @if(in_array($preferencevalue,$workpreference))
                                                                <input type="checkbox" name="work_preference[]" value="{{$preferencevalue}}" checked="" />{{$preferencevalue}}
                                                                @else
                                                                <input type="checkbox" name="work_preference[]" value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="work_preference[]" value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                                @endif
                                                            </li>    
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="col-6 col-sm-6 col-md-auto form-group selecttransparent">
                                                    {{Form::select('experience',[null => 'Experience'] + $experienceList,isset($experience) ? $experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                    {{Form::select('availability_date',[null => 'Availability Date'] + $avalabilityDateArray,isset($avalabilityDate) ? $avalabilityDate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                    <span class="consultant-state-lable" id="consultant-state-lable">State</span>
                                                    <img src="{{admin_asset('images/iconSelect.png')}}">
                                                    <div class="consultant-state-options form-control toogle-div" style="display: none;">
                                                        <ul>
                                                            <li class="close-consutant-state">Close</li>
                                                            @foreach($states as $statekey => $statevalue)
                                                            <li>
                                                                @if(isset($state))
                                                                @if(in_array($statevalue,$state))
                                                                <input type="checkbox" name="state[]" value="{{$statevalue}}" checked="" />{{$statevalue}}
                                                                @else
                                                                <input type="checkbox" name="state[]" value="{{$statevalue}}" />{{$statevalue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="state[]" value="{{$statevalue}}" />{{$statevalue}}
                                                                @endif
                                                            </li>    
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col mt-5">
                                                    <div class="col-12 form-group">
                                                        <button type="button" class="btn btn-danger btn-block mobilesubmit-search border-rad-9 " name="search" value="search">Search</button>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <a href="{{route('general-search',['search_type' => 'consultant'])}}" class="btn btn-outline-light btn-block view-all border-rad-9 " name="search" value="viewall">View all</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade <?php
                                if (isset($search_type) && $search_type == "job") {
                                    echo "show active";
                                }
                                ?>" id="search-jobs-tab" role="tabpanel"
                                    aria-labelledby="search-jobs">
                                    <div class="row mb-2">
                                        <div class="col-sm-6 col-md-3 form-group">
                                            <span class="form-control" id="job-category-search1">Category</span>
                                            <div class="job-category-options form-control toogle-div" style="display: none;">
                                                <ul>
                                                    @foreach($categories as $catkey => $catvalue)
                                                    <li>
                                                        @if(isset($jobcategory))
                                                        @if(in_array($catvalue,$jobcategory))
                                                        <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}" checked="" style="margin-right: 5px;"/>{{$catvalue}}
                                                        @else
                                                        <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}" style="margin-right: 5px;"/>{{$catvalue}}
                                                        @endif
                                                        @else
                                                        <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}" style="margin-right: 5px;"/>{{$catvalue}}
                                                        @endif
                                                    </li>    
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-5 col-lg-6 form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <i class="fas fa-search"></i>
                                                    </div>
                                                </div>
                                                {{Form::text('jobsearchterm','',['class' => 'form-control search-jobresult','placeholder' => 'Search by Job Title Skills or Job ID'])}}
                                                <div class="job-suggestions"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-3 p-md-l-5">
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <button type="button" class="btn btn-danger btn-block submit-search">Search</button>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <a class="btn btn-outline-light btn-block view-all" href="{{route('search',['search_type' => 'job'])}}">View all</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <div class="row">
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                    <!--{{Form::select('job_mode1',[null => 'Job Mode'] + $jobModeList, (isset($job_mode1) && $job_mode1 != null) ? $job_mode1 : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                    <span class="job-mode1-lable" id="job-mode1-lable">Job Mode</span>
                                                    <img src="{{admin_asset('images/iconSelect.png')}}">
                                                    <div class="job-mode1-options form-control toogle-div" style="display: none;">
                                                        <ul>
                                                            @foreach($jobModeList as $jobmodekey => $jobmodeValue)
                                                            <li>
                                                                @if(isset($job_mode1))
                                                                @if(in_array($jobmodeValue,$job_mode1))
                                                                <input type="checkbox" name="job_mode1[]" value="{{$jobmodeValue}}" checked="" />{{$jobmodeValue}}
                                                                @else
                                                                <input type="checkbox" name="job_mode1[]" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="job_mode1[]" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                                @endif
                                                            </li>    
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                    <span class="job-type1-lable" id="job-type1-lable">Job Type</span>
                                                    <img src="{{admin_asset('images/iconSelect.png')}}">                                                
                                                    <div class="job-type1-options form-control toogle-div" style="display: none;">
                                                        <ul>

                                                            @foreach($jobTypeList as $jobtypekey => $jobtypeValue)
                                                            <li>
                                                                @if(isset($jobtype))
                                                                @if(in_array($jobtypeValue,$jobtype))
                                                                <input type="checkbox" name="job_type[]" value="{{$jobtypeValue}}" checked="" />{{$jobtypeValue}}
                                                                @else
                                                                <input type="checkbox" name="job_type[]" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="job_type[]" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                                @endif
                                                            </li>    
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                    <span class="job-baserate-1" id="job-baserate-1">Base Rate</span>
                                                    <img src="{{admin_asset('images/iconSelect.png')}}">
                                                    <div class="job-baserate-1-options form-control toogle-div" style="display: none;">
                                                        <ul>
                                                            <li>
                                                                <input type="text" name="job_baserate_from" value="{{(isset($job_baserate_from)) ? $job_baserate_from : ''}}" id="job_baserate_from" class="baserate_number" placeholder="Base Rate From"/>
                                                            </li>
                                                            <li>
                                                                <input type="text" name="job_baserate_to" value="{{(isset($job_baserate_to)) ? $job_baserate_to : ''}}" id="job_baserate_to" class="baserate_number" placeholder="Base Rate To"/>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                    {{Form::select('work_experience',[null => 'Experience'] + $experienceList,(isset($work_experience) && $work_experience != null) ? $work_experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                    {{Form::select('job_start_date',[null => 'Start Date'] + $startDateList,(isset($job_start_date) && $job_start_date != null) ? $job_start_date : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                    {{Form::select('job_duration',[null => 'Duration'] + $durationList,(isset($job_duration) && $job_duration != null) ? $job_duration : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                    {{Form::select('job_industry',[null => 'Industry'] + $industryList,(isset($job_industry) && $job_industry != null) ? $job_industry : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                                </div>
                                                <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                    <!--{{Form::select('job_state',[null => 'State'] + $states,(isset($job_state) && $job_state != null) ? $job_state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                    <span class="job-state-lable" id="job-state-lable">State</span>
                                                    <img src="{{admin_asset('images/iconSelect.png')}}">
                                                    <div class="job-state-options form-control toogle-div" style="display: none;">
                                                        <ul>
                                                            @foreach($states as $statekey1 => $statevalue1)
                                                            <li>
                                                                @if(isset($job_state))
                                                                @if(in_array($statevalue1,$job_state))
                                                                <input type="checkbox" name="job_state[]" value="{{$statevalue1}}" checked="" />{{$statevalue1}}
                                                                @else
                                                                <input type="checkbox" name="job_state[]" value="{{$statevalue1}}" />{{$statevalue1}}
                                                                @endif
                                                                @else
                                                                <input type="checkbox" name="job_state[]" value="{{$statevalue1}}" />{{$statevalue1}}
                                                                @endif
                                                            </li>    
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-2">
                        <!-- <a class="btn btn-warning  btnQuestions" href="{{route('atapVirtualOffice')}}">Have any questions?</a> -->
                    </div>
                </div>
            </div>
        </div>
        <!--Box Filer For Consultant--> 
        <div class="boxFilters <?php
        if (isset($search_type) && $search_type == "consultant" && Route::currentRouteName() == "search") {
            echo "show";
        } else {
            echo "hide";
        }
        ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="d-inline">You searched for:
                            <span class="text-red">
                                {{(isset($searchterm) && $searchterm != null) ? $searchterm : ""}}
                            </span>
                        </p>
                        <ul class="listCheckbox checkboxinline d-inline">
                            <li>
                                @if(isset($job_title_matching) && $job_title_matching == "Yes")
                                <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only" checked=""> Keywords-matching Job Titles Only
                                @else
                                <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only"> Keywords-matching Job Titles Only
                                @endif
                            </li>
                        </ul>
                    </div>
                    <!-- <div class="col-md-8">
                        <ul class="listCheckbox checkboxinline">
                            <li>
                                @if(isset($job_title_matching) && $job_title_matching == "Yes")
                                <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only" checked=""> Keywords-matching Job Titles Only
                                @else
                                <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only"> Keywords-matching Job Titles Only
                                @endif
                            </li>
                        </ul>
                    </div> -->
                </div>
                <div class="row rs-5">
                    <div class="col-md-6">
                        <ul class="listFilters">
                            <li>
                                <div class="dropdownTransparent">
                                    <span class="" id="category-search2">Category</span>
                                    <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                                    <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                                    <div class="form-control category-options2 toogle-div" style="display: none;">
                                        <ul>
                                            @foreach($categories as $catkey => $catvalue)
                                            <li class="category-options2-li">
                                                @if(isset($category2))
                                                @if(in_array($catvalue,$category2))
                                                <input type="checkbox" name="category2[]" class="subfilter" value="{{$catvalue}}" checked="" />{{$catvalue}}
                                                @else
                                                <input type="checkbox" name="category2[]" class="subfilter" value="{{$catvalue}}" />{{$catvalue}}
                                                @endif
                                                @else
                                                <input type="checkbox" name="category2[]" class="subfilter" value="{{$catvalue}}" />{{$catvalue}}
                                                @endif
                                            </li>    
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdownTransparent">
                                    <span class="" id="baserate-2-lable">Base Rate</span>
                                    <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                                    <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                                    <div class="baserate-2-options form-control toogle-div" style="display: none;">
                                        <ul>
                                            <li class="baserate2-options2-li">
                                                <input type="text" name="baserate2_from" value="{{(isset($baserate2_from)) ? $baserate2_from : ''}}" id="baserate2_from" class="baserate_number" placeholder="Base Rate From"/>
                                            </li>
                                            <li class="baserate2-options2-li">
                                                <input type="text" name="baserate2_to" value="{{(isset($baserate2_to)) ? $baserate2_to : ''}}" id="baserate2_to" class="baserate_number" placeholder="Base Rate To"/>
                                            </li>
                                            <li class="baserate2-options2-li">
                                                <button type="button" class="btn btn-secondary clt-blue submit-search">Apply</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdownTransparent">
                                    {{Form::select('experience2',[null => 'Experience'] + $experienceList,isset($experience2) ? $experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                                </div>
                            </li>
                            <li>
                                <div class="dropdownTransparent">
                                    {{Form::select('availability_date2',[null => 'Availability Date'] + $avalabilityDateArray,isset($availability_date2) ? $availability_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="listCheckbox checkboxinline">
                            <li>
                                @if(isset($available) && $available == "Yes")
                                <input type="checkbox" class="subfilter" name="available" title="Available" checked=""> Available
                                @else
                                <input type="checkbox" class="subfilter" name="available" title="Available"> Available
                                @endif
                            </li>
                            <li>
                                @if(isset($willing_to_travel) && $willing_to_travel == "Yes")
                                <input type="checkbox" class="subfilter" name="willing_to_travel" title="Willing to travel" checked=""> Willing to travel
                                @else
                                <input type="checkbox" class="subfilter" name="willing_to_travel" title="Willing to travel"> Willing to travel
                                @endif
                            </li>
                            <li>
                                @if(isset($full_time) && $full_time == "Yes")
                                <input type="checkbox" class="subfilter" name="full_time" title="Full time" checked=""> Full time
                                @else
                                <input type="checkbox" class="subfilter" name="full_time" title="Full time"> Full time
                                @endif
                            </li>
                            <li>
                                @if(isset($part_time) && $part_time == "Yes")
                                <input type="checkbox" class="subfilter" name="part_time" title="Part time" checked=""> Part time
                                @else
                                <input type="checkbox" class="subfilter" name="part_time" title="Part time"> Part time
                                @endif
                            </li>
                            <li>
                                @if(isset($project) && $project == "Yes")
                                <input type="checkbox" class="subfilter" name="project" title="Project" checked=""> Project
                                @else
                                <input type="checkbox" class="subfilter" name="project" title="Project"> Project
                                @endif
                            </li>
                            <li>
                                @if(isset($support) && $support == "Yes")
                                <input type="checkbox" class="subfilter" name="support" title="Support" checked=""> Support
                                @else
                                <input type="checkbox" class="subfilter" name="support" title="Support"> Support
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--Box Filter For Job-->
        <div class="boxFilters <?php
        if (isset($search_type) && $search_type == "job" && Route::currentRouteName() == "search") {
            echo "show";
        } else {
            echo "hide";
        }
        ?>">
            <div class="container ">
                <div class="row">
                    <div class="col-md-6">
                        <p>You searched for:
                            <span class="text-red">
                                {{(isset($jobsearchterm) && $jobsearchterm != null) ? $jobsearchterm : ""}}
                            </span>
                        </p>
                    </div>
                    <div class="col-md-6 text-right">
                        <p>
                            <a href="javascript:void(0)" class="text-red" id="clear-filter">Clear all filters</a>
                    </div>
                </div>
                <ul class="listFilters">                
                    <li>
                        <div class="dropdownTransparent">
                            <!--{{Form::select('job_mode2',[null => 'Job Mode'] + $jobModeList,isset($job_mode2) ? $job_mode2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}-->
                            <span class="job-mode2-lable" id="job-mode2-lable">Job Mode</span>
                            <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                            <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                            <div class="job-mode2-options form-control toogle-div" style="display: none;">
                                <ul>
                                    @foreach($jobModeList as $jobmodekey => $jobmodeValue)
                                    <li>
                                        @if(isset($job_mode2))
                                        @if(in_array($jobmodeValue,$job_mode2))
                                        <input type="checkbox" name="job_mode2[]" class="subfilter" value="{{$jobmodeValue}}" checked="" />{{$jobmodeValue}}
                                        @else
                                        <input type="checkbox" name="job_mode2[]" class="subfilter" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                        @endif
                                        @else
                                        <input type="checkbox" name="job_mode2[]" class="subfilter" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                        @endif
                                    </li>    
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            <span class="job-type2-lable" id="job-type2-lable">Job Type</span>
                            <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                            <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                            <div class="job-type2-options form-control toogle-div" style="display: none;">
                                <ul>
                                    @foreach($jobTypeList as $jobtypekey => $jobtypeValue)
                                    <li>
                                        @if(isset($job_type2) && $job_type2 != "")
                                        @if(in_array($jobtypeValue,$job_type2))
                                        <input type="checkbox" name="job_type2[]" class="subfilter" value="{{$jobtypeValue}}" checked="" />{{$jobtypeValue}}
                                        @else
                                        <input type="checkbox" name="job_type2[]" class="subfilter" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                        @endif
                                        @else
                                        <input type="checkbox" name="job_type2[]" class="subfilter" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                        @endif
                                    </li>    
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            <span class="" id="job-baserate-2-lable">Base Rate</span>
                            <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                            <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                            <div class="job-baserate-2-options form-control toogle-div" style="display: none;">
                                <ul>
                                    <li class="job-baserate2-options2-li">
                                        <input type="text" name="job_baserate2_from" value="{{(isset($job_baserate2_from)) ? $job_baserate2_from : ''}}" id="job_baserate2_from" class="baserate_number" placeholder="Base Rate From"/>
                                    </li>
                                    <li class="job-baserate2-options2-li">
                                        <input type="text" name="job_baserate2_to" value="{{(isset($job_baserate2_to)) ? $job_baserate2_to : ''}}" id="job_baserate2_to" class="baserate_number" placeholder="Base Rate To"/>
                                    </li>
                                    <li class="job-baserate2-options2-li">
                                        <button type="button" class="btn btn-secondary clt-blue submit-search">Apply</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            {{Form::select('work_experience2',[null => 'Experience'] + $experienceList,isset($work_experience2) ? $work_experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            {{Form::select('job_start_date2',[null => 'Start Date'] + $startDateList,isset($job_start_date2) ? $job_start_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            {{Form::select('job_duration2',[null => 'Duration'] + $durationList,isset($job_duration2) ? $job_duration2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            {{Form::select('job_industry2',[null => 'Industry'] + $industryList,isset($job_industry2) ? $job_industry2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                        </div>
                    </li>
                    <li>
                        <div class="dropdownTransparent">
                            <!--{{Form::select('job_state2',[null => 'State'] + $states,isset($job_state2) ? $job_state2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}-->
                            <span class="job-state2-lable" id="job-state2-lable">State</span>
                            <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                            <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                            <div class="job-state2-options form-control toogle-div" style="display: none;">
                                <ul>
                                    @foreach($states as $statekey2 => $statevalue2)
                                    <li>
                                        @if(isset($job_state2))
                                        @if(in_array($statevalue2,$job_state2))
                                        <input type="checkbox" name="job_state2[]" class="subfilter" value="{{$statevalue2}}" checked="" />{{$statevalue2}}
                                        @else
                                        <input type="checkbox" name="job_state2[]" class="subfilter" value="{{$statevalue2}}" />{{$statevalue2}}
                                        @endif
                                        @else
                                        <input type="checkbox" name="job_state2[]" class="subfilter" value="{{$statevalue2}}" />{{$statevalue2}}
                                        @endif
                                    </li>    
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        {{Form::close()}}
    </div>
    {{ Form::open(['route' => ['search'], 'method' => 'GET', 'id'=>'search-form','autocomplete' => 'off']) }}
    <input type="hidden" name="search_type" id="search_type" value="{{isset($search_type) ? $search_type : "consultant"}}"/>
    {{csrf_field()}}
    <div class="headerMain cus-mobile-hide">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-2 text-right">
                    <h1 id="logo">
                        <a href="{{url('/')}}" title="aplikasi.us">
                            <img src="{{ admin_asset('images/logo.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                        </a>
                    </h1>
                </div>
                <div class="col-md-8">
                    <div class="boxTabsearch">
                        <ul class="nav nav-tabs" id="search-job-consultants-section" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link <?php
                                if (isset($search_type)) {
                                    if ($search_type == "consultant") {
                                        echo "active";
                                    }
                                } else {
                                    echo "active";
                                }
                                ?>" id="search-consultants" data-toggle="tab"
                                   href="#search-consultants-tab" role="tab" aria-controls="search-consultants-tab"
                                   aria-selected="true">Search Consultants</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php
                                if (isset($search_type) && $search_type == "job") {
                                    echo "active";
                                }
                                ?>" id="search-jobs" data-toggle="tab" href="#search-jobs-tab"
                                   role="tab" aria-controls="search-jobs-tab" aria-selected="false">Search Jobs</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="search-job-consultants-content">
                            <div class="tab-pane fade <?php
                            if (isset($search_type)) {
                                if ($search_type == "consultant") {
                                    echo "show active";
                                }
                            } else {
                                echo "show active";
                            }
                            ?>" id="search-consultants-tab" role="tabpanel"
                                 aria-labelledby="search-consultants">

                                <div class="row mb-2">
                                    <div class="col-sm-6 col-md-3">                                        
                                        <span class="form-control" id="category-search1">Category</span>
                                        <div class="category-options form-control toogle-div" style="display: none;">
                                            <ul>
                                                @foreach($categories as $catkey => $catvalue)
                                                <li>
                                                    @if(isset($category))
                                                    @if(in_array($catvalue,$category))
                                                    <input type="checkbox" name="category[]" value="{{$catvalue}}" checked="" style="margin-right: 5px;"/>{{$catvalue}}
                                                    @else
                                                    <input type="checkbox" name="category[]" value="{{$catvalue}}" style="margin-right: 5px;" />{{$catvalue}}
                                                    @endif
                                                    @else
                                                    <input type="checkbox" name="category[]" value="{{$catvalue}}" style="margin-right: 5px;" />{{$catvalue}}
                                                    @endif
                                                </li>    
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-search"></i>
                                                </div>
                                            </div>
                                            {{Form::text('searchterm',isset($searchterm) ? $searchterm : '',['class' => 'form-control search-consulresult','placeholder' => 'Search by Job Title, Skills, or ID'])}}
                                            <div class="consutant-suggestions"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <button type="button" class="btn btn-danger btn-block submit-search" name="search" value="search">Search</button>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <a href="{{route('search',['search_type' => 'consultant'])}}"  class="btn btn-outline-light btn-block view-all" name="search" value="viewall">View all</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 order-md-4">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <span class="baserate-1" id="baserate-1">Base Rate</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="baserate-1-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        <li>
                                                            <input type="text" name="baserate_from" value="{{(isset($baserate_from)) ? $baserate_from : ''}}" id="baserate_from" class="baserate_number" placeholder="Base Rate From"/>
                                                        </li>
                                                        <li>
                                                            <input type="text" name="baserate_to" value="{{(isset($baserate_to)) ? $baserate_to : ''}}" id="baserate_to" class="baserate_number" placeholder="Base Rate To"/>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('experience',[null => 'Experience'] + $experienceList,isset($experience) ? $experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('availability_date',[null => 'Availability Date'] + $avalabilityDateArray,isset($avalabilityDate) ? $avalabilityDate : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <span class="work-preference-lable" id="work-preference-lable">Work Preferences</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="work-preference-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        @foreach($workPrefrencesArray as $preferencekey => $preferencevalue)
                                                        <li>
                                                            @if(isset($workpreference))
                                                            @if(in_array($preferencevalue,$workpreference))
                                                            <input type="checkbox" name="work_preference[]" value="{{$preferencevalue}}" checked="" />{{$preferencevalue}}
                                                            @else
                                                            <input type="checkbox" name="work_preference[]" value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="work_preference[]" value="{{$preferencevalue}}" />{{$preferencevalue}}
                                                            @endif
                                                        </li>    
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                <!--{{Form::select('state',[null => 'State'] + $states,isset($state) ? $state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                <span class="consultant-state-lable" id="consultant-state-lable">State</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="consultant-state-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        @foreach($states as $statekey => $statevalue)
                                                        <li>
                                                            @if(isset($state))
                                                            @if(in_array($statevalue,$state))
                                                            <input type="checkbox" name="state[]" value="{{$statevalue}}" checked="" />{{$statevalue}}
                                                            @else
                                                            <input type="checkbox" name="state[]" value="{{$statevalue}}" />{{$statevalue}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="state[]" value="{{$statevalue}}" />{{$statevalue}}
                                                            @endif
                                                        </li>    
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade <?php
                            if (isset($search_type) && $search_type == "job") {
                                echo "show active";
                            }
                            ?>" id="search-jobs-tab" role="tabpanel"
                                 aria-labelledby="search-jobs">
                                <div class="row mb-2">
                                    <div class="col-sm-6 col-md-3 form-group">
                                        <span class="form-control" id="job-category-search1">Category</span>
                                        <div class="job-category-options form-control toogle-div" style="display: none;">
                                            <ul>
                                                @foreach($categories as $catkey => $catvalue)
                                                <li>
                                                    @if(isset($jobcategory))
                                                    @if(in_array($catvalue,$jobcategory))
                                                    <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}" checked="" style="margin-right: 5px;"/>{{$catvalue}}
                                                    @else
                                                    <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}" style="margin-right: 5px;"/>{{$catvalue}}
                                                    @endif
                                                    @else
                                                    <input type="checkbox" name="jobcategory[]" value="{{$catvalue}}" style="margin-right: 5px;"/>{{$catvalue}}
                                                    @endif
                                                </li>    
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-5 col-lg-6 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-search"></i>
                                                </div>
                                            </div>
                                            {{Form::text('jobsearchterm','',['class' => 'form-control search-jobresult','placeholder' => 'Search by Job Title Skills or Job ID'])}}
                                            <div class="job-suggestions"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3 p-md-l-5">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <button type="button" class="btn btn-danger btn-block submit-search">Search</button>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <a class="btn btn-outline-light btn-block view-all" href="{{route('search',['search_type' => 'job'])}}">View all</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <!--{{Form::select('job_mode1',[null => 'Job Mode'] + $jobModeList, (isset($job_mode1) && $job_mode1 != null) ? $job_mode1 : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                <span class="job-mode1-lable" id="job-mode1-lable">Job Mode</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="job-mode1-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        @foreach($jobModeList as $jobmodekey => $jobmodeValue)
                                                        <li>
                                                            @if(isset($job_mode1))
                                                            @if(in_array($jobmodeValue,$job_mode1))
                                                            <input type="checkbox" name="job_mode1[]" value="{{$jobmodeValue}}" checked="" />{{$jobmodeValue}}
                                                            @else
                                                            <input type="checkbox" name="job_mode1[]" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="job_mode1[]" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                                            @endif
                                                        </li>    
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <span class="job-type1-lable" id="job-type1-lable">Job Type</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">                                                
                                                <div class="job-type1-options form-control toogle-div" style="display: none;">
                                                    <ul>

                                                        @foreach($jobTypeList as $jobtypekey => $jobtypeValue)
                                                        <li>
                                                            @if(isset($jobtype))
                                                            @if(in_array($jobtypeValue,$jobtype))
                                                            <input type="checkbox" name="job_type[]" value="{{$jobtypeValue}}" checked="" />{{$jobtypeValue}}
                                                            @else
                                                            <input type="checkbox" name="job_type[]" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="job_type[]" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                                            @endif
                                                        </li>    
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                <span class="job-baserate-1" id="job-baserate-1">Base Rate</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="job-baserate-1-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        <li>
                                                            <input type="text" name="job_baserate_from" value="{{(isset($job_baserate_from)) ? $job_baserate_from : ''}}" id="job_baserate_from" class="baserate_number" placeholder="Base Rate From"/>
                                                        </li>
                                                        <li>
                                                            <input type="text" name="job_baserate_to" value="{{(isset($job_baserate_to)) ? $job_baserate_to : ''}}" id="job_baserate_to" class="baserate_number" placeholder="Base Rate To"/>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('work_experience',[null => 'Experience'] + $experienceList,(isset($work_experience) && $work_experience != null) ? $work_experience : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent">
                                                {{Form::select('job_start_date',[null => 'Start Date'] + $startDateList,(isset($job_start_date) && $job_start_date != null) ? $job_start_date : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                {{Form::select('job_duration',[null => 'Duration'] + $durationList,(isset($job_duration) && $job_duration != null) ? $job_duration : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                {{Form::select('job_industry',[null => 'Industry'] + $industryList,(isset($job_industry) && $job_industry != null) ? $job_industry : '',['class' => 'form-control select2','style' => 'width:100%;'])}}
                                            </div>
                                            <div class="col-4 col-sm-3 col-md-auto form-group selecttransparent" style="min-width: 100px;">
                                                <!--{{Form::select('job_state',[null => 'State'] + $states,(isset($job_state) && $job_state != null) ? $job_state : '',['class' => 'form-control select2','style' => 'width:100%;'])}}-->
                                                <span class="job-state-lable" id="job-state-lable">State</span>
                                                <img src="{{admin_asset('images/iconSelect.png')}}">
                                                <div class="job-state-options form-control toogle-div" style="display: none;">
                                                    <ul>
                                                        @foreach($states as $statekey1 => $statevalue1)
                                                        <li>
                                                            @if(isset($job_state))
                                                            @if(in_array($statevalue1,$job_state))
                                                            <input type="checkbox" name="job_state[]" value="{{$statevalue1}}" checked="" />{{$statevalue1}}
                                                            @else
                                                            <input type="checkbox" name="job_state[]" value="{{$statevalue1}}" />{{$statevalue1}}
                                                            @endif
                                                            @else
                                                            <input type="checkbox" name="job_state[]" value="{{$statevalue1}}" />{{$statevalue1}}
                                                            @endif
                                                        </li>    
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-2">
                    <!-- <a class="btn btn-warning  btnQuestions" href="{{route('atapVirtualOffice')}}">Have any questions?</a> -->
                </div>
            </div>
        </div>
    </div>
    <!--Box Filer For Consultant--> 
    <div class="boxFilters <?php
    if (isset($search_type) && $search_type == "consultant" && Route::currentRouteName() == "search") {
        echo "show";
    } else {
        echo "hide";
    }
    ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="d-inline">You searched for:
                        <span class="text-red">
                            {{(isset($searchterm) && $searchterm != null) ? $searchterm : ""}}
                        </span>
                    </p>
                    <ul class="listCheckbox checkboxinline d-inline">
                        <li>
                            @if(isset($job_title_matching) && $job_title_matching == "Yes")
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only" checked=""> Keywords-matching Job Titles Only
                            @else
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only"> Keywords-matching Job Titles Only
                            @endif
                        </li>
                    </ul>
                </div>
                <!-- <div class="col-md-8">
                    <ul class="listCheckbox checkboxinline">
                        <li>
                            @if(isset($job_title_matching) && $job_title_matching == "Yes")
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only" checked=""> Keywords-matching Job Titles Only
                            @else
                            <input type="checkbox" class="subfilter" name="job_title_matching" title="Keywords-matching Job Titles Only"> Keywords-matching Job Titles Only
                            @endif
                        </li>
                    </ul>
                </div> -->
            </div>
            <div class="row rs-5">
                <div class="col-md-6">
                    <ul class="listFilters">
                        <li>
                            <div class="dropdownTransparent">
                                <span class="" id="category-search2">Category</span>
                                <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                                <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                                <div class="form-control category-options2 toogle-div" style="display: none;">
                                    <ul>
                                        @foreach($categories as $catkey => $catvalue)
                                        <li class="category-options2-li">
                                            @if(isset($category2))
                                            @if(in_array($catvalue,$category2))
                                            <input type="checkbox" name="category2[]" class="subfilter" value="{{$catvalue}}" checked="" />{{$catvalue}}
                                            @else
                                            <input type="checkbox" name="category2[]" class="subfilter" value="{{$catvalue}}" />{{$catvalue}}
                                            @endif
                                            @else
                                            <input type="checkbox" name="category2[]" class="subfilter" value="{{$catvalue}}" />{{$catvalue}}
                                            @endif
                                        </li>    
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdownTransparent">
                                <span class="" id="baserate-2-lable">Base Rate</span>
                                <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                                <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                                <div class="baserate-2-options form-control toogle-div" style="display: none;">
                                    <ul>
                                        <li class="baserate2-options2-li">
                                            <input type="text" name="baserate2_from" value="{{(isset($baserate2_from)) ? $baserate2_from : ''}}" id="baserate2_from" class="baserate_number" placeholder="Base Rate From"/>
                                        </li>
                                        <li class="baserate2-options2-li">
                                            <input type="text" name="baserate2_to" value="{{(isset($baserate2_to)) ? $baserate2_to : ''}}" id="baserate2_to" class="baserate_number" placeholder="Base Rate To"/>
                                        </li>
                                        <li class="baserate2-options2-li">
                                            <button type="button" class="btn btn-secondary clt-blue submit-search">Apply</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdownTransparent">
                                {{Form::select('experience2',[null => 'Experience'] + $experienceList,isset($experience2) ? $experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                            </div>
                        </li>
                        <li>
                            <div class="dropdownTransparent">
                                {{Form::select('availability_date2',[null => 'Availability Date'] + $avalabilityDateArray,isset($availability_date2) ? $availability_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="listCheckbox checkboxinline">
                        <li>
                            @if(isset($available) && $available == "Yes")
                            <input type="checkbox" class="subfilter" name="available" title="Available" checked=""> Available
                            @else
                            <input type="checkbox" class="subfilter" name="available" title="Available"> Available
                            @endif
                        </li>
                        <li>
                            @if(isset($willing_to_travel) && $willing_to_travel == "Yes")
                            <input type="checkbox" class="subfilter" name="willing_to_travel" title="Willing to travel" checked=""> Willing to travel
                            @else
                            <input type="checkbox" class="subfilter" name="willing_to_travel" title="Willing to travel"> Willing to travel
                            @endif
                        </li>
                        <li>
                            @if(isset($full_time) && $full_time == "Yes")
                            <input type="checkbox" class="subfilter" name="full_time" title="Full time" checked=""> Full time
                            @else
                            <input type="checkbox" class="subfilter" name="full_time" title="Full time"> Full time
                            @endif
                        </li>
                        <li>
                            @if(isset($part_time) && $part_time == "Yes")
                            <input type="checkbox" class="subfilter" name="part_time" title="Part time" checked=""> Part time
                            @else
                            <input type="checkbox" class="subfilter" name="part_time" title="Part time"> Part time
                            @endif
                        </li>
                        <li>
                            @if(isset($project) && $project == "Yes")
                            <input type="checkbox" class="subfilter" name="project" title="Project" checked=""> Project
                            @else
                            <input type="checkbox" class="subfilter" name="project" title="Project"> Project
                            @endif
                        </li>
                        <li>
                            @if(isset($support) && $support == "Yes")
                            <input type="checkbox" class="subfilter" name="support" title="Support" checked=""> Support
                            @else
                            <input type="checkbox" class="subfilter" name="support" title="Support"> Support
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Box Filter For Job-->
    <div class="boxFilters <?php
    if (isset($search_type) && $search_type == "job" && Route::currentRouteName() == "search") {
        echo "show";
    } else {
        echo "hide";
    }
    ?>">
        <div class="container ">
            <div class="row">
                <div class="col-md-6">
                    <p>You searched for:
                        <span class="text-red">
                            {{(isset($jobsearchterm) && $jobsearchterm != null) ? $jobsearchterm : ""}}
                        </span>
                    </p>
                </div>
                <div class="col-md-6 text-right">
                    <p>
                        <a href="javascript:void(0)" class="text-red" id="clear-filter">Clear all filters</a>
                </div>
            </div>
            <ul class="listFilters">                
                <li>
                    <div class="dropdownTransparent">
                        <!--{{Form::select('job_mode2',[null => 'Job Mode'] + $jobModeList,isset($job_mode2) ? $job_mode2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}-->
                        <span class="job-mode2-lable" id="job-mode2-lable">Job Mode</span>
                        <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                        <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                        <div class="job-mode2-options form-control toogle-div" style="display: none;">
                            <ul>
                                @foreach($jobModeList as $jobmodekey => $jobmodeValue)
                                <li>
                                    @if(isset($job_mode2))
                                    @if(in_array($jobmodeValue,$job_mode2))
                                    <input type="checkbox" name="job_mode2[]" class="subfilter" value="{{$jobmodeValue}}" checked="" />{{$jobmodeValue}}
                                    @else
                                    <input type="checkbox" name="job_mode2[]" class="subfilter" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                    @endif
                                    @else
                                    <input type="checkbox" name="job_mode2[]" class="subfilter" value="{{$jobmodeValue}}" />{{$jobmodeValue}}
                                    @endif
                                </li>    
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        <span class="job-type2-lable" id="job-type2-lable">Job Type</span>
                        <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                        <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                        <div class="job-type2-options form-control toogle-div" style="display: none;">
                            <ul>
                                @foreach($jobTypeList as $jobtypekey => $jobtypeValue)
                                <li>
                                    @if(isset($job_type2) && $job_type2 != "")
                                    @if(in_array($jobtypeValue,$job_type2))
                                    <input type="checkbox" name="job_type2[]" class="subfilter" value="{{$jobtypeValue}}" checked="" />{{$jobtypeValue}}
                                    @else
                                    <input type="checkbox" name="job_type2[]" class="subfilter" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                    @endif
                                    @else
                                    <input type="checkbox" name="job_type2[]" class="subfilter" value="{{$jobtypeValue}}" />{{$jobtypeValue}}
                                    @endif
                                </li>    
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        <span class="" id="job-baserate-2-lable">Base Rate</span>
                        <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                        <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                        <div class="job-baserate-2-options form-control toogle-div" style="display: none;">
                            <ul>
                                <li class="job-baserate2-options2-li">
                                    <input type="text" name="job_baserate2_from" value="{{(isset($job_baserate2_from)) ? $job_baserate2_from : ''}}" id="job_baserate2_from" class="baserate_number" placeholder="Base Rate From"/>
                                </li>
                                <li class="job-baserate2-options2-li">
                                    <input type="text" name="job_baserate2_to" value="{{(isset($job_baserate2_to)) ? $job_baserate2_to : ''}}" id="job_baserate2_to" class="baserate_number" placeholder="Base Rate To"/>
                                </li>
                                <li class="job-baserate2-options2-li">
                                    <button type="button" class="btn btn-secondary clt-blue submit-search">Apply</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('work_experience2',[null => 'Experience'] + $experienceList,isset($work_experience2) ? $work_experience2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_start_date2',[null => 'Start Date'] + $startDateList,isset($job_start_date2) ? $job_start_date2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_duration2',[null => 'Duration'] + $durationList,isset($job_duration2) ? $job_duration2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        {{Form::select('job_industry2',[null => 'Industry'] + $industryList,isset($job_industry2) ? $job_industry2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}
                    </div>
                </li>
                <li>
                    <div class="dropdownTransparent">
                        <!--{{Form::select('job_state2',[null => 'State'] + $states,isset($job_state2) ? $job_state2 : '',['class' => 'form-control select2 subfilter','style' => 'width:100%;'])}}-->
                        <span class="job-state2-lable" id="job-state2-lable">State</span>
                        <!-- <img src="{{admin_asset('images/iconSelect2.png')}}"> -->
                        <i class="fa fa-sort-desc colorWhite" aria-hidden="true"></i>
                        <div class="job-state2-options form-control toogle-div" style="display: none;">
                            <ul>
                                @foreach($states as $statekey2 => $statevalue2)
                                <li>
                                    @if(isset($job_state2))
                                    @if(in_array($statevalue2,$job_state2))
                                    <input type="checkbox" name="job_state2[]" class="subfilter" value="{{$statevalue2}}" checked="" />{{$statevalue2}}
                                    @else
                                    <input type="checkbox" name="job_state2[]" class="subfilter" value="{{$statevalue2}}" />{{$statevalue2}}
                                    @endif
                                    @else
                                    <input type="checkbox" name="job_state2[]" class="subfilter" value="{{$statevalue2}}" />{{$statevalue2}}
                                    @endif
                                </li>    
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    {{Form::close()}}
</header>