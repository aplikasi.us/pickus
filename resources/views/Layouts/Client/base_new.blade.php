<!DOCTYPE html>
<html>
@include('Layouts.Client.head_new')
<body class="pageLatest pagebackend">
    @include('Layouts.General.header_new')
     <div class="row rowColumns no-gutters">
                @if(Route::currentRouteName() == "consultantsearch" || Route::currentRouteName() == "consultantjobdetails")
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 ">
                @else
                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 columnLeft newSidebar">
                        @include('Layouts.Client.sidebar_new') 
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">    
                @endif
                <article>
                    <section class="secColumn">
                        <div class="row background-floted">
                        </div>
                        <div class="row rowColumns no-gutters ">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 columnRight">
                                @yield('content')
                            </div>
                        </div>
                    </section>
                </article>
            </div>
        </div>
    </div>
    @include('Layouts.Client.footer_new')
    @include('Layouts.Client.foot_new')
    @yield('javascript')
</body>

</html>