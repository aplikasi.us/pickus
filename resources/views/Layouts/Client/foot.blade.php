@include('Layouts.General.jsroutes')
<!-- Mainly scripts -->
<script type="text/javascript" src="{{admin_asset('js/jquery-3.1.1.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/bootbox.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/theme_custom.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/validator.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/summernote_editor.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/custom.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/calculator1.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/calculator2.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/calculator3.js')}}"></script>
<script type="text/javascript" src="{{admin_asset('js/dropzone.js')}}"></script>
<!-- Mainly scripts -->