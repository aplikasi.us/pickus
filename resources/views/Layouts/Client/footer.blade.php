<footer class="siteFooter">
    <div class="footerTop">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-2 footer-logo align-self-center text-md-center">
                    <a href="{{route('client-home')}}" title="aplikasi.us">
                        <img src="{{admin_asset('images/logoFooter.png')}}" alt="aplikasi.us" title="aplikasi.us" />
                    </a>
                </div>
                <div class="col-sm-3 col-md-2 col-lg-1">
                    <h4>Menu</h4>
                    <ul class="listMenu">
                        <li>
                            <a href="{{route('client-home')}}" title="Home">Home</a>
                        </li>
                        <li>
                            <a href="{{url('/about-us')}}" title="About us">About us</a>
                        </li>
                        <li>
                            <a href="#" title="FAQ">FAQ</a>
                        </li>
                        <li>
                            <a href="http://206.189.86.153/blog/" title="Blog">Blog</a>
                        </li>
                        <li>
                            <a href="{{route('contactus')}}" title="Contact us">Contact us</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-7 col-lg-2 col-lg-4">
                    <h4>Recent posts</h4>
                    <ul class="list-unstyled">
                        @if(count($wordpresBlog))
                        @foreach ($wordpresBlog as $key=>$value)
                        @if($key < 5)
                        <li><span class="recent-box"></span><a target="_blank" href="{{$value['link']}}">{{$value['title']['rendered']}}</a></li>
                        @endif
                        @endforeach
                        @endif
                    </ul>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <h4>Address</h4>
                    <ul class="listMenu">
                        <li>
                            <a href="#" title="">Kumpulan Aplikasi Sempurna Sdn Bhd 2-2B,Tower 9, UOA Business Park,1
                                Jalan Pengaturcara U1/51A, 40150 Shah Alam,
                                Selangor, Malaysia</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2 col-lg-2">
                    <h4>Connect</h4>
                    <ul class="listSocialMedia">
                        <li>
                            <a href="https://www.linkedin.com/company/aplikasi-us" target="_blank">
                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCxGX87T3P9WySGZeQaM9OtQ?reload=9" target="_blank">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footerbottom">
        <div class="container-fluid">
            Copyright 2018. All rights reserved. Kumpulan Aplikasi Sempurna Sdn Bhd
        </div>
    </div>
</footer>