<div class="boxProfile">
    <div class="imgProfile">
        @if(Auth::user()->profile_pic != null)
        @if(file_exists(public_path('profile_image/'.Auth::user()->profile_pic)))
        <img src="{{admin_asset('profile_image/default.jpg')}}" alt="" title="" id="sidebar-profile-default" style="display: none;"/>
        <img src="{{admin_asset('profile_image/'.Auth::user()->profile_pic)}}" id="sidebar-profile-picture" alt="" title="" />
        @else
        <img src="{{admin_asset('profile_image/default.jpg')}}" alt="" title="" />
        @endif
        
        @else
        <img src="{{admin_asset('profile_image/default.jpg')}}" alt="" title="" />
        @endif
    </div>
    <h6></h6>
    <h5>
        {{Auth::user()->first_name . ' '.Auth::user()->last_name}}
    </h5>
    <span class="textlogged colorWhite">Logged in as Client</span>
</div>
<ul class="menuProfile">
    <li class="{{ (Route::currentRouteName() == "client-home" || Route::currentRouteName() == "successful-hires") ? 'active' : '' }}">
        <a href="{{route('client-home')}}" title="My Home" class="cus-iconBox cus-iconHome">My Home</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "client-profile") ? 'active' : '' }}">
        <a href="{{route('client-profile')}}" title="Profile" class="cus-iconBox cus-iconProfile">Profile</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "client-jobs" || Route::currentRouteName() == "client-edit-job") ? 'active' : '' }}">
        <a href="{{route('client-jobs')}}" title="My Jobs" class="cus-iconBox cus-iconMyjobs">My Jobs</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "client-interviews") ? 'active' : '' }}">
        <a href="{{route('client-interviews')}}" title="Interviews" class="cus-iconBox cus-iconInterviews">Interviews</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "client-notifications") ? 'active' : '' }}">
        <a href="{{route('client-notifications')}}" title="Notifications" class="cus-iconBox cus-iconNoti">Notifications</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "clientSettings") ? 'active' : '' }}">
        <a href="{{route('clientSettings')}}" title="Settings" class="cus-iconBox cus-iconSettings">Settings</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "addComparepage") ? 'active' : '' }}">
        <a href="{{route('addComparepage')}}" title="Comparison" class="cus-iconBox cus-iconcompare">Comparison</a>
    </li>
    <li class="{{ (Route::currentRouteName() == "logout") ? 'active' : '' }}">
        <a class="nav-link cus-iconBox cus-iconLogin" href="{{route('logout')}}" title="Logout" >Logout</a>
    </li>
</ul>