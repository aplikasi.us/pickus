@extends('Layouts.General.base_new')
@section('content')
@section('title','Home Page')


<div class="text-center">
    <img src="{{admin_asset('images/home_loader.gif')}}" />
    <div class="container Black text-left">
        <p style="color:#121212"><span class="text-red">Pause AdBlock:</span> Turn off AdBlock everywhere temporarily The quickest and easiest solution is to pause AdBlock, temporarily allowing the site's ads to appear.</br>
        To temporarily stop blocking ads on the current site:</br>
        Click the AdBlock button in the browser toolbar and select either Pause on this site or Pause AdBlock, depending on which browser you use.</br>
        Reload the page.</br>
        If you selected Pause on this site, AdBlock will automatically start blocking ads again when you leave the site. Otherwise, click the AdBlock button and select Resume blocking ads when you're finished.</p>
       
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function (){
    var ip = "<?php echo $_SERVER['REMOTE_ADDR']; ?>";
    var redirect_url = "{!! url('/') !!}";
    var url = "https://api.ipinfodb.com/v3/ip-city/?key=040d993abee04300a9a5afe6e865ab5b9eb6859ddfaa4b8b8bf517613306d78b&ip="+ip+"&format=json";
    
    delete $.ajaxSettings.headers["X-CSRF-TOKEN"]; // Remove header before call

    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        
        data: {},
            
        success: function (data) {
            if(data.countryCode == "PH"){
                location.href = redirect_url+'/PH';
            }else{
                location.href = redirect_url+'/MY';
            }
        }
    });
});
</script>
@endsection