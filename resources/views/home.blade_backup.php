@extends('Layouts.General.base')
@section('content')
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    {{ Session::get('error') }}
</div>
@endif
<section class="sap-consultants-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-5 sap-left">
                        <div class="row">
                            <div class="col-12 mt-20">
                                <p class="text-right">can't spell trust without<span class="us-section">US</span></p>
                            </div>
                            <h3 class="text-right col-12">Malaysia's First <span>Online Platform </span></h3>
                            <h2 class="text-right col-12">FOR SAP CONSULTANTS</h2>
                            <div class="sap-con-img col-12"><img class="img-fluid"
                                    src="{{admin_asset('images/sap-consultants.png')}}" alt="sap-consultants" title="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 align-self-center sap-right">
                        <div class="row">
                            <div class="card col-md-8 offset-md-2 join-us-section">
                                <div class="card-body">
                                    <h3>Join us now!</h3>
                                    <p>and enjoy a lot of benefits you can't get elsewhere.</p><a
                                        class="btn btn-block btn-success"
                                        href="{{route('consultant-registration')}}">MAKE ME A SUPERSTAR!</a>
                                    <p class="or-text text-center">OR</p><a class="btn btn-block btn-outline-success"
                                        href="{{route('client-registration')}}">I WANT TO BE A CLIENT</a>
                                    @if(!Auth::check())
                                    <p class="onboard-text text-center" href="javascript:;">Already onboard?</p><a
                                        class="btn btn-block btn-danger login-form-poup" id="login-button" href="javascript:;"
                                        >LOGIN</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="whyus-section">
    <div class="container-fluid pl-40 pr-40">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="main-title">Why <span class="us-section">us</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 whyus-boxes">
                <div class="row">
                    <div class="col-md-3 whyus-box">
                        <div class="icon-section"><img src="{{admin_asset('images/trustworthy-icon.png')}}"
                                alt="trustworthy-icon" title=""></div>
                        <h3>TRUSTWORTHY</h3>
                        <p>Being virtuous is what we do and we make sure it is translated into our consultants too.
                            Join us and put your worry to rest.</p>
                    </div>
                    <div class="col-md-3 whyus-box">
                        <div class="icon-section"><img src="{{admin_asset('images/transparent-icon.png')}}"
                                alt="trustworthy-icon" title=""></div>
                        <h3>TRANSPARENT</h3>
                        <p>Transparency is our core value and we uphold it in high regards. Many have claimed to be but
                            never revealed the numbers. We do.</p>
                    </div>
                    <div class="col-md-3 whyus-box">
                        <div class="icon-section"><img src="{{admin_asset('images/local-icon.png')}}"
                                alt="trustworthy-icon" title=""></div>
                        <h3>LOCAL</h3>
                        <p>Our own local talents are world-class and we know this well. Why go to great lengths of
                            finding foreign force when we can do a better job?</p>
                    </div>
                    <div class="col-md-3 whyus-box">
                        <div class="icon-section"><img src="{{admin_asset('images/superstars-icon.png')}}"
                                alt="trustworthy-icon" title=""></div>
                        <h3>SUPERSTARS</h3>
                        <p>With excellent skill set and extensive experience, our consultants are the who’s who in the
                            market. We are ready to vouch for them.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="how-us-section">
    <div class="container-fluid pl-40 pr-40">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="main-title">How<span class="us-section">us</span><span class="text-black">Work</span></h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-11 howus-boxs">
                <ul class="nav nav-tabs" id="consultants-clients-section" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="consultants-tab" data-toggle="tab"
                            href="#consultants" role="tab" aria-controls="consultants"
                            aria-selected="true">Consultants</a></li>
                    <li class="nav-item"><a class="nav-link" id="clients-tab" data-toggle="tab" href="#clients"
                            role="tab" aria-controls="clients" aria-selected="false">Clients</a></li>
                </ul>
                <div class="tab-content" id="consultants-clients">
                    <div class="tab-pane fade show active" id="consultants" role="tabpanel"
                        aria-labelledby="consultants-tab">
                        <div class="row">
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/advertise.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>ADVERTISE</h3>
                                <p>We advertise your job openings to those who have the expertise in the fields you
                                    need.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/search-profile.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>SEARCH PROFILE</h3>
                                <p>Look for what you need in your consultants here. We have a lot of consultants to
                                    choose from.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/placement.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>PLACEMENT</h3>
                                <p>The process is simple and fast. Once you have found your consultant, we can deploy
                                    them without hassle.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="clients" role="tabpanel" aria-labelledby="clients-tab">
                        <div class="row">
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/advertise.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>ADVERTISE</h3>
                                <p>We advertise your job openings to those who have the expertise in the fields you
                                    need.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/search-profile.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>SEARCH PROFILE</h3>
                                <p>Look for what you need in your consultants here. We have a lot of consultants to
                                    choose from.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/placement.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>PLACEMENT</h3>
                                <p>The process is simple and fast. Once you have found your consultant, we can deploy
                                    them without hassle.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sap-job section">
    <div class="container-fluid pl-40 pr-40">
        <div class="row">
            <div class="col-md-5"><img class="img-fluid sap-jobs" src="{{admin_asset('images/sap-jobs-bg.png')}}"
                    alt="sap-jobs-bg" title=""></div>
            <div class="col-md-6">
                <h2 class="main-title"><span class="text-red">Get Sap </span><span>Jobs Easier</span></h2>
                <p>
                    Finding a job is not a simple thing to do in this line of work, we know. But if you are reading
                    this, then you are<span class="text-red">one step close </span>to defy that mantra. </p>
                <p>
                    With a pool of reputable companies, personalised match-making and procurement management, you can
                    expect to have work fulfilment<span class="text-red">without having to worry about your
                        payment.</span></p>
                <p>Take another step and<span class="text-green">join us now.</span></p>
            </div>
        </div>
    </div>
</section>
<section class="reliable-sap-section">
    <div class="container-fluid pl-40 pr-15">
        <div class="row">
            <div class="col-md-6">
                <h2 class="main-title"><span class="text-red">Reliable SAP Consultants </span><span
                        class="text-black">at
                        Your Disposal</span></h2>
                <p>
                    If you are looking for a convenient, fast and reliable way to acquire the talent you need, then you
                    have come to the right place.</p>
                <p>
                    With a large pool of SAP consultants to choose from, you don’t have to worry about getting
                    irrelevant candidates anymore.<span class="text-red"> Post any vacancies here for free!</span></p>
                <p>Shop for them to your heart’s content. Let us make your way easier.</p>
            </div>
            <div class="col-md-5 pr-0 offset-md-1"><img class="img-fluid"
                    src="{{admin_asset('images/reliable-sap-bg.png')}}" alt="reliable-sap-bg" title=""></div>
        </div>
    </div>
</section>
<section class="how-us-section services-section">
    <div class="container-fluid pl-40 pr-40">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="main-title">Our <span class="text-black">Services</span></h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-11 howus-boxs">
                <ul class="nav nav-tabs" id="services-consultants-clients-section" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="services-consultants-tab" data-toggle="tab"
                            href="#services-consultants" role="tab" aria-controls="services-consultants"
                            aria-selected="true">Consultants</a></li>
                    <li class="nav-item"><a class="nav-link" id="services-clients-tab" data-toggle="tab"
                            href="#services-clients" role="tab" aria-controls="services-clients"
                            aria-selected="false">Clients</a></li>
                </ul>
                <div class="tab-content" id="services-consultants-clients">
                    <div class="tab-pane fade show active" id="services-consultants" role="tabpanel"
                        aria-labelledby="services-consultants-tab">
                        <div class="row">
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/payroll-hosting.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>PAYROLL HOSTING</h3>
                                <p>No more painstaking and cumbersome billing process. Let us deal with the clients’
                                    finance department and get you paid quickly.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/co-working.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>CO-WORKING SPACE</h3>
                                <p>No proper working space? Work comfortably here with us where we have everything you
                                    need. After-office hours? No problem.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/medical-concierge.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>MEDICAL CONCIERGE</h3>
                                <p>Need a good medical protection? Need a convenient outpatient package you can use
                                    anywhere? Join us now and enjoy all of these at low prices!</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="services-clients" role="tabpanel"
                        aria-labelledby="services-clients-tab">
                        <div class="row">
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/payroll-hosting.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>PAYROLL HOSTING</h3>
                                <p>No more painstaking and cumbersome billing process. Let us deal with the clients’
                                    finance department and get you paid quickly.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/co-working.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>CO-WORKING SPACE</h3>
                                <p>No proper working space? Work comfortably here with us where we have everything you
                                    need. After-office hours? No problem.</p>
                            </div>
                            <div class="col-md-4 howus-box">
                                <div class="icon-section"><img src="{{admin_asset('images/medical-concierge.png')}}"
                                        alt="trustworthy-icon" title=""></div>
                                <h3>MEDICAL CONCIERGE</h3>
                                <p>Need a good medical protection? Need a convenient outpatient package you can use
                                    anywhere? Join us now and enjoy all of these at low prices!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="testimonial-section">
    <div class="container-fluid pl-40 pr-40">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="main-title"><span class="text-red">What People </span><span class="text-white">Say</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-theme owl-loaded" id="testimonial-carousel">



                    <div class="owl-stage-outer">
                        <div class="owl-stage"
                            style="transform: translate3d(-4320px, 0px, 0px); transition: all 0s ease 0s; width: 10080px;">
                            <div class="owl-item cloned" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item animated owl-animated-in fadeIn active" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item animated owl-animated-in fadeIn" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 1440px;">
                                <div class="item">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 testimonial-content">
                                            <p class="text-center text-regular">I’ve gone to many recruiters before but
                                                being general recruiters, they don’t always meet my needs. That is when
                                                I found <span class="text-bold">aplikasi.us. </span>With a pool of
                                                experienced and skillful talents, hiring has never been so easy for me.
                                                I can pick and choose whomever that I want with the right expertise and
                                                price.</p>
                                            <p class="text-center"><i class="text-light">C.N. Hashim</i><br><i
                                                    class="text-light">SAP
                                                    Functional Support Team Lead,</i><br><i class="text-light">Sime
                                                    Darby Global Services Centre</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><img
                                src="{{admin_asset('images/left-arrow.png')}}"></button><button type="button"
                            role="presentation" class="owl-next"><img
                                src="{{admin_asset('images/right-arrow.png')}}"></button></div>
                    <div class="owl-dots"><button role="button" class="owl-dot"><span></span></button><button
                            role="button" class="owl-dot active"><span></span></button><button role="button"
                            class="owl-dot"><span></span></button></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="counter-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6 counter-box text-center">
                        <h2 class="counter-title">200+</h2>
                        <h6 class="counter-content">Consultants</h6>
                    </div>
                    <div class="col-md-6 counter-box text-center">
                        <h2 class="counter-title">130</h2>
                        <h6 class="counter-content">Placement</h6>
                    </div>
                    <div class="col-md-6 counter-box text-center">
                        <h2 class="counter-title">24</h2>
                        <h6 class="counter-content">Clients</h6>
                    </div>
                    <div class="col-md-6 counter-box text-center">
                        <h2 class="counter-title">1000%</h2>
                        <h6 class="counter-content">Awesome</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="recognised-section">
    <div class="container-fluid pl-40 pr-40">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="main-title"><span class="text-red">Recognised </span><span class="text-black">By</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel owl-theme owl-loaded owl-drag" id="client-logo-carousel">
                <div class="owl-stage-outer">
                    <div class="owl-stage"
                        style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1490px;">
                        <div class="owl-item active" style="width: 228.333px; margin-right: 20px;">
                            <div class="item align-self-center"><img class="img-fluid"
                                    src="{{admin_asset('images/mof.png')}}" alt="mof" title=""></div>
                        </div>
                        <div class="owl-item active" style="width: 228.333px; margin-right: 20px;">
                            <div class="item align-self-center"><img class="img-fluid"
                                    src="{{admin_asset('images/wilhelmsen-logo.png')}}" alt="wilhelmsen" title=""></div>
                        </div>
                        <div class="owl-item active" style="width: 228.333px; margin-right: 20px;">
                            <div class="item align-self-center"><img class="img-fluid"
                                    src="{{admin_asset('images/ervice.png')}}" alt="ervice" title=""></div>
                        </div>
                        <div class="owl-item active" style="width: 228.333px; margin-right: 20px;">
                            <div class="item align-self-center"><img class="img-fluid"
                                    src="{{admin_asset('images/ibm-logo.png')}}" alt="ibm" title=""></div>
                        </div>
                        <div class="owl-item active" style="width: 228.333px; margin-right: 20px;">
                            <div class="item align-self-center"><img class="img-fluid"
                                    src="{{admin_asset('images/asr-selangor.png')}}" alt="asr-selangor" title=""></div>
                        </div>
                        <div class="owl-item active" style="width: 228.333px; margin-right: 20px;">
                            <div class="item align-self-center"><img class="img-fluid"
                                    src="{{admin_asset('images/alam-maritim.png')}}" alt="alam-maritim" title=""></div>
                        </div>
                    </div>
                </div>
                <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><img
                            src="{{admin_asset('images/left-arrow.png')}}" alt="arrow"></button><button type="button"
                        role="presentation" class="owl-next"><img src="{{admin_asset('images/right-arrow.png')}}"
                            alt="arrow"></button></div>
                <div class="owl-dots disabled"><button role="button" class="owl-dot active"><span></span></button></div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" method="post" action="{{route('login')}}" id="consultant-login">
                <div class="modal-body mx-3">

                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="email" id="email" class="form-control input-sm"
                                    placeholder="Enter email address" style="border-radius: 50px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control input-sm"
                                    placeholder="Enter password" style="border-radius: 50px;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-block btn-danger" type="submit"
                        style="border-radius: 50px;width: 50%;">GET ME IN</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection