@extends('Layouts.General.base_new')
@section('content')
@section('title','Search')
<div class="row ">    
    <div class="col-md-9">
        @if(count($candidates))
        <p style="color:black; top:-10px;bottom:-10px;">Showing <span style="color:#DEAB68!important;"> {{count($candidates)}} </span> out of {{$total}}</p>
        <div class="row ">
            @if(Auth::check())
            @if(Auth::user()->role_id == 1)
            
            <p class="text-right d-block" style="width:100%">
                <a title="Compare Now!" href="{{route('addComparepage')}}" class="btn btn-md btnRemove comparecount">
                    Compare Now ({{count($addTocompare)}})!
                </a>
            </p>
            @endif
            @endif   
            @foreach($candidates as $key => $value)
            <div class="col-md-4">
                <div class="commanContent">
                    
                    @if($value->available_for_contract == 1)
                    <div class="status online" data-toggle="tooltip" title="Available"></div>
                    @else
                    <div class="status offline" data-toggle="tooltip" title="Unavailable"></div>
                    @endif
                    
                    <ul class="listContract" style="padding-left:10px;">
                        
                        <li>{{$value->candidate_id}} 
                            <div class="box-rating consultant-start-rating">
                                @if(Auth::check())
                                @if(Auth::user()->role_id == 1)
                                
                                @for($i = 1; $i <= 5; $i++)
                                @if($value->rating < $i)
                                <span class="fa fa-star unchecked"></span>
                                @else
                                <span class="fa fa-star checked"></span>
                                @endif
                                @endfor
                                
                                @endif
                                @endif
                            </div>
                        </li>
                    </ul>
                    <div class="row rowContract">
                    <a href="{{route('consultant-detail',[\Crypt::encryptString($value->CANDIDATEID)])}}"><h5>{{$value->sap_job_title}}</h5></a>
                    
                        <div class="col-md-8">
                        <div class="cus-verticale-center">
                            <div class=" text-red">
                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                <span class="ml-1">MYR </span>
                                <span
                                class="c-job-base-rate"
                                id="con-copy-1{{$key}}">{{($value->base_rate != null) ? $value->base_rate : 0}}/day</span>
                                <div class="iconBox iconCopy copy-to-clipboard job-tile-button-right d-inline m-l-10"></div>
                            </div>
                                
                        </div>
                           
                        </div>
                    </div>
                    <div class="row rowContract">
                        <div class="col-sm-12 col-lg-6">
                            <img src="{{admin_asset('images/cus-experience.png')}}"
                                                class="icon-cus-img">
                            <div class="ml-1 d-inline Black">{{$value->experience_in_years}} years exp.</div>
                        </div>

                        <div class="col-sm-12 col-lg-6 unavalibale {{($value->willing_to_travel == 1) ? '' : 'disabled'}}">
                            <img src="{{admin_asset('images/cus-group-6.png')}}" class="icon-cus-img will-travel-img">
                            <div class="ml-1 d-inline Black {{($value->willing_to_travel == 1) ? '' : 'disabled'}}"> {{($value->willing_to_travel == 1) ? "Ok to Travel" : "Travel"}}</div>
                        </div>
                    </div>
                    <div class="row rowContract">
                    <div class="col-sm-12 col-lg-6">
                    <img src="{{admin_asset('images/cus-month.png')}}"
                                                class="icon-cus-img">
                            <div class="ml-1 d-inline Black">{{$value->notice_period_days}} days</div>
                        </div>

                    <div class="col-sm-12 col-lg-6 unavalibale {{($value->project == 1) ? '' : 'disabled'}}">
                            <img src="{{admin_asset('images/cus-Group-9.png')}}" class="icon-cus-img">
                            <div class="ml-1 d-inline Black {{($value->project == 1) ? '' : 'disabled'}}">{{($value->project == 1) ? "Project" : "Project"}}</div>
                        </div>
                        
                        
                    </div>
                    <div class="row rowContract">
                    <div class="col-sm-12 col-lg-6">
                            <img src="{{admin_asset('images/cus-start.png')}}"
                                                class="icon-cus-img">
                            <div class="ml-1 d-inline Black">{{($value->availability_date) ? date('d M. Y',strtotime($value->availability_date)): '-'}} </div>
                        </div>

                        <div class="col-sm-12 col-lg-6  unavalibale {{($value->support == 1) ? '' : 'disabled'}}">
                            <img src="{{admin_asset('images/cus-Group-3.png')}}" class="icon-cus-img">
                            <div class="ml-1 d-inline Black {{($value->support == 1) ? '' : 'disabled'}}">{{($value->support == 1) ? "Support" : "Support"}}</div>
                        </div>
                        
                    </div>
                    <div class="row rowContract">

                    <div class="col-sm-12 col-lg-6 unavalibale {{($value->full_time == 1) ? '' : 'disabled'}}">
                            <img src="{{admin_asset('images/cus-ull-time.png')}}"
                                                class="icon-cus-img">
                            <div class="ml-1 d-inline Black {{($value->full_time == 1) ? '' : 'disabled'}}">{{($value->full_time == 1) ? "Full time" : "Full time"}}</div>
                        </div>

                    <div class="col-sm-12 col-lg-6">
                            <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                            <div class="ml-1 d-inline Black ">{{($value->city != null) ? $value->city : "N/A"}}</div>
                        </div>
                    </div>
                    <div class="row rowContract">
                    <div class="col-sm-12 col-lg-6 unavalibale {{($value->part_time == 1) ? '' : 'disabled'}}">
                            <img src="{{admin_asset('images/cus-Group-8.png')}}"
                                                class="icon-cus-img">
                            <div class="ml-1 d-inline Black  {{($value->part_time == 1) ? '' : 'disabled'}}">{{($value->part_time == 1) ? "Part time" : "Part time"}}</div>
                        </div>
                    </div>
                    <div class="boxbtns job-tile-button-right">
                        <div class="row no-gutters border-top-btn">
                            @if(Auth::check())
                                @if(Auth::user()->role_id == 1)
                                <div class="col-md-6  border-right-btn">
                                    @php
                                    $btnCompare = 'can-btnRemoveCompare';
                                    $btntext = 'Remove Compare';
                                    if(!in_array($value->CANDIDATEID,$addTocompare)){
                                    $btnCompare = 'can-btnCompare';
                                    $btntext = 'Add to Compare';
                                    }
                                    @endphp
                                    <a title="{{$btntext}}" href="javascript:;"  data-id="{{$value->CANDIDATEID}}" data-candidate-id="{{$value->id}}" class="btn btn-custom-trans colorBlue btn-block addcompare-icon  {{$btnCompare}}">{{$btntext}}</a>
                                    <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading" style="display: none;">
                                </div>
                                <div class="col-md-6">
                                    @if(in_array($value->CANDIDATEID,$wishlist))
                                    <a title="Remove" href="javascript:;" class="btn btn-custom-trans colorRed btn-block btnSave remove-to-wish-list-con whishilst-herat-icon" data-id="{{$value->CANDIDATEID}}" data-candidate-id="{{$value->id}}">Remove Wishlist</a>
                                    @else
                                    <a title="Save" href="javascript:void(0)" class="btn btn-custom-trans colorRed btn-block btnSave save-to-wish-list whishilst-herat-icon" data-id="{{$value->CANDIDATEID}}" data-candidate-id="{{$value->id}}">Save to Wishlist</a>
                                    @endif
                                </div>
                                
                                @endif
                            @else
                            
                            <div class="col-md-6  border-right-btn">
                                <a title="Add to Compare" href="javascript:;" class="btn btn-custom-trans colorBlue btn-block addcompare-icon  general-saveto-compare addcompare-icon" data-id="{{$value->CANDIDATEID}}" data-candidate-id="{{$value->id}}" data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Add to Compare</a>
                            </div>
                            <div class="col-md-6">
                                <a title="Save" href="javascript:void(0)" class="btn btn-custom-trans colorRed btn-block  general-saveto-wishlist whishilst-herat-icon" data-id="{{$value->CANDIDATEID}}" data-candidate-id="{{$value->id}}" data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Save to Wishlist</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{$candidates->links()}}
        @if(Auth::check())
        @if(Auth::user()->role_id == 1)
        <p class="text-right d-block" style="width:100%">
            <a title="Compare Now!" href="{{route('addComparepage')}}" class="btn btn-md btnRemove comparecount">
                Compare Now ({{count($addTocompare)}})!
            </a>
        </p>
        @endif
        @endif 
        @else
        <div class="col-md-12">
            <p class="text-center">
                <span class="no-result">No result found</span>
            </p>
        </div>
        @endif
    </div>
    <div class="col-md-3">
        <h3 class="header-red-bg colorWhite text-center pd-5 how-much-left">Doing Your Budget?</h3>
        <div class="commanContent boxHowmuch">
            
            <div class="contentHowmuch">
                <p class="pb-1">Enter Base Rate</p>
                <div class=" text-red inlineText">
                    <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                    <span class="ml-1">MYR </span>
                </div>
                <input type="text" class="text-red text-right" value="" id="c3-base-rate-perday" maxlength="10"/>
                <span class="text-red">/day</span>
            </div>
            <div class="row f-12 rs-5">
                <div class="col-md-6">
                    <p class="pb-2">Length of contract
                        <a href="javascript:void(0)" class="iconInfo">
                            <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                        </a>
                    </p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5 months</li>
                        <li>
                            <input type="radio" name="months" class="months" value="2" title="6-11 months" checked=""/> 6-11 months</li>
                        <li>
                            <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12 months</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <p class="pb-2">Fees</p>
                    <ul class="listCheckbox">
                        <li>
                            <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee" checked="" disabled=""/> Admin fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                        <li>
                            <input type="checkbox" class="fees" name="professional_fee" title="Professional fee" id="professional_fee" checked="" /> Professional fee
                            <a href="javascript:void(0)" class="iconInfo">
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="boxDoyouKnow mb-0">
                <h6 class="f-14 mb-0">The Billing Rate is</h6>
                <h2 class="text-red" id="billing-rate">MYR 0.00/day</h2>
            </div>
        </div>
        @include('Layouts.General.doyouknow')
        @include('Layouts.General.tips')
        @if(Auth::check())
            @if(Auth::user()->role_id == 1)
                @include('Layouts.General.rating')
            @endif
        @endif
    </div>
</div>

@endsection