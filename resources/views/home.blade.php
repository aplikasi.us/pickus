@extends('Layouts.General.base_new')
@section('content')
@section('title','Home Page')
@if(Session::has('success'))
<div class="alert alert-success ">
    {!! Session::get('success') !!}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger ">
    {!! Session::get('error') !!}
</div>
@endif
<article>
    <section class="homesection1">
        <div class="container">
            <div class="row rowHomeTopcontent">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    @if(Session::get('territory') == "MY")
                        <h2>Malaysia’s First Online Platform For SAP Consultants</h2>
                    @else
                        <h2>Philippines's First Online Platform For SAP Consultants</h2>
                    @endif
                    
                    <p id="downword-search">SAP Consultants are looking for jobs and clients are finding good SAP consultants, but you guys just can’t find each other easily, can you? But worry not dear people, we have arrived.</p>
                    <p>
                        <a href="#" class="btnVideo youtube-link-pop" title="What is aplikasi.us?" youtubeid="YUQAVqb5SP0"><img src="{{admin_asset('images/icon-play.png')}}" alt="" title=""/> What is aplikasi.us?</a>
                    </p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="imgElement1">
                        <img src="{{admin_asset('images/home_element1.png')}}" alt="" title=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="homesection2">
        <div class="container" id="registration-div">
            <div class="row rowBeliever">
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="imgElement2">
                        <img src="{{admin_asset('images/home_element2.png')}}" alt="" title=""/>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="contentBeliever">
                        <h3>Be A Part of The Evolution!</h3>
                        <p>No more headhunters, no more strangers. You don’t have to wait anymore to find the right fit; no matter if you are a consultant or someone who’s looking for one. Do it fast, with aplikasi.us.</p>
                        {{-- <div class="row">
                            @if(!Auth::check())
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <a href="{{route('consultant-registration')}}" class="btn btn-darkgreen btn-home-s btn-block" title="Make Me A Superstar">Make Me A Superstar</a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <a href="{{route('client-registration')}}" class="btn btn-orange btn-block btn-home-s" title="Make Me A Superstar">I Want To Be A Client</a>
                            </div>
                            @endif
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="boxwhyUs">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <h2 class="heading2 pd-b-30">Why <span class="textUs"><img src="{{admin_asset('images/textUs.png')}}" alt="" title=""></span></h2>
                        <div class="tab-home-black">
                            <ul class="nav nav-tabs nav-fill">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#consultantProfiles" role="tab" aria-selected="true">For Consultants</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " data-toggle="tab" href="#clientProfiles" role="tab" aria-selected="false">For Clients</a>
                                </li>
                            </ul>
                            <div class="tab-content border-ra-5">
                                <div class="tab-pane fade active show" id="consultantProfiles">
                                    <div class="row">
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Proactive</h3>
                                                <p>We don’t just sit around and let your profile go to waste. We are working days and nights to match you with the best spot you deserve.</p>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Convenience</h3>
                                                <p>Find where you belong, track your application, interview schedule, project history and calculate your earnings with aplikasi.us. Everything in one place.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Opportunity</h3>
                                                <p>Market yourself to reputable companies and become a Superstar, and you know what they say about one; it’ll shine even in the darkest night.</p>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Rewards</h3>
                                                <p>Yes. We highly appreciate loyal stars and as such, we have various rewards for them ready. Join us and find out yourselves how we thank you.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane fade " id="clientProfiles">
                                    <div class="row">
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Trustworthy</h3>
                                                <p>Being virtuous is what we do and we make sure it is translated into our consultants too. Why? Because you deserve it</p>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Transparent</h3>
                                                <p>Transparency is our core value and we do hold to it tightly. Numbers? Margins? Skills? Experience? Quality? Yes, we reveal them all.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Local Superstars</h3>
                                                <p>Yes, our own local talents are world-class and we know this well; we all do. And where to get the best of the best? Yes, you know that too.</p>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                            <div class="why-us-content">
                                                <h3>Convenience</h3>
                                                <p>Post jobs for free, track them, keep tabs on your interviews, and calculate your cost with aplikasi.us. This is the one place you’re looking for.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="imgElement3">
                            <img src="{{admin_asset('images/home_element3.png')}}" alt="" title=""/>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section>  
    <section class="homesection3">
        <div class="imgElement5">
            <img src="{{admin_asset('images/home_element5.png')}}" alt="" title=""/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 aboutColumnLeft">
                    <h2 class="heading2">What They Said<br/> About  <span class="textUs"><img src="{{admin_asset('images/textUs.png')}}" alt="" title=""></span></h2>
                    <div class="sliderAboutus">
                        <div id="carouselAboutus" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <p class="testimonyTextHome">I’ve been approached by many recruiters out there to get placements. But most of them don’t have as much understanding and knowledge about SAP as us do and I always ended up with jobs that do not fit my expertise and expectations. Being SAP consultants themselves, They have a good grasp on what I need and that surely makes my job more fulfilling and worthy of my time.</p>
                                    <p class="testimonyTextHome">M.A. Husin</p>
                                    <p class="testimonyTextHome">SAP ABAP Consultant</p>
                                </div>
                                <div class="carousel-item">
                                    <p class="testimonyTextHome">I’ve gone to many recruiters before but being general recruiters, they don’t always meet my needs. That is when I found us. With a pool of experienced and skillful talents, hiring has never been so easy for me. I can pick and choose whomever that I want with the right expertise and price.</p>
                                    <p class="testimonyTextHome">C.N. Hashim</p>
                                    <p class="testimonyTextHome"> SAP Functional Support Team Lead, Sime Darby Global Services Centre.</p>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselAboutus" role="button" data-slide="prev">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselAboutus" role="button" data-slide="next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="contentConsultants">
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <div class="number-content">
                                    <h2>200 <span>Consultants</span></h2>
                                    <h2>24 <span>Clients</span></h2>
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <div class="number-content">
                                    <h2>130 <span>Placements</span></h2>
                                    <h2>10000% <span>Awesome</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="imgElement4">
                        <img src="{{admin_asset('images/home_element4.png')}}" alt="" title=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>      
    <section class="homesection4">
    <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 columnGetinTouch">
                    <div class="boxGetinTouch">
                        <h2>Get In Touch</h2>
                        <p>Want to know more about aplikasi.us? Just talk with us through the chat box, or if you’d like to drop an email or a phone call, we’ll be happy to connect with you.</p>	
                        <p><a  href="javascript:;" onclick='$("#zsiq_agtpic").click();' class="btn btnRed" title="Talk To Us!">Talk To Us!</a> </p>			
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="imgElement6_mobile">
                        
                        <img src="{{admin_asset('images/home_element6.png')}}" alt="" title="">
                    </div>  
                    <h2 class="heading2" style="margin-left: 32%;margin-bottom:-2%;">Trusted By</h2>
                    <div class="imgElement6">
                        <div id="trustedSlider">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @if(Session::get('territory') == "MY")
                                        <div class="carousel-item active">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_wasco.png')}}" alt="First slide">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_sime-darby.png')}}" alt="Second slide">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_sesb.png')}}" alt="Third slide">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_puncak-niaga.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_prasarana.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_mof.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_manpower.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_ibm.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_ia.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_hcl.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_fgv.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_cognizant.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_air-selangor.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_accenture.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_kelly.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_alam.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trusted/aplikasi_us_sap_malaysia_wilhelmses_ship.png')}}" alt="" title="">
                                        </div>
                                @else
                                <div class="carousel-item active">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_chanmbers.png')}}" alt="" title="">
                                            </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_corporate-hero.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_desk-one.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_gpcci.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_MSI-ECS.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_sap-silver-partner.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_scmap.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_tac-services.png')}}" alt="" title="">
                                        </div>
                                        <div class="carousel-item">
                                        <img class="d-block w-100 trustedLogo" src="{{admin_asset('images/trustedPH/aplikasi_us_sap_ph_vinea.png')}}" alt="" title="">
                                        </div>                                        
                                        @endif                                                                             
                            </div>
                            <!-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon1" style="color:black;" aria-hidden="true"></span>
                                <span class="sr-only" >Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon2" style="color:black;" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a> -->
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" >
                                <i class="fa fa-angle-left" style="color:#121212;font-size:20px;"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <i class="fa fa-angle-right" style="color:#121212; font-size:20px;"></i> 
                        </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="boxSearchConsultants">
            <div class="imgElement8">
                <img src="{{admin_asset('images/home_element8.png')}}" alt="" title=""/>
            </div>
            <div class="imgElement7">
                <img src="{{admin_asset('images/home_element7.png')}}" alt="" title=""/>
            </div>
            <div class="container">
                <div class="row rowSap">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="contentSAP">
                            <h3><span>Are you an</span> SAP Client?</h3>
                            <p>And need quality SAP consultants like, yesterday? Well, we don’t have a time machine, but we do have a pool of Superstars we’re sure you’ll like.</p>
                            <p>
                                <a href="{{route('client-registration')}}" class="btn btntransprant " title="START DRAFTING!">START DRAFTING!</a>
                            </p>                                    
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="contentSAP contentSAPRight">
                            <h3><span>Are you an</span> SAP Consultant?</h3>
                            <p>And struggling to get that job continuity? Why not aplikasi.us where you can search for your future and have us doing the same too?</p>
                            <p class="text-left">
                                <a href="{{route('consultant-registration')}}" class="btn btntransprant " title="JOIN US NOW!">JOIN US NOW!</a>
                            </p>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>              
</article>
@endsection
@section('javascript')
<script type="text/javascript">

var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("trustedLogo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

    $(document).ready(function () {
        var search = "<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>";
        if (search == "search-job") {
            $(".search-jobsTab").trigger("click");
            $('html, body').animate({
                scrollTop: $("#downword-search").offset().top
            }, 1600);
        }
        if (search == "registration") {
            $('html, body').animate({
                scrollTop: $("#registration-div").offset().top
            }, 1600);
        }

        $(".join-current").on("click", function () {
            $('html, body').animate({
                scrollTop: $("#registration-div").offset().top
            }, 1600);
        });
        $(".youtube-link-pop").grtyoutube();
    });

    $(document).ready(function () {

        var isTerritorySet = "<?php echo Session::get('territory'); ?>";
        if (isTerritorySet == "") {
            $("#teritories-modal").modal({
                // backdrop: 'static',
                // keyboard: false
            });
        }
        $("#territory-link").click(function () {
            $("#teritories-modal").modal({
                // backdrop: 'static',
                // keyboard: false
            });
        });
    });

</script>
@endsection