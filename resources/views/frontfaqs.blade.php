@extends('Layouts.General.base_new')
@section('content')
@section('title','FAQ')
<style>
    /*******************************
* Does not work properly if "in" is added after "collapse".
* Get free snippets on bootpen.com
*******************************/
    .mb-0 > a {
    display: block;
    position: relative;
    }
    .mb-0 > a:after {
    content: "\f067"; /* fa-chevron-down */
    font-family: 'FontAwesome';
    position: absolute;
    right: 0;
    }
    .mb-0 > a[aria-expanded="true"]:after {
    content: "\f068"; /* fa-chevron-up */
    }
</style>
<article class="pageFaq">
    <section class="faqsection1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="contentFaqTop">
                        <h2 class="headingFaq title-border-b">Frequently Asked Questions</h2>
                        <h3 class="colorWhite">Consultant</h3>
                        @if(count($consultant->faq))
                            <div id="accordion" class="cus-faq-accordion">
                                @foreach ($consultant->faq as $faqKey1 => $faqValue1 )
                                        <div class="card">
                                            <div class="card-header" id="heading-1">
                                                <h5 class="mb-0 pb-0 cus-icon-faq ">
                                                    <a role="button" data-toggle="collapse" href="#collapse-con-{{$faqKey1}}" aria-expanded="{{($faqKey1 == 0 ) ? 'true' : 'false'}}" aria-controls="collapse-{{$faqKey1}}">
                                                    {{$faqValue1->question}}
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapse-con-{{$faqKey1}}" class="collapse {{($faqKey1 == 0 ) ? 'show' : 'hide'}} collapas-cus" data-parent="#accordion" aria-labelledby="heading-1">
                                                <div class="card-body">
                                                {{$faqValue1->answer}}
                                                </div>
                                            </div>
                                        </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="imgFaqElement1">
                        <img src="{{admin_asset('images/faq-element-1.png')}}" alt="" title="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faqsection2">
        <div class="container">
            <div class="row mobile-reveserv">
                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="imgFaqElement2">
                        <img src="{{admin_asset('images/faq-element-2.png')}}" alt="" title="" />
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="contentFaqTop">
                        <!-- <h2 class="headingFaq title-border-b">Frequently Asked Questions</h2> -->
                        <h3 class="colorWhite">Pricing Related (Consultant)</h3>
                        <div id="accordion" class="cus-faq-accordion">
                            @if(count($priceConsultant->faq))
                                <div id="accordion" class="cus-faq-accordion">
                                    @foreach ($priceConsultant->faq as $faqKey2 => $faqValue2 )
                                            <div class="card">
                                                <div class="card-header" id="heading-1">
                                                    <h5 class="mb-0 pb-0 cus-icon-faq ">
                                                        <a role="button" data-toggle="collapse" href="#collapse-pcons{{$faqKey2}}" aria-expanded="{{($faqKey2 == 0 ) ? 'true' : 'false'}}" aria-controls="collapse-{{$faqKey2}}">
                                                        {{$faqValue2->question}}
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapse-pcons{{$faqKey2}}" class="collapse {{($faqKey2 == 0 ) ? 'show' : 'hide'}} collapas-cus" data-parent="#accordion" aria-labelledby="heading-1">
                                                    <div class="card-body">
                                                    {{$faqValue2->answer}}
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <section class="faqsection3">
        <div class="container">
            <div class="row mb-80">
                <div class="col-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="contentFaqTop">
                        <!-- <h2 class="headingFaq title-border-b">Frequently Asked Questions</h2> -->
                        <h3 class="colorWhite">Clients</h3>
                        <div id="accordion1" class="cus-faq-accordion">
                            @if(count($client->faq))
                                    @foreach ($client->faq as $faqKey3 => $faqValue3 )
                                            <div class="card">
                                                <div class="card-header" id="heading-1">
                                                    <h5 class="mb-0 pb-0 cus-icon-faq ">
                                                        <a role="button" data-toggle="collapse" href="#collapse-cli{{$faqKey3}}" aria-expanded="{{($faqKey3 == 0 ) ? 'true' : 'false'}}" aria-controls="collapse-{{$faqKey3}}">
                                                        {{$faqValue3->question}}
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapse-cli{{$faqKey3}}" class="collapse {{($faqKey3 == 0 ) ? 'show' : 'hide'}} collapas-cus" data-parent="#accordion" aria-labelledby="heading-1">
                                                    <div class="card-body">
                                                    {{$faqValue3->answer}}
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="imgFaqElement3">
                        <img src="{{admin_asset('images/faq-element-3.png')}}" alt="" title="" />
                    </div>
                </div>
            </div>
            <div class="row mobile-reveserv">
                
                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="imgFaqElement4">
                        <img src="{{admin_asset('images/faq-element-4.png')}}" alt="" title="" />
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 pb-5">
                    <div class="contentFaqTop">
                        <!-- <h2 class="headingFaq title-border-b">Frequently Asked Questions</h2> -->
                        <h3 class="colorWhite">Pricing Related (Client)</h3>
                        <div id="accordion2" class="cus-faq-accordion">
                            @if(count($priceClient->faq))
                                    @foreach ($priceClient->faq as $faqKey4 => $faqValue4 )
                                            <div class="card">
                                                <div class="card-header" id="heading-1">
                                                    <h5 class="mb-0 pb-0 cus-icon-faq ">
                                                        <a role="button" data-toggle="collapse" href="#collapse-pcli{{$faqKey4}}" aria-expanded="{{($faqKey4 == 0 ) ? 'true' : 'false'}}" aria-controls="collapse-{{$faqKey4}}">
                                                        {{$faqValue4->question}}
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapse-pcli{{$faqKey4}}" class="collapse {{($faqKey4 == 0 ) ? 'show' : 'hide'}} collapas-cus" data-parent="#accordion" aria-labelledby="heading-1">
                                                    <div class="card-body">
                                                    {{$faqValue4->answer}}
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faqsection4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3 class="colorWhite text-center mb-5">Can't find what your looking for?</br>Just ask us <a href="javascript:;" onclick='$("#zsiq_agtpic").click();' class="colorWhite border-thrRed">here!</a></h3>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection
@section('javascript')
<!-- <script type="text/javascript">
    function toggleIcon(e) {
        $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus fa-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script> -->
@endsection