@extends('Layouts.General.base_new')
@section('content')
@section('title', $job->posting_title)
@section('description', strip_tags($job->job_description))
<script>

   function calPayRate() {
  var payRate = document.getElementById("payRate").value;
  return payRate;
}

function calContractLength() {
  var contractLength = document.getElementById("contractLength").value;
  return contractLength;
}

function calPaymentTerm() {
  var getPaymentTerm = 0;
  if (document.getElementById("backToBack").checked) {
    getPaymentTerm = document.getElementById("backToBack").value;
  } else if (document.getElementById("30Days").checked) {
    getPaymentTerm = document.getElementById("30Days").value;
  } else if (document.getElementById("7Days").checked) {
    getPaymentTerm = document.getElementById("7Days").value;
  }

  return getPaymentTerm;
}

function calculatePayRate() {
  var actualPRDay = calPayRate() - calPayRate() * calPaymentTerm();
  var actualPRMonth = actualPRDay * 20;
  document.getElementById("payRateDay").value = actualPRDay.toLocaleString(
    undefined,
    { minimumFractionDigits: 2 }
  );
  document.getElementById("payRateMonth").value = actualPRMonth.toLocaleString(
    undefined,
    { minimumFractionDigits: 2 }
  );
  if (calContractLength() == "") {
    document.getElementById(
      "totalPayRate"
    ).value = actualPRMonth.toLocaleString(undefined, {
      minimumFractionDigits: 2
    });
  } else {
    var actualTotalPR = actualPRMonth * calContractLength();
    document.getElementById(
      "totalPayRate"
    ).value = actualTotalPR.toLocaleString(undefined, {
      minimumFractionDigits: 2
    });
  }
}
</script>

<div class="container-temp mb-4">
    <div class="row">
        <div class="col-md-8">

            <p class="pb-3">
                <a href="{{ url()->previous() }}" title="Go Back" class="btnBack">
                    <!-- <img src="{{admin_asset('images/icon-back.png')}}" alt="" title="">-->
                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
        </div>
    </div>
    @if(Auth::check())
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                {{ Session::get('success') }}
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
                {{ Session::get('error') }}
            </div>
            @endif

            @php
            $is_applied = 0;
            @endphp
            @foreach ($job->associatecandidates as $key1=>$v)
            @if ($v->CANDIDATEID == Auth::user()->zoho_id)
            @if(!empty($v->status))

            @php
            $is_applied = 1;
            @endphp
            <div class="boxProgress">
                <h5>Job Application Progress</h5>
                <ul class="listProgress">
                    <?php
                    $ScreactiveClass = $SubactiveClass = $InteactiveClass = $StaactiveClass = $HireactiveClass = "disabled";
                    $ScreshowClass = $subshowClass = $InteshowClass = $StashowClass = $HireshowClass = "hide";
                    if ($v->CANDIDATEID == Auth::user()->zoho_id) {
                        if (in_array($v->status, $candidateScreeningStatus)) {
                            $ScreactiveClass = "active";
                            $ScreshowClass = "show";
                        }
                        if (in_array($v->status, $candidateSubmissionStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $subshowClass = "show";
                        }
                        if (in_array($v->status, $candidateInterviewStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $InteactiveClass = "active";
                            $InteshowClass = "show";
                        }
                        if (in_array($v->status, $candidateOfferStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $InteactiveClass = "active";
                            $StaactiveClass = "active";
                            $StashowClass = "show";
                        }
                        if (in_array($v->status, $candidateHireStatus)) {
                            $ScreactiveClass = "active";
                            $SubactiveClass = "active";
                            $InteactiveClass = "active";
                            $StaactiveClass = "active";
                            $HireactiveClass = "active";
                            $HireshowClass = "show";
                        }
                    }
                    ?>
                    <li class="{{$ScreactiveClass}}">
                        <span>Screening</span>
                        <span class="text-red text-block {{$ScreshowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$SubactiveClass}}">
                        <span>Submission</span>
                        <span class="text-red text-block {{$subshowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$InteactiveClass}}">
                        <span> Interview </span>
                        <span class="text-red text-block {{$InteshowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$StaactiveClass}}">
                        <span> Offer</span>
                        <span class="text-red text-block {{$StashowClass}}">{{$v->status}}</span>
                    </li>
                    <li class="{{$HireactiveClass}}">
                        <span> Hire</span>
                        <span class="text-red text-block {{$HireshowClass}}">{{$v->status}}</span>
                    </li>
                </ul>
            </div>
            @endif
            @endif
            @endforeach


        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <ul class="listContract">
                        <li>{{$job->Job_ID}}</li>
                        <li>{{$job->job_type}}</li>
                    </ul>
                    <h5 class="colorRed">{{$job->posting_title}}</h5>
                </div>
                <div class="col-md-6 text-right">
                    @if(Auth::check())
                    <!--Block 1-->
                    @if(Auth::user()->role_id == 2)

                    @php $is_applied = 0; $is_saved = 0; @endphp
                    <!--Block 2-->
                    @foreach ($job->associatecandidates as $key1=>$value1)
                    <!--Block 3-->
                    @if ($value1->CANDIDATEID == Auth::user()->zoho_id || $value1->candidate_id == Auth::user()->id)
                    <!--Block 4-->
                    @if(!empty($value1->status))

                    @php $is_applied = 1; @endphp
                    <div class="btnsRight">
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader ">
                        @if($value1->status == "Associated")
                        <a title="Withdraw Application" href="javascript:void(0)"
                            class="btn btn-danger btn-sm btnApply-white withdraw-application border-ra-5 "
                            data-zohoid="{{$value1->JOBOPENINGID}}" data-id="{{$value1->id}}">Withdraw Application</a>
                        @else
                        <a title="Withdrawn" href="javascript:void(0);"
                            class="border-ra-5  btn btn-danger btn-sm btnApply-white disabled">Withdrawn</a>
                        @endif
                    </div>

                    @endif
                    <!--End Block 4-->
                    @endif
                    <!--End Block 3-->
                    @endforeach
                    <!--End Block 2-->
                    @if($is_applied == 0)
                    <div class="btnsRight">
                        @foreach ($job->associatecandidates as $key1=>$value1)
                        @if ($value1->CANDIDATEID == Auth::user()->zoho_id)
                        @php
                        $is_saved = 1;
                        @endphp
                        <div class="btnsRight">
                            <a title="Unsave" href="javascript:void(0)"
                                class="btn btn-secondary btn-sm btn-save-icon icon-s-10 savejob-delete border-ra-5 "
                                data-zohoid="{{$value1->JOBOPENINGID}}">Unsave</a>
                            <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                            <a title="Apply Now!" href="javaScript:void(0)"
                                class="btn btn-danger btn-sm btnApply-white consultant-job-applied border-ra-5 "
                                data-zohoid="{{$value1->JOBOPENINGID}}" data-id="{{$value1->id}}">Apply Now!</a>
                        </div>
                        @endif
                        @endforeach
                        @if($is_saved == 0)
                        <a title="Save" href="JavaScript:void(0);"
                            class="btn btn-secondary btn-sm btn-save-icon icon-s-10 consultant-job-saved border-ra-5 "
                            data-zohoid="{{$job->JOBOPENINGID}}" data-id="{{$job->id}}">Save</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="consultant-img-loader">
                        <a title="Apply Now!" href="javaScript:void(0)"
                            class="btn btn-danger btn-sm btnApply-white consultant-job-applied border-ra-5 "
                            data-zohoid="{{$job->JOBOPENINGID}}" data-id="{{$job->id}}">Apply Now!</a>
                        @endif
                    </div>
                    @endif

                    @endif
                    <!--End Block 1-->
                    @else
                    <!-- <a title="Save" href="#" class="btn btn-secondary btn-md btnSave">Save</a> -->
                    <a title="Save" href="javascript:void(0)"
                        class="btn  btn-save-icon icon-s-10 btn-secondary btn-sm btn-big-wid save-icon general-apply-job border-ra-5"
                        data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Save</a>
                    <a title="Apply Now!" href="javascript:void(0)"
                        class="btn btn-danger btn-sm btn-big-wid general-apply-job border-ra-5 btnApply-white"
                        data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Apply
                        Now!</a>
                    @endif
                </div>
            </div>
            <div class="commanContent">
                <!-- <div class="row">
                    <div class="col-md-6">
                        <ul class="listContract">
                            <li>{{$job->Job_ID}}</li>
                            <li>{{$job->job_type}}</li>
                        </ul>
                        <h5>{{$job->posting_title}}</h5>
                    </div>
                    Only consultant can save/apply job 
                   
                </div> -->
                <div class="row rowContract">
                    @if(Auth::user())
                    <div class="col-md-4">
                        @if(Auth::user()->role_id == 2)
                        <p class="pb-2 color6Gray"><span>Base Rate</span></p>
                        <div class="cus-verticale-center">
                            <div class=" text-red">
                                <!-- <span class="cus-br-icon">BR</span> MYR  -->
                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                <span class="ml-1">MYR </span>
                                <span class="c-job-base-rate" id="con-copy-1">{{$job->job_base_rate}}/day</span>
                                <div class="iconBox iconCopy copy-to-clipboard job-tile-button-right d-inline m-l-10">
                                </div>
                            </div>

                        </div>
                        <!-- <div class=" text-red"><span class="cus-br-icon">BR</span> MYR {{$job->job_base_rate}}/day
                            <div class="d-inline iconBox iconCopy copy-to-clipboard ">

                            </div>
                        </div> -->
                        @endif
                    </div>
                    @else
                    <div class=" text-blue general-apply-job"
                        data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">
                        <p class="pb-2 color6Gray"><span>Base Rate</span></p>
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        Login as a Consultant to view
                    </div>
                    @endif
                </div>
                <div class="row rowContract mob-detai-res">
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Job Mode</span></p>
                        <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$job->job_mode}}</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Experience</span></p>
                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$job->work_experience}} exp.</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Availability Date</span></p>
                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{date('d M. Y',strtotime($job->date_opened))}}</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Job Duration</span></p>
                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$job->job_duration}} months</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Industry</span></p>
                        <img src="{{admin_asset('images/cus-oil.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$job->industry}}</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Location</span></p>
                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$job->city}}</div>
                    </div>
                </div>
                <div class="textAgo">
                    <span class="iconWeek">{{ time_elapsed_string($job->last_activity_time) }}</span>
                </div>
            </div>
            @if(isset($job['key_skills']) && $job['key_skills'] != '' )
            <div class="commanContent spacing20">
                <h2 class="headingBorder text-dark">Key Skills</h2>
                @php
                $skills = [];
                if(isset($job['key_skills'])){
                $skills = explode(",",$job['key_skills']);
                }
                @endphp
                <ul class="listSkills listSkillsBig">
                    @if(count($skills))
                    @foreach($skills as $skillValue)
                    <li>
                        <a href="javascript:void(0)">{{$skillValue}}</a>
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
            @endif
            <div class="commanContent spacing20 mb-3">
                <h2 class="headingBorder text-dark">Job Description</h2>
                @if(isset($job['all_description']))
                <div class="d-block ul-ml-15 job-desc-hsame">{!! $job['all_description'] !!}</div>
                <hr />
                @else
                <div class="d-block ul-ml-15 job-desc-hsame">No Job Description Listed</div>
                <hr />
                @endif
            </div>
            <?php
            $url = url()->current();
            $twitterUrl = 'http://twitter.com/share?text=' . $job->posting_title . '&url=' . $url;
            ?>
            <ul class="listSocialMedia listSocialMediaSmall mb-4">
                <li>
                    <a onClick="window.open('<?php echo $twitterUrl; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"
                        target="_parent" href="javascript: void(0)">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php echo $url; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"
                        target="_parent" href="javascript: void(0)">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a onClick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo  $job->posting_title; ?>%20Developer%20Network&source=LinkedIn','sharer','toolbar=0,status=0,width=548,height=325');"
                        target="_parent" href="javascript: void(0)">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <!--Only consultant can ask about job-->
            @if(Auth::user())
            @if(Auth::user()->role_id == 2)
            <div class="commanContent spacing20 mb-4">
                <h2 class="headingBorder text-dark">Ask
                    <img src="{{admin_asset('images/textUs.png')}}" alt="" title="" />about the job!</h2>
                {{Form::open(['route' => 'job-enquiry','id' => 'enquiry-form'])}}
                {{Form::hidden('job_link',Request::url())}};
                <div class="form-group">
                    <textarea name="content" class="form-control"></textarea>
                </div>
                <p class="text-right pb-0">
                    <input type="submit" value="Submit" class="btn btn-secondary btn-sm btn-submit" />
                    <img src="{{admin_asset('images/loader.svg')}}" id="enquiry-img-loader" style="display: none;">
                </p>
                {{Form::close()}}
            </div>
            @endif

            @endif
        </div>

       <!-- Start calculator -->
        <div class="col-md-4">
        	<h3 class="header-red-bg colorWhite text-center pd-5 how-much-left"> Calculate Your Earnings Sas</h3>
            <div class="calculatorContent text-center">
                <p class="pb-1 textcolor font-size">Click<img src="{{admin_asset('images/new-design/copy.png')}}"
                        class="copy-icon">to copy Pay Rate/day!</p>
                <div class="calculatorContentHowmuch">
                    <div class="cal-form">
                        <div class="d-flex justify-content-around">
                            <div class="p-1"> <label style="margin-left: -40px" for="billing-rate" class="col-5">Pay Rate/Day: </label></div>
                            <div class="p-4">
                                <input type="text" class="payRate" id="payRate" placeholder="MYR" maxlength="5" onkeyup="calculatePayRate()"
              />
                                <!-- <input type="text" name="prDay" id="prDay" maxlength="5" class="position-right" placeholder="MYR"> -->
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="row calculator-font">
                    <div class="col-6">
                        <p class="calculatorTitle text-center">
                            Contract Length
                        </p>
                        <div class="d-flex justify-content-center">
                            <div class="p-1"> 
                                <!-- <input type="number" name="cLength" id="cLength" maxlength="2" class="months"> -->
                                <input type="text" name="cLength" id="contractLength" class="months" maxlength="2" onkeyup="calculatePayRate()"/>
                                </div>
                            <div class="p-1">
                                <p class="px-2 calculatorDesc1"> Month(s)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <p class="calculatorTitle text-center">
                            Payment Term
                        </p>
                        <div class="d-flex justify-content-left">
                            <ul class="listCheckbox" >
                            <li>
                                <!-- <label class='radiolabel'><input type="radio"  name="selectedPaymentTerms" value="backToBack" onclick="calculateTotal()" />Back to Back</label> -->
                                <label class="radiolabel"><input type="radio" name="selectedPaymentTerm" id="backToBack" value="0" onclick="calculatePayRate()" checked /> Back to Back</label >
                            </li>
                            <li>
                                <!-- <label class='radiolabel'><input type="radio"  name="selectedPaymentTerms" value="30Days" onclick="calculateTotal()" />30 Days</label>-->
                                <label class="radiolabel" ><input type="radio" name="selectedPaymentTerm" id="30Days" value="0.1" onclick=" calculatePayRate()" /> 30Days</label >                        
                            </li>
                            <li>
                                <!-- <label class='radiolabel'><input type="radio"  name="selectedPaymentTerms" value="7Days" onclick="calculateTotal()" />7 Days</label> -->
                                <label class="radiolabel" ><input type="radio" name="selectedPaymentTerm" id="7Days" value="0.2" onclick="calculatePayRate()" /> 7 Days </label>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="boxResult">
                <div class="d-flex justify-content-around">
                    <div class="p-1">
                        <p class="calculatorTitle">Pay Rate/day</p>
                    </div>
                    <div class="p-1">
                        <p class=" calculatorDesc">MYR</p>
                    </div>
                    <div class="p-1">
                        <!-- <input type="text" name="prdResult" id="prdResult" readonly /> -->
                        <input class="payRateDay" id="payRateDay" value="0" />
                    </div>
                </div>
                <div class="d-flex justify-content-around">
                    <div class="p-1">
                        <p class="calculatorTitle">Pay Rate/month</p>
                    </div>
                    <div class="p-1">
                        <p class=" calculatorDesc">MYR</p>
                    </div>
                    <div class="p-1">
                        <!-- <input type="text" name="prdMonth" id="prdMonth" readonly /> -->
                        <input class="payRateMonth" id="payRateMonth" value="0" />
                    </div>
                </div>
                <div class="d-flex justify-content-around space-bottom">
                    <div class="p-1">
                        <p class="calculatorTitle">Total Pay Rate</p>
                    </div>
                    <div class="p-1">
                        <p class="lr calculatorDesc">MYR</p>
                    </div>
                    <div class="p-1">
                        <!-- <input type="text" name="prdTotal" id="prdTotal" readonly /> -->
                        <input class="totalPayRate" id="totalPayRate" value="0" />
                        </p>
                    </div>
                </div>
                <p class="text-center calculator-font"> Based on 20 mandays/month. Numbers shown are just for estimation
                    purpose only.</p>
            </div>
            <!-- <h3 class="header-red-bg colorWhite text-center pd-5 how-much-left">How Much Will You Get?</h3>
            <div class="commanContent boxHowmuch">
                <div class="contentHowmuch">
                    <p class="pb-1">Enter the Job’s Base Rate</p>
                    <div class=" text-red inlineText">
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                    </div>
                    <input type="text" class="text-red text-right" value="" id="c1-base-rate-perday" maxlength="10" />
                    <span class="text-red">/day</span>
                </div>
                <div class="row f-12 rs-5">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Length of contract
                            <a href="#" class="iconInfo">
                                <span class="tooltiptext">Calculated based on 20 mandays/month. Surcharge of 5% is applicable for 3-month period and 3% discount will be given for 12-month period.</span>
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5
                                months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="2" title="6-11 months"
                                    checked="" /> 6-11 months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                                months</li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <p class="pb-2">Fees</p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                    checked="" disabled="" /> Admin fee
                                <a href="#" class="iconInfo">
                                    <span class="tooltiptext1">For talent management, onboarding process, payroll hosting, basic administrations and other related handling process.</span>
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                            <li>
                                <input type="checkbox" class="fees" name="guaranteed_payment"
                                    title="Guaranteed on-time payment" id="guaranteed_payment" checked="" /> Guaranteed
                                on-time payment
                                <a href="#" class="iconInfo">
                                    <span class="tooltiptext1">Guarantee 30-day payment term.</span>
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="boxDoyouKnow mb-0">
                    <h6 class="f-14 mb-0">Your Daily Rate Will Be</h6>
                    <h2 class="text-red" id="daily-rate">MYR 0.00</h2>
                </div>
            </div> -->
            @include('Layouts.General.doyouknow')
            @include('Layouts.General.tips')
        </div>
    </div>
</div>
<!-- <section class="contributesection">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="htagf">Want to contribute?</h3>
                <p class="ptagf">Feel free to drop us an email</p>
                <div class="btntagf"><a href="{{route('contactus')}}">Contact Us</a></div>
            </div>
        </div>
    </div>
</section> -->
<?php

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>
@endsection