@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title">
                    <h5>Job Request </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['jobRequestedProfile'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group cus-refresh">
                                <a href="{{route('jobRequestedProfile')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}

                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button> 
                                </span>

                            </div>


                            <div class="input-group width25">
                                {{Form::select('consultant_id',[''=>'Select Consultant']+$consultantarray,old('consultant_id'),['class'=>'input-sm form-control'])}}
                            </div>
                            <div class="input-group width25">
                                {{Form::select('client_id',[''=>'Select Client']+$clientarray,old('client_id'),['class'=>'input-sm form-control'])}}
                            </div>

                        </div>
                        {{Form::close()}} 
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Job Id </th>
                                    <th>Client Name</th>
                                    <th>Consultant Name</th>
                                    <th>Post Title </th>
                                    <th>Job Type </th>
                                    <th>Job Mode</th>
                                    <th>City</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($jobs))
                                @foreach($jobs as $row)

                                <tr>
                                    <td>{{ $row->Job_ID ? $row->Job_ID : '-'}}</td>
                                    <td>
                                        @if($row->client != null)
                                        <a href="{{route('clients.index',['clientid' => $row->client->CLIENTID])}}">{{$row->client->client_name}}</a>
                                        @else
                                        {{"-"}}
                                        @endif
                                    </td>
                                     <td>
                                        <?php
                                       
                                        if (!empty($row->associatecandidates) ) {
                                            
                                            $can = array();
                                            foreach ($row->associatecandidates as $val) {
                                                if (!empty($val)) {
                                                    $first_name = $last_name= '';
                                                    if(!empty($val->consultant->first_name)){
                                                        $first_name = $val->consultant->first_name;
                                                    }
                                                    if(!empty($val->consultant->last_name)){
                                                        $last_name = $val->consultant->last_name;
                                                    }
                                                    $can[] = $first_name . ' ' . $last_name;
                                                }
                                            }
                                            if (count($can)) {
                                                $commaList = implode(', ', $can);   
                                                if (count($commaList)) {
                                                    echo $commaList;
                                                } else {
                                                    echo '-';
                                                }
                                            }else{
                                                echo '-';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="{{route('jobs.index',['jobid' => $row->id])}}">{{ $row->posting_title ? $row->posting_title : '-'}}</a>
                                    </td>
                                    <td>{{ $row->job_type ? $row->job_type : '-'}}</td>
                                    <td>{{$row->job_mode ? $row->job_mode : '-'}}</td>
                                    <td>{{$row->city ? $row->city : '-'}}</td>

                                    <?php
                                    $color = '';
                                    if ($row->job_opening_status == 'On-Hold') {
                                        $color = 'btn-warning';
                                    } elseif ($row->job_opening_status == 'Filled') {
                                        $color = 'btn-info';
                                    } else {
                                        $color = 'btn-danger';
                                    }

                                    if ($row->job_opening_status == 'In-progress') {
                                        $status = 'Active';
                                    } else {
                                        $status = $row->job_opening_status;
                                    }
                                    ?>
                                    <?php if ($row->job_opening_status == 'In-progress') { ?>
                                        <td class="status-btn">
                                            <button class="btn btn-xs btn-primary" data-id="{{$row->id}}">{{$status}}</button>
                                        </td>
                                    <?php } elseif ($row->job_opening_status == 'Inactive' || $row->job_opening_status == 'Pending') { ?>
                                        <td class="status-btn">
                                            <button class="ibtn btn-xs btn-danger" data-id="{{$row->id}}">{{$status}}</button>
                                        </td>
                                    <?php } else { ?>
                                        <td class="status-btn">
                                            <button class="btn btn-xs {{$color}}" data-id="{{$row->id}}">{{$status}}</button>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <a href="{{route('jobs.edit',[$row->id])}}" data-id="{{$row->id}}" class="cus-icon" title="Edit Job"><i class="fa fa-pencil"></i></a>
                                        @if($row->job_opening_status=='Inactive' || $row->job_opening_status=='Pending')
                                        <a href="javascript:void(0)" data-id="{{$row->id}}" class="cus-icon job-delete" title="Delete Job"><i class="fa fa-trash"></i></a>
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($jobs))
                                <tr><td colspan="9"><h4 class="text-center">No Records Found</h4></td></tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($jobs)){!! $jobs->render() !!}@endif 
                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')