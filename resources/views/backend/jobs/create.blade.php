@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title pagetitle">

                    <div class="row">
                        <div class="col-md-9">
                            <h5>Create a new job</h5>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" action="{{route('jobs.store')}}" id="client-job-post">
                        {{csrf_field()}}
                        <input type="text" value="{{ $contactId }}" name="contactId" hidden/>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Job Title</label>
                                    <input name="job_title" type="text" class="form-control"
                                        placeholder="Enter job title">
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="row rs-5">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">Daily Budget</label>
                                            <div class="input-group formborder-left">
                                                <span class="input-group-addon ">MYR</span>
                                                <!-- <div class="textMyr sticky-image">MYR</div> -->
                                                <input name="budget" type="text" class="form-control inputMyr"
                                                    id="job-budget" maxlength="10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Experience Required (Years)</label>
                                    {{Form::select('experiece',[null => '--Select Experience--'] + $experienceList1,'',['class' => 'form-control select2','required'])}}
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Industry</label>
                                    {{Form::select('industry',[null => '--Select Industry--'] + $industryList,'',['class' => 'form-control select2'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="row rs-5">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">Start Date</label>
                                            <div class="input-group formborder-right">
                                                <!-- <div class="textMyr sticky-image">MYR</div> -->
                                                <input name="start_date" id="job-start-date" type="text"
                                                    class="form-control" readonly=""
                                                    placeholder="Select Start Date">
                                                <span class="input-group-addon "><i
                                                        class="fa fa-calendar colorSkyBlue"
                                                        aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">Duration</label>
                                            <input name="duration" type="text" class="form-control" maxlength="10"
                                                placeholder="Enter month(s)" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="row rs-5">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">State</label>
                                            {{Form::select('state',[null => '--Select State--'] + $stateList,'',['class' => 'form-control select2','id' => 'job-state','required' => true])}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">City</label>
                                            {{Form::select('city',[null => '--Select City--'],'',['class' => 'form-control select2','id' => 'job-city','required'=> true])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="row rs-5">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">Job Type</label>
                                            {{Form::select('job_type',[null => '--Select Job Type--'] + $jobTypeList,'',['class' => 'form-control select2'])}}
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="mb-2 colorBlue f-14">Job Mode</label>
                                            {{Form::select('job_mode',[null => '--Select Job Mode--'] + $jobModeList,'',['class' => 'form-control select2'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Categories</label>
                                    <!-- {{Form::select('category',[null => '--Select Category--'] + $jobCategoryList,'',['class' => 'form-control select2'])}} -->
                                    {{Form::select('category[]',$jobCategoryList,'',['class'=>'form-control select2','id' => 'candidate-category','multiple' => true])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">


                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Key Skills</label>
                                    <!-- <textarea name="key_skills" class="form-control textareaSmall" placeholder="Please enter comma separated key skills. e.g Node, Angular, Php etc..."></textarea> -->
                                    <input data-skill="" data-role="tagsinput" name="key_skills" class="form-control"
                                        placeholder="Enter key skills">
                                    <small class="colorBlue mt-2">Separate the skill with comma</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Job Description</label>
                                    <textarea name="job_description" class="form-control textareaSmall"
                                        id="job_description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Requirements</label>
                                    <textarea name="requirements" class="form-control textareaSmall"
                                        id="job_requirements"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <label class="mb-2 colorBlue f-14">Benefits</label>
                                    <textarea name="benefits" class="form-control textareaSmall"
                                        id="job_benefits"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <p class="colorBlue pb-4 pt-2">You can add notes once the job has been approved,
                                    isn’t that good?</p>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <p class="text-right pb-0">
                                    <input type="submit" value="Submit"
                                        class="btn form-submit-btn btn-sm btn-submit">
                                </p>
                            </div>
                        </div>
                    </form>
                    
                    {{-- {{Form::open(['route' => ['consultant.store'],'class'=>'system_user','id'=>'system_user', 'enctype' => 'multipart/form-data'])}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name: <span class="input-required">*</span></label>
                                    {{Form::text('first_name',old('first_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name: <span class="input-required">*</span></label>
                                    {{Form::text('last_name',old('last_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address: <span class="input-required">*</span></label>
                                    {{Form::email('email',old('email'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number: <span class="input-required">*</span></label>
                                    {{Form::text('phone',old('phone'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>SAP Job Title: <span class="input-required">*</span></label>
                                    {{Form::text('sap_job_title',old('sap_job_title'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>CV: <span class="input-required">*</span></label>
                                    {{Form::file('cv',old('cv'),['class'=>'form-control m-input','required'])}}
                                    <p><small>Please select only docx or pdf and select file size below 2 mb</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('systemuser.index')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}} --}}
                </div>
            </div>
        </div>
    </div>
    <!--End Add-->
    @include('Layouts.backend.foot')
</div>
@endsection('content')