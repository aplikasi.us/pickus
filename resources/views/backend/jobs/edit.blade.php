@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                {!! Form::model($job, ['method' => 'put','url' => url('/admin/jobs/update'),'id' =>
                'admin-job-approve']) !!}
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title">
                    <h5>Edit Job</h5>
                </div>
                <div class="ibox-content">

                    {{ Form::hidden('id', null) }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Job Title: <span class="input-required">*</span></label>
                                    {{Form::text('job_title',$job->posting_title,['class'=>'form-control m-input','required','maxlength' => 70])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Budget: <span class="input-required">*</span></label>
                                    {{Form::text('budget',$job->job_base_rate,['class'=>'form-control m-input','required','maxlength' => 10,'id' => 'job-budget'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Calculated Base Rate: <span class="input-required">*</span></label>
                                    {{Form::text('baserate',number_format($job->job_base_rate / 1.07,2, '.', ''),['class'=>'form-control m-input','required','maxlength' => 10,'readonly','id' => 'job-calculated-baserate'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Experience Required (Years): <span class="input-required">*</span></label>
                                    {{Form::select('experiece',$experienceList,$job->work_experience,['class' => 'form-control select2','required'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Job Type: <span class="input-required">*</span></label>
                                    {{Form::select('job_type',[null => "--Select Job Type--"] + $jobTypeList,$job->job_type,['class' => 'form-control select2'])}}

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Job_mode: <span class="input-required">*</span></label>
                                    {{Form::select('job_mode',$jobModeList,$job->job_mode,['class' => 'form-control select2'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Start Date: <span class="input-required">*</span></label>
                                    {{Form::text('start_date',$job->date_opened,['class'=>'form-control m-input','required','id' => 'job-start-date','autocomplete' => 'off','readonly' => true])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Duration: <span class="input-required">*</span></label>
                                    {{Form::text('duration',$job->job_duration,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Industry: <span class="input-required">*</span></label>
                                    {{Form::select('industry',[null => '--Select Industry--'] + $industryList,$job->industry,['class' => 'form-control','id' => 'industry'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>State: <span class="input-required">*</span></label>
                                    @php
                                    $state_id = "";
                                    if(count($state)){
                                    $state_id = $state->id;
                                    }
                                    @endphp
                                    {{Form::select('state',[null => '--Select State--'] + $stateList,$state_id,['class' => 'form-control select2','id' => 'job-state','required' => true])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>City: <span class="input-required">*</span></label>
                                    @if($city !="")
                                    {{Form::select('city',[null => '--Select City--'] + $city,$job->city,['class' => 'form-control select2','id' => 'job-city','required'=> true])}}
                                    @else
                                    {{Form::select('city',[null => '--Select City--'],$job->city,['class' => 'form-control select2','id' => 'job-city','required'=> true])}}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Categories: <span class="input-required">*</span></label>
                                    <!-- {{Form::select('category',[null => '--Select Category--'] + $jobCategoryList,$job->category,['class' => 'form-control select2'])}} -->
                                    {{Form::select('category[]',$categories,explode(',',$job->category),['class'=>'form-control','id' => 'candidate-category','multiple' => true])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Key Skills: <span class="input-required">*</span></label>
                                    {{Form::textarea('key_skills',$job->key_skills,['class'=>'form-control m-input','rows' => 2])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status: <span class="input-required">*</span></label>
                                    {{Form::select('job_opening_status',['Pending' => 'Pending','In-progress' => 'In-progress','Filled' => 'Filled','Cancelled' => 'Cancelled','Declined' => 'Declined','Inactive' => 'Inactive','Submitted by client' => 'Submitted by client'],$job->job_opening_status,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Job Description:</label>
                                {{Form::textarea('job_description',$job->job_description,['class'=>'form-control m-input','rows' => 2,'id' => 'edit_job_description'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Requirements:</label>
                                {{Form::textarea('requirements',$job->job_requirements,['class'=>'form-control m-input','rows' => 2,'id' => 'edit_job_requirements'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Benefits:</label>
                                {{Form::textarea('benefits',$job->job_benefits,['class'=>'form-control m-input','rows' => 2,'id' => 'edit_job_benefits'])}}
                                <!-- @ckeditor('job_benefits') -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <a href="{{route('jobs.index')}}" class="btn btn-white">Cancel</a>
                                <button class="btn btn-primary" type="submit">Update</button>
                                @if($job->is_approve_by_admin == 0)
                                <a href="{{route('approve-job',[$job->id])}}" class="btn btn-warning" type="button"
                                    id="approve-job">Approve</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{Form::close()}}
        </div>

    </div>

</div>
@include('Layouts.backend.foot')
</div>

@endsection('content')

@section('javascript')
<script type="text/javascript">
CKEDITOR.replace("edit_job_description");
CKEDITOR.replace("edit_job_requirements");
CKEDITOR.replace("edit_job_benefits");
</script>
@endsection('javascript')