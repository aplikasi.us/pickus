@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-9">
                            <h5>Job Openings </h5>
                        </div>
                        {{-- <div class="col-md-3 pull-right">
                                <a href="{{route('jobs.create')}}" type="button" class="btn btn-outline btn-primary pull-right">Create</a>
                        </div> --}}
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['jobs.index'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group cus-refresh">
                                <a href="{{route('jobs.index')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width20">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}

                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button> 
                                </span>

                            </div>
                            <div class="input-group width20">
                                {{Form::select('status',[''=>'Select Status']+$statusArray,old('consultant_id'),['class'=>'input-sm form-control'])}}
                            </div>

                            <div class="input-group width20">
                                {{Form::select('consultant_id',[''=>'Select Consultant']+$consultantarray,old('consultant_id'),['class'=>'input-sm form-control'])}}
                            </div>
                            <div class="input-group width20">
                                {{Form::select('client_id',[''=>'Select Client']+$clientarray,old('client_id'),['class'=>'input-sm form-control'])}}
                            </div>

                        </div>
                        {{Form::close()}} 
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Job Id </th>
                                    <th>Client Name</th>
                                    
                                    <th>Posting Title </th>
                                    <th>Job Type </th>
                                    <th>Job Mode</th>
                                    <th>City</th>
                                    <th>Associated Candidates</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($jobs))
                                @foreach($jobs as $row)

                                <tr>
                                    <td>{{ $row->Job_ID ? $row->Job_ID : '-'}}</td>
                                    <td>
                                    @if($row->client != null)
                                    <a href="{{route('clients.index',['clientid' => $row->client->CLIENTID])}}">{{$row->client->client_name}}</a>
                                    @else
                                    {{"-"}}
                                    @endif
                                    </td>
                                     
                                    <td>{{ $row->posting_title ? $row->posting_title : '-'}}</td>
                                    <td>{{ $row->job_type ? $row->job_type : '-'}}</td>
                                    <td>{{$row->job_mode ? $row->job_mode : '-'}}</td>
                                    <td>{{$row->city ? $row->city : '-'}}</td>
                                    <td>
                                        @if($row->associatecandidates != null)
                                        <ul class="candidate-ul">
                                            @foreach ($row->associatecandidates as $k => $val)
                                            @if($val->consultant != null)
                                                @if($k < 3)
                                                <li>{{$val->consultant->first_name .' '.$val->consultant->last_name}}</li>
                                                @else
                                                <li class="candidate-li hide">{{$val->consultant->first_name .' '.$val->consultant->last_name}}</li>
                                                @endif
                                            @endif
                                            @endforeach
                                        </ul>
                                        @if(count($row->associatecandidates) > 3)
                                        <span class="more-less-link"><a href="javascript:void(0)" class="view-more-associated-candidate">View More</a></span>
                                        @endif
                                        @else
                                        {{"-"}}
                                        @endif
                                    </td>
                                    <?php
                                    $color = '';
                                    if ($row->job_opening_status == 'On-Hold') {
                                        $color = 'btn-warning';
                                    } elseif ($row->job_opening_status == 'Filled') {
                                        $color = 'btn-info';
                                    } else {
                                        $color = 'btn-danger';
                                    }

                                    if($row->job_opening_status == 'Pending'){
                                        $btnClass = 'btn-warning';
                                    } else {
                                        $btnClass = 'btn-danger';
                                    }
                                    if ($row->job_opening_status == 'In-progress') {
                                        $status = 'Active';
                                    } else {
                                        $status = $row->job_opening_status;
                                    }
                                    ?>
                                    <?php if ($row->job_opening_status == 'In-progress') { ?>
                                        <td class="status-btn">
                                            <button class="btn btn-xs btn-primary" data-id="{{$row->id}}">{{$status}}</button>
                                        </td>
                                    <?php } elseif ($row->job_opening_status == 'Inactive' || $row->job_opening_status == 'Pending') { ?>
                                        <td class="status-btn">
                                            <button class="btn btn-xs {{$btnClass}}" data-id="{{$row->id}}">{{$status}}</button>
                                        </td>
                                    <?php } else { ?>
                                        <td class="status-btn">
                                            <button class="btn btn-xs {{$color}}" data-id="{{$row->id}}">{{$status}}</button>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <a href="{{route('jobs.edit',[$row->id])}}" data-id="{{$row->id}}" class="cus-icon" title="Edit Job"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" data-id="{{$row->id}}" class="cus-icon job-delete" title="Delete Job"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($jobs))
                                <tr><td colspan="9"><h4 class="text-center">No Records Found</h4></td></tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($jobs)){!! $jobs->render() !!}@endif 
                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')