@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title pagetitle">
                    <div class="row">
                        <div class="col-md-9">
                            <h5>Clients </h5>
                        </div>
                        <div class="col-lg-2 pull-right">
                            <a href="{{route('clients.create')}}" type="button" class="btn btn-outline btn-primary pull-right">Create</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['clients.index'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group cus-refresh">
                                <a href="{{route('clients.index')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                </span>
                            </div>
                            <div class="input-group width25">
                                {{Form::select('account_status',[null => 'Select Account Status',1 => "Active",2 => "Inactive"],'',['class'=>'form-control m-input'])}}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped" id="client-table">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Contact Name</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Account Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($client))
                                @foreach($client as $key => $row)
                                <tr>
                                    <td>
                                        {{ ($row->client_name) ? $row->client_name : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->first_name) ? $row->first_name : "-"}}
                                        {{ ($row->last_name) ? $row->last_name : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->email) ? $row->email : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->mobile) ? $row->mobile : "-"}}
                                    </td>
                                    <td>
                                        @if($row->is_delete == 0)
                                        <button class="btn btn-xs btn-primary client-active-btn" data-status="inactive"
                                            data-id="{{$row->CONTACTID}}">Active</button>
                                        @else
                                        <button class="btn btn-xs btn-danger client-active-btn" data-status="active"
                                            data-id="{{$row->CONTACTID}}">Inactive</button>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group table-group-button">
                                            <button data-toggle="dropdown"
                                                class="btn btn-primary btn-sm dropdown-toggle"
                                                aria-expanded="false">Action <span class="caret"></span></button>
                                            <ul class="dropdown-menu  dropdown-menu-right">
                                                <li>
                                                    <a href="{{route('jobs.index')}}?client_id={{$row->CONTACTID}}"
                                                        class="btn btn-xs">View Job</a>
                                                </li>
                                                <li>
                                                <a href="{{ route('job_create', ['id' => $row->CONTACTID]) }}"
                                                        class="btn btn-xs">Create Job</a>
                                                </li>
                                                @if(count($ClientRequested))
                                                @if(in_array($row->CLIENTID,$ClientRequested))
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('indexRequestProfile',['client_id' => $row->CONTACTID])}}"
                                                        class="btn btn-xs ">View Requested Profile</a>
                                                </li>
                                                @endif
                                                @endif
                                                @if($row->user != null)
                                                @if($row->user->password == null)
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('changecontactpassword',[$row->CONTACTID])}}"
                                                        title="Generate Password"
                                                        class="contact-generate-password-link">Generate Password</a>
                                                </li>
                                                @endif
                                                @endif
                                                @if($row->is_delete == 1)
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('restore-clientcontact-account',$row->CONTACTID)}}"
                                                        class="btn btn-xs restore-account">Restore Account</a>
                                                </li>
                                                @endif
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('deleteclient',$row->CONTACTID)}}"
                                                        class="btn btn-xs delete-account">Delete Account</a>
                                                </li>
                                            </ul>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($client))
                                <tr>
                                    <td colspan="7">
                                        <h4 class="text-center">No Records Found</h4>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($client)){!! $client->render() !!}@endif

                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--CONFIRM PASSWORD MODAL-->
    <div class="modal inmodal" id="verify-password-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">

                {!! Form::open(['url'=>'admin/verifrypassword','id'=>'verify-password'])!!}
                <input type="hidden" name="type" value="delete-clientcontact-account" id="action-type" />
                <input type="hidden" name="redirect_url" value="" id="redirect_url" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Verify Your Password</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissable" id="error-message" style="display: none;">
                        <span>You have enter wrong password</span>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">Enter Password: <span class="input-required">*</span></label>
                        </div>
                        <div class="col-md-8">
                            {{ Form::password('password',['class' => 'form-control','placeholder' => 'Enter password']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submit-button">Verify Now</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--End Add-->
    @include('Layouts.backend.foot')
</div>
@endsection('content')