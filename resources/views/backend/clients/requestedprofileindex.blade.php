@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title">
                    <h5>Request Profiles</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['indexRequestProfile'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group cus-refresh">
                                <a href="{{route('indexRequestProfile')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::select('status',[''=>'Select Status']+$requeststatus,old('status'),['class'=>'input-sm form-control'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                </span>

                            </div>


                            <div class="input-group width25">
                                {{Form::select('consultant_id',[''=>'Select Consultant']+$consultantarray,old('consultant_id'),['class'=>'input-sm form-control'])}}
                            </div>
                            <div class="input-group width25">
                                {{Form::select('client_id',[''=>'Select Company']+$clientarray,old('client_id'),['class'=>'input-sm form-control'])}}
                            </div>

                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="Javascript:void(0)" type="button"
                                class=" btn btn- btn-outline btn-primary i-check-show multiple-cs-download">Download
                                cv</a>
                            <a href="{{route('multipledeleteRequestedprofile',['replaceId'])}}" type="button"
                                class=" btn btn- btn-outline btn-danger i-check-show multiple-delete">Delete</a>
                        </div>
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="i-checks " name="select_all" value="1"
                                            id="example-select-all" type="checkbox" /></th>
                                    <th>Company Name</th>
                                    <th>Consultant Name </th>
                                    <!-- <th>Consultant ID </th> -->
                                    <th>Request status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($clientRequested))
                                @foreach($clientRequested as $key => $value)
                                <tr>
                                    <td class="table-data">
                                        <input type="checkbox" class="i-checks checkall i-event-show" id="sub_chk"
                                            data-id='{{$value->id}}' name="id[]" value='{{$value->id}}'>
                                    </td>
                                    <td>{{ isset($value->client) ? $value->client->client_name  : '-'}}</td>
                                    <td>{{ isset($value->candidate) ? $value->candidate->first_name  : '-'}}
                                        {{ isset($value->candidate) ? $value->candidate->last_name  : ''}}</td>

                                    <td>
                                        {{$value->request_status}}
                                    </td>
                                    <td>
                                        <div class="btn-group table-group-button">
                                            <button data-toggle="dropdown"
                                                class="btn btn-primary btn-sm dropdown-toggle"
                                                aria-expanded="false">Action <span class="caret"></span></button>
                                            <ul class="dropdown-menu  dropdown-menu-right">
                                                @if($value->request_status == "New")
                                                <li>
                                                    <a class="btn btn-xs btn- btnapproveummary"
                                                        href="{{route('approvprofileadmin',[$value->id])}}"
                                                        class="font-bold">Approve</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a class="btn btn-xs btn- btncancelled"
                                                        href="{{route('cancelprofileadmin',[$value->id])}}"
                                                        class="font-bold">Cancel</a>
                                                </li>
                                                @endif
                                                @if($value->request_status == "Approved")
                                                <li>
                                                    <a href="{{route('formattedResumeDownload',[$value->CANDIDATEID])}}"
                                                        class="btn btn-xs ">Download CV</a>
                                                </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($clientRequested))
                                <tr>
                                    <td colspan="5">
                                        <h4 class="text-center">No Records Found</h4>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($clientRequested)){!! $clientRequested->render() !!}@endif
                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')