@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title pagetitle">

                    <div class="row">
                        <div class="col-md-9">
                            <h5>Create a new client</h5>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">

                    {{Form::open(['route' => ['clients.store'],'class'=>'system_user','id'=>'system_user'])}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name: <span class="input-required">*</span></label>
                                    {{Form::text('first_name',old('first_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name: <span class="input-required">*</span></label>
                                    {{Form::text('last_name',old('last_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address: <span class="input-required">*</span></label>
                                    {{Form::email('email',old('email'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Company Name: <span class="input-required">*</span></label>
                                    {{Form::text('company_name',old('company_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number: <span class="input-required">*</span></label>
                                    {{Form::text('phone',old('phone'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Job Title: <span class="input-required">*</span></label>
                                    {{Form::text('job_title',old('job_title'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('systemuser.index')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    <!--End Add-->
    @include('Layouts.backend.foot')
</div>
@endsection('content')