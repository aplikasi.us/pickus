@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title">
                    <h5>Request view</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Consultant name </th>
                                    <th>Consultant ID </th>
                                    <th>Request status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($clientRequested))
                                @foreach($clientRequested as $key => $value)
                                <tr>
                                    <td>{{ $value->candidate->first_name ? $value->candidate->first_name  : '-'}}</td>
                                    <td><a href="{{route('consultant-detail',[\Crypt::encryptString($value->CANDIDATEID)])}}" class="">{{ $value->CANDIDATEID}}</a></td>
                                    <td>
                                        @if($value->is_request_approve_by_admin == 0)
                                            Pending
                                        @else
                                            Approved
                                        @endif
                                    </td>
                                    <td>

                                        <div class="btn-group table-group-button">
                                                @if($value->is_request_approve_by_admin == 0)
                                                    <a class="btn btn-xs btn-primary btnapproveummary" href="{{route('approvprofileadmin',[$value->id])}}" class="font-bold">Approved</a>
                                                @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($clientRequested))
                                <tr><td colspan="3"><h4 class="text-center">No Records Found</h4></td></tr>
                                @endif
                            </tbody>
                        </table>
                       @if(isset($clientRequested)){!! $clientRequested->render() !!}@endif
                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')