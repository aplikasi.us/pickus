@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title">
                    <h5>Clients </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['clients.index'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group cus-refresh">
                                <a href="{{route('clients.index')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button> 
                                </span>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped" id="client-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Revenue Type</th>
                                    <th>Status</th>
                                    <th>Currency</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($client))
                                @foreach($client as $key => $row)
                                <tr>
                                    <td>
                                        <a data-toggle="collapse" data-target="#collapse-{{$key}}" class="accordion-toggle collapsed">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                    </td>
                                    <td>
                                        {{ ($row->client_name) ? $row->client_name : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->email) ? $row->email : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->contact_number) ? $row->contact_number : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->revenue_type) ? $row->revenue_type : "-"}}
                                    </td>
                                    <td>
                                        @if($row->status == 'Active')
                                        <button class="btn btn-xs btn-primary">Active</button>
                                        @else
                                        <button class="btn btn-xs btn-danger">Inactive</button>
                                        @endif
                                    </td>
                                    <td>
                                        {{ ($row->currency) ? $row->currency : "-"}}
                                    </td>
                                    <td>
                                        <div class="btn-group table-group-button">
                                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></button>
                                            <ul class="dropdown-menu  dropdown-menu-right">
                                                <li>
                                                    <a href="{{route('jobs.index')}}?client_id={{$row->CLIENTID}}" class="btn btn-xs" >View Job</a>
                                                </li>
                                                @if(count($ClientRequested))
                                                @if(in_array($row->CLIENTID,$ClientRequested))
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('viewRequestProfile',$row->CLIENTID)}}" class="btn btn-xs " >View Requested Profile</a>
                                                </li>
                                                @endif
                                                @endif
<!--                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('deleteclient',$row->CLIENTID)}}" class="btn btn-xs delete-client" >Delete</a>
                                                </li>-->
                                            </ul>
                                        </div>

                                    </td>
                                </tr>
                                <tr id="collapse-{{$key}}" class="accordian-body collapse">
                                    <td colspan="8">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile Number</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            @if(count($row->contacts))
                                            <tbody>
                                                @foreach($row->contacts as $key1 => $value1)
                                                <tr>
                                                    <td>{{$value1->first_name}}</td>
                                                    <td>{{$value1->last_name}}</td>
                                                    <td>{{$value1->email}}</td>
                                                    <td>{{$value1->mobile}}</td>
                                                    <td>
                                                        <a href="{{route('changecontactpassword',[$value1->CONTACTID])}}" title="Generate Password" class="contact-generate-password-link btn btn-danger">Generate Password</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            @else
                                            <tr>
                                                <td class="text-center text-red" colspan="8">No contacts found</td>
                                            </tr>
                                            @endif
                                        </table>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($client))
                                <tr><td colspan="7"><h4 class="text-center">No Records Found</h4></td></tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($client)){!! $client->render() !!}@endif

                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>
@endsection('content')
