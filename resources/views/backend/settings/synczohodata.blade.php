@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">  
    @include('Layouts.backend.header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h3> Sync Zoho Data</h3>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    {{ Form::open(['route' => ['synczohodata'], 'method' => 'POST', 'id'=>'syncform','class' => 'form-horizontal']) }}
                    <div class="row">
<!--                        <div class="col-md-12">
                            <div class="parent-div">
                                <div class="i-checks"><label> <input type="checkbox" name="clients" value="clients" id="clients" class="parent"> <i></i> Clients </label></div>
                                <div class="form-group" style="padding-left: 30px;">
                                    <div class="col-sm-12">
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="client_profile" value="profile" id="client_profie" class="childs"> Profile </label> 
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="client_job_openings" value="job" id="client_jobs" class="childs"> Job Openings </label> 
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="client_interview" value="interviews" id="client_interviews" class="childs"> Interviews </label> 
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="parent-div">
                                <div class="i-checks"><label> <input type="checkbox"  name="candidates" value="candidates" class="parent"> <i></i> Candidates </label></div>
                                <div class="form-group" style="padding-left: 30px;">
                                    <div class="col-sm-12">
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="candidate_profile" value="profile" id="candidate_profile" class="childs"> Profile </label>                                     
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="candidate_tabular_data" value="candidate_tabular_data" id="candidate_tabular_data" class="childs"> Contract History </label>
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="candidate_interview" value="interviews" id="candidate_interviews" class="childs"> Interviews </label> 
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="parent-div">
                                <div class="i-checks"><label> <input type="checkbox" name="attachments" value="attachments" class="parent"> <i></i> Attachments </label></div>
                                <div class="form-group" style="padding-left: 30px;">
                                    <div class="col-sm-12">
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="resume" value="resume" id="resume" class="childs"> Resume and Other Attachments </label> 
                                        <label class="checkbox-inline i-checks"> <input type="checkbox" name="service_agreement" value="service_agreement" id="service_agreement" class="childs"> Service Agreements </label> 
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-primary" type="button" id="syncnow">Sync Zoho Data</button>
                            <img src="{{admin_asset('images/input-spinner.gif')}}" id="sync-loader" style="display: none;">
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>
<!--CONFIRM PASSWORD MODAL-->
<div class="modal inmodal" id="verify-password-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">

            {!! Form::open(['url'=>'admin/verifrypassword','id'=>'verify-password'])!!}
            <input type="hidden" name="type" value="syncdata" id="action-type"/>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Verify Your Password</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissable" id="error-message" style="display: none;">
                    <span>You have enter wrong password</span>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        <label class="control-label">Enter Password: <span class="input-required">*</span></label>
                    </div>
                    <div class="col-md-8">
                        {{ Form::password('password',['class' => 'form-control','placeholder' => 'Enter password']) }}                      
                    </div>                    
                </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary submit-button">Save</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!--End Add--> 
@endsection