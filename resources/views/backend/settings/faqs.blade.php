@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                @include('Layouts.backend.message')
                <div class="ibox-title">
                    <h5>FAQ </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=> 'faq','method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group pl-1 display-inline">
                                <a href="{{route('addfaq')}}" class="btn btn-sm btn-primary " type="button"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                            <div class="input-group cus-refresh">
                                <a href="{{route('faq')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button> 
                                </span>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Territory</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($faq))
                                @foreach($faq as $key => $value)
                                <tr>
                                    <td>{{$value->faqcategory->name}}</td>
                                    <td>{{$value->question}}</td>
                                    <td>{{$value->answer}}</td>
                                    <td>{{$value->territory}}</td>
                                    <td>
                                        <a href="{{route('updatefaq',[$value->id])}}" data-id="{{$value->id}}" class="cus-icon" title="Edit FAQ"><i class="fa fa-pencil"></i></a>
                                        <a href="{{route('deletefaq',[$value->id])}}" class="cus-icon text-danger delete-faq" title="Delete FAQ"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5" class="text-danger text-center">No FAQ's found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($faq)){!! $faq->render() !!}@endif
                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')