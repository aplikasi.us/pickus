@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">  
    @include('Layouts.backend.header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h3> Update Site Details</h3>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All form elements with * are required fields.</h5>
                </div>
                <div class="ibox-content">
                    {{ Form::open(['route' => ['updatesite'], 'method' => 'POST', 'id'=>'site-settings-form']) }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Site Name: <span class="input-required">*</span></label>
                                    {{ Form::text('site_name',$site_settings[0]->settingvalue, ['class' => 'form-control', 'id' => 'site-name']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Address: <span class="input-required">*</span></label>
                                    {{ Form::text('address',$site_settings[1]->settingvalue, ['class' => 'form-control', 'id' => 'address']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">City: <span class="input-required">*</span></label>
                                    {{ Form::text('city',$site_settings[2]->settingvalue, ['class' => 'form-control', 'id' => 'site-name']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Zip Code: <span class="input-required">*</span></label>
                                    {{ Form::text('zipcode',$site_settings[3]->settingvalue, ['class' => 'form-control', 'id' => 'address']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Phone: <span class="input-required">*</span></label>
                                    {{ Form::text('phone',$site_settings[4]->settingvalue, ['class' => 'form-control', 'id' => 'site-name','maxlength' => 10]) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Territory (PH) Email: <span class="input-required">*</span></label>
                                    {{ Form::text('ph_email',$site_settings[5]->settingvalue, ['class' => 'form-control', 'id' => 'address']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Territory (MY) Email: <span class="input-required">*</span></label>
                                    {{ Form::text('my_email',$site_settings[16]->settingvalue, ['class' => 'form-control', 'id' => 'address']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Rows Per Page: <span class="input-required">*</span></label>
                                    {{ Form::select('rows_per_page',["10" => "10","20" => "20","30" => "30","50" => "50"],$site_settings[6]->settingvalue, ['class' => 'form-control', 'id' => 'rows-per-page']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Date Format: <span class="input-required">*</span></label>
                                    {{ Form::select('date_format',$date_format,$site_settings[7]->settingvalue, ['class' => 'form-control', 'id' => 'address']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>
@endsection