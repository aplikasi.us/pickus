@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Tagline</h5>
                </div>
                <div class="ibox-content">
                    {{Form::open(['route' => ['addtagline'],'class'=>'tagline-form','id'=>'tagline-form'])}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>User Type: <span class="input-required">*</span></label>
                                    {{Form::select('user_type',[null => 'Select User Type','Client' => 'Client','Consultant' => 'Consultant'],'',['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Category: <span class="input-required">*</span></label>
                                    {{Form::select('category',[null => 'Select Category',"Facts" => 'Facts',"Tips" => 'Tips'],'',['class'=>'form-control m-input','required'])}}
                                </div>  
                            </div>                            
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Content: <span class="input-required">*</span></label>
                                    {{Form::textarea('content','',['class'=>'form-control m-input','required','rows' => 4])}}
                                </div>  
                            </div>        

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">                                                          
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('taglines')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')