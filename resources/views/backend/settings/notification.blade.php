@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">  
    @include('Layouts.backend.header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h3> See all notification</h3>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All Notification</h5>
                </div>
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-double-bounce">
                        <div class="sk-double-bounce1"></div>
                        <div class="sk-double-bounce2"></div>
                    </div>
                    <ul class="sortable-list connectList agile-list ui-sortable" id="todo">
                        @if(count($notifications))
                        @foreach ($notifications as $key => $value)
                        <li class="success-element" id="{{$key}}">
                            {!!$value->message!!}
                            <div class="agile-detail">
                                <!-- <a href="#" class="pull-right btn btn-xs btn-white">{{$value->type}}</a> -->
                                <i class="fa fa-calendar" aria-hidden="true"></i> {{date('d M. Y',strtotime($value->created_at))}}
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                    <div class="mb-50">
                        @if(isset($notifications)){!! $notifications->render() !!}@endif
                    </div>
                </div>                
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>
@endsection