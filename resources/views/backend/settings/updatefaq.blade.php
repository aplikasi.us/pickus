@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update FAQ</h5>
                </div>
                <div class="ibox-content">
                    {{Form::open(['route' => ['updatefaq',$faq->id],'class'=>'addfaq','id'=>'faqform'])}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Category: <span class="input-required">*</span></label>
                                    {{Form::select('category',[null => 'Select Category'] + $category,$faq->category,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Territory: <span class="input-required">*</span></label>
                                    {{Form::select('territory',['MY' => 'MY','PH' => 'PH','BOTH' => 'BOTH'],$faq->territory,['class'=>'form-control m-input'])}}
                                </div>  
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Question: <span class="input-required">*</span></label>
                                    {{Form::text('question',$faq->question,['class'=>'form-control m-input'])}}
                                </div>  
                            </div>        

                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Answer: <span class="input-required">*</span></label>
                                    {{Form::textarea('answer',$faq->answer,['class'=>'form-control m-input','rows' => 4])}}
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">                                                          
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('faq')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')