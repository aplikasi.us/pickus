@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Taglines </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['taglines'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group pl-1 display-inline">
                                <a href="{{route('addtagline')}}" class="btn btn-sm btn-primary " type="button"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                            <div class="input-group cus-refresh">
                                <a href="{{route('taglines')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button> 
                                </span>
                            </div>
                            <div class="input-group width25">
                                {{Form::select('category',[null=>'Select Category','Facts' => 'Facts','Tips' => 'Tips','Rating' => 'Rating'],'',['class'=>'input-sm form-control'])}}
                            </div>
                            <div class="input-group width25">
                                {{Form::select('user_type',[null=>'Select User Type','Client' => 'Client','Consultant' => 'Consultant'],'',['class'=>'input-sm form-control'])}}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">User Type </th>
                                    <th width="10%">Category</th>
                                    <th width="10%">Star</th>
                                    <th width="30%">Content</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($taglines))
                                @foreach($taglines as $row)
                                <tr>
                                    <td>
                                        {{ ($row->user_type) ? $row->user_type : "-"}}</td>
                                    <td>
                                        {{ ($row->category) ? $row->category : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->subcategory) ? $row->subcategory : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->content) ? $row->content : "-"}}
                                    </td>
                                    <td>
                                        @if($row->status == 'Active')
                                        <a href="Javascript:Void(0)" class="btn btn-xs btn-primary">Active</a>
                                        @else
                                        <a href="Javascript:Void(0)" class="btn btn-xs btn-danger">Inactive</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('updatetagline',$row->id)}}" class="cus-icon" title="Edit Tagline"><i class="fa fa-pencil"></i></a>
                                        @if($row->category != "Rating")
                                        <a href="{{route('deletetagline',$row->id)}}" class="cus-icon delete-tagline" title="Delete Tagline"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($taglines))
                                <tr><td colspan="6"><h4 class="text-center">No Records Found</h4></td></tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($taglines)){!! $taglines->render() !!}@endif

                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')