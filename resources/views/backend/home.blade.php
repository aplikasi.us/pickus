@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error') }}
        </div>
        @endif
        <div class="col-lg-4">
            <a href="{{route('clients.index')}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Clients</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$clients}}</h1>
                        <small>Total clients</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{route('consultant.index')}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Consultants</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$consultant}}</h1>
                        <small>Total consultants</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{route('jobs.index')}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Job Openings</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$jobs}}</h1>
                        <small>Total job openings</small>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <a href="{{route('indexRequestProfile')}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Requested Profiles</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$requestedProfiles}}</h1>
                        <small>Total requested profiles</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{route('consultant.index',['requested_summary' => 1])}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Requested Summary</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$requestedSummary}}</h1>
                        <small>Total requested summary</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{route('jobRequestedProfile')}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Pending Jobs Request</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$pendingJobRequests}}</h1>
                        <small>Total pending job request</small>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <a href="{{route('upcoming_interviews')}}">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Upcoming Interviews</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$upcomingInterview}}</h1>
                        <small>Total upcoming interviews</small>
                    </div>
                </div>
            </a>
        </div>
    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')