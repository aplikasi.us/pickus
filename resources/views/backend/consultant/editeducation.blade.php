@extends('Layouts.backend.main')
@section('content') @include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            {{ Form::open(['route' => ['updateeducation',$education->TABULARROWID], 'method' => 'POST', 'id'=>'education-form']) }}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Education Details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Institute: </label>
                                    <input type="hidden" name="CANDIDATEID" value="{{$education->CANDIDATEID}}" />
                                    {{Form::text('institute',$education->institute,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Department: <span class="input-required">*</span></label>
                                    {{Form::text('department',$education->department,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Degree: <span class="input-required">*</span></label>
                                    {{Form::text('degree',$education->degree,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label>Duration From: <span class="input-required">*</span></label>
                                <div>
                                    <div class="form-group duration-div">
                                        @if($education->duration_from != null || $education->duration_from != "")
                                        @php
                                        $workDurationFrom = explode('-',$education->duration_from);
                                        @endphp
                                        {{Form::select('duration_from_month',[null => "Select Month"] + $monthList,$workDurationFrom[0],['class' => 'duration_from_month form-control m-input to'])}}

                                        @else
                                        {{Form::select('duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'duration_from_month form-control m-input to'])}}

                                        @endif
                                    </div>
                                    <div class="form-group duration-div">
                                        @if($education->duration_from != null || $education->duration_from != "")

                                        {{Form::select('duration_from_year',[null => "Select Year"] + $yearList,$workDurationFrom[1],['class' => 'duration_from_year form-control m-input'])}}
                                        @else

                                        {{Form::select('duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'duration_from_year form-control m-input'])}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="clearfix d-block">
                                    <label>Duration To: </label>
                                    <div class="pull-right">
                                        @if($alreadyPersuing != null)
                                        @if($alreadyPersuing->TABULARROWID == $education->TABULARROWID)
                                        <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id"
                                            value="" />
                                        @else
                                        <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id"
                                            value="{{$alreadyPersuing->TABULARROWID}}" />
                                        @endif

                                        @else
                                        <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id"
                                            value="" />
                                        @endif
                                        <label for="is_currently_pursuing">Is Currently Pursuing:</label>
                                        @if($education->currently_pursuing == "true")
                                        <input type="checkbox" name="is_currently_pursuing" id="is_currently_pursuing"
                                            checked="">
                                        @else
                                        <input type="checkbox" name="is_currently_pursuing" id="is_currently_pursuing">
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix d-block">
                                    <div class="form-group duration-div">
                                        @if($education->currently_pursuing == "true")
                                        {{Form::select('duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_to form-control m-input to','id' => 'duration_to_month','disabled'])}}

                                        @else

                                        @if($education->duration_to != null || $education->duration_to != "")
                                        @php
                                        $workDurationTo = explode('-',$education->duration_to);
                                        @endphp
                                        {{Form::select('duration_to_month',[null => "Select Month"] + $monthList,$workDurationTo[0],['class' => 'duration_to form-control m-input to','id' => 'duration_to_month'])}}

                                        @else
                                        {{Form::select('duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'duration_to form-control m-input to','id' => 'duration_to_month'])}}

                                        @endif

                                        @endif
                                    </div>
                                    <div class="form-group duration-div">
                                        @if($education->currently_pursuing == "true")

                                        {{Form::select('duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_to form-control m-input','id' => 'duration_to_year','disabled'])}}
                                        @else

                                        @if($education->duration_to != null || $education->duration_to != "")
                                        @php
                                        $workDurationTo = explode('-',$education->duration_to);
                                        @endphp

                                        {{Form::select('duration_to_year',[null => "Select Year"] + $yearList,$workDurationTo[1],['class' => 'duration_to form-control m-input','id' => 'duration_to_year'])}}
                                        @else

                                        {{Form::select('duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'duration_to form-control m-input','id' => 'duration_to_year'])}}
                                        @endif

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('editConsultant',[$education->CANDIDATEID])}}"
                                        class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>
    @include('Layouts.backend.foot')
</div>
@endsection('content')