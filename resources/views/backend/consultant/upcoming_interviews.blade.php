@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upcoming Interviews </h5>
                </div>
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['upcoming_interviews'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">


                            <div class="input-group cus-refresh">
                                <a href="{{route('upcoming_interviews')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                </span>
                            </div>
                            <div class="input-group width25">
                                {{Form::select('candidate',[null => 'Select Candidate'] + $consultants,'',['class'=>'form-control m-input'])}}
                            </div>
                            <div class="input-group width25">
                                {{Form::select('client',[null => 'Select Client'] + $clients,'',['class'=>'form-control m-input'])}}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Interview Name</th>
                                    <th>From </th>
                                    <th>To</th>
                                    <th>Candidate Name</th>
                                    <th>Client Name</th>
                                    <th>Interviewers</th>
                                    <th>Posting Title</th>
                                    <th>Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($upcomingInterviews))
                                @foreach($upcomingInterviews as $row)
                                <tr>
                                    <td>
                                        {{ ($row->interview_name) ? $row->interview_name : "-"}}
                                    </td>
                                    <td>
                                        {{date('Y-m-d H:i A',strtotime($row->start_datetime))}}
                                    </td>
                                    <td>
                                        {{date('Y-m-d H:i A',strtotime($row->end_datetime))}}
                                    </td>
                                    <td>
                                        <a
                                            href="{{route('editConsultant',[$row->CANDIDATEID])}}">{{ isset($row->candidate) ? $row->candidate->first_name.' '.$row->candidate->last_name : "-"}}</a>
                                    </td>
                                    <td>
                                        <a
                                            href="{{route('clients.index',['s' => $row->Interviewers])}}">{{ isset($row->client) ? $row->client->client_name : "-"}}</a>
                                    </td>
                                    <td>
                                        {{ $row->Interviewers}}
                                    </td>
                                    <td>
                                        {{ isset($row->job) ? $row->job->posting_title : "-"}}
                                    </td>
                                    <td>
                                        {{ isset($row->venue) ? $row->venue : "-"}}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="8">
                                        <h4 class="text-center">No Records Found</h4>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($upcomingInterviews)){!! $upcomingInterviews->render() !!}@endif

                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')