@extends('Layouts.backend.main')
@section('content') @include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            {{ Form::open(['route' => ['addreference',$id], 'method' => 'POST', 'id'=>'reference-form']) }}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Reference Details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name: <span class="input-required">*</span></label>
                                    {{Form::text('name','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Position: <span class="input-required">*</span></label>
                                    {{Form::text('position','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Company: <span class="input-required">*</span></label>
                                    {{Form::text('company','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Phone: <span class="input-required">*</span></label>
                                    {{Form::text('phone','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email: <span class="input-required">*</span></label>
                                    {{Form::email('email','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">                                                          
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('editConsultant',[$id])}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>
    @include('Layouts.backend.foot')
</div>
@endsection('content')