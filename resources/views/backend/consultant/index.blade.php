@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-9">
                            <h5>Consultants </h5>
                        </div>
                        <div class="col-md-3 pull-right">
                            <a href="{{route('consultant.create')}}" type="button" class="btn btn-outline btn-primary pull-right">Create</a>
                        </div>
                    </div>
                </div>
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['consultant.index'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">


                            <div class="input-group cus-refresh">
                                <a href="{{route('consultant.index')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                </span>
                            </div>
                            <div class="input-group width25">
                                {{Form::select('requested_summary',[null => 'Select Requested Summary',1 => "Yes",0 => "No"],'',['class'=>'form-control m-input'])}}
                            </div>
                            <div class="input-group width25">
                                {{Form::select('account_status',[null => 'Select Account Status',1 => "Active",2 => "Inactive"],'',['class'=>'form-control m-input'])}}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Candidate ID</th>
                                    <th>Candidate Name </th>
                                    <th>Email</th>
                                    <th>Contact No.</th>
                                    <th>Currency</th>
                                    <th>Zoho Status</th>
                                    <th>Account Status</th>
                                    <th>Requested Summary</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($consultant))
                                @foreach($consultant as $row)
                                <tr>
                                    <td>
                                        {{ ($row->candidate_id) ? $row->candidate_id : "-"}}
                                    </td>
                                    <td>
                                        {{$row->first_name}} {{ ($row->last_name)}}
                                    </td>
                                    <td>
                                        {{ ($row->email) ? $row->email : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->mobile_number) ? $row->mobile_number : "-"}}
                                    </td>
                                    <td>
                                        {{ ($row->currency) ? $row->currency : "-"}}
                                    </td>
                                    <td>
                                        <button class="btn btn-xs btn-primary">{{$row->candidate_status}}</button>
                                    </td>
                                    <td>
                                        @if($row->is_delete == 0)
                                        <button class="btn btn-xs btn-primary consultant-active-btn"
                                            data-id="{{$row->CANDIDATEID}}" data-status="inactive">Active</button>
                                        @else
                                        <button class="btn btn-xs btn-danger consultant-active-btn"
                                            data-id="{{$row->CANDIDATEID}}" data-status="active">Inactive</button>
                                        @endif
                                    </td>
                                    <td>
                                        @if($row->is_summary_requested == 1 )
                                        Requested
                                        @else
                                        No
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group table-group-button">
                                            <button data-toggle="dropdown"
                                                class="btn btn-primary btn-sm dropdown-toggle"
                                                aria-expanded="false">Action <span class="caret"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{route('jobs.index')}}?consultant_id={{$row->CANDIDATEID}}"
                                                        class="btn btn-xs">View Job</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('editConsultant',[$row->CANDIDATEID])}}"
                                                        class="btn btn-xs">Edit</a>
                                                </li>
                                                @if($row->is_delete == 1)
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('restore-consultant-account',[$row->CANDIDATEID])}}"
                                                        class="btn btn-xs restore-account">Restore Account</a>
                                                </li>
                                                @endif
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('delete-consultant',[$row->CANDIDATEID])}}"
                                                        class="btn btn-xs delete-account">Delete Account</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{route('associate-job-openings',[$row->CANDIDATEID])}}"
                                                        class="btn btn-xs">Associate Job Openings</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($consultant))
                                <tr>
                                    <td colspan="7">
                                        <h4 class="text-center">No Records Found</h4>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($consultant)){!! $consultant->render() !!}@endif

                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--CONFIRM PASSWORD MODAL-->
    <div class="modal inmodal" id="verify-password-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">

                {!! Form::open(['url'=>'admin/verifrypassword','id'=>'verify-password'])!!}
                <input type="hidden" name="type" value="delete-consultant-account" id="action-type" />
                <input type="hidden" name="redirect_url" value="" id="redirect_url" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Verify Your Password</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissable" id="error-message" style="display: none;">
                        <span>You have enter wrong password</span>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">Enter Password: <span class="input-required">*</span></label>
                        </div>
                        <div class="col-md-8">
                            {{ Form::password('password',['class' => 'form-control','placeholder' => 'Enter password']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submit-button">Verify Now</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--End Add-->
    @include('Layouts.backend.foot')
</div>

@endsection('content')