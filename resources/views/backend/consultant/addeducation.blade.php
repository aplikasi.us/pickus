@extends('Layouts.backend.main')
@section('content') @include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            {{ Form::open(['route' => ['addeducation',$id], 'method' => 'POST', 'id'=>'education-form']) }}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Education Details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Institute: </label>
                                    {{Form::text('institute','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Department: <span class="input-required">*</span></label>
                                    {{Form::text('department','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Degree: <span class="input-required">*</span></label>
                                    {{Form::text('degree','',['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label>Duration From: <span class="input-required">*</span></label>
                                <div>
                                    <div class="form-group duration-div">
                                        {{Form::select('duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'duration_from_month form-control m-input to'])}}
                                    </div>
                                    <div class="form-group duration-div">
                                        {{Form::select('duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'duration_from_year form-control m-input'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="clearfix d-block">
                                    <label>Duration To: </label>
                                    @if($alreadyPersuing != null)
                                    <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id"
                                        value="{{$alreadyPersuing->TABULARROWID}}" />

                                    @else
                                    <input type="hidden" name="is_already_persuing_id" id="is_already_persuing_id"
                                        value="" />
                                    @endif
                                    <div class="pull-right">
                                        <label for="is_currently_pursuing">Is Currently Pursuing:</label>
                                        <input type="checkbox" name="is_currently_pursuing" id="is_currently_pursuing">
                                    </div>
                                </div>
                                <div class="clearfix d-block">
                                    <div class="form-group duration-div">
                                        {{Form::select('duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'duration_to form-control m-input to','id' => 'duration_to_month'])}}
                                    </div>
                                    <div class="form-group duration-div">
                                        {{Form::select('duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'duration_to form-control m-input','id' => 'duration_to_year'])}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('editConsultant',[$id])}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>
    @include('Layouts.backend.foot')
</div>
@endsection('content')