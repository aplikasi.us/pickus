@extends('Layouts.backend.main')
@section('content') @include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            {{ Form::open(['route' => ['consultantsummaryapprove',$editconsultant->CANDIDATEID], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            {{ Form::hidden('id',$editconsultant->CANDIDATEID) }}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Consultant Basic Details</h5>
                </div>
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address: </label>
                                    {{Form::email('email',$editconsultant->email,['class'=>'form-control m-input','disabled' => 'disabled'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sap job title: <span class="input-required">*</span></label>
                                    {{Form::text('sap_job_title',$editconsultant->sap_job_title,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name: <span class="input-required">*</span></label>
                                    {{Form::text('first_name',$editconsultant->first_name,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name: <span class="input-required">*</span></label>
                                    {{Form::text('last_name',$editconsultant->last_name,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Category: </label>
                                    {{Form::select('category[]',$categories,explode(',',$editconsultant->category),['class'=>'form-control','id' => 'candidate-category','multiple' => true])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Mobile number: <span class="input-required">*</span></label>
                                    {{Form::text('mobile_number',$editconsultant->mobile_number,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Experience in year: <span class="input-required">*</span></label>
                                    {{Form::number('experience_in_years',$editconsultant->experience_in_years,['class'=>'form-control m-input','min' => 0])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>City: <span class="input-required">*</span></label>
                                    {{Form::text('city',$editconsultant->city,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>State: <span class="input-required">*</span></label>
                                    {{Form::text('state',$editconsultant->state,['class'=>'form-control m-input'])}}
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Skill Set: <span class="input-required">*</span></label>
                                    {{Form::text('skill_set',$editconsultant->skill_set,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Base Rate: <span class="input-required">*</span></label>
                                    {{Form::text('base_rate',$editconsultant->base_rate,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Reserved BR: <span class="input-required">*</span></label>
                                    {{Form::text('reserved_base_rate',$editconsultant->reserved_base_rate,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Notice Period: <span class="input-required">*</span></label>
                                    {{Form::text('notice_period_days',$editconsultant->notice_period_days,['class'=>'form-control m-input','placeholder' => 'Notice period days'])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Availability Date: <span class="input-required">*</span></label>
                                    {{Form::text('availability_date',$editconsultant->availability_date,['class'=>'form-control m-input','id' => 'availability_date','readonly'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Certification & Training: </label>
                                    {{Form::textarea('certification_and_training',$editconsultant->certification_and_training,['class'=>'form-control m-input','rows' => 4])}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Work Preferences:</label><br />
                                    @if($editconsultant->full_time == 1)
                                    <input type="checkbox" name="full_time" checked="" id="full_time" value="Full Time">
                                    <label for="full_time">Full Time</label><br />
                                    @else
                                    <input type="checkbox" name="full_time" id="full_time" value="Full Time"> <label
                                        for="full_time">Full Time</label><br />
                                    @endif
                                    @if($editconsultant->part_time == 1)
                                    <input type="checkbox" name="part_time" checked="" id="part_time" value="Part Time">
                                    <label for="part_time">Part Time</label><br />
                                    @else
                                    <input type="checkbox" name="part_time" id="part_time" value="Part Time"> <label
                                        for="part_time">Part Time</label><br />
                                    @endif
                                    @if($editconsultant->willing_to_travel == 1)
                                    <input type="checkbox" name="willing_to_travel" checked="" id="willing_to_travel"
                                        value="Willing to Travel"> <label for="willing_to_travel">Willing to
                                        Travel</label>
                                    @else
                                    <input type="checkbox" name="willing_to_travel" id="willing_to_travel"
                                        value="Willing to Travel"> <label for="willing_to_travel">Willing to
                                        Travel</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Role Preferences:</label><br />
                                    @if($editconsultant->project == 1)
                                    <input type="checkbox" name="project" checked="" id="project" value="Project">
                                    <label for="project">Project</label> <br />
                                    @else
                                    <input type="checkbox" name="project" id="project" value="Project"> <label
                                        for="project">Project</label> <br />
                                    @endif
                                    @if($editconsultant->support == 1)
                                    <input type="checkbox" name="support" checked="" id="support" value="Support">
                                    <label for="support">Support</label>
                                    @else
                                    <input type="checkbox" name="support" id="support" value="Support"> <label
                                        for="support">Support</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if(count($editconsultant->associatejobopenings) == 0)
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zoho Status: <span class="input-required">*</span></label>
                                    {{Form::select('status',$status,$editconsultant->candidate_status,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Consultants Summary Details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @php
                                    $readonly = '';
                                    if($editconsultant->is_summary_requested == 2 &&
                                    $editconsultant->is_summary_requested == 0 ){
                                    $readonly = 'readonly';
                                    }
                                    $additional_info = $editconsultant->additional_info;

                                    @endphp
                                    <label>Summary: <span class="input-required"></span></label>
                                    {{Form::textarea('additional_info',$additional_info,['class'=>'form-control m-input',$readonly])}}
                                </div>
                            </div>
                            @if($editconsultant->is_summary_requested == 1 )
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requested Summary: <span class="input-required"></span></label>
                                    {{Form::textarea('additional_info_2',$editconsultant->additional_info_2,['class'=>'form-control m-input',$readonly])}}
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('consultant.index')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit" name="updatesummary"
                                        value="1">Update</button>
                                    @if($editconsultant->is_summary_requested == 1 )
                                    <!-- <a href="{{route('consultantsummaryapprove',$editconsultant->CANDIDATEID)}}" class="btn btn-warning" type="button" id="approve-summary">Approve</a>    -->
                                    <button class="btn btn-warning" type="submit" name="updatesummary"
                                        value="2">Approve</button>
                                    <button class="btn btn-danger" type="submit" name="updatesummary"
                                        value="3">Reject</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-2">
                            <h5>Experience</h5>
                        </div>
                        @if(count($editconsultant->experience) < 10) <div class="col-md-2 pull-right">
                            <a href="{{route('addexperience',[$editconsultant->CANDIDATEID])}}"
                                class="btn btn-primary">Add Experience</a>
                    </div>
                    @endif
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive1">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Company </th>
                                <th>Occupation</th>
                                <th>Industry </th>
                                <th>Work Duration From </th>
                                <th>Work Duration To</th>
                                <th>Is Currently Working Here</th>
                                <th>Summary</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($editconsultant->experience))
                            @foreach($editconsultant->experience as $experienceKey => $experienceValue)
                            <tr>
                                <td>{{$experienceValue->company}}</td>
                                <td>{{$experienceValue->occupation}}</td>
                                <td>{{$experienceValue->industry}}</td>
                                <td>{{$experienceValue->work_duration}}</td>
                                <td>{{$experienceValue->work_duration_to}}</td>
                                <td>
                                    @if($experienceValue->is_currently_working_here == "true")
                                    {{"Yes"}}
                                    @else
                                    {{"No"}}
                                    @endif
                                </td>
                                <td>{{$experienceValue->summary}}</td>
                                <td>
                                    <a href="{{route('updateexperience',[$experienceValue->TABULARROWID])}}"
                                        class="cus-icon" title="Edit Experience"><i class="fa fa-pencil"></i></a>
                                    <a href="{{route('deleteexperience',[$experienceValue->TABULARROWID])}}"
                                        class="cus-icon delete-tabulardata" title="Delete Experience"
                                        data-title="experience"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td class="text-danger text-center" colspan="8">No experience found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-2">
                        <h5>Education</h5>
                    </div>
                    @if(count($editconsultant->education) < 10) <div class="col-md-2 pull-right">
                        <a href="{{route('addeducation',[$editconsultant->CANDIDATEID])}}" class="btn btn-primary">Add
                            Education</a>
                </div>
                @endif
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive1">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Institute </th>
                            <th>Department</th>
                            <th>Degree </th>
                            <th>Duration From </th>
                            <th>Duration To</th>
                            <th>Is Currently pursuing</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($editconsultant->education))
                        @foreach($editconsultant->education as $educationKey => $educationValue)
                        <tr>
                            <td>{{$educationValue->institute}}</td>
                            <td>{{$educationValue->department}}</td>
                            <td>{{$educationValue->degree}}</td>
                            <td>{{$educationValue->duration_from}}</td>
                            <td>{{$educationValue->duration_to}}</td>
                            <td>
                                @if($educationValue->currently_pursuing == "true")
                                {{"Yes"}}
                                @else
                                {{"No"}}
                                @endif
                            </td>
                            <td>
                                <a href="{{route('updateeducation',[$educationValue->TABULARROWID])}}" class="cus-icon"
                                    title="Edit Education"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('deleteeducation',[$educationValue->TABULARROWID])}}"
                                    class="cus-icon delete-tabulardata" title="Delete Education"
                                    data-title="education"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td class="text-danger text-center" colspan="7">No education found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-md-2">
                    <h5>Reference</h5>
                </div>
                @if(count($editconsultant->reference) < 10) <div class="col-md-2 pull-right">
                    <a href="{{route('addreference',[$editconsultant->CANDIDATEID])}}" class="btn btn-primary">Add
                        Reference</a>
            </div>
            @endif
        </div>
    </div>
    <div class="ibox-content">
        <div class="table-responsive1">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name </th>
                        <th>Position</th>
                        <th>Company </th>
                        <th>Mobile Number </th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($editconsultant->reference))
                    @foreach($editconsultant->reference as $referenceKey => $referenceValue)
                    <tr>
                        <td>{{$referenceValue->name}}</td>
                        <td>{{$referenceValue->position}}</td>
                        <td>{{$referenceValue->company}}</td>
                        <td>{{$referenceValue->phone}}</td>
                        <td>{{$referenceValue->email}}</td>
                        <td>
                            <a href="{{route('updatereference',[$referenceValue->TABULARROWID])}}" class="cus-icon"
                                title="Edit Reference"><i class="fa fa-pencil"></i></a>
                            <a href="{{route('deletereference',[$referenceValue->TABULARROWID])}}"
                                class="cus-icon delete-tabulardata" title="Delete Reference" data-title="reference"><i
                                    class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td class="text-danger text-center" colspan="6">No reference found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
@include('Layouts.backend.foot')
</div>
@endsection('content')