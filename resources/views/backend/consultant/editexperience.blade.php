@extends('Layouts.backend.main')
@section('content') @include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            {{ Form::open(['route' => ['updateexperience',$experience->TABULARROWID], 'method' => 'POST', 'id'=>'experience-form']) }}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Experience Details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Company: </label>
                                    <input type="hidden" name="CANDIDATEID" value="{{$experience->CANDIDATEID}}" />
                                    {{Form::text('company',$experience->company,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Occupation: <span class="input-required">*</span></label>
                                    {{Form::text('occupation',$experience->occupation,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Industry: <span class="input-required">*</span></label>
                                    {{Form::text('industry',$experience->industry,['class'=>'form-control m-input'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label>Work Duration From: <span class="input-required">*</span></label>
                                <div>
                                    <div class="form-group duration-div">
                                        @if($experience->work_duration != null || $experience->work_duration != "")
                                        @php
                                        $workDurationFrom = explode('-',$experience->work_duration);
                                        @endphp
                                        {{Form::select('work_duration_from_month',[null => "Select Month"] + $monthList,$workDurationFrom[0],['class' => 'work_duration_from_month form-control m-input to'])}}

                                        @else
                                        {{Form::select('work_duration_from_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_from_month form-control m-input to'])}}

                                        @endif
                                    </div>
                                    <div class="form-group duration-div">
                                        @if($experience->work_duration != null || $experience->work_duration != "")


                                        {{Form::select('work_duration_from_year',[null => "Select Year"] + $yearList,$workDurationFrom[1],['class' => 'work_duration_from_year form-control m-input'])}}
                                        @else

                                        {{Form::select('work_duration_from_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_from_year form-control m-input'])}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="clearfix d-block">
                                    @if($alreadyWorkingInCompany != null)
                                    @if($alreadyWorkingInCompany->TABULARROWID == $experience->TABULARROWID)
                                    <input type="hidden" name="is_already_working_experience_id" id="is_already_working_experience_id" value="" />
                                    @else
                                    <input type="hidden" name="is_already_working_experience_id" id="is_already_working_experience_id" value="{{$alreadyWorkingInCompany->TABULARROWID}}" />
                                    @endif
                                    
                                    @else
                                    <input type="hidden" name="is_already_working_experience_id" id="is_already_working_experience_id" value="" />
                                    @endif
                                    <label>Work Duration To: </label>
                                    <div class="pull-right">
                                        <label for="is_currently_working_here">Is Currently Working Here:</label>
                                        @if($experience->is_currently_working_here == null || $experience->is_currently_working_here == "true")
                                        <input type="checkbox" name="is_currently_working_here" id="is_currently_working_here" checked="">
                                        @else
                                        <input type="checkbox" name="is_currently_working_here" id="is_currently_working_here">
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix d-block">
                                    <div class="form-group duration-div">
                                        @if($experience->is_currently_working_here == "true")        
                                        {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_to form-control m-input to','id' => 'work_duration_to_month','disabled'])}}

                                        @else

                                        @if($experience->work_duration_to != null || $experience->work_duration_to != "")
                                        @php
                                        $workDurationTo = explode('-',$experience->work_duration_to);
                                        @endphp
                                        {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,$workDurationTo[0],['class' => 'work_duration_to form-control m-input to','id' => 'work_duration_to_month'])}}

                                        @else
                                        {{Form::select('work_duration_to_month',[null => "Select Month"] + $monthList,'',['class' => 'work_duration_to form-control m-input to','id' => 'work_duration_to_month'])}}

                                        @endif

                                        @endif
                                    </div>
                                    <div class="form-group duration-div">
                                        @if($experience->is_currently_working_here == "true")        

                                        {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_to form-control m-input','id' => 'work_duration_to_year','disabled'])}}
                                        @else

                                        @if($experience->work_duration_to != null || $experience->work_duration_to != "")
                                        @php
                                        $workDurationTo = explode('-',$experience->work_duration_to);
                                        @endphp

                                        {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,$workDurationTo[1],['class' => 'work_duration_to form-control m-input','id' => 'work_duration_to_year'])}}
                                        @else

                                        {{Form::select('work_duration_to_year',[null => "Select Year"] + $yearList,'',['class' => 'work_duration_to form-control m-input','id' => 'work_duration_to_year'])}}
                                        @endif

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">                                                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Summary: <span class="input-required">*</span></label>
                                    {{Form::textarea('summary',$experience->summary,['class'=>'form-control m-input','rows' => 4])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">                                                          
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('editConsultant',[$experience->CANDIDATEID])}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>
    @include('Layouts.backend.foot')
</div>
@endsection('content')