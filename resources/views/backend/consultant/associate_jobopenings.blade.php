@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Job Openings associated to
                        {{($consultant != null) ? $consultant->first_name.' '.$consultant->last_name : "Consultant"}}
                    </h5>
                </div>
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="button" id="change-candidate-status" class="btn btn-sm btn-primary">
                                    Change Status</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="i-checks" id="example-select-all"></th>
                                    <th>Job ID </th>
                                    <th>Candidate Status</th>
                                    <th>Posting Title</th>
                                    <th>Job Status</th>
                                    <th>Target Date</th>
                                    <th>Client Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($associateJobOpenings))
                                @foreach($associateJobOpenings as $key => $value)
                                <tr>
                                    <td>
                                        <input class="checkall i-checks" type="checkbox" name="check[]" id="sub_chk"
                                            data-consultant-id="{{$value->CANDIDATEID}}"
                                            data-job-id="{{$value->JOBOPENINGID}}">
                                    </td>
                                    <td>{{ isset($value->job) ? $value->job->Job_ID : "-"}}</td>
                                    <td>{{ $value->status }}</td>
                                    <td>
                                        @if(isset($value->job))
                                        <a
                                            href="{{route('jobs.index',['jobid' => $value->job->id])}}">{{$value->job->posting_title}}</a>
                                        @else
                                        {{"-"}}
                                        @endif
                                    </td>
                                    <td>{{ isset($value->job) ? $value->job->job_opening_status : "-"}}</td>
                                    <td>{{ isset($value->job) ? $value->job->target_date : "-"}}</td>
                                    <td>
                                        @if($value->job != null)
                                        @if($value->job->client != null)
                                        <a
                                            href="{{route('clients.index',['clientid' => $value->job->client->CLIENTID])}}">{{$value->job->client->client_name}}</a>
                                        @else
                                        {{"-"}}
                                        @endif
                                        @else
                                        {{"-"}}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="7">
                                        <h4 class="text-center">No Jobs Found</h4>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(isset($associateJobOpenings)){!! $associateJobOpenings->render() !!}@endif

                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--CHANGE STATUS MODAL-->
    <div class="modal inmodal" id="change-candidate-status-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                {!! Form::open(['route'=>'change-consultant-status','id'=>'change-candidate-status-form'])!!}
                <input type="hidden" name="job_ids" value="" id="job_ids" />
                <input type="hidden" name="consultant_id" value="{{$consultant->CANDIDATEID}}" id="consultant_id" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Change Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">Candidate Status: <span class="input-required">*</span></label>
                        </div>
                        <div class="col-md-8">
                            {{ Form::select('status',[null => 'Select Status'] + $statusList,$consultant->candidate_status,['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary submit-button">Update</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--End Add-->
    @include('Layouts.backend.foot')
</div>

@endsection('content')