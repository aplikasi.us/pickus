@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="ibox-title pagetitle">

                    <div class="row">
                        <div class="col-md-9">
                            <h5>Create a new consultant</h5>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    {{Form::open(['route' => ['consultant.store'],'class'=>'system_user','id'=>'system_user', 'enctype' => 'multipart/form-data'])}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name: <span class="input-required">*</span></label>
                                    {{Form::text('first_name',old('first_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name: <span class="input-required">*</span></label>
                                    {{Form::text('last_name',old('last_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address: <span class="input-required">*</span></label>
                                    {{Form::email('email',old('email'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number: <span class="input-required">*</span></label>
                                    {{Form::text('phone',old('phone'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>SAP Job Title: <span class="input-required">*</span></label>
                                    {{Form::text('sap_job_title',old('sap_job_title'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>CV: <span class="input-required">*</span></label>
                                    {{Form::file('cv',old('cv'),['class'=>'form-control m-input','required'])}}
                                    <p><small>Please select only docx or pdf and select file size below 2 mb</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('systemuser.index')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    <!--End Add-->
    @include('Layouts.backend.foot')
</div>
@endsection('content')