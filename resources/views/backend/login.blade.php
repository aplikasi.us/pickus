<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aplikasi | Login</title>
    <link href="{{asset_public('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset_public('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset_public('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset_public('css/style.css')}}" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
            <div class="mb-3">
                <h1 class="logo-name">Aplikasi</h1>
            </div>
            <?php 
            if(Session::has('errorLogin')){ ?>
                <div class="alert alert-danger">
                    <span><?=Session::get('errorLogin');?></span>
                </div>
            <?php } ?>

            <form class="m-t" method="POST" id="loginform" action="{{ url('admin/login/check') }}">
            {{ csrf_field() }}
            <!-- @if(isset($_GET['redirect_url']) && !empty($_GET['redirect_url']))
            <input type="hidden" name="redirect_url" value ="{{$_GET['redirect_url']}}">
            @endif -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block new-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="help-block new-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
            <button type="submit" class="login btn btn-primary block full-width m-b">
                Login
            </button>
        </form>
            <p class="m-t"> <small>2019 All Rights Reserved</small> </p>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset_public('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset_public('js/bootstrap.min.js')}}"></script>
    <script src="{{asset_public('js/validator.js')}}"></script>
    <script src="{{asset_public('js/login.js')}}"></script>


</body>

</html>

