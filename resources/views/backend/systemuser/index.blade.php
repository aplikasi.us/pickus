@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    {{ Session::get('error') }}
                </div>
                @endif
                @include('Layouts.backend.message')
                <div class="ibox-title">
                    <h5>System Users </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        {{Form::open(['route'=>['systemuser.index'],'method'=>'get'])}}
                        <div class="col-sm-1 m-b-xs">
                        </div>
                        <div class="col-sm-1 m-b-xs">

                        </div>
                        <div class="col-sm-10">
                            <div class="input-group pl-1 display-inline">
                                <a href="javascript:void(0)" id="delete-selected-user" class="btn btn-sm btn-danger "
                                    type="button"><i class="fa fa-trash"></i> Delete </a>
                            </div>

                            <div class="input-group pl-1 display-inline">
                                <a href="{{route('systemuser.create')}}" class="btn btn-sm btn-primary "
                                    type="button"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                            <div class="input-group cus-refresh">
                                <a href="{{route('systemuser.index')}}"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="input-group width25">
                                {{Form::text('s',old('s'),['class'=>'input-sm form-control','placeholder' => 'Search'])}}
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                </span>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="table-responsive1">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>

                                        <input type="checkbox" class="i-checks" id="example-select-all"></th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>User Type</th>
                                    <th>Territory</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(!empty($systemuser))
                                @foreach($systemuser as $row )
                                <tr>
                                    <td>
                                        @if(Auth::user()->id != $row->id)<input class="checkall i-checks"
                                            type="checkbox" name="check[]" id="sub_chk" data-id="{{$row->id}}">@endif
                                    </td>
                                    <td>{{$row->first_name ? $row->first_name : "-"}}</td>
                                    <td>{{$row->last_name ? $row->last_name : "-"}}</td>
                                    <td>
                                        {{$row->email ? $row->email : "-"}}
                                    </td>
                                    <td>
                                        <?php 
                    $class = '';
                    
                    if(Session::get('id') == $row->id){
                      $class = "disabled";
                    }
                    
                    ?>
                                        @if($row->status == 'Active')
                                        <button class="system-active-btn btn btn-xs btn-primary" data-id="{{$row->id}}"
                                            data-status="active" <?php echo $class;?>>Active</button>
                                        @else
                                        <button class="system-active-btn btn btn-xs btn-danger" data-id="{{$row->id}}"
                                            data-status="inactive" {<?php echo $class;?>>Inactive</button>
                                        @endif
                                    </td>
                                    <td>{{$row->user_type ? $row->user_type : "Super Admin"}}</td>
                                    <td>{{$row->territory ? $row->territory : "-"}}</td>
                                    <td>
                                        <a href="{{route('systemuser.edit',$row->id)}}" class="cus-icon"><i
                                                class="fa fa-pencil"></i>
                                        </a>
                                        @if(Auth::user()->id != $row->id)
                                        <a href="{{route('systemuser_delete_record',$row->id)}}"
                                            class="cus-icon system-user-delete" title="Delete"><i
                                                class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @if(!count($systemuser))
                                <tr>
                                    <td colspan="6">
                                        <h4 class="text-center">No Records Found</h4>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')