@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add System User</h5>
                </div>
                <div class="ibox-content">
                    {{Form::open(['route' => ['systemuser.store'],'class'=>'system_user','id'=>'system_user'])}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name: <span class="input-required">*</span></label>
                                    {{Form::text('first_name',old('first_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name: <span class="input-required">*</span></label>
                                    {{Form::text('last_name',old('last_name'),['class'=>'form-control m-input','required'])}}
                                </div>  
                            </div>        

                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Email Address: <span class="input-required">*</span></label>
                                    {{Form::email('email',old('email'),['class'=>'form-control m-input','required'])}}
                                </div>  
                            </div>        

                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Password: <span class="input-required">*</span></label>
                                    {{Form::password('password',['class'=>'form-control m-input','required'])}}
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Confirm Password: <span class="input-required">*</span></label>
                                    {{Form::password('confirm_password',['class'=>'form-control m-input','required'])}}

                                </div>  
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group"><label class="control-label">Status:</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"><label class="control-label">Type:</label>
                                    <select class="form-control" id="systemadmin_type" name="systemadmin_type">
                                        <option value="Super Admin">Super Admin</option>
                                        <option value="Territory Admin">Territory Admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" id="territory-div" style="display: none;">
                                <div class="form-group"><label class="control-label">Territory:</label>
                                    <select class="form-control" id="territory" name="territory">
                                        <option value="">Select Territory</option>
                                        <option value="MY">MY</option>
                                        <option value="PH">PH</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">                                                          
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('systemuser.index')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>

    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')