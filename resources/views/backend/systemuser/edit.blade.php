@extends('Layouts.backend.main')
@section('content')
@include('Layouts.backend.sidebar')
<div id="page-wrapper" class="gray-bg dashbard-1">
    @include('Layouts.backend.header')
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit System User</h5>
                </div>
                <div class="ibox-content">

                    {!! Form::model($editAdmin, ['method' => 'put','url' => url('/admin/systemuser/update'), 'files' => true, 'id' => 'edit_system_user']) !!}

                    {{ Form::hidden('id', null) }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Email Address: <span class="input-required">*</span></label>
                                    {{Form::email('email',old('email'),['class'=>'form-control m-input','required','disabled' => 'disabled'])}}
                                </div>  
                            </div>        
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name: <span class="input-required">*</span></label>
                                    {{Form::text('first_name',old('first_name'),['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name: <span class="input-required">*</span></label>
                                    {{Form::text('last_name',old('last_name'),['class'=>'form-control m-input','required'])}}
                                </div>  
                            </div>        

                        </div>


                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group"><label class="control-label">Status:</label>
                                    <?php
                                    $class = '';

                                    if (Session::get('id') == $editAdmin->id) {
                                        $class = "disabled";
                                    }
                                    ?> 
                                    <select class="form-control" id="status" name="status"  {{$class}}>
                                        <option value="Active" {{($editAdmin->status == 'Active')?'selected':''}}>Active</option>
                                        <option value="Inactive" {{($editAdmin->status == 'Inactive')?'selected':''}}>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>User Type: <span class="input-required">*</span></label>
                                    {{Form::text('user_type',old('user_type'),['class'=>'form-control m-input','disabled' => 'disabled'])}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Territory: <span class="input-required">*</span></label>
                                    {{Form::text('territory',old('territory'),['class'=>'form-control m-input','disabled' => 'disabled'])}}
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">                                                          
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{{route('systemuser.index')}}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Change password</h5>
                </div>
                <div class="ibox-content">
                    <!-- BEGIN FORM-->
                    {!! Form::model($editAdmin, ['method' => 'post','url' => url('/admin/systemuser/changepwd'), 'id' => 'edit_Pwd']) !!}
                    {{ csrf_field() }}
                    <div class="form-body">
                        {{ Form::hidden('id', null) }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>New Password</label>
                                    {{Form::password('password',['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    {{Form::password('confirm_password',['class'=>'form-control m-input','required'])}}
                                </div>
                            </div>
                        </div>	
                    </div>
                    <div class="form-actions right">
                        <a href="{{route('systemuser.index')}}" class="btn btn-white">Cancel</a>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i> Change Password</button>
                    </div>
                    {!! Form::close() !!}
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    @include('Layouts.backend.foot')
</div>

@endsection('content')