@extends('Layouts.General.base_new')
@section('content')
@section('title','Consultant Profile')
<div class="container-temp mb-4">
    <p class="pb-3">
        <a href="{{ url()->previous() }}" title="Go Back" class="btnBack">
            <!-- <img src="{{admin_asset('images/icon-back.png')}}" alt="" title="">-->
            <i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
    </p>
    <div class="row rowConsultants">
        <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
            <div class="row align-items-end">
                <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
                    <h6 class="color6Gray d-inline">{{$consultant->candidate_id}}</h6>
                    <div class="box-rating consultant-start-rating f-16 cus-width-100">
                        @if(Auth::check())
                        @if(Auth::user()->role_id == 1)

                        @for($i = 1; $i <= 5; $i++) @if($consultant->rating < $i) <span class="fa fa-star unchecked">
                                </span>
                                @else
                                <span class="fa fa-star checked"></span>
                                @endif
                                @endfor

                                @endif
                                @endif
                    </div>
                    <div class="clear"></div>
                    <div>
                        <h2 class="text-red pb-2 d-inline-block">{{$consultant->sap_job_title}}</h2>
                        @if ($consultant->available_for_contract == 1)
                        <a href="javascript:;"
                            class="btn btn-greenDark btn-greendark-hvr btn-sm btn-round mb-un-av mob-d-inline cursor-default">Available</a>
                        @else
                        <a href="javascript:;"
                            class="btn btn-secondary cursor-default btn-second-hvr btn-sm btn-round mb-un-av mob-d-inline">Unavailable</a>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 text-right">
                    <div class="btnsRight btnContentPart" style="margin-bottom: 10px;">
                        @if(Auth::check())

                        @if(Auth::user()->role_id == 1)
                        @php
                        $btnCompare = 'can-btnRemoveCompare';
                        $btntext = 'Remove Compare';
                        if(!in_array($consultant->CANDIDATEID,$addTocompare)){
                        $btnCompare = 'can-btnCompare';
                        $btntext = 'Add to Compare';
                        }
                        @endphp
                        <a title="{{$btntext}}" href="javascript:;" data-id="{{$consultant->CANDIDATEID}}"
                            data-candidate-id="{{$consultant->id}}"
                            class="btn btn-secondary border-rad-9 btn-sm addcompare-icon {{$btnCompare}}">{{$btntext}}</a>
                        <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading" style="display: none;">
                        @if(in_array($consultant->CANDIDATEID,$wishlist))
                        <a title="Remove" href="javascript:;"
                            class="btn btn-red btn-sm  remove-to-wish-list-con border-rad-9 whishilst-herat-icon"
                            data-id="{{$consultant->CANDIDATEID}}" data-candidate-id="{{$consultant->id}}"> Remove
                            Wishlist</a>
                        <!--<span class="btn btn-lightGray disabled btn-sm btnSave">Saved</span>-->
                        <img src="{{admin_asset('images/loader.svg')}}" class="wishlist-loading" style="display: none;">
                        @else
                        <a title="Save" href="javascript:void(0)"
                            class="btn btn-red btn-sm  save-to-wish-list border-rad-9 whishilst-herat-icon"
                            data-id="{{$consultant->CANDIDATEID}}" data-candidate-id="{{$consultant->id}}"> Save to
                            wishlist</a>
                        @endif


                        @endif
                        @else

                        <a title="Add to Compare" href="javascript:;"
                            class="btn btn-secondary  btn-sm btnCompare general-saveto-compare border-rad-9 "
                            data-id="{{$consultant->CANDIDATEID}}" data-candidate-id="{{$consultant->id}}"
                            data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">Add
                            to Compare</a>
                        <a title="Save" href="javascript:void(0)"
                            class="btn btn-red  btn-sm  general-saveto-wishlist border-rad-9 whishilst-herat-icon"
                            data-id="{{$consultant->CANDIDATEID}}" data-candidate-id="{{$consultant->id}}"
                            data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}">
                            Save to wishlist</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
            <div class="commanContent mb-20 consultant-details">
                <div class="row rowContract pb-0 mob-detai-res ">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
                        <p class="pb-2 color6Gray"><span>Billing Rate/day</span></p>
                        <div class="cus-verticale-center">
                            <div class=" text-red inlineText">
                                <!-- <span class="cus-br-icon">BR</span> MYR  -->
                                <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                                <span class="ml-1">MYR </span>
                                <span class="c-job-base-rate"
                                    id="con-copy">{{($consultant->base_rate != null) ? $consultant->base_rate : "0.00"}}</span>/day
                                <div class="iconBox iconCopy inlineText ml-1 copy-to-clipboard job-tile-button-right">
                                    Copy
                                </div>
                            </div>
                        </div>
                        <!-- @if(Auth::user())
                        
                            @if(Auth::user()->role_id == 1)
    
                            <div class=" text-red inlineText"><span class="cus-br-icon">BR</span> MYR <span class="c-job-base-rate" id="con-copy">{{($consultant->base_rate != null) ? $consultant->base_rate : "0.00"}}</span>/day <div class="iconBox iconCopy inlineText ml-1 copy-to-clipboard job-tile-button-right"></div></div>
                            @endif
                        
                        @else
                            <div class=" text-blue general-apply-job" data-redirect="{{\Request::Url() . (\Request::getQueryString() ? ('?' . \Request::getQueryString()) : '')}}"><span class="cus-br-icon">BR</span> Login as a Client to view</div>
                        @endif -->

                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Experience</span></p>
                        <img src="{{admin_asset('images/cus-experience.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$consultant->experience_in_years}} years</div>

                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Notice Period</span></p>
                        <img src="{{admin_asset('images/cus-month.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$consultant->notice_period_days}} days</div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Availability Date</span></p>
                        <img src="{{admin_asset('images/cus-start.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">
                            {{($consultant->availability_date) ? date('d M. Y',strtotime($consultant->availability_date)) : '-'}}
                        </div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Work Preferences</span></p>
                        @php
                        $travelClass = $fulltimeClass = $parttimeClass = "";
                        if($consultant->willing_to_travel == 0){
                        $travelClass = "disabled";
                        }
                        if($consultant->full_time == 0){
                        $fulltimeClass = "disabled";
                        }
                        if($consultant->part_time == 0){
                        $parttimeClass = "disabled";
                        }
                        @endphp
                        <div class="unavalibale {{$fulltimeClass}}">
                            <img src="{{admin_asset('images/cus-ull-time.png')}}" class="icon-cus-img">
                            <div class="ml-1 Black d-inline inlineText {{$fulltimeClass}}">Full time</div>
                        </div>
                        <div class="unavalibale {{$parttimeClass}}">
                            <img src="{{admin_asset('images/cus-Group-8.png')}}" class="icon-cus-img">
                            <div class="ml-1 Black d-inline inlineText {{$parttimeClass}}">Part time</div>
                        </div>
                        <div class="unavalibale {{$travelClass}}">
                            <img src="{{admin_asset('images/cus-group-6.png')}}" class="icon-cus-img will-travel-img">
                            <div class="ml-1 Black d-inline inlineText {{$travelClass}}">Ok to Travel</div>
                        </div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Role</span></p>
                        @php
                        $projectClass = $supportClass = "";
                        if($consultant->project == 0){
                        $projectClass = "disabled";
                        }
                        if($consultant->support == 0){
                        $supportClass = "disabled";
                        }
                        @endphp
                        <div class="unavalibale {{$projectClass}}">
                            <img src="{{admin_asset('images/cus-Group-9.png')}}" class="icon-cus-img">
                            <div class="ml-1 Black d-inline inlineText {{$projectClass}}">Project</div>
                        </div>
                        <div class="unavalibale {{$supportClass}}">
                            <img src="{{admin_asset('images/cus-Group-3.png')}}" class="icon-cus-img">
                            <div class="ml-1 Black d-inline inlineText {{$supportClass}}">Support</div>
                        </div>
                    </div>
                    <div class="col">
                        <p class="pb-2 color6Gray"><span>Location</span></p>
                        <img src="{{admin_asset('images/cus-location.png')}}" class="icon-cus-img">
                        <div class="ml-1 Black d-inline">{{$consultant->city .' '.$consultant->state}}</div>
                    </div>
                </div>
            </div>
            <div class="row rs-10 rowTwoColumn">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="commanContent spacing20 mb-20">
                        <h2 class="headingBorder">Summary</h2>
                        <p class="color6Gray">{!! $consultant->additional_info !!}</p>
                    </div>
                </div>

            </div>
            @if(isset($consultant['skill_set']) && !empty($consultant['skill_set']))
            <div class="row rs-10 rowTwoColumn">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="commanContent spacing20 mb-20">
                        <h2 class="headingBorder">Skill Set</h2>
                        @php
                        $skills = [];
                        if(isset($consultant['skill_set'])){
                        $skills = explode(",",$consultant['skill_set']);
                        }
                        @endphp
                        <ul class="listSkills listSkillsBig">
                            @if(count($skills))
                            @foreach($skills as $skillValue)
                            <li>
                                <a href="javascript:void(0)" class="cursor-default">{{$skillValue}}</a>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @else
            <div class="row rs-10 rowTwoColumn">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="commanContent spacing20 mb-20">
                        <h2 class="headingBorder">Skill Set</h2>
                        
                        <ul class="listSkills listSkillsBig">
                        
                        <ul class="listContract">
                                No skills listed
                            </li>
                        
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            <div class="row rs-10 rowTwoColumn">
                @if(count($experience))
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                    <div class="commanContent spacing20 mb-20">
                        <h2 class="headingBorder">Work Experience</h2>

                        @foreach($experience as $expKey => $expeValue)
                        @if( $expKey < 10) <ul class="listContract">
                            <li class="color6Gray">{{$expeValue->occupation}}</li>
                            <li class="color6Gray">{{$expeValue->industry}}</li>
                            <li class="color6Gray">{{date('M. Y' , strtotime('01-'.$expeValue->work_duration))}}
                                @if(!empty($expeValue->is_currently_working_here) &&
                                $expeValue->is_currently_working_here == 'true')
                                - Present
                                @else
                                - {{date('M. Y' , strtotime('01-'.$expeValue->work_duration_to))}}
                                @endif</li>
                            </ul>
                            @endif
                            @endforeach
                    </div>
                </div>
                @else
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                    <div class="commanContent spacing20 mb-20">
                        <h2 class="headingBorder">Work Experience</h2>

                        <ul class="listContract">
                            No Experience listed
                            </ul>
                            
                    </div>
                </div>
                @endif
                @if(isset($consultant['certification_and_training']) &&
                !empty($consultant['certification_and_training']))
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="commanContent spacing20 mb-20">
                                    <h2 class="headingBorder">Certification &amp; Training</h2>
                                    @php
                                    $certificate = [];
                                    if(isset($consultant['certification_and_training'])){
                                    $certificates = explode(",",$consultant['certification_and_training']);
                                    }
                                    @endphp
                                    @if(count($certificates) && !empty($consultant['certification_and_training']))
                                    <ul class="listContent listBullets">

                                        @foreach($certificates as $certificateValue)
                                        <li class="color6Gray">
                                            {{$certificateValue}}
                                        </li>
                                        @endforeach
                                    </ul>
                                    
                                    

                        
                        @endif
                        
                    </div>
                </div>
                @else
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="commanContent spacing20 mb-20">
                                    <h2 class="headingBorder">Certification &amp; Training</h2>
                                   
                                    <ul class="listContent listBullets">

                                   
                                    <ul class="listContract">
                            No Certification &amp; Training listed
                            </ul>
                                   
                                    </ul>
                                    
                                    

                        
                       
                        
                    </div>
                </div>
                @endif
            </div>
            <div class="row rs-10">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                    <?php
                    $url = url()->current();
                    $twitterUrl = 'http://twitter.com/share?text=' . $consultant->sap_job_title . '&url=' . $url;
                    ?>
                    <ul class="listSocialMedia listSocialMediaSmall mb-4">
                        <li>
                            <a onClick="window.open('<?php echo $twitterUrl; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"
                                target="_parent" href="javascript: void(0)">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php echo $url; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"
                                target="_parent" href="javascript: void(0)">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a onClick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo  $consultant->sap_job_title; ?>%20Developer%20Network&source=LinkedIn','sharer','toolbar=0,status=0,width=548,height=325');"
                                target="_parent" href="javascript: void(0)">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- <ul class="listSocialMedia listSocialMediaSmall mb-4">
                        <li>
                            <a href="#" title="twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="facebook">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="linkedin">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul> -->
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">

                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 howmuch-3">
            <h3 class="header-red-bg colorWhite text-center pd-5 how-much-left">Doing Your Budget?</h3>
            <div class="commanContent boxHowmuch">
                <div class="contentHowmuch">
                    <p class="pb-1">Enter Base Rate</p>
                    <div class=" text-red inlineText">
                        <!-- <span class="cus-br-icon">BR</span> MYR -->
                        <img class="cus-br-icon-small" src="{{admin_asset('images/cus-br-icon.png')}}">
                        <span class="ml-1">MYR </span>
                    </div>
                    <input type="text" class="text-red text-right" value="" id="c3-base-rate-perday" />
                    <span class="text-red">/day</span>
                </div>
               <div class="row f-12 rs-5">
                    <div class="col-md-6">
                        <p class="pb-2">Length of contract
                            <a href="javascript:void(0)" class="iconInfo">
                            <span class="tooltiptext">Calculated based on 20 mandays/month. Surcharge of 5% is applicable for 3-month period and 3% discount will be given for 12-month period.</span>
                                <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                            </a>
                        </p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="radio" name="months" class="months" value="1" title="0-5 months" /> 0-5
                                months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="2" title="6-11 months"
                                    checked="" /> 6-11 months</li>
                            <li>
                                <input type="radio" name="months" class="months" value="3" title="> 12 months" /> > 12
                                months</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <p class="pb-2">Fees</p>
                        <ul class="listCheckbox">
                            <li>
                                <input type="checkbox" class="fees" name="admin_fee" title="Admin fee" id="admin_fee"
                                    checked="" disabled="" /> Admin fee
                                <a href="javascript:void(0)" class="iconInfo">
                                <span class="tooltiptext1">For talent management, onboarding process, payroll hosting, basic administrations and other related handling process.</span>
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                            <li>
                                <input type="checkbox" class="fees" name="professional_fee" title="Professional fee"
                                    id="professional_fee" checked="" /> Professional fee
                                <a href="javascript:void(0)" class="iconInfo">
                                    <span class="tooltiptext1">For talent acquisition, communication and mediation process.</span>
                                    <img src="{{admin_asset('images/iconInfo.png')}}" alt="" title="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="boxDoyouKnow mb-0">
                    <h6 class="f-16 mb-0">The Billing Rate is</h6>
                    <h2 class="text-red" id="billing-rate">MYR 0.00/day</h2>
                </div>
            </div>
            @include('Layouts.General.doyouknow')
            @include('Layouts.General.tips')
        </div>
    </div>
</div>
{{-- <section class="contributesection">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="htagf">Want to contribute?</h3>
                <p class="ptagf">Feel free to drop us an email</p>
                <div class="btntagf"><a href="{{route('contactus')}}">Contact Us</a></div>
            </div>
        </div>
    </div>
</section> --}}
<?php

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>
@endsection