<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('cors')->get('/job-search', 'SearchController@index');
Route::middleware('cors')->get('/fetchState', 'SearchController@fetchState');
Route::middleware('cors')->get('/fetchCity', 'SearchController@fetchCity');
Route::middleware('cors')->get('/fetchIndustry', 'SearchController@fetchIndustry');
Route::middleware('cors')->get('/doYouKnow', 'SearchController@doYouKnow');
Route::middleware('cors')->get('/tips', 'SearchController@tips');


Route::middleware('cors')->get('/consultant-search', 'SearchController@indexConsultant');
Route::middleware('cors')->get('{userId}/get-total-compare', 'SearchController@getTotalCompare');
Route::middleware('cors')->post('/createClient', 'SearchController@createUserClient');
// Route::middleware('cors')->post('/testapi','SearchController@testfunction');

// zoho to laravel api for updating record on workflow trigger
Route::middleware('cors')->post('/aplikasiUpdateConsultant','SearchController@updateConsultant');
Route::middleware('cors')->post('/aplikasiUpdateClient','SearchController@updateClient');