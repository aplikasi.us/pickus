<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::prefix('admin')->namespace('Admin')->group(function () {

    Route::get('/', 'LoginController@index');
    Route::get('login', 'LoginController@index');
    Route::get('login/check', 'LoginController@check');
    Route::post('login/check', 'LoginController@check');
    Route::get('logout', 'LoginController@logout');



    Route::middleware(['AdminAuth'])->group(function () {

        /* Route::resource('dashboard', 'DashboardController@index'); */

        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        
        Route::any('territorystore', 'DashboardController@territorySession')->name('territorystore');
        //for jobopenings
        Route::resource('jobs', 'JobOpeningController');
        Route::get('jobs/{id}/create', 'JobOpeningController@create')->name('job_create');
        Route::post('jobs/destroy', 'JobOpeningController@destroy')->name('job_destroy');
        Route::post('jobs/change_status', 'JobOpeningController@status')->name('job_status');
        // City List
        Route::post('jobs/city-list', 'JobOpeningController@getCityList')->name('job-city-list');
        // Approve Job
        Route::get('jobs/approve/{id}', 'JobOpeningController@approveJob')->name('approve-job');
        //Job requested
        Route::get('jobsrequested', 'JobOpeningController@jobRequestedProfile')->name('jobRequestedProfile');

        //clients
        Route::resource('clients', 'ClientsController');
        Route::post('clients/change_status', 'ClientsController@status')->name('client_status');
        Route::get('clients/requestedprofile/{id}', 'ClientsController@viewRequestProfile')->name('viewRequestProfile');
        Route::get('clients/approverequestedprofile/{id}', 'ClientsController@approveRequestProfile')->name('approvprofileadmin');
        Route::get('clients/cancelrequestedprofile/{id}', 'ClientsController@cancelRequestProfile')->name('cancelprofileadmin');
        Route::get('clients/delete/{id}', 'ClientsController@deleteClient')->name('deleteclient');
        // Restore Client Contact Account
        Route::get('restoreaccount/{id}', 'ClientsController@restoreClientContactAccount')->name('restore-clientcontact-account');
        
        //Requsted profile view
        Route::get('/requestprofilelist', 'ClientsController@indexRequestProfile')->name('indexRequestProfile');
        Route::get('requestprofilelist/downloadcv/{id}', 'ClientsController@formattedResumeDownload')->name('formattedResumeDownload');

        Route::post('requestprofilelist/delete', 'ClientsController@multipleDeleteRequest')->name('multipledeleteRequestedprofile');
        Route::post('requestprofilelist/downloadcv', 'ClientsController@multipleFormattedResumeDownload')->name('multipleFormattedResumeDownload');

        //consultant
        Route::resource('consultant', 'ConsultantController');
        Route::post('consultant/change_status', 'ConsultantController@status')->name('consultant_status');
        Route::get('consultant/edit/{id}', 'ConsultantController@editConsultant')->name('editConsultant');
        Route::post('consultant/edit/summaryapprove/{id}', 'ConsultantController@summaryApprove')->name('consultantsummaryapprove');
        Route::get('consultant/delete/{id}', 'ConsultantController@deleteConsultant')->name('delete-consultant');
        Route::get('consultant/associate-jobopenings/{id}', 'ConsultantController@associateJobOpenings')->name('associate-job-openings');
        Route::post('consultant/change-consultant-status', 'ConsultantController@changeConsultantStatus')->name('change-consultant-status');
        // Upcoming Interviews
        Route::get('upcoming-interviews', 'ConsultantController@upcomingInterviews')->name('upcoming_interviews');
        // Add candidate experience
        Route::any('add/experience/{id}', 'ConsultantController@addExperience')->name('addexperience');
        // Restore Consultant Account
        Route::get('restoreconsultantaccount/{id}', 'ConsultantController@restoreConsultantAccount')->name('restore-consultant-account');
        
        // Edit candidate experience
        Route::any('edit/experience/{id}', 'ConsultantController@updateExperience')->name('updateexperience');
        // Add candidate experience
        Route::any('delete/experience/{id}', 'ConsultantController@deleteExperience')->name('deleteexperience');
        
        // Add candidate education
        Route::any('add/education/{id}', 'ConsultantController@addEducation')->name('addeducation');
        // Edit candidate education
        Route::any('edit/education/{id}', 'ConsultantController@updateEducation')->name('updateeducation');
        // Delete candidate experience
        Route::any('delete/education/{id}', 'ConsultantController@deleteEducation')->name('deleteeducation');
        // Add candidate reference
        Route::any('add/reference/{id}', 'ConsultantController@addReference')->name('addreference');
        // Edit candidate reference
        Route::any('edit/reference/{id}', 'ConsultantController@updateReference')->name('updatereference');
        // Delete candidate reference
        Route::any('delete/reference/{id}', 'ConsultantController@deleteReference')->name('deletereference');
        //systemuser
        Route::resource('systemuser', 'UserController');
        Route::post('systemuser-exists', 'UserController@isUserEmailExists')->name('isUserEmailExists');
        Route::post('systemuser/change_status', 'UserController@status')->name('systemuser_status');

        Route::post('systemuser/delete', 'UserController@delete')->name('systemuser_delete');

        Route::get('systemuser/deleterecord/{id}', 'UserController@deleteSingleRecord')->name('systemuser_delete_record');

        Route::post('systemuser/changepwd', 'UserController@changepwd')->name('changepwd');

        /* Setting Start */
        // Setting email templates
        Route::get('settings/emailtemplate', 'SettingController@emailTemplates')->name('emailtemplate');
        // Edit Email Template
        Route::get('settings/emailtemplate/edit/{id}', 'SettingController@editEmailTemplate')->name('editemailtemplate');
        // Update Email Template Settings
        Route::post('settings/emailtemplate/update', 'SettingController@updateEmailTemplate')->name('updateemailtemplate');
        // Edit Site Settings
        Route::get('settings/site', 'SettingController@site')->name('site');
        // Update Site Settings
        Route::post('settings/updatesite', 'SettingController@updateSiteSettings')->name('updatesite');
        // FAQ List
        Route::get('faq', 'SettingController@faq')->name('faq');
        // Add FAQ
        Route::any('faq/add', 'SettingController@addFAQ')->name('addfaq');
        // Update FAQ
        Route::any('faq/update/{id}', 'SettingController@updateFAQ')->name('updatefaq');
        // Delete FAQ
        Route::get('faq/delete/{id}', 'SettingController@deleteFAQ')->name('deletefaq');
        //Notification
        Route::get('settings/notification', 'SettingController@notification')->name('adminnotification');
        //Sync data from zoho
        Route::any('synczohodata', 'SettingController@syncZohoData')->name('synczohodata');
        // Verifry Admin Password
        Route::post('verifrypassword', 'SettingController@verifyAdminPassword')->name('verify_admin_password');
        // Taglines
        Route::any('taglines', 'SettingController@tagLines')->name('taglines');
        // Add tagline
        Route::any('tagline/add', 'SettingController@addTagline')->name('addtagline');
        // Update tagline
        Route::any('tagline/update/{id}', 'SettingController@updateTagline')->name('updatetagline');
        // Delete tagline
        Route::get('tagline/delete/{id}', 'SettingController@deleteTagline')->name('deletetagline');
        // Read and Remove Notification Counter
        Route::post('/readadminnotification', 'SettingController@readNotification')->name('readadminnotification');
        // Change Contact Password
        Route::get('changecontactpassword/{id}', 'ClientsController@contactChangePassword')->name('changecontactpassword');
        
        
        
    });
});

Route::get('/', 'HomeController@checkCountry')->name('checkcountry');
//Route::get('/', 'HomeController@index')->name('dashboard');

Route::middleware(['DefaultLocale'])->prefix('{locale}')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
  
    Route::get('/login-successful', 'HomeController@loginRedirect')->name('login-successful');
});

Route::get('/apicalltest', 'ApiController@test');
// Route::get('/', 'ApiController@test');

/* Auth::routes();
  Route::get('/admin', 'Auth\LoginController@login'); */
/* Route::get('admin/dashboard', 'HomeController@dashboard')->name('dashboard'); */

/* Route::middleware(['auth'])->group(function () { */
Route::any('/setterritories', 'HomeController@setTerritories')->name('setterritories');


/*});*/